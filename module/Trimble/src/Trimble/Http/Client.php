<?php

namespace Trimble\Http;


use Trimble\Options\TrimbleConfiguration;
use Zend\Http\Cookies;
use Zend\Http\Request;
use Zend\Http\Response;
use Zend\Session\Container;
use Zend\Stdlib\ArraySerializableInterface;

class Client extends \Zend\Http\Client
{
    const MAX_LOGIN_TRIES = 3;

    /**
     * Configuration array, set using the constructor or using ::setOptions()
     *
     * @var array
     */
    protected $config = [
        'maxredirects' => 5,
        'strictredirects' => false,
        'useragent' => 'Zend\Http\Client',
        'timeout' => 100,
        'adapter' => 'Zend\Http\Client\Adapter\Curl',
        'httpversion' => Request::VERSION_11,
        'storeresponse' => true,
        'keepalive' => true,
        'outputstream' => false,
        'encodecookies' => true,
        'argseparator' => null,
        'rfc3986strict' => false,
        'sslverifypeer' => false,
        'curloptions' => [
            CURLOPT_ENCODING => 'gzip'
        ]
    ];

    /**
     * @var TrimbleConfiguration
     */
    protected $configuration;

    protected $retries = 0;
    protected $lastResponse;

    public function __construct(TrimbleConfiguration $configuration, $uri = null, $options = null)
    {
        $this->configuration = $configuration;
        parent::__construct($uri, $options);
        $this->setHeaders([
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Accept-Encoding' => 'gzip'
        ]);
    }

    public function send(Request $request = null, ArraySerializableInterface $object = null)
    {
        $this->setAuth($this->configuration->getUserName(), $this->configuration->getPassword());
        $jar = $this->getCookiesJar();
        if ($jar !== null) {
            $cookies = $jar->getMatchingCookies($this->getUri());
            $this->clearCookies();
            $this->addCookie($cookies);
        }
        $response = parent::send($request);
        if ($response->isForbidden() || $response->getStatusCode() === 401) {
            throw new \Exception('Could not authenticate to Trimble');
        }
        $this->setCookieJar($response);
        if ($object === null) {
            return $response;
        }
        if ($response->isOk()) {
            try {
                $data = json_decode($response->getContent(), true);
                $object->exchangeArray($data);
            } catch (\Exception $e) {
                throw new \Exception('Unable to parse received data!');
            }
        }
        return $object;
    }

    public function setPath($path)
    {
        $this->setUri($this->configuration->getUrl() . $path);
    }

    protected function setCookieJar(Response $response)
    {
        $container = new Container('trimble');
        $container['cookies'] = Cookies::fromResponse($response, $this->getUri());
    }

    /**
     * @return Cookies
     */
    protected function getCookiesJar()
    {
        $container = new Container('trimble');
        if (!isset($container['cookies'])) {
            return null;
        }
        return $container['cookies'];
    }
}