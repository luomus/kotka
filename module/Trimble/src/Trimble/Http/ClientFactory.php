<?php

namespace Trimble\Http;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ClientFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /** @var \Trimble\Options\TrimbleConfiguration $options */
        $options = $serviceLocator->get('Trimble\Options\TrimbleConfiguration');
        return new Client($options);
    }
}
