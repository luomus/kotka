<?php

namespace Trimble\Options;

use RuntimeException;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class TrimbleConfigurationFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $options = $this->getOptions($serviceLocator);
        $config = new TrimbleConfiguration($options);

        return $config;
    }

    /**
     * @param  ServiceLocatorInterface $serviceLocator
     * @return mixed
     * @throws RuntimeException
     */
    public function getOptions(ServiceLocatorInterface $serviceLocator)
    {
        $options = $serviceLocator->get('Config');
        $options = isset($options['trimble']) ? $options['trimble'] : null;

        if (null === $options) {
            throw new RuntimeException(
                'Configuration for trimble service could not be found in "trimble".'
            );
        }

        return $options;
    }
}
