<?php

namespace Trimble\Model;

use Zend\Stdlib\ArraySerializableInterface;

class Auth implements ArraySerializableInterface
{
    const RESOURCE_PATH = '/auth';

    /** @var string */
    protected $provider;
    /** @var string */
    protected $state;
    /** @var string */
    protected $oauth_token;
    /** @var string */
    protected $oauth_verifier;
    /** @var string */
    protected $userName;
    /** @var string */
    protected $password;
    /** @var boolean */
    protected $rememberMe;
    /** @var string */
    protected $continue;
    /** @var string */
    protected $nonce;
    /** @var string */
    protected $uri;
    /** @var string */
    protected $response;
    /** @var string */
    protected $qop;
    /** @var string */
    protected $nc;
    /** @var string */
    protected $cnonce;

    /**
     * @return string
     */
    public function getProvider()
    {
        return $this->provider;
    }

    /**
     * @param string $provider
     */
    public function setProvider($provider)
    {
        $this->provider = $provider;
    }

    /**
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param string $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return string
     */
    public function getOauthToken()
    {
        return $this->oauth_token;
    }

    /**
     * @param string $oauth_token
     */
    public function setOauthToken($oauth_token)
    {
        $this->oauth_token = $oauth_token;
    }

    /**
     * @return string
     */
    public function getOauthVerifier()
    {
        return $this->oauth_verifier;
    }

    /**
     * @param string $oauth_verifier
     */
    public function setOauthVerifier($oauth_verifier)
    {
        $this->oauth_verifier = $oauth_verifier;
    }

    /**
     * @return string
     */
    public function getUserName()
    {
        return $this->userName;
    }

    /**
     * @param string $userName
     */
    public function setUserName($userName)
    {
        $this->userName = $userName;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return boolean
     */
    public function isRememberMe()
    {
        return $this->rememberMe;
    }

    /**
     * @param boolean $rememberMe
     */
    public function setRememberMe($rememberMe)
    {
        $this->rememberMe = $rememberMe;
    }

    /**
     * @return string
     */
    public function getContinue()
    {
        return $this->continue;
    }

    /**
     * @param string $continue
     */
    public function setContinue($continue)
    {
        $this->continue = $continue;
    }

    /**
     * @return string
     */
    public function getNonce()
    {
        return $this->nonce;
    }

    /**
     * @param string $nonce
     */
    public function setNonce($nonce)
    {
        $this->nonce = $nonce;
    }

    /**
     * @return string
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * @param string $uri
     */
    public function setUri($uri)
    {
        $this->uri = $uri;
    }

    /**
     * @return string
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @param string $response
     */
    public function setResponse($response)
    {
        $this->response = $response;
    }

    /**
     * @return string
     */
    public function getQop()
    {
        return $this->qop;
    }

    /**
     * @param string $qop
     */
    public function setQop($qop)
    {
        $this->qop = $qop;
    }

    /**
     * @return string
     */
    public function getNc()
    {
        return $this->nc;
    }

    /**
     * @param string $nc
     */
    public function setNc($nc)
    {
        $this->nc = $nc;
    }

    /**
     * @return string
     */
    public function getCnonce()
    {
        return $this->cnonce;
    }

    /**
     * @param string $cnonce
     */
    public function setCnonce($cnonce)
    {
        $this->cnonce = $cnonce;
    }

    /**
     * Exchange internal values from provided array
     *
     * @param  array $array
     * @return void
     */
    public function exchangeArray(array $array)
    {
        foreach ($array as $key => $value) {
            $this->$key = $value;
        }
    }

    /**
     * Return an array representation of the object
     *
     * @return array
     */
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
}