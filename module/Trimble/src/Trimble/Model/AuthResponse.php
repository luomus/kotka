<?php

namespace Trimble\Model;


use Zend\Stdlib\ArraySerializableInterface;

class AuthResponse implements ArraySerializableInterface
{
    /** @var string */
    protected $sessionId;
    /** @var string */
    protected $userName;
    /** @var string */
    protected $referrerUrl;
    /** @var ResponseStatus */
    protected $responseStatus;

    /**
     * @return string
     */
    public function getSessionId()
    {
        return $this->sessionId;
    }

    /**
     * @param string $sessionId
     */
    public function setSessionId($sessionId)
    {
        $this->sessionId = $sessionId;
    }

    /**
     * @return string
     */
    public function getUserName()
    {
        return $this->userName;
    }

    /**
     * @param string $userName
     */
    public function setUserName($userName)
    {
        $this->userName = $userName;
    }

    /**
     * @return string
     */
    public function getReferrerUrl()
    {
        return $this->referrerUrl;
    }

    /**
     * @param string $referrerUrl
     */
    public function setReferrerUrl($referrerUrl)
    {
        $this->referrerUrl = $referrerUrl;
    }

    /**
     * @return ResponseStatus
     */
    public function getResponseStatus()
    {
        return $this->responseStatus;
    }

    /**
     * @param ResponseStatus $responseStatus
     */
    public function setResponseStatus(ResponseStatus $responseStatus)
    {
        $this->responseStatus = $responseStatus;
    }

    /**
     * Exchange internal values from provided array
     *
     * @param  array $array
     * @return void
     */
    public function exchangeArray(array $array)
    {
        if (isset($array['responseStatus'])) {
            $this->responseStatus = new ResponseStatus();
            $this->responseStatus->exchangeArray($array['responseStatus']);
            unset($array['responseStatus']);
        }
        foreach ($array as $key => $value) {
            $this->$key = $value;
        }
    }

    /**
     * Return an array representation of the object
     *
     * @return array
     */
    public function getArrayCopy()
    {
        $array = get_object_vars($this);
        if (isset($array['responseStatus']) && $array['responseStatus'] instanceof ResponseStatus) {
            $array['responseStatus'] = $array['responseStatus']->getArrayCopy();
        }
        return $array;
    }
}