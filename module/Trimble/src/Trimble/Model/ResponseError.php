<?php
namespace Trimble\Model;

use Trimble\Stdlib\SimpleArraySerializableTrait;
use Zend\Stdlib\ArraySerializableInterface;

class ResponseError implements ArraySerializableInterface
{
    use SimpleArraySerializableTrait;

    /** @var string */
    protected $errorCode;
    /** @var string */
    protected $fieldName;
    /** @var string */
    protected $message;

    /**
     * @return string
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @param string $errorCode
     */
    public function setErrorCode($errorCode)
    {
        $this->errorCode = $errorCode;
    }

    /**
     * @return string
     */
    public function getFieldName()
    {
        return $this->fieldName;
    }

    /**
     * @param string $fieldName
     */
    public function setFieldName($fieldName)
    {
        $this->fieldName = $fieldName;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }
}