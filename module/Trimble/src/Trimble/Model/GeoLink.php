<?php

namespace Trimble\Model;

use Trimble\Stdlib\SimpleArraySerializableTrait;
use Zend\Stdlib\ArraySerializableInterface;

class GeoLink implements ArraySerializableInterface
{
    use SimpleArraySerializableTrait;

    /** @var string */
    protected $hRef;
    /** @var string */
    protected $rel;

    /**
     * @return string
     */
    public function getHRef()
    {
        return $this->hRef;
    }

    /**
     * @param string $hRef
     */
    public function setHRef($hRef)
    {
        $this->hRef = $hRef;
    }

    /**
     * @return string
     */
    public function getRel()
    {
        return $this->rel;
    }

    /**
     * @param string $rel
     */
    public function setRel($rel)
    {
        $this->rel = $rel;
    }
}