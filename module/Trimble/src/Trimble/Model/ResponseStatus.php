<?php

namespace Trimble\Model;


use Zend\Stdlib\ArraySerializableInterface;

class ResponseStatus implements ArraySerializableInterface
{
    /** @var string */
    protected $errorCode;
    /** @var string */
    protected $message;
    /** @var string */
    protected $stackTrace;
    /** @var array */
    protected $errors;

    /**
     * @return string
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @param string $errorCode
     */
    public function setErrorCode($errorCode)
    {
        $this->errorCode = $errorCode;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return string
     */
    public function getStackTrace()
    {
        return $this->stackTrace;
    }

    /**
     * @param string $stackTrace
     */
    public function setStackTrace($stackTrace)
    {
        $this->stackTrace = $stackTrace;
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param array $errors
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;
    }

    /**
     * Exchange internal values from provided array
     *
     * @param  array $array
     * @return void
     */
    public function exchangeArray(array $array)
    {
        if (isset($array['errors'])) {
            $this->errors = new ResponseError();
            $this->errors->exchangeArray($array['errors']);
            unset($array['errors']);
        }
        foreach ($array as $key => $value) {
            $this->$key = $value;
        }
    }

    /**
     * Return an array representation of the object
     *
     * @return array
     */
    public function getArrayCopy()
    {
        $array = get_object_vars($this);
        if (isset($array['errors'])) {
            foreach ($array['errors'] as $key => $value) {
                if ($value instanceof ResponseError) {
                    $array['errors'][$key] = $value->getArrayCopy();
                }
            }
        }
        return $array;
    }
}