<?php
namespace Trimble\Model;


use Trimble\Stdlib\SimpleArraySerializableTrait;
use Zend\Stdlib\ArraySerializableInterface;

class JToken implements ArraySerializableInterface
{
    use SimpleArraySerializableTrait;

    protected $children = [
        'parent' => '\Trimble\Model\JContainer',
        'root' => '\Trimble\Model\JToken',
        'next' => '\Trimble\Model\JToken',
        'previous' => '\Trimble\Model\JToken',
        'item' => '\Trimble\Model\JToken',
        'first' => '\Trimble\Model\JToken',
        'last' => '\Trimble\Model\JToken',
    ];

    /** @var JTokenEqualityComparer */
    protected $equalityComparer;
    /** @var JContainer */
    protected $parent;
    /** @var JToken */
    protected $root;
    /** @var string */
    protected $type;
    /** @var boolean */
    protected $hasValues;
    /** @var JToken */
    protected $next;
    /** @var JToken */
    protected $previous;
    /** @var string */
    protected $path;
    /** @var JToken */
    protected $item;
    /** @var JToken */
    protected $first;
    /** @var JToken */
    protected $last;

    /**
     * @return array
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param array $children
     */
    public function setChildren($children)
    {
        $this->children = $children;
    }

    /**
     * @return JTokenEqualityComparer
     */
    public function getEqualityComparer()
    {
        return $this->equalityComparer;
    }

    /**
     * @param JTokenEqualityComparer $equalityComparer
     */
    public function setEqualityComparer($equalityComparer)
    {
        $this->equalityComparer = $equalityComparer;
    }

    /**
     * @return JContainer
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param JContainer $parent
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
    }

    /**
     * @return JToken
     */
    public function getRoot()
    {
        return $this->root;
    }

    /**
     * @param JToken $root
     */
    public function setRoot($root)
    {
        $this->root = $root;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return boolean
     */
    public function isHasValues()
    {
        return $this->hasValues;
    }

    /**
     * @param boolean $hasValues
     */
    public function setHasValues($hasValues)
    {
        $this->hasValues = $hasValues;
    }

    /**
     * @return JToken
     */
    public function getNext()
    {
        return $this->next;
    }

    /**
     * @param JToken $next
     */
    public function setNext($next)
    {
        $this->next = $next;
    }

    /**
     * @return JToken
     */
    public function getPrevious()
    {
        return $this->previous;
    }

    /**
     * @param JToken $previous
     */
    public function setPrevious($previous)
    {
        $this->previous = $previous;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param string $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * @return JToken
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * @param JToken $item
     */
    public function setItem($item)
    {
        $this->item = $item;
    }

    /**
     * @return JToken
     */
    public function getFirst()
    {
        return $this->first;
    }

    /**
     * @param JToken $first
     */
    public function setFirst($first)
    {
        $this->first = $first;
    }

    /**
     * @return JToken
     */
    public function getLast()
    {
        return $this->last;
    }

    /**
     * @param JToken $last
     */
    public function setLast($last)
    {
        $this->last = $last;
    }
}