<?php

namespace Trimble\Model;

use Trimble\Stdlib\SimpleArraySerializableTrait;
use Zend\Stdlib\ArraySerializableInterface;

class JObject implements ArraySerializableInterface
{
    use SimpleArraySerializableTrait;

    protected $children = [
        'item' => '\Trimble\Model\JToken',
        'first' => '\Trimble\Model\JToken',
        'last' => '\Trimble\Model\JToken',
        'parent' => '\Trimble\Model\JContainer',
        'root' => '\Trimble\Model\JToken',
        'next' => '\Trimble\Model\JToken',
        'previous' => '\Trimble\Model\JToken',
    ];

    /** @var string */
    protected $type;
    /** @var JToken */
    protected $item;
    /** @var boolean */
    protected $hasValues;
    /** @var JToken */
    protected $first;
    /** @var JToken */
    protected $last;
    /** @var int */
    protected $count;
    /** @var JContainer */
    protected $parent;
    /** @var JToken */
    protected $root;
    /** @var JToken */
    protected $next;
    /** @var JToken */
    protected $previous;
    /** @var string */
    protected $path;
    /** @var array */
    protected $properties;

    /**
     * @return array
     */
    public function getProperties()
    {
        return $this->properties;
    }

    /**
     * @param array $properties
     */
    public function setProperties($properties)
    {
        $this->properties = $properties;
    }

    /**
     * @param $property
     * @return bool
     */
    public function hasProperty($property)
    {
        return isset($this->properties[$property]);
    }

    public function getProperty($property)
    {
        return isset($this->properties[$property]) ? $this->properties[$property] : null;
    }


}