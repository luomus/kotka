<?php

namespace Trimble\Model;

use Trimble\Stdlib\SimpleArraySerializableTrait;
use Zend\Stdlib\ArraySerializableInterface;

class JContainer implements ArraySerializableInterface
{
    use SimpleArraySerializableTrait;

    protected $children = [
        'first' => '\Trimble\Model\JToken',
        'last' => '\Trimble\Model\JToken',
        'root' => '\Trimble\Model\JToken',
        'next' => '\Trimble\Model\JToken',
        'previous' => '\Trimble\Model\JToken',
        'item' => '\Trimble\Model\JToken',
        'parent' => '\Trimble\Model\JContainer'
    ];

    /** @var boolean */
    protected $hasValues;
    /** @var JToken */
    protected $first;
    /** @var JToken */
    protected $last;
    /** @var int */
    protected $count;
    /** @var JContainer */
    protected $parent;
    /** @var JToken */
    protected $root;
    /** @var string */
    protected $type;
    /** @var JToken */
    protected $next;
    /** @var JToken */
    protected $previous;
    /** @var string */
    protected $path;
    /** @var JToken */
    protected $item;

    /**
     * @return array
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param array $children
     */
    public function setChildren($children)
    {
        $this->children = $children;
    }

    /**
     * @return boolean
     */
    public function isHasValues()
    {
        return $this->hasValues;
    }

    /**
     * @param boolean $hasValues
     */
    public function setHasValues($hasValues)
    {
        $this->hasValues = $hasValues;
    }

    /**
     * @return JToken
     */
    public function getFirst()
    {
        return $this->first;
    }

    /**
     * @param JToken $first
     */
    public function setFirst(JToken $first)
    {
        $this->first = $first;
    }

    /**
     * @return JToken
     */
    public function getLast()
    {
        return $this->last;
    }

    /**
     * @param JToken $last
     */
    public function setLast(JToken $last)
    {
        $this->last = $last;
    }

    /**
     * @return int
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @param int $count
     */
    public function setCount($count)
    {
        $this->count = $count;
    }

    /**
     * @return JContainer
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param JContainer $parent
     */
    public function setParent(JContainer $parent)
    {
        $this->parent = $parent;
    }

    /**
     * @return JToken
     */
    public function getRoot()
    {
        return $this->root;
    }

    /**
     * @param JToken $root
     */
    public function setRoot(JToken $root)
    {
        $this->root = $root;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return JToken
     */
    public function getNext()
    {
        return $this->next;
    }

    /**
     * @param JToken $next
     */
    public function setNext(JToken $next)
    {
        $this->next = $next;
    }

    /**
     * @return JToken
     */
    public function getPrevious()
    {
        return $this->previous;
    }

    /**
     * @param JToken $previous
     */
    public function setPrevious(JToken $previous)
    {
        $this->previous = $previous;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param string $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * @return JToken
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * @param JToken $item
     */
    public function setItem(JToken $item)
    {
        $this->item = $item;
    }
}