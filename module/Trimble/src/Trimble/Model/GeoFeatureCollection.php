<?php

namespace Trimble\Model;


use Trimble\Stdlib\SimpleArraySerializableTrait;
use Zend\Stdlib\ArraySerializableInterface;

class GeoFeatureCollection implements ArraySerializableInterface
{
    use SimpleArraySerializableTrait;

    const RESOURCE_PATH = '/features';

    protected $children = [
        'links' => ['\Trimble\Model\GeoLink'],
        'features' => ['\Trimble\Model\JObject'],
    ];

    /** @var array */
    protected $links;
    /** @var int */
    protected $total;
    /** @var string */
    protected $type;
    /** @var array */
    protected $features;

    /**
     * @return array
     */
    public function getLinks()
    {
        return $this->links;
    }

    /**
     * @param array $links
     */
    public function setLinks($links)
    {
        $this->links = $links;
    }

    /**
     * @return int
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param int $total
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return array
     */
    public function getFeatures()
    {
        return $this->features;
    }

    /**
     * @param array $features
     */
    public function setFeatures($features)
    {
        $this->features = $features;
    }
}