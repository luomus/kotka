<?php

namespace Trimble\Stdlib;


use Zend\Stdlib\ArraySerializableInterface;

trait SimpleArraySerializableTrait
{
    /**
     * Exchange internal values from provided array
     *
     * @param  array $array
     * @return void
     */
    public function exchangeArray(array $array)
    {
        if (property_exists($this, 'children')) {
            foreach ($this->children as $key => $class) {
                if (isset($array[$key])) {
                    if (is_array($class)) {
                        $class = $class[0];
                        $this->$key = [];
                        foreach ($array[$key] as $idx => $value) {
                            $this->{$key}[$idx] = new $class();
                            $this->{$key}[$idx]->exchangeArray($value);
                        }
                    } else {
                        $this->$key = new $class();
                        $this->$key->exchangeArray($array[$key]);
                    }
                    unset($array[$key]);
                }
            }
        }
        foreach ($array as $key => $value) {
            $this->$key = $value;
        }
    }

    /**
     * Return an array representation of the object
     *
     * @return array
     */
    public function getArrayCopy()
    {
        $array = get_object_vars($this);
        if (property_exists($this, 'children')) {
            foreach ($this->children as $key => $class) {
                if (isset($array[$key])) {
                    if (is_array($class)) {
                        foreach ($array[$key] as $idx => $value) {
                            if ($value instanceof ArraySerializableInterface) {
                                $array[$key][$idx] = $value->getArrayCopy();
                            }
                        }
                    } else {
                        if ($array[$key] instanceof ArraySerializableInterface) {
                            $array[$key] = $array[$key]->getArrayCopy();
                        }
                    }
                }
            }
        }
        return $array;
    }
}