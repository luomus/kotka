<?php
namespace Trimble\Service;

use Trimble\Http\Client;
use Trimble\Model\GeoFeatureCollection;

class FeatureService
{
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
        $this->client->setPath(GeoFeatureCollection::RESOURCE_PATH);
    }

    /**
     * @return GeoFeatureCollection
     * @throws \Exception
     */
    public function getFeatures()
    {
        return $this->client->send(null, new GeoFeatureCollection());
    }
}