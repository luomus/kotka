<?php

namespace Trimble\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class FeatureServiceFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /** @var \Trimble\Http\Client $client */
        $client = $serviceLocator->get('Trimble\Http\Client');
        return new FeatureService($client);
    }
}
