<?php
return array(
    'service_manager' => array(
        'factories' => array(
            'Trimble\Options\TrimbleConfiguration' => 'Trimble\Options\TrimbleConfigurationFactory',
            'Trimble\Http\Client' => 'Trimble\Http\ClientFactory',
            'Trimble\Service\FeatureService' => 'Trimble\Service\FeatureServiceFactory',
        )
    ),
);