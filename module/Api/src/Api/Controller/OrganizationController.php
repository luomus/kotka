<?php
namespace Api\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\Stdlib\Hydrator\Reflection as Hydrator;
use Zend\View\Model\JsonModel;


class OrganizationController extends AbstractRestfulController
{
    protected $collectionOptions = array('GET');
    protected $resourceOptions = array('GET');
    protected $organizationService;

    public function getList()
    {
        $organizations = $this->getOrganizationService()->getAll();
        $organizations = $organizations->toArray();
        $hydr = new Hydrator();
        foreach ($organizations as $key => $organization) {
            $organizations[$key] = $hydr->extract($organization);
        }

        return new JsonModel($organizations);
    }

    /**
     * @return \Kotka\Service\OrganizationService
     */
    protected function getOrganizationService()
    {
        if ($this->organizationService === null) {
            $this->organizationService = $this->serviceLocator->get('Kotka\Service\OrganizationService');
        }

        return $this->organizationService;
    }

} 