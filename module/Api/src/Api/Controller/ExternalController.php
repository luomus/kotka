<?php

namespace Api\Controller;

use Common\Service\IdService;
use Kotka\Triple\MAPerson;
use User\Enum\User;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;

/**
 * Class ExternalController
 *
 * This is to route external APIs so that they can be accessed locally from javascript
 *
 * @package Api\Controller
 */
class ExternalController extends AbstractActionController
{
    private $taxonSearchUrl;
    private $digitariumImageUrl;

    /**
     * Action for getting image json from kuvat.luomus.fi
     *
     * @return JsonModel
     */
    public function imageAction()
    {
        $id = $this->getEvent()->getRouteMatch()->getParam('id');
        /** @var \Api\Service\ImageService $imageService */
        $imageService = $this->getServiceLocator()->get('Api\Service\ImageService');
        /** @var MAPerson $user */
        $user = $this->getServiceLocator()->get('user');
        return new JsonModel(
            $imageService->getImages(
                $id,
                $user->getMARoleKotka() === User::ROLE_GUEST || $user->getMARoleKotka() === null
            )
        );
    }

    public function coordinatesAction()
    {
        /** @var \Kotka\Service\CoordinateService $coordinateService */
        $coordinateService = $this->getServiceLocator()->get('Kotka\Service\CoordinateService');
        $request = $this->getRequest();
        $lat = $request->getQuery('lat');
        $lon = $request->getQuery('lon');
        $system = $request->getQuery('system');
        return new JsonModel($coordinateService->convertToWgs84($lat, $lon, $system));
    }

    public function taxonSearchAction()
    {
        $id = $this->getEvent()->getRouteMatch()->getParam('id');
        $url = sprintf($this->getTaxonSearchUrl(), rawurlencode($id));
        $data = json_decode(file_get_contents($url), true);
        $result = [];
        if (isset($data['results'])) {
            $has = [];
            if (isset($data['results']['exactMatch'])) {
                foreach ($data['results']['exactMatch'] as $qname => $item) {
                    $item['qname'] = $qname;
                    $has[$qname] = true;
                    $result[] = $item;
                }
            }
            if (isset($data['results']['partialMatches'])) {
                foreach ($data['results']['partialMatches'] as $qname => $item) {
                    if (!isset($item['scientificName'])) {
                        $item = array_pop($item);
                    }
                    if (!isset($has[$qname])) {
                        $item['qname'] = $qname;
                        $result[] = $item;
                        $has[$qname] = true;
                    }
                }
            }
            if (isset($data['results']['likelyMatches'])) {
                foreach ($data['results']['likelyMatches'] as $qname => $item) {
                    if (!isset($item['scientificName'])) {
                        $item = array_pop($item);
                    }
                    if (!isset($has[$qname])) {
                        $item['qname'] = $qname;
                        $result[] = $item;
                    }
                }
            }
        }
        return new JsonModel($result);
    }

    /**
     * Action for getting taxon data
     * @return JsonModel
     */
    public function taxonAction()
    {
        $id = $this->getEvent()->getRouteMatch()->getParam('id');
        $url = sprintf($this->getTaxonSearchUrl(), rawurlencode($id));
        $ctx = stream_context_create(array('http'=>
            array('timeout' => 3)
        ));
        $data = json_decode(file_get_contents($url, false, $ctx), true);
        $view = new JsonModel();
        if (!isset($data['results']) || !isset($data['results']['exactMatch'])) {
            $view->setVariable('result', 'not found');

            return $view;
        }
        $data = $data['results']['exactMatch'];
        if (count($data) > 1) {
            $view->setVariable('result', 'found multiple');

            return $view;
        }
        $qname = key($data);
        $data = $data[$qname];
        $view->setVariable('result', 'found');
        $view->setVariable('scientificName', $data['scientificName']);
        if (!isset($data['taxonRank'])) {
            $view->setVariable('result', 'not found');
            return $view;
        }
        if (isset($data['scientificNameAuthorship'])) {
            $view->setVariable('scientificNameAuthorship', $data['scientificNameAuthorship']);
        }
        $view->setVariable('taxonRank', IdService::getQName($data['taxonRank'], true));
        $view->setVariable('qname', IdService::getQName($qname, true));

        return $view;
    }

    private function getTaxonSearchUrl()
    {
        if ($this->taxonSearchUrl === null) {
            /** @var \Kotka\Options\ExternalsOptions $externals */
            $externals = $this->getServiceLocator()->get('Kotka\Options\ExternalsOptions');
            $triple = $externals->getTriplestoreApi();
            $this->taxonSearchUrl = $triple->getUrl() . '/taxon-search/%s?format=json';
        }
        return $this->taxonSearchUrl;
    }

    private function getDidgitariumImageUrl()
    {
        if ($this->digitariumImageUrl === null) {
            /** @var \Kotka\Options\ExternalsOptions $externals */
            $externals = $this->getServiceLocator()->get('Kotka\Options\ExternalsOptions');
            $digitarium = $externals->getDigitariumApi();
            $this->digitariumImageUrl = $digitarium->getUrl() . '/%s';
        }
        return $this->digitariumImageUrl;
    }

}
