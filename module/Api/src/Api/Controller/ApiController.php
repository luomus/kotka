<?php

namespace Api\Controller;

use Kotka\DataGrid\ElasticGrid;
use Common\Service\IdService;
use Kotka\Service\FormElementService;
use Zend\Http\Response;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;

/**
 * Json api for kotka
 * @package Api\Controller
 */
class ApiController extends AbstractActionController
{
    public function rangeAction()
    {
        /** @var \Kotka\DataGrid\ElasticGrid $searchDataGrid */
        $grid = new ElasticGrid();
        /** @var \Elastic\Client\Search $adapter */
        $search = $this->serviceLocator->get('Elastic\Client\Search');
        $range = $this->getEvent()->getRouteMatch()->getParam('id');
        $grid->prepareGrid(array('identifier' => $range, 'identifierAsRange' => '1'), $search, ['documentQName']);
        $items = [];
        /** @var \Zend\Db\Adapter\Driver\Sqlsrv\Result $itemResult */
        $itemResult = $grid->getDocument();
        foreach ($itemResult as $result) {
            $uri = IdService::getUri($result['documentQName'], true);
            $items[$uri] = null;
        }

        $result = [];
        if ($itemResult->count() === 0) {
            $result['status'] = 'Nothing found with the given range \'' . $range . '\'';
            return new JsonModel($result);
        }
        $result['status'] = 'ok';
        $result['items'] = array_keys($items);
        return new JsonModel($result);
    }

    public function fieldAction()
    {
        $field = $this->getEvent()->getRouteMatch()->getParam('id');
        /** @var \Kotka\Service\FormElementService $elementManager */
        $select = [];
        try {
            $elementManager = $this->getServiceLocator()->get('Kotka\Service\FormElementService');
            $select = $elementManager->getSelect($field);
            if (isset($select[FormElementService::ALIAS_KEY])) {
                unset($select[FormElementService::ALIAS_KEY]);
            }
        } catch (\Exception $e) {
            /** @var Response $response */
            $response = $this->getResponse();
            $response->setStatusCode(400);
        }
        return new JsonModel($select);
    }

}
