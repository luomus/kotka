<?php

namespace Api\Service;

use Common\Service\IdService;
use Kotka\Service\FormElementService;

class ImageService {
    private $imageService;
    private $formElementService;
    private $digitariumUrl;

    public function __construct(\ImageApi\Service\ImageService $imageService, FormElementService $formElementService, $digitariumUrl)
    {
        $this->imageService = $imageService;
        $this->formElementService = $formElementService;
        $this->digitariumUrl = $digitariumUrl;
    }

    public function getImages($id, $skipSecret = true) {
        return $this->imageService->convertMediaArrayToOldApi(
            $this->imageService->getImages(IdService::getUri($id)),
            $id,
            $this->formElementService->getSelect('MZ.intellectualRights', []),
            $skipSecret
        );
    }
}