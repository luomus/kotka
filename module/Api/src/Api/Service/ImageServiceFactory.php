<?php

namespace Api\Service;

use Kotka\Options\ExternalsOptions;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class ImageServiceFactory
 * @package Api\Service
 */
class ImageServiceFactory implements FactoryInterface
{
    /**
     * Creates the service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return ImageService
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /** @var ExternalsOptions $externals */
        $externals = $serviceLocator->get('Kotka\Options\ExternalsOptions');
        $digitarium = $externals->getDigitariumApi();
        $digitariumUrl = $digitarium->getUrl() . '/%s';

        return new ImageService(
            $serviceLocator->get('ImageService'),
            $serviceLocator->get('Kotka\Service\FormElementService'),
            $digitariumUrl
        );
    }
}
