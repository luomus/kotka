<?php

namespace Api;

use Zend\ModuleManager\Feature\ConfigProviderInterface;

/**
 * Initializes ApiModule
 * @package Api
 */
class Module implements ConfigProviderInterface
{
    /**
     * Gets the module config
     * @return array|mixed|\Traversable
     */
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

}
