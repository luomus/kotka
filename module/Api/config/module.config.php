<?php

return array(
    'service_manager' => array(
        'factories' => array(
            'Api\Service\ImageService' => 'Api\Service\ImageServiceFactory',
        )
    ),
    'controllers' => array(
        'invokables' => array(
            'Api\Controller\Api' => 'Api\Controller\ApiController',
            'Api\Controller\Organization' => 'Api\Controller\OrganizationController',
            'Api\Controller\External' => 'Api\Controller\ExternalController',
            'Api\Controller\Mock' => 'Api\Controller\MockController',
        )
    ),
    'router' => array(
        'routes' => array(
            'api' => array(
                'type' => 'segment',
                'constrains' => array(
                    'action' => '[a-zA-Z][a-zA-Z0-9_-]+',
                    'id' => '[a-zA-Z]+\.[0-9a-zA-Z]+',
                ),
                'options' => array(
                    'route' => '/api[/:action[/:id]]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Api\Controller',
                        'controller' => 'Api',
                        'action' => 'index',
                    ),
                ),
            ),
            'ws' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/ws[/:controller[/:id]]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Api\Controller',
                        'controller' => 'Index',
                    ),
                ),
            ),
            'external' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/apiwrapper[/:action[/:id]]',
                    'defaults' => array(
                        'controller' => 'Api\Controller\External',
                        'action' => 'index'
                    )
                )
            ),
            'mock' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/triplestore/uri[/:namespace]',
                    'defaults' => array(
                        'controller' => 'Api\Controller\Mock',
                        'action' => 'idGenerator'
                    )
                )
            )
        ),
    ),
    'view_manager' => array(
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    )
);
