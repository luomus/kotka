<?php
namespace KotkaConsole;

use Zend\Console\Adapter\AdapterInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\ConsoleBannerProviderInterface;
use Zend\ModuleManager\Feature\ConsoleUsageProviderInterface;

/**
 * KotkaConsole is commandline tool for Kotka
 * and can be used from commandline issuing "php index.php command" in the public folder
 *
 * @package KotkaConsole
 */
class Module implements ConsoleBannerProviderInterface, ConsoleUsageProviderInterface, ConfigProviderInterface
{
    /**
     * Gets the config for the module
     * This includes the routing configurations
     *
     * @return array
     */
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    /**
     * Returns a string containing a banner text, that describes the module and/or the application.
     * The banner is shown in the console window, when the user supplies invalid command-line parameters or invokes
     * the application with no parameters.
     *
     * The method is called with active Zend\KotkaConsole\Adapter\AdapterInterface that can be used to directly access KotkaConsole and send
     * output.
     *
     * @param  AdapterInterface $console
     * @return string|null
     */
    public function getConsoleBanner(AdapterInterface $console)
    {
        return
            "==------------------------------------------------------==" . PHP_EOL .
            "           Welcome to Kotka commandline tool              " . PHP_EOL .
            "==------------------------------------------------------==" . PHP_EOL .
            "Version 0.1.0" . PHP_EOL;
    }

    /**
     * Returns an array or a string containing usage information for this module's KotkaConsole commands.
     * The method is called with active Zend\KotkaConsole\Adapter\AdapterInterface that can be used to directly access
     * KotkaConsole and send output.
     *
     * If the result is a string it will be shown directly in the console window.
     * If the result is an array, its contents will be formatted to console window width. The array must
     * have the following format:
     *
     *     return array(
     *                'Usage information line that should be shown as-is',
     *                'Another line of usage info',
     *
     *                '--parameter'        =>   'A short description of that parameter',
     *                '-another-parameter' =>   'A short description of another parameter',
     *                ...
     *            )
     *
     * @param  AdapterInterface $console
     * @return array|string|null
     */
    public function getConsoleUsage(AdapterInterface $console)
    {
        return array(
            'EXPORT',
            'export <command>' => 'exports data from different sources',
            'Available commands',
            array('collection', 'Collection data to excel', "This exports all the collections to file called export_collection.xlsx"),
            array('organization', 'Organization data to excel', "This exports all the organizations to file called export_organization.xlsx"),
            array('hyntikka-to-excel', 'Hyntikka data to excel', "This fetches the hyntikka data from database and exports it to hyntikka.xlsx file in the kotka root"),

            'IMPORT SPECIMEN',
            'import [options] --user= [json|excel|database]:type <source>' => 'imports document',
            array('<username>', 'Username', 'Username of the person marked as edit/creator to the imported documents.'),
            array('<source>', 'Source name', 'Most commonly the name of the file that contains the data.'),
            array('[json|csv|database]', 'Source type', 'Format used for parsing the document data.'),
            'options',
            array('--datatype=', 'Type of the data', "Either zoospecimem, botanyspecimen, accession, culture or palaeontology if omitted zoospecimem will be used"),
            array('--user-from-data', 'User info from data', "Pick username and createdate from the data being imported"),
            array('--allow-dirty=', 'Allow dirty fields', "Comma separated list of field names that are allowed to enter to database even if validation fails. Please run first with --dry-run wo see which fields contain incorrect data."),
            array('--dry-run', 'Dry run', "Runs the importer but doesn't make any changes to databases."),
            array('--existing=[skip|update]', 'With existing', "This tells what to do when encountered an item that already exists in the database."),
            array('--organization=', 'Owning organization', 'Specify organization that owns the imported documents'),
            array('--skip-on-error', 'Skip items with error', "If errors are encountered, continue import and skip the ones with error."),
            array('--halt-on-error', 'Halts on validation error', "If errors are encountered validation is stopped and error is show."),
            array('--error-file=', 'Location to log errors', "Specifies the File where validation errors are logged."),

            'IMPORT OTHER',
            'import [collection|organization]:type <file> --user=' => 'imports other types',
            'options',
            array('--user', 'Username', 'LTKM username to login as.'),
            array('type', 'Type of import', 'Specifies the type of imported model'),
            array('file', 'import filename', 'File to be imported in kotka format!'),

            'GENERATE',
            'generate class' => 'Generates classes based on the ontologycal database',
            'generate map' => 'Generates maps used to normalize data in exports',

            'UPDATE',
            'update <command>' => 'Updates Kotkas data',
            'Available commands',
            array('mustikka', 'Queues all specimens', 'This adds every specimen with resend status to jobQueue. From there the jobQueue picks it up and sends to mustikka (sending everything )'),
            array('name-to-qname', 'Convert name to qname', 'This changes the value of MZ.editor and MZ.owner from full name string to users qname'),
            array('transactions-to-private', 'Transaction to private', 'Marks every transaction as private.'),
            array('hrb-to-hra', 'Move loan specimen data', 'Moves loans specimen data from HRB.specimen to HRA.transaction'),
            array('condition-to-literal', 'condition values from resource to literal', 'converts all the condition values from resource to literal')
        );
    }
}
