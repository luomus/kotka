<?php
return array(
    'console' => array(
        'router' => array(
            'routes' => array(
                'updateDb' => array(
                    'options' => array(
                        'route' => 'update db <collection>',
                        'defaults' => array(
                            'controller' => 'KotkaConsole\Controller\Update',
                            'action' => 'db'
                        )
                    )
                ),
                'update' => array(
                    'options' => array(
                        'route' => 'update <action> [--subject=] [--existing=[skip|update]] [--from=] [--to=]',
                        'defaults' => array(
                            'controller' => 'KotkaConsole\Controller\Update',
                            'action' => 'index'
                        )
                    )
                ),
                'generate' => array(
                    'options' => array(
                        'route' => 'generate <action> [--filter=]',
                        'defaults' => array(
                            'controller' => 'KotkaConsole\Controller\Generate',
                            'action' => 'index'
                        )
                    )
                ),
                'cron' => array(
                    'options' => array(
                        'route' => 'run cron [--send-to-mustikka|-m]  [--send-to-ontology] [--items=]',
                        'defaults' => array(
                            'controller' => 'KotkaConsole\Controller\Cron',
                            'action' => 'run',
                        )
                    )
                ),
                'import' => array(
                    'options' => array(
                        'route' => 'import [--dry-run] [--existing=[skip|update]] [--datatype=[zoospecimen|botanyspecimen]] [--domain=[luomus|tun]] [--allow-dirty=] [--halt-on-error] [--error-file=] [--skip-on-error] [--organization=] --user= [--user-from-data] [--new-id] [--transform=] [json|excel|database]:type <source>',
                        'defaults' => array(
                            'controller' => 'KotkaConsole\Controller\Import',
                            'action' => 'import',
                        )
                    )
                ),
                'export-generic' => array(
                    'options' => array(
                        'route' => 'import2 <action>',
                        'defaults' => array(
                            'controller' => 'KotkaConsole\Controller\Import',
                            'action' => 'index',
                        )
                    )
                ),
                'export' => array(
                    'options' => array(
                        'route' => 'export <action>',
                        'defaults' => array(
                            'controller' => 'KotkaConsole\Controller\Export',
                            'action' => 'index',
                        )
                    )
                ),
                'generic_import' => array(
                    'options' => array(
                        'route' => 'import --user= <action> <file>',
                        'defaults' => array(
                            'controller' => 'KotkaConsole\Controller\Import',
                            'action' => 'index',
                        )
                    )
                ),
                'tool' => array(
                    'options' => array(
                        'route' => 'tool <action> [--subject=]',
                        'defaults' => array(
                            'controller' => 'KotkaConsole\Controller\Tool',
                            'action' => 'index',
                        )
                    )
                )
            )
        )
    ),
    'controllers' => array(
        'invokables' => array(
            'KotkaConsole\Controller\Update' => 'KotkaConsole\Controller\UpdateController',
            'KotkaConsole\Controller\Generate' => 'KotkaConsole\Controller\GenerateController',
            'KotkaConsole\Controller\Cron' => 'KotkaConsole\Controller\CronController',
            'KotkaConsole\Controller\Import' => 'KotkaConsole\Controller\ImportController',
            'KotkaConsole\Controller\Export' => 'KotkaConsole\Controller\ExportController',
            'KotkaConsole\Controller\Tool' => 'KotkaConsole\Controller\ToolController',
        ),
    ),
    'service_manager' => array(
        'invokables' => array(
            'KotkaConsole\Service\GeneratorService' => 'KotkaConsole\Service\GeneratorService',
        ),
        'factories' => array(
            'KotkaConsole\Service\DatabaseService' => 'KotkaConsole\Service\DatabaseServiceFactory',
            'KotkaConsole\Service\Generator' => 'KotkaConsole\Service\GeneratorFactory',
            'KotkaConsole\Service\MapGenerator' => 'KotkaConsole\Service\MapGeneratorFactory',

        )
    ),
);
