<?php

namespace KotkaConsole\Template\Maps;


class MYCollectionID extends Clear
{

    protected $template = array(
        'HR.160' => array('MZH', 'MHZ', 'MZH ?'),
        'HR.101' => array('MZH, Coleoptera Palearctica'),
        'HR.204' => array('Diptera Exotic'),
        'HR.110' => array('MZH Diptera Fennica'),
        'HR.129' => array('H'),
    );

} 