<?php

namespace KotkaConsole\Template\Maps;


class MYLanguage extends Clear
{

    protected $template = array(
        'finnish' => array('fi', 'fin'),
        'english' => array('en', 'eng', 'engs'),
        'swedish' => array('sv', 'swe'),
        'sami' => array('se', 'sme'),
        'russian' => array('ru', 'rus'),
        'estonian' => array('et', 'est'),
        'german' => array('de', 'deu', 'ger', 'deus'),
        'french' => array('fr', 'fra', 'fre', 'fras'),
        'spanish' => array('es', 'spa'),
        'chinese' => array('zh', 'zho', 'chi'),
        'latin' => array('la', 'lat', 'lats'),
    );

} 