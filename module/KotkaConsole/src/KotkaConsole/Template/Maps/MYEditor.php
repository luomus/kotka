<?php

namespace KotkaConsole\Template\Maps;


class MYEditor extends Clear
{

    protected $template = array(
        'Lempiäinen, Anna' => array('HP'),
        'Vilkamaa, Pekka' => array('Vilkamaa, P.', 'Vilkamaa, P', 'Vilkamaaa, Pekka'),
        'Ståhls, Gunilla' => array('Ståhls Gunilla', 'Ståhls-Mäkelä, Gunilla'),
        'Kullberg, Jaakko' => array('Jaakko Kullberg'),
        'Virsula, Emmi' => array('Emmi Virsula'),
    );

} 