<?php

namespace KotkaConsole\Template\Maps;


class MYCoordinateSystem extends Clear
{

    protected $template = array(
        'MY.coordinateSystemEtrs-tm35fin' => array('etrs-tm35fin'),
        'MY.coordinateSystemDd' => array('deg'),
    );

} 