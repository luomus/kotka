<?php

namespace KotkaConsole\Template\Maps;


class MYTypeStatus extends Clear
{

    protected $template = array(
        'MY.typeStatusNo' => array('no', 'not type'),
        'MY.typeStatusType' => array('lectotype/paralectotype', 'doubtful', 'typ'),
        'MY.typeStatusLectotype' => array('lectotypus'),
        'MY.typeStatusParalectotype' => array('paralectotyp'),
        'MY.typeStatusParatype' => array('paratyp'),
    );

} 