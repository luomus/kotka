<?php

namespace KotkaConsole\Template\Maps;


class MYTypeVerification extends Clear
{

    protected $template = array(
        'MY.typeVerificationVerified' => array('yes'),
        'MY.typeVerificationUnverified' => array('a', 'no'),
        'MY.typeVerificationProbable' => array('possible')
    );

} 