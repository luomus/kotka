<?php

namespace KotkaConsole\Template\Maps;

class MYBiologicalProvinceEn extends Clear
{

    protected $template = array(
        // FI full  => FI short, latin plant short, latin, latin insect sort
        // missing if no transformation needed
        'Ahvenanmaa' => array('A', 'Alandia', 'Al'),
        'Varsinais-Suomi' => array('V', 'Ab', 'Regio aboënsis'),
        'Uusimaa' => array('U', 'N', 'Nylandia'),
        'Etelä-Karjala' => array('EK', 'Ka', 'Karelia australis'),
        'Satakunta' => array('St'),
        'Etelä-Häme' => array('EH', 'Ta', 'Tavastia australis'),
        'Etelä-Savo' => array('ES', 'Sa', 'Savonia australis'),
        'Laatokan Karjala' => array('LK', 'Kl', 'Karelia ladogensis'),
        'Etelä-Pohjanmaa' => array('EP', 'Oa', 'Ostrobottnia australis'),
        'Pohjois-Häme' => array('PH', 'Tb', 'Tavastia borealis'),
        'Pohjois-Savo' => array('PS', 'Sb', 'Savonia borealis'),
        'Pohjois-Karjala' => array('PK', 'Kb', 'Karelia borealis'),
        'Keski-Pohjanmaa' => array('KP', 'Om', 'Ostrobottnia media'),
        'Kainuu' => array('Kn', 'Ostrobottnia kajanensis', 'Ok'),
        'Oulun Pohjanmaa' => array('OP', 'Obo', 'Ostrobottnia ouluensis', 'Oba'),
        'Perä-Pohjanmaa' => array('PeP', 'Obu', 'Ostrobottnia ultima', 'Obb'),
        'Koillismaa' => array('Ks', 'Regio kuusamoënsis'),
        'Kittilän Lappi' => array('KiL', 'Lkk', 'Lapponia kittilensis', 'Lkoc'),
        'Sompion Lappi' => array('SoL', 'Lks', 'Lapponia sompiensis', 'Lkor'),
        'Enontekiön Lappi' => array('EnL', 'Le', 'Lapponia enontekiensis'),
        'Inarin Lappi' => array('InL', 'Li', 'Lapponia inarensis'),
        'Aunuksen Karjala' => array('AK', 'Kol', 'Karelia olonetsensis'),
        'Äänisen Karjala' => array('ÄK', 'Kon', 'Karelia onegensis'),
        'Äänisen takainen Karjala' => array('ÄtK', 'Kton', 'Karelia transonegensis'),
        'Läntinen pomoria' => array('LPom', 'Kpoc', 'Karelia pomorica occidentalis'),
        'Itäinen pomoria' => array('IPom', 'Ipoc', 'Karelia pomorica orientalis'),
        'Kemin Lappi' => array('KemL', 'Lkem', 'Lapponia kemensis'),
        'Kieretin Karjala' => array('KerK', 'Kk', 'Karelia keretina'),
        'Imanteron Lappi' => array('ImL', 'Lim', 'Lapponia imandrae'),
        'Varsugan Lappi' => array('VL', 'Lv', 'Lapponia varsugar'),
        'Petsamon Lappi' => array('PsL', 'Lps', 'Lapponia petsamoensis'),
        'Ponoin Lappi' => array('PoL', 'Lp', 'Lapponia ponojensis'),
        'Tuuloman Lappi' => array('TL', 'Lt', 'Lapponia tulomensis'),
        'Muurmannin Lappi' => array('MrL', 'Lmur', 'Lapponia murmanica'),
    );
} 