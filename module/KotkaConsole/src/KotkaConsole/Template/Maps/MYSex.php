<?php

namespace KotkaConsole\Template\Maps;


class MYSex extends Clear
{

    protected $template = array(
        'MY.sexM' => array('m', 'male'),
        'MY.sexF' => array('f', 'female'),
        'MY.sexU' => array('u', 'unknown', 'unknowable'),
        'MY.sexN' => array('n', 'not applicable'),
        'MY.sexX' => array('x', 'mixed'),
        'MY.sexW' => array('w', 'worker'),
    );

} 