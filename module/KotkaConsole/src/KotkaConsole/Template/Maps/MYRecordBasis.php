<?php

namespace KotkaConsole\Template\Maps;


class MYRecordBasis extends Clear
{

    protected $template = array(
        'MY.recordBasisPreservedSpecimen' => array('spe'),
        'MY.recordBasisPreparation' => array('prep'),
    );

} 