<?php
namespace KotkaConsole\Tools;

/**
 * Class ProgressBar
 * @deprecated Use Zend\ProgressBar instead
 * @package KotkaConsole\Tools
 */
class ProgressBar
{

    private $total;
    private $size = 30;

    private static $start_time;

    public function showStatusBar($done)
    {
        if ($done < 0 || $this->total == 0) {
            return;
        }
        if (self::$start_time == null) {
            self::$start_time = time();
        }

        $ratio = $done / $this->total;
        $soFar = floor($this->size * $ratio);
        $rest = $this->size - $soFar;

        $status = "\r[";
        $status .= str_repeat('#', $soFar);
        $status .= str_repeat('-', $rest);
        $status .= "] (done $done/{$this->total})";

        $elapsed = time() - self::$start_time;
        $perItem = $elapsed / ($done == 0 ? 1 : $done);
        if ($elapsed > 10) {
            $remain = intval(($this->total - $done) * 1.5 * $perItem);
            if ($remain > 300) {
                $remain = intval($remain / 60) . ' min.';
            } else {
                $remain = $remain . ' sec.';
            }
            $status .= " remain ($remain)";
        }

        echo "$status   ";
        flush();

    }

    public function setTotal($total)
    {
        if (is_array($total)) {
            $total = count($total);
        } else if ($total instanceof \Countable) {
            $total = $total->count();
        }
        $this->total = (int)$total;
        self::$start_time = null;
    }

    public function getTotal()
    {
        return $this->total;
    }

    public function setSize($size)
    {
        $this->size = (int)$size;
    }

} 