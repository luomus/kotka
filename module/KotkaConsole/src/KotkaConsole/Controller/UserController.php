<?php

namespace KotkaConsole\Controller;


use User\Authentication\Adapter\Ltkm;
use Zend\EventManager\EventManager;
use Zend\Mvc\Controller\AbstractActionController;

class UserController extends AbstractActionController
{

    protected function login($username, $organization)
    {
        /** @var \Zend\Authentication\AuthenticationService $auth */
        $auth = $this->getServiceLocator()->get('auth');
        /** @var Ltkm $authAdapter */
        $authAdapter = $auth->getAdapter();
        $authAdapter->_skipLdapAuthentication($organization);
        $authAdapter->setIdentity($username);
        $result = $auth->authenticate();
        $isValid = $result->isValid();
        if ($isValid) {
            $_SERVER['REMOTE_ADDR'] = $username . '@' . gethostname();
            $even = new EventManager('login');
            $even->trigger('success', null, array('username' => $username));
        }

        return $isValid;
    }

} 