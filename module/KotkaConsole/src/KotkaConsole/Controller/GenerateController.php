<?php

namespace KotkaConsole\Controller;

use Kotka\PrograssBar\DummyProgressBar;
use Kotka\Service\ExcelService;
use Common\Service\IdService;
use Kotka\Service\ImportService;
use Kotka\Triple\MYDocument;
use Triplestore\Service\ObjectManager;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\ProgressBar\Adapter\Console;
use Zend\ProgressBar\ProgressBar;
use Zend\View\Model\ConsoleModel;

/**
 * Class GenerateController is for generating class files from the ontology database
 * This is invoked from commandline
 *
 * @package KotkaConsole\Controller
 */
class GenerateController extends AbstractActionController
{
    public function classAction()
    {
        /** @var \KotkaConsole\Service\GeneratorService $generator */
        $generator = $this->serviceLocator->get('KotkaConsole\Service\GeneratorService');

        $progressBar = new DummyProgressBar();
        $generator->getEventManager()->attach('generate-start', function ($e) use (&$progressBar) {
            $console = new Console();
            $console->setElements([Console::ELEMENT_PERCENT, Console::ELEMENT_BAR, Console::ELEMENT_TEXT]);
            $progressBar = new ProgressBar($console, 0, $e->getParams()['total']);
        });
        $generator->getEventManager()->attach('generate', function ($e) use (&$progressBar) {
            $params = $e->getParams();
            $progressBar->update($params['spot'], $params['class']);
        });
        $generator->getEventManager()->attach('generate-finnish', function ($e) use (&$progressBar) {
            $progressBar->update(null, '');
            $progressBar->finish();
        });
        $generator->generateClasses();
        $viewModel = new ConsoleModel();
        $viewModel->setResult("Done\n");
        return $viewModel;
    }

    public function mapAction()
    {
        /** @var \KotkaConsole\Service\MapGenerator $generator */
        $generator = $this->serviceLocator->get('KotkaConsole\Service\MapGenerator');
        $fields = [
            'MY.editor',
            'MY.typeVerification',
            'MY.sex',
            'MY.biologicalProvince',
            'MY.collectionID',
            'MY.recordBasis',
            'MY.collectionID',
            'MY.typeStatus',
            'MY.coordinateSystem'
        ];
        foreach ($fields as $field) {
            $generator->generateMap($field);
        }

        return "DONE" . PHP_EOL;
    }

    public function missingExcelAction()
    {
        /** @var ObjectManager $objectManager */
        $objectManager = $this->getServiceLocator()->get('Triplestore\ObjectManager');
        /** @var ImportService $importService */
        $importService = $this->getServiceLocator()->get('Kotka\Service\ImportService');
        $dir = new \DirectoryIterator(ImportService::IMPORT_DIR);
        $files = array();
        foreach ($dir as $fileinfo) {
            if (!$fileinfo->isDot()) {
                $files[$fileinfo->getMTime()][] = $fileinfo->getFilename();
            }
        }
        ksort($files);
        $files = call_user_func_array('array_merge', $files);
        $files = array_reverse($files);
        $progressBar = new ProgressBar(new Console(), 0, count($files));

        $missing = [];
        $subjects = [];
        foreach ($files as $file) {
            $progressBar->next();
            $full = ImportService::IMPORT_DIR . $file;
            try {
                $data = $importService->Excel2Array($full, 0);
            } catch (\Exception $e) {
                echo "\nPHPExcel failed to read the file '$file'\n";
                continue;
            }

            if ($data === false) {
                echo "\nImport error on '$file' ("
                    . $importService->getErrorCode() . "): "
                    . $importService->getErrorCause() . " \n";
                continue;
            }
            if (empty($data)) {
                echo "\nFile '$file' didn't have any data\n";
                continue;
            }
            $noticeMissingId = true;
            $noticeMissingSpecimen = true;
            foreach ($data as $key => $row) {
                if ($key == ImportService::RESET_KEY) {
                    continue;
                }
                if (!isset($row['MYNamespaceID']) ||
                    !isset($row['MYObjectID']) ||
                    $row['MYNamespaceID'] == '' ||
                    $row['MYObjectID'] == ''
                ) {
                    if ($noticeMissingId) {
                        $noticeMissingId = false;
                        echo "\n'$file' has missing ids\n";
                    }
                    continue;
                }
                $subject = IdService::getQName($row['MYNamespaceID'] . '.' . $row['MYObjectID'], true);
                if (isset($subjects[$subject])) {
                    continue;
                }
                $model = $objectManager->fetch($subject, false);
                if ($model !== null) {
                    continue;
                }
                if ($noticeMissingSpecimen) {
                    $noticeMissingSpecimen = false;
                    echo "\n'$file' has specimens missing from Kotka\n";
                }
                $row['MYURL'] = $file;
                $missing[] = $row;
                $subjects[$subject] = true;
            }
        }
        $progressBar->finish();
        if (empty($missing)) {
            echo "Done and no missing items where found!\n";
            exit();
        }
        echo "Generating missing excel file\n";
        /** @var ExcelService $excelService */
        $excelService = $this->getServiceLocator()->get('Kotka\Service\ExcelService');
        /** @var \Kotka\Service\FormGeneratorService $formGenerator */
        $formGenerator = $this->serviceLocator->get('Kotka\Service\FormGeneratorService');
        $excelService->setForm($formGenerator->getForm(new MYDocument()));
        $excelService->generateExcelFromArray($missing, 'missing.xlsx', false, true);
        echo "Done\n";
    }
}
