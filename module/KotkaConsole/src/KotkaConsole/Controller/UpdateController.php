<?php

namespace KotkaConsole\Controller;

use Elastic\Client\Index;
use Elastic\Extract\Autocomplete;
use Elastic\Extract\ExtractInterface;
use Elastic\Query\Query;
use ImageApi\Model\Media;
use ImageApi\Model\PictureMeta;
use ImageApi\Service\ImageService;
use Kotka\ElasticExtract\BioGeoGraphicalProvince;
use Kotka\ElasticExtract\BioGeoGraphicalProvinceJson;
use Kotka\ElasticExtract\Branch;
use Kotka\ElasticExtract\Municipality;
use Kotka\ElasticExtract\Natura;
use Kotka\ElasticExtract\Region;
use Kotka\ElasticExtract\Sample;
use Kotka\ElasticExtract\Specimen;
use Kotka\ElasticExtract\Specimen as SpecimenExtract;
use Kotka\Job\Mustikka;
use Kotka\Job\Specimen as SpecimenJob;
use Kotka\Service\AccessionService;
use Common\Service\IdService;
use Kotka\Service\SpecimenService;
use Kotka\Triple\KotkaEntityInterface;
use Kotka\Triple\MYDocument;
use Kotka\Triple\MYGathering;
use Kotka\Triple\MYMeasurement;
use SlmQueue\Queue\QueueInterface;
use Triplestore\Classes\MYDocumentInterface;
use Triplestore\Classes\MYGatheringInterface;
use Triplestore\Classes\MYIdentificationInterface;
use Triplestore\Classes\MYUnitInterface;
use Triplestore\Db\Adapter\Adapter;
use Triplestore\Db\Oracle;
use Triplestore\Model\Model;
use Triplestore\Model\Object;
use Triplestore\Model\Predicate;
use Triplestore\Service\ObjectManager;
use Zend\Dom\Document;
use Zend\Form\Element\DateTime;
use Zend\ProgressBar\Adapter\Console;
use Zend\ProgressBar\ProgressBar;
use Zend\Stdlib\RequestInterface as Request;
use Zend\Stdlib\ResponseInterface as Response;
use Zend\Http\Client;

/**
 * Class UpdateController is for handling all the needed updates
 * This is invoked from commandline
 *
 * @package KotkaConsole\Controller
 */
class UpdateController extends UserController
{
    const BATCH_SIZE = 50;
    const MAX_RETRY = 5;
    const ALLOWED_KEY = '__allowed__';
    const DEFAULT_KEY = '__default__';
    const PREFIX_KEY = '__prefix__';
    const CONTAIN = '__contains__';
    const NO_VALUE = '__no-value__';

    /** @var  \Triplestore\Service\ObjectManager */
    protected $om;
    /** @var  \Kotka\Service\CoordinateService */
    protected $coordinateService;

    protected $defaultQueue;

    protected static $buffer = [];

    /** @var  \Common\Service\IdService */
    protected $idGenerator;

    public function dispatch(Request $request, Response $response = null)
    {
        $this->idGenerator = $this->getServiceLocator()->get('Common\Service\IdService');
        $this->om = $this->getServiceLocator()->get('Triplestore\ObjectManager');
        parent::dispatch($request, $response);
    }

    public function checkEnvAction()
    {
        echo KOTKA_ENV . "\n";
    }

    public function transactionsToPrivateAction()
    {
        /** @var ObjectManager $om */
        $om = $this->getServiceLocator()->get('Triplestore\ObjectManager');
        $om->disableHydrator();
        $models = $om->findBy(array(ObjectManager::PREDICATE_TYPE => 'HRA.transaction'), null, null, null, array(ObjectManager::PREDICATE_TYPE));
        $cnt = 0;
        $adapter = new Console();
        $progress = new ProgressBar($adapter, 0, count($models));
        foreach ($models as $subject => $model) {
            $predicate = $om->getPredicate($subject, KotkaEntityInterface::RESTRICTION_PREDICATE);
            if ($predicate === null) {
                $predicate = new Predicate(KotkaEntityInterface::RESTRICTION_PREDICATE);
            }
            $predicate->addObject(new Object(KotkaEntityInterface::RESTRICTION_PRIVATE));
            $om->updatePredicate($subject, $predicate);
            $progress->update(++$cnt);
        }
        $progress->finish();
    }

    public function conditionToLiteralAction()
    {
        $om = $this->getServiceLocator()->get('Triplestore\ObjectManager');
        $om->disableHydrator();
        $models = $om->findBy(array('MY.condition' => 'MY.conditionBad'), null, null, null, array('MY.condition'));
        $cnt = 0;
        $adapter = new Console();
        $progress = new ProgressBar($adapter, 0, count($models));
        foreach ($models as $subject => $model) {
            $progress->update(++$cnt);
            /** @var Model $model */
            $predicate = new Predicate('MY.condition');
            $predicate->addLiteralValue('bad');
            $result = $om->updatePredicate($subject, $predicate);
            if ($result === false) {
                echo "\tFailed to update $subject" . PHP_EOL;
            }
        }
        $progress->finish();
    }

    /**
     * run in production 2015-05-07 19:22
     */
    public function datasetToResourceAction()
    {
        /** @var ObjectManager $om */
        $om = $this->getServiceLocator()->get('Triplestore\ObjectManager');
        /** @var Oracle $connection */
        $connection = $om->getConnection();
        $dbName = $connection->getDatabase();
        $sql = "select DISTINCT SUBJECTNAME from $dbName.RDF_STATEMENTVIEW where PREDICATENAME = 'MY.datasetID'";
        $results = $connection->executeSelectSql($sql);
        $adapter = new Console();
        $progress = new ProgressBar($adapter, 0, count($results));
        $om->disableHydrator();
        foreach ($results as $result) {
            $progress->next();
            /** @var Model $model */
            $model = $om->fetch($result['SUBJECTNAME'], false);
            $subject = $model->getSubject();
            $predicate = $model->getOrCreatePredicate('MY.datasetID');
            $values = $predicate->getLiteralValue();
            if (empty($values)) {
                continue;
            }
            $predicate->setLiteralValue([]);
            $predicate->setResourceValue($values);
            $result = $om->updatePredicate($subject, $predicate);
            if ($result === false) {
                echo "\tFailed to update $subject" . PHP_EOL;
            }
        }
        $progress->finish();
    }

    public function mustikkaAction()
    {
        $subjects = $this->params('subject', null);
        if ($subjects !== null) {
            $subjects = explode(',', $subjects);
            foreach ($subjects as $subject) {
                $this->addToQueue($subject, true);
            }
            return;
        }
        /** @var Adapter $adapter */
        $adapter = $this->serviceLocator->get('Triplestore\Db\Adapter\Array');
        $db = $adapter->getDbName();
        echo "All specimens...\n";
        $results = $adapter->query("Select subjectname from $db.RDF_STATEMENTVIEW where OBJECTNAME = 'MY.document' AND PREDICATENAME = 'rdf:type'")->execute();
        foreach ($results as $data) {
            echo '.';
            $this->addToQueue($data['subject']);
        }
        $this->addToQueue(null, true);
    }

    /**
     * Run in production at ?
     * Run in production at 22.1.2014 klo 18.06
     */
    public function addHttpToImagesAction()
    {
        /** @var ObjectManager $om */
        $om = $this->getServiceLocator()->get('Triplestore\ObjectManager');
        /** @var Oracle $connection */
        $connection = $om->getConnection();
        $dbName = $connection->getDatabase();
        $sql = "select
                  image.SUBJECTNAME as subject,
                  documentID.RESOURCELITERAL as documentURI,
                  source.OBJECTNAME
                from $dbName.RDF_STATEMENTVIEW image
                join $dbName.RDF_STATEMENTVIEW documentID on image.SUBJECTNAME = documentID.SUBJECTNAME and documentID.PREDICATENAME ='MM.documentURI'
                join $dbName.RDF_STATEMENTVIEW source on image.SUBJECTNAME = source.SUBJECTNAME and source.PREDICATENAME ='MM.sourceSystem'
                where image.PREDICATENAME = 'rdf:type'
                  and image.OBJECTNAME = 'MM.image'
                  AND source.OBJECTNAME = 'KE.3'
                  AND substr(documentID.RESOURCELITERAL,1,4) != 'http'";

        $results = $connection->executeSelectSql($sql);
        $adapter = new Console();
        $progress = new ProgressBar($adapter, 0, count($results));
        foreach ($results as $result) {
            $progress->next();
            if (strpos($result['DOCUMENTURI'], 'http') !== false) {
                continue;
            }
            $predicate = new Predicate('MM.documentURI');
            $predicate->addLiteralValue(IdService::getUri($result['DOCUMENTURI']));
            $om->updatePredicate($result['SUBJECT'], $predicate);
        }
        $progress->finish();
    }

    /**
     * Run in production at 8.12.2016 klo 7.00 - 10.22
     */
    public function addCreatorToSpecimensAction()
    {
        /** @var ObjectManager $om */
        $om = $this->getServiceLocator()->get('Triplestore\ObjectManager');
        /** @var Oracle $connection */
        $connection = $om->getConnection();
        $dbName = $connection->getDatabase();
        $sql = "SELECT
                    documents.SUBJECTNAME AS subject,
                    creator.OBJECTNAME AS creator,
                    editor.OBJECTNAME AS editor
                  FROM $dbName.RDF_STATEMENTVIEW documents
                    JOIN $dbName.RDF_STATEMENTVIEW createddates ON (documents.SUBJECTNAME = createddates.SUBJECTNAME AND createddates.PREDICATENAME = 'MZ.dateCreated')
                    LEFT JOIN $dbName.RDF_STATEMENTVIEW creator ON (documents.SUBJECTNAME = creator.SUBJECTNAME AND creator.PREDICATENAME = 'MZ.creator')
                    LEFT JOIN $dbName.RDF_STATEMENTVIEW editor ON (documents.SUBJECTNAME = editor.SUBJECTNAME AND editor.PREDICATENAME = 'MZ.editor')
                  WHERE documents.PREDICATENAME = 'rdf:type'
                    AND documents.OBJECTNAME = 'MY.document'
                    AND creator.OBJECTNAME is null
                    AND editor.OBJECTNAME is not null
                    AND documents.SUBJECTNAME not like 'luomus:HLA%'
                    AND documents.CREATED > TO_DATE('2016-12-06', 'yyyy-mm-dd')";
        $results = $connection->executeSelectSql($sql);
        $adapter = new Console();
        echo 'Total of: ' . count($results) . "\n";
        $progress = new ProgressBar($adapter, 0, count($results));
        foreach ($results as $result) {
            $progress->next();
            $subject = $result['SUBJECT'];
            $editor = $result['EDITOR'];
            $sql = "select qname.OBJECTNAME AS qname
                    from $dbName.RDF_STATEMENTVIEW_HISTORY qname
                    where qname.SUBJECTNAME = :subject
                        and qname.PREDICATENAME = 'MZ.editor'
                        and qname.OBJECTNAME like 'MA.%'
                        and ROWNUM = 1
                    ORDER BY qname.CREATED";
            $editors = $connection->executeSelectSql($sql, ['subject' => $subject]);
            $candidate = array_shift($editors);
            if ($candidate) {
                $editor = $candidate['QNAME'];
            }
            if (empty($editor)) {
                echo "\nMissing editor on $subject \n";
            }
            //echo $subject . ' ' . $editor . "\n";
            $predicate = new Predicate('MZ.creator');
            $predicate->addResourceValue($editor);
            $om->updatePredicate($subject, $predicate);
        }
        $progress->finish();
    }

    public function addDigitariumDatasetToVonBonsdorfCollectionAction()
    {
        /** @var ObjectManager $om */
        $om = $this->getServiceLocator()->get('Triplestore\ObjectManager');
        /** @var Oracle $connection */
        $connection = $om->getConnection();
        $dbName = $connection->getDatabase();
        $sql = "SELECT
                  SUBJECTNAME
                FROM $dbName.RDF_STATEMENTVIEW
                WHERE PREDICATENAME = 'MY.collectionID' AND OBJECTNAME = 'HR.407' and SUBJECTNAME NOT IN (
                  SELECT
                    DISTINCT SUBJECTNAME
                  FROM $dbName.RDF_STATEMENTVIEW
                  WHERE PREDICATENAME = 'MY.projectId'
                    AND RESOURCELITERAL = 'GX.270'
                )";
        $specimens = $connection->executeSelectSql($sql);
        $adapter = new Console();
        $progress = new ProgressBar($adapter, 0, count($specimens));
        foreach ($specimens as $specimen) {
            $progress->next();
            $subject = $specimen['SUBJECTNAME'];
            $predicate = new Predicate('MY.projectId');
            $predicate->addLiteralValue('GX.270');
            $om->updatePredicate($subject, $predicate);
            $this->addToQueue($subject);
        }
        $this->addToQueue(null, true);
        $progress->finish();
    }

    public function measurementsAction() {
        /** @var ObjectManager $objectManager */
        $objectManager = $this->getServiceLocator()->get('Triplestore\ObjectManager');
        /** @var SpecimenService $specimenService */
        $specimenService = $this->getServiceLocator()->get('Kotka\Service\SpecimenService');
        /** @var Oracle $connection */
        $connection = $objectManager->getConnection();
        $dbName = $connection->getDatabase();
        $sql = "SELECT * FROM $dbName.RDF_STATEMENTVIEW where PREDICATENAME = 'MY.measurementOld' order by STATEMENTID";
        $results = $connection->executeSelectSql($sql);
        $map = [
            // 'wingMm' => 'WingMillimeters'
        ];

        $measurements = [];
        foreach ($results as $result) {
            $subject = $result['SUBJECTNAME'];
            $value = explode(':', $result['RESOURCELITERAL']);
            if ($value[1] === '') {
                continue;
            }
            if (!isset($measurements[$subject])) {
                $measurements[$subject] = new MYMeasurement();
            }
            if (isset($map[$value[0]])) {
                $value[0] = $map[$value[0]];
            }
            /** @var MYMeasurement $measurement */
            $measurement = $measurements[$subject];
            $getMethod = 'getMY' . ucfirst($value[0]);
            $setMethod = 'setMY' . ucfirst($value[0]);
            if (!method_exists($measurement, $setMethod)) {
                echo "CANNOT STORE MEASUREMENT " . $result['RESOURCELITERAL'] . ' ' . $setMethod . "\n";
                continue;
            }
            $current = $measurement->$getMethod();
            if (is_array($current)) {
                array_push($current, $value[1]);
                $measurement->$setMethod($current);
            } else {
                $measurement->$setMethod([$value[1]]);
            }
        }
        $sql = "select OBJECTNAME from $dbName.RDF_STATEMENTVIEW where PREDICATENAME = 'MY.isPartOf' and SUBJECTNAME in (
                  select OBJECTNAME from $dbName.RDF_STATEMENTVIEW where PREDICATENAME = 'MY.isPartOf' and SUBJECTNAME in (
                    select SUBJECTNAME from $dbName.RDF_STATEMENTVIEW where PREDICATENAME = 'MY.measurementOld'
                  )
                )";

        $results = $connection->executeSelectSql($sql);
        foreach ($results as $result) {
            echo ".";
            $subject = $result['OBJECTNAME'];
            $document = $specimenService->getById($subject);
            /** @var MYGathering[] $gatherings */
            $gatherings = $document->getMYGathering();
            foreach ($gatherings as $gathering) {
                /** @var MYUnitInterface[] $units */
                $units = $gathering->getMYUnit();
                foreach ($units as $unit) {
                    $unitSubject = $unit->getSubject();
                    $unit->setMYMeasurementOld([]);
                    if (isset($measurements[$unitSubject])) {
                        $unit->setMYMeasurement($measurements[$unitSubject]);
                    }
                }
            }
            $specimenService->update($document, true);
        }
    }

    public function clearCoordinatesAction()
    {
        /** @var ObjectManager $objectManager */
        $objectManager = $this->getServiceLocator()->get('Triplestore\ObjectManager');
        /** @var SpecimenService $specimenService */
        $specimenService = $this->getServiceLocator()->get('Kotka\Service\SpecimenService');
        /** @var Oracle $connection */
        $connection = $objectManager->getConnection();
        $dbName = $connection->getDatabase();
        $sql = "SELECT COUNT(SUBJECTNAME) FROM $dbName.RDF_STATEMENTVIEW WHERE PREDICATENAME = 'MY.longitude'";
        $results = $connection->executeSelectSql($sql);
        $cnt = $results[0][0];
        $connection->setUseResultSet(true);
        $sql = "SELECT SUBJECTNAME FROM $dbName.RDF_STATEMENTVIEW WHERE PREDICATENAME = 'MY.longitude'";
        $results = $connection->executeSelectSql($sql);
        $adapter = new Console();
        $progress = new ProgressBar($adapter, 0, $cnt);
        foreach ($results as $result) {
            $progress->next();
            $subject = $result['SUBJECTNAME'];
            //$subject = 'MY.226403';
            $gathering = $objectManager->fetch($subject, false);
            if (!$gathering instanceof MYGathering || empty($gathering->getMYIsPartOf())) {
                continue;
            }
            $this->clearCoordinates($gathering, $objectManager, $specimenService);
        }
        $progress->finish();
    }

    private function clearCoordinates(MYGathering $gathering, ObjectManager $om, SpecimenService $specimenService, $retry = 0)
    {
        try {
            if (!$specimenService->setGatheringWgsCoordinate($gathering)) {
                return;
            }
            $model = $gathering->getModelCopy();
            $om->updatePredicate($model->getSubject(), $model->getPredicate(SpecimenJob::WGS_84_LAT_PREDICATE));
            $om->updatePredicate($model->getSubject(), $model->getPredicate(SpecimenJob::WGS_84_LON_PREDICATE));
            $this->addToQueue($gathering->getMYIsPartOf(), true);
        } catch(\Exception $e) {
            echo "\n error {$e->getMessage()}\n";
            $retry++;
            if ($retry < 2) {
                sleep(30);
                $this->clearCoordinates($gathering, $om, $specimenService, $retry);
            } else {
                echo "\n MAX retry reached {$e->getMessage()}\n";
            }
        }
    }

    private function addToQueue($subject, $flush = false)
    {
        if (!empty($subject)) {
            self::$buffer[] = $subject;
        }
        $cnt = count(self::$buffer);
        if (($flush || $cnt >= self::BATCH_SIZE) && $cnt > 0) {
            $queue = $this->getDefaultQueue();
            $job = new Mustikka();
            $job->setSubjects(self::$buffer);
            $queue->push($job);
            self::$buffer = [];
        }
    }

    /**
     * @return QueueInterface
     */
    private function getDefaultQueue()
    {
        if ($this->defaultQueue === null) {
            $this->defaultQueue = $this->getServiceLocator()->get('SlmQueue\Queue\QueuePluginManager')->get('default');
        }

        return $this->defaultQueue;
    }

    /**
     * run in production at 2015-05-07 17:19-17:20/
     */
    public function fixTimesAction()
    {
        /** @var ObjectManager $objectManager */
        $objectManager = $this->getServiceLocator()->get('Triplestore\ObjectManager');
        /** @var SpecimenService $specimenService */
        $specimenService = $this->getServiceLocator()->get('Kotka\Service\SpecimenService');
        /** @var Oracle $connection */
        $connection = $objectManager->getConnection();
        $connection->setUseResultSet(true);
        $sql = "SELECT SUBJECTNAME AS \"subject\", RESOURCELITERAL AS \"date\" FROM LTKM_LUONTO.RDF_STATEMENTVIEW WHERE PREDICATENAME = 'MM.captureDateTime' AND subjectname IN (
                    SELECT subjectname
                    FROM  LTKM_LUONTO.RDF_STATEMENTVIEW
                    WHERE predicatename = 'MM.sourceSystem' AND objectname = 'KE.3'
                )";
        $results = $connection->executeSelectSql($sql);
        foreach ($results as $result) {
            $subject = $result['subject'];
            $date = new \DateTime($result['date']);
            $timestamp = substr($date->format('Uu'), 0, -3);
            $date->setTimestamp($timestamp);
            if (new \DateTime() < $date) {
                continue;
            }
            $correct = $date->format("Y-m-d\TH:i:s.000");
            $predicate = new Predicate('MM.captureDateTime');
            $predicate->setLiteralValue($correct);
            echo $subject . ': ' . $result['date'] . " -> " . $correct . "\n";
            $objectManager->updatePredicate($subject, $predicate);
        }
    }
    /**
     * run in production at 2015-05-07 17:19-17:20/
     */
    public function fixAction()
    {
        /** @var ObjectManager $objectManager */
        $objectManager = $this->getServiceLocator()->get('Triplestore\ObjectManager');
        /** @var SpecimenService $specimenService */
        $specimenService = $this->getServiceLocator()->get('Kotka\Service\SpecimenService');
        /** @var Oracle $connection */
        $connection = $objectManager->getConnection();
        $connection->setUseResultSet(true);
        $sql = "select a.SUBJECTNAME, b.OBJECTNAME, a.RESOURCELITERAL from LTKM_LUONTO.RDF_STATEMENTVIEW a
                  left join LTKM_LUONTO.RDF_STATEMENTVIEW b on a.SUBJECTNAME = b.SUBJECTNAME and b.PREDICATENAME = 'MY.isPartOf' and b.OBJECTNAME like 'luomus:C.%'
                  where a.PREDICATENAME = 'MY.higherGeography' and b.OBJECTNAME is not null";
        $results = $connection->executeSelectSql($sql);
        foreach ($results as $result) {
            $subject = $result['OBJECTNAME'];

            /** @var Predicate $predicate */
            $predicate = $connection->getPredicate($subject, 'MY.documentLocation');
            if ($predicate === null || empty($predicate->getLiteralValue())) {
                $predicate = new Predicate('MY.documentLocation');
                $predicate->setLiteralValue($result['RESOURCELITERAL']);
                echo '.';
                $objectManager->updatePredicate($subject, $predicate);
                continue;
            }
            echo "\ndocumentLocation already exists" . $subject . "\n";
        }
    }

    public function branchCollectionsAction() {
        /** @var \Kotka\Service\AccessionService $accessionService */
        $accessionService = $this->getServiceLocator()->get('Kotka\Service\AccessionService');

        /** @var \Elastic\Client\Index $indexer */
        $indexer = $this->getServiceLocator()->get('Elastic\Client\Index');
        /** @var \Elastic\Client\Search $search */
        $search = $this->serviceLocator->get('Elastic\Client\Search');

        $extractor = new Branch();
        $extractor->setServiceLocator($this->getServiceLocator());

        $collections = [
            'HR.2350',
            'HR.2367',
            'HR.2370',
            'HR.2432',
            'HR.2433',
            'HR.2457'
        ];

        $colMap = [
            'HR.2350' => 290,
            'HR.2367' => 111,
            'HR.2370' => 114,
            'HR.2432' => 190,
            'HR.2433' => 198,
            'HR.2457' => 510
        ];
        
        $oldMap = [
            'P.38597' => 'HR.2350',
            'P.38643' => 'HR.2350',
            'P.38649' => 'HR.2350',
            'P.38791' => 'HR.2350',
            'P.37531' => 'HR.2350',
            'P.38885' => 'HR.2350',
            'P.21369' => 'HR.2367',
            'P.31982' => 'HR.2367',
            'P.30953' => 'HR.2367',
            'P.36090' => 'HR.2367',
            'P.21346' => 'HR.2367',
            'P.23460' => 'HR.2367',
            'P.24337' => 'HR.2367',
            'P.11218' => 'HR.2370',
            'P.31691' => 'HR.2370',
            'P.11036' => 'HR.2370',
            'P.29011' => 'HR.2432',
            'P.38472' => 'HR.2432',
            'P.38565' => 'HR.2432',
            'P.38594' => 'HR.2432',
            'P.11411' => 'HR.2433',
        ];

        foreach ($collections as $collection) {
            $query = new Query(Branch::getIndexName(), Branch::getTypeName());
            $queryStr = 'collectionID: "' . $collection . '"';
            $query->setQueryString($queryStr);
            $query->setFields(['documentQName', 'collectionID']);
            $query->setSize(10000);
            $results = $search->query($query)->execute();
            foreach ($results as $result) {
                if ($result['collectionID'] !== $collection) {
                    continue;
                }
                /** @var \Kotka\Triple\PUUBranch $branch */
                $branch = $accessionService->getBranchById($result['documentQName']);
                if ($branch->getPUUCollectionID() !== $collection) {
                    $notes = $branch->getPUUNotes();
                    if (preg_match('/HR\./', $notes)) {
                        $sub = $branch->getSubject();
                        if (!isset($oldMap[$sub])) {
                            continue;
                        }
                        $oldCol = $oldMap[$sub];
                        $newCol = $colMap[$oldCol];
                        $branch->setPUUNotes(str_replace('HR.2650', $newCol, $branch->getPUUNotes()));
                    } else {
                        continue;
                    }
                }
                // var_dump($branch);
                $accessionService->storeBranch($branch);
                echo '.';
            }
        }

        echo "DONE\n";
    }

    public function atlantisPicsAction() {
        /** @var \Kotka\Service\ImportService $importService */
        $importService = $this->getServiceLocator()->get('Kotka\Service\ImportService');
        /** @var \Kotka\Service\SpecimenService $specimenService */
        $specimenService = $this->getServiceLocator()->get('Kotka\Service\SpecimenService');
        /** @var \Elastic\Client\Index $indexer */
        $indexer = $this->getServiceLocator()->get('Elastic\Client\Index');
        /** @var \Elastic\Client\Search $search */
        $search = $this->serviceLocator->get('Elastic\Client\Search');
        /** @var Adapter $adapter */
        $adapter = $this->serviceLocator->get('Triplestore\Db\Adapter\Array');
        /** @var ObjectManager $om */
        $om = $this->serviceLocator->get('Triplestore\ObjectManager');
        $db = $adapter->getDbName();
        $extractor = new Specimen();

        $extractor->setServiceLocator($this->getServiceLocator());
        echo "Initializing...\n";
        //$extractor->init();

        $console = new Console();
        $console->setElements(array(Console::ELEMENT_PERCENT, Console::ELEMENT_BAR, Console::ELEMENT_ETA, Console::ELEMENT_TEXT));

        $importService->setMaxRows(1000000);
        $data = $importService->Excel2Array('./Atlantis_kuvien_metatiedot_MM_AK.xls');
        if ($data === false) {
            var_dump($importService->getErrorCode());
            var_dump($importService->getErrorCause());
            exit();
        }
        $metadata = [];

        echo "Parsing data...\n";

        foreach ($data as $row) {
            if (empty($row['Capturers'])) {
                continue;
            }
            $metadata[] = [
                'id' => $row['imgid'],
                'original' => $row['OriginalID'],
                'capturers' => $row['Capturers']
            ];
        }


        $progressBar = new ProgressBar($console, 0, count($metadata));
        $cache = [];

        if (!file_exists('cache.json')) {
            echo "Finding original specimen id documents\n";
            foreach ($metadata as $meta) {
                $progressBar->next();
                $query = new Query(SpecimenExtract::getIndexName(), SpecimenExtract::getTypeName());
                $queryStr = 'originalSpecimenID.raw: "' . $meta['original'] . '" AND datatype: "accession" AND firstInDocument: true';
                $query->setQueryString($queryStr);
                $query->setFields(['documentQName']);
                $result = $search->query($query)->execute();
                if ($result->count() === 1) {
                    foreach ($result as $doc) {
                        $cache[$meta['original']] = $doc['documentQName'];
                    }
                } else if ($result->count() === 0) {
                    var_dump(['No document found for ', $meta, $queryStr]);
                } else {
                    var_dump(['Too many hits for ', $meta]);
                }
            }
            file_put_contents('cache.json', json_encode($cache));
        }

        $cache = json_decode(file_get_contents('cache.json'), true);

        /** @var ImageService $imageService */
        $imageService = $this->getServiceLocator()->get('ImageService');

        echo "\nUpdating image metadata\n\n";
        $errors = [];
        $progressBar->update(0);
        foreach ($metadata as $meta) {
            $progressBar->next();
            $doc = IdService::getUri($cache[$meta['original']]);
            if (empty($doc)) {
                var_dump(['No linked document found', $meta]);
                continue;
            }
            $images = $imageService->getImages($doc, false);
            if (empty($images)) {
                var_dump(['no images found for', $doc, $meta]);
                if (!isset($errors[$doc])) {
                    $errors[$doc] = [];
                }
                $errors[$doc][] = $meta['original'] .  ': '  .$meta['id'];
                continue;
            }
            $got = false;
            foreach ($images as $image) {
                if (!$image instanceof Media) {
                    continue;
                }
                $file = $image->getMeta()->getOriginalFilename();
                $imgid = array_pop(explode('_', str_replace('.jpg', '', $file)));
                if ($imgid === $meta['id']) {
                    $got = true;
                    if (!empty($image->getMeta()->getCapturers())) {
                        continue;
                    }
                    $image->getMeta()->setCapturers([$meta['capturers']]);
                    $imageService->updateMeta($image->getId(), $image->getMeta());
                }
            }
            if (!$got) {
                var_dump(['No metadata found for', $doc, $meta]);
                if (!isset($errors[$doc])) {
                    $errors[$doc] = [];
                }
                $errors[$doc][] = $meta['original'] .  ': '  .$meta['id'];
            }
        }
        // file_put_contents('errors.json', json_encode($errors));
        echo "done";
    }

    public function indexMissingAction()
    {
        /** @var \Elastic\Client\Index $indexer */
        $indexer = $this->getServiceLocator()->get('Elastic\Client\Index');
        /** @var \Elastic\Client\Search $search */
        $search = $this->serviceLocator->get('Elastic\Client\Search');
        /** @var Adapter $adapter */
        $adapter = $this->serviceLocator->get('Triplestore\Db\Adapter\Array');
        /** @var ObjectManager $om */
        $om = $this->serviceLocator->get('Triplestore\ObjectManager');
        $db = $adapter->getDbName();
        $extractor = new Specimen();
        echo "Querying for all specimens... to check if they exist in index and to add missing to '{$extractor->getIndexName()}' index\n";
        $results = $adapter->query("Select subjectname from $db.RDF_STATEMENTVIEW where PREDICATENAME = 'rdf:type' AND OBJECTNAME = 'MY.document'")->execute();

        $extractor->setServiceLocator($this->getServiceLocator());
        echo "Initializing...\n";
        $extractor->init();

        $console = new Console();
        $console->setElements(array(
            Console::ELEMENT_PERCENT,
            Console::ELEMENT_BAR,
            Console::ELEMENT_ETA,
            Console::ELEMENT_TEXT));
        $progressBar = new ProgressBar($console, 0, $om->count(array('rdf:type' => 'MY.document')));

        foreach ($results as $result) {
            $progressBar->next();
            if (!isset($result['subject'])) {
                var_dump($result);
                echo "DIDN'T GET SUBJECT\n";
                return;
            }
            $uri = $result['subject'];
            $query = new Query(SpecimenExtract::getIndexName(), SpecimenExtract::getTypeName());
            $queryStr = 'documentQName: ("' . IdService::getQName($uri) . '")';
            $query->setQueryString($queryStr);
            $query->setFields(['documentQName']);
            $result = $search->query($query)->execute();
            if ($result->count() === 0) {
                echo "\nAdding: $uri \n";
                $indexer->indexResults([$result], $extractor, 1);
            }
        }
        echo "Done\n";
    }

    public function reindexAllAction()
    {
        /** @var \Elastic\Client\Index $indexer */
        $indexer = $this->getServiceLocator()->get('Elastic\Client\Index');
        /** @var Adapter $adapter */
        $adapter = $this->serviceLocator->get('Triplestore\Db\Adapter\Array');
        /** @var ObjectManager $om */
        $om = $this->serviceLocator->get('Triplestore\ObjectManager');
        $db = $adapter->getDbName();
        $extractor = new Specimen();
        echo "Querying for all specimens... to be added to '{$extractor->getIndexName()}' index\n";
        //$results = $adapter->query("Select subjectname from $db.RDF_STATEMENTVIEW where PREDICATENAME = 'MY.datatype' and RESOURCELITERAL = 'accession'")->execute();
        $results = $adapter->query("Select subjectname from $db.RDF_STATEMENTVIEW where PREDICATENAME = 'rdf:type' AND OBJECTNAME = 'MY.document'")->execute();
        //$results = $adapter->query("'luomus:JA.66174'")->execute();
        //$results = $adapter->query("'tun:GV.303'")->execute();
        //$results = $adapter->query("'HT.12466'")->execute(); // this is accession

        $extractor->setServiceLocator($this->getServiceLocator());
        echo "Initializing...\n";
        $extractor->init();

        $console = new Console();
        $console->setElements(array(
            Console::ELEMENT_PERCENT,
            Console::ELEMENT_BAR,
            Console::ELEMENT_ETA,
            Console::ELEMENT_TEXT));
        $progressBar = new ProgressBar($console, 0, $om->count(array('rdf:type' => 'MY.document')));

        $indexer->getEventManager()->attach('indexed', function ($e) use ($progressBar) {
            $params = $e->getParams();
            if (isset($params['finnish'])) {
                $progressBar->finish();
            } else {
                $progressBar->next(1, '');
            }
        });
        $indexer->getEventManager()->attach('elastic', function ($e) use ($progressBar) {
            $progressBar->update(null, $e->getParams()['action']);
        });

        //$indexer->delete(Autocomplete::getIndexName(), Autocomplete::getTypeName(new Specimen()));
        //$indexer->delete(Specimen::getIndexName(), Specimen::getTypeName());
        //$indexer->delete(Sample::getIndexName(), Sample::getTypeName());
        $indexer->indexResults($results, $extractor, 10000);
        echo "Done\n";
    }

    public function reindexAllBranchesAction() {
        /** @var \Elastic\Client\Index $indexer */
        $indexer = $this->getServiceLocator()->get('Elastic\Client\Index');
        /** @var Adapter $adapter */
        $adapter = $this->serviceLocator->get('Triplestore\Db\Adapter\Array');
        /** @var ObjectManager $om */
        $om = $this->serviceLocator->get('Triplestore\ObjectManager');
        $db = $adapter->getDbName();
        $extractor = new Branch();
        echo "Querying for all branches... to be added to '{$extractor->getIndexName()}' index\n";
        $results = $adapter->query("Select subjectname from $db.RDF_STATEMENTVIEW where PREDICATENAME = 'rdf:type' AND OBJECTNAME = 'PUU.branch'")->execute();

        $extractor->setServiceLocator($this->getServiceLocator());
        echo "Initializing...\n";
        $extractor->init();

        $console = new Console();
        $console->setElements(array(
            Console::ELEMENT_PERCENT,
            Console::ELEMENT_BAR,
            Console::ELEMENT_ETA,
            Console::ELEMENT_TEXT));
        $progressBar = new ProgressBar($console, 0, $om->count(array('rdf:type' => 'PUU.branch')));

        $indexer->getEventManager()->attach('indexed', function ($e) use ($progressBar) {
            $params = $e->getParams();
            if (isset($params['finnish'])) {
                $progressBar->finish();
            } else {
                $progressBar->next(1, '');
            }
        });
        $indexer->getEventManager()->attach('elastic', function ($e) use ($progressBar) {
            $progressBar->update(null, $e->getParams()['action']);
        });

        //$indexer->delete(Autocomplete::getIndexName(), Autocomplete::getTypeName(new Branch()));
        $indexer->delete(Branch::getIndexName(), Branch::getTypeName());
        $indexer->indexResults($results, $extractor, 1000);
        echo "Done\n";
    }

    public function reindexAllSampleAction() {
        /** @var \Elastic\Client\Index $indexer */
        $indexer = $this->getServiceLocator()->get('Elastic\Client\Index');
        /** @var Adapter $adapter */
        $adapter = $this->serviceLocator->get('Triplestore\Db\Adapter\Array');
        /** @var ObjectManager $om */
        $om = $this->serviceLocator->get('Triplestore\ObjectManager');
        $db = $adapter->getDbName();
        $extractor = new Specimen();
        echo "Querying for all specimens... to be added to '{$extractor->getIndexName()}' index\n";
        $results = $adapter->query("select SUBJECTNAME from $db.rdf_statementview where PREDICATENAME = 'rdf:type' and SUBJECTNAME in (
              select OBJECTNAME from $db.rdf_statementview where PREDICATENAME = 'MY.isPartOf'and SUBJECTNAME in (
                select OBJECTNAME from $db.rdf_statementview where PREDICATENAME = 'MY.isPartOf' and SUBJECTNAME in (
                  select OBJECTNAME from $db.rdf_statementview where PREDICATENAME = 'MY.isPartOf' and SUBJECTNAME in (
                    Select SUBJECTNAME from $db.RDF_STATEMENTVIEW where PREDICATENAME = 'rdf:type' AND OBJECTNAME = 'MF.sample'
                  )
                )
              )
        )")->execute();

        $extractor->setServiceLocator($this->getServiceLocator());
        echo "Initializing...\n";
        $extractor->init();

        $console = new Console();
        $console->setElements(array(
            Console::ELEMENT_PERCENT,
            Console::ELEMENT_BAR,
            Console::ELEMENT_ETA,
            Console::ELEMENT_TEXT));
        $progressBar = new ProgressBar($console, 0,100);

        $indexer->getEventManager()->attach('indexed', function ($e) use ($progressBar) {
            $params = $e->getParams();
            if (isset($params['finnish'])) {
                $progressBar->finish();
            } else {
                $progressBar->next(1, '');
            }
        });
        $indexer->getEventManager()->attach('elastic', function ($e) use ($progressBar) {
            $progressBar->update(null, $e->getParams()['action']);
        });

        //$indexer->delete(Autocomplete::getIndexName(), Autocomplete::getTypeName(new Specimen()));
        $indexer->delete(Sample::getIndexName(), Sample::getTypeName());
        $indexer->indexResults($results, $extractor, 10000);
        echo "Done\n";
    }


    public function insertBranchesAction()
    {
        $accessionService = new AccessionService();
        $accessionService->setServiceLocator($this->getServiceLocator());
        /** @var \Kotka\Service\ImportService $importServce */
        $importService = $this->getServiceLocator()->get('Kotka\Service\ImportService');
        /** @var \Kotka\Service\SpecimenService $specimenService */
        $specimenService = $this->getServiceLocator()->get('Kotka\Service\SpecimenService');
        $data = $importService->Excel2Array('./branch.xls');
        /** @var \Triplestore\Service\ObjectManager $om */
        $om = $this->getServiceLocator()->get('Triplestore\ObjectManager');
        $events = [];
        foreach ($data as $branch) {
            $root = str_replace('-', '', $branch['Event']);
            $events[$root][] = $branch['Data'];
        }
        foreach ($events as $root => $event) {
            $subject = 'JAAA.' . $root;
            $document = $specimenService->getById($subject);
            if (!$document instanceof MYDocument) {
                echo "Skipping $root \n";
                continue;
            }
            $document->setMYEvent($event);
            $branches = $accessionService->getAllBranchesByRoot($subject, false);
            $all = $branches->getBranches();
            foreach ($all as $branch) {
                $om->remove($branch);
            }
            $specimenService->update($document);
        }
        $om->commit();
    }

    public function idRangeAction()
    {
        $offset = 2000;
        $ns = 'KL';
        $start = 2001;
        $till = 2441;
        for ($i = $start; $i <= $till; $i++) {
            $from = $ns . '.' . $i;
            $to = $ns . '.' . ($i + $offset);
            $this->idAction($from, $to);
        }
        echo "All done!\n";
    }

    public function IdAction($from = null, $to = null)
    {
        $from = $this->params('from', $from);
        $to = $this->params('to', $to);
        if ($from === null || $to === null) {
            echo "Required parameters 'from' and 'to'\n";
            return;
        }
        $fromUri = IdService::getUri($from);
        $toUri = IdService::getUri($to);
        $toQName = str_replace(IdService::DEFAULT_DB_QNAME_PREFIX, '', IdService::getQName($to));
        /** @var ObjectManager $om */
        $om = $this->getServiceLocator()->get('Triplestore\ObjectManager');
        /** @var Oracle $connection */
        $connection = $om->getConnection();
        $dbName = $connection->getDatabase();
        $sql = "SELECT
                  RESOURCEID
                FROM $dbName.RDF_RESOURCE
                WHERE RESOURCEURI= :uriname";
        $connection->setUseResultSet(false);
        if (empty($connection->executeSelectSql($sql, ['uriname' => $fromUri]))) {
            echo "Could not find id: '$from'\n";
            return;
        }
        if (!empty($connection->executeSelectSql($sql, ['uriname' => $toUri]))) {
            echo "There is already resource with id: '$to'\n";
            return;
        }
        $connection->startTransaction();
        $sql = "UPDATE $dbName.RDF_RESOURCE
                SET RESOURCEURI = :toURI, RESOURCENAME = :toQNname
                WHERE RESOURCEURI= :fromUri";
        $connection->setUseResultSet(true);
        $connection->executeSelectSql($sql, ['toURI' => $toUri, 'toQNname' => $toQName, 'fromUri' => $fromUri]);
        $this->addToQueue($toQName, true);
        $this->deleteSpecimenReferences($fromUri);
        echo "Done: $from => $to\n";
    }

    private function deleteSpecimenReferences($uri)
    {
        /** @var \Elastic\Client\Index $indexer */
        $indexer = $this->getServiceLocator()->get('Elastic\Client\Index');
        $delete = new Query(SpecimenExtract::getIndexName(), SpecimenExtract::getTypeName());
        $queryStr = 'documentQName: ("' . IdService::getQName($uri) . '")';
        $delete->setQueryString($queryStr);
        try {
            $indexer->deleteByQuery($delete);
        } catch (\Exception $e) {

        }
        /** @var \Kotka\Service\MustikkaService $mustikkaService */
        //$mustikkaService = $this->getServiceLocator()->get('Kotka\Service\MustikkaService');
        //$mustikkaService->deleteSpecimen($uri);
    }

    function deleteAcquisitionDateAction()
    {
        /** @var \Elastic\Client\Search $search */
        $search = $this->serviceLocator->get('Elastic\Client\Search');
        $query = new Query(SpecimenExtract::getIndexName(), SpecimenExtract::getTypeName());
        $queryStr = '(collectionID: "HR.46" AND acquisitionDate: [1999-01-01 TO 2015-12-31]) OR (collectionID: "HR.647" AND acquisitionDate: [2000-01-01 TO 2016-12-31])';
        $query->setQueryString($queryStr);
        $query->setSize(1000000);
        $query->setFields(['documentQName']);
        $results = $search->query($query)->execute();
        /** @var ObjectManager $om */
        $om = $this->getServiceLocator()->get('Triplestore\ObjectManager');
        foreach ($results as $result) {
            echo '.';
            var_dump($result);
            $pred = new Predicate('MY.acquisitionDate');
            $pred->setResourceValue('');
            $om->updatePredicate(IdService::getQName($result['documentQName'], true), $pred);
        }

    }

    public function addDatasetAction()
    {
        /** @var Adapter $adapter */
        $adapter = $this->getServiceLocator()->get('Triplestore\Db\Adapter\Array');
        /** @var ObjectManager $om */
        $om = $this->getServiceLocator()->get('Triplestore\ObjectManager');
        /** @var Oracle $connection */
        $connection = $om->getConnection();
        $sql = "SELECT DISTINCT subjectname FROM LTKM_LUONTO.RDF_STATEMENTVIEW
                WHERE SUBJECTNAME like 'luomus:HLA.%'";
        $results = $adapter->query($sql)->execute();
        /** @var ImageService $imageService */
        foreach ($results as $row) {
            if (!isset($row['subject']) || strpos($row['subject'], 'luomus:HLA.') !== 0) {
                echo "skipping {$row['subject']}\n";
                continue;
            }
            if (!isset($row['MY.datasetID'])) {
                $row['MY.datasetID'] = [];
            }
            if (!in_array('GX.195', $row['MY.datasetID'])) {
                array_push($row['MY.datasetID'], 'GX.195');
                $predicate = new Predicate('MY.datasetID');
                $predicate->setResourceValue($row['MY.datasetID']);
                $connection->updatePredicate($row['subject'], $predicate);
            }
        }
        echo "All done!\n";
    }

    public function delImagesAction()
    {
        /** @var ObjectManager $om */
        $om = $this->getServiceLocator()->get('Triplestore\ObjectManager');
        /** @var Oracle $connection */
        $connection = $om->getConnection();
        // Find dublicates with
        // select b.RESOURCELITERAL, c.RESOURCELITERAL, count(1) as cnt from RDF_STATEMENTVIEW  a
        //  LEFT JOIN RDF_STATEMENTVIEW b on a.SUBJECTNAME = b.SUBJECTNAME and b.PREDICATENAME = 'MM.originalFilename'
        //  LEFT JOIN RDF_STATEMENTVIEW c on a.SUBJECTNAME = c.SUBJECTNAME and c.PREDICATENAME = 'MM.documentURI'
        //  where
        //      a.PREDICATENAME = 'rdf:type' AND
        //      a.OBJECTNAME = 'MM.image' AND
        //      c.RESOURCELITERAL like 'http%' AND
        //      c.CREATED > to_date('2018-04-03', 'YYYY-MM-DD')
        //GROUP BY b.RESOURCELITERAL, c.RESOURCELITERAL HAVING COUNT(1) > 1;
        $sql = "SELECT * FROM LTKM_LUONTO.RDF_STATEMENTVIEW
                WHERE PREDICATENAME = 'MM.capturerVerbatim' AND RESOURCELITERAL = 'Pekka Malinen'";
        $connection->setUseResultSet(true);
        $result = $connection->executeSelectSql($sql);
        /** @var ImageService $imageService */
        $imageService = $this->getServiceLocator()->get('ImageService');
        foreach ($result as $row) {
            $subject = $row['SUBJECTNAME'];
            if ($imageService->removeImage($subject, false)) {
                echo "$subject OK\n";
            } else {
                echo "Failed to delete $subject\n";
            }
        }
        echo "All done!\n";
    }

    public function resendDeletedAction()
    {
        /** @var ObjectManager $om */
        $om = $this->getServiceLocator()->get('Triplestore\ObjectManager');
        /** @var \Kotka\Service\MustikkaService $mustikkaService */
        $mustikkaService = $this->getServiceLocator()->get('Kotka\Service\MustikkaService');

        /** @var Oracle $connection */
        $connection = $om->getConnection();
        $dbName = $connection->getDatabase();
        $sql = "select SUBJECTNAME from $dbName.RDF_STATEMENTVIEW_HISTORY where PREDICATENAME = 'rdf:type' and OBJECTNAME = 'MY.document'
              and SUBJECTNAME not in (
                select SUBJECTNAME from $dbName.RDF_STATEMENTVIEW where PREDICATENAME = 'rdf:type' and OBJECTNAME = 'MY.document'
            ) and DELETED > to_date('2016-12', 'YYYY-MM')";
        $results = $connection->executeSelectSql($sql);
        $adapter = new Console();
        $progress = new ProgressBar($adapter, 0, count($results));
        $om->disableHydrator();
        foreach ($results as $result) {
            $progress->next();
            if (!$mustikkaService->deleteSpecimen($result['SUBJECTNAME'])) {
                echo "\n\nFailed to delete ".  $result['SUBJECTNAME'] . "\n";
            }
        }
        $progress->finish();
    }

    public function originalSpecimenIDAction()
    {
        /** @var Adapter $adapter */
        $adapter = $this->serviceLocator->get('Triplestore\Db\Adapter\Model');
        /** @var ObjectManager $om */
        $om = $this->serviceLocator->get('Triplestore\ObjectManager');
        $db = $adapter->getDbName();
        echo "Querying for all specimens...\n";
        $results = $adapter->query("Select subjectname from $db.RDF_STATEMENTVIEW where PREDICATENAME = 'rdf:type' AND OBJECTNAME = 'MY.document'")->execute();
        //$results = $adapter->query("'HT.5562'")->execute();
        //$results = $adapter->query("'tun:GV.303'")->execute();
        //$results = $adapter->query("'HT.12466'")->execute(); // this is accession

        $console = new Console();
        $console->setElements(array(
            Console::ELEMENT_PERCENT,
            Console::ELEMENT_BAR,
            Console::ELEMENT_ETA,
            Console::ELEMENT_TEXT));
        $progressBar = new ProgressBar($console, 0, $om->count(array('rdf:type' => 'MY.document')));

        foreach ($results as $document) {
            /** @var $document MYDocumentInterface */
            $subject = $document->getSubject();
            $originalID = $document->getMYOriginalSpecimenID();
            if (!empty($originalID)) {
                $progressBar->next(1, $subject . ' has originalID! Skipping');
                continue;
            }
            $additionalIDs = $document->getMYAdditionalIDs();
            if (strpos($subject, 'HA.') === 0) {
                $originalID = str_replace('HA.', '', $subject);
            } elseif (!empty($additionalIDs)) {
                foreach ($additionalIDs as $id) {
                    if (strpos($id, 'htunniste:') === 0) {
                        $originalID = trim(str_replace('htunniste:', '', $id));
                    }
                }
            }
            if (!empty($originalID)) {
                $progressBar->next(1, 'Adding: ' . $subject);
                $predicate = new Predicate('MY.originalSpecimenID');
                $predicate->addLiteralValue($originalID);
                $om->updatePredicate($subject, $predicate);
            } else {
                $progressBar->next();
            }
        }

        echo "Done\n";
    }

    public function kmlLocationsAction() {
        /** @var \Elastic\Client\Index $indexer */
        $indexer = $this->getServiceLocator()->get('Elastic\Client\Index');
        $file = $this->params('from', null);
        if ($file === null || !file_exists($file)) {
            throw new \Exception('Could not find file ' . $file);
        }
        $document = new Document(str_replace(
            '<kml xmlns="http://earth.google.com/kml/2.0">',
            '<kml>',
            file_get_contents($file)
        ), Document::DOC_XML);
        $query = new Document\Query();
        $results = $query->execute('//Placemark', $document, Document\Query::TYPE_XPATH);
        $bioGeo = new BioGeoGraphicalProvince();
        $bioGeo->setServiceLocator($this->getServiceLocator());
        $bioGeo->init();
        $this->internalLocateIndexer($bioGeo, $results, $indexer);
    }

    public function maakunnatLocationsAction() {

        // NOTE:
        // Need to connect to production db since area codes are only there!

        /** @var \Elastic\Client\Index $indexer */
        $indexer = $this->getServiceLocator()->get('Elastic\Client\Index');
        $file = $this->params('from', null);
        if ($file === null || !file_exists($file)) {
            throw new \Exception('Could not find file ' . $file);
        }
        $data = json_decode(file_get_contents($file), true);
        if (!isset($data['features']) || !is_array($data['features'])) {
            throw new \Exception("Given json didn't contain geoJsons features");
        }
        $bioGeoJson = new BioGeoGraphicalProvinceJson();
        $bioGeoJson->setServiceLocator($this->getServiceLocator());
        $bioGeoJson->init();
        //$indexer->delete(Natura::getIndexName(), Natura::getTypeName());
        $this->internalLocateIndexer($bioGeoJson, $data['features'], $indexer);
    }


    public function naturaLocationsAction() {
        /** @var \Elastic\Client\Index $indexer */
        $indexer = $this->getServiceLocator()->get('Elastic\Client\Index');
        $file = $this->params('from', null);
        if ($file === null || !file_exists($file)) {
            throw new \Exception('Could not find file ' . $file);
        }
        $data = json_decode(file_get_contents($file), true);
        if (!isset($data['features']) || !is_array($data['features'])) {
            throw new \Exception("Given json didn't contain geoJsons features");
        }
        $natura = new Natura();
        $natura->setServiceLocator($this->getServiceLocator());
        //$indexer->delete(Natura::getIndexName(), Natura::getTypeName());
        $this->internalLocateIndexer($natura, $data['features'], $indexer);
    }

    public function locationsAction()
    {
        /** @var \Elastic\Client\Index $indexer */
        $indexer = $this->getServiceLocator()->get('Elastic\Client\Index');
        //$file = $this->params('from', 'SuomenKuntajako_2017_1000k.xml');
        //$file = $this->params('from', 'SuomenKuntajako_2017_10k.xml');
        $file = $this->params('from', 'SuomenKuntajako_2017_250k.xml');
        if (!file_exists($file)) {
            throw new \Exception('Could not find file ' . $file);
        }
        $document = new Document(file_get_contents($file), Document::DOC_XML);
        $query = new Document\Query();
        $results = $query->execute('gml:featureMember', $document, Document\Query::TYPE_CSS);

        $municipality = new Municipality();
        $region = new Region();
        $municipality->setServiceLocator($this->getServiceLocator());
        $region->setServiceLocator($this->getServiceLocator());
        $municipality->init();

        // Need to use commandline to delete only certain types or change this to use delete by query
        // $indexer->delete(Municipality::getIndexName(), Municipality::getTypeName());
        // $indexer->delete(Region::getIndexName(), Region::getTypeName());

        echo "Updating Municipalities\n";
        $this->internalLocateIndexer($municipality, $results, $indexer);

        //echo "Updating regions\n";
        //$this->internalLocateIndexer($region, $results, $indexer);

        echo "Done\n";
    }

    private function internalLocateIndexer(ExtractInterface $extractor, $results, Index $indexer)
    {
        echo "Adding coordinates to '{$extractor->getIndexName()}/{$extractor::getTypeName()}' index\n";
        $console = new Console();
        $console->setElements(array(
            Console::ELEMENT_PERCENT,
            Console::ELEMENT_BAR,
            Console::ELEMENT_ETA,
            Console::ELEMENT_TEXT));
        $progressBar = new ProgressBar($console, 0, count($results));

        $indexer->getEventManager()->attach('indexed', function ($e) use ($progressBar) {
            $params = $e->getParams();
            if (isset($params['finnish'])) {
                $progressBar->finish();
            } else {
                $progressBar->next(1, '');
            }
        });
        $indexer->getEventManager()->attach('elastic', function ($e) use ($progressBar) {
            $progressBar->update(null, $e->getParams()['action']);
        });

        $indexer->indexResults($results, $extractor, 1);

        $indexer->getEventManager()->clearListeners('indexed');
        $indexer->getEventManager()->clearListeners('elastic');
    }

    public function checkExistingAction()
    {
        $file = $this->params('from');
        if (!file_exists($file)) {
            throw new \Exception('Could not find file ' . $file);
        }
        /** @var \Kotka\Service\ImportService $importService */
        $importService = $this->getServiceLocator()->get('Kotka\Service\ImportService');
        $importService->setMaxRows(100000);
        /** @var \Kotka\Transform\Lutikka $lutikka */
        $lutikka = $this->getServiceLocator()->get('Kotka\Transform\Lutikka');
        $data = $importService->Excel2Array($file);
        if ($data === false) {
            var_dump($importService->getErrorCause());
            var_dump($importService->getErrorCode());
        }
        /** @var \TripleStore\Service\ObjectManager $om */
        $om = $this->getServiceLocator()->get('TripleStore\ObjectManager');
        $om->disableHydrator();
        $progress = new ProgressBar(new Console(), 0, count($data));
        $missing = '';
        foreach ($data as $id) {
            $progress->next();
            $oid = $id['No'];
            if (!$om->fetch('luomus:HLA.' . $oid, false)) {
                if (!$om->fetch('luomus:HLB.' . $oid, false)) {
                    $missing .= $oid . "\n";
                }
            }
        }
        $progress->finish();
        file_put_contents('./missing_diario.txt', $missing);
    }

    public function removeDuplicatesAction()
    {
        $file = $this->params('from');
        if (!file_exists($file)) {
            throw new \Exception('Could not find file ' . $file);
        }
        /** @var \Kotka\Service\ImportService $importService */
        $importService = $this->getServiceLocator()->get('Kotka\Service\ImportService');
        $importService->setMaxRows(100000);
        /** @var \Kotka\Transform\Lutikka $lutikka */
        $lutikka = $this->getServiceLocator()->get('Kotka\Transform\Lutikka');
        $data = $importService->Excel2Array($file);
        if ($data === false) {
            var_dump($importService->getErrorCause());
            var_dump($importService->getErrorCode());
        }
        $dateTimes = [];
        $remove = [];
        foreach ($data as $key => &$document) {
            $document['subject'] = $document['MYNamespaceID'] . '.' . $document['MYObjectID'];
            if (!isset($document['MZDateEdited'])) {
                echo "skipping" . $document['subject'] . "\n";
                continue;
            }
            $current = new DateTime($document['MZDateEdited']);

            if (isset($dateTimes[$document['subject']])) {
                if ($dateTimes[$document['subject']]['time'] > $current) {
                    $remove[] = $key;
                } else {
                    $remove[] = $dateTimes[$document['subject']]['key'];
                    $dateTimes[$document['subject']] = [
                        'time' => $current,
                        'key' => $key,
                    ];
                }
            } else {
                $dateTimes[$document['subject']] = [
                    'time' => $current,
                    'key' => $key,
                ];
            }
            $document = $lutikka->transform($document);
        }
        $documents = array_diff_key($data, array_flip($remove));
        /** @var \Kotka\Service\ExcelService $excelService */
        $excelService = $this->serviceLocator->get('Kotka\Service\ExcelService');
        $filename = 'updated-nests.xlsx';
        $form = $this->getSpecimenForm();
        $form->add([
            'name' => 'MZDateCreated',
            'type' => 'text',
        ]);
        $form->add([
            'name' => 'MZCreator',
            'type' => 'text',
        ]);
        $excelService->setForm($form);
        $success = $excelService->generateExcelFromArray($documents, $filename, true, true);
        if (!$success) {
            echo "File creation failed!";
        } else {
            echo "All done";
        }
        echo "\n";
    }

    public function deleteByExcelAction()
    {
        $file = $this->params('from');
        if (!file_exists($file)) {
            throw new \Exception('Could not find file ' . $file);
        }
        /** @var \Elastic\Client\Index $indexer */
        $indexer = $this->getServiceLocator()->get('Elastic\Client\Index');
        /** @var \Kotka\Service\ImportService $importService */
        $importService = $this->getServiceLocator()->get('Kotka\Service\ImportService');
        $importService->setMaxRows(100000);
        $excelData = $importService->Excel2Array($file);
        if ($excelData === false) {
            var_dump($importService->getErrorCause());
            var_dump($importService->getErrorCode());
            exit();
        }
        /** @var ObjectManager $om */
        $om = $this->getServiceLocator()->get('TripleStore\ObjectManager');
        $om->enableHydrator();
        /** @var \Kotka\Service\MustikkaService $mustikkaService */
        $mustikkaService = $this->getServiceLocator()->get('Kotka\Service\MustikkaService');

        $progress = new ProgressBar(new Console(), 0, count($excelData));
        foreach ($excelData as $key => $data) {
            $progress->next();
            if (empty($data['MYNamespaceID'])) {
                continue;
            }
            $data['subject'] = $data['MYNamespaceID'] . '.' . $data['MYObjectID'];
            if (strpos($data['subject'], ':') === false) {
                $data['subject'] = 'luomus:' . $data['subject'];
            }
            $document = $om->fetch($data['subject'], false);
            if (!$document instanceof MYDocumentInterface) {
                echo "Couldn't find {$data['subject']}\n";
                continue;
            }
            // Delete from triplestore
            $om->remove($document);
            $om->commit();

            // Delete from elastic
            $query = new Query(SpecimenExtract::getIndexName(), SpecimenExtract::getTypeName());
            $queryStr = 'documentQName: ("' . IdService::getQName($document->getSubject()) . '")';
            $query->setQueryString($queryStr);
            try {
                $indexer->deleteByQuery($query);
            } catch (\Exception $e) {
            }

            // Delete from Mustikka
            $mustikkaService->deleteSpecimen(IdService::getUri($document->getSubject()));
        }
        $progress->finish();
    }

    public function fixImportErrorsAction()
    {
        $file = $this->params('from');
        if (!file_exists($file)) {
            throw new \Exception('Could not find file ' . $file);
        }
        /** @var \Elastic\Client\Index $indexer */
        $indexer = $this->getServiceLocator()->get('Elastic\Client\Index');
        /** @var \Kotka\Service\ImportService $importService */
        $importService = $this->getServiceLocator()->get('Kotka\Service\ImportService');
        $importService->setMaxRows(100000);
        $excelData = $importService->Excel2Array($file);
        if ($excelData === false) {
            var_dump($importService->getErrorCause());
            var_dump($importService->getErrorCode());
            exit();
        }
        /** @var ObjectManager $om */
        $om = $this->getServiceLocator()->get('TripleStore\ObjectManager');
        $om->enableHydrator();
        /** @var \Kotka\Service\MustikkaService $mustikkaService */
        $mustikkaService = $this->getServiceLocator()->get('Kotka\Service\MustikkaService');

        $progress = new ProgressBar(new Console(), 0, count($excelData));
        foreach ($excelData as $key => $data) {
            $progress->next();
            if (empty($data['MYNamespaceID'])) {
                continue;
            }
            $data['subject'] = $data['MYNamespaceID'] . '.' . $data['MYObjectID'];
            if (strpos($data['subject'], ':') === false) {
                $data['subject'] = 'luomus:' . $data['subject'];
            }
            $document = $om->fetch($data['subject']);
            if (!$document instanceof MYDocumentInterface) {
                echo "Couldn't find {$data['subject']}\n";
                continue;
            }

            if ($document->getMYStatus() !== 'MY.statusDeaccessioned') {
                $document->setMYStatus('MY.statusDeaccessioned');

                $om->save($document);
            }

            /*
            foreach ($document->getMYGathering() as $gathering) {
                /** @var MYGatheringInterface $gathering *
                $units = $gathering->getMYUnit();
                if (count($units) > 1) {
                    echo "Document {$data['subject']} has more than 1 unit!!!\n";
                    exit();
                }
                foreach ($units as $unit) {
                    /** @var MYUnitInterface $unit *
                    $identifications = $unit->getMYIdentification();
                    $idx = -1;
                    foreach ($identifications as $key => $identification) {
                        /** @var MYIdentificationInterface $identification *
                        if ($identification->getMYTaxonVerbatim() === $data['OLD_TaxonVerbatim']) {
                            $idx = $key;
                            break;
                        }
                    }
                    if ($idx === -1) {
                        continue;
                    }
                    $iData = $data['MYGathering'][0]['MYUnit'][0]['MYIdentification'][0];
                    $identification = $identifications[$idx];
                    $identification->setMYTaxonVerbatim($iData['MYTaxonVerbatim']);
                    $identification->setMYTaxonRank(empty($iData['MYTaxonRank']) ? '' : 'MX.' . lcfirst($iData['MYTaxonRank']));
                    $identification->setMYTaxon($iData['MYTaxon']);
                    $identification->setMYAuthor($iData['MYAuthor']);
                    $identification->setMYGenusQualifier($iData['MYGenusQualifier']);
                    $identification->setMYSpeciesQualifier($iData['MYSpeciesQualifier']);
                    $identification->setMYInfraRank(empty($iData['MYInfraRank']) ? '' : 'MY.infraRank' . $iData['MYInfraRank']);
                    $identification->setMYInfraEpithet($iData['MYInfraEpithet']);
                    $identification->setMYInfraAuthor($iData['MYInfraAuthor']);

                    try {
                        $om->save($identification);
                    } catch (\Exception $e) {
                        echo "\n FAILED {$data['subject']}\n\n";
                    };
                    // update to laji-etl
                    $mustikkaService->sendSpecimens([IdService::getUri($document->getSubject())]);
                }
            }
            */
        }
        $progress->finish();
    }

    public function sendToMustikkaByExcelAction()
    {
        $file = $this->params('from');
        if (!file_exists($file)) {
            throw new \Exception('Could not find file ' . $file);
        }
        /** @var \Kotka\Service\ImportService $importService */
        $importService = $this->getServiceLocator()->get('Kotka\Service\ImportService');
        $importService->setMaxRows(100000);
        $excelData = $importService->Excel2Array($file);
        if ($excelData === false) {
            var_dump($importService->getErrorCause());
            var_dump($importService->getErrorCode());
            exit();
        }
        /** @var ObjectManager $om */
        $om = $this->getServiceLocator()->get('TripleStore\ObjectManager');
        $om->enableHydrator();
        /** @var \Kotka\Service\MustikkaService $mustikkaService */
        $mustikkaService = $this->getServiceLocator()->get('Kotka\Service\MustikkaService');
        $progress = new ProgressBar(new Console(), 0, count($excelData));
        $batch = [];
        foreach ($excelData as $key => $data) {
            $progress->next();
            if (empty($data['MYNamespaceID'])) {
                continue;
            }
            $data['subject'] = $data['MYNamespaceID'] . '.' . $data['MYObjectID'];
            if (strpos($data['subject'], ':') === false) {
                $data['subject'] = 'luomus:' . $data['subject'];
            }
            $document = $om->fetch($data['subject'], false);
            if (!$document instanceof MYDocumentInterface) {
                echo "Couldn't find {$data['subject']}\n";
                continue;
            }
            array_push($batch, $data['subject']);
            if (count($batch) >= 50) {
                $mustikkaService->sendSpecimens($batch);
                $batch = [];
            }
        }
        if (count($batch) > 0) {
            $mustikkaService->sendSpecimens($batch);
        }
        $progress->finish();
    }


    public function makeCopiesAction()
    {
        $file = $this->params('from');
        if (!file_exists($file)) {
            throw new \Exception('Could not find file ' . $file);
        }
        /** @var \Kotka\Service\ImportService $importService */
        $importService = $this->getServiceLocator()->get('Kotka\Service\ImportService');
        $importService->setMaxRows(100000);
        /** @var \Kotka\Transform\Lutikka $lutikka */
        $lutikka = $this->getServiceLocator()->get('Kotka\Transform\Lutikka');
        $data = $importService->Excel2Array($file);
        if ($data === false) {
            var_dump($importService->getErrorCause());
            var_dump($importService->getErrorCode());
            exit();
        }
        $documents = [];
        foreach ($data as $document) {
            $document = $lutikka->transform($document);
            $oid = $document['MYObjectID'];
            if (strpos($oid, ',')) {
                $oid = trim($oid, ',');
                $document['MYOriginalSpecimenID'] = trim($document['MYOriginalSpecimenID'], ',');
                if ($oid !== $document['MYOriginalSpecimenID']) {
                    throw new \Exception("OriginalSpecimenID differs from MYObjectID!\n" . $document['MYOriginalSpecimenID'] . ':' . $oid);
                }
                $oIds = explode(',', $oid);
                foreach ($oIds as $realOId) {
                    $realOId = trim($realOId);
                    $copy = $document;
                    $copy['subject'] = $copy['MYNamespaceID'] . '.' . $realOId;
                    $copy['MYOriginalSpecimenID'] = $realOId;
                    $documents[] = $copy;
                }
            } else {
                $document['subject'] = $document['MYNamespaceID'] . '.' . $document['MYObjectID'];
                $documents[] = $document;
            }
        }
        /** @var \Kotka\Service\ExcelService $excelService */
        $excelService = $this->serviceLocator->get('Kotka\Service\ExcelService');
        $filename = 'updated-duplicates.xlsx';
        $form = $this->getSpecimenForm();
        $form->add([
            'name' => 'MZDateCreated',
            'type' => 'text',
        ]);
        $form->add([
            'name' => 'MZCreator',
            'type' => 'text',
        ]);
        $excelService->setForm($form);
        $success = $excelService->generateExcelFromArray($documents, $filename, true, true);
        if (!$success) {
            echo "File creation failed!";
        } else {
            echo "All done";
        }
        echo "\n";
    }

    public function updateLegsAction()
    {
        $file = $this->params('from');
        if (!file_exists($file)) {
            throw new \Exception('Could not find file ' . $file);
        }
        /** @var \Kotka\Service\ImportService $importService */
        $importService = $this->getServiceLocator()->get('Kotka\Service\ImportService');
        $importService->setMaxRows(100000);
        $data = $importService->Excel2Array($file);
        if ($data === false) {
            var_dump($importService->getErrorCause());
            var_dump($importService->getErrorCode());
            exit();
        }
        /** @var \Triplestore\Service\ObjectManager $om */
        $om = $this->serviceLocator->get('Triplestore\ObjectManager');
        foreach ($data as $document) {
            $subject = $document['MYNamespaceID'] . '.' . $document['MYObjectID'];
            $realLeg = explode(';',$document['MYGathering'][0]['MYLeg'][0]);
            array_walk($realLeg, function(&$val) {
                $val = trim($val);
            });
            $model = $om->fetch($subject);
            if (!$model instanceof MYDocumentInterface) {
                echo "couldn't find $subject\n";
                continue;
            }
            $gatherings = $model->getMYGathering();
            foreach($gatherings as $gathering) {
                if (!$gathering instanceof MYGatheringInterface) {
                    echo "Gathering is not a gathering!";
                    exit();
                }
                $legs = $gathering->getMYLeg();
                if (!empty($legs)) {
                    continue;
                }
                foreach($realLeg as $leg) {
                    $legPredicate = new Predicate('MY.leg');
                    $legPredicate->setLiteralValue($leg);
                    $om->updatePredicate($gathering->getSubject(), $legPredicate);
                }
            }
        }
        echo "All done\n";
    }

    public function updateBoldAction()
    {
        $fetchedIds = [];
        $file = 'bold_synonyms2.tsv';
        if (file_exists($file)) {

            $file = file_get_contents('bold_synonyms.tsv');
            $rows = explode("\n", $file);
            foreach ($rows as $row) {
                array_push(UpdateController::$buffer, $row);
                $parts = explode("\t", $row);
                array_push($fetchedIds, $parts[0]);
            }
        }
        array_push(UpdateController::$buffer, "ID\tTAXON\tsynonym_type\tsynonym\ttotal_records\trecords_with_species_name\tbins\tcountries\tdepositories\torder\tfamily\tgenus\tspecies");
        $this->fetchTaxa($file, $fetchedIds);
        echo "\nDONE!";
    }

    private function fetchTaxa($file, $fetchedIds, $page = 1) {
        $types = [
            'orthographicVariant',
            'basionym',
            'synonym',
            'misappliedName',
            'uncertainSynonym',
            'misspelledName',
            'objectiveSynonym',
            'subjectiveSynonym'
        ];
        $client = new Client(
            'https://api.laji.fi/v0/taxa/MX.37600/species?langFallback=true&includeHidden=false&includeMedia=false&includeDescriptions=false&onlyFinnish=true&sortOrder=taxonomic&page=' . $page . '&pageSize=1000&access_token=XqqrDHkYDBI0pj01fPao4oWWa27n6WeQRFahTXphGsAGWQVHZ5Zllnj7o4xQakyG',
            [
                'adapter' => 'Zend\Http\Client\Adapter\Curl',
                'timeout' => 120
            ]
        );
        $response = $client->send();
        if ($response->isOk()) {
            $data = json_decode($response->getBody(), true);
            if (isset($data['results'])) {
                echo "\nPAGE $page";
                foreach ($data['results'] as $taxon) {
                    if (!in_array($taxon['id'], $fetchedIds) && isset($taxon['scientificName'])) {
                        foreach ($types as $type) {
                            $plural = $type . 's';
                            if (isset($taxon[$type])) {
                                if (is_array($taxon[$type])) {
                                    foreach ($taxon[$type] as $value) {
                                        $this->makeBoldRow($taxon['id'], $taxon['scientificName'], $value, $type);
                                    }
                                } else {
                                    $this->makeBoldRow($taxon['id'], $taxon['scientificName'], $taxon[$type], $type);
                                }
                            } else if (isset($taxon[$plural])) {
                                if (is_array($taxon[$plural])) {
                                    foreach ($taxon[$plural] as $value) {
                                        $this->makeBoldRow($taxon['id'], $taxon['scientificName'], $value, $type);
                                    }
                                } else {
                                    $this->makeBoldRow($taxon['id'], $taxon['scientificName'], $taxon[$plural], $type);
                                }
                            }
                        }
                        echo '.';
                    }
                }
                file_put_contents($file, implode("\n",UpdateController::$buffer));
                if ($data['lastPage'] > $page) {
                    $this->fetchTaxa($file, $fetchedIds, $page + 1);
                }
            } else {
                array_push(UpdateController::$buffer, "\t\tAPI LAJI FI ERROR");
            }
        }
    }

    private function makeBoldRow($id, $taxon, $synonym, $type) {
        if (!is_string($synonym)) {
            if (isset($synonym['scientificName'])) {
                $this->makeBoldRow($id, $taxon, $synonym['scientificName'], $type);
            }
            return;
        }
        $client = new Client(
            'http://boldsystems.org/index.php/API_Public/stats?taxon=' . rawurlencode($synonym),
            [
                'adapter' => 'Zend\Http\Client\Adapter\Curl',
                'timeout' => 120
            ]
        );
        $response = $client->send();
        if ($response->isOk()) {
            $data = json_decode($response->getBody(), true);
            array_push(UpdateController::$buffer, "$id\t$taxon\t$type\t$synonym\t" . $this->boldNumbers($data));
        } else {
            array_push(UpdateController::$buffer, "$id\t$taxon\tBOLD ERROR");
        }
    }

    private function boldNumbers($result) {
        $response = [];
        if (isset($result['total_records'])) {
            array_push($response, $result['total_records']);
        } else {
            array_push($response, 0);
        }
        if (isset($result['records_with_species_name'])) {
            array_push($response, $result['records_with_species_name']);
        } else {
            array_push($response, 0);
        }
        $locs = ['bins', 'countries', 'depositories', 'order', 'family', 'genus', 'species'];
        foreach ($locs as $loc) {
            if (isset($result[$loc]) && isset($result[$loc]['count'])) {
                array_push($response, $result[$loc]['count']);
            } else {
                array_push($response, 0);
            }
        }
        return implode("\t", $response);
    }

    public function updatePrimaryLocationAction()
    {
        $file = $this->params('from');
        if (!file_exists($file)) {
            throw new \Exception('Could not find file ' . $file);
        }
        /** @var \Kotka\Service\ImportService $importService */
        $importService = $this->getServiceLocator()->get('Kotka\Service\ImportService');
        $importService->setMaxRows(100000);
        $data = $importService->Excel2Array($file);
        if ($data === false) {
            var_dump($importService->getErrorCause());
            var_dump($importService->getErrorCode());
            exit();
        }
        /** @var \Triplestore\Service\ObjectManager $om */
        $om = $this->serviceLocator->get('Triplestore\ObjectManager');
        foreach ($data as $document) {
            $subject = 'luomus:' . $document['MYNamespaceID'] . '.' . $document['MYObjectID'];
            $model = $om->fetch($subject, false);
            if (!$model instanceof MYDocumentInterface) {
                echo "couldn't find $subject\n";
                continue;
            }
            $location = $model->getMYPrimaryDataLocation();
            if (!empty($location)) {
                continue;
            }
            if (empty($document['MYPrimaryDataLocation'])) {
                echo "no location for $subject\n";
                continue;
            }
            //echo "update $subject\n";
            $locationPredicate = new Predicate('MY.primaryDataLocation');
            $locationPredicate->setLiteralValue($document['MYPrimaryDataLocation']);
            $om->updatePredicate($subject, $locationPredicate);
        }
        echo "All done\n";
    }

    public function moveBonsdorffAction()
    {
        /** @var Adapter $adapter */
        $adapter = $this->serviceLocator->get('Triplestore\Db\Adapter\Model');
        /** @var ObjectManager $om */
        $om = $this->serviceLocator->get('Triplestore\ObjectManager');
        $db = $adapter->getDbName();
        echo "Querying for all von Bonsdorff collections specimens...\n";
        $results = $adapter->query("Select subjectname from $db.RDF_STATEMENTVIEW where PREDICATENAME = 'MY.collectionID' AND OBJECTNAME = 'HR.407'")->execute();
        foreach ($results as $model) {
            if (!$model instanceof MYDocumentInterface) {
                continue;
            }
            $subject = $model->getSubject();
            $dataset = $model->getMYDatasetID();
            $dataset[] = 'GX.610';
            $collectionPredicate = new Predicate('MY.collectionID');
            $collectionPredicate->setResourceValue('HR.527');
            $datasetPredicate = new Predicate('MY.datasetID');
            $datasetPredicate->setResourceValue($dataset);
            $om->updatePredicate($subject, $datasetPredicate);
            $om->updatePredicate($subject, $collectionPredicate);
            $om->commit();
        }
    }

    /**
     * @return \Kotka\Form\MYDocument
     */
    private function getSpecimenForm()
    {
        $formGenerator = $this->serviceLocator->get('Kotka\Service\FormGeneratorService');
        $form = $formGenerator->getForm(new MYDocument());

        return $form;
    }

}
