<?php

namespace KotkaConsole\Controller;

use Importer\Service\DigitariumService;
use Importer\Service\HyntikkaService;
use Kotka\Triple\MOSOrganization;
use Kotka\Triple\MYCollection;
use Kotka\Triple\MYDocument;
use Zend\Form\Form;
use Zend\ProgressBar\Adapter\Console;

/**
 * Class ExportController is for handling all data exports done from the commandline
 *
 * @package KotkaConsole\Controller
 */
class ExportController extends UserController
{
    public function hyntikkaToExcelAction()
    {
        $adapter = $this->serviceLocator->get('hyntikka_adapter');
        $hyntikkaService = new HyntikkaService();
        $hyntikkaService->setProgressBarAdapter(new Console());
        $data = $hyntikkaService->getAll($adapter);
        /** @var \Kotka\Service\ExcelService $excelService */
        $excelService = $this->serviceLocator->get('Kotka\Service\ExcelService');
        $formGenerator = $this->serviceLocator->get('Kotka\Service\FormGeneratorService');
        $form = $formGenerator->getForm(new MYDocument());
        $excelService->setForm($form);
        $excelService->generateExcelFromArray($data, 'hyntikka.xlsx', false, true);
    }

    public function digitariumToExcelAction()
    {
        /** @var DigitariumService $digitariumService */
        $digitariumService = $this->getServiceLocator()->get('Importer\Service\DigitariumService');
        //$excelSheet = $digitariumService->getAllInExcelSheet();
        $excelSheet = $digitariumService->getAllXmlInExcelSheet('Sanikkaiset_updated');
        /** @var \Kotka\Service\ExcelService $excelService */
        $excelService = $this->serviceLocator->get('Kotka\Service\ExcelService');
        $excelService->generateExcelFromSheet($excelSheet, 'digitarium.xlsx', true);
    }

    public function collectionAction()
    {
        $this->classToExcelAction('Kotka\Service\CollectionService', new MYCollection());
        echo "Done\n";
    }

    public function organizationAction()
    {
        $this->classToExcelAction('Kotka\Service\OrganizationService', new MOSOrganization());
        echo "Done\n";
    }

    protected function classToExcelAction($serviceName, $formClass)
    {
        /** @var \Kotka\Service\ExcelService $excelService */
        $excelService = $this->serviceLocator->get('Kotka\Service\ExcelService');
        /** @var \Kotka\Service\FormGeneratorService $formGenerator */
        $formGenerator = $this->serviceLocator->get('Kotka\Service\FormGeneratorService');
        $form = $formGenerator->getForm($formClass);
        self::addMetaToForm($form);
        $excelService->setForm($form);
        $service = $this->serviceLocator->get($serviceName);
        $excelService->setSkip([]);
        $parts = explode('\\', $serviceName);
        $file = 'export_' . str_replace('service','',strtolower(array_pop($parts))) . '.xlsx';
        $excelService->generateExcelFromArray($service->getAll(), $file, false, true);
    }

    public static function addMetaToForm(Form $form) {
        $form->add([
            'name' => 'subject',
            'type' => 'text'
        ], ['priority' => 1000]);
        $form->add([
            'name' => 'MZEditor',
            'type' => 'text'
        ], ['priority' => 10]);
        $form->add([
            'name' => 'MZCreator',
            'type' => 'text'
        ], ['priority' => 11]);
        $form->add([
            'name' => 'MZDateCreated',
            'type' => 'text'
        ], ['priority' => 12]);
        $form->add([
            'name' => 'MZDateEdited',
            'type' => 'text'
        ], ['priority' => 13]);
    }
}
