<?php

namespace KotkaConsole\Controller;

use Common\Service\IdService;
use Kotka\Model\Util;
use Kotka\ResultSet\AccessionResultSet;
use Kotka\Service\DocumentService;
use Kotka\Service\DocumentServiceInterface;
use Kotka\Service\ImportService;
use Kotka\Triple\KotkaEntityInterface;
use Kotka\Triple\PUUBranch;
use Kotka\Triple\PUUEvent;
use Triplestore\Db\Adapter\Adapter;
use Triplestore\Service\ObjectManager;
use Zend\Console\Request;
use Zend\ProgressBar\Adapter\Console;
use Zend\ProgressBar\ProgressBar;

/**
 * Class CronController is commandline controller to handle
 * cron related stuff
 *
 * @package KotkaConsole\Controller
 */
class ImportController extends UserController
{
    /** @var  \Kotka\Service\ImportService */
    protected $importService;

    /** @var  \Kotka\Service\CollectionService */
    protected $collectionService;

    public function collectionAction() {
        return $this->genericImportAction('Kotka\Service\CollectionService', 'Kotka\Triple\MYCollection');
    }

    public function organizationAction() {
        return $this->genericImportAction('Kotka\Service\OrganizationService', 'Kotka\Triple\MOSOrganization');
    }

    public function transactionAction() {
        return $this->genericImportAction('Kotka\Service\TransactionService', 'Kotka\Triple\HRATransaction');
    }

    /**
     * This should only be used by admins!!!
     * @return string
     */
    protected function genericImportAction($serviceName, $className)
    {
        /** @var Request $request */
        $request = $this->getRequest();
        $file = $request->getParam('file');
        $user = $request->getParam('user');
        if (!file_exists($file)) {
            return sprintf("Could not find file '%s'" . PHP_EOL, $file);
        }
        $importService = $this->getImportService();
        $importService->setRemoveLangEnding(false);
        $data = $importService->Excel2Array($file);
        if ($data === false) {
            $code = $importService->getErrorCode();
            $cause = $importService->getErrorCause();
            return sprintf("Could not import file (%s) %s" . PHP_EOL .
                "make sure that all the columns are in text format!" . PHP_EOL, $code, $cause);
        }
        /** @var \Kotka\Service\FormGeneratorService $formGenerator */
        $formGenerator = $this->serviceLocator->get('Kotka\Service\FormGeneratorService');
        $service = $this->serviceLocator->get($serviceName);
        if (!$service instanceof DocumentServiceInterface) {
            return sprintf('Invalid service given!');
        }
        $organizations = [];
        foreach($data as $row) {
            if (isset($row['MZOwner'])) {
                $organizations[$row['MZOwner']] = $row['MZOwner'];
            }
        }
        if (!$this->login($user, $organizations)) {
            return sprintf("Could not login with username '%s'" . PHP_EOL, $user);
        }
        $adapter = new Console();
        $progress = new ProgressBar($adapter, 0, count($data));
        foreach($data as $row) {
            $progress->next();
            $method = 'create';
            $instance = null;
            if (!isset($row['subject']) || empty($row['subject'])) {
                // continue;
            } else {
                $instance = $service->getById($row['subject']);
                $method = 'update';
            }
            if (isset($row['MOSDateOrdersDue']) && !empty($row['MOSDateOrdersDue'])) {
                $row['MOSDateOrdersDue'] = implode('-', array_reverse(explode('/', $row['MOSDateOrdersDue'])));
            } else {
                unset($row['MOSDateOrdersDue']);
            }
            if (!$instance instanceof $className) {
                $instance = new $className();
            }
            $form = $formGenerator->getForm($instance);
            ExportController::addMetaToForm($form);

            $form->bind($instance);
            $form->setData($row);
            if ($form->isValid()) {
                if ($instance instanceof KotkaEntityInterface) {
                    if ($service instanceof DocumentService) {
                        $service->setEditor($instance->getMZEditor());
                        $service->setCreator($instance->getMZCreator());
                    }
                    if (!$service->{$method}($instance)) {
                        echo "failed to insert " . PHP_EOL;
                        var_dump($instance);
                        exit();
                    }
                }
            } else {
                $msgs = Util::extract_messages($form->getMessages());
                foreach ($msgs as $place => $message) {
                    $errorSubject = isset($row['subject']) ? $row['subject'] : '';
                    echo $errorSubject . ' => ' . $place . ': ' . $message . "\n";
                }
            }
        }
        $progress->finish();
        return '';
    }

    public function importAction()
    {
        /** @var Request $request */
        $request = $this->getRequest();
        IdService::setDefaultQNamePrefix($request->getParam('domain', 'luomus'));
        $source = $request->getParam('source');
        $username = $request->getParam('user');
        $organization = $request->getParam('organization');
        $datatype = $request->getParam('datatype', 'zoospecimen');
        $type = $request->getParam('type');
        $allowErrorsIn = $request->getParam('allow-dirty');
        if (!$this->login($username, $organization)) {
            return sprintf("Could not login with username '%s'" . PHP_EOL, $username);
        }
        $importService = $this->getImportService();
        if ($allowErrorsIn !== null) {
            $importService->setAllowErrorsIn(explode(',', $allowErrorsIn));
        }
        if ($organization !== null) {
            $importService->setDefaultOrganization($organization);
        }
        $importService->setOptions($this->getOptions());
        if ($request->getParam('transform')) {
            $transformer = 'Kotka\Transform\\' . $request->getParam('transform');
            if ($this->getServiceLocator()->has($transformer)) {
                $importService->setTransform($this->getServiceLocator()->get($transformer));
            }
        }
        switch ($type) {
            case 'json':
                return "needs update to triplestore" . PHP_EOL;
            case 'excel':
                return $this->excelAction($source, $organization, $datatype);
            case 'database':
                return "needs update to triplestore" . PHP_EOL;
            default:
                return sprintf("Invalid type '%s' specified" . PHP_EOL, $type);
        }
    }

    public function branchAction() {
        $importService = $this->getImportService();
        $importService->setMaxRows(100000);
        $importService->setRemoveLangEnding(false);

        // Read excel
        $data = $importService->Excel2Array('SijoituksetKotkaanTuotantoon_FINAL.xls');
        if ($data === false) {
            $code = $importService->getErrorCode();
            $cause = $importService->getErrorCause();
            return sprintf("Could not import file (%s) %s" . PHP_EOL .
                "make sure that all the columns are in text format!" . PHP_EOL, $code, $cause);
        }

        // Fetch documents that have originalSpecimenID
        /** @var Adapter $adapter */
        $adapter = $this->serviceLocator->get('Triplestore\Db\Adapter\Array');
        $db = $adapter->getDbName();
        $results = $adapter->query(
    "Select subjectname from $db.RDF_STATEMENTVIEW 
              where PREDICATENAME = 'MY.originalSpecimenID' 
                  AND subjectname like 'luomus:L.%' 
                  AND resourceliteral like '%-%'
              ")->execute();

        // This will be filled with data in form of originalSpecimenID => documentID
        $roots = [];
        foreach ($results as $row) {
            if (isset($row['subject'])) {
                $roots[$row['MY.originalSpecimenID']] = $row['subject'];
            }
        }

        // Create the branch data
        /** @var \Kotka\Service\AccessionService $accessionService */
        $accessionService = $this->serviceLocator->get('Kotka\Service\AccessionService');
        $formGenerator = $this->serviceLocator->get('Kotka\Service\FormGeneratorService');
        $cnt = 0;
        $branches = [];
        foreach ($data as $row) {
            $cnt++;
            $original = $row['MYOriginalSpecimenID'];
            if (!isset($roots[$original])) {
                continue;
            }
            $key = $row['MYOriginalSpecimenID'] . ':' . $row['CollectionID'];
            if (isset($branches[$key])) {
                if (isset($branches[$key]['PUUNotes']) && !empty($branches[$key]['PUUNotes'])) {
                    $branches[$key]['PUUNotes'] = $branches[$key]['PUUNotes'] . ' | ' . $row['PUUNotes'];
                } else {
                    $branches[$key]['PUUNotes'] = $row['PUUNotes'];
                }
                echo "exists $key\n---\n" . $branches[$key]['PUUNotes'] . "\n---\n";
            } else {
                $branches[$key] = [
                    'PUUAccessionID' => $roots[$original],
                    'PUUCollectionID' => $row['CollectionID'],
                    'PUUNotes' => $row['PUUNotes'],
                    'PUUExists' => $row['PUUExists'] === 'Yes' ? true : false,
                    'PUULocation' => $row['PUULocation'],
                    'PUUWgs84Longitude' => $row['PUUWgs84Longitude'],
                    'PUUWgs84Latitude' => $row['PUUWgs85Latitude'],
                    'PUUGardenLocationNotes' => $row['PUUGardenLocationNotes']
                ];
            }
        }

        foreach ($branches as $key => $branchData) {
            $branch = new PUUBranch();
            /** @var \Kotka\Form\PUUBranch $form */
            $form = $formGenerator->getForm($branch);
            $form->setObject($branch);
            $form->setData($branchData);

            if ($form->isValid()) {
                /** @var PUUBranch $obj */
                $obj = $form->getData();
                try {
                    echo '.';
                    $accessionService->storeBranch($obj);
                } catch (\Exception $e) {
                    echo "SAVING FAILED";
                    var_dump($row);
                    echo "delete everyting before this and try again. Row: " . $cnt;
                    exit();
                }
            } else {
                echo "NOT VALID";
                var_dump($row);
                print_r($form->getMessages());
                echo "delete everyting before this and try again. Row: " . $cnt;
                exit();
            }
        }

        echo "\nDONE $cnt rows!\n\n";
    }

    public function eventsAction() {
        $importService = $this->getImportService();
        $importService->setMaxRows(100000);
        $importService->setRemoveLangEnding(false);

        // Read excel branch
        $data = $importService->Excel2Array('SijoituksetKotkaanTuotantoon_FINAL.xls');
        if ($data === false) {
            $code = $importService->getErrorCode();
            $cause = $importService->getErrorCause();
            return sprintf("Could not import file (%s) %s" . PHP_EOL .
                "make sure that all the columns are in text format!" . PHP_EOL, $code, $cause);
        }

        $nroOfSpecimen = [];
        $collection = [];
        foreach ($data as $row) {
            $key = $row['MYOriginalSpecimenID'] . '-' . $row['LohkonumeroOriginal'] . '-' . $row['PUUExists'];
            if (isset($row['CollectionID'])) {
                $collection[$key] = $row['CollectionID'];
            } else {
                echo "no collection for $key\n";
            }
            if (isset($row['PUUAmount']) && !isset($nroOfSpecimen[$key])) {
                $nroOfSpecimen[$key] = $row['PUUAmount'];
            }
        }

        // Fetch documents that have originalSpecimenID
        $file = 'accession.json';
        if (!file_exists($file)) {
            echo 'no file';
            /** @var Adapter $adapter */
            $adapter = $this->serviceLocator->get('Triplestore\Db\Adapter\Array');
            $db = $adapter->getDbName();
            $results = $adapter->query(
                "Select subjectname from $db.RDF_STATEMENTVIEW 
              where PREDICATENAME = 'MY.originalSpecimenID' 
                  AND subjectname like 'luomus:L.%' 
                  AND resourceliteral like '%-%'
              ")->execute();
            $roots = [];
            foreach ($results as $row) {
                if (isset($row['subject'])) {
                    $roots[$row['MY.originalSpecimenID']] = $row['subject'];
                }
            }
            file_put_contents($file, json_encode($roots));
        }
        $roots = json_decode(file_get_contents($file), true);

        // Read events excel
        $data = $importService->Excel2Array('Fixed.xls');
        if ($data === false) {
            $code = $importService->getErrorCode();
            $cause = $importService->getErrorCause();
            return sprintf("Could not import file (%s) %s" . PHP_EOL .
                "make sure that all the columns are in text format!" . PHP_EOL, $code, $cause);
        }


        // Create the event data
        /** @var \Kotka\Service\AccessionService $accessionService */
        $accessionService = $this->serviceLocator->get('Kotka\Service\AccessionService');
        $formGenerator = $this->serviceLocator->get('Kotka\Service\FormGeneratorService');
        $allBranches = [];
        $cnt = 1;
        foreach ($data as $row) {
            $cnt++;
            $key = $row['MYOriginalSpecimenID'] . '-' . $row['Location'] . '-' . $row['PUUExists'];
            if (!isset($collection[$key]) ||!isset($row['MYOriginalSpecimenID']) || !isset($roots[$row['MYOriginalSpecimenID']])) {
                continue;
            }
            $amount = isset($nroOfSpecimen[$key]) ? $nroOfSpecimen[$key] : '';
            $exits = $row['PUUExists'] === 'Yes' ? true : false;
            /** @var AccessionResultSet $branches */
            if (!isset($allBranches[$roots[$row['MYOriginalSpecimenID']]])) {
                $allBranches[$roots[$row['MYOriginalSpecimenID']]] = $accessionService->getAllBranchesByRoot($roots[$row['MYOriginalSpecimenID']], false)->getBranches();
            }
            $branches = $allBranches[$roots[$row['MYOriginalSpecimenID']]];
            $candi = [];
            foreach ($branches as $branch) {
                /** @var \Kotka\Triple\PUUBranch $branch */
                if ($branch->getPUUCollectionID() === $collection[$key]) {
                    if ($branch->getPUUExists() === $exits) {
                        $date = '';
                        if (isset($row['PUUDate'])) {
                            $date = '2018-01-26';
                        }

                        $event = new PUUEvent();
                        /** @var \Kotka\Form\PUUBranch $form */
                        $form = $formGenerator->getForm($event);
                        $form->setObject($event);
                        $form->setData([
                            'PUUAmount' => $amount,
                            'PUUEventType' => 'MY.eventTypeAtlantis',
                            'PUUNotes' => $row['PUUNotes'],
                            'PUUDate' =>$date,
                            'PUUBranchID' => $branch->getSubject()
                        ]);
                        if ($form->isValid()) {
                            /** @var PUUEvent $obj */
                            $obj = $form->getData();
                            try {
                                echo '.';
                                $accessionService->storeEvent($obj);
/*
                                echo "---\n";
                                echo $row['MYOriginalSpecimenID'] . "\n";
                                echo $row['Location'] . "\n";
                                echo $row['PUUExists'] . "\n";
                                echo $date . "\n";
*/

                            } catch (\Exception $e) {
                                echo "SAVING FAILED";
                                var_dump($row);
                                echo "delete everyting before this and try again. Row " . $cnt;
                                exit();
                            }
                        } else {
                            echo "NOT VALID";
                            var_dump($row);
                            print_r($form->getMessages());
                            echo "delete everyting before this and try again. Row " . $cnt;
                            exit();
                        }
                        continue 2;
                    }
                    array_push($candi, $branch);
                }
            }
            if (count($candi) > 0) {
                $branch = $candi[0];
                $date = '';
                if (isset($row['PUUDate'])) {
                    $date = '2018-01-26';
                }

                $event = new PUUEvent();
                /** @var \Kotka\Form\PUUBranch $form */
                $form = $formGenerator->getForm($event);
                $form->setObject($event);
                $form->setData([
                    'PUUAmount' => $amount,
                    'PUUEventType' => 'MY.eventTypeAtlantis',
                    'PUUNotes' => $row['PUUNotes'],
                    'PUUDate' =>$date,
                    'PUUBranchID' => $branch->getSubject()
                ]);
                if ($form->isValid()) {
                    /** @var PUUEvent $obj */
                    $obj = $form->getData();
                    try {
                        echo '.';
                        $accessionService->storeEvent($obj);
/*
                        echo "---\n";
                        echo $row['MYOriginalSpecimenID'] . "\n";
                        echo $row['Location'] . "\n";
                        echo $row['PUUExists'] . "\n";
                        echo $date . "\n";
*/

                    } catch (\Exception $e) {
                        echo "SAVING FAILED";
                        var_dump($row);
                        echo "delete everyting before this and try again. Row " . $cnt;
                        exit();
                    }
                } else {
                    echo "NOT VALID";
                    var_dump($row);
                    print_r($form->getMessages());
                    echo "delete everyting before this and try again. Row " . $cnt;
                    exit();
                }
            }
        }
        echo "\nDONE $cnt\n\n";
    }

    private function excelAction($source, $organization, $datatype)
    {
        $importer = $this->getImportService();
        $importer->setMaxRows(100000);

        echo "Reading excel file" . PHP_EOL;
        $data = $importer->Excel2Array($source, 0);
        if (!is_array($data)) {
            return "Could not import excel. " . $importer->getErrorCause() . "Error code(" . $importer->getErrorCode() . ")\n";
        }

        echo "Validating" . PHP_EOL;
        $docCount = count($data) - 1;
        $adapter = new Console();
        $progress = new ProgressBar($adapter, 0, $docCount);
        $importer->setProgressBar($progress);
        if ($importer->validateArray($data, $datatype, $organization) === false) {
            $errorFile = $this->getRequest()->getParam('error-file');
            $cause = $importer->getErrorCause();
            if ($errorFile) {
                file_put_contents($errorFile, $cause);
            }
            if ($this->getRequest()->getParam('skip-on-error')) {
                echo "Skipping:" . PHP_EOL . $cause;
            } else {
                return "Failed " . $importer->getErrorCode() . " (" . $cause . ")" . PHP_EOL;
            }
        }
        $progress->finish();
        if ($this->getRequest()->getParam('dry-run')) {
            return "Validation done!" . PHP_EOL;
        }

        echo PHP_EOL . "Saving specimens" . PHP_EOL;
        $progress = new ProgressBar($adapter, 0, $docCount);
        $importer->getEventManager()->attach('import', function ($e) use (&$progress) {
            $params = $e->getParams();
            if (isset($params['action']) && $params['action'] === 'next') {
                $progress->next();
            }
        });
        $result = $importer->importArray($data);

        $progress->finish();
        if ($result === false) {
            return "Failed " . $importer->getErrorCode() . " (" . $importer->getErrorCause() . ")" . PHP_EOL;
        }

        return "Done!" . PHP_EOL;
    }

    private function getOptions()
    {
        /** @var Request $request */
        $request = $this->getRequest();
        $withExisting = $request->getParam('existing');
        $options = array();
        if ($withExisting == 'update') {
            $options[] = ImportService::OPTION_UPDATE_EXISTING;
        }
        if ($withExisting == 'skip') {
            $options[] = ImportService::OPTION_SKIP_EXISTING;
        }
        if ($request->getParam('verbose') || $request->getParam('v')) {
            $options[] = ImportService::OPTION_VERBOSE;
        }
        if ($request->getParam('halt-on-error')) {
            $options[] = ImportService::OPTION_HALT_ON_ERROR;
        }
        if ($request->getParam('user-from-data')) {
            $options[] = ImportService::OPTION_USER_FROM_DATA;
        }
        if ($request->getParam('new-id')) {
            $options[] = ImportService::OPTION_NEW_ID;
        }
        return $options;
    }

    /**
     * Returns import service
     *
     * @return \Kotka\Service\ImportService
     */
    private function getImportService()
    {
        if ($this->importService === null) {
            $this->importService = $this->getServiceLocator()->get('Kotka\Service\ImportService');
        }

        return $this->importService;
    }

    private function getCollectionService() {
        if ($this->collectionService=== null) {
            $this->collectionService = $this->getServiceLocator()->get('Kotka\Service\CollectionService');
        }

        return $this->collectionService;
    }

}
