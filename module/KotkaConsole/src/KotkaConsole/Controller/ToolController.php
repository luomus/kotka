<?php

namespace KotkaConsole\Controller;

use Kotka\Model\ExcelColumn;
use Kotka\Model\ExcelSheet;
use Triplestore\Model\Predicate;
use Triplestore\Service\ObjectManager;
use Zend\Console\Request;
use Zend\Mvc\Controller\AbstractActionController;

/**
 * Class ToolController is commandline controller to handle
 * miscellaneous things with the kotka
 *
 * @package KotkaConsole\Controller
 */
class ToolController extends AbstractActionController
{
    private $importService;

    public function indexAction()
    {
        return "Nothing to do." . PHP_EOL;
    }

    public function deleteAction()
    {
        /** @var Request $request */
        $request = $this->getRequest();
        $subjects = $request->getParam('subject');
        /** @var ObjectManager $om */
        $om = $this->getServiceLocator()->get('Triplestore\ObjectManager');
        $msg = array();
        if (!empty($subjects)) {
            $items = explode(',', $subjects);
            $items = array_map('trim', $items);
        } else {
            $items = $om->findBy(['MZ.scheduledForDeletion' => 'true'], null, null, null, ['MZ.scheduledForDeletion']);
            $items = array_keys($items);
        }
        $queue = $this->getServiceLocator()->get('SlmQueue\Queue\QueuePluginManager')->get('default');
        foreach ($items as $item) {
            $msg[] = 'Added ' . $item . ' to delete queue';
            $job = new \Kotka\Job\Delete();
            $job->setSubject($item);
            $queue->push($job, array('delay' => 300));
            $predicate = new Predicate('MZ.scheduledForDeletion');
            $predicate->setLiteralValue('true');
            $om->updatePredicate($item, $predicate);
        }
        $msg[] = 'Done (All above will be deleted in 5min)';

        return implode(PHP_EOL, $msg) . PHP_EOL;
    }

    /**
     * Run with php public/index.php tool gpi
     */
    public function gpiAction()
    {
        $importer = $this->getImportService();
        $importer->setMaxRows(100000);
        $data = $importer->Excel2Array('./H-ACH_specimentable_all_text.xls', 0);
        if (!$data) {
            var_dump($importer->getErrorCode());
            var_dump($importer->getErrorCause());
            exit();
        }
        $lookup = [];
        foreach ($data as $row) {
            $lookup[$row['Barcode']] = $row;
        }
        $data = $importer->Excel2Array('./H-ACH_identificationstable_all_text.xls', 0);
        if (!$data) {
            var_dump($importer->getErrorCode());
            var_dump($importer->getErrorCause());
            exit();
        }
        foreach ($data as $row) {
            $idnro = $row['IdeNo'];
            $id = $row['Barcode'];
            unset($row['IdeNo']);
            unset($row['Barcode']);
            foreach ($row as $key => $value) {
                $lookup[$id][$idnro . $key] = $value;
            }
        }
        $processed = array_values($lookup);
        $columns = [];
        foreach ($processed as $idx => $data) {
            foreach ($data as $key => $value) {
                if (!isset($columns[$key])) {
                    $columns[$key] = [];
                }
                $columns[$key][$idx] = $value;
            }
        }
        $sheet = new ExcelSheet();
        foreach ($columns as $header => $data) {
            $column = new ExcelColumn();
            $column->setField($header);
            $column->setLabel($header);
            $column->setData($data);
            $sheet->addColumn($column);
        }
        /** @var \Kotka\Service\ExcelService $excelService */
        $excelService = $this->serviceLocator->get('Kotka\Service\ExcelService');
        $excelService->generateExcelFromSheet($sheet, 'gpi.xlsx', true);
    }

    /**
     * Returns import service
     *
     * @return \Kotka\Service\ImportService
     */
    private function getImportService()
    {
        if ($this->importService === null) {
            $this->importService = $this->getServiceLocator()->get('Kotka\Service\ImportService');
        }

        return $this->importService;
    }

}
