<?php

namespace KotkaConsole\Controller;

use Common\Service\IdService;
use Traversable;
use Zend\Mvc\Controller\AbstractActionController;

/**
 * Class CronController is commandline controller to handle
 * cron related stuff
 *
 * @package KotkaConsole\Controller
 */
class CronController extends AbstractActionController
{
    /** Delimiter used in the lists given to this controller */
    const DELIMITER = ',';

    /** @var  \Kotka\Service\MustikkaService */
    protected $mustikkaService;
    /** @var  \Kotka\Service\SpecimenService */
    protected $specimenService;

    /**
     * This is the action is ran when called from commandline
     * with "run cron" parameters
     *
     * @return string
     */
    public function runAction()
    {
        $request = $this->getRequest();
        $items = $request->getParam('items');
        $items = $items !== null ? explode(self::DELIMITER, $items) : null;
        $msg = array();
        if ($request->getParam('send-to-mustikka') || $request->getParam('m')) {
            $msg[] = $this->toMustikkaAction($items);
        }
        $msg[] = 'Done';

        return implode(PHP_EOL, $msg) . PHP_EOL;
    }

    /**
     * Sends specimens to Mustikka
     * @param $list [optional] list of uris or qnames to be send
     * @return string
     */
    public function toMustikkaAction(array $list = null)
    {
        if ($list === null) {
            $specimens = $this->getSpecimenService()->getAllNotInMustikka();
        } else {
            $specimens = array();
            foreach ($list as $item) {
                $qname = IdService::getQName($item);
                $specimen = $this->getSpecimenService()->getById($qname);
                if ($specimen !== null) {
                    $specimens[$specimen->getSubject()] = $specimen;
                } else {
                    return sprintf("could not find item with uri '%s'", $qname);
                }
            }
        }
        if (empty($specimens)) {
            return 'No specimens in queue to mustikka';
        }
        return $this->sendSpecimens($specimens);
    }

    public function sendSpecimens($specimens)
    {
        if (!is_array($specimens) && !$specimens instanceof Traversable) {
            throw new \Exception('Invalid list of specimens provided.');
        }
        $count = array(true => 0, false => 0);
        $mustikkaService = $this->getMustikkaService();
        $specimenService = $this->getSpecimenService();
        foreach ($specimens as $key => $value) {
            $specimen = $specimenService->getById($key);
            /** @var \Kotka\Triple\MYDocument $specimen */
            $specimenService->checkWgsCoordinateExists($specimen);
            $success = $mustikkaService->sendSpecimen($specimen->getSubject());
            $specimenService->_update($specimen);
            $count[$success]++;
        }

        return sprintf("Sending to Mustikka: %s succeeded and %s failed", $count[true], $count[false]);
    }

    /**
     * Returns mustikka service
     *
     * @return \Kotka\Service\MustikkaService
     */
    public function getMustikkaService()
    {
        if ($this->mustikkaService === null) {
            $this->mustikkaService = $this->getServiceLocator()->get('Kotka\Service\MustikkaService');
        }

        return $this->mustikkaService;
    }

    /**
     * Returns specimen service
     *
     * @return \Kotka\Service\SpecimenService
     */
    public function getSpecimenService()
    {
        if ($this->specimenService === null) {
            $this->specimenService = $this->getServiceLocator()->get('Kotka\Service\SpecimenService');
        }

        return $this->specimenService;
    }

}
