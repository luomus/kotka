<?php

namespace KotkaConsole\Generator\Form\Template;

/**
 * Class MFSample
 * @package KotkaConsole\Generator\Form\Template
 */
class MFSample extends Subject
{

    protected $base = 'Kotka\Form\MFSample';
    protected $hydrator = null;

    public function __construct()
    {
        parent::__construct();

        $this->remove('subject');

        $this->add(array(
            'name' => 'subject',
            'type' => 'text',
            'options' => array()
        ));

        $this->add(array(
            'name' => 'MYMeasurement',
            'type' => 'FieldSet'
        ));

        $this->add(array(
            'name' => 'MFPreparation',
            'options' => array(
                'count' => 1,
                'should_create_template' => false,
                'allow_add' => true,
                'allow_remove' => true,
                'template_placeholder' => '__preparation__',
            ),
        ));

        $this->add(array(
            'name' => 'MFQualityCheckDate',
            'type' => 'text',
            'attributes' => array(
                'class' => 'dateInput',
                'placeholder' => 'date',
                'data-date-format' => 'DD.MM.YYYY',
                'data-date-autoclose' => 'true',
                'data-date-keep-invalid' => 'true',
                'data-date-use-strict' => 'true',
                'data-date-max-date' => 'now',
                'data-date-use-current' => 'day',
            ),
            'options' => array(
                'template' => 'kotka/partial/element/datetime-picker',
            )
        ));

        $this->add(array(
            'name' => 'MFIndividualsInPreparation',
            'type' => 'DatabaseSelect',
            'options' => array(
                'empty_option' => 'Select',
                'field' => 'MF.individualsInPreparation'
            )
        ));
        $this->add(array(
            'name' => 'MFElutionMedium',
            'type' => 'DatabaseSelect',
            'options' => array(
                'empty_option' => 'Select',
                'field' => 'MF.elutionMedium'
            )
        ));

        $this->add(array(
            'name' => 'MFPreparationType',
            'type' => 'DatabaseSelect',
            'options' => array(
                'empty_option' => 'Select',
                'field' => 'MF.preparationType'
            )
        ));

        $this->add(array(
            'name' => 'MFMaterial',
            'type' => 'DatabaseSelect',
            'options' => array(
                'empty_option' => 'Select',
                'field' => 'MF.material'
            )
        ));

        $this->add(array(
            'name' => 'MFQuality',
            'type' => 'DatabaseSelect',
            'options' => array(
                'empty_option' => 'Select',
                'field' => 'MF.quality'
            )
        ));

        $this->add(array(
            'name' => 'MFRecordBasisSample',
            'type' => 'DatabaseSelect',
            'options' => array(
                'empty_option' => 'Select',
                'field' => 'MF.recordBasisSample'
            )
        ));

        $this->add(array(
            'name' => 'MFRecordParts',
            'type' => 'DatabaseSelect',
            'options' => array(
                'empty_option' => 'Select',
                'field' => 'MF.recordParts'
            )
        ));
        $this->add(array(
            'name' => 'MFStatus',
            'type' => 'DatabaseSelect',
            'options' => array(
                'empty_option' => 'Select',
                'field' => 'MF.status'
            )
        ));

        $this->add(array(
            'name' => 'MFPreservation',
            'type' => 'DatabaseSelect',
            'attributes' => array(
                'id' => 'preservation',
                'multiple' => true
            ),
            'options' => array(
                'field' => 'MF.preservation',
                'template' => 'kotka/partial/element/select2'
            )
        ));

        $this->add(array(
            'name' => 'MFQualityCheckMethod',
            'type' => 'DatabaseSelect',
            'attributes' => array(
                'id' => 'qualityCheckMethod',
                'multiple' => true
            ),
            'options' => array(
                'field' => 'MF.qualityCheckMethod',
                'template' => 'kotka/partial/element/select2'
            )
        ));

        $this->add(array(
            'name' => 'MFCollectionID',
            'type' => 'DatabaseSelect',
            'attributes' => array(
                'id' => 'collectionID'
            ),
            'options' => array(
                'empty_option' => 'Select',
                'field' => 'MF.collectionID',
                'template' => 'kotka/partial/element/select2'
            )
        ));


        $this->add(array(
            'name' => 'MFDatasetID',
            'type' => 'DatabaseSelect',
            'attributes' => array(
                'id' => 'datasetID',
                'multiple' => true,
            ),
            'options' => array(
                'field' => 'MF.datasetID',
                'template' => 'kotka/partial/element/select2',
            )
        ));

        $this->add(array(
            'name' => 'MFSpecimenID',
            'type' => 'resource',
            'filters' => array(
                array(
                    'name' => 'ToURI'
                )
            ),
            'validators' => array(
                array(
                    'name' => 'Kotka\Validator\DocumentType',
                    'options' => array(
                        'type' => 'MY.document'
                    )
                )
            )
        ));

        $this->add(array(
            'name' => 'MFAdditionalIDs',
            'type' => 'ArrayCollection',
            'attributes' => array(
                'id' => 'MFAdditionalIDs',
            ),
            'options' => array(
                'count' => 1,
                'should_create_template' => true,
                'allow_add' => true,
                'allow_remove' => true,
                'target_element' => array(
                    'name' => 'MFAdditionalIDs',
                    'type' => 'text',
                )
            ),
            'filters' => array(
                array(
                    'name' => 'ForceArray'
                )
            )
        ));

        $this->add(array(
            'name' => 'MFEvent',
            'type' => 'ArrayCollection',
            'attributes' => array(
                'id' => 'MFEvent'
            ),
            'options' => array(
                'count' => 1,
                'should_create_template' => true,
                'allow_add' => true,
                'allow_remove' => true,
                'target_element' => array(
                    'name' => 'MFEvent',
                    'type' => 'textarea',
                )
            ),
            'filters' => array(
                array(
                    'name' => 'ForceArray'
                )
            )
        ));

        $this->add(array(
            'name' => 'MFGenbank',
            'type' => 'ArrayCollection',
            'attributes' => array(
                'id' => 'MFGenbank'
            ),
            'options' => array(
                'count' => 1,
                'should_create_template' => true,
                'allow_add' => true,
                'allow_remove' => true,
                'target_element' => array(
                    'name' => 'MFGenbank',
                    'type' => 'text',
                )
            ),
            'filters' => array(
                array(
                    'name' => 'ForceArray'
                )
            )
        ));

        $this->add(array(
            'name' => 'MFPublication',
            'type' => 'ArrayCollection',
            'attributes' => array(
                'id' => 'MFPublication'
            ),
            'options' => array(
                'count' => 1,
                'should_create_template' => true,
                'allow_add' => true,
                'allow_remove' => true,
                'target_element' => array(
                    'name' => 'MFPublication',
                    'type' => 'text',
                )
            ),
            'filters' => array(
                array(
                    'name' => 'ForceArray'
                )
            )
        ));

        $this->add(array(
            'name' => 'MFNotes',
            'type' => 'textarea',
        ));
    }
}
