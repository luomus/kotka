<?php

namespace KotkaConsole\Generator\Form\Template;

/**
 * Class PUUBranch
 * @package KotkaConsole\Generator\Form\Template
 */
class PUUEvent extends Base
{

    protected $base = 'Kotka\Form\PUUEvent';

    public function __construct()
    {

        $this->add(array(
            'name' => 'PUUBranchID',
            'type' => 'hidden',
        ));

        $this->add(array(
            'name' => 'PUULifeStage',
            'type' => 'hidden',
            'options' => array(
                'empty_option' => '',
                'field' => 'PUU.lifeStage'
            )
        ));

        $this->add(array(
            'name' => 'PUUEventType',
            'type' => 'DatabaseSelect',
            'options' => array(
                'empty_option' => '',
                'field' => 'PUU.eventType'
            )
        ));

        $this->add(array(
            'name' => 'PUUDate',
            'type' => 'text',
            'attributes' => array(
                'id' => 'transactionSent',
                'type' => 'text',
                'data-date-format' => 'YYYY-MM-DD',
                'data-date-autoclose' => 'true',
                'data-date-max-date' => 'now',
                'data-date-use-current' => 'day',
            ),
            'options' => array(
                'template' => 'kotka/partial/element/datetime-picker',
            )
        ));

        $this->add(array(
            'name' => 'PUUAgent',
            'type' => 'DatabaseSelect',
            'options' => array(
                'empty_option' => 'Select',
                'field' => 'MA.person-known',
                'template' => 'kotka/partial/element/select2'
            )
        ));

        $this->add(array(
            'name' => 'PUUNotes',
            'type' => 'textarea'
        ));
        $this->add(array(
            'name' => 'PUUAmountEstimationUnit',
            'type' => 'text'
        ));
        $this->add(array(
            'name' => 'PUUEstimatedSeedQuantity',
            'type' => 'text'
        ));
        $this->add(array(
            'name' => 'PUUNumberOfSeedsTested',
            'type' => 'text'
        ));
        $this->add(array(
            'name' => 'PUUNumberOfSeedsFull',
            'type' => 'text'
        ));
        $this->add(array(
            'name' => 'PUUNumberOfSeedsPartFull',
            'type' => 'text'
        ));
        $this->add(array(
            'name' => 'PUUNumberOfSeedsEmpty',
            'type' => 'text'
        ));
        $this->add(array(
            'name' => 'PUUERH',
            'type' => 'text'
        ));
        $this->add(array(
            'name' => 'PUUGerminationPercentage',
            'type' => 'text'
        ));
        $this->add(array(
            'name' => 'PUUViabilityPercentage',
            'type' => 'text'
        ));
        $this->add(array(
            'name' => 'PUUGerminationRate',
            'type' => 'text'
        ));
        $this->add(array(
            'name' => 'PUUSeedsExchangedInstitution',
            'type' => 'DatabaseSelect',
            'options' => array(
                'empty_option' => 'Select',
                'field' => 'PUU.seedsExchangedInstitution',
                'template' => 'kotka/partial/element/select2'
            )
        ));
        $this->add(array(
            'name' => 'PUUGerminationConditions',
            'type' => 'textarea'
        ));
        $this->add(array(
            'name' => 'PUUTestPassed',
            'type' => 'DatabaseSelect',
            'options' => array(
                'field' => 'boolean',
                'empty_option' => 'Select',
                'value_options' => array(
                    'true' => 'Yes',
                    'false' => 'No'
                ),
            ),
        ));


    }
}
