<?php

namespace KotkaConsole\Generator\Form\Template;

/**
 * Class MYUnit
 * @package KotkaConsole\Generator\Form\Template
 */
class HRAPermit extends AbstractTemplate
{

    protected $base = 'Kotka\Form\HRAPermit';
    protected $hydrator = null;

    public function __construct()
    {
        $this->add(array(
            'name' => 'HRAPermitType',
            'type' => 'DatabaseSelect',
            'options' => array(
                'empty_option' => 'Select',
                'field' => 'HRA.permitType'
            ),
        ));
        $this->add(array(
            'name' => 'HRAPermitStatus',
            'type' => 'DatabaseSelect',
            'options' => array(
                'empty_option' => 'Select',
                'field' => 'HRA.permitStatus'
            ),
        ));

        $this->add(array(
            'name' => 'HRAPermitFile',
            'type' => 'file',
            'options' => array(
                'template' => 'kotka/partial/element/file',
                'label' => 'File'
            )
        ));

        $this->add(array(
            'name' => 'HRAPermitStartDate',
            'type' => 'date',
            'attributes' => array(
                'id' => 'receiptReturned',
                'type' => 'text',
                'data-date-format' => HRATransaction::DATE_FORMAT_JS,
                'data-date-autoclose' => 'true',
                'data-date-max-date' => '2200-01-01',
                'data-date-use-current' => 'day',
            ),
            'options' => array(
                'format' => HRATransaction::DATE_FORMAT,
                'template' => 'kotka/partial/element/datetime-picker',
            )
        ));
        $this->add(array(
            'name' => 'HRAPermitEndDate',
            'type' => 'date',
            'attributes' => array(
                'id' => 'receiptReturned',
                'type' => 'text',
                'data-date-format' => HRATransaction::DATE_FORMAT_JS,
                'data-date-autoclose' => 'true',
                'data-date-max-date' => '2200-01-01',
                'data-date-use-current' => 'day',
            ),
            'options' => array(
                'format' => HRATransaction::DATE_FORMAT,
                'template' => 'kotka/partial/element/datetime-picker',
            )
        ));
    }
}
