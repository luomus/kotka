<?php

namespace KotkaConsole\Generator\Form\Template;

/**
 * Class MFSample
 * @package KotkaConsole\Generator\Form\Template
 */
class MFPreparation extends Subject
{

    protected $base = 'Kotka\Form\MFPreparation';
    protected $hydrator = null;

    public function __construct()
    {
        parent::__construct();

        $this->add(array(
            'name' => 'MFPreparedBy',
            'type' => 'text',
            'options' => array(
                'template' => 'kotka/partial/element/add-me',
                'add-me' => array(
                    'name' => 'this',
                )
            ),
        ));

        $this->add(array(
            'name' => 'MFPreparationDate',
            'type' => 'text',
            'attributes' => array(
                'class' => 'dateInput',
                'placeholder' => 'date',
                'data-date-format' => 'DD.MM.YYYY',
                'data-date-autoclose' => 'true',
                'data-date-keep-invalid' => 'true',
                'data-date-use-strict' => 'true',
                'data-date-max-date' => 'now',
                'data-date-use-current' => 'day',
            ),
            'options' => array(
                'template' => 'kotka/partial/element/datetime-picker',
            )
        ));

        $this->add(array(
            'name' => 'MFPreparationMaterials',
            'type' => 'DatabaseSelect',
            'attributes' => array(
                'id' => 'preparationMaterials',
                'multiple' => true
            ),
            'options' => array(
                'field' => 'MF.preparationMaterials',
                'template' => 'kotka/partial/element/select2'
            )
        ));

        $this->add(array(
            'name' => 'MFPreparationProcess',
            'type' => 'DatabaseSelect',
            'attributes' => array(
                'id' => 'preparationProcess',
                'multiple' => true
            ),
            'options' => array(
                'field' => 'MF.preparationProcess',
                'template' => 'kotka/partial/element/select2'
            )
        ));
    }
}
