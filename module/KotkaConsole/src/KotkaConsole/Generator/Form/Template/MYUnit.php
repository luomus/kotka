<?php

namespace KotkaConsole\Generator\Form\Template;

/**
 * Class MYUnit
 * @package KotkaConsole\Generator\Form\Template
 */
class MYUnit extends AbstractTemplate
{

    protected $base = 'Kotka\Form\MYUnit';
    protected $hydrator = null;

    public function __construct()
    {

        $this->add(array(
            'name' => 'MYNotes',
            'type' => 'textarea',
        ));


        $this->add(array(
            'name' => 'MYMicrobiologicalRiskGroup',
            'type' => 'DatabaseSelect',
            'options' => array(
                'empty_option' => 'Select',
                'field' => 'MY.microbiologicalRiskGroup'
            )
        ));
        $this->add(array(
            'name' => 'MYEarliestEpochOrLowestSeries',
            'type' => 'DatabaseSelect',
            'options' => array(
                'empty_option' => 'Select',
                'field' => 'MY.earliestEpochOrLowestSeries'
            )
        ));

        $this->add(array(
            'name' => 'MYLatestEpochOrHighestSeries',
            'type' => 'DatabaseSelect',
            'options' => array(
                'empty_option' => 'Select',
                'field' => 'MY.latestEpochOrHighestSeries'
            )
        ));

        $this->add(array(
            'name' => 'MYPlantStatusCode',
            'type' => 'DatabaseSelect',
            'options' => array(
                'empty_option' => 'Select',
                'field' => 'MY.plantStatusCode'
            )
        ));

        $this->add(array(
            'name' => 'MYLifeStage',
            'type' => 'DatabaseSelect',
            'options' => array(
                'empty_option' => 'Select',
                'field' => 'MY.lifeStage'
            )
        ));

        $this->add(array(
            'name' => 'MYProvenance',
            'type' => 'DatabaseSelect',
            'options' => array(
                'empty_option' => 'Select',
                'field' => 'MY.provenance'
            )
        ));

        $this->add(array(
            'name' => 'MYFruitType',
            'type' => 'DatabaseSelect',
            'options' => array(
                'empty_option' => 'Select',
                'field' => 'MY.fruitType'
            )
        ));

        $this->add(array(
            'name' => 'MYSeedMaturity',
            'type' => 'DatabaseSelect',
            'options' => array(
                'empty_option' => 'Select',
                'field' => 'MY.seedMaturity'
            )
        ));

        $this->add(array(
            'name' => 'MYSeedMorphology',
            'type' => 'DatabaseSelect',
            'options' => array(
                'empty_option' => 'Select',
                'field' => 'MY.seedMorphology'
            )
        ));



        $this->add(array(
            'name' => 'MYMeasurement',
            'type' => 'FieldSet'
        ));

        $this->add(array(
            'name' => 'MYSex',
            'type' => 'DatabaseSelect',
            'options' => array(
                'empty_option' => 'Select',
                'field' => 'MY.sex'
            )
        ));

        $this->add(array(
            'name' => 'MYRecordBasis',
            'type' => 'DatabaseSelect',
            'options' => array(
                'empty_option' => 'Select',
                'field' => 'MY.recordBasis'
            ),
        ));

        $this->add(array(
            'name' => 'MYRecordParts',
            'type' => 'DatabaseSelect',
            'attributes' => array(
                'multiple' => true,
            ),
            'options' => array(
                'field' => 'MY.recordParts',
                'template' => 'kotka/partial/element/select2',
            )
        ));

        $this->add(array(
            'name' => 'MYIdentification',
            'options' => array(
                'count' => 1,
                'should_create_template' => false,
                'allow_add' => true,
                'allow_remove' => true,
                'template_placeholder' => '__identification__',
            ),
        ));

        $this->add(array(
            'name' => 'MYTypeSpecimen',
            'options' => array(
                'count' => 0,
                'should_create_template' => false,
                'allow_add' => true,
                'allow_remove' => true,
                'template_placeholder' => '__type__',
            ),
        ));

        $this->add(array(
            'name' => 'MFSample',
            'options' => array(
                'count' => 0,
                'should_create_template' => false,
                'allow_add' => true,
                'allow_remove' => true,
                'template_placeholder' => '__sample__',
            ),
        ));

        $this->add(array(
            'name' => 'MYWeightInGrams',
            'type' => 'ArrayCollection',
            'attributes' => array(
                'id' => 'weightInGrams'
            ),
            'options' => array(
                'count' => 1,
                'should_create_template' => true,
                'allow_add' => true,
                'allow_remove' => true,
                'target_element' => array(
                    'name' => 'MYWeight',
                    'type' => 'text',
                    'attributes' => array(
                        'class' => 'text-right'
                    ),
                    'options' => array(
                        'suffix' => 'g&nbsp;&nbsp;&nbsp;&nbsp;',
                        'template' => 'kotka/partial/element/input-group'
                    )
                )
            )
        ));

        $this->add(array(
            'name' => 'MYWingInMillimeters',
            'type' => 'ArrayCollection',
            'attributes' => array(
                'id' => 'wingInMillimeters'
            ),
            'options' => array(
                'count' => 1,
                'should_create_template' => true,
                'allow_add' => true,
                'allow_remove' => true,
                'target_element' => array(
                    'name' => 'MYWing',
                    'type' => 'text',
                    'attributes' => array(
                        'class' => 'text-right'
                    ),
                    'options' => array(
                        'suffix' => 'mm',
                        'template' => 'kotka/partial/element/input-group'
                    )
                )
            )
        ));

        $this->add(array(
            'name' => 'MYBeakInMillimeters',
            'type' => 'ArrayCollection',
            'attributes' => array(
                'id' => 'beakInMillimeters'
            ),
            'options' => array(
                'count' => 1,
                'should_create_template' => true,
                'allow_add' => true,
                'allow_remove' => true,
                'target_element' => array(
                    'name' => 'MYBeak',
                    'type' => 'text',
                    'attributes' => array(
                        'class' => 'text-right'
                    ),
                    'options' => array(
                        'suffix' => 'mm',
                        'template' => 'kotka/partial/element/input-group'
                    )
                )
            )
        ));

        $this->add(array(
            'name' => 'MYGonadInMillimeters',
            'type' => 'ArrayCollection',
            'attributes' => array(
                'id' => 'gonadInMillimeters'
            ),
            'options' => array(
                'count' => 1,
                'should_create_template' => true,
                'allow_add' => true,
                'allow_remove' => true,
                'target_element' => array(
                    'name' => 'MYGonad',
                    'type' => 'text',
                    'attributes' => array(
                        'class' => 'text-right'
                    ),
                    'options' => array(
                        'suffix' => 'mm',
                        'template' => 'kotka/partial/element/input-group'
                    )
                )
            )
        ));

        $this->add(array(
            'name' => 'MYLengthInMillimeters',
            'type' => 'ArrayCollection',
            'attributes' => array(
                'id' => 'lengthInMillimeters'
            ),
            'options' => array(
                'count' => 1,
                'should_create_template' => true,
                'allow_add' => true,
                'allow_remove' => true,
                'target_element' => array(
                    'name' => 'MYLength',
                    'type' => 'text',
                    'attributes' => array(
                        'class' => 'text-right'
                    ),
                    'options' => array(
                        'suffix' => 'mm',
                        'template' => 'kotka/partial/element/input-group'
                    )
                )
            )
        ));

        $this->add(array(
            'name' => 'MYTailInMillimeters',
            'type' => 'ArrayCollection',
            'attributes' => array(
                'id' => 'tailInMillimeters'
            ),
            'options' => array(
                'count' => 1,
                'should_create_template' => true,
                'allow_add' => true,
                'allow_remove' => true,
                'target_element' => array(
                    'name' => 'MYTail',
                    'type' => 'text',
                    'attributes' => array(
                        'class' => 'text-right'
                    ),
                    'options' => array(
                        'suffix' => 'mm',
                        'template' => 'kotka/partial/element/input-group'
                    )
                )
            )
        ));

        $this->add(array(
            'name' => 'MYPrimarySpecimen',
            'type' => 'DatabaseSelect',
            'options' => array(
                'field' => 'boolean',
                'empty_option' => 'Select',
                'value_options' => array(
                    'true' => 'Yes',
                    'false' => 'No'
                ),
            ),
        ));

        $this->add(array(
            'name' => 'MYAnkleInMillimeters',
            'type' => 'ArrayCollection',
            'attributes' => array(
                'id' => 'ankleInMillimeters'
            ),
            'options' => array(
                'count' => 1,
                'should_create_template' => true,
                'allow_add' => true,
                'allow_remove' => true,
                'target_element' => array(
                    'name' => 'Ankle',
                    'type' => 'text',
                    'attributes' => array(
                        'class' => 'text-right'
                    ),
                    'options' => array(
                        'suffix' => 'mm',
                        'template' => 'kotka/partial/element/input-group'
                    )
                )
            )
        ));

        $this->add(array(
            'name' => 'MYIsPartOf',
            'type' => 'hidden',
        ));

        $this->add(array(
            'name' => 'MZPublicityRestrictions',
            'type' => 'hidden',
        ));

        $this->add(array(
          'name' => 'MYStratigraphyVerbatim',
          'type' => 'text',
        ));
    }
}
