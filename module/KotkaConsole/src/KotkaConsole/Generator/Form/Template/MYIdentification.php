<?php

namespace KotkaConsole\Generator\Form\Template;

/**
 * Class MYIdentification is used within the specimen form
 * @package KotkaConsole\Generator\Form\Template
 */
class MYIdentification extends AbstractTemplate
{
    protected $base = 'Kotka\Form\MYIdentification';
    protected $hydrator = null;

    /**
     * Creates the form with all the necessary fields in it
     */
    public function __construct()
    {

        $this->add(array(
            'name' => 'MYInfraRank',
            'type' => 'DatabaseSelect',
            'options' => array(
                'empty_option' => 'Rank',
                'field' => 'MY.infraRank'
            ),
        ));

        $this->add(array(
            'name' => 'MYPreferredIdentification',
            'type' => 'select',
            'options' => array(
                'empty_option' => 'Select',
                'value_options' => array(
                    'yes' => 'Yes',
                    'no' => 'No',
                )
            )
        ));

        $this->add(array(
            'name' => 'MYTaxonRank',
            'type' => 'Kotka\Form\Element\TaxonRank',
            'options' => array(
                'empty_option' => 'Rank',
                'field' => 'MY.taxonRank'
            ),
            'attributes' => array(
                'class' => 'taxonRankSelect form-control'
            )
        ));

        $this->add(array(
            'name' => 'MYTaxon',
            'type' => 'text',
            'attributes' => array(
                'class' => 'taxonInputField form-control',
                'placeholder' => 'name'
            ),
        ));

        $this->add(array(
            'name' => 'MYAuthor',
            'type' => 'text',
            'attributes' => array(
                'class' => 'authorInput form-control',
                'placeholder' => 'author'
            ),
        ));
        $this->add(array(
            'name' => 'MYInfraEpithet',
            'type' => 'text',
            'attributes' => array(
                'placeholder' => 'name'
            )
        ));

        $this->add(array(
            'name' => 'MYInfraAuthor',
            'type' => 'text',
            'attributes' => array(
                'class' => 'authorInput',
                'placeholder' => 'author'
            ),
        ));

        $this->add(array(
            'name' => 'MYDet',
            'type' => 'text',
            'options' => array(
                'template' => 'kotka/partial/element/add-me',
                'add-me' => array(
                    'name' => 'this',
                )
            ),
        ));

        $this->add(array(
            'name' => 'MYDetDate',
            'type' => 'text',
            'attributes' => array(
                'class' => 'dateInput',
                'placeholder' => 'date',
                'data-date-format' => 'DD.MM.YYYY',
                'data-date-autoclose' => 'true',
                'data-date-keep-invalid' => 'true',
                'data-date-use-strict' => 'true',
                'data-date-max-date' => 'now',
                'data-date-use-current' => 'day',
            ),
            'options' => array(
                'template' => 'kotka/partial/element/datetime-picker',
            )
        ));

        $this->add(array(
            'name' => 'MYIdentificationNotes',
            'type' => 'textarea',
        ));

        $this->add(array(
            'name' => 'MYAssociatedObservationTaxa',
            'type' => 'hidden',
        ));

        $this->add(array(
            'name' => 'MYIsPartOf',
            'type' => 'hidden',
        ));

        $this->add(array(
            'name' => 'MZPublicityRestrictions',
            'type' => 'hidden',
        ));

    }
}
