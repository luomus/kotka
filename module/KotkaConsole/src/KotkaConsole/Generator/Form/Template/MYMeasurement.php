<?php

namespace KotkaConsole\Generator\Form\Template;

/**
 * Class MYMeasurement
 * @package KotkaConsole\Generator\Form\Template
 */
class MYMeasurement extends AbstractTemplate
{

    protected $base = 'Kotka\Form\MYMeasurement';
    protected $hydrator = null;

    protected $template = array(
        TemplateInterface::DEFAULT_ELEM => 'auto'
    );

    public function __construct()
    {
        $this->add(array(
            'name' => 'MYIsPartOf',
            'type' => 'hidden',
        ));
    }


}
