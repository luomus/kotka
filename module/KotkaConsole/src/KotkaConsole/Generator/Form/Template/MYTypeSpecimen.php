<?php

namespace KotkaConsole\Generator\Form\Template;

/**
 * Class TypeFieldset is the form used for specifying units type
 * @package KotkaConsole\Generator\Form\Template
 */
class MYTypeSpecimen extends AbstractTemplate
{
    protected $base = 'Kotka\Form\MYTypeSpecimen';
    protected $hydrator = null;

    /**
     * Creates the form with all the necessary fields in it
     */
    public function __construct()
    {

        $this->add(array(
            'name' => 'MYTypeStatus',
            'type' => 'DatabaseSelect',
            'options' => array(
                'empty_option' => 'of type',
                'field' => 'MY.typeStatus'
            )
        ));

        $this->add(array(
            'name' => 'MYTypeVerification',
            'type' => 'DatabaseSelect',
            'options' => array(
                'empty_option' => 'Select',
                'field' => 'MY.typeVerification'
            )
        ));

        $this->add(array(
            'name' => 'MYTypeSpecies',
            'type' => 'text',
            'attributes' => array(
                'placeholder' => 'species name'
            )
        ));
        $this->add(array(
            'name' => 'MYTypeSubspecies',
            'type' => 'text',
            'attributes' => array(
                'class' => 'indented',
                'placeholder' => 'ssp name'
            ),
        ));
        $this->add(array(
            'name' => 'MYTypeAuthor',
            'type' => 'text',
            'attributes' => array(
                'placeholder' => 'author'
            )
        ));
        $this->add(array(
            'name' => 'MYTypeSubspeciesAuthor',
            'type' => 'text',
            'attributes' => array(
                'placeholder' => 'author'
            )
        ));
        $this->add(array(
            'name' => 'MYTypifDate',
            'type' => 'text',
            'attributes' => array(
                'class' => 'dateInput',
                'placeholder' => 'date',
                'data-date-format' => 'DD.MM.YYYY',
                'data-date-autoclose' => 'true',
                'data-date-keep-invalid' => 'true',
                'data-date-use-strict' => 'true',
                'data-date-use-current' => 'day'
            ),
            'options' => array(
                'template' => 'kotka/partial/element/datetime-picker',
            )
        ));
        $this->add(array(
            'name' => 'MYTypeNotes',
            'type' => 'textarea',
        ));

        $this->add(array(
            'name' => 'MYIsPartOf',
            'type' => 'hidden',
        ));

        $this->add(array(
            'name' => 'MZPublicityRestrictions',
            'type' => 'hidden',
        ));
    }
}
