<?php

namespace KotkaConsole\Generator\Form\Template;


abstract class AbstractTemplate implements TemplateInterface
{

    protected $base = 'Kotka\Form\WarningForm';

    protected $hydrator = 'Kotka\Stdlib\Hydrator\Reflection';

    protected $template = array(
        TemplateInterface::DEFAULT_ELEM => array(
            'type' => 'text',
        )
    );

    public function get($key, $namespace = '')
    {
        if (isset($this->template[$key])) {
            return $this->template[$key];
        }
        if (!isset($this->template[$key])) {
            throw new \InvalidArgumentException('Could not find specks for key ' . $key);
        }

        return $this->template[$key];
    }

    public function has($key, $namespace = '')
    {
        if (isset($this->template[$key])) {
            return true;
        }

        return isset($this->template[$key]);
    }

    public function add(Array $elem)
    {
        if (!isset($elem['name'])) {
            throw new \InvalidArgumentException('Name is required in template');
        }
        $this->template[$elem['name']] = $elem;
    }

    public function remove($key)
    {
        if (!isset($this->template[$key])) {
            throw new \InvalidArgumentException('Could not find specks for key ' . $key);
        }
        unset($this->template[$key]);
    }

    public function getBase()
    {
        return $this->base;
    }

    public function getHydrator()
    {
        return $this->hydrator;
    }

    public function getAll()
    {
        return $this->template;
    }
}