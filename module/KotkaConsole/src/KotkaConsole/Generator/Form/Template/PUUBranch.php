<?php

namespace KotkaConsole\Generator\Form\Template;

/**
 * Class PUUBranch
 * @package KotkaConsole\Generator\Form\Template
 */
class PUUBranch extends Base
{

    protected $base = 'Kotka\Form\PUUBranch';

    public function __construct()
    {

        $this->add(array(
            'name' => 'PUUAccessionID',
            'type' => 'hidden',
        ));

        $this->add(array(
            'name' => 'PUUExists',
            'type' => 'checkbox',
        ));

        $this->add(array(
            'name' => 'PUUCollectionID',
            'type' => 'DatabaseSelect',
            'options' => array(
                'empty_option' => 'Select',
                'field' => 'MY.collectionID-GardenArea',
                'template' => 'kotka/partial/element/select2'
            )
        ));

        $this->add(array(
            'name' => 'PUUNotes',
            'type' => 'textarea'
        ));

        $this->add(array(
          'name' => 'PUUWgs84Latitude',
          'type' => 'text'
        ));

        $this->add(array(
          'name' => 'PUUWgs84Longitude',
          'type' => 'text'
        ));

        $this->add(array(
            'name' => 'PUUGardenLocationNotes',
            'type' => 'textarea'
        ));
    }
}
