<?php

namespace KotkaConsole\Generator\Form\Template;

use Kotka\Model\Util;
use Kotka\Validator\DateCompare;
use Zend\Validator\NotEmpty;

/**
 * Class HRATransaction
 * @package KotkaConsole\Generator\Form\Template
 */
class HRATransaction extends Base
{
    const DATE_FORMAT_JS = 'DD.MM.YYYY';
    const DATE_FORMAT = 'd.m.Y';

    protected $base = 'Kotka\Form\HRATransaction';

    /**
     * Creates the form with all the necessary fields in it
     */
    public function __construct()
    {
        parent::__construct();

        $this->add(array(
            'name' => 'HRAAvailableForGeneticResearch',
            'type' => 'DatabaseSelect',
            'options' => array(
                'empty_option' => 'Select',
                'field' => 'HRA.availableForGeneticResearch'
            ),
        ));

        $this->add(array(
            'name' => 'HRATransactionStatus',
            'type' => 'DatabaseRadio',
            'options' => array(
                'empty_option' => 'Select',
                'field' => 'HRA.transactionStatus',
                'label_attributes' => array(
                    'class' => 'btn btn-default'
                )
            ),
        ));

        $this->add(array(
            'name' => 'HRACollectionID',
            'type' => 'DatabaseSelect',
            'attributes' => array(
                'id' => 'collectionID'
            ),
            'options' => array(
                'empty_option' => 'Select',
                'field' => 'HRA.collectionID',
                'template' => 'kotka/partial/element/select2'
            )
        ));

        $this->add(array(
            'name' => 'HRAResourceImported',
            'type' => 'DatabaseSelect',
            'options' => array(
                'empty_option' => 'Select',
                'field' => 'HRA.resourceImported'
            ),
        ));
        $this->add(array(
            'name' => 'HRAGeneticResourceType',
            'type' => 'DatabaseSelect',
            'options' => array(
                'empty_option' => 'Select',
                'field' => 'HRA.geneticResourceType'
            ),
        ));

        $this->add(array(
            'name' => 'HRATransactionType',
            'type' => 'DatabaseSelect',
            'options' => array(
                'empty_option' => 'Select',
                'field' => 'HRA.transactionType',
            ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'break_chain_on_failure' => true,
                    'options' => array(
                        'messages' => array(
                            NotEmpty::IS_EMPTY => 'Transaction type must be set.'
                        )
                    )
                )
            ),
        ));

        $this->add(array(
            'name' => 'HRACorrespondenceHeaderOrganizationCode',
            'type' => 'select',
            'options' => array(
                'empty_option' => 'Select',
                'value_options' => array(
                    'H' => 'H',
                    'MZH' => 'MZH'
                )
            )
        ));

        $this->add(array(
            'name' => 'HRAGeneticResourceAcquisitionDate',
            'type' => 'date',
            'attributes' => array(
                'id' => 'geneticResourceAcquisitionDate',
                'type' => 'text',
                'data-date-format' => self::DATE_FORMAT_JS,
                'data-date-autoclose' => 'true',
                'data-date-max-date' => 'now',
                'data-date-use-current' => 'day'
            ),
            'options' => array(
                'format' => self::DATE_FORMAT,
                'template' => 'kotka/partial/element/datetime-picker',
            )
        ));

        $this->add(array(
            'name' => 'HRATransactionReturned',
            'type' => 'date',
            'attributes' => array(
                'id' => 'geneticResourceAcquisitionDate',
                'type' => 'text',
                'data-date-format' => self::DATE_FORMAT_JS,
                'data-date-autoclose' => 'true',
                'data-date-max-date' => 'now',
                'data-date-use-current' => 'day'
            ),
            'options' => array(
                'format' => self::DATE_FORMAT,
                'template' => 'kotka/partial/element/datetime-picker',
            )
        ));

        $this->add(array(
            'name' => 'HRAPICStartDate',
            'type' => 'date',
            'attributes' => array(
                'id' => 'PICStartDate',
                'type' => 'text',
                'data-date-format' => self::DATE_FORMAT_JS,
                'data-date-autoclose' => 'true',
                'data-date-use-current' => 'day'
            ),
            'options' => array(
                'format' => self::DATE_FORMAT,
                'template' => 'kotka/partial/element/datetime-picker',
            )
        ));

        $this->add(array(
            'name' => 'HRAPICEndDate',
            'type' => 'date',
            'attributes' => array(
                'id' => 'PICEndDate',
                'type' => 'text',
                'data-date-format' => self::DATE_FORMAT_JS,
                'data-date-autoclose' => 'true',
                'data-date-use-current' => 'day'
            ),
            'options' => array(
                'format' => self::DATE_FORMAT,
                'template' => 'kotka/partial/element/datetime-picker',
            )
        ));

        $this->add(array(
            'name' => 'HRATransactionRequestReceived',
            'type' => 'date',
            'attributes' => array(
                'id' => 'transactionRequestReceived',
                'type' => 'text',
                'data-date-format' => self::DATE_FORMAT_JS,
                'data-date-autoclose' => 'true',
                'data-date-max-date' => 'now',
                'data-date-use-current' => 'day',
                'required' => true
            ),
            'options' => array(
                'format' => self::DATE_FORMAT,
                'template' => 'kotka/partial/element/datetime-picker',
            ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'break_chain_on_failure' => true,
                    'options' => array(
                        'messages' => array(
                            NotEmpty::IS_EMPTY => 'Transaction request received date must be set.'
                        )
                    )
                ),
                array(
                    'name' => 'DateCompare',
                    'options' => array(
                        'format' => Util::DATETIME_FORMAT,
                        'direction' => DateCompare::BEFORE,
                    )
                ),
            )
        ));

        $this->add(array(
            'name' => 'HRAAvailableForGeneticResearchNotes',
            'type' => 'textarea',
        ));

        $this->add(array(
            'name' => 'HRAGeneticResourceDescription',
            'type' => 'textarea',
        ));

        $this->add(array(
            'name' => 'HRAMaterialEn',
            'type' => 'textarea',
        ));


        $this->add(array(
            'name' => 'HRAPublicRemarksEn',
            'type' => 'textarea',
        ));

        $this->add(array(
            'name' => 'HRAGeneticResourceRightsAndObligations',
            'type' => 'textarea',
        ));

        $this->add(array(
            'name' => 'HRAInternalRemarksEn',
            'type' => 'textarea',
        ));

        $this->add(array(
            'name' => 'HRASentType',
            'type' => 'DatabaseSelect',
            'options' => array(
                'empty_option' => 'Select',
                'field' => 'HRA.sentType',
            ),
        ));

        $this->add(array(
            'name' => 'HRAUnderNagoya',
            'type' => 'DatabaseSelect',
            'options' => array(
                'field' => 'boolean',
                'empty_option' => 'Select',
                'value_options' => array(
                    'true' => 'Yes',
                    'false' => 'No'
                ),
            ),
        ));

        $this->add(array(
            'name' => 'HRATransactionSent',
            'type' => 'date',
            'attributes' => array(
                'id' => 'transactionSent',
                'type' => 'text',
                'data-date-format' => self::DATE_FORMAT_JS,
                'data-date-autoclose' => 'true',
                'data-date-max-date' => 'now',
                'data-date-use-current' => 'day',
            ),
            'options' => array(
                'format' => self::DATE_FORMAT,
                'template' => 'kotka/partial/element/datetime-picker',
            )
        ));

        $this->add(array(
            'name' => 'HRADueDate',
            'type' => 'date',
            'attributes' => array(
                'id' => 'dueDate',
                'type' => 'text',
                'data-date-format' => self::DATE_FORMAT_JS,
                'data-date-autoclose' => 'true',
                'data-date-max-date' => 'false',
            ),
            'options' => array(
                'format' => self::DATE_FORMAT,
                'template' => 'kotka/partial/element/datetime-picker',
                'add-to' => '#transactionSent'
            )
        ));

        $this->add(array(
            'name' => 'HRAReceiptReturned',
            'type' => 'date',
            'attributes' => array(
                'id' => 'receiptReturned',
                'type' => 'text',
                'data-date-format' => self::DATE_FORMAT_JS,
                'data-date-autoclose' => 'true',
                'data-date-max-date' => 'now',
                'data-date-use-current' => 'day',
            ),
            'options' => array(
                'format' => self::DATE_FORMAT,
                'template' => 'kotka/partial/element/datetime-picker',
            )
        ));

        $this->add(array(
            'name' => 'HRACorrespondentOrganization',
            'type' => 'DatabaseSelect',
            'options' => array(
                'template' => 'kotka/partial/element/select2',
                'empty_option' => 'Select',
                'field' => 'HRA.correspondentOrganization',
            ),
            'attributes' => array(
                'id' => 'correspondentOrganization',
                'required' => true
            ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'break_chain_on_failure' => true,
                    'options' => array(
                        'messages' => array(
                            NotEmpty::IS_EMPTY => 'Choose correspondent organization.'
                        )
                    )
                ),
            )
        ));

        $this->add(array(
            'name' => 'HRACorrespondentPerson',
            'type' => 'textarea',
        ));

        $this->add(array(
            'name' => 'HRALocalDepartment',
            'type' => 'DatabaseSelect',
            'options' => array(
                'empty_option' => 'Select',
                'field' => 'HRA.localDepartment',
            ),
        ));

        $this->add(array(
            'name' => 'HRALocalPerson',
            'type' => 'text',
            'options' => array(
                'template' => 'kotka/partial/element/add-me',
                'add-me' => array(
                    'name' => '.HRALocalPerson',
                    'email' => '.HRALocalPersonEmail'
                )
            ),
        ));

        $this->add(array(
            'name' => 'HRAIds',
            'type' => 'textarea',
            'attributes' => array(
                'id' => 'ids',
                'rows' => 3
            ),
        ));

        $this->add(array(
            'name' => 'HRBAway',
            'type' => 'hidden',
        ));

        $this->add(array(
            'name' => 'HRBReturned',
            'type' => 'hidden',
        ));

        $this->add(array(
            'name' => 'HRBMissing',
            'type' => 'hidden',
        ));

        $this->add(array(
            'name' => 'HRBDamaged',
            'type' => 'hidden',
        ));

        $this->add(array(
            'name' => 'HRBAwayOther',
            'type' => 'number',
        ));

        $this->add(array(
            'name' => 'HRBReturnedOther',
            'type' => 'number',
        ));

        $this->add(array(
            'name' => 'HRBMissingOther',
            'type' => 'number',
        ));

        $this->add(array(
            'name' => 'HRBDamagedOther',
            'type' => 'number',
        ));

        $this->add(array(
            'name' => 'MZPublicityRestrictions',
            'type' => 'hidden',
        ));

        $this->add(array(
            'name' => 'HRAPermit',
            'options' => array(
                'count' => 1,
                'should_create_template' => false,
                'allow_add' => true,
                'allow_remove' => true,
                'template_placeholder' => '__permit__',
            ),
        ));

        $this->add(array(
            'name' => 'HRAGeneticResourceAcquisitionCountry',
            'type' => 'DatabaseSelect',
            'attributes' => array(
                'id' => 'geneticResourceAcquisitionCountry'
            ),
            'options' => array(
                'empty_option' => 'Select',
                'field' => 'country2Alpha',
                'template' => 'kotka/partial/element/select2'
            )
        ));
    }
}
