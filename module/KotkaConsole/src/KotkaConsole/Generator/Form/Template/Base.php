<?php

namespace KotkaConsole\Generator\Form\Template;

/**
 * Class Base contains the basic items needed in form
 * @package KotkaConsole\Generator\Form\Template
 */
class Base extends Subject
{
    public function __construct()
    {
        parent::__construct();

        $this->add(array(
            'name' => 'MZEditor',
        ));

        $this->add(array(
            'name' => 'MZOwner',
            'required' => true,
            'type' => 'OwnerOrganization',
            'options' => array(
                'empty_option' => 'Select',
                'disable_inarray_validator' => true,
            ),
            'attributes' => array(
                'class' => 'form-control'
            )
        ));
    }
}
