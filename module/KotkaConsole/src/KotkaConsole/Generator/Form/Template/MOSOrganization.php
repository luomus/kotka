<?php

namespace KotkaConsole\Generator\Form\Template;

/**
 * Class MOSOrganization
 * @package KotkaConsole\Generator\Form\Template
 */
class MOSOrganization extends Base
{
    protected $base = 'Kotka\Form\MOSOrganization';

    /**
     * Creates the form with all the necessary fields in it
     */
    public function __construct()
    {
        parent::__construct();

        $this->add(array(
            'name' => 'MOSDatasetID',
            'type' => 'DatabaseSelect',
            'attributes' => array(
                'id' => 'datasetID',
                'multiple' => true,
            ),
            'options' => array(
                'field' => 'MOS.datasetID',
                'template' => 'kotka/partial/element/select2',
            )
        ));

        $this->add(array(
            'name' => 'MOSOrganizationLevel1En',
            'type' => 'text',
            'options' => array(
                'template' => 'kotka/partial/element/select2',
                'source' => '/picker/string/organizations',
                'allow-new' => true,
            ),
            'attributes' => array(
                'id' => 'MOSOrganizationLevel1',
            ),
        ));

        $this->add(array(
            'name' => 'MOSAbbreviationExplanation',
            'type' => 'DatabaseSelect',
            'options' => array(
                'field' => 'MOS.abbreviationExplanation',
                'empty_option' => 'Select',
            ),
        ));

        $this->add(array(
            'name' => 'MOSHidden',
            'type' => 'DatabaseSelect',
            'options' => array(
                'field' => 'boolean',
                'empty_option' => '',
            ),
        ));

        $this->add(array(
            'name' => 'MOSDateOrdersDue',
            'type' => 'KotkaDate',
            'attributes' => array(
                'id' => 'dateOrdersDue',
                'type' => 'text',
                'data-date-format' => 'DD.MM.YYYY',
                'data-date-autoclose' => 'true',
                'data-date-end-date' => 'now',
                'data-date-force-parse' => 'false'
            ),
            'options' => array(
                'format' => HRATransaction::DATE_FORMAT,
                'template' => 'kotka/partial/element/datetime-picker',
            )
        ));

        $this->add(array(
          'name' => 'MOSCourierAddress',
          'type' => 'textarea'
        ));
    }

}
