<?php

namespace KotkaConsole\Generator\Form\Template;

/**
 * Class Subject
 * @package KotkaConsole\Generator\Form\Template
 */
class Subject extends AbstractTemplate
{
    public function __construct()
    {
        $this->add(array(
            'name' => 'subject',
            'type' => 'hidden',
        ));
    }
}
