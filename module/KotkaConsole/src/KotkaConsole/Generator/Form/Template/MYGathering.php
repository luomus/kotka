<?php

namespace KotkaConsole\Generator\Form\Template;

use Kotka\Model\Util;

/**
 * Class MYGathering
 * @package KotkaConsole\Generator\Form\Template
 */
class MYGathering extends AbstractTemplate
{

    protected $base = 'Kotka\Form\MYGathering';
    protected $hydrator = null;

    public function __construct()
    {

        $this->add(array(
            'name' => 'MYLeg',
            'type' => 'ArrayCollection',
            'attributes' => array(
                'id' => 'leg'
            ),
            'options' => array(
                'count' => 1,
                'should_create_template' => true,
                'allow_add' => true,
                'allow_remove' => true,
                'target_element' => array(
                    'name' => 'leg',
                    'type' => 'text',
                )
            )
        ));

        $this->add(array(
            'name' => 'MYIsPartOf',
            'type' => 'hidden',
        ));

        $this->add(array(
            'name' => 'MZPublicityRestrictions',
            'type' => 'hidden',
        ));

        $this->add(array(
            'name' => 'MYCoordinateNotes',
            'type' => 'textarea',
            'attributes' => array(
                'style' => 'min-height: 46px;'
            )
        ));

        $this->add(array(
            'name' => 'MYLocalityVerbatim',
            'type' => 'textarea',
        ));

        $this->add(array(
            'name' => 'MYLocalityDescription',
            'type' => 'textarea',
        ));

        $this->add(array(
            'name' => 'MYHabitatDescription',
            'type' => 'textarea',
        ));

        $this->add(array(
            'name' => 'MYNotes',
            'type' => 'textarea',
        ));

        $this->add(array(
            'name' => 'MYCoordinateSystem',
            'type' => 'DatabaseSelect',
            'attributes' => array(
                'id' => 'coordinateSystem'
            ),
            'options' => array(
                'empty_option' => 'Select',
                'field' => 'MY.coordinateSystem'
            ),
        ));

        $this->add(array(
            'name' => 'MYForestVegetationZone',
            'type' => 'DatabaseSelect',
            'attributes' => array(
                'id' => 'coordinateSystem'
            ),
            'options' => array(
                'empty_option' => 'Select',
                'field' => 'MY.forestVegetationZone'
            ),
        ));

        $this->add(array(
            'name' => 'MYCoordinateSource',
            'type' => 'DatabaseSelect',
            'attributes' => array(
                'id' => 'coordSource'
            ),
            'options' => array(
                'empty_option' => 'Select',
                'field' => 'MY.coordinateSource'
            )
        ));

        $this->add(array(
            'name' => 'MYGeoreferenceSource',
            'type' => 'DatabaseSelect',
            'attributes' => array(
                'id' => 'georeferenceSource'
            ),
            'options' => array(
                'empty_option' => 'Select',
                'field' => 'MY.georeferenceSource'
            ),
        ));

        $this->add(array(
            'name' => 'MYWgs84Latitude',
            'type' => 'hidden',
        ));

        $this->add(array(
            'name' => 'MYWgs84Longitude',
            'type' => 'hidden',
        ));

        $this->add(array(
            'name' => 'MYDateBegin',
            'type' => 'text',
            'attributes' => array(
                'data-date-format' => 'DD.MM.YYYY',
                'data-date-autoclose' => 'true',
                'data-date-max-date' => 'now',
                'data-date-keep-invalid' => 'true',
                'data-date-use-strict' => 'true',
                'data-date-force-parse' => 'false',
                'placeholder' => 'begin',
            ),
            'options' => array(
                'format' => Util::DATETIME_FORMAT,
                'template' => 'kotka/partial/element/datetime-picker',
            ),
        ));

        $this->add(array(
            'name' => 'MYDateEnd',
            'type' => 'text',
            'attributes' => array(
                'id' => 'dateEnd',
                'data-date-format' => 'DD.MM.YYYY',
                'data-date-max-date' => 'now',
                'data-date-keep-invalid' => 'true',
                'data-date-use-strict' => 'true',
                'data-date-autoclose' => 'true',
                'data-date-force-parse' => 'false',
                'placeholder' => 'end',
            ),
            'options' => array(
                'format' => Util::DATETIME_FORMAT,
                'template' => 'kotka/partial/element/datetime-picker',
            ),
        ));

        $this->add(array(
            'name' => 'MYSamplingMethod',
            'type' => 'DatabaseSelect',
            'options' => array(
                'empty_option' => 'Select',
                'field' => 'MY.samplingMethod'
            ),
        ));

        $this->add(array(
            'name' => 'MYUnit',
            'options' => array(
                'template' => 'kotka/specimens/unit',
                'count' => 1,
                'should_create_template' => false,
                'allow_add' => true,
                'allow_remove' => true,
                'template_placeholder' => '__unit__',
            ),
        ));
    }
}
