<?php

namespace KotkaConsole\Generator\Form\Template;

/**
 * Class MYDocument
 * @package KotkaConsole\Generator\Form\Template
 */
class MYDocument extends Base
{

    protected $base = 'Kotka\Form\MYDocument';

    public function __construct()
    {
        parent::__construct();
        $this->add(array(
            'name' => 'MYDatatype',
            'type' => 'hidden',
        ));

        $this->add(array(
            'name' => 'MYEditor',
            'type' => 'hidden',
        ));

        $this->add(array(
            'name' => 'MYEntered',
            'type' => 'hidden',
        ));

        $this->add(array(
            'name' => 'MYCollectionID',
            'type' => 'DatabaseSelect',
            'attributes' => array(
                'id' => 'collectionID',
                'required' => true
            ),
            'options' => array(
                'empty_option' => 'Select',
                'field' => 'MY.collectionID',
                'template' => 'kotka/partial/element/select2'
            )
        ));

        $this->add(array(
            'name' => 'MYDatasetID',
            'type' => 'DatabaseSelect',
            'attributes' => array(
                'id' => 'datasetID',
                'multiple' => true,
            ),
            'options' => array(
                'field' => 'MY.datasetID',
                'template' => 'kotka/partial/element/select2',
            )
        ));

        $this->add(array(
            'name' => 'MYAdditionalIDs',
            'type' => 'ArrayCollection',
            'attributes' => array(
                'id' => 'additionalIDs'
            ),
            'options' => array(
                'count' => 1,
                'should_create_template' => true,
                'allow_add' => true,
                'allow_remove' => true,
                'target_element' => array(
                    'name' => 'MYAdditionalIDs',
                    'type' => 'text',
                )
            )
        ));

        $this->add(array(
            'name' => 'MYBold',
            'type' => 'ArrayCollection',
            'options' => array(
                'count' => 1,
                'should_create_template' => true,
                'allow_add' => true,
                'allow_remove' => true,
                'target_element' => array(
                    'name' => 'MYBold',
                    'type' => 'text',
                )
            )
        ));

        $this->add(array(
            'name' => 'MYGenbank',
            'type' => 'ArrayCollection',
            'options' => array(
                'count' => 1,
                'should_create_template' => true,
                'allow_add' => true,
                'allow_remove' => true,
                'target_element' => array(
                    'name' => 'MYGenbank',
                    'type' => 'text',
                )
            )
        ));

        $this->add(array(
            'name' => 'MYPublication',
            'type' => 'ArrayCollection',
            'attributes' => array(
                'id' => 'publication'
            ),
            'options' => array(
                'count' => 1,
                'should_create_template' => true,
                'allow_add' => true,
                'allow_remove' => true,
                'target_element' => array(
                    'name' => 'MYPublication',
                    'type' => 'text',
                )
            )
        ));

        $this->add(array(
            'name' => 'MYSeparatedTo',
            'type' => 'ArrayCollection',
            'attributes' => array(
                'id' => 'separatedTo'
            ),
            'options' => array(
                'count' => 1,
                'should_create_template' => true,
                'allow_add' => true,
                'allow_remove' => true,
                'target_element' => array(
                    'name' => 'MYSeparatedTo',
                    'type' => 'text',
                )
            )
        ));

        $this->add(array(
            'name' => 'MYAcquiredFromOrganization',
            'type' => 'DatabaseSelect',
            'options' => array(
                'template' => 'kotka/partial/element/select2',
                'empty_option' => 'Select',
                'field' => 'MY.acquiredFromOrganization',
            ),
        ));

        $this->add(array(
            'name' => 'MYEvent',
            'type' => 'ArrayCollection',
            'options' => array(
                'count' => 1,
                'should_create_template' => true,
                'allow_add' => true,
                'allow_remove' => true,
                'target_element' => array(
                    'name' => 'MYEvent',
                    'type' => 'text',
                )
            )
        ));

        $this->add(array(
            'name' => 'MYUnreliableFields',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 'unreliableFields'
            ),
        ));

        $this->add(array(
            'name' => 'MYPrivateNotes',
            'type' => 'textarea'
        ));

        $this->add(array(
            'name' => 'MYNotes',
            'type' => 'textarea',
        ));

        $this->add(array(
            'name' => 'MYEditNotes',
            'type' => 'textarea',
        ));

        $this->add(array(
            'name' => 'MYTranscriberNotes',
            'type' => 'textarea',
        ));

        $this->add(array(
            'name' => 'MYLabelsVerbatim',
            'type' => 'textarea',
        ));

        $this->add(array(
            'name' => 'MYRawOCRData',
            'type' => 'textarea',
        ));
        $this->add(array(
          'name' => 'MYObservationID',
          'type' => 'text',
        ));
        $this->add(array(
            'name' => 'MYNamespaceID',
            'type' => 'text',
        ));

        $this->add(array(
            'name' => 'MYObjectID',
            'type' => 'text',
        ));


        $this->add(array(
            'name' => 'MYStatus',
            'type' => 'DatabaseSelect',
            'options' => array(
                'empty_option' => 'Select',
                'field' => 'MY.status'
            )
        ));

        $this->add(array(
            'name' => 'MYVerificationStatus',
            'type' => 'DatabaseSelect',
            'attributes' => array(
                'id' => 'MYVerificationStatus',
                'multiple' => true,
            ),
            'options' => array(
                'field' => 'MY.verificationStatus',
                'template' => 'kotka/partial/element/select2',
            )
        ));

        $this->add(array(
            'name' => 'MYPreservation',
            'type' => 'DatabaseSelect',
            'attributes' => array(
                'multiple' => true,
            ),
            'options' => array(
                'template' => 'kotka/partial/element/select2',
                'field' => 'MY.preservation'
            )
        ));

        $this->add(array(
            'name' => 'MZPublicityRestrictions',
            'type' => 'DatabaseSelect',
            'options' => array(
                'empty_option' => 'Select',
                'field' => 'MZ.publicityRestrictions'
            )
        ));

        $this->add(array(
            'name' => 'MYPublicityRestrictions',
            'type' => 'DatabaseSelect',
            'options' => array(
                'empty_option' => 'Select',
                'field' => 'MY.publicityRestrictions'
            )
        ));

        $this->add(array(
            'name' => 'MYLanguage',
            'type' => 'Language',
            'options' => array(
                'label' => 'Main language of this data',
                'empty_option' => 'Select',
                'help' => '',
            ),
        ));
        $this->add(array(
            'name' => 'MYGathering',
            'options' => array(
                'template' => 'kotka/specimens/gathering',
                'count' => 1,
                'should_create_template' => true,
                'allow_add' => true,
                'allow_remove' => true,
                'template_placeholder' => '__gathering__',
            ),
        ));

        $this->add(array(
            'name' => 'MYDatatype',
            'type' => 'hidden',
        ));

        $this->add(array(
            'name' => 'MYAcquisitionDate',
            'type' => 'KotkaDate',
            'attributes' => array(
                'id' => 'acquisitionDate',
                'type' => 'text',
                'data-date-format' => 'DD.MM.YYYY',
                'data-date-autoclose' => 'true',
                'data-date-end-date' => 'now',
                'data-date-force-parse' => 'false'
            ),
            'options' => array(
                'format' => HRATransaction::DATE_FORMAT,
                'template' => 'kotka/partial/element/datetime-picker',
            )
        ));


        $this->add(array(
            'name' => 'MYRelationship',
            'type' => 'ArrayCollection',
            'options' => array(
                'count' => 1,
                'should_create_template' => true,
                'allow_add' => true,
                'allow_remove' => true,
                'target_element' => array(
                    'name' => 'MYRelationship',
                    'type' => 'PrefixText',
                    'options' => array(
                        'field' => 'MY.relationshipEnum',
                        'value_prefix' => 'MY.relationship'
                    )
                )
            )
        ));

    }
}
