<?php

namespace KotkaConsole\Generator\Form\Template;


interface TemplateInterface
{

    const DEFAULT_ELEM = '__default__';

    public function get($elem);

    public function has($elem);

    public function add(Array $elem);

    public function getBase();

    public function getHydrator();

    public function getAll();

} 