<?php

namespace KotkaConsole\Generator\Form\Template;


/**
 * Class HRBSpecimen
 * @package KotkaConsole\Generator\Form\Template
 */
class HRBSpecimen extends Subject
{

    protected $base = 'Kotka\Form\HRBSpecimen';
    protected $hydrator = null;

    /**
     * Creates the form with all the necessary fields in it
     */
    public function __construct()
    {
        parent::__construct();

        $this->add(array(
            'name' => 'HRBItem',
            'type' => 'hidden'
        ));

        $this->add(array(
            'name' => 'MYIsPartOf',
            'type' => 'hidden'
        ));

        $this->add(array(
            'name' => 'HRBAmount',
            'type' => 'text',
            'attributes' => array(
                'class' => 'text-right'
            )
        ));

        $this->add(array(
            'name' => 'HRBStatus',
            'type' => 'DatabaseSelect',
            'options' => array(
                'empty_option' => 'Select',
                'field' => 'HRB.status',
            ),
        ));

    }
}
