<?php

namespace KotkaConsole\Generator\Form\Template;

/**
 * Class MYCollection
 * @package KotkaConsole\Generator\Form\Template
 */
class MYCollection extends Base
{

    protected $base = 'Kotka\Form\MYCollection';

    /**
     * Creates the form with all the necessary fields in it
     */
    public function __construct()
    {
        parent::__construct();

        $this->add(array(
            'name' => 'MYIsPartOf',
            'type' => 'DatabaseSelect',
            'attributes' => array(
              'id' => "isPartOf"
            ),
            'options' => array(
                'empty_option' => 'Select',
                'field' => 'MY.collectionID',
                'template' => 'kotka/partial/element/select2'
            ),
            'validators' => array(
                array(
                    'name' => 'Cycle',
                    'break_chain_on_failure' => true,
                    'options' => array(
                        'predicate' => 'MY.isPartOf',
                        'messages' => array(
                            \Kotka\Validator\Cycle::BECOMES_CYCLE => 'Value would make a cycle'
                        )
                    )
                )
            )
        ));

        $this->add(array(
            'name' => 'MYCollectionNameEn',
            'type' => 'text',
            'options' => array(
                'label_attributes' => array(
                    'class' => 'mandatory langcode'
                )
            )
        ));

        $this->add(array(
            'name' => 'MYCollectionNameFi',
            'type' => 'text',
            'options' => array(
                'label_attributes' => array(
                    'class' => 'mandatory langcode'
                )
            ),
        ));

        $this->add(array(
            'name' => 'MYConcealmentBasisEn',
            'type' => 'text',
            'options' => array(
                'label_attributes' => array(
                    'class' => 'langcode'
                )
            )
        ));

        $this->add(array(
            'name' => 'MYConcealmentBasisFi',
            'type' => 'text',
            'options' => array(
                'label_attributes' => array(
                    'class' => 'langcode'
                )
            ),
        ));

        $this->add(array(
            'name' => 'MYDataNotes',
            'type' => 'textarea'
        ));
        $this->add(array(
            'name' => 'MYDataUseTermsEn',
            'type' => 'textarea'
        ));

        $this->add(array(
            'name' => 'MYDataUseTermsFi',
            'type' => 'textarea'
        ));

        $this->add(array(
            'name' => 'MYDataQualityDescriptionEn',
            'type' => 'textarea'
        ));

        $this->add(array(
            'name' => 'MYDataQualityDescriptionFi',
            'type' => 'textarea'
        ));



        $this->add(array(
            'name' => 'MYCollectionNameSv',
            'type' => 'text',
            'required' => false,
            'options' => array(
                'label_attributes' => array(
                    'class' => 'langcode'
                )
            )
        ));

        $this->add(array(
            'name' => 'MYCollectionQuality',
            'type' => 'DatabaseSelect',
            'options' => array(
                'empty_option' => 'Select',
                'field' => 'MY.collectionQuality',
            ),
        ));


        $this->add(array(
            'name' => 'MYCollectionType',
            'type' => 'DatabaseSelect',
            'options' => array(
                'empty_option' => 'Select',
                'field' => 'MY.collectionType',
            ),
        ));

        $this->add(array(
            'name' => 'MYDataQuality',
            'type' => 'DatabaseSelect',
            'options' => array(
                'empty_option' => 'Select',
                'field' => 'MY.dataQuality',
            ),
        ));
        $this->add(array(
            'name' => 'MXSecureLevel',
            'type' => 'DatabaseSelect',
            'options' => array(
                'empty_option' => 'Select',
                'field' => 'MX.secureLevel',
            ),
        ));

        $this->add(array(
            'name' => 'MYDescriptionEn',
            'type' => 'textarea',
        ));

        $this->add(array(
            'name' => 'MYDescriptionFi',
            'type' => 'textarea',
        ));

        $this->add(array(
            'name' => 'MYDescriptionSv',
            'type' => 'textarea',
            'required' => false,
            'attributes' => array(
                'id' => 'descriptionSV'
            )
        ));

        $this->add(array(
            'name' => 'MYMethodsEn',
            'type' => 'textarea',
        ));

        $this->add(array(
            'name' => 'MYIntellectualRights',
            'type' => 'DatabaseSelect',
            'options' => array(
                'empty_option' => 'Select',
                'field' => 'MY.intellectualRights',
            )
        ));

        $this->add(array(
            'name' => 'MYIntellectualDescriptionEn',
            'type' => 'textarea',
        ));

        $this->add(array(
            'name' => 'MYPublicationTerms',
            'type' => 'DatabaseSelect',
            'options' => array(
                'empty_option' => 'Select',
                'field' => 'MY.publicationTerms',
            )
        ));

        $this->add(array(
            'name' => 'MYPublicationDescriptionEn',
            'type' => 'textarea',
        ));

        $this->add(array(
            'name' => 'MYConcealmentBasisEn',
            'type' => 'textarea',
        ));

        $this->add(array(
            'name' => 'MYConcealmentBasisFi',
            'type' => 'textarea',
        ));

        $this->add(array(
            'name' => 'MYPersonResponsible',
            'type' => 'text',
            'options' => array(
                'template' => 'kotka/partial/element/add-me',
                'add-me' => array(
                    'name' => '#creator'
                )
            ),
            'attributes' => array(
                'id' => 'creator'
            )
        ));

        $this->add(array(
            'name' => 'MYCitation',
            'type' => 'textarea',
        ));

        $this->add(array(
            'name' => 'MYLanguage',
            'type' => 'Language',
            'options' => array(
                'empty_option' => 'Select'
            ),
        ));

        $this->add(array(
            'name' => 'MYInMustikka',
            'type' => 'select',
            'options' => array(
                'value_options' => array(
                    'true' => 'Yes',
                    'false' => 'No'
                ),
                'empty_option' => 'Select',
            ),
        ));

        $this->add(array(
            'name' => 'MYPublicAccess',
            'type' => 'DatabaseSelect',
            'options' => array(
                'field' => 'boolean',
                'empty_option' => 'Select',
                'value_options' => array(
                    'true' => 'Yes',
                    'false' => 'No'
                ),
            ),
        ));

        $this->add(array(
            'name' => 'MYShareToGbif',
            'type' => 'DatabaseSelect',
            'attributes' => array(
              'id' => "shareToGbif"
            ),
            'options' => array(
                'empty_option' => 'Select',
                'field' => 'MY.collectionID',
                'template' => 'kotka/partial/element/select2'
            ),
        ));

        $this->add(array(
            'name' => 'MYShareToFEO',
            'type' => 'DatabaseSelect',
            'options' => array(
                'field' => 'boolean',
                'empty_option' => 'Select',
                'value_options' => array(
                    'true' => 'Yes',
                    'false' => 'No'
                ),
            ),
        ));

        $this->add(array(
            'name' => 'MYBoundingBoxLatMin',
            'type' => 'text',
            'validators' => array(
                array(
                    'name' => 'Coordinate',
                    'options' => array(
                        'coordinateSystem' => \Kotka\Service\CoordinateService::SYSTEM_WSG84,
                        'direction' => \Kotka\Validator\Coordinate::LATITUDE,
                        'messages' => array(
                            \Kotka\Validator\Coordinate::OUT_OF_BOUNDS => 'Latitude min out of bounds.',
                            \Kotka\Validator\Coordinate::NOT_NUMERIC => 'Latitude min must be a number.'
                        ),
                    )
                ),
                array(
                    'name' => 'RequireWithThis',
                    'options' => array(
                        'field' => 'MYBoundingBoxLatMax',
                        'messages' => array(
                            \Kotka\Validator\RequireWithThis::MISSING => 'All bonding box values should be set when one or more are.'
                        ),
                    ),
                ),
                array(
                    'name' => 'RequireWithThis',
                    'options' => array(
                        'field' => 'MYBoundingBoxLonMin',
                        'messages' => array(
                            \Kotka\Validator\RequireWithThis::MISSING => 'All bonding box values should be set when one or more are.'
                        ),
                    ),
                ),
                array(
                    'name' => 'RequireWithThis',
                    'options' => array(
                        'field' => 'MYBoundingBoxLonMax',
                        'messages' => array(
                            \Kotka\Validator\RequireWithThis::MISSING => 'All bonding box values should be set when one or more are.'
                        ),
                    ),
                ),
                array(
                    'name' => 'NumberCompare',
                    'options' => array(
                        'compareTo' => 'MYBoundingBoxLatMax',
                        'direction' => 'less than'
                    ),
                )
            )
        ));

        $this->add(array(
            'name' => 'MYBoundingBoxLatMax',
            'type' => 'text',
            'validators' => array(
                array(
                    'name' => 'Coordinate',
                    'options' => array(
                        'coordinateSystem' => \Kotka\Service\CoordinateService::SYSTEM_WSG84,
                        'direction' => \Kotka\Validator\Coordinate::LATITUDE,
                        'messages' => array(
                            \Kotka\Validator\Coordinate::OUT_OF_BOUNDS => 'Latitude max out of bounds.',
                            \Kotka\Validator\Coordinate::NOT_NUMERIC => 'Latitude max must be a number.'
                        ),
                    )
                ),
                array(
                    'name' => 'RequireWithThis',
                    'options' => array(
                        'field' => 'MYBoundingBoxLatMin',
                        'messages' => array(
                            \Kotka\Validator\RequireWithThis::MISSING => 'All bonding box values should be set when one or more are.'
                        ),
                    ),
                ),
                array(
                    'name' => 'RequireWithThis',
                    'options' => array(
                        'field' => 'MYBoundingBoxLonMin',
                        'messages' => array(
                            \Kotka\Validator\RequireWithThis::MISSING => 'All bonding box values should be set when one or more are.'
                        ),
                    ),
                ),
                array(
                    'name' => 'RequireWithThis',
                    'options' => array(
                        'field' => 'MYBoundingBoxLonMax',
                        'messages' => array(
                            \Kotka\Validator\RequireWithThis::MISSING => 'All bonding box values should be set when one or more are.'
                        ),
                    ),
                ),
            )
        ));

        $this->add(array(
            'name' => 'MYBoundingBoxLonMin',
            'type' => 'text',
            'validators' => array(
                array(
                    'name' => 'Coordinate',
                    'options' => array(
                        'coordinateSystem' => \Kotka\Service\CoordinateService::SYSTEM_WSG84,
                        'direction' => \Kotka\Validator\Coordinate::LONGITUDE,
                        'messages' => array(
                            \Kotka\Validator\Coordinate::OUT_OF_BOUNDS => 'Longitude min out of bounds.',
                            \Kotka\Validator\Coordinate::NOT_NUMERIC => 'LOngitude min must be a number.'
                        ),
                    )
                ),
                array(
                    'name' => 'RequireWithThis',
                    'options' => array(
                        'field' => 'MYBoundingBoxLatMin',
                        'messages' => array(
                            \Kotka\Validator\RequireWithThis::MISSING => 'All bonding box values should be set when one or more are.'
                        ),
                    ),
                ),
                array(
                    'name' => 'RequireWithThis',
                    'options' => array(
                        'field' => 'MYBoundingBoxLatMax',
                        'messages' => array(
                            \Kotka\Validator\RequireWithThis::MISSING => 'All bonding box values should be set when one or more are.'
                        ),
                    ),
                ),
                array(
                    'name' => 'RequireWithThis',
                    'options' => array(
                        'field' => 'MYBoundingBoxLonMax',
                        'messages' => array(
                            \Kotka\Validator\RequireWithThis::MISSING => 'All bonding box values should be set when one or more are.'
                        ),
                    ),
                ),
                array(
                    'name' => 'NumberCompare',
                    'options' => array(
                        'compareTo' => 'MYBoundingBoxLonMax',
                        'direction' => 'less than'
                    ),
                )
            )
        ));

        $this->add(array(
            'name' => 'MYBoundingBoxLonMax',
            'type' => 'text',
            'validators' => array(
                array(
                    'name' => 'Coordinate',
                    'options' => array(
                        'coordinateSystem' => \Kotka\Service\CoordinateService::SYSTEM_WSG84,
                        'direction' => \Kotka\Validator\Coordinate::LONGITUDE,
                        'messages' => array(
                            \Kotka\Validator\Coordinate::OUT_OF_BOUNDS => 'Longitude min out of bounds.',
                            \Kotka\Validator\Coordinate::NOT_NUMERIC => 'Longitude min must be a number.'
                        ),
                    )
                ),
                array(
                    'name' => 'RequireWithThis',
                    'options' => array(
                        'field' => 'MYBoundingBoxLatMin',
                        'messages' => array(
                            \Kotka\Validator\RequireWithThis::MISSING => 'All bonding box values should be set when one or more are.'
                        ),
                    ),
                ),
                array(
                    'name' => 'RequireWithThis',
                    'options' => array(
                        'field' => 'MYBoundingBoxLatMax',
                        'messages' => array(
                            \Kotka\Validator\RequireWithThis::MISSING => 'All bonding box values should be set when one or more are.'
                        ),
                    ),
                ),
                array(
                    'name' => 'RequireWithThis',
                    'options' => array(
                        'field' => 'MYBoundingBoxLonMin',
                        'messages' => array(
                            \Kotka\Validator\RequireWithThis::MISSING => 'All bonding box values should be set when one or more are.'
                        ),
                    ),
                ),
            )
        ));

        $this->add(array(
            'name' => 'MYAllowedForDwStatistics',
            'type' => 'DatabaseSelect',
            'options' => array(
                'field' => 'boolean',
                'empty_option' => 'Select',
                'value_options' => array(
                    'true' => 'Yes',
                    'false' => 'No'
                ),
            ),
        ));

        $this->add(array(
            'name' => 'MYMetadataCreator',
            'type' => 'text',
            'options' => array(
                'template' => 'kotka/partial/element/add-me',
                'add-me' => array(
                    'name' => '#metadataCreator'
                )
            ),
            'attributes' => array(
                'id' => 'metadataCreator'
            )
        ));

        $this->add(array(
            'name' => 'MYMetadataStatus',
            'type' => 'DatabaseSelect',
            'options' => array(
                'empty_option' => 'Select',
                'field' => 'MY.metadataStatus',
            ),
        ));

        $this->add(array(
            'name' => 'MYNotes',
            'type' => 'textarea',
        ));

        $this->add(array(
            'name' => 'MYDownloadRequestHandler',
            'type' => 'ArrayCollection',
            'attributes' => array(
                'id' => 'MYDownloadRequestHandler'
            ),
            'options' => array(
                'count' => 1,
                'should_create_template' => true,
                'allow_add' => true,
                'allow_remove' => true,
                'target_element' => array(
                    'type' => 'DatabaseSelect',
                    'options' => array(
                        'empty_option' => 'Select',
                        'field' => 'MY.downloadRequestHandler',
                    ),
                )
            )
        ));

        $this->add(array(
            'name' => 'MYDigitizedSize',
            'validators' => array(
                array(
                    'name' => 'Between',
                    'break_chain_on_failure' => true,
                    'options' => array(
                        'min' => 0,
                        'max' => 100,
                        'messages' => array(
                            \Zend\Validator\Between::NOT_BETWEEN => 'Digitization % must be between 0...100.'
                        )
                    )
                ),
                array(
                    'name' => 'Digits',
                    'break_chain_on_failure' => true,
                    'options' => array(
                        'messages' => array(
                            \Zend\Validator\Digits::NOT_DIGITS => 'Digitization % must be whole number (integer) . If exact value is not known, insert the best estimate.'
                        )
                    )
                )
            )
        ));

        $this->add(array(
            'name' => 'MYDataQuarantinePeriod',
            'validators' => array(
                array(
                    'name' => 'GreaterThan',
                    'break_chain_on_failure' => true,
                    'options' => array(
                        'min' => 0,
                        'inclusive' => true,
                        'messages' => array(
                            \Zend\Validator\GreaterThan::NOT_GREATER => 'Quarantine period must be equal or greater than 0.'
                        )
                    )
                ),
                array(
                    'name' => 'Float',
                )
            )
        ));

        $this->add(array(
            'name' => 'MYCollectionSize',
            'validators' => array(
                array(
                    'name' => 'GreaterThan',
                    'break_chain_on_failure' => true,
                    'options' => array(
                        'min' => 0,
                        'inclusive' => true,
                        'messages' => array(
                            \Zend\Validator\GreaterThan::NOT_GREATER => 'Collection size must be equal or larger than 0.'
                        )
                    )
                ),
                array(
                    'name' => 'Digits',
                    'break_chain_on_failure' => true,
                    'options' => array(
                        'messages' => array(
                            \Zend\Validator\Digits::NOT_DIGITS => 'Collection size must be whole number (integer) . If exact value is not known, insert the best estimate.'
                        )
                    )
                )
            )
        ));

        $this->add(array(
            'name' => 'MYTypesSize',
            'validators' => array(
                array(
                    'name' => 'GreaterThan',
                    'break_chain_on_failure' => true,
                    'options' => array(
                        'min' => 0,
                        'inclusive' => true,
                        'messages' => array(
                            \Zend\Validator\GreaterThan::NOT_GREATER => 'Type size must be larger than 0.'
                        )
                    )
                ),
                array(
                    'name' => 'Digits',
                    'break_chain_on_failure' => true,
                    'options' => array(
                        'messages' => array(
                            \Zend\Validator\Digits::NOT_DIGITS => 'Amount of type specimens must be whole number (integer) . If exact value is not known, insert the best estimate.'
                        )
                    )
                )
            )
        ));

        $this->add(array(
            'name' => 'MYInternalUseOnly',
            'type' => 'DatabaseSelect',
            'options' => array(
                'field' => 'boolean',
                'empty_option' => '',
            ),
        ));
    }
}
