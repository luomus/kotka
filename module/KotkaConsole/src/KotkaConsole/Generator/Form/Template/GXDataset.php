<?php

namespace KotkaConsole\Generator\Form\Template;

/**
 * Class GXDataset
 * @package KotkaConsole\Generator\Form\Template
 */
class GXDataset extends Base
{
    protected $base = 'Kotka\Form\GXDataset';

    /**
     * Creates the form with all the necessary fields in it
     */
    public function __construct()
    {
        parent::__construct();

        $this->add(array(
            'name' => 'GXDatasetType',
            'type' => 'DatabaseSelect',
            'options' => array(
                'empty_option' => 'Basic',
                'field' => 'GX.datasetType'
            )
        ));

        $this->add(array(
            'name' => 'GXDatasetNameEn',
            'type' => 'Text',
            'options' => array(),
            'attributes' => array(
                'id' => 'datasetName',
                'class' => 'form-control',
                'required' => true
            ),
            'filters' => array(
                array(
                    'name' => 'StringTrim',
                )
            ),
            'validators' => array(
                array(
                    'name' => 'UniqueDatasetName',
                ),
                array(
                    'name' => 'NotEmpty',
                    'break_chain_on_failure' => true,
                    'options' => array(
                        'messages' => array(
                            \Zend\Validator\NotEmpty::IS_EMPTY => 'Name must be given.'
                        )
                    )
                ),
            )
        ));

        $this->add(array(
            'name' => 'GXPersonsResponsible',
            'type' => 'text',
            'options' => array(
                'template' => 'kotka/partial/element/add-me',
                'add-me' => array(
                    'name' => '#personsResponsible',
                )
            ),
            'attributes' => array(
                'id' => 'personsResponsible',
                'class' => 'form-control',
                'required' => true
            ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'break_chain_on_failure' => true,
                    'options' => array(
                        'messages' => array(
                            \Zend\Validator\NotEmpty::IS_EMPTY => 'Persons responsible must be given.'
                        )
                    )
                )
            )
        ));

        $this->add(array(
            'name' => 'GXDescriptionEn',
            'type' => 'Textarea',
            'options' => array(),
            'attributes' => array(
                'id' => 'description',
                'class' => 'form-control'
            )
        ));

        $this->add(array(
            'name' => 'GXBenefitsDerivedAndShared',
            'type' => 'Textarea',
            'options' => array(),
            'attributes' => array(
                'id' => 'benefitsDerivedAndShared',
                'class' => 'form-control'
            )
        ));
    }
}
