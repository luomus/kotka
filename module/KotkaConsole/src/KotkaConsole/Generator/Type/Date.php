<?php
namespace KotkaConsole\Generator\Type;


class Date implements TypeInterface
{


    public function extract($value)
    {
        return "\$value = $value instanceof \\DateTime ? {$value}->format(DATE_ISO8601) : $value;";

    }

    public function hydrate($value)
    {
        return "new \\DateTime($value)";
    }
}