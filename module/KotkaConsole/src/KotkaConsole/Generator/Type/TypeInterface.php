<?php
namespace KotkaConsole\Generator\Type;


interface TypeInterface
{

    public function extract($value);

    public function hydrate($value);

} 