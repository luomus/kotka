<?php

namespace KotkaConsole\Service;

use KotkaConsole\Generator\Form\Template\TemplateInterface;
use Triplestore\Model\Metadata;
use Triplestore\Service\MetadataService;
use Triplestore\Service\PhpVariableConverter;
use Zend\Filter\Word\UnderscoreToCamelCase;

/**
 * Class Generator
 * @deprecated Use triplestore generator instead!
 * @package KotkaConsole\Service
 */
class Generator
{

    const FORM_ANNOTATION = '* @Form\\';
    const META_ANNOTATION = '* @Metadata\\';

    private $classNS;
    private $classTplFile;
    private $classFolder;
    private $filter;
    private $children = array();

    private $extend = array(
        'HRATransaction' => 'KotkaEntity',
        'MOSOrganization' => 'KotkaEntity',
        'MYCollection' => 'KotkaEntity',
        'MYDocument' => 'Document',
        'GXDataset' => 'KotkaEntity',
        'MFSample' => 'KotkaEntity',
    );

    private $langs = ['fi', 'sv', 'en'];

    /**
     * Skip template parts with these keys
     * This prevents the generator from creating unnecessary annotation fields
     *
     * @var array
     */
    private $skip = array(
        'name'
    );

    public function __construct($classTplFile, $classFolder, $classNS)
    {
        $this->classNS = rtrim($classNS, '\\');
        $this->classTplFile = $classTplFile;
        $this->classFolder = rtrim($classFolder, DIRECTORY_SEPARATOR);
        if (!is_dir($this->classFolder)) {
            throw new \Exception("Could not find class folder: '{$this->classFolder}'");
        }
        if (!is_writable($this->classFolder)) {
            throw new \Exception("Cannot write to file '{$this->classFolder}'");
        }
        $this->filter = new UnderscoreToCamelCase();
    }

    public function generateClass($class, Metadata $metadata)
    {
        PhpVariableConverter::init();
        $classTpl = file_get_contents($this->classTplFile);
        $saveClass = PhpVariableConverter::toPhpClassName($class);
        $extends = isset($this->extend[$saveClass]) ? $this->extend[$saveClass] : 'AbstractOntology';
        $classFile = $this->classFolder . DIRECTORY_SEPARATOR . $saveClass . '.php';
        $classMap = array_flip(MetadataService::$classMap);
        $strMethod = '';
        $strProperty = '';
        $strExchange = '';
        $strCopy = '';
        $properties = $metadata->getProperties();
        $tplClass = '\KotkaConsole\Generator\Form\Template\\' . $saveClass;
        $fields = array();
        if (!class_exists($tplClass)) {
            return;
        }
        /** @var TemplateInterface $template */
        $template = new $tplClass();
        $this->children = array();
        foreach ($properties as $property) {
            $strMethod .= $this->methodLine($property, $metadata);
            $strProperty .= $this->propertyLine($property, $metadata, $template, $fields, $saveClass);
            $strExchange .= $this->exchangeLine($property, $metadata);
            $strCopy .= $this->copyLine($property, $metadata);
        }
        if (count($this->children) > 0) {
            $strProperty = $this->childPropertyes($this->children, $strProperty);
        }
        $classAnno = $this->classAnnotations($template);
        $classTpl = str_replace('%baseForm%', $template->getBase(), $classTpl);
        $classTpl = str_replace('%getters-n-setters%', $strMethod, $classTpl);
        $classTpl = str_replace('%classAnnotations%', $classAnno, $classTpl);
        $classTpl = str_replace('%properties%', $strProperty, $classTpl);
        $classTpl = str_replace('%exchange%', $strExchange, $classTpl);
        $classTpl = str_replace('%model-copy%', $strCopy, $classTpl);
        $classTpl = str_replace('%ns%', $this->classNS, $classTpl);
        $classTpl = str_replace('%class%', $saveClass, $classTpl);
        $classTpl = str_replace('%interfaceClass%', $saveClass . 'Interface', $classTpl);
        $classTpl = str_replace('%extends%', $extends, $classTpl);
        $classTpl = str_replace('%origClass%', isset($classMap[$class]) ? $classMap[$class] : $class, $classTpl, $classTpl);

        file_put_contents($classFile, $classTpl);
    }

    private function classAnnotations(TemplateInterface $template)
    {
        $return = array();
        $hydrator = $template->getHydrator();
        if (!empty($hydrator)) {
            $return[] = self::FORM_ANNOTATION . 'Hydrator("' . $hydrator . '")';
        }
        if (count($return) > 0) {
            return PHP_EOL . $this->getAnnotationString($return, 0, false);
        }
        return '';
    }

    private function propertyLine($property, Metadata $metadata, TemplateInterface $template = null, array & $fields, $saveClass)
    {
        if ($metadata->isMultiLanguage($property)) {
            $str = '';
            foreach ($this->langs as $lang) {
                $propertyLang = $property . ucfirst($lang);
                $str .= $this->buildAnnotations($property, $saveClass, $metadata, $template, $lang);
                $str .= $this->property($propertyLang, $metadata, $fields);
            }
            return $str;
        }
        return $this->buildAnnotations($property, $saveClass, $metadata, $template)
        . $this->property($property, $metadata, $fields);
    }

    private function property($property, Metadata $metadata, array & $fields)
    {
        $phpSaveProperty = PhpVariableConverter::toPhpMethod($property);
        $fields[$phpSaveProperty] = true;
        $multiple = '';
        if ($metadata->hasMany($property)) {
            $multiple = ' = array()';
        }
        return "protected \${$phpSaveProperty}{$multiple};
    ";
    }

    private function childPropertyes($list, $str)
    {
        $line = "protected \$children = array('" . implode("', '", $list) . "');" . PHP_EOL . "    ";
        return $line . $str;
    }

    private function buildAnnotations($property, $saveClass, Metadata $metadata, TemplateInterface $template = null, $lang = null)
    {
        if ($template === null) {
            return '';
        }
        $phpSaveProperty = PhpVariableConverter::toPhpMethod($property) . ucfirst($lang);
        $hasChild = $metadata->hasChildren($property);
        $annotation = array('/**');
        $annotation[] = self::META_ANNOTATION . 'Type("' . $metadata->getType($property) . '")';
        $order = $metadata->getOrder($property);
        $annotation[] = self::META_ANNOTATION . 'Order(' . $order . ')';
        $specs = $template->has($phpSaveProperty) ?
            $template->get($phpSaveProperty) :
            $template->get(TemplateInterface::DEFAULT_ELEM);

        if ($specs === 'auto') {
            $specs = $this->buildAutoElement($phpSaveProperty, $property, $metadata);
        }

        if ($hasChild) {
            if (!$template->has($phpSaveProperty)) {
                return '';
            }
            $this->children[] = $phpSaveProperty;
            $class = PhpVariableConverter::toPhpClassName($property);
            $class = $this->classNS . '\\' . $class;
            $options = '';
            if (isset($specs['options'])) {
                $options .= ', "options":';
                $options .= json_encode($specs['options']);
            }
            if ($metadata->hasMany($property)) {
                $options .= ', "is_collection":"true"';
            }
            $optionsSpec = ['label' => $metadata->getLabel($property)];
            $annotation[] = self::FORM_ANNOTATION . 'Options(' . json_encode($optionsSpec) . ')';
            $annotation[] = self::FORM_ANNOTATION . 'ComposedObject({"target_object":"' . $class . '"' . $options . '})';
            //$annotation[] = self::FORM_ANNOTATION . 'Required(false)';
            return $this->getAnnotationString($annotation);
        }

        if ($order == -1) {
            $annotation[] = self::FORM_ANNOTATION . 'Exclude()';
        }

        $isDB = isset($specs['type']) && $specs['type'] == 'DatabaseSelect';
        $label = $metadata->getLabel($property, 'en', null);
        $help = $metadata->getHelp($property);

        // Populate the information for the label and help
        // If value exists in the template then that is used instead

        if (!isset($specs['options'])) {
            $specs['options'] = array();
        }
        if (!isset($specs['attributes'])) {
            $specs['attributes'] = array('class' => $phpSaveProperty . ' ' . $saveClass);
        } else {
            $attrClass = $phpSaveProperty;
            if (isset($specs['attributes']['class']) && strpos($specs['attributes']['class'], $attrClass) === false) {
                $attrClass .= ' ' . $specs['attributes']['class'];
            }
            $specs['attributes']['class'] = $attrClass . ' ' . $saveClass;
        }
        if ($label !== null && !isset($specs['options']['label'])) {
            $specs['options']['label'] = $label;
        }
        if ($help !== null && !isset($specs['options']['help'])) {
            $specs['options']['help'] = htmlentities($help);
        }
        if ($metadata->isRequired($property) && !isset($specs['required'])) {
            $specs['attributes']['required'] = true;
        }
        if (count($specs['options']) === 0) {
            unset($specs['options']);
        }
        if (count($specs['attributes']) === 0) {
            unset($specs['attributes']);
        }
        $skips = $this->skip;
        foreach ($specs as $key => $value) {
            if (in_array(strtolower($key), $skips)) {
                continue;
            }
            if (strtolower($key) === 'validators') {
                foreach ($value as $validatorSpec) {
                    $annotation[] = self::FORM_ANNOTATION . 'Validator(' . json_encode($validatorSpec) . ')';
                }
            } else if (strtolower($key) === 'filters') {
                foreach ($value as $filterSpec) {
                    $annotation[] = self::FORM_ANNOTATION . 'Filter(' . json_encode($filterSpec) . ')';
                }
            } else {
                $value = $this->makeAnnotationSave($value);
                $annotation[] = self::FORM_ANNOTATION . ucfirst(strtolower($key)) . '(' . json_encode($value) . ')';
            }
        }
        if (!isset($specs['required']) && !$hasChild) {
            if ($metadata->isRequired($property)) {
                $annotation[] = self::FORM_ANNOTATION . 'Required(true)';
            } else {
                $annotation[] = self::FORM_ANNOTATION . 'Required(false)';
            }
        }

        return $this->getAnnotationString($annotation);
    }

    private function buildAutoElement($phpSaveProperty, $property, Metadata $metadata) {
        $spec = [
            'name' => $phpSaveProperty,
            'type' => 'text'
        ];
        if ($metadata->hasMany($property) && !$metadata->hasChildren($property)) {
            $parts = explode('.', $property);
            $name = array_pop($parts);
            $spec['type'] = 'ArrayCollection';
            $spec['attributes'] = ['id' => $name, 'style' => '', 'autocomplete' => 'off'];
            $spec['options'] = [
                'count' => 1,
                'should_create_template' => true,
                'allow_add' => true,
                'allow_remove' => true,
                'target_element' => array(
                    'name' => $name,
                    'type' => 'text',
                )
            ];
        }
        return $spec;
    }

    private function makeAnnotationSave($value)
    {
        if (is_bool($value)) {
            return $value;
        }
        if (is_array($value)) {
            foreach ($value as $key => $val) {
                $value[$key] = $this->makeAnnotationSave($val);
            }
        } else {
            $value = str_replace('"', "'", $value);
        }
        return $value;
    }

    private function getAnnotationString(array $annotation, $tabCount = 1, $addEnding = true)
    {
        // Adding options JSON_UNESCAPED_SLASHES to json encode makes this loop unnecessary
        // Unfortunately option is only available PHP > 5.4.0
        foreach ($annotation as $key => $value) {
            $value = str_replace("\\/", "/", $value);
            $value = str_replace("\\\\", "\\", $value);
            $annotation[$key] = $value;
        }
        array_walk($annotation, array($this, 'addSpaces'), $tabCount);
        $line = implode(PHP_EOL, $annotation);
        if ($addEnding) {
            $ending = str_repeat('    ', $tabCount) . '**/';
            $line .= PHP_EOL . $ending . PHP_EOL . "    ";
        }
        return $line;
    }

    private function addSpaces(&$value, $key, $count)
    {
        if ($key == 0) {
            return;
        }
        $line = str_repeat('    ', $count);
        $value = $line . $value;
    }


    private function exchangeLine($property, Metadata $metadata)
    {
        $var = PhpVariableConverter::toPhpMethod($property);
        $valueExtract = '';
        $predicateExtract = array('getResourceValue()' => $var);
        if ($metadata->isLiteral($property)) {
            $predicateExtract = array();
            if ($metadata->isMultiLanguage($property)) {
                foreach ($this->langs as $lang) {
                    $predicateExtract["getLiteralValue('$lang')"] = $var . ucfirst($lang);
                }
            } else {
                $predicateExtract['getLiteralValue()'] = $var;
            }
        }

        $hasMany = $metadata->hasMany($property);
        foreach ($predicateExtract as $method => $variable) {
            if ($hasMany) {
                $valueExtract .=
                    "\$value = \$model->getPredicate('$property')->$method;
            \$this->$variable = \$value;";
            } else {
                $valueExtract .=
                    "\$value = \$model->getPredicate('$property')->$method;
            \$this->$variable = array_pop(\$value);";
            }

            if ($metadata->hasChildren($property)) {
                if ($metadata->hasMany($property)) {
                    $valueExtract = "
                \$values = \$model->getPredicate('$property')->getResourceValue();
                \$this->$variable = array();
                foreach(\$values as \$key => \$m) {
                    \$object = new $variable();
                    \$object->exchangeModel(\$m);
                    \$this->{$variable}[\$key] = \$object;
                }
                ";
                } else {
                    $valueExtract = "
                \$values = \$model->getPredicate('$property')->getResourceValue();
                if (isset(\$values[0])) {
                    \$this->$variable = new $variable();
                    \$this->{$variable}->exchangeModel(\$values[0]);
                }
                ";
                }
            }
        }

        return "
        if (\$model->hasPredicate('$property')) {
            $valueExtract
        }
        ";
    }

    private function copyLine($property, Metadata $metadata)
    {
        $var = PhpVariableConverter::toPhpMethod($property);
        if ($metadata->hasChildren($property)) {
            if ($metadata->hasMany($property)) {
                return "
        \$models = array();
        foreach (\$this->$var as \$key => \$object) {
            \$models[\$key] = \$object->getModelCopy();
            \$models[\$key]->getOrCreatePredicate('MY.isPartOf')->setResourceValue(\$this->subject);
        }
        \$model->getOrCreatePredicate('$property')->setResourceValue(\$models);";
            } else {
                return "
        \$models = array();
        if(\$this->$var) {
            \$models[0] = \$this->{$var}->getModelCopy();
            \$models[0]->getOrCreatePredicate('MY.isPartOf')->setResourceValue(\$this->subject);
        }
        \$model->getOrCreatePredicate('$property')->setResourceValue(\$models);";
            }
        }
        if ($metadata->isLiteral($property)) {
            if ($metadata->isMultiLanguage($property)) {
                $str = '';
                foreach ($this->langs as $lang) {
                    $str .= "
        \$model->getOrCreatePredicate('$property')->setLiteralValue(\$this->$var" . ucfirst($lang) . ", '$lang');";
                }
                return $str;
            } else {
                return "
        \$model->getOrCreatePredicate('$property')->setLiteralValue(\$this->$var);";
            }
        } else {
            return "
        \$model->getOrCreatePredicate('$property')->setResourceValue(\$this->$var);";
        }
    }

    private function methodLine($property, Metadata $metadata)
    {
        $cast = $metadata->hasMany($property) ? 'array ' : '';
        $method = PhpVariableConverter::toPhpMethod($property);
        if ($metadata->isLiteral($property)) {
            $defaultLang = 'null';
            $metaLang = $metadata->getDefaultLang($property);
            if ($metaLang !== null && is_string($metaLang)) {
                $defaultLang = "'$metaLang'";
            }
            return "
    public function get$method(\$lang = $defaultLang)
    {
        \$method = \$this->getMethod('$method', \$lang);
        return \$this->\$method;
    }

    public function set$method($cast\$value, \$lang = $defaultLang)
    {
        \$method = \$this->getMethod('$method', \$lang);
        \$this->\$method = \$value;

        return \$this;
    }";
        } else { // in case of resource
            return "
    public function get$method()
    {
        return \$this->$method;
    }

    public function set$method($cast\$value)
    {
        \$this->$method = \$value;

        return \$this;
    }";
        }
    }
}

