<?php

namespace KotkaConsole\Service;


use Zend\EventManager\EventManagerAwareInterface;
use Zend\EventManager\EventManagerAwareTrait;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

class GeneratorService implements ServiceLocatorAwareInterface, EventManagerAwareInterface
{
    use ServiceLocatorAwareTrait;
    use EventManagerAwareTrait;

    public function generateClasses()
    {
        /** @var \Triplestore\Service\ObjectManager $om */
        /** @var \KotkaConsole\Service\Generator $generator */
        $cache = $this->getServiceLocator()->get('cache');
        $om = $this->getServiceLocator()->get('Triplestore\ObjectManager');
        $om->getCache()->flush();
        $permaCache = $this->getServiceLocator()->get('perma-cache');
        $koGenerator = $this->getServiceLocator()->get('KotkaConsole\Service\Generator');
        $tsGenerator = $this->getServiceLocator()->get('Triplestore\Service\GeneratorService');
        $classes = $om->getMetadataService()->getAllClasses();
        $cnt = 0;
        $this->getEventManager()->trigger('generate-start', $this, ['total' => count($classes)]);
        foreach ($classes as $class => $metadata) {
            $cnt++;
            $this->getEventManager()->trigger('generate', $this, ['spot' => $cnt, 'class' => $class]);
            $koGenerator->generateClass($class, $metadata);    // This generates old classes
            $tsGenerator->generateClass($class, $metadata);    // This generates classes and the interfaces
            $tsGenerator->generateHydrator($class, $metadata); // This generates hydrators for the classes
            $tsGenerator->generateHydrator($class, $metadata, true); // This generates hydrators for the classes in db format
        }
        $this->getEventManager()->trigger('generate-finnish', $this);
        $permaCache->flush();
        $cache->flush();
    }
}