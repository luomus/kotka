<?php

namespace KotkaConsole\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class GeneratorFactory is a factory class for the generator service
 * @package KotkaConsole\Service
 */
class GeneratorFactory implements FactoryInterface
{
    /**
     * Creates the service
     * @param ServiceLocatorInterface $serviceLocator
     * @return Generator
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /** @var \Triplestore\Options\ModuleOptions $options */
        $options = $serviceLocator->get('Triplestore\Options\ModuleOptions');
        $classTemplateFile = realpath(__DIR__ . '/../Template/Class.tpl');
        $classFolder = $options->getClassDir();
        $classNS = $options->getClassNS();

        return new Generator($classTemplateFile, $classFolder, $classNS);
    }
}
