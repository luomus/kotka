<?php

namespace KotkaConsole\Service;

use Kotka\Service\FormElementService;
use KotkaConsole\Generator\Form\Template\TemplateInterface;
use Triplestore\Service\PhpVariableConverter;

class MapGenerator
{

    private $tplFile;
    private $targetFolder;
    /** @var \Kotka\Service\FormElementService */
    private $elementService;
    private $has = [];
    private $field;

    public function __construct(FormElementService $elementService)
    {
        $this->tplFile = 'module/KotkaConsole/src/KotkaConsole/Template/Map.tpl';
        $this->targetFolder = 'module/Importer/src/Importer/Maps/';
        $this->elementService = $elementService;
        if (!is_dir($this->targetFolder)) {
            throw new \Exception("Could not find class folder: '{$this->targetFolder}'");
        }
        if (!is_writable($this->targetFolder)) {
            throw new \Exception("Cannot write to file '{$this->targetFolder}'");
        }
    }

    public function generateMap($field)
    {
        $tplFile = file_get_contents($this->tplFile);
        $saveClass = PhpVariableConverter::toPhpClassName($field);
        $classFile = $this->targetFolder . $saveClass . '.php';
        $strValues = '';
        $tplClass = '\KotkaConsole\Template\Maps\\' . $saveClass;
        $this->field = $field;
        if (!class_exists($tplClass)) {
            $tplClass = '\KotkaConsole\Template\Maps\Clear';
        }
        /** @var TemplateInterface $template */
        $template = new $tplClass();
        $values = $this->elementService->getSelect($field, []);
        $strValues .= $this->valueLines($values, $template);
        $tplFile = str_replace('%field%', $saveClass, $tplFile);
        $tplFile = str_replace('%values%', $strValues, $tplFile);

        file_put_contents($classFile, $tplFile);
    }

    private function valueLines($values, TemplateInterface $template)
    {
        $strValues = '';
        $this->has = [];
        $tplValues = $template->getAll();
        $diff = array_diff_key($tplValues, $values);
        $values = $this->getAllOptions($values);
        if (!empty($values) && count($diff) !== 0) {
            throw new \Exception("Template for {$this->field} has values that cannot be mapped (" . implode(', ', array_keys($diff)) . ")");
        }
        foreach ($values as $key => $value) {
            $strValues .= $this->addLine($key, $key);
            if ($template->has($key)) {
                $elems = $template->get($key);
                foreach ($elems as $elem) {
                    $strValues .= $this->addLine($elem, $key);

                }
            }
            if ($key != $value) {
                $strValues .= $this->addLine($value, $key, true);
            }
        }
        foreach ($diff as $key => $value) {
            $strValues .= $this->addLine($key, $key);
            foreach ($value as $elem) {
                $strValues .= $this->addLine($elem, $key);
            }
        }
        return $strValues;
    }

    private function addLine($key, $value, $skipIfExists = false)
    {
        $cKey = mb_strtolower($key, 'UTF-8');
        $cKey = str_replace("'", "\\'", $cKey);
        $value = str_replace("'", "\\'", $value);
        if (isset($this->has[$cKey])) {
            if ($skipIfExists) {
                return '';
            }
            throw new \Exception("Could not specify unique mapping for '$key' => '$value' in {$this->field}");
        }
        $this->has[$cKey] = true;
        return "
        '$cKey' => '$value',";
    }


    private function getAllOptions($options)
    {
        $all = [];
        foreach ($options as $key => $value) {
            if (is_array($value)) {
                $all = array_merge($all, $this->getAllOptions($value));
            } else {
                if ($key !== 'label') {
                    $all[$key] = $value;
                }
            }
        }
        return $all;
    }

}

