<?php

namespace KotkaConsole\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class MapGeneratorFactory is a factory class for the map generator service
 * @package KotkaConsole\Service
 */
class MapGeneratorFactory implements FactoryInterface
{
    /**
     * Creates the service
     * @param ServiceLocatorInterface $serviceLocator
     * @return MapGenerator
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $formElementService = $serviceLocator->get('Kotka\Service\FormElementService');

        return new MapGenerator($formElementService);
    }
}
