<?php

namespace KotkaConsoleTest;

class Console
{
    const TEST_DATATYPE = 'test';

    const RUN_CRON = 'run cron';

    const OPTION_MUSTIKKA = '--send-to-mustikka';
    const OPTION_MUSTIKKA_SHORT = '-m';
}
