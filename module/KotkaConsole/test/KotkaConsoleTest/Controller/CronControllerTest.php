<?php

namespace KotkaConsoleTest\Controller;

use Kotka\Document\Specimen;
use KotkaConsoleTest\Console;
use Zend\Test\PHPUnit\Controller\AbstractConsoleControllerTestCase;

class CronControllerTest extends AbstractConsoleControllerTestCase
{
    protected $traceError = true;

    public function setUp()
    {
        $this->setApplicationConfig(include 'config/application.config.php');
        parent::setUp();
    }

    public function testCronRouteAccessible()
    {
        $this->dispatch(Console::RUN_CRON);
        $this->assertResponseStatusCode(0);
        $this->assertModuleName('KotkaConsole');
        $this->assertControllerClass('CronController');
        $this->assertMatchedRouteName('cron');
    }

    public function testSendingToMustikkaAccessible()
    {
        $this->dispatch(Console::RUN_CRON . ' ' . Console::OPTION_MUSTIKKA);
        $this->assertResponseStatusCode(0);
        $this->assertModuleName('KotkaConsole');
        $this->assertControllerClass('CronController');
        $this->assertMatchedRouteName('cron');
        $this->assertActionName('run');
    }

    public function testSendingToMustikkaAccessibleWithSortParam()
    {
        $this->dispatch(Console::RUN_CRON . ' ' . Console::OPTION_MUSTIKKA_SHORT);
        $this->assertResponseStatusCode(0);
        $this->assertModuleName('KotkaConsole');
        $this->assertControllerClass('CronController');
        $this->assertMatchedRouteName('cron');
        $this->assertActionName('run');
    }

    /**
     * @depends testSendingToMustikkaAccessible
     */
    public function testSendingAllSuccessful()
    {
        $this->dispatch(Console::RUN_CRON . ' ' . Console::OPTION_MUSTIKKA);
        $this->assertResponseStatusCode(0);
        $this->assertConsoleOutputContains('0 succeeded');
    }

    public function testNoSpecimensInQueue()
    {
        $specimenMock = $this->getMockBuilder('Kotka\Service\SpecimenService')
            ->disableAutoload()
            ->disableOriginalConstructor()
            ->getMock();

        $specimenMock->expects($this->once())
            ->method('getAllNotInMustikka')
            ->will($this->returnValue(array()));

        $sl = $this->getApplicationServiceLocator();
        $sl->setAllowOverride(true);
        $sl->setService('Kotka\Service\SpecimenService', $specimenMock);
        $this->dispatch(Console::RUN_CRON . ' ' . Console::OPTION_MUSTIKKA);
        $this->assertResponseStatusCode(0);
        $this->assertConsoleOutputContains('No specimens in queue');
    }

    public function testTestSendingOneSuccessful()
    {
        $specimen = new Specimen();
        $specimen->setInMustikka(false);
        $specimenMock = $this->getMockBuilder('Kotka\Service\SpecimenService')
            ->disableOriginalConstructor()
            ->disableAutoload()
            ->getMock();
        $specimenMock->expects($this->once())
            ->method('getAllNotInMustikka')
            ->will($this->returnValue(array($specimen)));
        $specimenMock->expects($this->once())
            ->method('_update');
        $mustikkaMock = $this->getMockBuilder('Kotka\Service\MustikkaService')
            ->disableOriginalConstructor()
            ->disableAutoload()
            ->getMock();
        $mustikkaMock->expects($this->once())
            ->method('sendSpecimen')
            ->will($this->returnValue(true));
        $sl = $this->getApplicationServiceLocator();
        $sl->setAllowOverride(true);
        $sl->setService('Kotka\Service\SpecimenService', $specimenMock);
        $sl->setService('Kotka\Service\MustikkaService', $mustikkaMock);
        $this->dispatch(Console::RUN_CRON . ' ' . Console::OPTION_MUSTIKKA);
        $this->assertResponseStatusCode(0);
        $this->assertConsoleOutputContains('1 succeeded and 0 failed');
        $this->assertTrue($specimen->getInMustikka());
    }

    public function testTestSendingOneFailed()
    {
        $specimen = new Specimen();
        $specimen->setInMustikka(false);
        $specimenMock = $this->getMockBuilder('Kotka\Service\SpecimenService')
            ->disableOriginalConstructor()
            ->disableAutoload()
            ->getMock();
        $specimenMock->expects($this->once())
            ->method('getAllNotInMustikka')
            ->will($this->returnValue(array($specimen)));
        $specimenMock->expects($this->once())
            ->method('_update');
        $mustikkaMock = $this->getMockBuilder('Kotka\Service\MustikkaService')
            ->disableOriginalConstructor()
            ->disableAutoload()
            ->getMock();
        $mustikkaMock->expects($this->once())
            ->method('sendSpecimen')
            ->will($this->returnValue(false));
        $sl = $this->getApplicationServiceLocator();
        $sl->setAllowOverride(true);
        $sl->setService('Kotka\Service\SpecimenService', $specimenMock);
        $sl->setService('Kotka\Service\MustikkaService', $mustikkaMock);
        $this->dispatch(Console::RUN_CRON . ' ' . Console::OPTION_MUSTIKKA);
        $this->assertResponseStatusCode(0);
        $this->assertConsoleOutputContains('0 succeeded and 1 failed');
        $this->assertFalse($specimen->getInMustikka());
    }

}
