<?php

namespace Importer\Service;


use Kotka\Filter\UnreliableFields;
use Kotka\Model\Util;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\Adapter\Driver\ResultInterface;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\TableIdentifier;
use Zend\ProgressBar\Adapter\AbstractAdapter;
use Zend\ProgressBar\ProgressBar;

class HyntikkaService
{

    const DATE_FORMAT = 'Ymd';
    const JOIN_AS_ARRAY = '_as_array_';

    private $normalizerService;
    private $specimen;
    private $progressBarAdapter;

    private $mapping = array(
        // Key is the sources field with lowercase value and value is the field it's mapped to in kotka
        'namespace_id' => 'MYNamespaceID',
        'object_id' => 'MYObjectID',
        'taxon_name' => 'MYGathering[0][MYUnit][0][MYIdentification][0][MYTaxon]',
        'taxon_rank' => 'MYGathering[0][MYUnit][0][MYIdentification][0][MYTaxonRank]',
        'taxon_id' => 'MYGathering[0][MYUnit][0][MYIdentification][0][MYTaxonID]',
        'sec' => 'MYGathering[0][MYUnit][0][MYIdentification][0][MYSec]',
        'author' => 'MYGathering[0][MYUnit][0][MYIdentification][0][MYAuthor]',
        'date_begin' => 'MYGathering[0][MYDateBegin]',
        'date_end' => 'MYGathering[0][MYDateEnd]',
        'leg' => 'MYGathering[0][MYLeg]',
        'det' => 'MYGathering[0][MYUnit][0][MYIdentification][0][MYDet]',
        'count' => 'MYGathering[0][MYUnit][0][MYCount]',
        'higher_geography' => 'MYGathering[0][MYHigherGeographyEn]',
        'country' => 'MYGathering[0][MYCountry]',
        'province' => 'MYGathering[0][MYBiologicalProvinceEn]',
        'municipality' => 'MYGathering[0][MYMunicipalityEn]',
        'locality' => 'MYGathering[0][MYLocalityEn]',
        'locality_id' => 'MYGathering[0][MYLocalityID]',
        'coord_n' => 'MYGathering[0][MYLatitude]',
        'coord_e' => 'MYGathering[0][MYLongitude]',
        'coord_system' => 'MYGathering[0][MYCoordinateSystem]',
        'coord_radius' => 'MYGathering[0][MYCoordinateRadius]',
        'type_name' => 'MYGathering[0][MYUnit][0][MYTypeSpecimen][0][MYTypeSpecies]',
        'type_status' => 'MYGathering[0][MYUnit][0][MYTypeSpecimen][0][MYTypeStatus]',
        'type_verification' => 'MYGathering[0][MYUnit][0][MYTypeSpecimen][0][MYTypeVerification]',
        'notes' => 'MYNotesEn',
        'labels' => 'MYLabelsVerbatim',
        'tags' => 'MYGathering[0][MYUnit][0][MYIdentification][0][MYIdentificationNotesEn]',
        'record_type' => 'MYGathering[0][MYUnit][0][MYRecordBasis]',
        'record_level' => 'MYNotesEn',
        'collection_id' => 'MYCollectionID',
        'specimen_location' => 'MYDocumentLocationEn',
        'dataset_id' => 'MYProjectId',
        //'publicity_restrictions' => '', Halutaan aina public niin siksi tätä kenttää ei tarvita
        'primary_data' => '',
        'interpreted_data' => 'MYNotesEn',
        'ext' => '',
        'editor' => 'MYEditor',
        'edit_notes' => 'MYNotesEn',
        'edit_time' => '',
        'entered' => 'MYEntered',
        'unreliable_fields' => 'MYUnreliableFields',
        'ext:biotope' => 'MYGathering[0][MYHabitatDescriptionEn]',
        'ext:biotope_notes' => 'MYGathering[0][MYHabitatDescriptionEn]',
        //'ext:region' => '',
        'ext:sampling_method' => 'MYGathering[0][MYSamplingMethod]',
        'ext:duplicates' => 'MYDuplicatesIn',
        'ext:type_notes' => 'MYGathering[0][MYUnit][0][MYTypeSpecimen][0][MYTypeNotesEn]',
        'ext:type_ssp' => 'MYGathering[0][MYUnit][0][MYTypeSpecimen][0][MYTypeSubspecies]',
        'ext:type_sec' => 'MYGathering[0][MYUnit][0][MYTypeSpecimen][0][MYTypePubl]',
        'ext:type_designation' => 'MYGathering[0][MYUnit][0][MYTypeSpecimen][0][MYTypePubl]',
        'ext:type_series_id' => 'MYGathering[0][MYUnit][0][MYTypeSpecimen][0][MYTypeSeriesID]',
        'ext:det_date_begin' => 'MYGathering[0][MYUnit][0][MYIdentification][0][MYDetDate]',
        'ext:det_date_end' => 'MYGathering[0][MYUnit][0][MYIdentification][0][MYDetDate]',
        'ext:det_notes' => 'MYGathering[0][MYUnit][0][MYIdentification][0][MYIdentificationNotesEn]',
        'ext:url' => 'MYURL',
        'ext:sex' => 'MYGathering[0][MYUnit][0][MYSex]',
        'ext:lifestage' => 'MYGathering[0][MYUnit][0][MYLifeStage]',
        'ext:age' => 'MYGathering[0][MYUnit][0][MYAge]',
        //'ext:establishment_means' => '',
        'ext:elevation' => 'MYGathering[0][MYAlt]',
        //'ext:depth' => 'MYGathering[0][MYDepth]',
        'ext:preservation' => 'MYPreservation',
        'ext:condition' => 'MYConditionEn',
        'ext:coord_verbatim' => 'MYGathering[0][MYCoordinatesVerbatim]',
        'ext:coord_source' => 'MYGathering[0][MYCoordinateSource]',
        //'ext:coord_poly' => '',
        'ext:coord_notes' => 'MYGathering[0][MYCoordinateNotesEn]',
        //'ext:location_notes' => 'MYGathering[0][MYNotesEn]',
        'ext:measurements' => 'MYNotesEn',
        'ext:ring' => 'MYGathering[0][MYUnit][0][MYRing]',
        'ext:preparations' => 'MYGathering[0][MYUnit][0][MYPreparations]',
        'ext:host' => 'MYGathering[0][MYUnit][0][MYHost]',
        'ext:associated_taxa' => 'MYGathering[0][MYUnit][0][MYNotesEn]',
        'ext:time_begin' => 'MYGathering[0][MYNotesEn]',
        'ext:time_end' => 'MYGathering[0][MYNotesEn]',
        'ext:date_verbatim' => 'MYGathering[0][MYDateVerbatim]',
        //'ext:host_notes' => '',
        //'ext:decay_degree' => '',
        //'ext:exsiccatum' => '',
        //'ext:lichen_morphotype' => '',
        //'ext:acquisition_date' => '',
        'ext:acquired_from' => 'MYAdditionalIDs',
        //'ext:field_id' => '',
        'ext:other_id' => 'MYAdditionalIDs',
        //'ext:previous_collections' => '',
        //'ext:field_notes' => '',
        'ext:clad_id' => 'MYCladBookID',
        'ext:clad_id_notes' => ' MYCladSpecimenID',
        'ext:clad_notes_verbatim' => 'MYCladVerbatim',
        'ext:lang' => 'MYLanguage',
        //'ext:loan' => '',
        'ext:specimen_collection_id' => 'MYNotesEn',
        //'ext:count_living' => '',
        //'ext:count_dead' => '',
        'ext:sample_size' => 'MYGathering[0][MYNotesEn]',
        'ext:weather' => 'MYGathering[0][MYNotesEn]',
        'ext:sampling_duration' => 'MYGathering[0][MYNotesEn]',
        'ext:biotope_moisture' => 'MYGathering[0][MYHabitatDescriptionEn]',
        //'ext:biotope_stoniness' => '',
        'ext:labcode' => 'MYAdditionalIDs',
        //'ext:data_source' => '',
        'ext:dna_proofed' => 'MYNotesEn',
    );

    private $specials = array(
        'ext' => 'processExt',
        'entered' => 'convertDate',
        'date_begin' => 'convertDate',
        'date_end' => 'convertDate',
        'specimen_location' => 'specimenLocation',
        'record_level' => 'recordLevel',
        'interpreted_data' => 'interpretedData',
        'taxon_rank' => 'taxonRank',
        'type_status' => 'typeStatus',
        'higher_geography' => 'higherGeography',
        'unreliable_fields' => 'unreliableFields',
        'province' => 'province',
        'country' => 'pickUnreliable',
        'municipality' => 'pickUnreliable',
        'ext:associated_taxa' => 'addText',
        'ext:time_begin' => 'addText',
        'ext:time_end' => 'addText',
        'ext:other_id' => 'addText',
        'ext:specimen_collection_id' => 'addText',
        'ext:sample_size' => 'addText',
        'ext:weather' => 'addText',
        'ext:sampling_duration' => 'addText',
        'ext:biotope_moisture' => 'addText',
        'ext:dna_proofed' => 'addText',
        'ext:labcode' => 'addText'
    );

    private $addText = array(
        'ext:other_id' => 'MZH: %s',
        'ext:time_end' => ' - %s ',
        'ext:time_begin' => '%s - ',
        'ext:specimen_collection_id' => 'specimen_collection_id: %s ',
        'ext:sample_size' => 'sample size: %s ',
        'ext:dna_proofed' => 'DNA proofed: %s ',
        'ext:weather' => 'weather: %s ',
        'ext:sampling_duration' => 'sampling duration: %s ',
        'ext:biotope_moisture' => 'biotope moisture: %s ',
        'ext:labcode' => 'labcode: %s ',
        'ext:associated_taxa' => 'associated taxa: %s ',
    );

    private $taxonRankFix = array(
        'Coleoptera' => 'order',
        'Diptera' => 'order',
        'Hymenoptera' => 'order',
        'Hemiptera' => 'order',
        'Apion' => 'genus',
        'Curculionidae' => 'family',
        'Alticinae' => 'subfamily',
        'Coccinellidae' => 'family',
        'Anaspis' => 'genus',
        'Tachinus schneideri' => 'species',
        'Meligethes' => 'genus',
        'Dasytes' => 'genus',
        'Orthoptera' => 'order',
        'Cantharidae' => 'family',
        'Oedemeridae' => 'family',
        'Formicidae' => 'family',
        'Bruchus' => 'genus',
        'Bruchidae' => 'subfamily',
        'Latridiidae' => 'family',
    );

    private $separators = array(
        'MYGathering[0][MYUnit][0][MYIdentification][0][MYDetDate]' => ' - ',
        'MYAdditionalIDs' => self::JOIN_AS_ARRAY,
    );

    public function __construct()
    {
        $this->normalizerService = new NormalizerService();
    }

    public function setProgressBarAdapter(AbstractAdapter $adapter)
    {
        $this->progressBarAdapter = $adapter;
    }

    public function getAll(AdapterInterface $adapter)
    {
        $sql = new Sql($adapter);
        $table = new TableIdentifier('data3', 'dbo');
        $select = new Select($table);
        //$select->limit(100)->offset(1);
        $select->where->and->in('collection_id', [
            'Coll. Tomi Salin',
            'Coll. Reima Leinonen & Guy Söderman',
            'Åbo Akademi',
            'Coll. Martti Raekunnas',
            'Coll. Salin',
            'Coll. Andrei Humala',
            'Coll. Karhu',
            'Coll. Jussi Vilen',
            'Coll. Raekunnas',
            'Forssan luonnontieteellinen museo',
            'Coll. Iiro Kakko',
            'Coll. Tommi Mäkelä',
            'Coll. Ilkka Teräs',
            'ZIN',
            'Coll. Soon',
            'Coll. Pekka Valtonen',
            'Coll. Niklas Johansson',
        ]);
        //$select->where->and->equalTo('namespace_id', 'GM')->equalTo('primary_data ', 2);
        $statement = $sql->prepareStatementForSqlObject($select);

        return $this->convertResultsToKotkaArray($statement->execute());
    }

    public function convertResultsToKotkaArray(ResultInterface $results)
    {
        $data = [];
        if ($this->progressBarAdapter === null) {
            foreach ($results as $result) {
                $data[] = $this->processItem($result);
            }
            return $data;
        }
        $progress = new ProgressBar($this->progressBarAdapter, 0, $results->count());
        foreach ($results as $result) {
            $data[] = $this->processItem($result);
            $progress->next();
        }
        $progress->finish();
        return $data;
    }

    protected function processItem(array $item)
    {
        $this->specimen = [];
        foreach ($item as $old => $value) {
            $value = trim($value);
            if (!isset($this->mapping[$old]) || empty($value)) {
                continue;
            }
            if (isset($this->specials[$old])) {
                $method = $this->specials[$old];
                $this->$method($value, $this->mapping[$old], $old);
                continue;
            }
            $this->setSpecimenField($this->mapping[$old], $value);
        }
        $specimen = [];
        parse_str(http_build_query($this->specimen), $specimen);

        return $specimen;
    }

    protected function processExt($data)
    {
        $parser = xml_parser_create();
        xml_parse_into_struct($parser, $data, $vals);
        foreach ($vals as $node) {
            if ($node['type'] !== 'complete' || $node['tag'] == 'EXT') {
                continue;
            }
            $value = isset($node['value']) ? trim($node['value']) : null;
            $tag = 'ext:' . strtolower($node['tag']);
            if (isset($this->specials[$tag])) {
                $method = $this->specials[$tag];
                $this->$method($value, $this->mapping[$tag], $tag);
                continue;
            }
            if (!isset($this->mapping[$tag])) {
                continue;
            }
            $this->setSpecimenField($this->mapping[$tag], $value);
        }
    }

    protected function pickUnreliable($value, $newField)
    {
        if (strpos($value, '?') !== false) {
            $value = trim(str_replace('?', '', $value));
            $this->setSpecimenField('MYUnreliableFields', $newField);
        }
        $this->setSpecimenField($newField, $value);
    }

    protected function typeStatus($data, $newField)
    {
        if ($data === 'doubtful') {
            $this->specimen['MYGathering[0][MYUnit][0][MYTypeSpecimen][0][MYTypeVerification]'] = 'MY.typeVerificationDoubtful';
        }
        $this->setSpecimenField($newField, $data);
    }

    protected function specimenLocation($value, $newField)
    {
        if ($value === 'Not known (missing)') {
            $this->specimen['MYStatus'] = 'MY.statusMissing';
        } else {
            $this->setSpecimenField($newField, $value);
        }
    }

    protected function recordLevel($value, $newField)
    {
        if ($value === 'unit') {
            $this->setSpecimenField($newField, 'Digitized as an unit (box)');
        }
    }

    protected function addText($value, $newField, $tag)
    {
        if (!isset($this->addText[$tag])) {
            throw new \Exception("Could not find prepend value for " . $tag);
        }
        $value = sprintf($this->addText[$tag], $value);
        $this->setSpecimenField($newField, $value);
    }

    protected function interpretedData($value, $newField)
    {
        if ($value == 1) {
            $this->setSpecimenField($newField, '[Data interpreted]');
        }
    }

    protected function taxonRank($value, $newField)
    {
        $taxon = null;
        if (isset($this->specimen[$this->specials['taxon_rank']])) {
            $taxon = trim($this->specimen[$this->specials['taxon_rank']]);
        }
        if ($taxon === 'Lepidoptera' && $value === 'family') {
            $this->setSpecimenField($newField, 'order');
        }
        if (!empty($value)) {
            $this->setSpecimenField($newField, $value);
        }
        if (isset($this->taxonRankFix[$taxon])) {
            $this->setSpecimenField($newField, $this->taxonRankFix[$taxon]);
        }
    }

    protected function higherGeography($value, $newField)
    {
        if ($value != 1 && $value !== null) {
            $this->setSpecimenField($newField, $value);
        }
    }

    protected function unreliableFields($value, $newField)
    {
        $parts = explode(',', $value);
        if (count($parts) == 1) {
            $parts = explode(';', $value);
        }
        $new = [];
        foreach ($parts as $part) {
            $part = trim($part);
            $new[] = $this->mapping[$part];
        }
        $this->setSpecimenField($newField, implode(UnreliableFields::UNRELIABLE_FIELD_SEPARATOR, $new));
    }

    protected function convertDate($date, $newField)
    {
        try {
            $dateTime = \DateTime::createFromFormat(self::DATE_FORMAT, $date);
            if ($dateTime instanceof \DateTime) {
                $this->specimen[$newField] = $dateTime->format(Util::DATETIME_FORMAT);
                return;
            }
        } catch (\Exception $e) {
        }
        $this->setSpecimenField($newField, $date);
    }

    protected function province($value, $newField)
    {
        $newValue = $this->normalizerService->normalize('MYBiologicalProvinceEn', $value, false);
        if ($newValue === false) {
            $this->setSpecimenField('MYGathering[0][MYAdministrativeProvinceEn]', $value);
        } else {
            $this->setSpecimenField($newField, $value);
        }
    }

    private function setSpecimenField($field, $value)
    {
        $value = $this->normalizerService->normalize(Util::getFieldFromQueryStr($field), $value, $value);
        $separator = "\r\n";
        $hasValue = isset($this->specimen[$field]);
        if (isset($this->separators[$field])) {
            if ($this->separators[$field] === self::JOIN_AS_ARRAY) {
                if (!$hasValue) {
                    $this->specimen[$field] = [];
                }
                $this->specimen[$field][] = $value;
                return;
            }
            $separator = $this->separators[$field];
        }
        if ($hasValue) {
            $this->specimen[$field] .= $separator . $value;
        } else {
            $this->specimen[$field] = $value;
        }
    }

} 