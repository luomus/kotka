<?php
namespace Importer\Service;

use DateTime;

class ConverterService
{

    protected $cache = array();

    protected $bioGeographicNotFound = null;
    protected $bioGeographic = array(
        // FI full  => FI short, latin plant short, latin, latin insect sort
        // missing if no transformation needed
        'Ahvenanmaa' => array('A', 'Alandia', 'Al'),
        'Varsinais-Suomi' => array('V', 'Ab', 'Regio aboënsis'),
        'Uusimaa' => array('U', 'N', 'Nylandia'),
        'Etelä-Karjala' => array('EK', 'Ka', 'Karelia australis'),
        'Satakunta' => array('St', 'Satakunta'),
        'Etelä-Häme' => array('EH', 'Ta', 'Tavastia australis'),
        'Etelä-Savo' => array('ES', 'Sa', 'Savonia australis'),
        'Laatokan Karjala' => array('LK', 'Kl', 'Karelia ladogensis'),
        'Etelä-Pohjanmaa' => array('EP', 'Oa', 'Ostrobottnia australis'),
        'Pohjois-Häme' => array('PH', 'Tb', 'Tavastia borealis'),
        'Pohjois-Savo' => array('PS', 'Sb', 'Savonia borealis'),
        'Pohjois-Karjala' => array('PK', 'Kb', 'Karelia borealis'),
        'Keski-Pohjanmaa' => array('KP', 'Om', 'Ostrobottnia media'),
        'Kainuu' => array('Kn', 'Ostrobottnia kajanensis', 'Ok'),
        'Oulun Pohjanmaa' => array('OP', 'Obo', 'Ostrobottnia ouluensis', 'Oba'),
        'Perä-Pohjanmaa' => array('PeP', 'Obu', 'Ostrobottnia ultima', 'Obb'),
        'Koillismaa' => array('Ks', 'Regio kuusamoënsis'),
        'Kittilän Lappi' => array('KiL', 'Lkk', 'Lapponia kittilensis', 'Lkoc'),
        'Sompion Lappi' => array('SoL', 'Lks', 'Lapponia sompiensis', 'Lkor'),
        'Enontekiön Lappi' => array('EnL', 'Le', 'Lapponia enontekiensis'),
        'Inarin Lappi' => array('InL', 'Li', 'Lapponia inarensis'),
        'Aunuksen Karjala' => array('AK', 'Kol', 'Karelia olonetsensis'),
        'Äänisen Karjala' => array('ÄK', 'Kon', 'Karelia onegensis'),
        'Äänisen takainen Karjala' => array('ÄtK', 'Kton', 'Karelia transonegensis'),
        'Läntinen pomoria' => array('LPom', 'Kpoc', 'Karelia pomorica occidentalis'),
        'Itäinen pomoria' => array('IPom', 'Ipoc', 'Karelia pomorica orientalis'),
        'Kemin Lappi' => array('KemL', 'Lkem', 'Lapponia kemensis'),
        'Kieretin Karjala' => array('KerK', 'Kk', 'Karelia keretina'),
        'Imanteron Lappi' => array('ImL', 'Lim', 'Lapponia imandrae'),
        'Varsugan Lappi' => array('VL', 'Lv', 'Lapponia varsugar'),
        'Petsamon Lappi' => array('PsL', 'Lps', 'Lapponia petsamoensis'),
        'Ponoin Lappi' => array('PoL', 'Lp', 'Lapponia ponojensis'),
        'Tuuloman Lappi' => array('TL', 'Lt', 'Lapponia tulomensis'),
        'Muurmannin Lappi' => array('MrL', 'Lmur', 'Lapponia murmanica'),
    );

    protected $sexNotFound = null;
    protected $sex = array(
        'M' => array('male'),
        'F' => array('female'),
        'U' => array('unknown', 'unknowable'),
        'N' => array('not applicable'),
        'X' => array('mixed'),
        'W' => array('worker'),
    );

    protected $collectionIDNotFound = false;
    protected $collectionID = array(
        'http://id.luomus.fi/HR.407' => array('Coll. von Bonsdorff', 'coll. von Bonsdoff'),
    );

    protected $languageNotFound = 'other';
    protected $language = array(
        'finnish' => array('fi', 'fin'),
        'english' => array('en', 'eng', 'engs'),
        'swedish' => array('sv', 'swe'),
        'sami' => array('se', 'sme'),
        'russian' => array('ru', 'rus'),
        'estonian' => array('et', 'est'),
        'german' => array('de', 'deu', 'ger', 'deus'),
        'french' => array('fr', 'fra', 'fre', 'fras'),
        'spanish' => array('es', 'spa'),
        'chinese' => array('zh', 'zho', 'chi'),
        'latin' => array('la', 'lat', 'lats'),
    );

    protected $lifeStageNotFound = '';
    protected $lifeStage = array(
        'egg' => array('ovo'),
        'larva' => array(),
        'pupa' => array('cocoon'),
        'subimago' => array(),
        'adult' => array('adiult'),
        'fertile' => array(),
        'sterile' => array(),
    );

    protected $samplingMethodNotFound = '';
    protected $samplingMethod = array(
        'light' => array('ad lucem', 'valopyydys', 'UV valo'),
    );

    protected $georeferenceSource = array(
        'geolocate' => array(),
        'karttapaikka' => array(),
        'google' => array(),
    );

    public function get($field, $value, $notFound = null, $usePartOfCheck = false, $getKey = -1)
    {
        if ($value === null) {
            return null;
        }
        if (!property_exists($this, $field)) {
            throw new \Exception(sprintf("Could not find translation for '%s'.", $field));
        }
        if (!isset($this->cache[$field])) {
            $this->createCache($field);
        }
        $lowValue = strtolower($value);

        if ($usePartOfCheck) {
            foreach ($this->cache[$field] as $check => $target) {
                if (strpos($lowValue, $check) === false) {
                    continue;
                }
                return $target;
            }
        } else {
            if (isset($this->cache[$field][$lowValue])) {
                if ($getKey > -1) {
                    $key = $this->cache[$field][$lowValue];
                    if (isset($this->{$field}[$key][$getKey])) {
                        return $this->{$field}[$key][$getKey];
                    }
                }
                return $this->cache[$field][$lowValue];
            }
        }
        return $notFound;
    }

    public function convertDate($year, $month, $day)
    {
        $original = $year . '-' . $month . '-' . $day;
        $dates = array();
        $dateEnd = $dateBegin = array();
        $hasEnd = false;

        $year = str_replace(array('[', ']', '?', '.', ' '), '', $year);
        $month = str_replace(array('[', ']', '?', '.', ' '), '', $month);
        $day = str_replace(array('[', ']', '?', '.', ' '), '', $day);

        // year
        if (!empty($year)) {
            $inter = preg_split('{[\/\-]}', $year);
            $dateBegin[2] = $inter[0];
            if (isset($inter[1])) {
                $dateEnd[2] = $inter[1];
                $hasEnd = true;
            } else {
                $dateEnd[2] = $inter[0];
            }
        } else {
            $dateBegin[2] = '????';
            $dateEnd[2] = '????';
        }

        // Month
        if (!empty($month)) {
            $inter = preg_split('{[\/\-]}', $month);
            $dateBegin[1] = $inter[0];
            if (isset($inter[1])) {
                $dateEnd[1] = $inter[1];
                $hasEnd = true;
            } else {
                $dateEnd[1] = $inter[0];
            }
        } else {
            $dateBegin[1] = '01';
            $dateEnd[1] = '12';
        }

        // Day
        if (!empty($day)) {
            $inter = preg_split('{[\/\-]}', $day);
            $dateBegin[0] = $inter[0];
            if (isset($inter[1])) {
                $dateEnd[0] = $inter[1];
                $hasEnd = true;
            } else {
                $dateEnd[0] = $inter[0];
            }
        } else {
            $dateBegin[0] = '01';
            $year = isset($dateEnd[2]) ? $dateEnd[2] : '????';
            $month = isset($dateEnd[1]) ? $dateEnd[1] : '12';
            if ($year == '????') {
                $year = '2012';
            }
            if (is_numeric($year) && is_numeric($month)) {
                try {
                    $date = new DateTime($year . '-' . $month . '-03');
                    $dateEnd[0] = $date->format('t');
                } catch (\Exception $e) {
                    return array('dateBegin' => $original);
                }
            } else {
                return array('dateBegin' => $original);
            }
        }
        ksort($dateBegin);
        ksort($dateEnd);
        if (empty($year) || empty($month) || empty($day)) {
            $hasEnd = true;
        }
        if ($hasEnd) {
            $dates['dateEnd'] = implode('.', $dateEnd);
        }
        $dates['dateBegin'] = implode('.', $dateBegin);

        return $dates;
    }

    private function createCache($field)
    {
        $array = array();
        foreach ($this->$field as $target => $list) {
            foreach ($list as $value) {
                $lower = strtolower($value);
                if (isset($array[$lower])) {
                    throw new \Exception("Could not specify unique mapping in '$field'");
                }
                $array[$lower] = $target;
            }
            $array[strtolower($target)] = $target;
        }
        $this->cache[$field] = $array;
    }
} 