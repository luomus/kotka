<?php
namespace Importer\Service;

use Importer\Maps\MapperInterface;
use Zend\Filter\StaticFilter;


/**
 * Class NormalizerService normalizes the field values.
 *
 * This is same as ConverterService but with data from ontology database
 *
 * @package Importer\Service
 */
class NormalizerService
{

    protected $cache = array();

    protected $filters = array(
        'MYLeg' => ['StringToArray', ['delimiter' => ';']],
        'MYDuplicatesIn' => 'AddDomain',
        'MYDepth' => 'ConvertToMeters',
    );

    public function __construct()
    {
        StaticFilter::getPluginManager()
            ->setInvokableClass('StringToArray', 'Kotka\Filter\StringToArray')
            ->setInvokableClass('AddDomain', 'Importer\Filter\AddDomain')
            ->setInvokableClass('ConvertToMeters', 'Importer\Filter\ConvertToMeters');
    }

    public function normalize($field, $value, $default = null)
    {
        if ($value === null) {
            return null;
        }
        if (is_array($value)) {
            $result = [];
            foreach ($value as $key => $val) {
                $result[$key] = $this->_normalize($field, $val, $val);
            }
            return $result;
        }
        return $this->_normalize($field, $value, $default);
    }

    public function _normalize($field, $value, $default = null)
    {
        if (isset($this->filters[$field])) {
            if (is_array($this->filters[$field])) {
                return StaticFilter::execute($value, $this->filters[$field][0], $this->filters[$field][1]);
            } else {
                return StaticFilter::execute($value, $this->filters[$field]);
            }
        }
        if (!isset($this->cache[$field])) {
            $class = '\\Importer\\Maps\\' . $field;
            if (!class_exists($class) || !method_exists($class, 'getNormalizedValues')) {
                return $default;
            }
            $object = new $class;
            $this->cache[$field] = $object->getNormalizedValues();
        }
        $lowValue = mb_strtolower($value, 'UTF-8');
        if (isset($this->cache[$field][$lowValue])) {
            return $this->cache[$field][$lowValue];
        }
        return $default;
    }
}