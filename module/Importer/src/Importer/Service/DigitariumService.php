<?php

namespace Importer\Service;

use Kotka\Model\ExcelColumn;
use Kotka\Model\ExcelSheet;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\TableIdentifier;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Zend\ServiceManager\ServiceLocatorInterface;

class DigitariumService implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    public function getAllInExcelSheet()
    {
        /** @var Adapter $adapter */
        $adapter = $this->getServiceLocator()->get('digitarium_adapter');
        $sql = new Sql($adapter);
        $table = new TableIdentifier('harvest');
        $select = new Select($table);
        $select->where->equalTo('in_kotka', 0);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $columns = [];
        foreach ($results as $idx => $digi) {
            /** @var \Importer\Model\Digitarium $digi */
            $data = json_decode($digi['json'], true);
            if (!is_array($data)) {
                continue;
            }
            foreach ($data as $key => $value) {
                if (!isset($columns[$key])) {
                    $columns[$key] = [];
                }
                $columns[$key][$idx] = $value;
            }
        }
        $sheet = new ExcelSheet();
        foreach ($columns as $header => $data) {
            $column = new ExcelColumn();
            $column->setField($header);
            $column->setLabel($header);
            $column->setData($data);
            $sheet->addColumn($column);
        }
        return $sheet;
    }

    public function getAllXmlInExcelSheet($directory) {
        $files = scandir($directory);
        $processed = array();
        $idx = -1;
        foreach ($files as $file) {
            if (substr($file,0,1) === '.') {
                continue;
            }
            $idx++;
            $processed[$idx] = [];
            $fullFile = $directory . DIRECTORY_SEPARATOR . $file;
            if ($fullFile !== 'Sanikkaiset_updated/EIG.10000.xml') {
               // continue;
            }
            $xmlStr = file_get_contents($fullFile);
            $xmlStr = preg_replace('/xmlns[^=]*="[^"]*"/i', '', $xmlStr);
            $xmlStr = preg_replace('/xsi[^=]*="[^"]*"/i', '', $xmlStr);
            $xmlStr = preg_replace('/dwr:DarwinRecordSet\s*/i', 'DarwinRecordSet', $xmlStr);
            $xmlStr = preg_replace('/[a-zA-Z]+:([a-zA-Z]+[=>])/', '$1', $xmlStr);
            $xml = @simplexml_load_string($xmlStr);
            $json = json_encode($xml);
            $array = json_decode($json, true);
            foreach($array as $subarr) {
                foreach($subarr as $key => $value) {
                    if (isset($processed[$idx][$key]) && $processed[$idx][$key] !== $value && $value !== 'http://id.luomus.fi/$id$') {
                        echo "overriding!!! in $fullFile $key";
                        exit();
                    }
                    if (is_array($value) && count($value) == 1) {
                        $value = array_pop($value);
                    }
                    $processed[$idx][$key] = $value;
                }
            }
        }
        $columns = [];
        foreach ($processed as $idx => $data) {
            foreach ($data as $key => $value) {
                if (!isset($columns[$key])) {
                    $columns[$key] = [];
                }
                $columns[$key][$idx] = $value;
            }
        }
        $sheet = new ExcelSheet();
        foreach ($columns as $header => $data) {
            $column = new ExcelColumn();
            $column->setField($header);
            $column->setLabel($header);
            $column->setData($data);
            $sheet->addColumn($column);
        }
        return $sheet;
    }
}