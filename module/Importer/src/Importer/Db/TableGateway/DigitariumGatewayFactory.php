<?php

namespace Importer\Model;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class DigitariumGatewayFactory is a factory class for table gateway
 * @package KotkaConsole\Service
 */
class DigitariumGatewayFactory implements FactoryInterface
{
    /**
     * Creates the service
     * @param ServiceLocatorInterface $serviceLocator
     * @return TableGateway
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $dbAdapter = $serviceLocator->get('digitarium_adapter');
        $resultSetPrototype = new ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new Digitarium());
        return new TableGateway('Harvest', $dbAdapter, null, $resultSetPrototype);
    }
}
