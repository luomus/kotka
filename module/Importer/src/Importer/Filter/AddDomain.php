<?php

namespace Importer\Filter;


use Common\Service\IdService;
use Zend\Filter\Exception;
use Zend\Filter\FilterInterface;

class AddDomain implements FilterInterface
{

    /**
     * Returns the result of filtering $value
     *
     * @param  mixed $value
     *
     * @throws Exception\RuntimeException If filtering $value is impossible
     * @return mixed
     */
    public function filter($value)
    {
        return IdService::getUri($value);
    }
}