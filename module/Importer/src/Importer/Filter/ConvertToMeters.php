<?php

namespace Importer\Filter;


use Zend\Filter\Exception;
use Zend\Filter\FilterInterface;

class ConvertToMeters implements FilterInterface
{

    /**
     * Returns the result of filtering $value
     *
     * @param  mixed $value
     *
     * @throws Exception\RuntimeException If filtering $value is impossible
     * @return mixed
     */
    public function filter($value)
    {
        $type = filter_var($value, FILTER_SANITIZE_STRING);
        if (empty($type)) {
            return $value;
        }
        $int = filter_var($value, FILTER_SANITIZE_NUMBER_FLOAT);
        switch (strtolower($type)) {
            case ('ft'):
                $value = $int * 0.3048;
                break;
            case ('km'):
                $value = $int * 1000;
                break;
            case ('m'):
                $value = $int;
                break;
            default:
        }
        return $value;
    }
}