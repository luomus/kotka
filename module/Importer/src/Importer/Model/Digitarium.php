<?php
namespace Importer\Model;


class Digitarium
{

    public $json;
    public $in_kotka;
    public $harverstID;

    public function exchangeArray($data)
    {
        $this->json = (!empty($data['json'])) ? $data['json'] : null;
        $this->in_kotka = (!empty($data['in_kotka'])) ? $data['in_kotka'] : null;
        $this->harverstID = (!empty($data['harvestID'])) ? $data['harvestID'] : null;
    }

} 