<?php

namespace Importer\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class DigitariumTableFactory is a factory class for digitarium table factory
 * @package KotkaConsole\Service
 */
class DigitariumTableFactory implements FactoryInterface
{
    /**
     * Creates the service
     * @param ServiceLocatorInterface $serviceLocator
     * @return TableGateway
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $tableGateway = $serviceLocator->get('DigitariumTableGateway');
        return new DigitariumTable($tableGateway);
    }
}
