<?php
namespace Importer\Model;


use Zend\Db\TableGateway\TableGateway;

class DigitariumTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tg)
    {
        $this->tableGateway = $tg;
    }

    public function FetchAll()
    {
        return $this->tableGateway->select();
    }

    public function fetchAllNotInKotka()
    {
        return $this->tableGateway->select(array('in_kotka' => 0));
    }

    public function getById($id)
    {
        $rowSet = $this->tableGateway->select(array('harvestID' => (int)$id));
        $row = $rowSet->current();
        if (!$row) {
            throw new \Exception("Could not find digitarium data with harvest id '$id'");
        }
        return $row;
    }

    public function save(Digitarium $digi)
    {
        $data = array(
            'in_kotka' => $digi->in_kotka
        );
        $harvestId = (int)$digi->harverstID;
        if ($harvestId == 0) {
            throw new \Exception("Digitarium data missing harvest ID");
        } else {
            if ($this->getById($harvestId)) {
                $this->tableGateway->update($data, "harvestID = $harvestId");
            } else {
                throw new \Exception("Digitarium data doesn't exist");
            }
        }

    }

} 