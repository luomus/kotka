<?php
return array(
    'service_manager' => array(
        'invokables' => array(
            'Importer\Service\DigitariumService' => 'Importer\Service\DigitariumService',
            'Importer\Service\HyntikkaService' => 'Importer\Service\HyntikkaService',
        ),
        'factories' => array(
            'Importer\Model\DigitariumTable' => 'Importer\Model\DigitariumTableFactory',
            'DigitariumTableGateway' => 'Importer\Db\TableGateway\DigitariumGatewayFactory'
        )
    ),
);