<?php

namespace PdfCreator\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class TcpdfFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     *
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $config = $serviceLocator->get('Config');
        $defaults = $config["pdf"]["defaults"];
        $pdf = new \TCPDF($defaults['orientation'], $defaults['unit'], $defaults['page_format'], true, $defaults["encoding"], false);
        $lg = $defaults["language"];
        $pdf->setLanguageArray($lg);
        $pdf->SetTitle($defaults['title']);
        $pdf->SetCreator($config["pdf"]['creator']);
        $pdf->SetAuthor($config["pdf"]['author']);
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
        $pdf->SetAutoPageBreak(true);
        $pdf->setFontSubsetting($defaults['font_subsetting']);
        $pdf->SetFont($defaults['font_family'], '', $defaults['font_size']);

        return $pdf;
    }
}