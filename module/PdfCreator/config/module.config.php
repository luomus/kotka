<?php
return array(
    'service_manager' => array(
        'factories' => array(
            'tcpdf' => 'PdfCreator\Service\TcpdfFactory'
        )
    ),

    'pdf' => array(
        'creator' => 'Kotka Collection Management System',
        'author' => 'Finnish Museum of Natural History',

        'defaults' => array(
            'title' => 'Kotka',
            'keyword' => array(
                'Kotka'
            ),
            'encoding' => 'UTF-8',
            'orientation' => 'P',
            'unit' => 'mm',
            'page_format' => 'A4',
            'font_family' => 'dejavusans',
            'font_size' => 10,
            'font_subsetting' => false,
            'language' => array(
                'a_meta_charset' => 'UTF-8',
                'a_meta_dir' => 'ltr',
                'a_meta_language' => 'en',
                'w_page' => 'page',
            )
        )
    )
);
