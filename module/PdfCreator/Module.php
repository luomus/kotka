<?php
namespace PdfCreator;

use Zend\ModuleManager\Feature\ConfigProviderInterface;

/**
 * Pdf creator module creates the tcpdf service
 * @package PdfCreator
 */
class Module implements ConfigProviderInterface
{
    /**
     * gets the configs for the module
     *
     * This contains the basic setting for the pdf created using this module
     *
     * @return mixed
     */
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

}
