<?php
$env = getenv('KOTKA_ENV') ?: 'production';

return array(
    'router' => array(
        'routes' => array(
            'image_https' => array(
                'type' => 'scheme',
                'options' => array(
                    'scheme' => $env === 'dev' ? 'http' : 'https',
                    'defaults' => array(
                        'https' => true,
                    )
                ),
                'child_routes' => array(
                    'image' => array(
                        'type' => 'Literal',
                        'options' => array(
                            'route' => '/images',
                            'defaults' => array(
                                'controller' => 'Kotka\Controller\Image',
                                'action' => 'get'
                            )
                        ),
                        'child_routes' => array(
                            'get' => array(
                                'type' => 'Method',
                                'options' => array(
                                    'verb' => 'get',
                                    'defaults' => array(
                                        'controller' => 'Kotka\Controller\Image',
                                        'action' => 'get'
                                    )
                                )
                            ),
                            'post' => array(
                                'type' => 'Method',
                                'options' => array(
                                    'verb' => 'post',
                                    'defaults' => array(
                                        'controller' => 'Kotka\Controller\Image',
                                        'action' => 'post'
                                    )
                                )
                            ),
                            'put' => array(
                                'type' => 'Method',
                                'options' => array(
                                    'verb' => 'put',
                                    'defaults' => array(
                                        'controller' => 'Kotka\Controller\Image',
                                        'action' => 'put'
                                    )
                                )
                            ),
                            'delete' => array(
                                'type' => 'Method',
                                'options' => array(
                                    'verb' => 'delete',
                                    'defaults' => array(
                                        'controller' => 'Kotka\Controller\Image',
                                        'action' => 'delete'
                                    )
                                )
                            )
                        )

                    )
                )
            ),
            'http' => array(
                'type' => 'scheme',
                'options' => array(
                    'scheme' => 'http',
                    'defaults' => array(
                        'https' => false,
                    )
                ),
                'child_routes' => array(
                    'home' => array(
                        'type' => 'Literal',
                        'options' => array(
                            'route' => '/',
                            'defaults' => array(
                                'controller' => 'Kotka\Controller\Index',
                                'action' => 'http'
                            )
                        )
                    )
                )
            ),
            'types' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/types[/:action[/:id]]',
                    'defaults' => array(
                        'controller' => 'Kotka\Controller\Types',
                        'action' => 'index',
                    )
                )
            ),
            'seed' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/seed[/:action[/:id]]',
                    'defaults' => array(
                        'controller' => 'Kotka\Controller\Seed',
                        'action' => 'index',
                    )
                )
            ),
            'maps' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/maps[/:action]',
                    'defaults' => array(
                        'controller' => 'Kotka\Controller\Maps',
                        'action' => 'index',
                    )
                )
            ),
            'form' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/form[/:element[/:action]]',
                    'defaults' => array(
                        'controller' => 'Kotka\Controller\FormPartial',
                        'action' => 'index',
                    )
                )
            ),
            'picker' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/picker[/:type[/:action[/:element]]]',
                    'schema' => 'https',
                    'defaults' => array(
                        'controller' => 'Kotka\Controller\Picker',
                        'action' => 'index',
                        'type' => 'uri'
                    )
                )
            ),
            'excel' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/excel[/:action]',
                    'defaults' => array(
                        'controller' => 'Kotka\Controller\Excel',
                        'action' => 'create',
                    )
                )
            ),
            'document' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/documentation[/:action]',
                    'defaults' => array(
                        'controller' => 'Kotka\Controller\Document',
                        'action' => 'index',
                        'pagecache' => true,
                    )
                )
            ),
            'view' => array(
                'type' => 'literal',
                'options' => array(
                    'route' => '/view',
                    'defaults' => array(
                        'controller' => 'Kotka\Controller\View',
                        'action' => 'index',
                    )
                )
            ),
            'https' => array(
                'type' => 'scheme',
                'priority' => 100,
                'options' => array(
                    'scheme' => $env === 'dev' ? 'http' : 'https',
                    'defaults' => array(
                        'https' => true,
                    )
                ),
                'child_routes' => array(
                    'user' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/user[/:action]',
                            'constraints' => array(
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                                'controller' => 'User\Controller\User',
                                'action' => 'index',
                            ),
                        ),
                    ),
                    'home' => array(
                        'type' => 'Literal',
                        'priority' => 100,
                        'options' => array(
                            'route' => '/',
                            'defaults' => array(
                                'controller' => 'Kotka\Controller\Index',
                                'action' => 'index'
                            )
                        )
                    ),
                    'tools' => array(
                        'type' => 'segment',
                        'priority' => 101,
                        'options' => array(
                            'route' => '/tools[/:action[/:element]]',
                            'defaults' => array(
                                'controller' => 'Kotka\Controller\Tools',
                            )
                        )
                    ),
                    'file' => array(
                        'type' => 'segment',
                        'priority' => 90,
                        'options' => array(
                            'route' => '/file[/:element[/:subject[/:filename]]]',
                            'defaults' => array(
                                'controller' => 'Kotka\Controller\File',
                                'action' => 'index'
                            )
                        )
                    ),
                    'pdf' => array(
                        'type' => 'regex',
                        'priority' => 10,
                        'options' => array(
                            'regex' => '/pdf/(?<action>[a-zA-Z0-9_-]+)_(?<id>[a-zA-Z0-9\.:]+).pdf',
                            'defaults' => array(
                                'controller' => 'Kotka\Controller\Pdf',
                            ),
                            'spec' => '/pdf/%action%_%id%.pdf'
                        )
                    ),
                    'reports' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/reports[/:action]',
                            'defaults' => array(
                                'controller' => 'Kotka\Controller\Report',
                                'action' => 'index',
                                'pagecache' => false,
                            )
                        )
                    ),
                    'accession' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/accessions[/:action]',
                            'defaults' => array(
                                'controller' => 'Kotka\Controller\Accessions',
                                'action' => 'index'
                            )
                        )
                    ),
                    'culture' => array(
                        'type' => 'literal',
                        'priority' => 101,
                        'options' => array(
                            'route' => '/culture',
                            'defaults' => array(
                                'controller' => 'Kotka\Controller\Culture',
                                'action' => 'index',
                                'pagecache' => false,
                            )
                        ),
                        'may_terminate' => true,
                        'child_routes' => array(
                            'action' => array(
                                'type' => 'segment',
                                'options' => array(
                                    'route' => '/[:action]',
                                    'defaults' => array(
                                        'pagecache' => false,
                                    )
                                )
                            )
                        )
                    ),
                    'samples' => array(
                        'type' => 'literal',
                        'priority' => 101,
                        'options' => array(
                            'route' => '/samples',
                            'defaults' => array(
                                'controller' => 'Kotka\Controller\Samples',
                                'action' => 'index',
                                'pagecache' => false,
                            )
                        ),
                        'may_terminate' => true,
                        'child_routes' => array(
                            'action' => array(
                                'type' => 'segment',
                                'options' => array(
                                    'route' => '/[:action]',
                                    'defaults' => array(
                                        'pagecache' => false,
                                    )
                                )
                            )
                        )
                    ),
                    'collections' => array(
                        'type' => 'literal',
                        'priority' => 101,
                        'options' => array(
                            'route' => '/collections',
                            'defaults' => array(
                                'controller' => 'Kotka\Controller\Collections',
                                'action' => 'index',
                                'pagecache' => true,
                            )
                        ),
                        'may_terminate' => true,
                        'child_routes' => array(
                            'action' => array(
                                'type' => 'segment',
                                'options' => array(
                                    'route' => '/[:action]',
                                    'defaults' => array(
                                        'pagecache' => false,
                                    )
                                )
                            )
                        )
                    ),
                    'organizations' => array(
                        'type' => 'literal',
                        'priority' => 101,
                        'options' => array(
                            'route' => '/organizations',

                            'defaults' => array(
                                'controller' => 'Kotka\Controller\Organizations',
                                'action' => 'index',
                                'pagecache' => true,
                            ),
                        ),
                        'may_terminate' => true,
                        'child_routes' => array(
                            'action' => array(
                                'type' => 'segment',
                                'options' => array(
                                    'route' => '/[:action]',
                                    'defaults' => array(
                                        'action' => 'index',
                                        'pagecache' => false
                                    )
                                )
                            )
                        )
                    ),
                    'specimens' => array(
                        'type' => 'segment',
                        'priority' => 101,
                        'options' => array(
                            'route' => '[/:group]/specimens[/:action]',
                            'defaults' => array(
                                'controller' => 'Kotka\Controller\Specimens',
                                'action' => 'index',
                                'group' => ''
                            )
                        )
                    ),
                    'image_upload' => array(
                        'type' => 'segment',
                        'priority' => 101,
                        'options' => array(
                            'route' => '/specimens/images[/:action]',
                            'defaults' => array(
                                'controller' => 'Kotka\Controller\Image',
                                'action' => 'index'
                            )
                        )
                    ),
                    'taxon_images' => array(
                        'type' => 'Literal',
                        'priority' => 101,
                        'options' => array(
                            'route' => '/tools/taxon-images',
                            'defaults' => array(
                                'controller' => 'Kotka\Controller\Image',
                                'action' => 'taxon'
                            )
                        )
                    ),
                    'search_specimens' => array(
                        'type' => 'segment',
                        'priority' => 101,
                        'options' => array(
                            'route' => '/specimens/search[/:action]',
                            'defaults' => array(
                                'controller' => 'Kotka\Controller\Search',
                                'action' => 'index'
                            )
                        )
                    ),
                    'count_specimens' => array(
                        'type' => 'segment',
                        'priority' => 101,
                        'options' => array(
                            'route' => '/specimens/count',
                            'defaults' => array(
                                'controller' => 'Kotka\Controller\Search',
                                'action' => 'count'
                            )
                        )
                    ),
                    'search_branch' => array(
                        'type' => 'segment',
                        'priority' => 101,
                        'options' => array(
                            'route' => '/specimens/search-branch[/:action]',
                            'defaults' => array(
                                'controller' => 'Kotka\Controller\Search',
                                'action' => 'branch'
                            )
                        )
                    ),
                    'search_sample' => array(
                        'type' => 'segment',
                        'priority' => 101,
                        'options' => array(
                            'route' => '/specimens/search-sample[/:action]',
                            'defaults' => array(
                                'controller' => 'Kotka\Controller\Search',
                                'action' => 'sample'
                            )
                        )
                    ),
                    'js' => array(
                        'type' => 'segment',
                        'priority' => 10,
                        'options' => array(
                            'route' => '/js/data/[:action].json',
                            'defaults' => array(
                                'controller' => 'Kotka\Controller\Js',
                            ),
                        )
                    ),
                )
            ),
        )
    ),
    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
            'Kotka\Service\Factory\MapperAbstractFactory',
        ),
        'aliases' => array(
            'translator' => 'MvcTranslator',
            'metadataService' => 'Kotka\Service\CollectionService',
            'datasetsService' => 'Kotka\Service\DatasetService',
            'transactionService' => 'Kotka\Service\TransactionService',
            'organizationService' => 'Kotka\Service\OrganizationService',
            'zoospecimenService' => 'Kotka\Service\SpecimenService',
            'botanyspecimenService' => 'Kotka\Service\SpecimenService',
            'sampleService' => 'Kotka\Service\SampleService',
            'cache' => 'Kotka\Cache',
        ),
        'invokables' => array(
            'Kotka\Service\NewKotkaService' => 'Kotka\Service\NewKotkaService',
            'Kotka\Form\Builder\ArrayBuilder' => 'Kotka\Form\Builder\ArrayBuilder',
            'Kotka\Service\ValidationService' => 'Kotka\Service\ValidationService',
            'Kotka\Service\AccessionService' => 'Kotka\Service\AccessionService',
            'Kotka\Service\ObjectFactoryService' => 'Kotka\Service\ObjectFactoryService',
            'Kotka\Service\LabelService' => 'Kotka\Service\LabelService',
            'Kotka\Service\FormService' => 'Kotka\Service\FormService',
            'Kotka\Service\FormElementService' => 'Kotka\Service\FormElementService',
            'Kotka\Service\EmailService' => 'Kotka\Service\EmailService',
            'Kotka\Service\TaxonService' => 'Kotka\Service\TaxonService',
            'Kotka\Service\NewsService' => 'Kotka\Service\NewsService',
            'Kotka\Listener\UserActionListener' => 'Kotka\Listener\UserActionListener',
            'Kotka\Service\UserStatsService' => 'Kotka\Service\UserStatsService',
            'Kotka\Service\LajiForm' => 'Kotka\Service\LajiForm',
            'Kotka\Transform\Lutikka' => 'Kotka\Transform\Lutikka',
        ),
        'factories' => array(
            'DocumentVersionService' => 'Kotka\Service\VersionServiceFactory',
            'Kotka\Service\SequenceService' => 'Kotka\Service\Factory\SequenceServiceFactory',
            'Kotka\Service\CoordinateService' => 'Kotka\Service\Factory\CoordinateService',
            'Kotka\Service\CollectionService' => 'Kotka\Service\Factory\CollectionServiceFactory',
            'Kotka\Service\SampleService' => 'Kotka\Service\Factory\SampleServiceFactory',
            'Kotka\Service\OrganizationService' => 'Kotka\Service\Factory\OrganizationServiceFactory',
            'Kotka\Service\DatasetService' => 'Kotka\Service\Factory\DatasetServiceFactory',
            'Kotka\Service\SpecimenService' => 'Kotka\Service\Factory\SpecimenServiceFactory',
            'Kotka\Service\TransactionService' => 'Kotka\Service\Factory\TransactionServiceFactory',
            'Common\Service\IdService' => 'Kotka\Service\Factory\IdServiceFactory',
            'Kotka\Service\RecordService' => 'Kotka\Service\Factory\RecordServiceFactory',
            'Kotka\Service\StatsService' => 'Kotka\Service\Factory\StatsServiceFactory',
            'Kotka\Service\MustikkaService' => 'Kotka\Service\Factory\MustikkaServiceFactory',
            'Kotka\Service\PdfService' => 'Kotka\Service\Factory\PdfServiceFactory',
            'Kotka\Service\FileService' => 'Kotka\Service\Factory\FileServiceFactory',
            'Kotka\Service\ErrorHandlingService' => 'Kotka\Service\Factory\ErrorHandlingServiceFactory',
            'Kotka\Service\ImportService' => 'Kotka\Service\Factory\ImportServiceFactory',
            'Kotka\Service\FormGeneratorService' => 'Kotka\Service\Factory\FormGeneratorServiceFactory',
            'Logger' => 'Kotka\Service\Factory\LoggerFactory',
            'Kotka\Cache' => 'Zend\Cache\Service\StorageCacheFactory',
            'perma-cache' => 'Kotka\Service\Factory\PermaCache',
            'Kotka\Filter\File\RenameUpload' => 'Kotka\Filter\File\RenameUploadFactor',
            'Kotka\DataGrid\ImportGrid' => 'Kotka\DataGrid\ImportGridFactory',
            'Kotka\DataGrid\SearchGrid' => 'Kotka\DataGrid\SearchGridFactory',
            'Kotka\Service\ExcelService' => 'Kotka\Service\Factory\ExcelServiceFactory',
            'Kotka\Service\ElasticStats' => 'Kotka\Service\Factory\ElasticStats',
            'Kotka\Options\ExternalsOptions' => 'Kotka\Factory\ExternalsOptionsFactory',
            'Zend\Session\SessionManager' => 'Zend\Session\Service\SessionManagerFactory',
            'Zend\Session\Config\ConfigInterface' => 'Zend\Session\Service\SessionConfigFactory',
            'Kotka\Http\TipuApiClient' => 'Kotka\Factory\TipuApiClientFactory',
            'mail-transport' => 'Kotka\Service\Factory\MailTransport'
        ),
    ),
    'form_elements' => array(
        'invokables' => array(
            'DatabaseSelect' => 'Kotka\Form\Element\DatabaseSelect',
            'DatabaseRadio' => 'Kotka\Form\Element\DatabaseRadio',
            'Select' => 'Kotka\Form\Element\Select',
            'OwnerOrganization' => 'Kotka\Form\Element\OwnerOrganization',
            'Language' => 'Kotka\Form\Element\Language',
            'ArrayCollection' => 'Kotka\Form\Element\ArrayCollection',
            'KotkaCollection' => 'Kotka\Form\Element\Collection',
            'KotkaDate' => 'Kotka\Form\Element\Date',
            'Resource' => 'Kotka\Form\Element\Resource',
            'PrefixText' => 'Kotka\Form\Element\PrefixText',
        ),
        'factories' => array(
            'Kotka\Form\GenericLabel' => 'Kotka\Form\Factory\GenericLabelFactory'
        )
    ),
    'validators' => array(
        'invokables' => array(
            'ConditionalList' => 'Kotka\Validator\ConditionalList',
            'UniqueDatasetName' => 'Kotka\Validator\UniqueDatasetName',
            'UniqueADGroup' => '\Kotka\Validator\UniqueADGroup',
            'RequireWithThis' => 'Kotka\Validator\RequireWithThis',
            'IircNumber' => 'Kotka\Validator\IircNumber',
            'HasIRCCChecks' => 'Kotka\Validator\HasIRCCChecks',
            'RequiredWhen' => 'Kotka\Validator\RequiredWhen',
            'AllowedFields' => 'Kotka\Validator\AllowedFields',
            'InDatabaseSelect' => 'Kotka\Validator\InDatabaseSelect',
            'InArrayCaseInsensitive' => 'Kotka\Validator\InArrayCaseInsensitive',
            'UserInOrganization' => 'Kotka\Validator\UserInOrganization',
            'Cycle' => 'Kotka\Validator\Cycle',
            'DateCompare' => 'Kotka\Validator\DateCompare',
            'NumberCompare' => 'Kotka\Validator\NumberCompare',
            'OneOrTheOther' => 'Kotka\Validator\OneOrTheOther',
            'BothNotSet' => 'Kotka\Validator\BothNotSet',
            'Coordinate' => 'Kotka\Validator\Coordinate',
            'Float' => 'Kotka\Validator\FloatValidator',
            'FloatRange' => 'Kotka\Validator\FloatRange',
            'StartsWith' => 'Kotka\Validator\StartsWith',
            'OnlyOnePrimarySpecimen' => 'Kotka\Validator\OnlyOnePrimarySpecimen',
        ),
    ),
    'filters' => array(
        'invokables' => array(
            'Kotka\InputFilter\GenericLabelWarning' => 'Kotka\InputFilter\GenericLabelWarning',
            'GXDatasetFilter' => 'Kotka\InputFilter\GXDatasetFilter',
            'MOSOrganizationFilter' => 'Kotka\InputFilter\MOSOrganizationFilter',
            'PrefixFilter' => 'Kotka\Filter\PrefixFilter',
            'NormalizeDatabaseSelect' => 'Kotka\Filter\NormalizeDatabaseSelect',
            'PrefixRemover' => 'Kotka\Filter\PrefixRemover',
            'MoveTo' => 'Kotka\Filter\MoveTo',
            'ForceArray' => 'Kotka\Filter\ForceArray',
            'EmptyArray' => 'Kotka\Filter\EmptyArray',
            'Coordinate' => 'Kotka\Filter\Coordinate',
            'DefaultQName' => 'Kotka\Filter\DefaultQName',
            'UnreliableFields' => 'Kotka\Filter\UnreliableFields',
            'FloatNormalizer' => 'Kotka\Filter\FloatNormalizer',
            'ArrayStringTrim' => 'Kotka\Filter\ArrayStringTrim',
            'StringToArray' => 'Kotka\Filter\StringToArray',
            'ExcelTimeToDateTime' => 'Kotka\Filter\ExcelTimeToDateTime',
            'ToQName' => 'Kotka\Filter\ToQName',
            'ToURI' => 'Kotka\Filter\ToURI',
            'RomanDateToDate' => 'Kotka\Filter\RomanDateToDate'
        )
    ),
    'translator' => array(
        'locale' => 'en_US'
    ),
    'controllers' => array(
        'invokables' => array(
            'User\Controller\User' => 'User\Controller\UserController',
            'Kotka\Controller\Collections' => 'Kotka\Controller\CollectionsController',
            'Kotka\Controller\Datasets' => 'Kotka\Controller\DatasetsController',
            'Kotka\Controller\Organizations' => 'Kotka\Controller\OrganizationsController',
            'Kotka\Controller\Specimens' => 'Kotka\Controller\SpecimensController',
            'Kotka\Controller\Samples' => 'Kotka\Controller\SamplesController',
            'Kotka\Controller\Culture' => 'Kotka\Controller\CultureController',
            'Kotka\Controller\Image' => 'Kotka\Controller\ImageController',
            'Kotka\Controller\Index' => 'Kotka\Controller\IndexController',
            'Kotka\Controller\Picker' => 'Kotka\Controller\PickerController',
            'Kotka\Controller\Tools' => 'Kotka\Controller\ToolsController',
            'Kotka\Controller\Search' => 'Kotka\Controller\SearchController',
            'Kotka\Controller\Pdf' => 'Kotka\Controller\PdfController',
            'Kotka\Controller\File' => 'Kotka\Controller\FileController',
            'Kotka\Controller\View' => 'Kotka\Controller\ViewController',
            'Kotka\Controller\FormPartial' => 'Kotka\Controller\FormPartialController',
            'Kotka\Controller\Document' => 'Kotka\Controller\DocumentController',
            'Kotka\Controller\Js' => 'Kotka\Controller\JsController',
            'Kotka\Controller\Excel' => 'Kotka\Controller\ExcelController',
            'Kotka\Controller\Types' => 'Kotka\Controller\TypesController',
            'Kotka\Controller\Seed' => 'Kotka\Controller\SeedController',
            'Kotka\Controller\Maps' => 'Kotka\Controller\MapsController',
            'Kotka\Controller\Report' => 'Kotka\Controller\ReportController',
            'Kotka\Controller\Accessions' => 'Kotka\Controller\AccessionsController',
        ),
    ),
    'controller_plugins' => array(
        'invokables' => array(
            'StoreOverRequest' => 'Kotka\Controller\Plugin\StoreOverRequest',
            'Store' => 'Kotka\Controller\Plugin\Store',
            'getForm' => 'Kotka\Controller\Plugin\getForm'
        )
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions' => ($env === 'dev'),
        'doctype' => 'HTML5',
        'not_found_template' => 'error/404',
        'exception_template' => 'error/index',
        'template_map' => include __DIR__ . '/../template_map.php',
    ),
    'view_helpers' => array(
        'invokables' => array(
            'OrganizationName' => 'Kotka\View\Helper\OrganizationName',
            'KotkaFormCollection' => 'Kotka\View\Helper\KotkaFormCollection',
            'Environment' => 'Kotka\View\Helper\Environment',
            'CollectionName' => 'Kotka\View\Helper\CollectionName',
            'DatasetName' => 'Kotka\View\Helper\DatasetName',
            'Time' => 'Kotka\View\Helper\Time',
            'Count' => 'Kotka\View\Helper\Count',
            'ParentCollection' => 'Kotka\View\Helper\ParentCollection',
            'Alert' => 'Kotka\View\Helper\Alert',
            'Taxon' => 'Kotka\View\Helper\Taxon',
            'FullTaxon' => 'Kotka\View\Helper\FullTaxon',
            'CurrentTaxon' => 'Kotka\View\Helper\CurrentTaxon',
            'Coordinate' => 'Kotka\View\Helper\Label\Coordinate',
            'SearchQuery' => 'Kotka\View\Helper\SearchQuery',
            'ElementGroup' => 'Kotka\View\Helper\ElementGroup',
            'formRow' => 'Kotka\Form\View\Helper\FormRow',
            'formElement' => 'Kotka\Form\View\Helper\FormElement',
            'formLabel' => 'Kotka\Form\View\Helper\FormLabel',
            'formElementErrors' => 'Kotka\Form\View\Helper\FormElementErrors',
            'formArrayCollection' => 'Kotka\Form\View\Helper\FormArrayCollection',
            'Filter' => 'Kotka\View\Helper\Filter',
            'SelectOptions' => 'Kotka\View\Helper\SelectOptions',
            'Id' => 'Kotka\View\Helper\Id',
            'MarkUnreliableFields' => 'Kotka\View\Helper\MarkUnreliableFields',
            'QrCode' => 'Kotka\View\Helper\QrCode',
            'Barcode' => 'Kotka\View\Helper\Barcode',
            'RowTracker' => 'Kotka\View\Helper\RowTracker',
            'ResolveHostName' => 'Kotka\View\Helper\ResolveHostName',
            'NullLabel' => 'Kotka\View\Helper\NullLabel',
            'MdToHtml' => 'Kotka\View\Helper\MdToHtml',
            'FormHelp' => 'Kotka\View\Helper\FormHelp',
        )
    ),
    'doctrine' => array(
        'driver' => array(
            'mustikka_entities' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/Kotka/Mustikka')
            ),
            'orm_mustikka' => array(
                'drivers' => array(
                    'Kotka\Mustikka' => 'mustikka_entities'
                )
            )
        )
    ),
);
