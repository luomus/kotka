<?php

namespace Kotka;

use Common\Service\IdService;
use Triplestore\Classes\MAPersonInterface;
use Zend\Cache\Exception\InvalidArgumentException;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\DependencyIndicatorInterface;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;

/**
 * This is the Kotka Module initializer.
 *
 * @package Kotka
 */
class Module implements ConfigProviderInterface, DependencyIndicatorInterface
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager = $e->getApplication()->getEventManager();
        $sharedManager = $eventManager->getSharedManager();
        $sharedManager->attach('Zend\Mvc\Application', array(MvcEvent::EVENT_DISPATCH_ERROR, MvcEvent::EVENT_RENDER_ERROR), array($this, 'logExceptions'));

        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
        $application = $e->getParam('application');
        $services = $application->getServiceManager();
        $this->initSession($services);
        $userActionListener = $services->get('Kotka\Listener\UserActionListener');
        $eventManager->attach($userActionListener);
        $eventManager->attach(MvcEvent::EVENT_ROUTE, array($this, 'getPageCache'), -1000);
        $eventManager->attach(MvcEvent::EVENT_RENDER, array($this, 'savePageCache'), -10000);

        $config = $services->get('config');
        $cache = $services->get('cache');
        // Enables cache for classes
        if (isset($config['cache-enabled-classes'])) {
            foreach ($config['cache-enabled-classes'] as $className) {
                call_user_func($className . '::setCache', $cache);
            }
        }
        IdService::setDefaultQNamePrefix(rtrim(IdService::DEFAULT_DB_QNAME_PREFIX, ':'));
    }

    private function initSession($services)
    {
        /** @var \Zend\Session\SessionManager $sessionManager */
        $sessionManager = $services->get('Zend\Session\SessionManager');
        try {
            $sessionManager->start();
        } catch (InvalidArgumentException $e) {
            try {
                $sessionManager->regenerateId(true);
            } catch (InvalidArgumentException $e) {

            }
        }
    }

    public function logExceptions(MvcEvent $event)
    {
        $sm = $event->getApplication()->getServiceManager();
        $ex = $event->getParam('exception');
        if (!$ex) {
            return;
        }
        /** @var \Kotka\Service\ErrorHandlingService $errorHandler */
        $errorHandler = $sm->get('Kotka\Service\ErrorHandlingService');
        $errorHandler->logException($ex);
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getPageCache(MvcEvent $event)
    {
        $match = $event->getRouteMatch();
        if (!$match) {
            return null;
        }

        if ($match->getParam('pagecache')) {
            // the page can be cached so lets check if we have a cache copy of it
            $cache = $event->getApplication()->getServiceManager()->get('cache');
            $cacheKey = $this->pageCacheKey($match);
            $data = $cache->getItem($cacheKey);
            if (null !== $data) {
                $response = $event->getResponse();
                $response->setContent($data);

                return $response;
            }
        }
    }

    public function savePageCache(MvcEvent $event)
    {
        $match = $event->getRouteMatch();
        if ($match && $match->getParam('pagecache')) {
            $response = $event->getResponse();
            $data = $response->getContent();
            $cache = $event->getApplication()->getServiceManager()->get('cache');
            $cacheKey = $this->pageCacheKey($match);
            $cache->setItem($cacheKey, $data);
            $tags = $match->getParam('tags');
            if (is_array($tags)) {
                $cache->setTags($cacheKey, $tags);
            }
        }
    }

    /**
     * Generates valid page cache key
     *
     * @param RouteMatch $match
     * @param string $prefix
     * @return string
     */
    protected function pageCacheKey(RouteMatch $match, $prefix = 'pagecache_')
    {
        $params = isset($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] : '';
        return $prefix . str_replace('/', '-', $match->getMatchedRouteName()) . '_' . md5($params);
    }

    /**
     * Expected to return an array of modules on which the current one depends on
     *
     * @return array
     */
    public function getModuleDependencies()
    {
        return ['User'];
    }
}
