<?php

namespace Kotka\Repository;

use Kotka\Mapper\MYCollectionMapperInterface;
use Common\Service\IdService;
use Kotka\Triple\MYCollection;
use Triplestore\Stdlib\EntityRepository;

/**
 * Class MYCollectionRepository handles with communication to DoctrineMongoODM
 *
 * @package Kotka\Repository
 */
class MYCollectionRepository extends EntityRepository implements MYCollectionMapperInterface
{

    protected $deep = false;

    /**
     * Create Collection
     *
     * @param  MYCollection $collection
     * @return boolean
     */
    public function create(MYCollection $collection)
    {
        return $this->om->save($collection);
    }

    /**
     *  Update Collection
     *
     * @param  MYCollection $collection
     * @return boolean
     */
    public function update(MYCollection $collection)
    {
        return $this->om->save($collection);
    }

    /**
     * @inheritdoc
     */
    public function findBy(array $criteria, array $sort = null, $limit = null, $skip = null, array $fields = null)
    {
        $criteria['rdf:type'] = 'MY.collection';

        return parent::findBy($criteria, $sort, $limit, $skip, $fields);
    }

    /**
     * Finds one with the id
     * @param  mixed|object|string $id
     * @return object
     */
    public function find($id)
    {
        return parent::find(IdService::getQName($id, true));
    }

    /**
     * Finds one with the given criteria
     *
     * @param  array $criteria
     * @return object
     */
    public function findOneBy(array $criteria)
    {
        $criteria['rdf:type'] = 'MY.collection';

        return parent::findOneBy($criteria);
    }
}
