<?php

namespace Kotka\Repository;

use Kotka\Mapper\MFSampleMapperInterface;
use Common\Service\IdService;
use Kotka\Triple\MFSample;
use Triplestore\Stdlib\EntityRepository;

/**
 * Class MFSampleRepository  handles with communication to Triplestore manager
 *
 * @package Kotka\Repository
 */
class MFSampleRepository extends EntityRepository implements MFSampleMapperInterface
{

    protected $deep = false;

    /**
     * Create Dataset
     *
     * @param  MFSample $sample
     * @return boolean
     */
    public function create(MFSample $sample)
    {
        return $this->om->save($sample);
    }

    /**
     *  Update Dataset
     *
     * @param  MFSample $dataset
     * @return boolean
     */
    public function update(MFSample $dataset)
    {
        return $this->om->save($dataset);
    }

    /**
     * @inheritdoc
     */
    public function findBy(array $criteria, array $sort = null, $limit = null, $skip = null, array $fields = null)
    {
        $criteria['rdf:type'] = 'MF.sample';

        return parent::findBy($criteria, $sort, $limit, $skip, $fields);
    }

    /**
     * @inheritdoc
     */
    public function find($id)
    {
        return parent::find(IdService::getQName($id, true));
    }

    /**
     * @inheritdoc
     */
    public function findOneBy(array $criteria)
    {
        $criteria['rdf:type'] = 'MF.sample';

        return parent::findOneBy($criteria);
    }
}
