<?php

namespace Kotka\Repository;

use Kotka\Mapper\GXDatasetMapperInterface;
use Common\Service\IdService;
use Kotka\Triple\GXDataset;
use Triplestore\Stdlib\EntityRepository;

/**
 * Class GXDatasetRepository  handles with communication to DoctrineMongoODM
 *
 * @package Kotka\Repository
 */
class GXDatasetRepository extends EntityRepository implements GXDatasetMapperInterface
{

    protected $deep = false;

    /**
     * Create Dataset
     *
     * @param  GXDataset $dataset
     * @return boolean
     */
    public function create(GXDataset $dataset)
    {
        return $this->om->save($dataset);
    }

    /**
     *  Update Dataset
     *
     * @param  GXDataset $dataset
     * @return boolean
     */
    public function update(GXDataset $dataset)
    {
        return $this->om->save($dataset);
    }

    /**
     * @inheritdoc
     */
    public function findBy(array $criteria, array $sort = null, $limit = null, $skip = null, array $fields = null)
    {
        $criteria['rdf:type'] = 'GX.dataset';

        return parent::findBy($criteria, $sort, $limit, $skip, $fields);
    }

    /**
     * @inheritdoc
     */
    public function find($id)
    {
        return parent::find(IdService::getQName($id, true));
    }

    /**
     * @inheritdoc
     */
    public function findOneBy(array $criteria)
    {
        $criteria['rdf:type'] = 'GX.dataset';

        return parent::findOneBy($criteria);
    }
}
