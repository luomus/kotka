<?php

namespace Kotka\Repository;


use Exception;
use Kotka\Mapper\MYDocumentMapperInterface;
use Common\Service\IdService;
use Kotka\Triple\MYDocument;
use Triplestore\Stdlib\EntityRepository;

/**
 * Class SpecimenRepository handles with communication to DoctrineMongoODM
 * This class is used for all the specimen types
 * For example zoo- and botany specimen.
 *
 * @package Kotka\Repository
 */
class MYDocumentRepository extends EntityRepository implements MYDocumentMapperInterface
{

    /**
     * Creates new specimen
     *
     * @param  MYDocument $specimen
     * @return boolean
     * @throws \Exception
     */
    public function create(MYDocument $specimen)
    {
        return $this->om->save($specimen);
    }

    /**
     * Updates existing specimen
     *
     * @param  MYDocument $specimen
     * @return boolean
     * @throws \Exception
     */
    public function update(MYDocument $specimen)
    {
        return $this->om->save($specimen, true, true);
    }

    /**
     * Finds one with the id
     * @param  mixed|object|string $id
     * @return object
     */
    public function find($id)
    {
        return parent::find(IdService::getQName($id, true));
    }

    /**
     * @inheritdoc
     */
    public function findBy(array $criteria, array $sort = null, $limit = null, $skip = null, array $fields = null)
    {
        $criteria['rdf:type'] = 'MY.document';

        return parent::findBy($criteria, $sort, $limit, $skip, $fields);
    }

}
