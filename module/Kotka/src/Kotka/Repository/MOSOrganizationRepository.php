<?php

namespace Kotka\Repository;

use Kotka\Mapper\MOSOrganizationMapperInterface;
use Common\Service\IdService;
use Kotka\Triple\MOSOrganization;
use Triplestore\Stdlib\EntityRepository;

/**
 * Class MOSMOSOrganizationRepository handles with communication to DoctrineMongoODM
 *
 * @package Kotka\Repository
 */
class MOSOrganizationRepository extends EntityRepository implements MOSOrganizationMapperInterface
{

    protected $deep = false;

    /**
     * Create MOSOrganization
     *
     * @param  MOSOrganization $organization
     * @return boolean
     */
    public function create(MOSOrganization $organization)
    {
        return $this->om->save($organization);
    }

    /**
     *  Update MOSOrganization
     *
     * @param  MOSOrganization $organization
     * @return boolean
     */
    public function update(MOSOrganization $organization)
    {
        return $this->om->save($organization);
    }

    /**
     * @inheritdoc
     */
    public function findBy(array $criteria, array $sort = null, $limit = null, $skip = null, array $fields = null)
    {
        $criteria['rdf:type'] = 'MOS.organization';
        return parent::findBy($criteria, $sort, $limit, $skip, $fields);
    }

    /**
     * @inheritdoc
     */
    public function find($id)
    {
        return parent::find(IdService::getQName($id, true));
    }

    /**
     * @inheritdoc
     */
    public function findOneBy(array $criteria)
    {
        $criteria['rdf:type'] = 'MOS.organization';

        return parent::findOneBy($criteria);
    }
}
