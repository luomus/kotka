<?php

namespace Kotka\Repository;

use Kotka\Mapper\HRATransactionMapperInterface;
use Common\Service\IdService;
use Kotka\Triple\HRATransaction;
use Triplestore\Stdlib\EntityRepository;

/**
 * Class HRATransactionRepository handles with communication to DoctrineMongoODM
 *
 * @package Kotka\Repository
 */
class HRATransactionRepository extends EntityRepository implements HRATransactionMapperInterface
{
    protected $deep = true;

    /**
     * Create transaction
     *
     * @param  HRATransaction $transaction
     * @return boolean
     */
    public function create(HRATransaction $transaction)
    {
        return $this->om->save($transaction);
    }

    /**
     * update transaction
     *
     * @param  HRATransaction $transaction
     * @return boolean
     */
    public function update(HRATransaction $transaction)
    {
        return $this->om->save($transaction, true, true);
    }

    /**
     * @inheritdoc
     */
    public function findBy(array $criteria, array $sort = null, $limit = null, $skip = null, array $fields = null)
    {
        $criteria['rdf:type'] = 'HRA.transaction';

        return parent::findBy($criteria, $sort, $limit, $skip, $fields);
    }

    /**
     * @inheritdoc
     */
    public function find($id)
    {
        return parent::find(IdService::getQName($id, true));
    }

    /**
     * @inheritdoc
     */
    public function findOneBy(array $criteria)
    {
        $criteria['rdf:type'] = 'HRA.transaction';

        return parent::findOneBy($criteria);
    }

}