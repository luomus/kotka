<?php
namespace Kotka\Factory;

use Zend\Http\Client;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class TipuApiClientFactory implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /** @var \Kotka\Options\ExternalsOptions $config */
        $config = $serviceLocator->get('Kotka\Options\ExternalsOptions');
        $tipuApi = $config->getTipuApi();
        $client = new Client(
            null,
            array(
                'adapter' => 'Zend\Http\Client\Adapter\Curl',
                'sslverifypeer' => false
            )
        );
        $user = $tipuApi->getUser();
        $client->getUri();
        if (!empty($user)) {
            $client->setAuth($user, $tipuApi->getPass());
        }
        return $client;
    }
}