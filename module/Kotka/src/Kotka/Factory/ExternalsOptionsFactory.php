<?php

namespace Kotka\Factory;


use Kotka\Options\ExternalsOptions;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ExternalsOptionsFactory implements FactoryInterface
{
    /**
     * {@inheritDoc}
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $config = $serviceLocator->get('Config');
        $config = isset($config['externals']) ? $config['externals'] : array();

        return new ExternalsOptions($config);
    }
}