<?php
namespace Kotka\Filter;

use Zend\Filter\AbstractFilter;

/**
 * Class EmptyArray removes empty strings from the array
 *
 * @package Kotka\Filter
 */
class EmptyArray extends AbstractFilter
{

    /**
     * Filters array so that the empty values are removed from the array
     *
     * @param  mixed $value
     * @return mixed
     */
    public function filter($value)
    {
        if (is_array($value)) {
            return array_filter($value);
        }
        return $value;
    }

}
