<?php
namespace Kotka\Filter;

use Common\Service\IdService;
use Triplestore\Model\Predicate;
use Triplestore\Service\ObjectManager;
use Zend\Filter\AbstractFilter;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

/**
 * Class ToQName
 *
 * @package Kotka\Filter
 */
class ToQName extends AbstractFilter implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    const MAX_CACHE_SIZE = 1000;

    protected static $cache = [];

    protected $findCorrectDomain = true;

    protected static $objectManager;

    /**
     * @return boolean
     */
    public function isFindCorrectDomain()
    {
        return $this->findCorrectDomain;
    }

    /**
     * @param boolean $findCorrectDomain
     */
    public function setFindCorrectDomain($findCorrectDomain)
    {
        $this->findCorrectDomain = $findCorrectDomain;
    }

    /**
     * Adds qname prefix to value if it's missing
     *
     * @param  mixed $value
     * @return mixed
     */
    public function filter($value)
    {
        if (empty($value)) {
            return $value;
        }
        if (is_array($value)) {
            array_walk($value, array($this, 'toQName'));
        } else if (is_string($value)) {
            $this->toQName($value);
        }
        return $value;
    }

    private function toQName(&$value)
    {
        if (isset(self::$cache[$value])) {
            $value = self::$cache[$value];
            return;
        }
        $qname = IdService::getQName($value, true);
        if ($this->findCorrectDomain) {
            $qname = $this->findCorrectDomain($qname);
        }
        if (count(self::$cache) > self::MAX_CACHE_SIZE) {
            self::$cache = [];
        }
        self::$cache[$value] = $qname;
        $value = $qname;
    }

    private function findCorrectDomain($qname)
    {
        if (strpos($qname, ':') !== false) {
            return $qname;
        }
        $om = $this->getObjectManager();
        $result = $om->getPredicate($qname, ObjectManager::PREDICATE_TYPE);
        if ($result === null || $result->count() == 0) {
            $candidate = IdService::QNAME_PREFIX_TUN . $qname;
            $result = $om->getPredicate($candidate, ObjectManager::PREDICATE_TYPE);
            if ($result instanceof Predicate && $result->count() === 1) {
                return $candidate;
            }
        }
        return $qname;
    }

    /**
     * @return ObjectManager
     */
    private function getObjectManager()
    {
        if (self::$objectManager === null) {
            $parentServiceLocator = $this->getServiceLocator()->getServiceLocator();
            self::$objectManager = $parentServiceLocator->get('Triplestore\ObjectManager');
        }
        return self::$objectManager;
    }
}
