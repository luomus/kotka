<?php
namespace Kotka\Filter;

use Zend\Filter\Exception;
use Zend\Stdlib\ArrayUtils;

class AssociatedTaxaToUnit extends StringToArray
{

    protected $options = array(
        'delimiter' => ',',
        'recordBasis' => 'MY.recordBasisHumanObservation',
        'source' => 'MYAssociatedObservationTaxa'
    );

    /**
     * @return string
     */
    public function getSource()
    {
        return $this->options['source'];
    }

    /**
     * @param string $source
     */
    public function setSource($source)
    {
        $this->options['source'] = $source;
    }

    /**
     * @return string
     */
    public function getRecordBasis()
    {
        return $this->options['recordBasis'];
    }

    /**
     * @param string $recordBasis
     */
    public function setRecordBasis($recordBasis)
    {
        $this->options['recordBasis'] = $recordBasis;
    }

    /**
     * Returns the result of filtering $value
     *
     * Value is given as a string and it's split into array using the defined delimeter
     *
     * @param  mixed $value
     * @throws Exception\RuntimeException If filtering $value is impossible
     * @return array
     */
    public function filter($value)
    {
        if (!is_array($value)) {
            return $value;
        }

        if (isset($value['MYGathering'])) {
            foreach ($value['MYGathering'] as $key => $gathering) {
                $value['MYGathering'][$key] = $this->filter($gathering);
            }
            return $value;
        }

        $newUnits = [];
        $source = $this->getSource();
        $this->newUnits($value, $newUnits, $source); // This will parse the gathering level
        if (isset($value['MYUnit'])) {
            foreach ($value['MYUnit'] as $key => &$unit) {
                if (!isset($unit['MYIdentification'])) {
                    continue;
                }
                foreach ($unit['MYIdentification'] as $key2 => &$identification) {
                    $this->newUnits($identification, $newUnits, $source); // This will parse the identification level
                }
            }
        }
        if (count($newUnits)) {
            if (!isset($value['MYUnit'])) {
                $value['MYUnit'] = [];
            }
            $value['MYUnit'] = array_merge($value['MYUnit'], $newUnits);
        }
        return $value;
    }

    private function newUnits(&$data, &$newUnits, $source)
    {
        if (isset($data[$source]) && !empty($data[$source])) {
            $taxa = parent::filter($data[$source]);
            foreach ($taxa as $taxon) {
                $newUnits[] = $this->getNewUnit($taxon);
            }
            $data[$source] = '';
        }
    }

    private function getNewUnit($taxon)
    {
        return [
            'MYRecordBasis' => $this->getRecordBasis(),
            'MYIdentification' => [
                [
                    'MYTaxon' => $taxon
                ]
            ]
        ];
    }
}