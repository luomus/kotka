<?php
namespace Kotka\Filter;

use Kotka\Service\FormElementService;
use Triplestore\Model\Properties;
use Zend\Filter\AbstractFilter;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Zend\Stdlib\ArrayUtils;

/**
 * Class NormalizeDatabaseSelect normalized database select value
 *
 * @package Kotka\Filter
 */
class NormalizeDatabaseSelect extends AbstractFilter implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    protected $options = [
        'field' => '',
    ];
    /** @var  Properties */
    private $elementService;
    protected $field;
    protected $alias;

    /**
     * Class constructor
     *
     * @param string|array|\Traversable $options (Optional) Options to set, if null mcrypt is used
     */
    public function __construct($options = null)
    {
        if ($options instanceof \Traversable) {
            $options = ArrayUtils::iteratorToArray($options);
        }
        if (isset($options['field'])) {
            $this->setField($options['field']);
        }
    }

    public function filter($value)
    {
        if ($value === null || $value === '') {
            return $value;
        }
        if (is_array($value)) {
            array_walk($value, array($this, 'normalizeValue'));
        } else {
            $this->normalizeValue($value);
        }

        return $value;
    }

    protected function normalizeValue(&$value)
    {
        $lowerValue = mb_convert_case($value, MB_CASE_LOWER, 'utf-8');
        $alias = $this->getAlias();
        if (isset($alias[$lowerValue])) {
            $value = $this->alias[$lowerValue];
        }
    }

    public function getField()
    {
        return $this->field;
    }

    public function setField($field)
    {
        $this->field = $field;
    }

    private function getAlias()
    {
        if ($this->alias === null) {
            $valueOptions = $this->getElementService()->getSelect($this->field);
            if (isset($valueOptions[FormElementService::ALIAS_KEY])) {
                $this->alias = $valueOptions[FormElementService::ALIAS_KEY];
            } else {
                $this->alias = [];
            }
        }
        return $this->alias;
    }

    /**
     * @return \Kotka\Service\FormElementService
     */
    private function getElementService()
    {
        if ($this->elementService === null) {
            $this->elementService = $this->serviceLocator->getServiceLocator()->get('Kotka\Service\FormElementService');
        }
        return $this->elementService;
    }
}
