<?php
namespace Kotka\Filter;

use Zend\Filter\AbstractFilter;

/**
 * Class ForceArray makes sure that the value is an array and if not it will make it.
 * This is used to make sure that the fields that have more than one occurrence are in correct format
 *
 * @package Kotka\Filter
 */
class ForceArray extends AbstractFilter
{

    /**
     * Filters the value to array
     *
     * @param  mixed $value
     * @return array
     */
    public function filter($value)
    {
        if ($value === null || $value === '') {
            return array();
        }
        if (is_array($value)) {
            if (count($value) === 1 && $value[0] === '') {
                return [];
            }
            return $value;
        }

        return array($value);
    }

}
