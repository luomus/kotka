<?php

namespace Kotka\Filter;

use Zend\Filter\AbstractFilter;

class DateBegin extends AbstractFilter
{

    const DATE_BEGIN_KEY = 'MYDateBegin';
    const DATE_END_KEY = 'MYDateEnd';

    public function filter($value)
    {
        if (!is_array($value) || !isset($value[self::DATE_BEGIN_KEY]) ||
            (isset($value[self::DATE_END_KEY]) && !empty($value[self::DATE_END_KEY]))
        ) {
            return $value;
        }
        $begin = trim($value[self::DATE_BEGIN_KEY], '.');
        $exploded = preg_split('/\./', $begin, NULL, PREG_SPLIT_NO_EMPTY);
        $partCount = count($exploded);
        if ($partCount === 3) {
            return $value;
        } else if ($partCount === 2) {
            try {
                $datetime = \DateTime::createFromFormat('m.Y', $begin);
            } catch (\Exception $e) {
                return $value;
            }
            if (!$datetime instanceof \DateTime) {
                return $value;
            }
            $value[self::DATE_BEGIN_KEY] = '01' . $datetime->format('.m.Y');
            $value[self::DATE_END_KEY] = $datetime->format('t.m.Y');
        } else if ($partCount === 1) {
            $value[self::DATE_BEGIN_KEY] = '01.01.' . $begin;
            $value[self::DATE_END_KEY] = '31.12.' . $begin;
        }
        return $value;
    }

} 