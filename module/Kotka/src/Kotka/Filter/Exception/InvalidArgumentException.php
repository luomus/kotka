<?php

namespace Kotka\Filter\Exception;

class InvalidArgumentException extends \InvalidArgumentException
{
}