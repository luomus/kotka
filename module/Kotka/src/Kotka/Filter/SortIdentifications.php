<?php

namespace Kotka\Filter;

use Triplestore\Classes\MYDocumentInterface;
use Triplestore\Classes\MYGatheringInterface;
use Triplestore\Classes\MYIdentificationInterface;
use Triplestore\Classes\MYUnitInterface;
use Zend\Filter\AbstractFilter;

class SortIdentifications extends AbstractFilter
{
    public function filter($value)
    {
        if ($value instanceof MYDocumentInterface) {
            self::sortDocumentObject($value);
            return $value;
        }
        if (!is_array($value)) {
            return $value;
        }
        if (isset($value['MYIdentification'])) {
            array_walk($value['MYIdentification'], array('self', 'decorate'));
            usort($value['MYIdentification'], array('\Kotka\Filter\SortIdentifications', 'cmpIdentification'));
            array_walk($value['MYIdentification'], array('self', 'unDecorate'));
        } else if (isset($value['MY.identification'])) {
            array_walk($value['MY.identification'], array('self', 'decorate'));
            usort($value['MY.identification'], array('\Kotka\Filter\SortIdentifications', 'cmpIdentification'));
            array_walk($value['MY.identification'], array('self', 'unDecorate'));
        } elseif (isset($value['MYGathering'])) {
            foreach ($value['MYGathering'] as $k1 => $v1) {
                if (isset($v1['MYUnit'])) {
                    foreach ($v1['MYUnit'] as $k2 => $v2) {
                        if (isset($v2['MYIdentification'])) {
                            array_walk($value['MYGathering'][$k1]['MYUnit'][$k2]['MYIdentification'], array('self', 'decorate'));
                            usort(
                                $value['MYGathering'][$k1]['MYUnit'][$k2]['MYIdentification'],
                                array(
                                    '\Kotka\Filter\SortIdentifications',
                                    'cmpIdentification'
                                )
                            );
                            array_walk($value['MYGathering'][$k1]['MYUnit'][$k2]['MYIdentification'], array('self', 'unDecorate'));
                        }
                    }
                }
            }
        } elseif (isset($value['MY.gathering'])) {
            foreach ($value['MY.gathering'] as $k1 => $v1) {
                if (isset($v1['MY.unit'])) {
                    foreach ($v1['MY.unit'] as $k2 => $v2) {
                        if (isset($v2['MY.identification'])) {
                            array_walk($value['MY.gathering'][$k1]['MY.unit'][$k2]['MY.identification'], array('self', 'decorate'));
                            usort(
                                $value['MY.gathering'][$k1]['MY.unit'][$k2]['MY.identification'],
                                array(
                                    '\Kotka\Filter\SortIdentifications',
                                    'cmpIdentification'
                                )
                            );
                            array_walk($value['MY.gathering'][$k1]['MY.unit'][$k2]['MY.identification'], array('self', 'unDecorate'));
                        }
                    }
                }
            }
        } else {
            array_walk($value, array('self', 'decorate'));
            usort($value, array('\Kotka\Filter\SortIdentifications', 'cmpIdentification'));
            array_walk($value, array('self', 'unDecorate'));
        }
        return $value;
    }

    public static function sortDocumentObject(MYDocumentInterface $specimen)
    {
        $gatherings = $specimen->getMYGathering();
        foreach ($gatherings as $gathering) {
            if (!$gathering instanceof MYGatheringInterface) {
                continue;
            }
            $units = $gathering->getMYUnit();
            foreach ($units as $unit) {
                if (!$unit instanceof MYUnitInterface) {
                    continue;
                }
                $identifications = $unit->getMYIdentification();
                array_walk($identifications, array('self', 'decorate'));
                usort($identifications, array('\Kotka\Filter\SortIdentifications', 'cpmIdentificationObject'));
                array_walk($identifications, array('self', 'unDecorate'));
                $unit->setMYIdentification($identifications);
            }
        }
    }
    public static function cpmIdentificationObject(MYIdentificationInterface $i1, MYIdentificationInterface $i2)
    {
        $a1 = [
            'MYPreferredIdentification' => $i1->getMYPreferredIdentification(),
            'MYDetDate' => $i1->getMYDetDate(),
            'weight' => $i1->getSortOrder()
        ];
        $a2 = [
            'MYPreferredIdentification' => $i2->getMYPreferredIdentification(),
            'MYDetDate' => $i2->getMYDetDate(),
            'weight' => $i1->getSortOrder()
        ];
        return self::cmpIdentification($a1, $a2);
    }

    public static function cmpIdentification($i1, $i2)
    {
        if (!is_array($i1) || !is_array($i2)) {
            return 1;
        }
        $pre1 = isset($i1['MYPreferredIdentification']) ? $i1['MYPreferredIdentification'] :
            (isset($i1['MY.preferredIdentification']) ? $i1['MY.preferredIdentification'] : '');
        $pre2 = isset($i2['MYPreferredIdentification']) ? $i2['MYPreferredIdentification'] :
            (isset($i2['MY.preferredIdentification']) ? $i2['MY.preferredIdentification'] : '');
        if ($pre1 !== '' && $pre1 === $pre2) {
            return 0;
        }
        if ($pre1 === 'yes' || $pre2 === 'no') {
            return -1;
        }
        if ($pre2 === 'yes' || $pre1 === 'no') {
            return 1;
        }
        $det1 = isset($i1['MYDetDate']) ? $i1['MYDetDate'] :
            (isset($i1['MY.detDate']) ? $i1['MY.detDate'] : '0000');
        $det2 = isset($i2['MYDetDate']) ? $i2['MYDetDate'] :
            (isset($i2['MY.detDate']) ? $i2['MY.detDate'] : '0000');
        $len1 = strlen($det1);
        $len2 = strlen($det2);
        if ($len1 > 10) {
            return 1;
        }
        if ($len2 > 10) {
            return -1;
        }
        if ($len1 > 4) {
            $parts = explode('.', $det1);
            $det1 = implode('', array_reverse($parts));
        }
        if ($len2 > 4) {
            $parts = explode('.', $det2);
            $det2 = implode('', array_reverse($parts));
        }
        $det1 = (int)str_pad($det1, 8, '0');
        $det2 = (int)str_pad($det2, 8, '0');
        if ($det1 == $det2) {
            $wei1 = isset($i1['weight']) ? $i1['weight'] : -1;
            $wei2 = isset($i2['weight']) ? $i2['weight'] : -1;
            if ($wei1 === $wei2) {
                return 0;
            }
            return $wei1 > $wei2;
        }
        return $det1 < $det2;
    }

    public static function decorate(&$v, $k)
    {
        if ($v instanceof MYIdentificationInterface) {
            if ($v->getSubject() === null || $v->getSubject() === '') {
                $v->setSortOrder($k);
            } else {
                $v->setSortOrder(-1 * ((int)filter_var($v->getSubject(), FILTER_SANITIZE_NUMBER_INT)));
            }
        } else if (is_array($v)) {
            $v['weight'] = $k;
        }
    }

    public static function unDecorate(&$v, $k)
    {
        if (is_array($v) && isset($v['weight'])) {
            unset($v['weight']);
        }
    }
} 