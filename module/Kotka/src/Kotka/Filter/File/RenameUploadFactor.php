<?php

namespace Kotka\Filter\File;

use Zend\Filter\File\RenameUpload;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class RenameUploadFactor implements FactoryInterface
{


    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $settings = array('target' => '/tmp/uploaded.csv', 'randomize' => true, 'use_upload_name' => true);

        return new RenameUpload($settings);
    }
}