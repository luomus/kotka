<?php
namespace Kotka\Filter\File;

use Zend\Filter;
use Zend\Filter\Exception;

/**
 * Decrypts a given file and stores the decrypted file content
 */
class ConvertToUtf8 implements Filter\FilterInterface
{
    /**
     * New filename to set
     *
     * @var string
     */
    protected $filename;

    /**
     * Returns the new filename where the content will be stored
     *
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * Sets the new filename where the content will be stored
     *
     * @param  string $filename (Optional) New filename to set
     * @return self
     */
    public function setFilename($filename = null)
    {
        $this->filename = $filename;
        return $this;
    }

    /**
     * Defined by Zend\Filter\FilterInterface
     *
     * Converts the content of the file to utf8
     *
     * @param  string|array $value Full path of file to change or $_FILES data array
     * @return string|array The filename which has been set
     * @throws Exception\InvalidArgumentException
     * @throws Exception\RuntimeException
     */
    public function filter($value)
    {
        if (!is_scalar($value) && !is_array($value)) {
            return $value;
        }

        // An uploaded file? Retrieve the 'tmp_name'
        $isFileUpload = false;
        if (is_array($value)) {
            if (!isset($value['tmp_name'])) {
                return $value;
            }

            $isFileUpload = true;
            $uploadData = $value;
            $value = $value['tmp_name'];
        }

        $extension = substr($value, strrpos($value, '.') + 1);
        if (!in_array($extension, array('csv', 'txt'))) {
            if ($isFileUpload) {
                /** @var array $uploadData */
                return $uploadData;
            }
            return $value;
        }

        if (!file_exists($value)) {
            throw new Exception\InvalidArgumentException("File '$value' not found");
        }

        if (!isset($this->filename)) {
            $this->filename = $value;
        }

        if (file_exists($this->filename) and !is_writable($this->filename)) {
            throw new Exception\RuntimeException("File '{$this->filename}' is not writable");
        }

        $content = file_get_contents($value);
        if (!$content) {
            throw new Exception\RuntimeException("Problem while reading file '$value'");
        }

        $decoded = $this->convertEncoding($content);
        $result = file_put_contents($this->filename, $decoded);

        if (!$result) {
            throw new Exception\RuntimeException("Problem while writing file '{$this->filename}'");
        }

        if ($isFileUpload) {
            $uploadData['tmp_name'] = $this->filename;
            return $uploadData;
        }
        return $this->filename;
    }

    private function convertEncoding($content)
    {
        $bom = substr($content, 0, 2);
        if ($bom === chr(0xff) . chr(0xfe) || $bom === chr(0xfe) . chr(0xff)) {
            $encoding = 'UTF-16';
        } else {
            $encoding = mb_detect_encoding($content, 'UTF-8, UTF-7, ISO-8859-1, ASCII', true);
        }
        return iconv($encoding, 'UTF-8', $content);
    }

}
