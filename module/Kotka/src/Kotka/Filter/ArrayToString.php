<?php
namespace Kotka\Filter;

use Zend\Filter\AbstractFilter;
use Zend\Filter\Exception;
use Zend\Stdlib\ArrayUtils;

class ArrayToString extends AbstractFilter
{

    protected $options = array(
        'delimiter' => ','
    );

    /**
     * Class constructor
     *
     * @param array|\Traversable $options (Optional) Options to set
     */
    public function __construct($options = null)
    {
        if ($options instanceof \Traversable) {
            $options = ArrayUtils::iteratorToArray($options);
        }
        if (isset($options['delimiter'])) {
            $this->setDelimiter($options['delimiter']);
        }
    }

    public function getDelimiter()
    {
        return $this->options['delimiter'];
    }

    public function setDelimiter($delimiter)
    {
        if (!is_string($delimiter)) {
            throw new \Exception("Delimiter should be a string");
        }
        $this->options['delimiter'] = $delimiter;
    }

    /**
     * Returns the result of filtering $value
     *
     * Value is given as a array and it's joined into a string using the defined delimeter
     * If value is not array the the value is returned as it is
     *
     * @param  mixed $value
     * @throws Exception\RuntimeException If filtering $value is impossible
     * @return array
     */
    public function filter($value)
    {
        $delimiter = $this->getDelimiter();
        if (is_array($value)) {
            $value = array_map('trim', $value);
            $value = implode($delimiter, $value);
        }
        return $value;
    }
}