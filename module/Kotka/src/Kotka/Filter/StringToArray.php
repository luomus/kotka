<?php
namespace Kotka\Filter;

use Zend\Filter\AbstractFilter;
use Zend\Filter\Exception;
use Zend\Stdlib\ArrayUtils;

class StringToArray extends AbstractFilter
{

    protected $options = array(
        'delimiter' => ','
    );

    /**
     * Class constructor
     *
     * @param array|\Traversable $options (Optional) Options to set
     */
    public function __construct($options = [])
    {
        $this->setOptions($options);
    }

    public function getDelimiter()
    {
        return $this->options['delimiter'];
    }

    public function setDelimiter($delimiter)
    {
        if (!is_string($delimiter)) {
            throw new \Exception("Delimiter should be a string");
        }
        $this->options['delimiter'] = $delimiter;
    }

    /**
     * Returns the result of filtering $value
     *
     * Value is given as a string and it's split into array using the defined delimeter
     *
     * @param  mixed $value
     * @throws Exception\RuntimeException If filtering $value is impossible
     * @return array
     */
    public function filter($value)
    {
        $delimiter = $this->getDelimiter();
        if (is_string($value)) {
            $data = preg_split('/' . $delimiter . '/', $value, -1, PREG_SPLIT_NO_EMPTY);
            return array_map('trim', $data);
        } elseif (!is_array($value)) {
            throw new Exception\RuntimeException('Value should be a string or an array!');
        }
        $return = [];
        foreach ($value as $val) {
            $array = preg_split('/' . $delimiter . '/', $val, -1, PREG_SPLIT_NO_EMPTY);
            $return = array_merge($return, array_map('trim', $array));
        }
        return $return;
    }
}