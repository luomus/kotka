<?php
namespace Kotka\Filter;

use Zend\Filter\AbstractFilter;
use Zend\Stdlib\ArrayUtils;

/**
 * Class MoveTo filter moves the value from source field to target field
 *
 * @package Kotka\Filter
 */
class MoveTo extends AbstractFilter
{

    private $source;
    private $target;
    private $override = false;
    private $clearSource = true;
    private $condition = [];

    /**
     * Class constructor
     *
     * @param string|array|\Traversable $options (Optional) Options to set, if null mcrypt is used
     */
    public function __construct($options = null)
    {
        if ($options instanceof \Traversable) {
            $options = ArrayUtils::iteratorToArray($options);
        }
        if (isset($options['source'])) {
            $this->setSource($options['source']);
        }
        if (isset($options['target'])) {
            $this->setTarget($options['target']);
        }
        if (isset($options['override'])) {
            $this->setOverride($options['override']);
        }
        if (isset($options['clear-source'])) {
            $this->setClearSource($options['clear-source']);
        }
        if (isset($options['condition'])) {
            $this->setCondition($options['condition']);
        }
    }

    /**
     * @return mixed
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @param mixed $source
     */
    public function setSource($source)
    {
        $this->source = $source;
    }

    /**
     * @return mixed
     */
    public function getTarget()
    {
        return $this->target;
    }

    /**
     * @param mixed $target
     */
    public function setTarget($target)
    {
        $this->target = $target;
    }

    /**
     * @return boolean
     */
    public function isOverride()
    {
        return $this->override;
    }

    /**
     * @param boolean $override
     */
    public function setOverride($override)
    {
        $this->override = (bool)$override;
    }

    /**
     * @return boolean
     */
    public function isClearSource()
    {
        return $this->clearSource;
    }

    /**
     * @param boolean $clearSource
     */
    public function setClearSource($clearSource)
    {
        $this->clearSource = (bool)$clearSource;
    }

    /**
     * @return array
     */
    public function getCondition()
    {
        return $this->condition;
    }

    /**
     * @param array $condition
     */
    public function setCondition(array $condition)
    {
        $this->condition = $condition;
    }


    /**
     * @param mixed $value
     * @return mixed
     * @throws \BadMethodCallException
     */
    public function filter($value)
    {
        if (!is_array($value)) {
            return $value;
        }
        $source = $this->getSource();
        $target = $this->getTarget();
        if (empty($source) || empty($target)) {
            throw new \BadMethodCallException('You need to specify both source and target to MoveTo filter!');
        }

        if (isset($value[$source])) {
            if (!$this->isOverride() && isset($value[$target]) && !empty($value[$target])) {
                return $value;
            }
            if (!$this->matchCondition($this->getCondition(), $value)) {
                return $value;
            }
            $value[$target] = $value[$source];
            if ($this->isClearSource()) {
                unset($value[$source]);
            }
        }

        return $value;
    }

    protected function matchCondition($condition, $data)
    {
        if (empty($condition)) {
            return true;
        }
        if (!isset($condition['source']) || !isset($condition['operation']) || !isset($condition['value'])) {
            throw new \Exception('Missing required key from the condition to the the move to filter');
        }
        $sourceValue = isset($data[$condition['source']]) ? $data[$condition['source']] : '';
        switch ($condition['operation']) {
            case ('equal'):
                return $sourceValue === $condition['value'];
            default:
                throw new \Exception('Invalid operation given to move to ');
        }
    }
}
