<?php

namespace Kotka\Filter;

use Zend\Filter\AbstractFilter;

class RomanDateToDate extends AbstractFilter
{

    protected static $romans = [
        '.01.' => ".I.",
        '.02.' => ".II.",
        '.03.' => ".III.",
        '.04.' => ".IV.",
        '.05.' => ".V.",
        '.06.' => ".VI.",
        '.07.' => ".VII.",
        '.08.' => ".VIII.",
        '.09.' => ".IX.",
        '.10.' => ".X.",
        '.11.' => ".XI.",
        '.12.' => ".XII.",
    ];

    public function filter($value, $dateToRoman = false)
    {
        if ($dateToRoman) {
            return str_replace(array_keys(self::$romans), array_values(self::$romans), $value);
        } else {
            return str_replace(array_values(self::$romans), array_keys(self::$romans), $value);
        }

    }

} 