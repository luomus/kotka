<?php
namespace Kotka\Filter;

use Zend\Filter\AbstractFilter;
use Zend\Filter\Exception;
use Zend\Stdlib\ArrayUtils;

/**
 * Class ExcelTimeToDateTime converts Excel string time to datetime
 *
 * If format option is given datetime with format is returned instead
 *
 * @package Kotka\Filter
 */
class ExcelTimeToDateTime extends AbstractFilter
{

    const WINDOWS_CALENDAR = 'win';
    const MAC_CALENDAR = 'mac';

    protected $options = array(
        'format' => null
    );

    /**
     * Class constructor
     *
     * @param string|array|\Traversable $options (Optional) Options to set, if null defaults are used
     */
    public function __construct($options = null)
    {
        if ($options instanceof \Traversable) {
            $options = ArrayUtils::iteratorToArray($options);
        }
        if (isset($options['format'])) {
            $this->setFormat($options['format']);
        }
    }

    public function getFormat()
    {
        return $this->options['format'];
    }

    public function setFormat($format)
    {
        if (!is_string($format)) {
            throw new \Exception("DateTime format should be a string");
        }
        $this->options['format'] = $format;
    }

    /**
     * Returns the result of filtering $value
     *
     * @param  mixed $value
     * @param  string $base base calendar
     * @return array
     */
    public function filter($value, $base = self::WINDOWS_CALENDAR)
    {
        if (!is_string($value) || !ctype_digit($value) || strlen($value) != 5) {
            return $value;
        }
        $format = $this->getFormat();
        $return = $this->ExcelTimeToDateTime((int)$value, $base);
        if ($format !== null && $return instanceof \DateTime) {
            $return = $return->format($format);
        }
        return $return;
    }

    private function ExcelTimeToDateTime($dateValue = 0, $base)
    {
        $myExcelBaseDate = $base == self::WINDOWS_CALENDAR ? 25569 : 24107; // widows : unix base (1900 : 1904)
        $unixTime = ($dateValue - $myExcelBaseDate) * 86400;
        $dateTime = new \DateTime("@$unixTime");

        return $dateTime;
    }

}