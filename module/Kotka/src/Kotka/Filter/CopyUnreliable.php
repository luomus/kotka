<?php

namespace Kotka\Filter;

use Zend\Filter\AbstractFilter;
use Zend\Stdlib\ArrayUtils;

class CopyUnreliable extends AbstractFilter
{

    protected $options = array(
        'source' => '',
        'target' => '',
        'fewerThanNumber' => false
    );

    /**
     * Class constructor
     *
     * @param array|\Traversable $options (Optional) Options to set
     */
    public function __construct($options = null)
    {
        if ($options instanceof \Traversable) {
            $options = ArrayUtils::iteratorToArray($options);
        }
        if (isset($options['source'])) {
            $this->setSource($options['source']);
        }
        if (isset($options['target'])) {
            $this->setTarget($options['target']);
        }
        if (isset($options['fewerThanNumber'])) {
            $this->setFewerThanNumber($options['fewerThanNumber']);
        }

    }

    public function getSource()
    {
        return $this->options['source'];
    }

    /**
     * @param string $source
     */
    public function setSource($source)
    {
        $this->options['source'] = $source;
    }

    public function getTarget()
    {
        return $this->options['target'];
    }

    /**
     * @param string $target
     */
    public function setTarget($target)
    {
        $this->options['target'] = $target;
    }

    public function getFewerThanNumber()
    {
        return $this->options['fewerThanNumber'];
    }

    /**
     * @param number $cnt
     */
    public function setFewerThanNumber($cnt)
    {
        $this->options['fewerThanNumber'] = $cnt;
    }

    public function filter($value)
    {

        $source = $this->getSource();
        if (!isset($value[$source]) || !is_string($value[$source]) || strpos($value[$source], '?') !== 0) {
            return $value;
        }
        $fewerThan = $this->getFewerThanNumber();
        if ($fewerThan && preg_match_all( "/[0-9]/", $value[$source]) >= $fewerThan) {
            return $value;
        }

        $target = $this->getTarget();
        if (!isset($value[$target]) || (is_string($value[$target]) && empty($value[$target]))) {
            $value[$target] = '?';
        }
        return $value;
    }

} 