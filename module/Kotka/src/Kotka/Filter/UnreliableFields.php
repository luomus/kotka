<?php
namespace Kotka\Filter;

use Kotka\Enum\Document;
use Kotka\Triple\MYDocument;
use Triplestore\Model\Properties;
use Triplestore\Service\ObjectManager;
use Triplestore\Service\PhpVariableConverter;
use Zend\Filter\AbstractFilter;
use Zend\Filter\Exception\BadMethodCallException;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class UnreliableFields marks every un reliable field with the desired value or
 * extracts the fields marked as unreliable and adds it to unreliable field value
 *
 * @package Kotka\Filter
 */
class UnreliableFields extends AbstractFilter implements ServiceLocatorAwareInterface
{
    const POS_PREPEND = 0;
    const POS_APPEND = 1;

    const UNRELIABLE_FIELD_INDICATOR = '?';
    const UNRELIABLE_FIELD_SEPARATOR = "\n";

    protected $options = [
        'mark' => '',
        'position' => self::POS_PREPEND
    ];

    protected $constants = [
        self::POS_PREPEND => 'prepend',
        self::POS_APPEND => 'append',
    ];

    private $serviceLocator;
    /** @var  Properties */
    private $properties;

    public function setMark($mark)
    {
        if (!is_scalar($mark)) {
            throw new BadMethodCallException('Can only mark with scalar value');
        }
        $this->options['mark'] = (string)$mark;
    }

    public function getMark()
    {
        return $this->options['mark'];
    }

    public function setPosition($position)
    {
        if (is_string($position) && in_array($position, $this->constants)) {
            $position = array_search($position, $this->constants);
        }
        if (!isset($this->constants[$position])) {
            throw new BadMethodCallException('Cannot set the desired position');
        }
        $this->options['position'] = $position;
    }

    public function getPosition()
    {
        return $this->options['position'];
    }

    public function filter($value)
    {
        if ($value === null) {
            return $value;
        }

        $mark = $this->getMark();
        $position = $this->getPosition();

        if ($mark === '') {
            if (is_array($value)) {
                $unreliable = $this->getUnreliableFields($value);
                if (isset($value['MYUnreliableFields'])) {
                    $unreliable[] = $value['MYUnreliableFields'];
                }
                $value['MYUnreliableFields'] = implode(self::UNRELIABLE_FIELD_SEPARATOR, $unreliable);
            } else {
                throw new BadMethodCallException('Filtered value should be an array');
            }
        } else {
            if ($value instanceof MYDocument) {
                $unreliable = $value->getMYUnreliableFields();
                if (empty($unreliable)) {
                    return $value;
                }
                $unreliable = $this->extractUnreliable($value->getMYUnreliableFields());
                $this->markUnreliableInMYDocument($value, $unreliable, $mark, $position);
            } else if (is_array($value)) {
                $unreliable = null;
                if (isset($value[Document::PREDICATE_UNRELIABLE_FIELDS])) {
                    $unreliable = $this->extractUnreliable($value[Document::PREDICATE_UNRELIABLE_FIELDS]);
                }
                if ($unreliable === null) {
                    $field = PhpVariableConverter::toPhpMethod(Document::PREDICATE_UNRELIABLE_FIELDS);
                    if (isset($value[$field])) {
                        $unreliable = $this->extractUnreliable($value[$field]);
                    }
                }
                if ($unreliable === null) {
                    return $value;
                }

                $this->initAllPredicates();
                $this->markUnreliable($value, $unreliable, $mark, $position);
            } else {
                throw new BadMethodCallException('Filtered value should be an array or MYDocument');
            }
        }

        return $value;
    }

    /**
     * Returns the array of all the unreliable fields
     *
     * @param $data
     * @param string $path
     * @return array
     */
    public function getUnreliableFields(&$data, $path = '')
    {
        $unreliableFields = array();
        if (is_array($data) || $data instanceof \Traversable) {
            foreach ($data as $key => $value) {
                $newPath = $path == '' ? $key : $path . '[' . $key . ']';
                if (is_array($value) || $value instanceof \Traversable) {
                    $unreliableFields = array_merge($unreliableFields, $this->getUnreliableFields($data[$key], $newPath));
                    continue;
                }
                if ($this->isUnreliable($value)) {
                    $data[$key] = ltrim($value, self::UNRELIABLE_FIELD_INDICATOR . ' ');
                    $unreliableFields[] = $newPath;
                }
            }
        }
        return $unreliableFields;
    }

    /**
     * Checks weather or not the value is unreliable
     *
     * @param $value
     * @return bool
     */
    private function isUnreliable($value)
    {
        return (substr(trim($value), 0, 1) === self::UNRELIABLE_FIELD_INDICATOR);
    }

    /**
     * Adds unreliable mark to the value of data array
     *
     * @param $object
     * @param $fields
     * @param $mark
     * @param $position
     */
    private function markUnreliable(& $object, $fields, $mark, $position)
    {
        foreach ($fields as $key => $value) {
            if ($value == 1) {
                if (is_array($object) && isset($object[$key])) {
                    $object[$key] = $this->getMarkedValue($object[$key], $mark, $position);
                    continue;
                }
                $predicate = $this->properties->getOriginal($key);
                if (isset($object[$predicate])) {
                    $object[$predicate] = $this->getMarkedValue($object[$predicate], $mark, $position);
                    continue;
                }
            } else if (is_numeric($key)) {
                if (isset($object[$key])) {
                    $this->markUnreliable($object[$key], $value, $mark, $position);
                }
            } else {
                $predicate = $this->properties->getOriginal($key);
                if (isset($object[$predicate])) {
                    $this->markUnreliable($object[$predicate], $value, $mark, $position);
                } else if (isset($object[$key])) {
                    $this->markUnreliable($object[$key], $value, $mark, $position);
                } else if (isset($object['has'])) {
                    $types = [];
                    foreach ($object['has'] as $cKey => $child) {
                        $type = PhpVariableConverter::toPhpMethod($child[ObjectManager::PREDICATE_TYPE]);
                        if (!isseT($types[$type])) {
                            $types[$type] = [];
                        }
                        $types[$type][] = &$object['has'][$cKey];
                    }
                    if (isset($types[$key])) {
                        $this->markUnreliable($types[$key], $value, $mark, $position);
                    }
                }
            }
        }
    }

    /**
     * Adds unreliable markings to MYDocument object
     *
     * @param $object
     * @param $fields
     * @param $mark
     * @param $position
     */
    private function markUnreliableInMYDocument(& $object, $fields, $mark, $position)
    {
        foreach ($fields as $key => $value) {
            if ($value == 1) {
                if (is_array($object)) {
                    if (!isset($object[$key])) {
                        continue;
                    }
                    $object[$key] = $this->getMarkedValue($object[$key], $mark, $position);
                    continue;
                }
                $setMethod = 'set' . $key;
                $getMethod = 'get' . $key;
                if (method_exists($object, $setMethod)) {
                    $curvalue = $object->$getMethod();
                    $curvalue = $this->getMarkedValue($curvalue, $mark, $position);
                    $object->$setMethod($curvalue);
                    continue;
                }
            } else if (is_numeric($key)) {
                if (isset($object[$key])) {
                    $this->markUnreliableInMYDocument($object[$key], $value, $mark, $position);
                }
            } else {
                $getMethod = 'get' . $key;
                if (method_exists($object, $getMethod)) {
                    $child = $object->$getMethod();
                    $this->markUnreliableInMYDocument($child, $value, $mark, $position);
                }
            }
        }
    }


    /**
     * Converts the content of unreliableFields value to an multidimensional array
     * that has keys as fields and value 1 on the fields that should be marked as unreliable
     *
     * @param $unreliableFields
     * @return array
     * @throws \Zend\Filter\Exception\BadMethodCallException
     */
    private function extractUnreliable($unreliableFields)
    {
        if (!is_string($unreliableFields)) {
            throw new BadMethodCallException('Unreliable fields should be a string instead');
        }
        $unreliableFields = explode(self::UNRELIABLE_FIELD_SEPARATOR, $unreliableFields);
        $allFields = [];
        foreach ($unreliableFields as $unreliable) {
            $allFields[] = $unreliable . '=1';
        }
        $data = [];
        parse_str(implode('&', $allFields), $data);

        return $data;
    }


    /**
     * Returns the value with the marking added to right position
     *
     * @param $value
     * @param $mark
     * @param $position
     * @return string
     */
    private function getMarkedValue($value, $mark, $position)
    {
        if (is_array($value)) {
            foreach ($value as $k => $v) {
                $value[$k] = $position == self::POS_PREPEND ? $mark . $v : $v . $mark;
            }
            return $value;
        }
        return $position == self::POS_PREPEND ? $mark . $value : $value . $mark;
    }


    /**
     * Initializes the predicate object
     */
    private function initAllPredicates()
    {
        /** @var \Triplestore\Service\ObjectManager $om */
        $om = $this->serviceLocator->getServiceLocator()->get('Triplestore\ObjectManager');
        $om->getMetadataService();
        $this->properties = Properties::instance();
    }

    /**
     * Sets service locator
     * @param ServiceLocatorInterface $sl
     */
    public function setServiceLocator(ServiceLocatorInterface $sl)
    {
        $this->serviceLocator = $sl;
    }

    /**
     * Gets service locator
     * @return ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

}
