<?php

namespace Kotka\Filter;

use Zend\Filter\StringTrim;

class ArrayStringTrim extends StringTrim
{
    /**
     * Defined by Zend\Filter\FilterInterface
     *
     * Returns the string $value with characters stripped from the beginning and end
     *
     * @param  array|string $value
     * @return string
     */
    public function filter($value)
    {
        if (is_array($value)) {
            foreach ($value as $key => $val) {
                $value[$key] = parent::filter($val);
            }
        } else if (is_string($value)) {
            return parent::filter($value);
        }
        return $value;
    }

} 