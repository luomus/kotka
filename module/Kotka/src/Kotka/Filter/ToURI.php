<?php
namespace Kotka\Filter;

use Common\Service\IdService;
use Zend\Filter\AbstractFilter;

/**
 * Class ToURI
 *
 * @package Kotka\Filter
 */
class ToURI extends AbstractFilter
{

    /**
     * Adds qname prefix to value if it's missing
     *
     * @param  mixed $value
     * @return mixed
     */
    public function filter($value)
    {
        if (empty($value)) {
            return $value;
        }
        if (is_array($value)) {
            array_walk($value, array($this, 'toUri'));
        } else if (is_string($value)) {
            $this->toUri($value);
        }
        return $value;
    }

    private function toUri(&$value)
    {
        $value = IdService::getUri($value);
    }

}
