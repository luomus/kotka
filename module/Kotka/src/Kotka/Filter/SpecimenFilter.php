<?php
namespace Kotka\Filter;

use Zend\Filter\AbstractFilter;
use Zend\Filter\FilterChain;
use Zend\Filter\FilterInterface;

/**
 * Class SpecimenFilter filters the specimen data so that it can be validated
 *
 * @package Kotka\Filter
 */
class SpecimenFilter extends AbstractFilter
{
    const ROOT_LEVEL = '_root_';
    const ALL_FIELDS = '_all_';

    /**
     * Filters are store in this array
     *
     * @var array
     */
    private $items = [];

    private $travel = [
        self::ROOT_LEVEL => ['MYGathering'],
        'MYGathering' => ['MYUnit'],
        'MYUnit' => [
            'MYIdentification',
            'MYTypeSpecimen',
            'MFSample',
            'MYMeasurement'
        ],
        'MFSample' => [
            'MFPreparation',
            'MYMeasurement'
        ]
    ];

    public function attach(FilterInterface $filter, $level, $field = self::ALL_FIELDS, $priority = 0, $fieldPriority = FilterChain::DEFAULT_PRIORITY)
    {
        if (!isset($this->items[$priority])) {
            $this->items[$priority] = [];
        }
        if (!isset($this->items[$priority][$level])) {
            $this->items[$priority][$level] = [];
        }
        if (!isset($this->items[$priority][$level][$field])) {
            $this->items[$priority][$level][$field] = new FilterChain();
        }

        $this->items[$priority][$level][$field]->attach($filter, $fieldPriority);

        return $this;
    }

    /**
     * Filters the specimen array
     *
     * @param  mixed $value
     * @param  string $level indicates the current level
     * @return array
     */
    public function filter($value, $level = self::ROOT_LEVEL)
    {
        $filteredValue = $value;
        foreach ($this->items as $priority => $filters) {
            $filteredValue = $this->_filter($filteredValue, $level, $filters);
        }
        return $filteredValue;
    }

    private function _filter($value, $level = self::ROOT_LEVEL, $filters)
    {
        $filteredValue = $value;
        if (isset($filters[$level])) {
            foreach ($filters[$level] as $field => $filter) {
                /** @var FilterChain $filter */
                if ($field === self::ALL_FIELDS) {
                    $filteredValue = $filter->filter($filteredValue, $level);
                } elseif (isset($filteredValue[$field])) {
                    $filteredValue[$field] = $filter->filter($filteredValue[$field], $level);
                }
            }
        }
        if (isset($this->travel[$level])) {
            foreach ($this->travel[$level] as $nextLevel) {
                if (isset($filteredValue[$nextLevel])) {
                    if (is_array($filteredValue[$nextLevel])) {
                        foreach ($filteredValue[$nextLevel] as $index => $values) {
                            $filteredValue[$nextLevel][$index] = $this->_filter($values, $nextLevel, $filters);
                        }
                    } else {
                        $filteredValue[$nextLevel] = $this->_filter($filteredValue[$nextLevel], $nextLevel, $filters);
                    }
                }
            }
        }
        return $filteredValue;
    }

}
