<?php

namespace Kotka\Filter;


use Kotka\Filter\Exception;
use Kotka\Service\DocumentAccess;
use Zend\Filter\AbstractFilter;
use Zend\Stdlib\ArrayUtils;

class ListFilter extends AbstractFilter
{

    const MODE_ALL = 'all';

    protected $modes = [
        self::MODE_ALL,
        DocumentAccess::MODE_PRIVATE,
        DocumentAccess::MODE_PROTECTED,
        DocumentAccess::MODE_PUBLIC
    ];

    protected $whitelist = [];
    protected $blacklist = [];
    protected $mode = self::MODE_ALL;

    /**
     * Class constructor
     *
     * @param array|\Traversable $options (Optional) Options to set
     */
    public function __construct($options = null)
    {
        if ($options instanceof \Traversable) {
            $options = ArrayUtils::iteratorToArray($options);
        }
        if (isset($options['mode'])) {
            $this->setMode($options['mode']);
        }
        foreach (['whitelist', 'blacklist'] as $list) {
            if (isset($options[$list])) {
                foreach ($this->modes as $mode) {
                    if (isset($options[$list][$mode])) {
                        $method = 'set' . ucfirst($list);
                        $this->$method($options[$list][$mode], $mode);
                    }
                }
            }
        }
    }

    /**
     * @return string
     */
    public function getMode()
    {
        return $this->mode;
    }

    /**
     * @param string $mode
     */
    public function setMode($mode)
    {
        if (!in_array($mode, $this->modes)) {
            throw new Exception\InvalidArgumentException(
                'Invalid mode given. Should be one of ' .
                implode(', ', $this->modes) .
                ' but now "' . $mode . '" is given'
            );
        }
        $this->mode = $mode;
    }

    /**
     * @param string $mode
     * @return array
     */
    public function getWhitelist($mode = null)
    {
        if ($mode === null) {
            $mode = $this->getMode();
        }
        if (!isset($this->whitelist[$mode])) {
            return [];
        }
        return $this->whitelist[$mode];
    }

    /**
     * @param array $whitelist
     * @param string $mode
     */
    public function setWhitelist(array $whitelist, $mode = self::MODE_ALL)
    {
        if (!in_array($mode, $this->modes)) {
            throw new Exception\InvalidArgumentException(
                'Invalid mode given. Should be one of ' .
                implode(', ', $this->modes) .
                ' but now "' . $mode . '" is given'
            );
        }
        $this->whitelist[$mode] = $whitelist;
    }

    /**
     * @param string $mode
     * @return array
     */
    public function getBlacklist($mode = null)
    {
        if ($mode === null) {
            $mode = $this->getMode();
        }
        if (!isset($this->blacklist[$mode])) {
            return [];
        }
        return $this->blacklist[$mode];
    }

    /**
     * @param array $blacklist
     * @param string $mode
     */
    public function setBlacklist(array $blacklist, $mode = self::MODE_ALL)
    {
        if (!in_array($mode, $this->modes)) {
            throw new Exception\InvalidArgumentException(
                'Invalid mode given. Should be one of ' .
                implode(', ', $this->modes) .
                ' but now "' . $mode . '" is given'
            );
        }
        $this->blacklist[$mode] = $blacklist;
    }

    /**
     * Returns the result of filtering $value
     *
     * @param  mixed $value
     * @throws Exception\RuntimeException If filtering $value is impossible
     * @return mixed
     */
    public function filter($value)
    {
        if (!is_array($value)) {
            return $value;
        }
        if (isset($this->whitelist[self::MODE_ALL])) {
            $value = array_intersect_key($value, array_flip($this->whitelist[self::MODE_ALL]));
        }
        if (isset($this->blacklist[self::MODE_ALL])) {
            $value = array_diff_key($value, array_flip($this->blacklist[self::MODE_ALL]));
        }

        $mode = $this->getMode();
        if ($mode === self::MODE_ALL) {
            return $value;
        }

        if (isset($this->whitelist[$mode])) {
            $value = array_intersect_key($value, array_flip($this->whitelist[$mode]));
        }
        if (isset($this->blacklist[$mode])) {
            $value = array_diff_key($value, array_flip($this->blacklist[$mode]));
        }

        return $value;
    }
}