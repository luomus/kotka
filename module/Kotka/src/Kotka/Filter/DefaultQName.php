<?php
namespace Kotka\Filter;

use Common\Service\IdService;
use Zend\Filter\AbstractFilter;

/**
 * Class DefaultQName
 *
 * @package Kotka\Filter
 */
class DefaultQName extends AbstractFilter
{
    public function filter($value)
    {
        $value = trim($value);
        if(!empty($value) && strpos($value, ':') === false) {
            return IdService::getDefaultQNamePrefix() . ':' . $value;
        }

        return $value;
    }

}
