<?php

namespace Kotka\Filter\View;

use Kotka\Filter\ListFilter;
use Zend\Filter\Exception;

class MOSOrganization extends ListFilter
{

    protected $blacklist = [
        ListFilter::MODE_ALL => [
            'MOS.streetAddress',
            'MOS.postalOfficeBox',
            'MOS.postalCode',
            'MOS.locality',
            'MOS.region',
            'MOS.email',
            'MOS.phone',
            'MOS.fax',
            'MOS.ad',
        ]
    ];

}