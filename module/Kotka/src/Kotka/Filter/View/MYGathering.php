<?php

namespace Kotka\Filter\View;

use Kotka\Filter\ListFilter;
use Kotka\Service\DocumentAccess;
use Zend\Filter\Exception;
use Zend\Filter\StaticFilter;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

class MYGathering extends ListFilter implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    /**
     * Returns the result of filtering $value
     *
     * @param  mixed $value
     * @throws Exception\RuntimeException If filtering $value is impossible
     * @return mixed
     */
    public function filter($value)
    {
        if (!is_array($value)) {
            return $value;
        }
        $user = $this->getServiceLocator()->getServiceLocator()->get('user');
        if (!DocumentAccess::canView($value, $user)) {
            $value = $this->coarsenData($value);
        }

        if (isset($value['has'])) {
            $class = '\Kotka\Filter\View\MYUnit';
            if (class_exists($class)) {
                $this->getServiceLocator()->setInvokableClass($class, $class);
                foreach ($value['has'] as $key => $unit) {
                    $value['has'][$key] = StaticFilter::execute($unit, $class, ['mode' => $this->getMode()]);
                }
            }
        }

        return parent::filter($value);
    }

    private function coarsenData($value)
    {
        $this->whitelist[DocumentAccess::MODE_PRIVATE] = [
            'qname',
            'rdf:type',
            'MY.dateBegin',
            'MY.country',
            'MY.higherGeography',
            'has'
        ];
        $this->whitelist[DocumentAccess::MODE_PROTECTED] = [
            'qname',
            'rdf:type',
            'MY.dateBegin',
            'MY.country',
            'MY.higherGeography',
            'has'
        ];
        if (isset($value['MY.dateBegin'])) {
            $value['MY.dateBegin'] = $this->coarsenDate($value['MY.dateBegin']);
        }
        return $value;
    }

    private function coarsenDate($date)
    {
        $matches = [];
        preg_match('/[0-9]{3,4}/', $date, $matches);
        $year = null;
        if (isset($matches[0])) {
            $year = str_split($matches[0]);

        }
        if (is_array($year)) {
            array_pop($year);
            $last = end($year);
            $year = implode('', $year);
            if ($last == '9') {
                $year = $year . '0-' . ((int)$year + 1) . '0';
            } else {
                $year = $year . '0-' . ($last + 1) . '0';
            }
        }
        return $year;
    }

}