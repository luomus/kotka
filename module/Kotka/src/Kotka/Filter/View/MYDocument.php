<?php

namespace Kotka\Filter\View;

use Kotka\Service\DocumentAccess;
use User\Enum\User;
use Zend\Filter\Exception;
use Zend\Filter\StaticFilter;

class MYDocument extends EditableDocument
{

    /**
     * Returns the result of filtering $value
     *
     * @param  mixed $value
     * @throws Exception\RuntimeException If filtering $value is impossible
     * @return mixed
     */
    public function filter($value)
    {
        if (!is_array($value)) {
            return $value;
        }
        $user = $this->getServiceLocator()->getServiceLocator()->get('user');
        $role = $user->getMARoleKotka();
        $this->addLoadInfo($value);
        $this->addAcceptedTaxa($value);
        if (isset($value['MY.privateNotes']) && $role === User::ROLE_GUEST ) {
            unset($value['MY.privateNotes']);
        }
        if (!DocumentAccess::canView($value, $user)) {
            $value = $this->coarsenData($value);
            $value['noImage'] = true;
        }
        if (isset($value['MY.temp'])) {
            unset($value['MY.temp']);
        }
        if (isset($value['has'])) {
            $class = '\Kotka\Filter\View\MYGathering';
            $this->getServiceLocator()->setInvokableClass($class, $class);
            foreach ($value['has'] as $key => $gathering) {
                $value['has'][$key] = StaticFilter::execute($gathering, $class, ['mode' => $this->getMode()]);
            }
        }
        return parent::filter($value);
    }

    private function coarsenData($value)
    {
        $this->whitelist[DocumentAccess::MODE_PRIVATE] = [
            'rdf:type',
            'qname',
            'noImage',
            'has'
        ];
        $this->whitelist[DocumentAccess::MODE_PROTECTED] = [
            'rdf:type',
            'qname',
            'noImage',
            'has'
        ];
        return $value;
    }

    private function addAcceptedTaxa(&$value)
    {
        if (!isset($value['has'])) {
            return;
        }
        $taxa = [];
        $identifications = [];
        foreach ($value['has'] as &$gathering) {
            if (!isset($gathering['has'])) {
                continue;
            }
            foreach ($gathering['has'] as &$unit) {
                if (!isset($unit['has'])) {
                    continue;
                }
                foreach ($unit['has'] as &$identification) {
                    if (!isset($identification['MY.taxon'])) {
                        continue;
                    }
                    $taxa[$identification['MY.taxon']] = true;
                    $identifications[] = &$identification;

                }
            }
        }
        if (empty($taxa)) {
            return;
        }
        /** @var \Kotka\Service\TaxonService $taxonService */
        $taxonService = $this->getServiceLocator()->getServiceLocator()->get('Kotka\Service\TaxonService');
        $taxonService->initTaxa(array_keys($taxa));
        foreach ($identifications as &$identification) {
            $taxon = $identification['MY.taxon'];
            if (!$taxonService->hasTaxon($taxon)) {
                continue;
            }
            $identification['acceptedTaxon'] = $taxonService->getCurrentName($taxon);
            $identification['acceptedAuthor'] = $taxonService->getAuthor($taxon);
        }
    }

    private function addLoadInfo(&$value)
    {
        if (isset($value['qname'])) {
            /** @var \Kotka\Service\NewKotkaService $newKotkaService */
            $newKotkaService = $this->getServiceLocator()->getServiceLocator()->get('Kotka\Service\NewKotkaService');
            $loaned = $newKotkaService->isLoaned($value['qname']);
            if (isset($loaned) && $loaned !== '') {
                $value['loanedIn'] = $loaned;
            }
            $value['HRA.transactions'] = $newKotkaService->findTransactionsWithDocument($value['qname']);
        }
    }
}