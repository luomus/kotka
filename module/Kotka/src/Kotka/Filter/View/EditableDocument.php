<?php

namespace Kotka\Filter\View;

use Kotka\Filter\ListFilter;
use Kotka\Service\DocumentAccess;
use Kotka\Triple\MAPerson;
use Zend\Filter\Exception;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

class EditableDocument extends ListFilter implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    /**
     * Returns the result of filtering $value
     *
     * @param  mixed $value
     * @throws Exception\RuntimeException If filtering $value is impossible
     * @return mixed
     */
    public function filter($value)
    {
        if (!is_array($value)) {
            return $value;
        }

        // Add can edit variable
        /** @var MAPerson $user */
        $user = $this->getServiceLocator()->getServiceLocator()->get('user');
        $value['canEdit'] = DocumentAccess::canEdit($value, $user);

        // Remove unneeded values
        if (isset($value['MZ.scheduledForDeletion'])) {
            unset($value['MZ.scheduledForDeletion']);
        }

        return parent::filter($value);
    }
}