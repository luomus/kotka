<?php

namespace Kotka\Filter\View;

use Kotka\Filter\ListFilter;
use Kotka\Service\DocumentAccess;
use Zend\Filter\Exception;
use Zend\Filter\StaticFilter;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

class MYUnit extends ListFilter implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    /**
     * Returns the result of filtering $value
     *
     * @param  mixed $value
     * @throws Exception\RuntimeException If filtering $value is impossible
     * @return mixed
     */
    public function filter($value)
    {
        if (!is_array($value)) {
            return $value;
        }
        $user = $this->getServiceLocator()->getServiceLocator()->get('user');
        if (!DocumentAccess::canView($value, $user)) {
            $value = $this->coarsenData($value);
        }
        if (isset($value['MY.measurement'])) {
            $value['MY.measurement'] = array_filter($value['MY.measurement'], function($value) {
                $parts = explode(':', $value);
                return count($parts) > 1 && $parts[1] !== '';
            });
            if (count($value['MY.measurement']) === 0) {
                unset($value['MY.measurement']);
            }
        }
        if (isset($value['has'])) {
            $identificationClass = '\Kotka\Filter\View\MYIdentification';
            $typeClass = '\Kotka\Filter\View\MYTypeSpecimen';
            $sampleClass = '\Kotka\Filter\View\MFSample';
            $this->getServiceLocator()->setInvokableClass($identificationClass, $identificationClass);
            $this->getServiceLocator()->setInvokableClass($typeClass, $typeClass);
            $this->getServiceLocator()->setInvokableClass($sampleClass, $sampleClass);
            foreach ($value['has'] as $key => $data) {
                $type = DocumentAccess::getType($data);
                if ($type === 'MY.typeSpecimen') {
                    $value['has'][$key] = StaticFilter::execute($data, $typeClass, ['mode' => $this->getMode()]);
                } else if ($type === 'MF.sample') {
                    $value['has'][$key] = StaticFilter::execute($data, $sampleClass, ['mode' => $this->getMode()]);
                } else {
                    $value['has'][$key] = StaticFilter::execute($data, $identificationClass, ['mode' => $this->getMode()]);
                }
            }
        }
        return parent::filter($value);
    }

    private function coarsenData($value)
    {
        $this->whitelist[DocumentAccess::MODE_PRIVATE] = [
            'qname',
            'rdf:type',
            'has'
        ];
        $this->whitelist[DocumentAccess::MODE_PROTECTED] = [
            'qname',
            'rdf:type',
            'has'
        ];
        return $value;
    }
}