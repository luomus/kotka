<?php

namespace Kotka\Filter\View;

use Common\Service\IdService;
use Kotka\Filter\ListFilter;
use Zend\Filter\Exception;

class MAPerson extends ListFilter
{

    protected $whitelist = [
        ListFilter::MODE_ALL => [
            'rdf:type',
            'qname',
            'MA.inheritedName',
            'MA.preferredName',
            'MA.defaultQNamePrefix'
        ]
    ];

    public function filter($value)
    {
        if (is_array($value) && !isset($value['MA.defaultQNamePrefix'])) {
            $value['MA.defaultQNamePrefix'] = rtrim(IdService::QNAME_PREFIX_LUOMUS,':');
        }
        return parent::filter($value);
    }

}