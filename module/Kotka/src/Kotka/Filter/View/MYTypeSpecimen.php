<?php

namespace Kotka\Filter\View;

use Kotka\Filter\ListFilter;
use Kotka\Service\DocumentAccess;
use Zend\Filter\Exception;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

class MYTypeSpecimen extends ListFilter implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    /**
     * Returns the result of filtering $value
     *
     * @param  mixed $value
     * @throws Exception\RuntimeException If filtering $value is impossible
     * @return mixed
     */
    public function filter($value)
    {
        if (!is_array($value)) {
            return $value;
        }
        $user = $this->getServiceLocator()->getServiceLocator()->get('user');
        if (!DocumentAccess::canView($value, $user)) {
            $value = $this->coarsenData($value);
        }
        return parent::filter($value);
    }

    private function coarsenData($value)
    {
        $this->whitelist[DocumentAccess::MODE_PRIVATE] = [
            'qname',
            'rdf:type'
        ];
        $this->whitelist[DocumentAccess::MODE_PROTECTED] = [
            'qname',
            'rdf:type'
        ];
        return $value;
    }
}