<?php
namespace Kotka\Filter;

use Zend\Filter\AbstractFilter;

/**
 * Class ClearEmptyCollections removes empty collections from the form array
 *
 * @package Kotka\Filter
 */
class ClearEmptyCollections extends AbstractFilter
{

    /**
     * Filters the value
     *
     * @param  mixed $value
     * @return array
     */
    public function filter($value)
    {
        if (is_array($value)) {
            $this->clearEmptyCollections($value);
        }

        return $value;
    }

    private function clearEmptyCollections(&$data)
    {
        if (isset($data['MYGathering'])) {
            $gatherings = $data['MYGathering'];
            foreach ($gatherings as $k1 => $gathering) {
                if (isset($gathering['MYUnit'])) {
                    $units = $gathering['MYUnit'];
                    foreach ($units as $k2 => $unit) {
                        if (isset($unit['MYIdentification'])) {
                            $identifications = $unit['MYIdentification'];
                            foreach ($identifications as $k3 => $identification) {
                                $test = $this->clearEmpty($identification);
                                if (empty($test)) {
                                    unset($data['MYGathering'][$k1]['MYUnit'][$k2]['MYIdentification'][$k3]);
                                }
                            }
                        }
                        if (isset($unit['MYTypeSpecimen'])) {
                            $types = $unit['MYTypeSpecimen'];
                            foreach ($types as $k3 => $type) {
                                $test = $this->clearEmpty($type);
                                if (empty($test)) {
                                    unset($data['MYGathering'][$k1]['MYUnit'][$k2]['MYTypeSpecimen'][$k3]);
                                }
                            }
                        }
                        if (isset($unit['MFSample'])) {
                            $samples = $unit['MFSample'];
                            foreach ($samples as $k3 => $sample) {
                                $test = $this->clearEmpty($sample);
                                if (empty($test)) {
                                    unset($data['MYGathering'][$k1]['MYUnit'][$k2]['MFSample'][$k3]);
                                }
                            }
                        }
                        $test = $this->clearEmpty($data['MYGathering'][$k1]['MYUnit'][$k2]);
                        if (empty($test)) {
                            unset($data['MYGathering'][$k1]['MYUnit'][$k2]);
                        }
                    }
                    $test = $this->clearEmpty($data['MYGathering'][$k1]);
                    if (empty($test)) {
                        unset($data['MYGathering'][$k1]);
                    }
                }
            }
        }
        // Make sure that there is at least empty array in collections
        if (!isset($data['MYGathering'])) {
            $data['MYGathering'] = [];
        }
        foreach ($data['MYGathering'] as $k1 => $gathering) {
            if (!isset($data['MYGathering'][$k1]['MYUnit'])) {
                $data['MYGathering'][$k1]['MYUnit'] = [];
            }
            foreach ($data['MYGathering'][$k1]['MYUnit'] as $k2 => $unit) {
                if (!isset($data['MYGathering'][$k1]['MYUnit'][$k2]['MYIdentification'])) {
                    $data['MYGathering'][$k1]['MYUnit'][$k2]['MYIdentification'] = [];
                }
                if (!isset($data['MYGathering'][$k1]['MYUnit'][$k2]['MYTypeSpecimen'])) {
                    $data['MYGathering'][$k1]['MYUnit'][$k2]['MYTypeSpecimen'] = [];
                }
                if (!isset($data['MYGathering'][$k1]['MYUnit'][$k2]['MFSample'])) {
                    $data['MYGathering'][$k1]['MYUnit'][$k2]['MFSample'] = [];
                }
            }

        }
    }

    private function clearEmpty($array)
    {
        foreach ($array as &$value) {
            if (is_array($value)) {
                $value = $this->clearEmpty($value);
            }
        }
        return array_filter($array);
    }

}
