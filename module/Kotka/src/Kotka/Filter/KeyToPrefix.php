<?php
namespace Kotka\Filter;

use Zend\Filter\AbstractFilter;
use Zend\Filter\Exception;
use Zend\Stdlib\ArrayUtils;

class KeyToPrefix extends AbstractFilter
{

    protected $options = array(
        'delimiter' => ':',
        'explode' => ';'
    );

    /**
     * Class constructor
     *
     * @param array|\Traversable $options (Optional) Options to set
     */
    public function __construct($options = null)
    {
        if ($options instanceof \Traversable) {
            $options = ArrayUtils::iteratorToArray($options);
        }
        if (isset($options['delimiter'])) {
            $this->setDelimiter($options['delimiter']);
        }
    }

    public function getDelimiter()
    {
        return $this->options['delimiter'];
    }

    public function setDelimiter($delimiter)
    {
        if (!is_string($delimiter)) {
            throw new \Exception("Delimiter should be a string");
        }
        $this->options['delimiter'] = $delimiter;
    }

    public function getExplode()
    {
        return $this->options['explode'];
    }

    public function setExplode($explode)
    {
        if (!is_string($explode)) {
            throw new \Exception("Delimiter should be a string");
        }
        $this->options['explode'] = $explode;
    }

    /**
     * Returns the result of filtering $value
     *
     * Value is given as a array and it's joined into a string using the defined delimeter
     * If value is not array the the value is returned as it is
     *
     * @param  mixed $value
     * @throws Exception\RuntimeException If filtering $value is impossible
     * @return array
     */
    public function filter($value)
    {
        if (!is_array($value)) {
            return $value;
        }
        $delim = $this->getDelimiter();
        $explode = $this->getExplode();
        $result = [];
        foreach ($value as $k => $v) {
            if (is_numeric($k)) {
                $pos = strpos($v, $delim);
                if ($pos !== false) {
                    $prefix = substr($v, 0, $pos);
                    $v = substr($v, $pos + 1);
                    $this->addValues($prefix, $delim, $explode, $v, $result);
                } else {
                    $result[] = $v;
                }
            } else {
                $this->addValues($k, $delim, $explode, $v, $result);
            }
        }
        return $result;
    }

    private function addValues($prefix, $delim, $explode, $value, &$results) {
        $parts = explode($explode, $value);
        foreach ($parts as $partValue) {
            $results[] = $prefix . $delim . $partValue;
        }
    }
}