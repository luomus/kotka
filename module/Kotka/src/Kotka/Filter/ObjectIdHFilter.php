<?php

namespace Kotka\Filter;

use Zend\Filter\AbstractFilter;

/**
 * Class ObjectIdHFilter filters the value so that the if it has H in the objectid it will be capitalized
 *
 * @package Kotka\Filter
 */
class ObjectIdHFilter extends AbstractFilter
{

    /**
     * Returns the result of filtering $value
     *
     * @param  mixed $value
     * @throws \Zend\Filter\Exception\RuntimeException If filtering $value is impossible
     * @return mixed
     */
    public function filter($value)
    {
        if (preg_match('{^h[0-9]*$}', $value)) {
            return str_replace('h', 'H', $value);
        }
        return $value;
    }
}