<?php
namespace Kotka\Filter;

use Zend\Filter\AbstractFilter;

/**
 * Class Coordinate
 *
 * @package Kotka\Filter
 */
class Coordinate extends AbstractFilter
{

    /**
     * Filters coordinate value and c
     *
     * @param  mixed $value
     * @return mixed
     */
    public function filter($value)
    {
        if (!is_string($value)) {
            return $value;
        }
        return str_replace(array("*", "d", "m", "s", '”', "''"), array('°', '°', "'", '"', '"', '"'), $value);
    }

}
