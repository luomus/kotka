<?php
namespace Kotka\Filter;

use Traversable;
use Zend\Filter\AbstractFilter;

/**
 * Class PrefixRemover removes prefix from the value.
 * Example:
 *  "http://id.luomus.fi/HRA.123" with prefix value "http://id.luomus.fi/" would become "HRA.123"
 *  if the prefix isn't at the beginning then nothing is changed and the value is returned as is.
 *
 * @package Kotka\Filter
 */
class PrefixRemover extends AbstractFilter
{
    /**
     * @var array
     */
    protected $options = array(
        'prefix' => null,
        'capitalize' => true,
    );

    /**
     * @param array|Traversable $options
     */
    public function __construct($options)
    {
        $this->setOptions($options);
    }

    /**
     * Sets a new prefix for this filter
     *
     * @param  callable $prefix
     * @throws Exception\InvalidArgumentException
     * @return self
     */
    public function setPrefix($prefix)
    {
        if (!is_string($prefix)) {
            throw new Exception\InvalidArgumentException(
                'Invalid parameter for prefix: must be a string'
            );
        }
        $this->options['prefix'] = $prefix;

        return $this;
    }

    /**
     * Returns the prefix
     *
     * @return string
     */
    public function getPrefix()
    {
        return $this->options['prefix'];
    }

    /**
     * Filters the value and makes sure that the prefix is in front of the value
     *
     * @param  string $value
     * @return string Result with the prefix in front of it
     */
    public function filter($value)
    {
        if ($value === null || $value === '') {
            return $value;
        }
        if (is_array($value)) {
            array_walk($value, array($this, 'remove'));
        } else {
            $this->remove($value);
        }

        return $value;
    }

    private function remove(&$value)
    {
        if (!is_string($value)) {
            return;
        }
        $prefix = $this->options['prefix'];
        if (strpos($value, $prefix) === 0) {
            $count = 1;
            $value = str_replace($prefix, '', $value, $count);
        }
    }

}
