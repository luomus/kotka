<?php
namespace Kotka\Filter;

use Zend\Filter\AbstractFilter;

/**
 * Class FloatNormalizer formalizes float number to 123123.2345
 *
 * @package Kotka\Filter
 */
class FloatNormalizer extends AbstractFilter
{

    /**
     * Filters array so that the empty values are removed from the array
     *
     * @param  mixed $value
     * @return mixed
     */
    public function filter($value)
    {
        if (!is_string($value)) {
            return $value;
        }
        // first let's remove empty spaces
        $value = str_replace(' ', '', $value);

        // get last positions of dot and comma
        $lastDot = strrpos($value, '.');
        $lastComma = strrpos($value, ',');

        if ($lastDot === false && $lastComma === false) { // Whole number
            return $value;
        } else if ($lastComma === false) { // float in correct form
            return $value;
        }
        // there is comma present
        if ($lastDot === false) { // only comma
            $value = str_replace(',', '.', $value);
        } else {
            if ($lastComma > $lastDot) { // in case of number is 11.000,00
                $value = str_replace('.', '', $value);
                $value = str_replace(',', '.', $value);
            } else { // in case number is 11,000.00
                $value = str_replace(',', '', $value);
            }
        }

        return $value;
    }
}
