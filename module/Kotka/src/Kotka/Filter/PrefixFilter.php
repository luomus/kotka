<?php
namespace Kotka\Filter;

use Traversable;
use Zend\Filter\AbstractFilter;

/**
 * Class PrefixFilter makes sure that the value has prefix in front of it
 * Example
 *  if prefix is "MX." and the given value is "species" then the value would be changed to "MX.species"
 *  if the given value would be "MX.species" then nothing would be changed.
 *
 * @package Kotka\Filter
 */
class PrefixFilter extends AbstractFilter
{
    /**
     * @var array
     */
    protected $options = array(
        'prefix' => null,
    );

    /**
     * @param array|Traversable $options
     */
    public function __construct($options)
    {
        $this->setOptions($options);
    }

    /**
     * Sets a new prefix for this filter
     *
     * @param  callable $prefix
     * @throws Exception\InvalidArgumentException
     * @return self
     */
    public function setPrefix($prefix)
    {
        if (!is_string($prefix)) {
            throw new Exception\InvalidArgumentException(
                'Invalid parameter for prefix: must be a string'
            );
        }
        $this->options['prefix'] = $prefix;

        return $this;
    }

    /**
     * Returns the prefix
     *
     * @return string
     */
    public function getPrefix()
    {
        return $this->options['prefix'];
    }

    /**
     * Filters the value and makes sure that the prefix is in front of the value
     *
     * @param  string $value
     * @return string Result with the prefix in front of it
     */
    public function filter($value)
    {
        if (is_array($value)) {
            array_walk($value, array($this, 'addPrefix'));
        } else {
            $this->addPrefix($value);
        }

        return $value;
    }

    private function addPrefix(&$value)
    {
        if ($value === null || $value === '') {
            return;
        }
        $prefix = $this->options['prefix'];
        $lowerPrefix = mb_convert_case($prefix, MB_CASE_LOWER, 'utf-8');
        $valuePrefix = mb_convert_case($value, MB_CASE_LOWER, 'utf-8');
        if (strpos($valuePrefix, $lowerPrefix) === 0) {
            return;
        }
        $value = $prefix . $value;

    }
}
