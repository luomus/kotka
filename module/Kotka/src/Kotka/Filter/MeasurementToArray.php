<?php

namespace Kotka\Filter;

use Zend\Filter\Exception;

class MeasurementToArray extends StringToArray
{
    /**
     * Returns the result of filtering $value
     *
     * Value is given as a string and it's split into array using the defined delimeter
     *
     * @param  mixed $value
     * @throws Exception\RuntimeException If filtering $value is impossible
     * @return array
     */
    public function filter($value)
    {
        $delimiter = $this->getDelimiter();
        if (is_string($value)) {
            $data = preg_split('/' . $delimiter . '/', $value, -1, PREG_SPLIT_NO_EMPTY);
            return array_map('trim', $data);
        } elseif (!is_array($value)) {
            throw new Exception\RuntimeException('Value should be string or an array!');
        }
        $return = [];
        foreach ($value as $val) {
            $array = preg_split('/' . $delimiter . '/', $val, -1, PREG_SPLIT_NO_EMPTY);
            $return = array_merge($return, array_map('trim', $array));
        }
        return $return;
    }
}