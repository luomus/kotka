<?php

namespace Kotka\Service;

/**
 * Class ExcelParams
 *
 * Interpreters the parameters given to excel generator
 *
 * @package Kotka\Service
 */
class ExcelParams
{
    const SPLIT = '_split_';
    const DATA = '_data_';

    private $lang;

    private $info = array(
        'gatherings' => array(
            'cnt' => 'num-of-gatherings'
        ),
        'units' => array(
            'cnt' => 'num-of-units'
        ),
        'identifications' => array(
            'cnt' => 'num-of-identifications'
        ),
        'types' => array(
            'cnt' => 'num-of-types'
        )
    );

    private $params;

    public function getLanguage()
    {
        return $this->lang;
    }

    public function setParams($params)
    {
        if (isset($params['language'])) {
            $this->lang = $params['language'];
            unset($params['language']);
        }
        $this->params = $params;

        return $this;
    }

    public function getParams()
    {
        return $this->params;
    }

    public function getProcessedParams()
    {
        return $this->processParams($this->params);
    }

    private function processParams($params)
    {
        $return = array();
        $counts = array();
        foreach ($this->info as $fieldset => $data) {
            if (isset($params[$data['cnt']])) {
                $counts[$fieldset] = $params[$data['cnt']];
                unset($params[$data['cnt']]);
            } else {
                $counts[$fieldset] = 1;
            }
        }
        if (isset($params['spot'])) {
            $params = $params['spot'];
        }
        $documents = array('MYNamespaceID', 'MYObjectID');
        $gatherings = array();
        $units = array();
        $identifications = array();
        $types = array();
        foreach ($params as $param) {
            if (strpos($param, '__type__') !== false) {
                $types[] = $param;
            } else if (strpos($param, '__identification__') !== false) {
                $identifications[] = $param;
            } else if (strpos($param, '__unit__') !== false) {
                $units[] = $param;
            } else if (strpos($param, '__gathering__') !== false) {
                $gatherings[] = $param;
            } else {
                $documents[] = $param;
            }
        }
        $specimen = array();
        $elem = array();
        foreach ($documents as $document) {
            parse_str($document . "=", $elem);
            $specimen = array_replace_recursive($specimen, $elem);
            $return[] = array('uri' => $document, 'prefix' => '', 'data' => $elem);
        }
        $return[] = self::SPLIT;
        for ($i = 0; $i < $counts['gatherings']; $i++) {
            foreach ($gatherings as $gathering) {
                $gathering = str_replace('__gathering__', $i, $gathering);
                parse_str($gathering . "=", $elem);
                $specimen = array_replace_recursive($specimen, $elem);
                $return[] = array('uri' => $gathering, 'prefix' => '' . ($i + 1), 'data' => $elem);
            }
            $return[] = self::SPLIT;
            for ($j = 0; $j < $counts['units']; $j++) {
                foreach ($units as $unit) {
                    $unit = str_replace('__gathering__', $i, $unit);
                    $unit = str_replace('__unit__', $j, $unit);
                    parse_str($unit . "=", $elem);
                    $specimen = array_replace_recursive($specimen, $elem);
                    $return[] = array('uri' => $unit, 'prefix' => '' . ($i + 1) . '.' . ($j + 1), 'data' => $elem);
                }
                $return[] = self::SPLIT;
                for ($k = 0; $k < $counts['identifications']; $k++) {
                    foreach ($identifications as $identification) {
                        $identification = str_replace('__gathering__', $i, $identification);
                        $identification = str_replace('__unit__', $j, $identification);
                        $identification = str_replace('__identification__', $k, $identification);
                        parse_str($identification . "=", $elem);
                        $specimen = array_replace_recursive($specimen, $elem);
                        $return[] = array('uri' => $identification, 'prefix' => '' . ($i + 1) . '.' . ($j + 1) . '.' . ($k + 1) . 'i', 'data' => $elem);
                    }
                    $return[] = self::SPLIT;
                }
                for ($k = 0; $k < $counts['types']; $k++) {
                    foreach ($types as $type) {
                        $type = str_replace('__gathering__', $i, $type);
                        $type = str_replace('__unit__', $j, $type);
                        $type = str_replace('__type__', $k, $type);
                        parse_str($type . "=", $elem);
                        $specimen = array_replace_recursive($specimen, $elem);
                        $return[] = array('uri' => $type, 'prefix' => '' . ($i + 1) . '.' . ($j + 1) . '.' . ($k + 1) . 't', 'data' => $elem);
                    }
                    $return[] = self::SPLIT;
                }
            }
        }
        $return[self::DATA] = $specimen;
        return $return;
    }
}
