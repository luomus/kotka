<?php

namespace Kotka\Service;

use Common\Service\IdService;
use DateTime;
use Kotka\Triple\KotkaEntityInterface;
use Kotka\Triple\MAPerson;
use Triplestore\Stdlib\OntologyInterface;
use Zend\EventManager\EventManager;

/**
 * Abstract service for common controllers to use.
 *
 * @package Kotka\Service
 */
abstract class DocumentService
{
    /**
     * @var MAPerson
     */
    protected $user;
    /** @var  \Common\Service\IdService */
    protected $idGen;

    protected $editor;
    protected $creator;

    /**
     * Initializes the object
     *
     * @param MAPerson $user
     * @param          $idGenerator
     *
     */
    public function __construct(MAPerson $user, $idGenerator)
    {
        $this->user = $user;
        $this->idGen = $idGenerator;
    }

    /**
     * This sets all id related data to the document
     *
     * @param OntologyInterface $document
     * @param $checkExisting
     * @throws Exception\LogicException
     */
    protected function setId(OntologyInterface $document, $checkExisting = true)
    {
        $this->idGen->clear();
        $qname = $document->getSubject();
        if (!empty($qname)) {
            $this->checkIfExists($qname, $checkExisting);
            return;
        }
        $this->idGen->generateFromPieces(null, null, $document->getType());
        $this->checkIfExists($this->idGen->getPQDN(), $checkExisting);
        $document->setSubject($this->idGen->getPQDN());
    }

    private function checkIfExists($subject, $checkExisting) {
        if (!$checkExisting) {
            return;
        }
        if ($this->getById($subject) !== null) {
            throw new Exception\LogicException(sprintf("Unable to set uri. Object with uri '%s' already exists.", $subject));
        }
    }

    /**
     * This sets the editor related stuff to the document
     *
     * isCreate null means that there should not be even triggered
     *          true means that also created data is added to the document and created event is triggered
     *          false means that only update event is triggered
     * In all cases update date is added to the document
     *
     * @param KotkaEntityInterface $document
     * @param mixed $isCreate
     * @throws Exception\LogicException
     */
    protected function setEdit(KotkaEntityInterface $document, $isCreate = false)
    {
        $now = new \DateTime();
        $usersQname = $this->user->getSubject();
        $document->setMZDateEdited($now);
        if ($this->editor === null) { // This will be empty string when we want to store no editor
            $document->setMZEditor($usersQname);
        } else {
            $document->setMZEditor($this->editor);
        }
        if ($this->creator !== null) {
            $document->setMZCreator($this->creator);
        }
        if ($isCreate === null) {
            return;
        } else if ($isCreate) {
            $event = new EventManager('add');
            $current = $document->getMZDateCreated();
            if (empty($current)) {
                $document->setMZDateCreated($now);
            }
            if ($this->creator === null) { // This will be empty string when we want to store no creator
                $document->setMZCreator($usersQname);
            }
        } else {
            $event = new EventManager('edit');
        }
        $event->trigger(get_class($document), null, array(
            'subject' => $document->getSubject(),
            'user' => $usersQname
        ));
    }

    /**
     * @param mixed $editor
     */
    public function setEditor($editor)
    {
        $this->editor = $editor;
    }

    /**
     * @return mixed
     */
    public function getEditor()
    {
        return $this->editor;
    }

    /**
     * @param mixed $creator
     */
    public function setCreator($creator)
    {
        $this->creator = $creator;
    }

    /**
     * @return mixed
     */
    public function getCreator()
    {
        return $this->creator;
    }


    /**
     * Checks whether or not the parameter is null or empty string
     * @param $var
     * @return bool
     */
    protected function isEmpty($var)
    {
        return $var === null || $var == '';
    }

    abstract public function getById($id);

}
