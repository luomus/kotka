<?php
namespace Kotka\Service;

use Zend\ServiceManager\ServiceLocatorInterface;

trait AccessionServiceTrait
{

    protected $accessionService;

    /**
     * @return \Kotka\Service\AccessionService
     * @throws \Exception
     */
    public function getAccessionService()
    {
        if ($this->accessionService === null) {
            if ($this->serviceLocator instanceof ServiceLocatorInterface) {
                $this->accessionService = $this->serviceLocator->get('Kotka\Service\AccessionService');
            } else {
                throw new \Exception('Could not find serviceLocator to use in AccessionsServiceTrait');
            }
        }
        return $this->accessionService;
    }

}