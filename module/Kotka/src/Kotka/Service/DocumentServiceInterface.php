<?php

namespace Kotka\Service;

/**
 * Interface DocumentServiceInterface
 * contains all the common methods needed by the common controller
 * @package Kotka\Service
 */
interface DocumentServiceInterface
{
    /**
     * Returns all the items of the given document type in an array
     *
     * You can specify witch fields you want to return. If you  don't then
     * all the fields are used.
     *
     * @param array $fields
     * @return mixed
     */
    public function getAll(array $fields = null);

    /**
     * Returns one item based on the uri
     *
     * @param $id
     * @return mixed
     */
    public function getById($id);

    /**
     * Creates new item
     * @param $document
     * @return mixed
     */
    public function create($document);

    /**
     * Updates existing item
     * @param $document
     * @return mixed
     */
    public function update($document);

    /**
     * Returns new object of the given type
     * @return mixed
     */
    public function getClassName();

}
