<?php

namespace Kotka\Service;

use Kotka\Mapper\MFSampleMapperInterface;
use Kotka\Triple\MAPerson;
use Kotka\Triple\MFSample;

/**
 * Class SampleService contains the business logic for samples.
 * This should be the only place where samples are handled!
 *
 * This service inherits few common methods from DocumentService
 *
 * @package Kotka\Service
 */
class SampleService extends DocumentService implements DocumentServiceInterface
{
    /** @var \Kotka\Mapper\MFSampleMapperInterface */
    protected $sampleMapper;

    /**
     * Initializes the object
     *
     * @param MFSampleMapperInterface $datasetMapper
     * @param MAPerson $authenticationService
     * @param $config
     */
    public function __construct(MFSampleMapperInterface $datasetMapper, MAPerson $authenticationService, $config)
    {
        $this->sampleMapper = $datasetMapper;
        parent::__construct($authenticationService, $config);
    }

    /**
     * Returns all the sample in the database.
     *
     * @param $fields
     * @return array of MFSample objects
     */
    public function getAll(array $fields = null)
    {
        return $this->sampleMapper->findBy(array(), array('datasetName' => 'ASC'), null, null, $fields);
    }

    /**
     * Return one sample object based on the uri
     * @param $id
     * @return MFSample
     */
    public function getById($id)
    {
        return $this->sampleMapper->find($id);
    }

    /**
     * Stores new dataset object to the database
     * @param MFSample $sample
     * @return MFSample
     * @throws Exception\DomainException
     */
    public function create($sample)
    {
        if (!$sample instanceof MFSample) {
            throw new Exception\DomainException('Sample service can\'t handle element of type ' . get_class($sample));
        }
        $this->setId($sample);
        $this->setEdit($sample, true);

        return $this->sampleMapper->create($sample);
    }

    /**
     * Updates existing object
     *
     * @param MFSample $sample
     * @return MFSample
     */
    public function update($sample)
    {
        $this->setEdit($sample);

        return $this->sampleMapper->update($sample);
    }

    /**
     * Returns new sample object
     * @return string class name
     */
    public function getClassName()
    {
        return $this->sampleMapper->getClassName();
    }
}
