<?php

namespace Kotka\Service;

use Common\Service\IdService;
use Kotka\Mapper\MYCollectionMapperInterface;
use Kotka\Triple\MAPerson;
use Kotka\Triple\MYCollection;

/**
 * Class CollectionService contains the business logic for collections.
 * This should be the only place where collections are handled!
 *
 * This service inherits few common methods from DocumentService
 *
 * @package Kotka\Service
 * @see Kotka\Service\DocumentService::
 */
class CollectionService extends DocumentService implements DocumentServiceInterface
{
    /** @var \Kotka\Mapper\MYCollectionMapperInterface */
    protected $collectionMapper;

    protected $abbreviationCache = [];

    /**
     * Initializes the object
     * @param MYCollectionMapperInterface $collectionMapper
     * @param MAPerson $authenticationService
     * @param $config
     */
    public function __construct(MYCollectionMapperInterface $collectionMapper, MAPerson $authenticationService, $config)
    {
        parent::__construct($authenticationService, $config);
        $this->collectionMapper = $collectionMapper;
    }

    /**
     * Returns all the collections
     *
     * @param $fields
     * @return array of collection objects
     */
    public function getAll(array $fields = null)
    {
        return $this->collectionMapper->findBy(array(), array('MY.collectionName' => 'ASC'), null, null, $fields);
    }

    /**
     * Returns the collection object with the given uri
     *
     * @param $uri
     * @return MYCollection
     */
    public function getById($uri)
    {
        return $this->collectionMapper->find($uri);
    }

    /**
     * This persists the collection object given to this.
     *
     * @param  MYCollection $collection
     * @return MYCollection
     * @throws Exception\DomainException
     */
    public function create($collection)
    {
        if (!$collection instanceof MYCollection) {
            throw new Exception\DomainException('Organization service can\'t handle element of type ' . get_class($collection));
        }
        $this->setId($collection);
        $this->setEdit($collection, true);

        return $this->collectionMapper->create($collection);
    }

    /**
     * Updates the given Collection object
     *
     * @param  MYCollection $collection
     * @return MYCollection
     * @throws Exception\DomainException
     */
    public function update($collection)
    {
        $this->setEdit($collection);

        return $this->collectionMapper->update($collection);
    }

    /**
     * Returns class name
     * @return mixed
     */
    public function getClassName()
    {
        return $this->collectionMapper->getClassName();
    }

    /**
     * Finds the collection from parents that has the collection abbreviation
     *
     * @param $uri
     * @return string
     */
    public function getCollectionAbbreviation($uri)
    {
        $dbQName = IdService::getQName($uri, true);
        if (isset($this->abbreviationCache[$dbQName])) {
            return $this->abbreviationCache[$dbQName];
        }
        /** @var MYCollection $collection */
        $collection = $this->getById($uri);
        if (!empty($collection->getMYAbbreviation())) {
            $this->abbreviationCache[$dbQName] = $collection->getMYAbbreviation();

            return $this->abbreviationCache[$dbQName];
        } elseif (!empty($collection->getMYIsPartOf())) {
            $abbr = $this->getCollectionAbbreviation($collection->getMYIsPartOf());
            $this->abbreviationCache[$dbQName] = $abbr;

            return $abbr;
        }
        return '';
    }
}
