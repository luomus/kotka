<?php

namespace Kotka\Service;


use Zend\Mail\Message;
use Zend\Mail\Transport;
use Zend\Mail\Transport\TransportInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\View\Model\ViewModel;
use Zend\View\Renderer\PhpRenderer;
use Zend\View\Resolver\TemplateMapResolver;

class EmailService implements ServiceLocatorAwareInterface
{
    const EMAIL_PATH = 'data/mail';
    private $fromAddress = 'kotka@luomus.fi';

    private $serviceLocator;

    private $mail;
    private $transport;
    private $fileTransport;

    public function setSubject($subject)
    {
        $this->getMail()->setSubject($subject);
    }

    public function addTo($email, $fullname = null)
    {
        $this->getMail()->addTo($email, $fullname);
    }

    public function sendTemplateEmail($template, array $variables = null)
    {
        $emailView = new ViewModel();
        $emailView->setTerminal(true);
        $emailView->setTemplate('kotka/email/' . $template);

        $resolver = new TemplateMapResolver();
        $resolver->setMap($this->serviceLocator->get('ViewTemplateMapResolver'));

        $renderer = $this->getServiceLocator()->has('viewrenderer') ?
            $this->getServiceLocator()->get('viewrenderer') :
            new PhpRenderer();
        $renderer->setResolver($resolver);

        if ($variables !== null) {
            $emailView->setVariables($variables);
        }
        $body = $renderer->render($emailView);

        return $this->sendEmail($body);
    }

    public function sendEmail($body)
    {
        $mail = $this->getMail();
        $mail->setBody($body);

        return $this->sendMessage($mail);
    }

    public function sendMessage(Message $mail)
    {
        $transport = $this->getTransport();
        if (!$transport instanceof Transport\File) {
            $this->getFileTransport()->send($mail);
        }
        return $transport->send($mail);
    }

    /**
     * @return Message
     */
    public function getMail()
    {
        if ($this->mail === null) {
            $this->mail = new Message();
            $this->mail->setEncoding('UTF-8');
            $this->mail->setFrom($this->fromAddress);
        }
        return $this->mail;
    }

    /**
     * @return TransportInterface
     */
    private function getTransport()
    {
        if ($this->transport === null) {
            $this->transport = $this->getServiceLocator()->get('mail-transport');
        }
        return $this->transport;
    }

    /**
     * @return Transport\File;
     */
    private function getFileTransport()
    {
        if ($this->fileTransport === null) {
            $transport = new Transport\File();
            $options = new Transport\FileOptions(['path' => self::EMAIL_PATH]);
            $transport->setOptions($options);
            $this->fileTransport = $transport;
        }
        return $this->fileTransport;
    }

    /**
     * Set service locator
     *
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    /**
     * Get service locator
     *
     * @return ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

} 