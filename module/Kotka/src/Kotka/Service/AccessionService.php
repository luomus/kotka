<?php

namespace Kotka\Service;

use Common\Service\IdService;
use Elastic\Query\Query;
use Kotka\ElasticExtract\Branch;
use Kotka\Job\Elastic as ElasticJob;
use Kotka\Job\Elastic;
use Kotka\ResultSet\AccessionResultSet;
use Kotka\Triple\MAPerson;
use Kotka\Triple\PUUBranch;
use Kotka\Triple\PUUEvent;
use Triplestore\Classes\AbstractOntologyInterface;
use Triplestore\Classes\PUUBranchInterface;
use Triplestore\Classes\PUUEventInterface;
use Triplestore\Db\Adapter\Adapter;
use Triplestore\Db\Criteria;
use Triplestore\Service\ObjectManager;
use Triplestore\Service\PhpVariableConverter;
use Triplestore\Stdlib\ModelSerializableInterface;
use Zend\Log\Logger;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

class AccessionService implements ServiceLocatorAwareInterface
{

    const LINK_KEY = '_link_';

    use ServiceLocatorAwareTrait;

    protected $om;
    protected $indexer;
    protected $userQName;
    protected $queue;
    protected $logger;

    public function getEventById($id)
    {
        return $this->getBySubject($id, PUUEventInterface::class);
    }

    public function getBranchById($id)
    {
        return $this->getBySubject($id, PUUBranchInterface::class);
    }

    public function getEventsByBranch($id, $asArray = false) {
        if ($asArray) {
            /** @var Adapter $adapter */
            $adapter = $this->serviceLocator->get('Triplestore\Db\Adapter\Array');
            /** @var ObjectManager $om */
            $db = $adapter->getDbName();
            $queryResults = $adapter->query("Select subjectname from $db.RDF_STATEMENTVIEW where PREDICATENAME = 'PUU.branchID' AND OBJECTNAME = :id", ['id' => $id]);
            $results = [];
            foreach ($queryResults as $result) {
                $results[] = $result->getArrayCopy();
            }
            return $results;
        }
        $om = $this->getObjectManager();
        $criteria = new Criteria();
        $criteria->setOr(['PUU.branchID' => $id]);
        return $om->className(PUUEvent::class)->findBy($criteria);
    }

    private function getBySubject($id, $class) {
        if (empty($id)) {
            return null;
        }
        $model = $this->getObjectManager()->fetch(IdService::getQName($id, true));
        if (!$model instanceof $class) {
            return null;
        }
        return $model;
    }

    public function getAccession($id)
    {
        /** @var \Kotka\Service\SpecimenService $specimenService */
        $specimenService = $this->getServiceLocator()->get('Kotka\Service\SpecimenService');
        return $specimenService->getById(IdService::getQName($id, true));
    }

    public function storeEvent(PUUEventInterface $event)
    {
        return $this->store($event, $event->getPUUBranchID());
    }

    public function deleteEvent(PUUEventInterface $event)
    {
        return $this->delete($event, $event->getPUUBranchID());
    }

    public function storeBranch(PUUBranchInterface $branch)
    {
        return $this->store($branch, $branch->getSubject(), $branch->getPUUAccessionID());
    }

    public function deleteBranch(PUUBranchInterface $branch)
    {
        return $this->delete($branch, $branch->getSubject(), $branch->getPUUAccessionID());
    }

    private function store(AbstractOntologyInterface $object, $branchID, $documentID = null)
    {
        $om = $this->getObjectManager();
        if ($object->getSubject() === null || $object->getSubject() === '') {
            $idGen = new IdService($om->getConnection());
            $idGen->generateFromPieces(null, null, $object->getType());
            $object->setSubject($idGen->getPQDN());
        }
        if ($object instanceof ModelSerializableInterface) {
            if ($om->save($object)) {
                $this->createBranchJobs($branchID, $documentID);
                return true;
            }
        }
        return false;
    }

    private function delete($object, $branchID, $documentID = null)
    {
        $om = $this->getObjectManager();
        if ($object instanceof ModelSerializableInterface) {
            if ($om->remove($object)) {
                $this->createBranchJobs($branchID, $documentID);
                return true;
            }
        }
        return false;
    }

    public function getUserQName()
    {
        if ($this->userQName === null) {
            $user = $this->getServiceLocator()->get('user');
            if ($user instanceof MAPerson) {
                $this->userQName = $user->getSubject();
            }
        }
        return $this->userQName;
    }

    public function getNodeByUri($uri)
    {
        $subject = IdService::getQName($uri);
        $query = new Query(Branch::getIndexName(), Branch::getTypeName());
        $query->setQueryString('subject: "' . $subject . '"');
        $query->setSize(1);

        $result = $this->getBranchesByQuery($query);
        return $result->getBranch($subject);
    }

    /**
     * @param $root
     * @param bool $usingSearchEngine
     * @param bool $branchesOnly
     * @return AccessionResultSet
     */
    public function getAllBranchesByRoot($root, $usingSearchEngine = true, $branchesOnly = false)
    {
        if ($usingSearchEngine) {
            $results = $this->getAllBranchesFromSearchEngine($root);
            if (count($results) > 0) {
                return $results;
            }
        }

        return $this->getAllBranchesFromDb($root, $branchesOnly);
    }

    public function sortEvents(&$events) {
        if (isset($events[0]) && $events[0] instanceof PUUEventInterface) {
            usort($events, function(PUUEventInterface $a, PUUEventInterface $b) {
                $diff = strcmp($b->getPUUDate(), $a->getPUUDate());
                if ($diff !== 0) {
                    return $diff;
                }
                return strcmp($b->getSubject(), $a->getSubject());
            });
        } else if (isset($events[0]) && is_array($events[0]) && isset($events[0]['PUU.date'])) {
            usort($events, function($a, $b) {
                $diff = strcmp(isset($b['PUU.date']) ? $b['PUU.date'] : '0000-00-00', isset($a['PUU.date']) ? $a['PUU.date'] : '0000-00-00');
                if ($diff !== 0) {
                    return $diff;
                }
                return strcmp($b['subject'], $a['subject']);
            });
        }
    }

    /**
     * Expects the array of events to be already sorted
     *
     * @param $events
     * @return int
     */
    public function getTotal($events) {
        $total  = 0;
        foreach ($events as $event) {
            /** @var PUUEvent $event */
            $val = trim($this->getValue($event, 'PUU.amount'));
            if ($val === '') {
                continue;
            }
            $firstChar = substr($val, 0, 1);
            $amount = intval(preg_replace('/[^\d]/', '', $val));
            if ($firstChar === '+') {
                $total += $amount;
            } else if ($firstChar === '-') {
                $total -= $amount;
            } else {
                $total += $amount;
                break;
            }
        }
        return $total;
    }

    private function getValue($source, $key) {
        if (is_object($source)) {
            $method = 'get' . PhpVariableConverter::toPhpMethod($key);
            return $source->{$method};
        } else if (isset($source[$key])) {
            return $source[$key];
        }
    }

    /**
     * @param $root
     * @return AccessionResultSet
     */
    private function getAllBranchesFromSearchEngine($root)
    {
        $query = new Query(Branch::getIndexName(), Branch::getTypeName());
        $query->setQueryString('root: "' . $root . '"');
        $query->setSize(100000);

        return $this->getBranchesByQuery($query, $root);
    }

    private function createBranchJobs($subject, $documentID = null)
    {
        try {
            if ($documentID !== null) {
                // Document elastic job is also firing branch job
                $job = new ElasticJob();
                $job->setContent(['ids' => [$documentID]]);
                $this->getQueue()->push($job, array('delay' => 3));
            } else {
                $job = new ElasticJob();
                $job->setContent(['ids' => [$subject], 'type' => 'branch']);
                $this->getQueue()->push($job, array('delay' => 2));
            }
        } catch(\Exception $e) {
            $this->getLogger()->crit('Adding ' . $subject . ' to background job failed!', [$e->getMessage()]);
        }
    }

    /**
     * @param $root
     * @param bool $branchesOnly
     * @return AccessionResultSet
     */
    private function getAllBranchesFromDb($root, $branchesOnly = false)
    {
        $subject = IdService::getQName($root, true);
        $branches = [];
        $events = [];
        $this->getBranchesByParent($subject, $branches, $events, $branchesOnly);
        $result = new AccessionResultSet();
        $result->initByArray($subject, $branches,$events, $this->getAllCollections());

        return $result;
    }

    private function getAllCollections()
    {
        $elementManager = $this->getServiceLocator()->get('Kotka\Service\FormElementService');
        return $elementManager->getSelect('MY.collectionID');
    }

    private function getBranchesByParent($parent, &$branchData = [], &$eventData = [], $branchesOnly = false)
    {
        $om = $this->getObjectManager();
        $criteria = new Criteria();
        $criteria->setOr(['PUU.accessionID' => $parent]);
        $branchData = $om
            ->className(PUUBranch::class)
            ->findBy($criteria);
        if ($branchData === null || $branchesOnly) {
            return;
        }
        $branchSubject = [];
        foreach ($branchData as $branch) {
            /** @var PUUBranch $branch */
            $branchSubject[] = IdService::getQName($branch->getSubject(), true);
        }
        if (empty($branchSubject)) {
            return;
        }
        $eventData = $this->getEventsByBranch($branchSubject);
    }

    public function getBranchesByQuery(Query $query, $root = null)
    {
        /** @var \Elastic\Client\Search $elastic */
        $search = $this->getServiceLocator()->get('Elastic\Client\Search');
        $resultSet = $search->query($query)->execute();
        $accessionResult = new AccessionResultSet();
        $accessionResult->initByElasticResultSet($resultSet, $root);

        return $accessionResult;
    }

    /**
     * @return \Elastic\Client\Index
     */
    public function getIndexer()
    {
        if ($this->indexer === null) {
            $this->indexer = $this->getServiceLocator()->get('Elastic\Client\Index');
        }
        return $this->indexer;
    }

    /**
     * @return ObjectManager
     */
    protected function getObjectManager()
    {
        if ($this->om === null) {
            $this->om = $this->getServiceLocator()->get('Triplestore\ObjectManager');
        }
        return $this->om;
    }


    /**
     * @return Logger
     */
    protected function getLogger()
    {
        if ($this->logger === null) {
            $this->logger = $this->getServiceLocator()->get('Logger');
        }
        return $this->logger;
    }

    protected function getQueue()
    {
        if ($this->queue === null) {
            $this->queue = $this->getServiceLocator()->get('SlmQueue\Queue\QueuePluginManager')->get('default');
        }

        return $this->queue;
    }

}