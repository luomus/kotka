<?php

namespace Kotka\Service;


use Kotka\Form\WarningForm;
use Zend\Form\Element\Collection;
use Zend\Form\Factory;
use Zend\Form\Fieldset;
use Zend\Form\Form;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

class LajiForm implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    /**
     * @param $id
     * @return Form
     */
    public function getForm($id)
    {
        $instructions = json_decode($this->getJson(), true);
        $form = new WarningForm();
        $factory = new Factory($this->getServiceLocator()->get('FormElementManager'));
        return $this->instructionsToForm($instructions, $factory, $form);
    }

    protected function instructionsToForm($instructions, Factory $factory, Fieldset $form = null)
    {
        if (isset($instructions['name'])) {
            $form->setName($instructions['name']);
        }
        if (isset($instructions['fields'])) {
            $fields = $instructions['fields'];
            foreach ($fields as $field) {
                if (!isset($field['type']) || !isset($field['name'])) {
                    continue;
                }
                $type = $field['type'];
                if ($type === 'collection') {
                    if (!isset($field['options']['fields'])) {
                        continue;
                    }
                    $target = new Fieldset($field['name']);
                    $target->setFormFactory($factory);
                    if (isset($field['options']['label'])) {
                        $target->setLabel($field['options']['label']);
                    }
                    $collection = new Collection();
                    $collection->setFormFactory($factory);
                    $collection->setName($field['name']);
                    $collection->setShouldCreateTemplate(true);
                    $collection->setTemplatePlaceholder('__' . $field['name'] . '__');
                    $collection->setTargetElement($this->instructionsToForm($field['options'], $factory, $target));
                    $collection->setOptions($field['options']);
                    $form->add($collection);
                    continue;
                }
                $form->add($field);
            }
        }
        return $form;
    }

    public function getJson()
    {
        return '{
  "name": "test",
  "fields": [
    {
      "name": "collectionID",
      "type": "text",
      "options": {
        "label": "Collection"
      }
    },
    {
      "name": "gathering",
      "type": "collection",
      "options" : {
        "label": "Gathering",
        "count": 1,
        "allow_add": true,
        "allow_remove": true,
        "fields": [
          {
            "name": "location",
            "type": "ArrayCollection",
            "options": {
              "count":"1",
              "should_create_template":true,
              "allow_add":true,
              "allow_remove":true,
              "target_element":{
                "name":"location",
                "type":"text"
               },
              "label": "Location"
            }
          },
          {
            "name": "country",
            "type": "select",
            "options": {
              "label": "Country",
              "value_options": {
                "fi": "Finland",
                "en": "England",
                "sv": "Sweden"
              }
            },
            "validators": {
              "name": "inHaystack",
              "options": {
                "haystack": ["fi", "en", "sv"]
              }
            }
          }
        ]
      }
    }
  ],
  "translations":{
    "fi": {
      "Collection": "Kokoelma",
      "Location": "Paikka",
      "Country": "Maa",
      "Finland": "Suomi",
      "England": "Englanti",
      "Sweden": "Ruotsi"
    },
    "sv": {
      "Collection": "ööö",
      "Location": "ööööö",
      "Country": "öö"
    }
  }
}';
    }

}