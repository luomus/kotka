<?php

namespace Kotka\Service;

use Kotka\Mapper\GXDatasetMapperInterface;
use Kotka\Triple\GXDataset;
use Kotka\Triple\MAPerson;

/**
 * Class DatasetService contains the business logic for datasets.
 * This should be the only place where datasets are handled!
 *
 * This service inherits few common methods from DocumentService
 *
 * @package Kotka\Service
 */
class DatasetService extends DocumentService implements DocumentServiceInterface
{
    /** @var \Kotka\Mapper\GXDatasetMapperInterface */
    protected $datasetMapper;

    /**
     * Initializes the object
     *
     * @param GXDatasetMapperInterface $datasetMapper
     * @param MAPerson $authenticationService
     * @param $config
     */
    public function __construct(GXDatasetMapperInterface $datasetMapper, MAPerson $authenticationService, $config)
    {
        $this->datasetMapper = $datasetMapper;
        parent::__construct($authenticationService, $config);
    }

    /**
     * Returns all the datasets in the database.
     *
     * @param $fields
     * @return array of Dataset objects
     */
    public function getAll(array $fields = null)
    {
        return $this->datasetMapper->findBy(array(), array('datasetName' => 'ASC'), null, null, $fields);
    }

    /**
     * Return one datasets object based on the uri
     * @param $id
     * @return GXDataset
     */
    public function getById($id)
    {
        return $this->datasetMapper->find($id);
    }

    /**
     * Stores new dataset object to the database
     * @param GXDataset $dataset
     * @return GXDataset
     * @throws Exception\DomainException
     */
    public function create($dataset)
    {
        if (!$dataset instanceof GXDataset) {
            throw new Exception\DomainException('Dataset service can\'t handle element of type ' . get_class($dataset));
        }
        $this->setId($dataset);
        $this->setEdit($dataset, true);

        return $this->datasetMapper->create($dataset);
    }

    /**
     * Updates existing object
     *
     * @param GXDataset $dataset
     * @return GXDataset
     */
    public function update($dataset)
    {
        $this->setEdit($dataset);

        return $this->datasetMapper->update($dataset);
    }

    /**
     * Returns new organization object
     * @return string class name
     */
    public function getClassName()
    {
        return $this->datasetMapper->getClassName();
    }
}
