<?php

namespace Kotka\Service;

use Common\Service\IdService;
use ImageApi\Service\ImageService;
use Kotka\Enum\Document as DocumentEnum;
use Kotka\Job\Delete;
use Kotka\Job\Elastic as ElasticJob;
use Kotka\Job\Specimen as SpecimenJob;
use Kotka\Mapper\MYDocumentMapperInterface;
use Kotka\Model\DataTypes;
use Kotka\Model\TaxonResult;
use Kotka\Triple\MAPerson;
use Kotka\Triple\MYDocument;
use Kotka\Triple\MYGathering;
use Kotka\Triple\MYIdentification;
use Kotka\Triple\MYUnit;
use SlmQueue\Queue\QueueInterface;
use Triplestore\Classes\MFPreparationInterface;
use Triplestore\Classes\MFSampleInterface;
use Triplestore\Classes\MYDocumentInterface;
use Triplestore\Classes\MYGatheringInterface;
use Triplestore\Classes\MYIdentificationInterface;
use Triplestore\Classes\MYMeasurementInterface;
use Triplestore\Classes\MYTypeSpecimenInterface;
use Triplestore\Classes\MYUnitInterface;
use Triplestore\Db\Oracle;
use Triplestore\Service\ObjectManager;
use Triplestore\Stdlib\OntologyInterface;
use Zend\EventManager\EventManager;
use Zend\Log\LoggerInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class SpecimenService contains the business logic for specimens.
 * This should be the only place where specimens are handled!
 *
 * This service inherits few common methods from DocumentService
 *
 * @package Kotka\Service
 */
class SpecimenService extends DocumentService implements DocumentServiceInterface
{
    protected $documentMapper;
    protected $specimenDatatypes;
    protected $queue;
    protected $coordinateService;
    protected $datatype;
    /** @var \Zend\ServiceManager\ServiceLocatorInterface */
    protected $serviceLocator;
    protected $isImport = false;
    /** @var \Kotka\Service\SequenceService */
    protected $seqService;

    private $logger;

    /**
     * @param boolean $isImport
     */
    public function setIsImport($isImport)
    {
        $this->isImport = (bool)$isImport;
    }

    /**
     * @param SequenceService $seqService
     * @param ServiceLocatorInterface $serviceLocator
     * @param MYDocumentMapperInterface $documentMapper
     * @param QueueInterface $queue
     * @param MAPerson $authenticationService
     * @param CoordinateService $coordinateService
     * @param $idGen
     */
    public function __construct(SequenceService $seqService, ServiceLocatorInterface $serviceLocator, MYDocumentMapperInterface $documentMapper, QueueInterface $queue, MAPerson $authenticationService, CoordinateService $coordinateService, $idGen)
    {
        parent::__construct($authenticationService, $idGen);
        $this->seqService = $seqService;
        $this->documentMapper = $documentMapper;
        $dataTypes = new DataTypes();
        $this->specimenDatatypes = $dataTypes->getSpecimenDatatypes();
        $this->queue = $queue;
        $this->coordinateService = $coordinateService;
        $this->serviceLocator = $serviceLocator;
    }

    /**
     * Stores new instance of specimen to the database
     *
     * @param $specimen
     * @return MYDocument|mixed
     * @throws Exception\DomainException
     */
    public function create($specimen)
    {
        if (!$specimen instanceof MYDocument) {
            throw new Exception\DomainException("Can't handle given object type");
        }
        if ($specimen->getMYDatatype() === '' || $specimen->getMYDatatype() === null) {
            throw new Exception\DomainException('Specimen must have a datatype, but none was given');
        }
        $this->setId($specimen);
        $this->setEdit($specimen, true);
        $this->prepareChildren($specimen, $specimen->getSubject());
        $result = $this->documentMapper->create($specimen);
        $this->createJobs($specimen->getSubject());

        return $result;
    }

    /**
     * updates existing specimen
     * @param $specimen
     * @param bool $skipEdit
     * @return MYDocument
     */
    public function update($specimen, $skipEdit = false)
    {
        if (!$specimen instanceof MYDocument) {
            throw new Exception\DomainException("Can't handle given object type");
        }
        if (!$skipEdit) {
            $this->setEdit($specimen);
        }
        $this->prepareChildren($specimen, $specimen->getSubject());
        $result = $this->_update($specimen);
        $this->createJobs($specimen->getSubject());

        return $result;
    }

    public function imageUploadResend($ids)
    {
        foreach($ids as $id) {
            $document = $this->getById($id);

            if (!$document instanceof MYDocument) {
              throw new Exception\DomainException("Can't handle given object type");
            }

            $this->createJobs($document->getSubject());
        }
    }

    public function delete($document)
    {
        if (!$document instanceof MYDocument) {
            throw new Exception\DomainException("Can't handle given object type");
        }
        if (!DocumentAccess::canDelete($document, $this->user)) {
            return false;
        }
        $document->setMZScheduledForDeletion(true);
        $this->setEdit($document, null);
        $this->_update($document);
        $event = new EventManager('delete');
        $event->trigger(get_class($document), null, array(
            'subject' => $document->getSubject(),
            'user' => $this->user->getSubject()
        ));
        $job = new Delete();
        $job->setSubject($document->getSubject());
        $this->queue->push($job, array('delay' => 3600));
        return true;
    }


    public function cancelDelete($document)
    {
        if (!$document instanceof MYDocument) {
            throw new Exception\DomainException("Can't handle given object type");
        }
        if (!DocumentAccess::canDelete($document, $this->user)) {
            return false;
        }
        // Remove deletion Job
        /** @var \Triplestore\Service\\ObjectManager $objectManager */
        $objectManager = $this->serviceLocator->get('Triplestore\ObjectManager');
        /** @var Oracle $connection */
        $connection = $objectManager->getConnection();
        // Remove scheduledForDeletion from the document
        $document->setMZScheduledForDeletion(false);
        $this->setEdit($document, null);
        $this->_update($document);
        $connection->commit();
        return true;
    }

    /**
     * @return LoggerInterface
     */
    private function getLogger()
    {
        if ($this->logger === null) {
            $this->logger = $this->serviceLocator->get('logger');
        }
        return $this->logger;
    }

    private function createJobs($subject)
    {
        try {
            $job = new SpecimenJob();
            $job->setSubject($subject);
            $this->queue->push($job, array('delay' => 2));
            if (!$this->isImport) {
                $job = new ElasticJob();
                $job->setContent(array('ids' => [IdService::getQName($subject)]));
                $this->queue->push($job, array('delay' => 3));
            }
        } catch(\Exception $e) {
            $this->getLogger()->crit('Adding ' . $subject . ' to background job failed!', [$e->getMessage()]);
        }
    }

    /**
     * Updates MYDocuments coordinates
     *
     * This goes through all the gatherings in the document and tries to convert and call the setGatheringWgsCoordinate to each
     *
     * @param MYDocument $specimen
     */
    public function setWgsCoordinate(MYDocument $specimen)
    {
        $gatherings = $specimen->getMYGathering();
        foreach ($gatherings as $gathering) {
            /** @var \Kotka\Triple\MYGathering $gathering */
            $this->setGatheringWgsCoordinate($gathering);
        }
    }

    /**
     * This checks that the wgs84 coordinate exists and if it dosn't I'll try to add them
     *
     * @param MYDocument $specimen
     */
    public function checkWgsCoordinateExists(MYDocument $specimen)
    {
        $gatherings = $specimen->getMYGathering();
        foreach ($gatherings as $gathering) {
            /** @var \Kotka\Triple\MYGathering $gathering */
            $lat = $gathering->getMYWgs84Latitude();
            $lon = $gathering->getMYWgs84Longitude();
            if (!empty($lat) && !empty($lon)) {
                $this->setGatheringWgsCoordinate($gathering);
            }
        }
    }

    /**
     * Updates Gathering coordinates
     *
     * This tries to convert the coordinate values to wgs84.
     * If the conversion is successful the the values are stored to MY.wgs84Latitude and MY.wgs84Longitude predicates.
     *
     * @param MYGathering $gathering
     * @return boolean true if something was changed
     */
    public function setGatheringWgsCoordinate(MYGathering $gathering)
    {
        $lat = $gathering->getMYLatitude();
        $lon = $gathering->getMYLongitude();
        $sys = $gathering->getMYCoordinateSystem();
        if (empty($lat) || empty($lon) || empty($sys)) {
            if ($gathering->getMYWgs84Latitude() !== null || $gathering->getMYWgs84Longitude() !== null) {
                $gathering->setMYWgs84Latitude(null);
                $gathering->setMYWgs84Longitude(null);
                return true;
            }
            return false;
        }
        $return = false;
        $wgs = $this->coordinateService->convertToWgs84($lat, $lon, $sys);
        if ($wgs !== null) {
            if (isset($wgs[0]) && $wgs[0] !== $gathering->getMYWgs84Latitude()) {
                $gathering->setMYWgs84Latitude($wgs[0]);
                $return = true;
            }
            if (isset($wgs[1]) && $wgs[1] !== $gathering->getMYWgs84Longitude()) {
                $gathering->setMYWgs84Longitude($wgs[1]);
                $return = true;
            }
        }
        return $return;
    }

    public function getSampleId($root) {
        $start = IdService::getQName($root, true) . "#P";
        return $start . $this->seqService->next($start);
    }

    public function generateUserId($seq) {
        $parts = explode(':', $seq);
        if (!isset($parts[1]) || !empty($parts[1]) || empty($parts[0])) {
            return $seq;
        }
        return $seq . $this->seqService->next($parts[0]);
    }

    /**
     * This creates the linkings beatween the document and it's child elements.
     *
     * @param $document
     * @param $root
     * @param $parent
     */
    private function prepareChildren($document, $root, $parent = null)
    {
        if ($parent === null) {
            $parent = $root;
        }
        if ($document instanceof OntologyInterface) {
            $subject = $document->getSubject();
            if ($subject === null || $subject === '') {
                if ($document instanceof MFSampleInterface) {
                    $document->setSubject($this->getSampleId($root));
                    $document->setMZDateCreated(new \DateTime());
                } else {
                    $this->setId($document, false);
                }
            }
        }

        $childMethods = null;
        if ($document instanceof MYDocumentInterface) {
            $childMethods = array('getMYGathering');
        } else if ($document instanceof MYGatheringInterface) {
            $childMethods = array('getMYUnit');
            $document->setMYIsPartOf($parent);
            $parent = $document->getSubject();
        } else if ($document instanceof MYUnitInterface) {
            $childMethods = array('getMYIdentification', 'getMYTypeSpecimen', 'getMFSample', 'getMYMeasurement');
            $document->setMYIsPartOf($parent);
            $parent = $document->getSubject();
        } else if ($document instanceof MFSampleInterface) {
            $childMethods = array('getMYMeasurement', 'getMFPreparation');
            $document->setMYIsPartOf($parent);
        } else if (
            $document instanceof MYIdentificationInterface ||
            $document instanceof MYTypeSpecimenInterface ||
            $document instanceof MYMeasurementInterface ||
            $document instanceof MFPreparationInterface
        ) {
            $document->setMYIsPartOf($parent);
            return;
        } else {
            return;
        }

        foreach ($childMethods as $childMethod) {
            $has = $document->$childMethod();
            if (is_array($has)) {
                foreach ($has as $doc) {
                    $this->prepareChildren($doc, $root, $parent);
                }
            } else {
                $this->prepareChildren($has, $root, $parent);
            }

        }
    }

    public function getAllIdentifications(MYDocument $specimen)
    {
        $result = [];
        if ($specimen === null) {
            return $result;
        }
        $gatherings = $specimen->getMYGathering();
        foreach ($gatherings as $gathering) {
            if (!$gathering instanceof MYGathering) {
                continue;
            }
            $units = $gathering->getMYUnit();
            foreach ($units as $unit) {
                if (!$unit instanceof MYUnit) {
                    continue;
                }
                $identifications = $unit->getMYIdentification();
                foreach ($identifications as $identification) {
                    if ($identification instanceof MYIdentification) {
                        $result[] = $identification;
                    }
                }

            }

        }
        return $result;
    }

    /**
     * Updates specimen without adding editor data
     *
     * @param MYDocument $specimen
     * @return MYDocument
     */
    public function _update(MYDocument $specimen)
    {
        return $this->documentMapper->update($specimen);
    }

    /**
     * Clears the cache for the specimens
     */
    public function clear()
    {
        $this->documentMapper->getObjectManager()->clear();
    }

    public function getTaxonData($scientificName, $checkList = 'MR.1')
    {
        /** @var ObjectManager $objectManager */
        $objectManager = $this->documentMapper->getObjectManager();
        /** @var Oracle $db */
        $db = $objectManager->getConnection();
        $db->setUseResultSet(true);
        $sql = "SELECT
                      STEP,
                      SCIENTIFICNAME,
                      RANK,
                      STEP_QNAME,
                      FINNISHNAME
                    FROM LTKM_LUONTO.RDF_TAXON_PARENTS_CHILDREN WHERE TAXON_QNAME IN (
                      SELECT TAXON_QNAME
                      FROM LTKM_LUONTO.RDF_TAXON_NAMES
                      WHERE
                        SCIENTIFICNAME = :scientificName
                        AND SOURCE = :checklist
                    ) AND STEP > -1 ORDER BY STEP";
        $result = $db->executeSelectSql($sql, ['scientificName' => $scientificName, 'checklist' => $checkList]);
        $db->setUseResultSet(false);

        return new TaxonResult($result);
    }

    public function findTypeSpecimensByCollection($collection)
    {
        /** @var ObjectManager $objectManager */
        $objectManager = $this->documentMapper->getObjectManager();
        $dbName = $objectManager->getConnection()->getDatabase();
        $query = "SELECT
                      d.SUBJECTNAME AS qname,
                      s1.OBJECTNAME AS typeStatus,
                      s2.RESOURCELITERAL AS typeSpecies,
                      s3.RESOURCELITERAL AS typeAuthor,
                      s4.OBJECTNAME AS typeVerification,
                      s5.RESOURCELITERAL AS taxon,
                      s6.RESOURCELITERAL AS author,
                      s7.RESOURCELITERAL AS wgs84Latitude,
                      s8.RESOURCELITERAL AS wgs84Longitude
                    FROM $dbName.RDF_STATEMENTVIEW d
                      JOIN $dbName.RDF_STATEMENTVIEW g
                        ON g.PREDICATENAME = '" . DocumentEnum::PREDICATE_PARENT . "' AND g.OBJECTNAME = d.SUBJECTNAME
                      JOIN $dbName.RDF_STATEMENTVIEW u
                        ON u.PREDICATENAME = '" . DocumentEnum::PREDICATE_PARENT . "' AND u.OBJECTNAME = g.SUBJECTNAME
                      JOIN $dbName.RDF_STATEMENTVIEW t
                        ON t.PREDICATENAME = '" . DocumentEnum::PREDICATE_PARENT . "' AND t.OBJECTNAME = u.SUBJECTNAME
                      LEFT JOIN $dbName.RDF_STATEMENTVIEW i
                        ON i.PREDICATENAME = '" . DocumentEnum::PREDICATE_PARENT . "' AND i.OBJECTNAME = t.OBJECTNAME
                      JOIN $dbName.RDF_STATEMENTVIEW s1
                        ON s1.PREDICATENAME = 'MY.typeStatus' AND s1.SUBJECTNAME = t.SUBJECTNAME
                      LEFT JOIN $dbName.RDF_STATEMENTVIEW s2
                        ON s2.PREDICATENAME = 'MY.typeSpecies' AND s2.SUBJECTNAME = t.SUBJECTNAME
                      LEFT JOIN $dbName.RDF_STATEMENTVIEW s3
                        ON s3.PREDICATENAME = 'MY.typeAuthor' AND s3.SUBJECTNAME = t.SUBJECTNAME
                      LEFT JOIN $dbName.RDF_STATEMENTVIEW s4
                        ON s4.PREDICATENAME = 'MY.typeVerification' AND s4.SUBJECTNAME = t.SUBJECTNAME
                      LEFT JOIN $dbName.RDF_STATEMENTVIEW s5
                        ON s5.PREDICATENAME = 'MY.taxon' AND s5.SUBJECTNAME = i.SUBJECTNAME
                      LEFT JOIN $dbName.RDF_STATEMENTVIEW s6
                        ON s6.PREDICATENAME = 'MY.author' AND s6.SUBJECTNAME = i.SUBJECTNAME
                      LEFT JOIN $dbName.RDF_STATEMENTVIEW s7
                        ON s7.PREDICATENAME = 'MY.wgs84Latitude' AND s7.SUBJECTNAME = g.SUBJECTNAME
                      LEFT JOIN $dbName.RDF_STATEMENTVIEW s8
                        ON s8.PREDICATENAME = 'MY.wgs84Longitude' AND s8.SUBJECTNAME = g.SUBJECTNAME
                    WHERE s1.OBJECTNAME != 'MY.typeStatusNo'
                          AND d.PREDICATENAME = '" . DocumentEnum::PREDICATE_COLLECTION_ID . "'
                          AND d.OBJECTNAME = :collection
                    ORDER BY i.STATEMENTID DESC";
        /** @var Oracle $db */
        $db = $objectManager->getConnection();
        $data = $db->executeSelectSql(
            $query,
            ['collection' => $collection]
        );
        $typesSorter = [];
        $types = [];
        foreach ($data as $key => $row) {
            $key = $row['QNAME'] . '_' . $row['TYPESPECIES'] . '_' . $row['TYPESPECIES'];
            if (!isset($types[$key]) || !isset($types[$key]['TAXON'])) {
                $typesSorter[$key] = $row['TYPESPECIES'];
                $types[$key] = $row;
            }
        }
        array_multisort($typesSorter, SORT_ASC, SORT_LOCALE_STRING, $types);
        return $types;
    }

    /**
     * Returns specimen with the given uri
     *
     * @param $id
     * @return MYDocument|mixed|null
     */
    public function getById($id)
    {
        if (is_array($id)) {
            $specimen = $this->findByIds($id);
        } else {
            $specimen = $this->documentMapper->find($id);
            if ($specimen === null && strpos($id, ':') === false) {
                $possible = [
                    IdService::QNAME_PREFIX_HERBO . $id,
                    IdService::QNAME_PREFIX_LUOMUS . $id,
                    IdService::QNAME_PREFIX_UTU . $id,
                    IdService::QNAME_PREFIX_ZMUO . $id,
                    IdService::QNAME_PREFIX_TUN . $id
                ];
                foreach ($possible as $try) {
                    $specimen = $this->documentMapper->find($try);
                    if ($specimen !== null) {
                        break;
                    }
                }
            }
        }

        $hasType = $this->hasDatatype();
        if (is_array($specimen)) {
            $result = array();
            foreach ($specimen as $subject => $document) {
                if (!$document instanceof MYDocument) {
                    continue;
                }
                if (($hasType && $document->getMYDatatype() !== $this->datatype) ||
                    (!$hasType && !in_array($document->getMYDatatype(), $this->specimenDatatypes))
                ) {
                    continue;
                }
                $result[$subject] = $document;
            }
            return $result;
        }
        if (!$specimen instanceof MYDocument) {
            return null;
        }

        if (($hasType && $specimen->getMYDatatype() !== $this->datatype) ||
            (!$hasType && !in_array($specimen->getMYDatatype(), $this->specimenDatatypes))
        ) {
            return null;
        }

        return $specimen;
    }

    private function findByIds($ids, $results = []) {
        if (count($ids) > 1000) {
            $set = array_splice($ids, 0, 1000);
            $specimen = $this->documentMapper->find($set);
        } else {
            $specimen = $this->documentMapper->find($ids);
            $ids = [];
        }
        if (is_array($specimen)) {
            $results = array_merge($results, $specimen);
        }
        if (count($ids)) {
            $results = $this->findByIds($ids, $results);
        }
        return $results;
    }

    public static function isPublic(MYDocument $document = null)
    {
        $public = true;
        $restrictions = $document->getMZPublicityRestrictions();
        if ($restrictions !== null && $restrictions !== 'MZ.publicityRestrictionsPublic') {
            $public = false;
        }
        return $public;
    }

    /**
     * Checks whether or not datatype has been set
     *
     * @return bool true if datatype is set
     */
    public function hasDatatype()
    {
        return $this->datatype !== null;
    }

    /**
     * Return all specimens
     *
     * Before calling this you should have called setCurrentType or setDatatype
     *
     * @param array $fields
     * @return array
     */
    public function getAll(array $fields = null)
    {
        return $this->documentMapper->findBy(array(), null, null, null, $fields);
    }

    /**
     * Return all the templates
     *
     * @return array
     */
    public function getAllTemplates()
    {
        return $this->documentMapper->findBy(array('recordType' => 'Template'));
    }

    /**
     * Return all specimen that match the given search criteria
     *
     * @param $searchTerms
     * @return array
     */
    public function getAllBySearch($searchTerms)
    {
        return $this->documentMapper->getAllBySearch($searchTerms);
    }

    /**
     * Returns new object.
     *
     * Before calling this you should have called setCurrentType or setDatatype
     *
     * @return mixed
     * @throws Exception\DomainException
     */
    public function getClassName()
    {
        return $this->documentMapper->getClassName();
    }

    /**
     * Gets all the specimens that have not been yet send to mustikka
     *
     * @return array
     */
    public function getAllNotInMustikka()
    {
        return $this->documentMapper->findBy(array(DocumentEnum::PREDICATE_IN_MUSTIKKA => 'false'));
    }

    /**
     * Sets the current datatype.
     * This method adds specimen to the datatype
     * if none where found in the string
     *
     * @param $datatype
     */
    public function setCurrentType($datatype)
    {
        if ($datatype == '') {
            return;
        }
        $this->setDatatype($datatype);
    }

    /**
     * Sets the current datatype
     *
     * @param $datatype
     *
     * @throws Exception\DomainException
     */
    public function setDatatype($datatype)
    {
        $type = $this->resolveDatatype($datatype);
        if ($type === null) {
            throw new Exception\DomainException('Unknown datatype: ' . $datatype);
        }
        $this->datatype = $type;
    }

    public function resolveDatatype($type)
    {
        if ($this->existsDataType($type)) {
            return $type;
        }
        if ($this->existsDataType($type . 'specimen')) {
            return $type . 'specimen';
        }
        return null;
    }

    public function setDefaultQNamePrefix()
    {
        $prefix = $this->user->getMADefaultQNamePrefix();
        $prefix = empty($prefix) ? IdService::QNAME_PREFIX_LUOMUS : $prefix;
        IdService::setDefaultQNamePrefix($prefix);
    }

    private function existsDataType($type)
    {
        return in_array($type, $this->specimenDatatypes);
    }

    /**
     * @return ImageService
     */
    private function getImageService()
    {
        return $this->serviceLocator->get('ImageService');
    }
}
