<?php

namespace Kotka\Service;

use Common\Service\IdService;
use Datetime;
use Kotka\Enum\Document as DocumentEnum;
use Kotka\Form\Element\ArrayCollection;
use Kotka\Job\Elastic as ElasticJob;
use Kotka\Model\Util;
use Kotka\PrograssBar\DummyProgressBar;
use Kotka\Stdlib\ArrayUtils;
use Kotka\Transform\TransformInterface;
use Kotka\Triple\MYDocument;
use SlmQueue\Queue\AbstractQueue;
use Triplestore\Service\MetadataService;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\EventManager\EventManagerAwareTrait;
use Zend\Form\Element\Select;
use Zend\Form\Fieldset;
use Zend\Form\Form;
use Zend\Log\LoggerInterface;
use Zend\ProgressBar\ProgressBar;
use Zend\ServiceManager\ServiceLocatorInterface;

class ImportService implements EventManagerAwareInterface
{
    use EventManagerAwareTrait;

    const IMPORT_DIR = './data/imported_files/';

    const MAX_EXCEL_ROW_SIZE = 5000;
    const MAX_HUGE_ROW_SIZE = 10000;
    const BATCH_SIZE = 100;

    const OPTION_SKIP_EXISTING = 1;
    const OPTION_UPDATE_EXISTING = 2;
    const OPTION_NEW_ID = 7;
    const OPTION_DRY_RUN = 3;
    const OPTION_VERBOSE = 4;
    const OPTION_HALT_ON_ERROR = 5;
    const OPTION_USER_FROM_DATA = 6;

    const IMPORT_STATUS_KEY = '__import-status__';
    const STATUS_CREATED = 'created';
    const STATUS_UPDATED = 'updated';
    const STATUS_ERRORED = 'has_errors';

    const ERROR_IN_VALID_FORMAT = -1;
    const ERROR_TOO_MANY_ROWS = -2;
    const ERROR_READING_FILE = -3;
    const ERROR_MISSING_HEADER = -4;
    const ERROR_AMBIGUOUS_HEADER = -5;
    const ERROR_FIELD_NOT_ON_FORM = -6;
    const ERROR_ON_FORM = -7;
    const ERROR_SAVING_DATA = -8;

    const TYPE_KEY = 'Type';
    const TYPE_UPDATE = 'update';
    const TYPE_INSERT = 'insert';

    const MODEL_KEY = '_model_';
    const RESET_KEY = '_reset_';

    protected $serviceLocator;
    protected $convertedService;
    protected $validationService;
    protected $idGenerator;
    /** @var \Kotka\Service\SpecimenService */
    protected $specimenService;
    /** @var  \Kotka\Service\ImportSenderInterface */
    protected $sender;
    protected $defaultOrganization;

    protected $options = [];
    protected $allowErrorsIn = [];
    protected $services = [];
    protected $labels = [];

    protected $errorCode = 0;
    protected $errorCause = '';
    protected $documentHydrator;

    protected $removeLangEnding = true;

    /** @var TransformInterface */
    protected $transform;

    private $allowedFormats = array(
        \PHPExcel_Style_NumberFormat::FORMAT_GENERAL => 1,
        \PHPExcel_Style_NumberFormat::FORMAT_TEXT => 1,
    );

    /** @var  ProgressBar */
    private $progressBar;
    private $logger;
    private $maxRows = self::MAX_EXCEL_ROW_SIZE;
    private $idCheck = [];
    private $overrideEditor = false;

    private $oldSupportedKeys = [
        '[MYDeathReason]' => '[MYCauseOfDeath]',
        '[MYAssociatedTaxa]' => '[MYAssociatedObservationTaxa]',
        '[MYType]' => '[MYTypeSpecimen]',
        '[MYMeasurement][ankleMillimeters]' => '[MYMeasurement][MYTarsusLengthMillimeters]',
        '[MYMeasurement][beakMillimeters]' => '[MYMeasurement][MYBeakMillimeters]',
        '[MYMeasurement][gonadMillimeters]' => '[MYMeasurement][MYGonadMillimeters]',
        '[MYMeasurement][totalLengthMillimeters]' => '[MYMeasurement][MYTotalLengthMillimeters]',
        '[MYMeasurement][totalLengthCentimeters]' => '[MYMeasurement][MYTotalLengthCentimeters]',
        '[MYMeasurement][bodyMillimeters]' => '[MYMeasurement][MYBodyMillimeters]',
        '[MYMeasurement][bodyCentimeters]' => '[MYMeasurement][MYBodyCentimeters]',
        '[MYMeasurement][tailMillimeters]' => '[MYMeasurement][MYTailMillimeters]',
        '[MYMeasurement][tailCentimeters]' => '[MYMeasurement][MYTailCentimeters]',
        '[MYMeasurement][weightGrams]' => '[MYMeasurement][MYWeightGrams]',
        '[MYMeasurement][weightKilograms]' => '[MYMeasurement][MYWeightKilograms]',
        '[MYMeasurement][wingMillimeters]' => '[MYMeasurement][MYWingMillimeters]',
        '[MYMeasurement][earLengthMillimeters]' => '[MYMeasurement][MYEarLengthMillimeters]',
        '[MYMeasurement][legLengthMillimeters]' => '[MYMeasurement][MYFootLenghtMillimeters]',
        '[MYMeasurement][embryoCount]' => '[MYMeasurement][MYEmbryoCount]',
        '[MYMeasurement][scarCount]' => '[MYMeasurement][MYUterineScarCount]',
        '[MYMeasurement][follicleDiameterMillimeters]' => '[MYMeasurement][MYFollicleDiameterMillimeters]',
        '[MYMeasurement][wingMaxMillimeters]' => '[MYMeasurement][MYWingMaxMillimeters]',
        '[MYMeasurement][wingMinMillimeters]' => '[MYMeasurement][MYWingMinMillimeters]',
    ];

    private $autofillColumns = [
        'MYGathering[*][MYUnit][*][MFSample][*][subject]' => 'sample',
        'MYGathering[*][MYUnit][*][MFSample][*][MFAdditionalIDs]' => 'prefix',
        'MYGathering[*][MYUnit][*][MFSample][*][MFAdditionalIDs][*]' => 'prefix'
    ];

    public function __construct(ServiceLocatorInterface $sl, ValidationService $validator)
    {
        $this->serviceLocator = $sl;
        $this->validationService = $validator;
        $this->idGenerator = new IdService();
        $this->documentHydrator = new \Triplestore\Classes\Hydrator\MYDocument();
    }

    public function setRemoveLangEnding($remove) {
        $this->removeLangEnding = (bool) $remove;
    }

    /**
     * Handles importing of json files
     * @param $json
     * @return array
     * @throws Exception\RuntimeException
     */
    public function importJson($json)
    {
        $data = json_decode($json, true);
        if (json_last_error() != JSON_ERROR_NONE) {
            throw new Exception\RuntimeException("File used doesn't contain well formatted json");
        }
        if (isset($data['datatype'])) {
            $data = array($data);
        }
        return '';// $this->importArray($data);
    }

    private function getExcel($file)
    {
        try {
            $fileType = \PHPExcel_IOFactory::identify($file);
            $reader = \PHPExcel_IOFactory::createReader($fileType);
            if ($reader instanceof \PHPExcel_Reader_CSV) {
                $reader->setDelimiter("\t");
            }
            $excel = $reader->load($file);
        } catch (\Exception $e) {
            $this->errorCode = self::ERROR_READING_FILE;
            return false;
        }
        return $excel;
    }

    public function getExcelSheets($file)
    {
        $excel = $this->getExcel($file);
        if ($excel === false) {
            return [];
        }
        return $excel->getSheetNames();
    }

    public function Excel2Array($file, $sheetNumber = 0, $skipSecondRow = null)
    {
        $excel = $this->getExcel($file);
        if ($excel === false) {
            return false;
        }
        /** @var \PHPExcel_Worksheet $sheet */
        $sheet = $excel->getSheet($sheetNumber);
        $data = array();
        $allFieldsWithData = array();

        $maxCol = $sheet->getHighestColumn();
        $maxRow = $sheet->getHighestRow();

        if ($maxRow > ($this->maxRows + 2)) {
            $this->errorCode = self::ERROR_TOO_MANY_ROWS;
            $this->errorCause = "More than " . $this->maxRows . " rows in the excel!";
            return false;
        }
        if ($skipSecondRow === null) {
            $skipSecondRow = $this->hasLabelRow($sheet);
        }

        $pRange = 'A1:' . $maxCol . $maxRow;

        //    Identify the range that we need to extract from the worksheet
        list($rangeStart, $rangeEnd) = \PHPExcel_Cell::rangeBoundaries($pRange);
        $minCol = \PHPExcel_Cell::stringFromColumnIndex($rangeStart[0] - 1);
        $minRow = $rangeStart[1];
        $maxCol = \PHPExcel_Cell::stringFromColumnIndex($rangeEnd[0] - 1);
        $maxRow = $rangeEnd[1];

        /** @var \PHPExcel_CachedObjectStorage_Memory $cellCache */
        $cellCache = $sheet->getCellCacheController();

        $maxCol++;
        $dataMax = $maxCol;
        $headers = array();
        // Loop through rows
        for ($row = $minRow; $row <= $maxRow; ++$row) {
            $c = -1;
            $query = '';
            $hasData = false;
            // Loop through columns in the current row
            for ($col = $minCol; $col != $maxCol; ++$col) {
                $cNum = ++$c;
                $field = '';
                if ($row > 1 && isset($headers[$cNum])) {
                    $field = $headers[$cNum];
                }
                //    Using getCell() will create a new cell if it doesn't already exist. We don't want that to happen
                //        so we test and retrieve directly against cache
                if ($cellCache->isDataSet($col . $row)) {
                    // Cell exists
                    $cell = $cellCache->getCacheData($col . $row);
                    if ($cell->getValue() !== null) {
                        $dataMax = $col;
                        if ($cell->getValue() instanceof \PHPExcel_RichText) {
                            $value = $cell->getValue()->getPlainText();
                        } else {
                            $value = $cell->getValue();
                            if (is_float($value)) {
                                $strValue = '' . $value;
                                $dotPos = strpos($strValue, '.');
                                $delta = strlen($strValue) - $dotPos;
                                if ($delta > 12) {
                                    $value = rtrim(number_format($value,12,'.',''),'0.');
                                }
                            }
                        }
                        $value = trim($value);
                        $style = $excel->getCellXfByIndex($cell->getXfIndex());
                        $styleCode = $style->getNumberFormat()->getFormatCode();
                        if (!isset($this->allowedFormats[$styleCode])) {
                            $this->errorCode = self::ERROR_IN_VALID_FORMAT;
                            $this->errorCause = $col . $row;

                            return false;
                        }
                        if ($row == 1) {
                            if (in_array($value, $headers)) {
                                $this->errorCode = self::ERROR_AMBIGUOUS_HEADER;
                                $this->errorCause = $value;

                                return false;
                            }

                            if ($this->removeLangEnding) {
                                // These two are only temporally here. They are here so that old style import file would be supported.
                                if (Util::endsWith($value, 'En')) {
                                    $value = substr($value, 0, -2);
                                } else if (Util::endsWith($value, 'En]')) {
                                    $value = substr($value, 0, -3) . ']';
                                }
                            }

                            // Support old excel keys
                            $value = str_replace(
                                array_keys($this->oldSupportedKeys),
                                array_values($this->oldSupportedKeys),
                                $value
                            );

                            $headers[$cNum] = $value;
                        } elseif ($row == 2 && $skipSecondRow) {
                            if (isset($headers[$cNum])) {
                                $this->labels[$headers[$cNum]] = $value;
                            }
                        } else {
                            if (!empty($value)) {
                                $hasData = true;
                            }
                            if (!isset($headers[$cNum])) {
                                $this->errorCode = self::ERROR_MISSING_HEADER;
                                $this->errorCause = $col . $row;

                                return false;
                            }
                            $query .= $field . '=' . urlencode($value);
                            $allFieldsWithData[$field . '='] = true;
                            $query .= '&';
                        }
                    } else {
                        // Cell holds a NULL
                        $query .= $field . '=&';
                    }
                } else {
                    // Cell doesn't exist
                    $query .= $field . '=&';
                }
            }
            if ($row == 1) {
                // Reset the col max to last field with data
                $maxCol = $dataMax;
                $maxCol++;
            } elseif ($hasData) {
                parse_str(rtrim($query, '&'), $data[]);
            }
        }
        if (count($allFieldsWithData)) {
            $queryStr = str_replace(['.'], ['(dot)'], implode('&', array_keys($allFieldsWithData)));
            parse_str($queryStr, $data[self::RESET_KEY]);
        }
        $excel->disconnectWorksheets();
        unset($excel);
        return $data;
    }

    public function autofillExcel($file, $sheetNumber = 0, $skipSecondRow = null)
    {
        $excel = $this->getExcel($file);
        if ($excel === false) {
            return false;
        }
        /** @var \PHPExcel_Worksheet $sheet */
        $sheet = $excel->getSheet($sheetNumber);

        $maxCol = $sheet->getHighestColumn();
        $maxRow = $sheet->getHighestRow();

        if ($maxRow > ($this->maxRows + 2)) {
            $this->errorCode = self::ERROR_TOO_MANY_ROWS;
            $this->errorCause = "More than " . $this->maxRows . " rows in the excel!";
            return false;
        }
        if ($skipSecondRow === null) {
            $skipSecondRow = $this->hasLabelRow($sheet);
        }

        $pRange = 'A1:' . $maxCol . $maxRow;

        //    Identify the range that we need to extract from the worksheet
        list($rangeStart, $rangeEnd) = \PHPExcel_Cell::rangeBoundaries($pRange);
        $minCol = \PHPExcel_Cell::stringFromColumnIndex($rangeStart[0] - 1);
        $minRow = $rangeStart[1];
        $maxCol = \PHPExcel_Cell::stringFromColumnIndex($rangeEnd[0] - 1);
        $maxRow = $rangeEnd[1];

        /** @var \PHPExcel_CachedObjectStorage_Memory $cellCache */
        $cellCache = $sheet->getCellCacheController();

        $maxCol++;
        $dataMax = $maxCol;
        $headers = array();
        $nsCol = -1;
        $oidCol = -1;
        // Loop through rows
        for ($row = $minRow; $row <= $maxRow; ++$row) {
            $c = -1;
            // Loop through columns in the current row
            for ($col = $minCol; $col != $maxCol; ++$col) {
                $cNum = ++$c;
                $field = '';
                if ($row > 1 && isset($headers[$cNum])) {
                    $field = $headers[$cNum];
                    if (!isset($this->autofillColumns[$field])) {
                        continue;
                    }
                }
                if ($cellCache->isDataSet($col . $row) || $row > 1) {
                    // Cell exists
                    $cell = $sheet->getCell($col . $row);
                    if ($cell->getValue() instanceof \PHPExcel_RichText) {
                        $value = $cell->getValue()->getPlainText();
                    } else {
                        $value = $cell->getValue();
                        if (is_float($value)) {
                            $strValue = '' . $value;
                            $dotPos = strpos($strValue, '.');
                            $delta = strlen($strValue) - $dotPos;
                            if ($delta > 12) {
                                $value = rtrim(number_format($value,12,'.',''),'0.');
                            }
                        }
                    }
                    $value = trim($value);
                    if ($row == 1) {
                        $dataMax = $col;
                        // Support old excel keys
                        $value = str_replace(
                            array_keys($this->oldSupportedKeys),
                            array_values($this->oldSupportedKeys),
                            $value
                        );
                        $headers[$cNum] = preg_replace('/\[[0-9]+\]/', '[*]', $value);
                        if ($headers[$cNum] === 'MYNamespaceID') {
                            $nsCol = $col;
                        } else if ($headers[$cNum] === 'MYObjectID') {
                            $oidCol = $col;
                        }
                    } elseif ($row == 2 && $skipSecondRow) {

                    } else {
                        switch ($this->autofillColumns[$field]) {
                            case 'prefix':
                                $cell->setValue($this->getSpecimenService()->generateUserId($value));
                                break;
                            case 'sample':
                                if ($nsCol !== -1 && $oidCol !== -1) {
                                    $nsCell = $cellCache->getCacheData($nsCol . $row);
                                    $oidCell = $cellCache->getCacheData($oidCol . $row);
                                    if ($nsCell !== null && $oidCell !== null) {
                                        $sheet->getCell($col . $row)->setValue($this->getSpecimenService()->getSampleId(
                                            $nsCell->getValue() . '.' .
                                            $oidCell->getValue()
                                        ));
                                    }
                                }
                                break;
                            default:
                                throw new \Exception('Invalid autofill type given (' . $this->autofillColumns[$field] . ') for field ' . $field);
                        }
                    }
                }
            }
            if ($row == 1) {
                // Reset the col max to last field with data
                $maxCol = $dataMax;
                $maxCol++;
            }
        }
        // $excel->disconnectWorksheets();
        // unset($excel);
        return $excel;
    }


    public function getErrorCode()
    {
        return $this->errorCode;
    }

    public function getErrorCause()
    {
        return $this->errorCause;
    }

    /**
     * @return array
     */
    public function getLabels()
    {
        return $this->labels;
    }

    /**
     * Imports array of specimen
     *
     * @param array $array
     * @param $datatype
     * @param null $organization
     * @param bool $addModelToModelKey
     * @return bool
     * @throws \Exception
     */
    public function validateArray(array &$array, $datatype, $organization = null, $addModelToModelKey = true)
    {
        $this->overrideEditor = false;
        /** @var \Kotka\Service\SpecimenService $specimenService */
        $specimenService = $this->serviceLocator->get('Kotka\Service\SpecimenService');
        $specimenService->setDefaultQNamePrefix();
        $reset = $array[self::RESET_KEY];
        $reset = $this->getArrayWithNeededFields($reset);
        $form = $this->getSpecimenForm();
        if ($this->hasOption(self::OPTION_USER_FROM_DATA)) {
            if (isset($reset['MZCreator'])) {
                unset($reset['MZCreator']);
            }
            if (isset($reset['MZEditor'])) {
                unset($reset['MZEditor']);
            }
            if (isset($reset['MZDateCreated'])) {
                unset($reset['MZDateCreated']);
            }
        }
        $missing = $this->getMissingFormFields($form, $reset);
        if (!empty($missing)) {
            $this->errorCode = self::ERROR_FIELD_NOT_ON_FORM;
            $this->errorCause = implode(', ', $missing);

            return false;
        }
        $progressBar = $this->getProgressBar();
        $progressBar->update(0);
        $existing = $this->hasOption(self::OPTION_SKIP_EXISTING) ?
            'skip' :
            ($this->hasOption(self::OPTION_UPDATE_EXISTING) ?
                'update' : ''
            );
        $errorStr = [];
        $hasErrors = false;

        $formTemplate = $this->getSpecimenForm();
        $formTemplate->setType($datatype);
        /** @var \Kotka\InputFilter\MYDocument $inputFilter */
        $inputFilter = $formTemplate->getInputFilter();
        $inputFilter::clearCache();
        $warningFilter = $formTemplate->getInputWarningFilter();

        $importErrors = $this->importFilter($array);

        foreach ($array as $key => $rawData) {
            $progressBar->next();
            if ($key === self::RESET_KEY) {
                continue;
            }
            if (!isset($rawData['subject'])) {
                $rawData['subject'] = '';
            }
            $document = $specimenService->getById($rawData['subject']);
            if ($document === null) {
                $array[$key][self::TYPE_KEY] = self::TYPE_INSERT;
                $document = new MYDocument();
                $document->setSubject($rawData['subject']);
                $form->setObject($document);
            } else {
                //$existing = 'update';
                if ($existing === 'skip') {
                    continue;
                } elseif ($existing === 'update') {
                    $rawData['MZDateEdited'] = $document->getMZDateEdited();
                    if ($rawData['MZDateEdited'] instanceof \DateTime) {
                        $rawData['MZDateEdited'] = $rawData['MZDateEdited']->format(\Triplestore\Stdlib\Hydrator\DateTime::FORMAT);
                    }
                }

                $form->bind($document);
                /* Validationgroup disabled because it's not yet possible have empty field value (to remove) alone in it's own specimen level
                $form->setValidationGroup(
                    $this->getValidationGroup(
                        $formData,
                        $metadata
                    )
                );
                */
                $array[$key][self::TYPE_KEY] = self::TYPE_UPDATE;
            }
            $rawData['MZOwner'] = $organization;
            $rawData = $inputFilter->preFilter($rawData);
            $inputFilter->setData($rawData);

            $hasImportErrors = isset($importErrors[$key]);
            if (!$inputFilter->isValid() || $hasImportErrors) {
                $errors = $inputFilter->getMessages();
                if ($hasImportErrors) {
                    $errors['importErrors'] = 'Import error: ' . implode(', ', $importErrors[$key]);
                }
                $this->checkForAllowedErrors($errors);
                if (count($errors)) {
                    $hasErrors = true;
                    $array[$key]['errors'] = $errors;
                    if ($errors !== $array[$key]['errors']) {
                        $array[$key]['errors']['hasErrors'] = 'Data has error(s)!';
                    }
                    $this->errorCode = self::ERROR_ON_FORM;
                    $msgs = Util::extract_messages($errors);
                    if ($hasImportErrors) {
                        $msgs = array_merge($msgs, $importErrors[$key]);
                    }
                    foreach ($msgs as $place => $message) {
                        $errorSubject = isset($rawData['subject']) ? $rawData['subject'] : '';
                        $errorStr[] = "\n" . $errorSubject . ' => ' . $place . ': ' . $message;
                        //$this->getLogger()->err($formData['subject'] . ' => ' . $place . ': ' . $message);
                    }
                    if ($this->hasOption(self::OPTION_HALT_ON_ERROR)) {
                        $this->errorCause = implode(', ', $errorStr);

                        return false;
                    }
                    continue;
                }
            }

            $validData = $inputFilter->getValues();
            if ($addModelToModelKey) {
                $this->populateDocument($document, $validData, $datatype, $rawData);
                $array[$key][self::MODEL_KEY] = $document;
            }

            $warningFilter->setData($validData);
            if (!$warningFilter->isValid()) {
                $warnings = $warningFilter->getMessages();
                if (!ArrayUtils::is_array_empty($warnings)) {
                    $array[$key]['warnings'] = $warnings;
                }
            }
        }
        $this->errorCause = implode(', ', $errorStr);
        $this->idCheck = [];

        return !$hasErrors;
    }

    private function populateDocument(MYDocument $model, $formData, $datatype, $rawData)
    {
        $this->documentHydrator->hydrate($formData, $model);
        $model->setMYDatatype($datatype);
        if ($this->hasOption(self::OPTION_USER_FROM_DATA)) {
            $model->setMZEditor('');
            $model->setMZCreator('');
            if (isset($rawData['MZCreator']) && strpos($rawData['MZCreator'], 'MA.') === 0) {
                $model->setMZCreator($rawData['MZCreator']);
            }
            if (isset($rawData['MZEditor']) && strpos($rawData['MZEditor'], 'MA.') === 0) {
                $this->overrideEditor = true;
                $model->setMZEditor($rawData['MZEditor']);
            }
            if (isset($rawData['MZDateCreated'])) {
                $model->setMZDateCreated($rawData['MZDateCreated']);
            }
        }
    }

    public function importArray(array &$array)
    {
        /** @var \Kotka\Service\SpecimenService $specimenService */
        $specimenService = $this->serviceLocator->get('Kotka\Service\SpecimenService');
        $queue = $this->serviceLocator->get('SlmQueue\Queue\QueuePluginManager')->get('default');
        $specimenService->setIsImport(true);
        $subjectList = [];
        $failed = [];
        $bulkIndex = [];
        $eventManager = $this->getEventManager();
        foreach ($array as $key => $specimenArray) {
            $eventManager->trigger('import', $this, ['action' => 'next']);
            if ($key === self::RESET_KEY
                || !isset($specimenArray[self::TYPE_KEY])
                || !isset($specimenArray[self::MODEL_KEY])
                || !$specimenArray[self::MODEL_KEY] instanceof MYDocument
            ) {
                continue;
            }
            /** @var MYDocument $model */
            $model = $specimenArray[self::MODEL_KEY];
            $method = $specimenArray[self::TYPE_KEY] == self::TYPE_UPDATE ? 'update' : 'create';
            $subject = $model->getSubject();
            $specimenService->setCreator($model->getMZCreator());
            if ($this->overrideEditor) {
                $specimenService->setEditor($model->getMZEditor());
            } else {
                $specimenService->setEditor(null);
            }
            try {
                if ($specimenService->$method($model)) {
                    $array[$key]['subject'] = $subject;
                    $subjectList[] = $subject;
                    $bulkIndex[] = $subject;
                    $eventManager->trigger('import', $this, ['action' => 'success', 'subject' => $subject]);
                } else {
                    $eventManager->trigger('import', $this, ['action' => 'failed', 'subject' => $subject]);
                }
            } catch (\Exception $e) {
                $this->getLogger()->crit("Import  type: " . $specimenArray[self::TYPE_KEY] . " of '$subject' failed because of " . $e->getMessage());
                $eventManager->trigger('import', $this, ['action' => 'failed', 'subject' => $subject]);
            }
            if (count($bulkIndex) > ElasticJob::MAX_SIZE) {
                $this->addToElasticJob($queue, $bulkIndex);
                $bulkIndex = [];
            }
        }
        if (count($bulkIndex)) {
            $this->addToElasticJob($queue, $bulkIndex);
        }
        $eventManager->trigger('import', $this, ['action' => 'done']);

        if (count($failed)) {
            $this->errorCause = implode(', ', $failed);
            $this->errorCode = self::ERROR_SAVING_DATA;
            return false;
        }

        return true;
    }

    private function addToElasticJob(AbstractQueue $queue, $ids)
    {
        $job = new ElasticJob();
        $job->setContent(array('ids' => $ids));
        $queue->push($job, array('delay' => 3));
    }

    public function hasSender()
    {
        return $this->sender !== null;
    }

    public function setSender(ImportSenderInterface $sender)
    {
        $this->sender = $sender;
    }

    public function getSender()
    {
        return $this->sender;
    }

    public function setDefaultOrganization($organizationID)
    {
        $this->defaultOrganization = IdService::getUri($organizationID);
    }

    public function getDefaultOrganization()
    {
        return $this->defaultOrganization;
    }

    public function hasDefaultOrganization()
    {
        return $this->defaultOrganization !== null;
    }

    public function setMaxRows($rows)
    {
        $this->maxRows = $rows;
    }

    public function setProgressBar(ProgressBar $progressBar)
    {
        $this->progressBar = $progressBar;
    }

    public function getProgressBar()
    {
        if ($this->progressBar === null) {
            $this->progressBar = new DummyProgressBar();
        }
        return $this->progressBar;
    }

    /**
     * @return \Kotka\Form\MYDocument
     */
    private function getSpecimenForm()
    {
        /** @var \Kotka\Service\FormGeneratorService $formGenerator */
        $formGenerator = $this->serviceLocator->get('Kotka\Service\FormGeneratorService');
        $form = $formGenerator->getForm(new MYDocument());

        return $form;
    }

    private function importFilter(&$all)
    {
        $ref = [];
        $errors = [];
        $double = [];
        $newID = $this->hasOption(self::OPTION_NEW_ID);
        foreach ($all as $key => $data) {
            $error = [];
            $oid = isset($data['MYObjectID']) ? trim($data['MYObjectID']) : '';
            $ns = isset($data['MYNamespaceID']) ? trim($data['MYNamespaceID']) : '';
            if (empty($oid) || empty($ns)) {
                $error['Uri'] = 'You need to give both objectid and namespace when importing data';
            } else {
                $this->idGenerator->generateFromPieces($oid, $ns, DocumentEnum::TYPE);
                $subject = $this->idGenerator->getPQDN();
                if (isset($ref[$subject])) {
                    if ($newID) {
                        if (isset($double[$subject])) {
                            if ($double[$subject] > 20) {
                                throw new \Exception('Import service has created over 20 new id\'s for ' . $subject . ' and can\'t continue doing it!');
                            }
                            $subject = $subject . chr(64 + (++$double[$subject]));
                        } else {
                            $double[$subject] = 2;
                            $all[$ref[$subject]]['subject'] = $subject . chr(64 + 1);
                            $subject = $subject . chr(64 + 2);
                        }
                    } else {
                        $error['Uri'] = $subject . ' already used in this import!';
                    }
                }
                $all[$key]['subject'] = $subject;
                $ref[$subject] = $key;
                if ($this->transform !== null) {
                    $all[$key] = $this->transform->transform($all[$key]);
                }
            }
            if (count($error) > 0) {
                $errors[$key] = $error;
            }
        }
        return $errors;
    }

    private function mergeWithFilteredData(array $a, array $b)
    {
        foreach ($b as $key => $value) {
            if (null === $value) {
                continue;
            }
            if (is_array($value) && isset($a[$key])) {
                $a[$key] = $this->mergeWithFilteredData($a[$key], $value);
            } else {
                $a[$key] = $value;
            }
        }

        return $a;
    }

    private function getMissingFormFields($form, array $data, $missed = array(), $parent = '')
    {
        if ($form instanceof Form) {
            try {
                $form->populateValues($data);
            } catch (\Zend\Form\Exception\InvalidArgumentException $e) {
                $missed[] = '(Invalid repeating element)';
            }
        }
        $fields = [];
        $isNumeric = false;
        $isAlpha = false;
        foreach ($form as $key => $elem) {
            $fields[] = $key;
            if (is_numeric($key)) {
                $isNumeric = true;
            } else {
                $isAlpha = true;
            }
        }
        foreach ($data as $field => $value) {
            if (!$form instanceof Fieldset || !in_array($field, $fields) || ($isNumeric && $isAlpha && !is_numeric($field))) {
                $missed[] = empty($parent) ? $field : $parent . '[' . $field . ']';
                continue;
            }
            if (is_array($value)) {
                $elem = $form->get($field);
                if ($elem instanceof ArrayCollection || $elem instanceof Select) {
                    continue;
                } elseif ($elem instanceof Fieldset && !is_array($value)) {
                    $missed[] = empty($parent) ? $field : $parent . '[' . $field . ']';
                    continue;
                }
                $nextParent = empty($parent) ? $field : $parent . '[' . $field . ']';
                $missed = $this->getMissingFormFields($elem, $value, $missed, $nextParent);
            }
        }
        return $missed;
    }

    private function getValidationGroup($data, MetadataService $metadata)
    {
        $return = [];
        if (is_array($data)) {
            foreach ($data as $key => $value) {
                if (is_array($value) && (is_numeric($key) || $metadata->getMetadataFor($key) !== null)) {
                    if (!empty($value)) {
                        $return[$key] = $this->getValidationGroup($value, $metadata);
                    }
                } else {
                    $return[] = $key;
                }
            }
        }
        return array_filter($return);
    }

    private function getArrayWithNeededFields($array)
    {
        $array['subject'] = '';
        $array['MZOwner'] = '';
        $array['MYDatatype'] = '';
        $array['MYUnreliableFields'] = '';

        return $array;
    }

    public function setOptions(array $options = array())
    {
        $this->options = $options;

        return $this;
    }

    public function getOptions()
    {
        return $this->options;
    }

    public function hasOption($option)
    {
        return in_array($option, $this->options);
    }

    public function setAllowErrorsIn(array $allowErrorsIn = array())
    {
        $this->allowErrorsIn = $allowErrorsIn;

        return $this;
    }

    public function getAllowErrorsIn()
    {
        return $this->allowErrorsIn;
    }

    public function checkForAllowedErrors(&$errors)
    {
        foreach ($errors as $key => &$value) {
            if (in_array($key, $this->allowErrorsIn, true)) {
                unset($errors[$key]);
                continue;
            }
            if (is_array($value)) {
                $this->checkForAllowedErrors($value);
            }
        }
        $errors = array_filter($errors);
    }

    /**
     * @return null|TransformInterface
     */
    public function getTransform()
    {
        return $this->transform;
    }

    /**
     * @param TransformInterface $transform
     */
    public function setTransform(TransformInterface $transform)
    {
        $this->transform = $transform;
    }

    /**
     * @return LoggerInterface
     */
    protected function getLogger()
    {
        if ($this->logger === null) {
            $this->logger = $this->serviceLocator->get('logger');
        }
        return $this->logger;
    }

    /**
     * @return \Kotka\Service\SpecimenService
     */
    protected function getSpecimenService()
    {
        if ($this->specimenService === null) {
            $this->specimenService = $this->serviceLocator->get('Kotka\Service\SpecimenService');
        }
        return $this->specimenService;
    }

    private function hasLabelRow(\PHPExcel_Worksheet $sheet)
    {
        $ns = $sheet->getCell('A2');
        $id = $sheet->getCell('B2');

        return preg_match('/^[a-z\s]*$/i', $ns . $id);
    }
} 