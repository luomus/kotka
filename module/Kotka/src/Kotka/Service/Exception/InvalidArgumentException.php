<?php

namespace Kotka\Service\Exception;

class InvalidArgumentException extends \InvalidArgumentException
{
}