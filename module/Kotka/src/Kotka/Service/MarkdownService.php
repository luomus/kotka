<?php

namespace Kotka\Service;


use Michelf\Markdown;

class MarkdownService
{
    const ALLOWED_TAGS = '<span><div>';

    public function mdToHtml($md)
    {
        // For safety allow only some of the html tags
        $md = strip_tags($md, self::ALLOWED_TAGS);

        return Markdown::defaultTransform($md);
    }

}