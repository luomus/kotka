<?php

namespace Kotka\Service;

use DateTime;
use Elasticsearch\Client;
use Zend\Cache\Storage\StorageInterface;
use Zend\Log\LoggerInterface;

class ElasticStats
{

    const CACHE_PREFIX = 'elastic_stats_';
    const LOGS_SINCE = '2014-03-01';

    private $cache;
    private $client;
    private $log;
    private $indexFormat = 'Y.m';
    private $indexWrapper = 'logstash-%s';
    private $specimenCounts = [
        '2014-03' => 32940
    ];

    public function __construct($params, StorageInterface $cache, LoggerInterface $log)
    {
        if (isset($params['index_format'])) {
            $this->indexFormat = $params['index_format'];
            unset($params['index_format']);
        }
        if (isset($params['index_wrapper'])) {
            $this->indexWrapper = $params['index_wrapper'];
            unset($params['index_wrapper']);
        }
        $this->client = new Client($params);
        $this->cache = $cache;
        $this->log = $log;
    }

    public function findSpecimenAddsInMonth($year, $month, $onlyTotal = false)
    {
        $month = (int)$month;
        $year = (int)$year;
        if ($month < 1) {
            $month = 12;
            $year--;
        } elseif ($month > 12) {
            $month = 1;
            $year++;
        }

        $cacheKey = self::CACHE_PREFIX . $year . '-' . $month . '-' . $onlyTotal;
        if ($this->cache->hasItem($cacheKey)) {
            return $this->cache->getItem($cacheKey);
        }

        $days = cal_days_in_month(CAL_GREGORIAN, $month, $year);
        $date = DateTime::createFromFormat('Y-m-d', $year . '-' . $month . '-' . $days);
        $result = $this->findSpecimenAdds($days, $date, $onlyTotal);
        $this->cache->setItem($cacheKey, $result);

        return $result;
    }

    public function findUserSpecimenAddsThisMonth()
    {
        return $this->findSpecimenAdds(
            (date('j') - 1)
        );
    }

    public function findUserSpecimenAddsThisWeek()
    {
        return $this->findSpecimenAdds(
            date('N')
        );
    }

    public function findSpecimenAddsByMonth($back = self::LOGS_SINCE, $from = 'now')
    {
        $back = $this->getDateTime($back);
        $from = $this->getDateTime($from);

        $result = $this->findSpecimenAdds(($from->diff($back)->format('%a') + 1), $from, true);
        $return = [];
        $cumulative = 0;
        foreach ($result as $time => $count) {
            $date = new DateTime();
            $time = (int)substr($time, 0, -3);
            $date->setTimestamp($time);
            $key = $date->format('Y-m');
            if (isset($this->specimenCounts[$key])) {
                $cumulative = $this->specimenCounts[$key];
            }
            $cumulative += $count;
            $return[$date->format('Y-m')] = $cumulative;
        }
        return $return;

    }

    public function findSpecimenAdds($days, $from = 'now', $onlyTotals = false, $interval = '1M')
    {
        if ($onlyTotals) {
            $json = '{"facets":{"terms":{"date_histogram":{"field":"@timestamp","interval":"' . $interval . '"},"global":true,"facet_filter":%s,"_cache":true}}]}}}}}}}},"size":0}';
        } else {
            $json = '{"facets":{"terms":{"terms":{"field":"userqname.raw","size":10,"order":"count","exclude":[]},"facet_filter":%s,"_cache":true}}]}}}}}}}},"size":0}';
        }

        $json = sprintf($json, $this->getDocumentAddFacetFilter());

        return $this->extractCountResult(
            $this->search(
                $this->getElasticParams($json, $days, $from)
            )
        );
    }

    private function search($json)
    {
        try {
            $result = $this->client->search($json);
        } catch (\Exception $e) {
            $this->log->err('Failed to connect to the elasticSearch. Error message: ' . $e->getMessage());
            $result = [];
        }


        return $result;
    }

    private function getDocumentAddFacetFilter()
    {
        return '{"fquery":{"query":{"filtered":{"query":{"bool":{"should":[{"query_string":{"query":"*"}}]}},"filter":{"bool":{"must":[{"range":{"@timestamp":{"from":%s,"to":%s}}},{"fquery":{"query":{"query_string":{"query":"documenttype:(\"MYDocument\")"}},"_cache":true}},{"fquery":{"query":{"query_string":{"query":"action:(\"added\")"}}';
    }

    private function getDateTime($time = 'now')
    {
        return new DateTime($time);
    }

    private function getElasticParams($json, $days, $from)
    {
        if ($from === 'now') {
            $from = new \DateTime();
        } elseif (!$from instanceof \DateTime) {
            throw new \Exception('Unable to determinate starting from date');
        }
        $params = [];
        $params['index'] = $this->getIndexes($days, $from);
        $params['ignore'] = 404;
        $params['body'] = $this->addTimeLimit($json, $days, $from);
        return $params;
    }

    private function extractCountResult($result)
    {
        if (!is_array($result)) {
            return [];
        }
        $return = [];
        if (isset($result['facets']) &&
            isset($result['facets']['terms']) &&
            isset($result['facets']['terms']['terms'])
        ) {
            $terms = $result['facets']['terms']['terms'];
            foreach ($terms as $value) {
                if (!isset($value['term']) || !isset($value['count'])) {
                    continue;
                }
                $return[$value['term']] = $value['count'];
            }
        }
        if (isset($result['facets']) &&
            isset($result['facets']['terms']) &&
            isset($result['facets']['terms']['entries'])
        ) {
            $entries = $result['facets']['terms']['entries'];
            foreach ($entries as $value) {
                if (!isset($value['time']) || !isset($value['count'])) {
                    continue;
                }
                $return['' . $value['time']] = $value['count'];
            }
        }
        return $return;
    }

    /**
     * Returns json and adds timestamp to it
     *
     * @param $json
     * @param $back
     * @param \DateTime $from
     * @param bool $gotoBeginning of the day
     * @return string
     * @throws \Exception
     */
    private function addTimeLimit($json, $back, \DateTime $from, $gotoBeginning = true)
    {
        $from = clone $from;
        $back = (int)$back;
        if ($gotoBeginning) {
            $from->setTime(0, 0, 0);
            $from->modify('+1 day');
        }
        if ($back == 0) {
            $back = 1;
        }
        $start = $from->getTimestamp() . '000';
        $from->modify('-' . $back . ' day');
        $end = $from->getTimestamp() . '000';

        return sprintf($json, $end, $start);
    }

    /**
     * Returns string with indexes from given date to back with given amount of dates
     *
     * @param $back
     * @param \DateTime $from
     * @return string
     * @throws \Exception
     */
    private function getIndexes($back, \DateTime $from)
    {
        $from = clone $from;
        $back = (int)$back;
        $indexes = [];
        if ($back > 365) {
            $idx = '_all';
            if ($this->indexWrapper !== null) {
                $idx = sprintf($this->indexWrapper, '*');
            }
            return $idx;
        }
        for ($i = $back; $i >= 0;) {
            $index = $this->getIndexDateLine($from);
            if ($this->client->indices()->exists(['index' => $index])) {
                $indexes[] = $this->getIndexDateLine($from);
            }
            $from->modify('-1 month');
            $i -= cal_days_in_month(CAL_GREGORIAN, $from->format('m'), $from->format('Y'));
        }

        return implode(',', $indexes);
    }

    /**
     * Returns string
     *
     * @param \DateTime $date
     * @return string
     */
    private function getIndexDateLine(\DateTime $date)
    {
        $dateStr = $date->format($this->indexFormat);
        if ($this->indexWrapper !== null) {
            $dateStr = sprintf($this->indexWrapper, $dateStr);
        }
        return $dateStr;
    }

}