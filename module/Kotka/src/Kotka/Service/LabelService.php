<?php

namespace Kotka\Service;


use Common\Service\IdService;
use Kotka\Filter\SortIdentifications;
use Kotka\Model\IdentifierInterpreter;
use Kotka\Model\Label;
use Kotka\Model\Label\Helper;
use Kotka\Triple\MYDocument;
use Kotka\View\Helper\Label\Field;
use Triplestore\Classes\MYDocumentInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Zend\View\Model\ViewModel;
use Zend\View\Renderer\PhpRenderer;
use Zend\View\Resolver\TemplateMapResolver;

class LabelService implements ServiceLocatorAwareInterface
{

    use ServiceLocatorAwareTrait;

    private $noLabelTypes = array(
        'MY.recordBasisVideo' => true,
        'MY.recordBasisHumanObservation' => true,
        'MY.recordBasisIndirectObservation' => true,
        'MY.recordBasisLiterature' => true,
        'MY.recordBasisTemplate' => true,
    );

    /**
     * This creates the html from the given list of labels
     *
     * @param array $labels
     * @param array $params
     * @return string
     */
    public function getLabelHtml(array $labels, array $params)
    {
        if (!isset($params['format']) && empty($labels)) {
            return '';
        }
        $format = $params['format'];
        $labelView = new ViewModel();
        $labelView->setTerminal(true);
        $labelView->setTemplate('kotka/label/' . $format);

        $resolver = new TemplateMapResolver();
        $resolver->setMap($this->serviceLocator->get('ViewTemplateMapResolver'));

        /** @var PhpRenderer $renderer */
        $renderer = $this->getServiceLocator()->get('viewmanager')->getRenderer();
        $renderer->setResolver($resolver);

        $fieldHelper = new Field();
        $fields = (isset($params['labelFields'])) ? $params['labelFields'] : [];
        $fieldHelper->setFields($fields);

        $labelView->setVariable('fieldHelper', $fieldHelper);
        $labelView->setVariable('labelHelper', new Helper());
        $labelView->setVariable('labels', $labels);
        $labelView->setVariable('params', $params);
        $html = $renderer->render($labelView);

        return $html;
    }

    public function extractGenericLabelData(IdentifierInterpreter $ids, $params)
    {
        $rows = [];
        $rows[] = isset($params['row1']) ? $params['row1'] : '';
        $rows[] = isset($params['row2']) ? $params['row2'] : '';
        $rows[] = isset($params['row3']) ? $params['row3'] : '';
        $rows[] = isset($params['row4']) ? $params['row4'] : '';
        $rows[] = isset($params['row5']) ? $params['row5'] : '';
        $reverse = array_reverse($rows);
        $has = false;
        foreach($reverse as &$row) {
            if (!empty($row)) {
                $has = true;
            } elseif ($has) {
                $row = '&nbsp;';
            }
        }
        $rows = array_reverse($reverse);
        $labels = [];
        foreach ($ids->getRange() as $qname) {
            $label = new Label();
            $label->setQname($qname);
            $label->setRows($rows);
            $labels[] = $label;
        }
        return $labels;
    }

    public function extractLabelData($documents, $sampleMap = null)
    {
        $labelHelper = new Helper();
        $data = [];
        $taxa = [];
        /** @var \Kotka\Filter\UnreliableFields $unreliableFilter */
        $unreliableFilter = $this->getServiceLocator()->get('FilterManager')->get('Kotka\Filter\UnreliableFields');
        $unreliableFilter->setMark('?');
        foreach ($documents as $document) {
            if (!$document instanceof MYDocumentInterface) {
                continue;
            }
            if ($document->getMZScheduledForDeletion() === true) {
                continue;
            }
            $document = $unreliableFilter->filter($document);
            $labels = $this->getLabelFromDocument($document, $labelHelper, $taxa, $sampleMap);
            $data = array_merge($data, $labels);
        }
        /** @var TaxonService $taxonService */
        $taxonService = $this->getServiceLocator()->get('Kotka\Service\TaxonService');
        $taxonService->initTaxa(array_keys($taxa));

        return $data;
    }

    private function getLabelFromDocument(MYDocument $document, Helper $labelHelper, &$taxa, $sampleMap)
    {
        $labelBase = new Label();
        SortIdentifications::sortDocumentObject($document);
        $subjectParts = explode(':', $document->getSubject());
        $id = array_pop($subjectParts);
        $explodedId = explode('.', $id);
        $objectID = end($explodedId);
        $labelBase->setId($id);
        $labelBase->setObjectID($objectID);
        $labelBase->setQname($document->getSubject());
        $labelBase->setIpen($document->getMYIPEN());
        $labelBase->setDomain(str_replace($id, '', $labelBase->getUri()));
        $labelBase->setCollectionID($document->getMYCollectionID());
        $labelBase->setLegID($document->getMYLegID());
        $labelBase->setPublication($document->getMYPublication());
        $labelBase->setOriginalCatalogueNumber($document->getMYOriginalSpecimenID());
        $labelBase->setAdditionalIDs($document->getMYAdditionalIDs());
        $labelBase->setObservationID($document->getMYObservationID());
        $labelBase->setExsiccatum($document->getMYExsiccatum());
        $labelBase->setRelationship($document->getMYRelationship());
        $labelBase->setExsiccatum($document->getMYExsiccatum());
        $labelBase->setBold($document->getMYBold());
        $labelBase->setTranscriberNotes($document->getMYTranscriberNotes());
        $relations = $document->getMYRelationship();
        foreach($relations as $relation) {
            if (strpos($relation, 'host:') !== false) {
                $host = trim(str_replace('host:', '', $relation));
                $labelBase->setHost($host);
                break;
            }
        }

        $gatherings = $document->getMYGathering();
        $labels = [];
        foreach ($gatherings as $gathering) {
            /** @var \Kotka\Triple\MYGathering $gathering */
            $labelBase->setAssociatedTaxa($labelHelper->getAssociatedTaxa($gathering));
            $labelBase->setLeg($gathering->getMYLeg());
            $labelBase->setLegVerbatim($gathering->getMYLegVerbatim());
            $labelBase->setSystem($gathering->getMYCoordinateSystem());
            $labelBase->setLatitude($gathering->getMYLatitude());
            $labelBase->setLongitude($gathering->getMYLongitude());
            $labelBase->setWgs84Latitude($gathering->getMYWgs84Latitude());
            $labelBase->setWgs84Longitude($gathering->getMYWgs84Longitude());
            $labelBase->setCoordinateRadius($gathering->getMYCoordinateRadius());
            $labelBase->setAlt($gathering->getMYAlt());
            $labelBase->setDepth($gathering->getMYDepth());
            $labelBase->setHabitatClassification($gathering->getMYHabitatClassification());
            $labelBase->setHabitatDescription($gathering->getMYHabitatDescription());
            $labelBase->setDateBegin($gathering->getMYDateBegin());
            $labelBase->setDateEnd($gathering->getMYDateEnd());
            $labelBase->setDateVerbatim($gathering->getMYDateVerbatim());
            $labelBase->setSubstrate($gathering->getMYSubstrate());
            $labelBase->setBiologicalProvince($gathering->getMYBiologicalProvince());
            $labelBase->setAdministrativeProvince($gathering->getMYAdministrativeProvince());
            $labelBase->setMunicipality($gathering->getMYMunicipality());
            $labelBase->setCountry($gathering->getMYCountry());
            $labelBase->setLocality($gathering->getMYLocality());
            $labelBase->setLocalityDescription($gathering->getMYLocalityDescription());
            $labelBase->setLocalityVerbatim($gathering->getMYLocalityVerbatim());
            $labelBase->setHigherGeography($gathering->getMYHigherGeography());
            $units = $gathering->getMYUnit();
            foreach ($units as $unit) {
                /** @var \Kotka\Triple\MYUnit $unit */
                if (isset($this->noLabelTypes[$unit->getMYRecordBasis()])) {
                    continue;
                }
                if ($sampleMap !== null) {
                    $sampleList = isset($sampleMap[$document->getSubject()]) ? $sampleMap[$document->getSubject()] : [];
                    $samples = $unit->getMFSample();
                    foreach ($samples as $sample) {
                        /** @var \Kotka\Triple\MFSample $sample */
                        if (!in_array($sample->getSubject(), $sampleList)) {
                            continue;
                        }
                        $label = $this->addUnitInfo($labelBase, $unit, $taxa);

                        $explodedSample = explode(':', $sample->getSubject());
                        $id = array_pop($explodedSample);
                        $explodedId = explode('.', $id);
                        $objectID = end($explodedId);

                        $label->setId($id);
                        $label->setQname($sample->getSubject());
                        $label->setDomain(str_replace($id, '', $label->getUri()));
                        $label->setObjectID($objectID);
                        $label->setSampleAdditionalIDs($sample->getMFAdditionalIDs());

                        $labels[] = $label;
                    }
                } else {
                    $labels[] = $this->addUnitInfo($labelBase, $unit, $taxa);
                }
            }
        }

        return $labels;
    }

    private function addUnitInfo(Label $labelBase, \Kotka\Triple\MYUnit $unit, &$taxa) {
        $label = clone $labelBase;
        $identifications = $unit->getMYIdentification();
        $identification = null;
        foreach ($identifications as $value) { // Identifications are sorted in the order so we just need the first one!
            $identification = $value;
            break;
        }
        if ($identification === null) {
            $identification = new \Kotka\Triple\MYIdentification();
        }
        $taxon = $identification->getMYTaxon();
        $taxa[$taxon] = true;
        $label->setTaxon($identification->getMYTaxon());
        $label->setTaxonVerbatim($identification->getMYTaxonVerbatim());
        $label->setTaxonRank($identification->getMYTaxonRank());
        $label->setGenusQualifier($identification->getMYGenusQualifier());
        $label->setSpeciesQualifier($identification->getMYSpeciesQualifier());
        $label->setDet($identification->getMYDet());
        $label->setDetDate($identification->getMYDetDate());
        $label->setDetYear($this->extractYear($identification->getMYDetDate()));
        $label->setIdentificationNotes($identification->getMYIdentificationNotes());
        $label->setAuthor($identification->getMYAuthor());
        $label->setInfraRank($identification->getMYInfraRank());
        $label->setInfraEpithet($identification->getMYInfraEpithet());
        $label->setInfraAuthor($identification->getMYInfraAuthor());
        $label->setSec($identification->getMYSec());
        $label->setSex($unit->getMYSex());
        $label->setAge($unit->getMYAge());
        $label->setRing($unit->getMYRing());
        $label->setLifeStage($unit->getMYLifeStage());
        $label->setMeasurement($unit->getMYMeasurement());
        $label->setCount($unit->getMYCount());
        $label->setCauseOfDeath($unit->getMYCauseOfDeath());
        $label->setPopulationAbundance($unit->getMYPopulationAbundance());
        $label->setDecayStage($unit->getMYDecayStage());
        $label->setDBH($unit->getMYDBH());

        return $label;
    }

    private function extractYear($value) {
        if (!is_string($value)) {
            return null;
        }
        $matches = [];
        preg_match('/([0-9]{4})/', $value, $matches);

        return isset($matches[1]) ? $matches[1] : null;
    }
}