<?php

namespace Kotka\Service;


use Zend\Cache\Storage\StorageInterface;
use Zend\Http\Client;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

class MunicipalityService implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    protected $cache;
    protected $init = false;
    protected $municipalities = [];
    protected $names = [];

    public function getCurrentMunicipality($municipality, $notFound = null)
    {
        $this->init();
        $id = $this->findPlaceId($municipality);
        if ($id === null) {
            return $notFound;
        }
        $parent = $this->findJoinedTo($id);

        return $this->getFinnishName($this->municipalities[$parent]);
    }

    private function getFinnishName($doc)
    {
        if (isset($doc['name'])) {
            foreach ($doc['name'] as $name) {
                if ($name['lang'] === 'FI') {
                    return $name['content'];
                }
            }
        }
        return null;
    }

    private function findJoinedTo($id)
    {
        $current = $this->municipalities[$id];
        if (isset($current['joined-to']) && !empty($current['joined-to']) && $current['joined-to'] !== $id) {
            return $this->findJoinedTo($current['joined-to']);
        }
        return $id;
    }

    private function findPlaceId($place)
    {
        $upper = mb_strtoupper($place, 'utf-8');
        if (isset($this->names[$upper])) {
            return $this->names[$upper];
        }
        return null;
    }


    private function init()
    {
        if ($this->init) {
            return;
        }
        $cache = $this->getCache();
        $cacheKey = 'municipality-service';
        if (!$cache->hasItem($cacheKey)) {
            $response = $this->getHttpClient()->send();
            if (!$response->isOk()) {
                throw new \Exception("Error connecting to the municipality service");
            }
            $data = json_decode($response->getBody(), true);
            $content = ['municipalities' => [], 'names' => []];
            if (isset($data['municipalities']) && isset($data['municipalities']['municipality'])) {
                $municipalities = $data['municipalities']['municipality'];
                foreach ($municipalities as $municipality) {
                    if (!isset($municipality['id'])) {
                        throw new \Exception("Error parsing result from municipality api. (missing id)");
                    }
                    $content['municipalities'][$municipality['id']] = $this->getParsedData($municipality, $content['names']);
                }
            } else {
                throw new \Exception("Error parsing result from municipality api. (municipalities or municipality is missing)");
            }
            $cache->setItem($cacheKey, $content);
        }
        $data = $cache->getItem($cacheKey);
        $this->municipalities = $data['municipalities'];
        $this->names = $data['names'];
        $this->init = true;
    }

    private function getParsedData($data, &$names)
    {
        $result = [];
        if (isset($data['joined-to'])) {
            $result['joined-to'] = $data['joined-to'];
        }
        if (isset($data['name'])) {
            $result['name'] = $data['name'];
            foreach ($data['name'] as $name) {
                $names[$name['content']] = $data['id'];
            }
            $result['name'] = $data['name'];
        }

        return $result;
    }

    /**
     * @return Client
     */
    private function getHttpClient()
    {
        /** @var \Kotka\Options\ExternalsOptions $config */
        $config = $this->getServiceLocator()->get('Kotka\Options\ExternalsOptions');
        $tipuApi = $config->getTipuApi();
        $client = $this->getServiceLocator()->get('Kotka\Http\TipuApiClient');
        $url = $tipuApi->getUrl() . '/municipalities?format=json';
        $client->setUri($url);
        return $client;
    }


    /**
     * @return StorageInterface
     */
    private function getCache()
    {
        if ($this->cache === null) {
            $this->cache = $this->getServiceLocator()->get('cache');
        }
        return $this->cache;
    }

}