<?php

namespace Kotka\Service;


use Common\Service\IdService;
use Zend\Paginator\Paginator;

class NavigationService
{

    private $pages = [];
    private $type;

    public function __construct($type)
    {
        $this->type = $type;
    }

    public function findNextDifferent(Paginator $paginator, $page, $spot, $url = null, $upward = false)
    {
        $candidate = $url;
        $total = $paginator->getTotalItemCount();
        $perPage = $paginator->getItemCountPerPage();
        while ($candidate == $url) {
            if ($upward) {
                $spot--;
                if ($spot >= 1) {
                    $candidate = $this->getCandidate($paginator, $page, $spot, $total);
                } else if ($page > 1) {
                    $page--;
                    $spot = $perPage + 1;
                } else {
                    return null;
                }
            } else {
                $spot++;
                if ($spot <= $perPage) {
                    $candidate = $this->getCandidate($paginator, $page, $spot, $total);
                } else if ($perPage * $page <= $total) {
                    $page++;
                    $spot = 0;
                } else {
                    return null;
                }
            }
        }
        if ($candidate !== null && $candidate !== $url) {
            $candidate = $this->getDataUri($candidate, $page, $spot);
        }
        return $candidate;
    }

    private function getCandidate($paginator, $page, $spot, $total)
    {
        try {
            $result = $this->getPage($paginator, $page);
        } catch (\Exception $e) {
            return null;
        }
        $spot--;
        if (!isset($result[$spot]) || ($page * $spot > $total)) {
            return null;
        }
        $data = $result[$spot];
        //$data = $paginator->getItem($spot, $page);
        return IdService::getUri($data['documentQName']);
    }

    private function getDataUri($uri, $page, $spot)
    {
        if (is_array($uri)) {
            $uri = current($uri);
        }
        return $uri . '&page=' . $page . '&spot=' . $spot . '&type=' . $this->type;
    }

    private function getPage(Paginator $paginator, $page)
    {
        if (!isset($this->pages[$page])) {
            $this->pages[$page] = $paginator->getItemsByPage($page)->toArray();
        }
        return $this->pages[$page];
    }
} 