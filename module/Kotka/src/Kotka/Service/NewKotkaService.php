<?php
namespace Kotka\Service;

use Kotka\Http\HttpClientTrait;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

class NewKotkaService implements ServiceLocatorAwareInterface
{
    use HttpClientTrait;
    use ServiceLocatorAwareTrait;

    function getDatasetSelect()
    {
      $client = $this->getHttpClient();
      $externals = $this->getServiceLocator()->get('Kotka\Options\ExternalsOptions');
      $newKotka = $externals->getNewKotka();
      $url = $newKotka->getUrl().'api/tagsSelect/';
      $response = $client->setUri($url)
          ->setHeaders(['Authorization' => $newKotka->getPass()])
          ->send();
      if (!$response->isOk()) {
          throw new \Exception('Communication New Kotka failed');
      }
      return json_decode($response->getBody(), true);
    }

    function getOrganizationSelect()
    {
      $client = $this->getHttpClient();
      $externals = $this->getServiceLocator()->get('Kotka\Options\ExternalsOptions');
      $newKotka = $externals->getNewKotka();
      $url = $newKotka->getUrl().'api/organizationsSelect/';
      $response = $client->setUri($url)
          ->setHeaders(['Authorization' => $newKotka->getPass()])
          ->send();
      if (!$response->isOk()) {
          throw new \Exception('Communication New Kotka failed');
      }
      return json_decode($response->getBody(), true);
    }

    function isLoaned($qname)
    {
        $client = $this->getHttpClient();
        $externals = $this->getServiceLocator()->get('Kotka\Options\ExternalsOptions');
        $newKotka = $externals->getNewKotka();
        $url = $newKotka->getUrl().'api/transaction/isLoaned/'.$qname;
        $response = $client->setUri($url)
            ->setHeaders(['Authorization' => $newKotka->getPass()])
            ->send();
        if (!$response->isOk()) {
            throw new \Exception('Communication New Kotka failed');
        }
        return $response->getBody();
    }

    function findTransactionsWithDocument($qname)
    {
        $client = $this->getHttpClient();
        $externals = $this->getServiceLocator()->get('Kotka\Options\ExternalsOptions');
        $newKotka = $externals->getNewKotka();
        $url = $newKotka->getUrl().'api/transaction/forSpecimen/'.$qname;
        $response = $client->setUri($url)
            ->setHeaders(['Authorization' => $newKotka->getPass()])
            ->send();
        if (!$response->isOk()) {
            throw new \Exception('Communication New Kotka failed');
        }
        return json_decode($response->getBody(), true);        
    }
}