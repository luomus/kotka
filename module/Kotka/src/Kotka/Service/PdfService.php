<?php

namespace Kotka\Service;

use DateTime;
use Kotka\Form\Element\LocalDepartment;
use Kotka\Options\PdfConvertOptions;
use Kotka\Triple\HRATransaction;
use Kotka\Triple\MOSOrganization;
use TCPDF;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

/**
 * Class PdfService creates pdf's
 * @package Kotka\Service
 */
class PdfService implements ServiceLocatorAwareInterface
{

    use ServiceLocatorAwareTrait;

    const EXCHANGE_OUTGOING = 'HRA.transactionTypeExchangeOutgoing';
    const EXCHANGE_INCOMING = 'HRA.transactionTypeExchangeIncoming';
    const GIFT_OUTGOING = 'HRA.transactionTypeGiftOutgoing';
    const GIFT_INCOMING = 'HRA.transactionTypeGiftIncoming';

    protected $api;
    protected $pdf;
    protected $departments;
    /** @var \Kotka\Service\OrganizationService */
    protected $organizationService;
    public $awayCount;

    protected $landscapeFormats = [
        'invertebrate_box',
        'invertebrate_tube',
        'invertebrate_tube_qr',
        'invertebrate_jar',
    ];

    protected $paperSize = [
        'dna_v1' => 'Letter'
    ];

    /**
     * initializes the new object
     * @param TCPDF $pdf
     * @param FormElementService $fes
     * @param $api
     */
    public function __construct(TCPDF $pdf, FormElementService $fes, $api)
    {
        $this->api = $api;
        $this->pdf = $pdf;

        // These fetch field labels into an array, which can be accessed later
        $this->departments = $fes->getSelect('HRA.localDepartment');
        $this->departments[''] = ''; // empty index in case value has not been set by user

        $this->sentTypes = $fes->getSelect('HRA.sentType');
        $this->sentTypes[''] = '';
    }

    public function htmlToPdfApi($html, $format = null)
    {
        $landscape = false;
        if (in_array($format, $this->landscapeFormats)) {
            $landscape = true;
        }

        $options = array(
            'http' => array(
                'method' => 'POST',
                'header' => 'Content-Type: text/html; charset=utf-8\r\n',
                'content' => $html
            )
        );
        $url = $this->api;
        $params = [];
        // $params[] = 'test=true';
        if ($landscape) {
            $params[] = 'orientation=Landscape';
        }
        if (isset($this->paperSize[$format])) {
            $params[] = 'paper-size=' . $this->paperSize[$format];
        }
        $url .= '?' . implode('&', $params);
        $context = stream_context_create($options);
        return file_get_contents($url, false, $context);
    }

    public function htmlToPdf($html, $margin = 10)
    {

        $this->pdf->setMargins($margin, $margin, $margin, TRUE);

        $this->pdf->AddPage();
        $this->pdf->SetFontSize(10);
        $this->pdf->writeHTML($html);

        return $this->pdf->getPDFData();
    }

    /**
     * Creates insect labels from given transaction
     * @param HRATransaction $transaction
     * @return string
     */
    public function getInsectLabelsPdf(HRATransaction $transaction)
    {
        $pageMargin = 10;

        $this->pdf->setMargins($pageMargin, $pageMargin, $pageMargin, TRUE);

        $this->pdf->AddPage();
        $this->pdf->SetFontSize(10);

// -------------------------------------------------------------------------------------------
// General info

        $transactionSent = $this->getDateString($transaction->getHRATransactionSent());

        $html = "
Labels for loan <strong>" . $transaction->getUri() . "</strong>, sent " . $transactionSent . "
";

        $this->pdf->writeHTML($html);

        $this->pdf->SetY(($this->pdf->GetY() + 10));
        $this->pdf->SetFontSize(5);

        $columnCount = 7;
        $columnWidth = 30;
        $this->pdf->setEqualColumns($columnCount, $columnWidth);
        $has = array_merge(
            (array)$transaction->getHRBAway(),
            (array)$transaction->getHRBReturned(),
            (array)$transaction->getHRBMissing(),
            (array)$transaction->getHRBDamaged()
        );
        $loanId = $transaction->getUri();
        foreach ($has as $item) {
            // Print lines
            $specimenID = $item;
            $this->pdf->Write(2, $specimenID, "", false, "L", true);
            $this->pdf->Write(2, "Loan " . $loanId, "", false, "L", true);
            $this->pdf->Write(2, $transactionSent, "", false, "L", true);

            // Cursor
            $this->pdf->SetY(($this->pdf->GetY() + 2));

            // If cursor is closing the page end, switch to next column or page
            if ($this->pdf->GetY() > 287) {
                // First column on next page
                if ($this->pdf->getColumn() == ($columnCount - 1)) {
                    $this->pdf->AddPage();
                    $this->pdf->selectColumn(0);
                } // Next column
                else {
                    $this->pdf->selectColumn(($this->pdf->getColumn() + 1));
                }
            }
//            }
        }

        return $this->pdf->getPDFData();

    }

    /**
     * Creates dispatch pdf for transactions
     * @param HRATransaction $transaction
     * @param MOSOrganization $organization
     * @return string
     */
    public function getDispatchPdf(HRATransaction $transaction, MOSOrganization $organization)
    {
        $pageMargin = 10;
        $leftMargin = 15;

        $this->pdf->setPageOrientation("P", true, $pageMargin);
        $this->pdf->setMargins($leftMargin, $pageMargin, $pageMargin, true);
        $this->pdf->SetFont("freeserif");

        $this->pdf->AddPage();

        $this->pdf->writeHTML($this->getHeader($transaction));

        $html = $this->getIntroText($transaction);

        $this->pdf->writeHTML($html);

        // -------------------------------------------------------------------------------------------
        // Loan info

        $this->pdf->SetY(($this->pdf->GetY() + 5));
        $lineStyle['width'] = 0.05;
        $this->pdf->Line($leftMargin, $this->pdf->GetY(), 200, $this->pdf->GetY(), $lineStyle);

        $this->pdf->SetY(($this->pdf->GetY() + 5));

        $this->pdf->writeHTML($this->getLoanInfoHtml($transaction, $organization));

        // -------------------------------------------------------------------------------------------
        // Remarks

        if ($transaction->getHRAPublicRemarks('en')) {
            $lineStyle['width'] = 0.05;
            $this->pdf->Line($leftMargin, $this->pdf->GetY(), 200, $this->pdf->GetY(), $lineStyle);

            $this->pdf->SetY(($this->pdf->GetY() + 5));
            $html = "
    <h3>Remarks</h3>
    " . str_replace("\n", "<p>", $transaction->getHRAPublicRemarks('en')) . "
    ";

            $this->pdf->writeHTML($html);
        }

        // -------------------------------------------------------------------------------------------
        // Signature
        $signatureSpotY = 250;
        if (($this->pdf->GetY()) > $signatureSpotY) {
            $this->pdf->AddPage();
        }
        $this->pdf->SetY($signatureSpotY);

        $lineStyle['width'] = 0.05;
        $this->pdf->Line($leftMargin, $this->pdf->GetY(), 200, $this->pdf->GetY(), $lineStyle);

        $this->pdf->SetY(($this->pdf->GetY() + 3));

        $html = $this->getSignature($transaction);

        $this->pdf->writeHTML($html);

        // -------------------------------------------------------------------------------------------
        // Material
        $this->pdf->AddPage();
        $this->pdf->SetY(($this->pdf->GetY() + 5));
        $lineStyle['width'] = 0.05;
        $this->pdf->Line($leftMargin, $this->pdf->GetY(), 200, $this->pdf->GetY(), $lineStyle);

        $specimens = $this->getSpecimenTableHtml($transaction);

        $this->pdf->SetY(($this->pdf->GetY() + 5));

        $additional = "";
        if ($transaction->getHRBAwayOther() > 0 && !in_array($transaction->getHRATransactionType(), [
                self::GIFT_OUTGOING,
                self::GIFT_INCOMING,
                self::EXCHANGE_OUTGOING,
                self::EXCHANGE_INCOMING
            ])) {
            $additional = "&nbsp;<br />In addition, loan includes " . $transaction->getHRBAwayOther() . " specimen(s) not specified in this list.";
        }

        $html = "
            <h3>Material</h3>
            <div>" . str_replace("\n", "<p>", $transaction->getHRAMaterial('en')) . "</div>
            <div style=\"font-size: 9px;\">
            $specimens
            $additional
            </div>
            <p><strong>Total no. of specimens:</strong> " . $this->awayCount . "<br />
            </p>";

        $this->pdf->writeHTML($html, true, false, true, false, '');

// -------------------------------------------------------------------------------------------
// Footer

//        $this->pdf->SetFontSize(5);
//        $this->pdf->SetY(($this->pdf->GetY() + 5));
//        $html = "Dispatch sheet printed from Kotka Collection Management System";
//        $this->pdf->writeHTML($html);

        return $this->pdf->getPDFData();

    }

    /**
     * Creates inquiry pdf for transaction
     * @param HRATransaction $transaction
     * @param MOSOrganization $organization
     * @return string
     */
    public function getInquiryPdf(HRATransaction $transaction, MOSOrganization $organization)
    {
        $pageMargin = 10;
        $leftMargin = 15;

        $this->pdf->setPageOrientation("P", true, $pageMargin);
        $this->pdf->setMargins($leftMargin, $pageMargin, $pageMargin, true);
        $this->pdf->SetFont("freeserif");

        $this->pdf->AddPage();

        $this->pdf->writeHTML($this->getHeader($transaction));

        $html = "
<h2>OVERDUE LOAN</h2>
<p>
Dear Colleague,<br />
Our records show that you have borrowed specimen(s) from us, with loan id <strong>" . $transaction->getUri() . "</strong> and due date of " . $this->getDateString($transaction->getHRADueDate()) . ".</p><p>
Please, inform us as soon as possible of the progress of your investigations by completing and returning this page";
        if ($transaction->getHRALocalPersonEmail() !== null && $transaction->getHRALocalPersonEmail() != '') {
            $html .= ", or by email to " . $transaction->getHRALocalPersonEmail();
        }
        $html .= ".</p>";

        $this->pdf->writeHTML($html);

        $this->pdf->SetY(($this->pdf->GetY() + 5));
        $html = "
&nbsp;&nbsp; [&nbsp;&nbsp;]&nbsp;&nbsp; I have completed my work and am returning the material immediately.<br /><br />
&nbsp;&nbsp; [&nbsp;&nbsp;]&nbsp;&nbsp; I wish an extension of this loan for approximately __________ months.<br /><br />
&nbsp;&nbsp; [&nbsp;&nbsp;]&nbsp;&nbsp; The material was returned on ____________________________, please recheck your files.<br /><br />
";

        $this->pdf->writeHTML($html);

        $html = "<p>Please also check your contact details below and correct them if needed.</p>";
        $this->pdf->writeHTML($html);

// -------------------------------------------------------------------------------------------
// Signature

        /*
        $this->pdf->SetY(($this->pdf->GetY() + 5));
        $lineStyle['width'] = 0.05;
        $this->pdf->Line(10, $this->pdf->GetY(), 200, $this->pdf->GetY(), $lineStyle);
        */

        $this->pdf->SetY(($this->pdf->GetY() + 20));
        $html = "
<table>
<tr>
<td>
Name
</td>
<td>
Signature
</td>
<td>
Place
</td>
<td>
Date
</td>
</tr>
</table>
";

        $this->pdf->writeHTML($html);

// -------------------------------------------------------------------------------------------
// Loan info

        $this->pdf->SetY(($this->pdf->GetY() + 1));
        $lineStyle['width'] = 0.05;
        $this->pdf->Line($leftMargin, $this->pdf->GetY(), 200, $this->pdf->GetY(), $lineStyle);

        $this->pdf->SetY(($this->pdf->GetY() + 5));

        $this->pdf->writeHTML($this->getLoanInfoHtml($transaction, $organization));

// -------------------------------------------------------------------------------------------
// Material

        $this->pdf->SetY(($this->pdf->GetY() + 5));
        $lineStyle['width'] = 0.05;
        $this->pdf->Line($leftMargin, $this->pdf->GetY(), 200, $this->pdf->GetY(), $lineStyle);

        $specimens = $this->getSpecimenTableHtml($transaction);

        $this->pdf->SetY(($this->pdf->GetY() + 5));


        $additional = "";
        if ($transaction->getHRBAwayOther() > 0) {
            $additional = "&nbsp;<br />In addition, loan includes " . $transaction->getHRBAwayOther() . " specimen(s) not specified in this list.";
        }

        $html = "
<h3>Loaned material</h3>
<div>" . str_replace("\n", "<p>", $transaction->getHRAMaterial('en')) . "</div>
<div style=\"font-size: 9px;\">
$specimens
$additional
</div>
<p><strong>Number of specimens still on loan</strong>: " . $this->awayCount . "<br /></p>
";

        $this->pdf->writeHTML($html);

// -------------------------------------------------------------------------------------------
// Remarks

        if ($transaction->getHRAPublicRemarks('en')) {
            $lineStyle['width'] = 0.05;
            $this->pdf->Line($leftMargin, $this->pdf->GetY(), 200, $this->pdf->GetY(), $lineStyle);

            $this->pdf->SetY(($this->pdf->GetY() + 5));
            $html = "
    <h3>Remarks</h3>
    " . str_replace("\n", "<p>", $transaction->getHRAPublicRemarks('en')) . "
    ";

            $this->pdf->writeHTML($html);
        }

// -------------------------------------------------------------------------------------------
// Footer

//        $this->pdf->SetFontSize(5);
//        $this->pdf->SetY(($this->pdf->GetY() + 5));
//        $html = "Inquiry sheet printed from Kotka Collection Management System";
//        $this->pdf->writeHTML($html);

        return $this->pdf->getPDFData();
    }


    /**
     * Creates return pdf for transactions
     * @param HRATransaction $transaction
     * @param MOSOrganization $organization
     * @param MOSOrganization $owner
     * @return string
     */
    public function getReturnPdf(HRATransaction $transaction, MOSOrganization $organization, MOSOrganization $owner)
    {
        $pageMargin = 10;
        $leftMargin = 15;

        $this->pdf->setPageOrientation("P", true, $pageMargin);
        $this->pdf->setMargins($leftMargin, $pageMargin, $pageMargin, true);
        $this->pdf->SetFont("freeserif");

        $this->pdf->AddPage();

        $this->pdf->writeHTML($this->getHeader($transaction));

        // ABBA
        $html = "
            <h2>&nbsp;<br />LOAN RETURN</h2>
            <p>We are returning the material indicated here belonging to the loan <strong>" . $transaction->getHRAExternalTransactionID() . "</strong>.
            After receiving the specimens, please sign and return one copy of this form to the above address" . (empty($transaction->getHRALocalPersonEmail()) ? '.' : ' or via email to ' . $transaction->getHRALocalPersonEmail() . '.') . "
            Thank you.
            </p>
            ";

        $this->pdf->writeHTML($html);

        // -------------------------------------------------------------------------------------------
        // Loan info

        $this->pdf->SetY(($this->pdf->GetY() + 5));
        $lineStyle['width'] = 0.05;
        $this->pdf->Line($leftMargin, $this->pdf->GetY(), 200, $this->pdf->GetY(), $lineStyle);

//        $this->pdf->SetY(($this->pdf->GetY() + 5));

        $this->pdf->writeHTML($this->getReturnInfoHtml($transaction, $organization, $owner));
        $html = '';
        // -------------------------------------------------------------------------------------------
        // Remarks
        if ($transaction->getHRAPublicRemarks('en')) {
            $lineStyle['width'] = 0.05;
            $this->pdf->Line($leftMargin, $this->pdf->GetY(), 200, $this->pdf->GetY(), $lineStyle);

            $this->pdf->SetY(($this->pdf->GetY() + 5));
            $html .= "
                <h3>Remarks</h3>
                <div>" . str_replace("\n", "<p>", $transaction->getHRAPublicRemarks('en')) . "</div>
            ";
        }

        // return text
        $awayCount = count($transaction->getHRBAway()) + $transaction->getHRBAwayOther();
        if ($awayCount == 0) {
            $html .= "
                    <br />
                    Complete return
                ";
        } else {
            $html .= "
                    <br />
                    Partial return<br />
                    $awayCount specimen(s) are still on loan.
                ";
        }
        $this->pdf->writeHTML($html);

        // -------------------------------------------------------------------------------------------
        // Signature
        $signatureSpotY = 250;
        if (($this->pdf->GetY()) > $signatureSpotY) {
            $this->pdf->AddPage();
        }
        $this->pdf->SetY($signatureSpotY);

        $lineStyle['width'] = 0.05;
        $this->pdf->Line($leftMargin, $this->pdf->GetY(), 200, $this->pdf->GetY(), $lineStyle);

        $this->pdf->SetY(($this->pdf->GetY() + 3));

        $html = $this->getSignature($transaction);

        $this->pdf->writeHTML($html);

        // -------------------------------------------------------------------------------------------
        // Material
        $this->pdf->AddPage();
        $this->pdf->SetY(($this->pdf->GetY() + 5));
        $lineStyle['width'] = 0.05;
        $this->pdf->Line($leftMargin, $this->pdf->GetY(), 200, $this->pdf->GetY(), $lineStyle);

        $specimens = $this->getSpecimenTableHtml($transaction);

        $this->pdf->SetY(($this->pdf->GetY() + 5));
        $html = "
            <h3>Material</h3>
            <div>" . str_replace("\n", "<p>", $transaction->getHRAMaterial('en')) . "<br /></div>
            ";
        $html .= $this->getParcelsLine($transaction);

        $this->pdf->writeHTML($html, true, false, true, false, '');

// -------------------------------------------------------------------------------------------
// Footer

//        $this->pdf->SetFontSize(5);
//        $this->pdf->SetY(($this->pdf->GetY() + 5));
//        $html = "Dispatch sheet printed from Kotka Collection Management System";
//        $this->pdf->writeHTML($html);

        return $this->pdf->getPDFData();

    }

    /**
     * Creates return pdf for transactions
     * @param HRATransaction $transaction
     * @param MOSOrganization $organization
     * @param MOSOrganization $owner
     * @return string
     */
    public function getReceiptPdf(HRATransaction $transaction, MOSOrganization $organization, MOSOrganization $owner)
    {
        $pageMargin = 10;
        $leftMargin = 15;

        $this->pdf->setPageOrientation("P", true, $pageMargin);
        $this->pdf->setMargins($leftMargin, $pageMargin, $pageMargin, true);
        $this->pdf->SetFont("freeserif");

        $this->pdf->AddPage();

        $this->pdf->writeHTML($this->getHeader($transaction));

        $type = $this->getTransactionType($transaction);
        // ABBA
        $html = "
            <h2>&nbsp;<br />INCOMING " . strtoupper($type) . "</h2>
            <p>We have received the material indicated here from your institution.
            Please use the " . $type . " ID <strong>" . $transaction->getUri() . "</strong> in all correspondence concerning this material.
            Thank you.</p>
            ";

        $this->pdf->writeHTML($html);

        // -------------------------------------------------------------------------------------------
        // Loan info

        $this->pdf->SetY(($this->pdf->GetY() + 5));
        $lineStyle['width'] = 0.05;
        $this->pdf->Line($leftMargin, $this->pdf->GetY(), 200, $this->pdf->GetY(), $lineStyle);

//        $this->pdf->SetY(($this->pdf->GetY() + 5));

        $this->pdf->writeHTML($this->getReturnInfoHtml($transaction, $organization, $owner, true));
        $html = '';
        // -------------------------------------------------------------------------------------------
        // Remarks
        if ($transaction->getHRAPublicRemarks('en')) {
            $lineStyle['width'] = 0.05;
            $this->pdf->Line($leftMargin, $this->pdf->GetY(), 200, $this->pdf->GetY(), $lineStyle);

            $this->pdf->SetY(($this->pdf->GetY() + 5));
            $html .= "
                <h3>Remarks</h3>
                <div>" . str_replace("\n", "<p>", $transaction->getHRAPublicRemarks('en')) . "</div>
            ";
        }

        $this->pdf->writeHTML($html);

        // -------------------------------------------------------------------------------------------
        // Signature
        $signatureSpotY = 250;
        if (($this->pdf->GetY()) > $signatureSpotY) {
            $this->pdf->AddPage();
        }
        $this->pdf->SetY($signatureSpotY);

        $lineStyle['width'] = 0.05;
        $this->pdf->Line($leftMargin, $this->pdf->GetY(), 200, $this->pdf->GetY(), $lineStyle);

        $this->pdf->SetY(($this->pdf->GetY() + 3));

        $html = $this->getSignature($transaction);

        $this->pdf->writeHTML($html);

        // -------------------------------------------------------------------------------------------
        // Material
        $this->pdf->AddPage();
        $this->pdf->SetY(($this->pdf->GetY() + 5));
        $lineStyle['width'] = 0.05;
        $this->pdf->Line($leftMargin, $this->pdf->GetY(), 200, $this->pdf->GetY(), $lineStyle);

        $specimens = $this->getSpecimenTableHtml($transaction);

        $this->pdf->SetY(($this->pdf->GetY() + 5));
        $html = "
            <h3>Material</h3>
            <div>" . str_replace("\n", "<p>", $transaction->getHRAMaterial('en')) . "<br /></div>
            ";
        $html .= $this->getParcelsLine($transaction);

        $this->pdf->writeHTML($html, true, false, true, false, '');

// -------------------------------------------------------------------------------------------
// Footer

//        $this->pdf->SetFontSize(5);
//        $this->pdf->SetY(($this->pdf->GetY() + 5));
//        $html = "Dispatch sheet printed from Kotka Collection Management System";
//        $this->pdf->writeHTML($html);

        return $this->pdf->getPDFData();

    }

    protected function getTransactionType(HRATransaction $transaction) {
        $type = 'loan';
        if ($transaction->getHRATransactionType() === self::EXCHANGE_INCOMING || $transaction->getHRATransactionType() === self::EXCHANGE_OUTGOING) {
            $type = 'exchange';
        } else if ($transaction->getHRATransactionType() === self::GIFT_INCOMING || $transaction->getHRATransactionType() === self::GIFT_OUTGOING) {
            $type = 'gift';
        }
        return $type;
    }


    /**
     * Helper method used to create one html line
     * @param $var
     * @return string
     */
    protected function getHTMLLine($var)
    {
        return empty($var) ? '' : $var . '<br />';
    }

    /**
     * Convert if needed the datetime value to string
     * @param $datetime
     * @return string
     */
    protected function getDateString($datetime)
    {
        if ($datetime instanceof DateTime) {
            return $datetime->format('d F Y');
        }

        return $datetime;
    }

    /**
     * Returns the POBox number based on the museum code
     * @param museumCode
     * @return int
     */
    protected function getPOBox($museumCode)
    {
        if ("H" == $museumCode) {
            return 7;
        }

        return 17;
    }

    /**
     * Creates the FMNH part for title in the page
     * @param HRATransaction $transaction
     * @return string
     */
    protected function getHeader(HRATransaction $transaction)
    {
        $owner = $this->getOwnerOrganization($transaction->getMZOwner());
        $address = '';
        $logo = '';
        if ($owner instanceof MOSOrganization) {
            $abbr = $owner->getMOSAbbreviation();
            $address = $owner->getMOSOrganizationLevel2('en') . '<br />' .
                $owner->getMOSOrganizationLevel3('en') .
                (empty($abbr) ? '' : ' (' . $abbr . ')') . '<br />';
            $addrLines = [
                'getMOSOrganizationLevel4En' => '%s',
                'getMOSPostOfficeBox' => 'P. O. BOX %s',
                'getMOSStreetAddress' => '%s',
                'postal' => ['getMOSPostalCode' => '%s', 'getMOSLocality' => '%s'],
                'getMOSCountry' => '%s',
            ];
            foreach ($addrLines as $property => $line) {
                if (is_array($line)) {
                    $value = [];
                    $delimiter = ' ';
                    if (isset($line['delimiter'])) {
                        $delimiter = $line['delimiter'];
                    }
                    unset($line['delimiter']);

                    foreach ($line as $prop => $l) {
                        $val = $owner->$prop();
                        if (!empty($val)) {
                            $value[] = sprintf($l, $val);
                        }
                    }
                    $value = implode($delimiter, $value);
                    $line = '%s';
                } else {
                    $value = $owner->$property();
                }
                if (!empty($value)) {
                    $address .= sprintf($line, $value) . '<br />';
                }
            }
            if (!empty($owner->getMOSLogo())) {
                $logo = '<td style="text-align: center"><img height="80" src="' . $owner->getMOSLogo() . '" /></td>';
            }
        }

        $ret = "";
        $ret .= "
            <table>
            <tr>
                <td>" . $address . "</td>
                " . $logo . " 
                <td style=\"text-align:right;\">
                    <img src=\"https://koivu.luomus.fi/projects/qrcode/?code=" . $transaction->getUri() . "\" width=\"50\" height=\"50\" />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <br />
                    " . $transaction->getUri() . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                </td>
            </tr>
            </table>
        ";
        return $ret;
    }

    protected function getOwnerOrganization($owner)
    {
        if ($this->organizationService === null) {
            $this->organizationService = $this->getServiceLocator()->get('Kotka\Service\OrganizationService');
        }
        return $this->organizationService->getById($owner);
    }

    /**
     * Creates the loan info part for the page
     * @param HRATransaction $transaction
     * @param MOSOrganization $organization
     * @return string
     */
    protected function getLoanInfoHtml(HRATransaction $transaction, MOSOrganization $organization)
    {
        // Line breaks if contains a private person's address
        $personDetails = str_replace("\n", "<br />", $transaction->getHRACorrespondentPerson());
        $poBox = '';
        if (!empty($organization->getMOSPostOfficeBox())) {
            $poBox = 'P.O. BOX ' . $organization->getMOSPostOfficeBox() . '<br>';
        }

        $ret = "";
        $ret .= "
<table>
<tr>
<td>
<h4>Recipient</h4>
$personDetails<br />
&nbsp;<br />" .
            $this->getHTMLline($organization->getMOSOrganizationLevel4('en') . (
                empty($organization->getMOSAbbreviation()) ? '' : ' (' . $organization->getMOSAbbreviation() . ')'
            )) .
            $this->getHTMLline($organization->getMOSOrganizationLevel1('en')) .
            $this->getHTMLline($organization->getMOSOrganizationLevel2('en')) .
            $this->getHTMLline($organization->getMOSOrganizationLevel3('en')) .
            $this->getHTMLline($poBox) .
            $this->getHTMLline($organization->getMOSStreetAddress()) .
            $this->getHTMLline($organization->getMOSPostalCode()) .
            $this->getHTMLline($organization->getMOSLocality()) .
            $this->getHTMLline($organization->getMOSRegion()) .
            mb_strtoupper($organization->getMOSCountry()) . "
</td>
<td>";

        // Gifts
        if ($transaction->getHRATransactionType() === self::GIFT_OUTGOING) {
            $ret .= "<strong>Gift ID:</strong> " . $transaction->getUri() . "<br />";
        } else if ($transaction->getHRATransactionType() === self::EXCHANGE_OUTGOING) {
            $ret .= "<strong>Exchange ID:</strong> " . $transaction->getUri() . "<br />";
        } else {
            $ret .= "<strong>Loan ID:</strong> " . $transaction->getUri() . "<br />
                <strong>Due date:</strong> " . $this->getDateString($transaction->getHRADueDate()) . "<br />";
        }

        $ret .= "<br />" . $this->getHandlerLine($transaction);

        if ($transaction->getHRATransactionType() === self::GIFT_OUTGOING || $transaction->getHRATransactionType() === self::EXCHANGE_OUTGOING) {

        } else {
            $ret .= "<strong>Loan request received:</strong> " . $this->getDateString($transaction->getHRATransactionRequestReceived()) . "<br />";
        }

        $ret .= "<strong>Sent to you on:</strong> " . $this->getDateString($transaction->getHRATransactionSent()) . "<br />";
        $ret .= $this->getParcelsLine($transaction);
        $ret .= "</td></tr></table>";
        return $ret;
    }

    protected function getHandlerLine(HRATransaction $transaction) {
        return "<strong>Handler:</strong> " . $transaction->getHRALocalPerson() . "<br />
            <strong>Handler email:</strong> " . $transaction->getHRALocalPersonEmail() . "<br />";
    }

    protected function getParcelsLine(HRATransaction $transaction) {
        $parcels = [];
        if (!empty($transaction->getHRASentParcels('en'))) {
            array_push($parcels, "<strong>Parcels:</strong> " . $transaction->getHRASentParcels('en'));
        }
        if (!empty($transaction->getHRASentType())) {
            array_push($parcels,"<strong>Sent by:</strong> " . $this->sentTypes[$transaction->getHRASentType()]);
        }
        if (count($parcels) > 0) {
            $parcels[0] = ucfirst($parcels[0]);
            return implode(', ', $parcels) . '<br />';
        }
        return '';
    }

    /**
     * Creates the return info part for the page
     * @param HRATransaction $transaction
     * @param MOSOrganization $organization
     * @param MOSOrganization $owner
     * @param bool $isRecipe
     * @return string
     */
    protected function getReturnInfoHtml(
        HRATransaction $transaction,
        MOSOrganization $organization,
        MOSOrganization $owner,
        $isRecipe = false
    )
    {
        // Line breaks if contains a private person's address
        $personDetails = str_replace("\n", "<br />", $transaction->getHRACorrespondentPerson());

        $ret = "";
        $ret .= "
<table>
    <tr>
    <td>
        <h3>Loan from</h3>
        $personDetails<br />
        &nbsp;<br />" .
            $this->getOrganizationName($organization, true) . "
        " . ($isRecipe ? '<br /><br />Sender\'s ' . $this->getTransactionType($transaction) . ' ID: ' . $transaction->getHRAExternalTransactionID() : '') . " 
    </td>
    <td>
        <h3>Loan to</h3>
        " . $this->getOrganizationName($owner, false, [4, 3, 2, 1]) . "
        " . $this->departments[$transaction->getHRALocalDepartment()] . "<br />
         <br />
        &nbsp;<br />
        <strong>Loan ID:</strong> " . $transaction->getUri() . "<br />
        <strong>Loan received:</strong> " . $this->getDateString($transaction->getHRATransactionRequestReceived()) . "<br />
        " . ($isRecipe ? '' : "<strong>Loan returned:</strong> " . $this->getDateString($transaction->getHRATransactionSent()) . "<br />" ) . "
        " . $this->getHandlerLine($transaction) . "
        </td>
    </tr>
</table>";
        return $ret;
    }

    /**
     *
     * Creates the specimen table part for the page
     *
     * @param HRATransaction $transaction
     *
     * @return string
     */
    protected function getSpecimenTableHtml(HRATransaction $transaction)
    {
        $currentColumn = 0;
        $awayCount = $transaction->getHRBAwayOther() === null ? 0 : (int)$transaction->getHRBAwayOther();
        $colCount = 3; // 3 is max for long HTTP URI's

        $specimens = "<br /><table>";
        $away = $transaction->getHRBAway();
        if ($away === null) {
            return '';
        }

        // Goes through all specimens and prints them out row by row, like this:
        // 1 2 3
        // 4 5 6
        // 7 8 9
        // This way long texts dont make any column much longer than others (as printing column by column could do)

        foreach ($away as $item) {
            $awayCount++;

            // Opens new row
            if (0 == $currentColumn) {
                $specimens .= "<tr>";
            }

            // Prints cell
            if (preg_match("{^(tun:)[a-z0-9]{1,4}\.[a-z0-9]+$}i", $item)) {
                $specimens .= "<td><a style=\"color:#000; text-decoration:none;\" href=\"" . IdService::getUri($item) . "\">" . IdService::getUri($item) . "</a></td>";
            } else {
                $specimens .= "<td>" . $item . "</td>";
            }

            $currentColumn++;

            // Closes row after every $colCount specimens
            if ($colCount == $currentColumn) {
                $specimens .= "</tr>";
                $currentColumn = 0;
            }
        }
        // Add ending </tr> if not already exist AND table contains specimens that are away
        $specimensEnding = substr($specimens, -5);
        if ("</tr>" != $specimensEnding && $awayCount > 0) {
            $specimens .= "</tr>";
        }
        $specimens .= "</table>";

        $this->awayCount = $awayCount;
        return $specimens;
    }

    /**
     * Creates the intro text part for the page
     * @param HRATransaction $transaction
     * @return string
     */
    protected function getIntroText(HRATransaction $transaction)
    {
        $ret = "";
        // Outgoing gift
        if (self::GIFT_OUTGOING === $transaction->getHRATransactionType()) {
            $ret .= "
            <h2>&nbsp;<br />SPECIMEN GIFT</h2>
            <p>We are sending you the material indicated here as a gift.
            After receiving the specimens, please sign and return one copy of this form to the above address" . (empty($transaction->getHRALocalPersonEmail()) ? '.' : ' or via email to ' . $transaction->getHRALocalPersonEmail() . '.') . "
            </p>
            <p>Please use the ID <strong>" . $transaction->getUri() . "</strong> in all correspondence concerning this material.
            </p>
            ";
        } else if (self::EXCHANGE_OUTGOING === $transaction->getHRATransactionType()) {
            $ret .= "
            <h2>&nbsp;<br />SPECIMEN EXCHANGE</h2>
            <p>We are sending you the material indicated here.
            After receiving the specimens, please sign and return one copy of this form to the above address" . (empty($transaction->getHRALocalPersonEmail()) ? '.' : ' or via email to ' . $transaction->getHRALocalPersonEmail() . '.') . "
            </p>
            <p>Please use the ID <strong>" . $transaction->getUri() . "</strong> in all correspondence concerning this material.
            </p>
            ";
        } // Other types
        else {
            $ret .= "
            <h2>&nbsp;<br />SPECIMEN LOAN</h2>
            <p>We are sending you the material indicated here.
            After receiving the specimens, please sign and return one copy of this form to the above address" . (empty($transaction->getHRALocalPersonEmail()) ? '.' : ' or via email to ' . $transaction->getHRALocalPersonEmail() . '.') . "
            </p>
            <p>All loans are subject to our loan regulations. Please use the loan ID <strong>" . $transaction->getUri() . "</strong> in all correspondence concerning this material. More information about loans at https://kotka.luomus.fi/loans
            </p>
            ";
        }
//        $POBox = $this->getPOBox($transaction->getHRALocalDepartment());


        return $ret;
    }

    /**
     * Creates the intro text part for the page
     * @param HRATransaction $transaction
     * @return string
     */
    protected function getSignature(HRATransaction $transaction)
    {
        $ret = "
            Material received in good condition<br />&nbsp;<br />&nbsp;<br />&nbsp;<br />
            Name &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            Signature &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            Place &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            Date
        ";

        return $ret;
    }

    protected function getOrganizationName(MOSOrganization $organization, $withAddress = false, $order = [4, 1, 2, 3]) {
        $line = '';
        foreach ($order as $spot) {
            if ($spot === 4) {
                $line .= $this->getHTMLline($organization->getMOSOrganizationLevel4('en') . (
                    empty($organization->getMOSAbbreviation()) ? '' : ' (' . $organization->getMOSAbbreviation() . ')'
                    ));
            } else {
                $method = 'getMOSOrganizationLevel' . $spot;
                $line .= $this->getHTMLLine($organization->$method('en'));
            }
        }

        if ($withAddress) {
            $line .= "<br />" .
                $this->getHTMLline($organization->getMOSStreetAddress()) .
                $this->getHTMLline($organization->getMOSPostalCode()) .
                $this->getHTMLline($organization->getMOSLocality()) .
                $this->getHTMLline($organization->getMOSRegion()) .
                mb_strtoupper($organization->getMOSCountry());
        }

        return $line;
    }
}
