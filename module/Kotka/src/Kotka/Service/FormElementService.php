<?php
namespace Kotka\Service;

use Common\Service\IdService;
use Elastic\Result\TypeMapping;
use Kotka\ElasticExtract\Specimen;
use Kotka\Http\HttpClientTrait;
use Triplestore\Db\Oracle;
use Triplestore\Model\Model;
use Triplestore\Model\Predicate;
use Triplestore\Service\ObjectManager;
use Zend\Cache\Storage\StorageInterface;
use Zend\Log\LoggerInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

class FormElementService implements ServiceLocatorAwareInterface
{

    use ServiceLocatorAwareTrait;
    use HttpClientTrait;

    const CACHE_PREFIX = 'select-';
    const ALIAS_KEY = '__alias__';

    protected $collectionService;
    protected $organizationService;
    protected $cacheAllowed = array();
    protected $specials = array(
        'HRA.collectionID' => 'getCollectionSelect',
        'MY.collectionID' => 'getCollectionSelect',
        'MF.collectionID' => 'getCollectionSelect',
        'MY.collectionID-GardenArea' => 'getCollectionGardenAreaSelect',
        'MZ.editor' => 'getEditor',
        'MY.datasetID' => 'getDatasetSelect',
        'MF.datasetID' => 'getDatasetSelect',
        'MOS.datasetID' => 'getDatasetSelect',
        'MA.person' => 'getUserSelect',
        'MA.person-known' => 'getDownloadUserSelect',
        'MY.downloadRequestHandler' => 'getDownloadUserSelect',
        'HRA.correspondentOrganization' => 'getOrganizationSelect',
        'PUU.seedsExchangedInstitution' => 'getOrganizationSelect',
        'MZ.owner' => 'getOrganizationSelect',
        'MZ.ownerID' => 'getOrganizationSelect',
        'MY.acquiredFromOrganization' => 'getOrganizationSelect',
        'MY.acquiredFromOrganizationID' => 'getOrganizationSelect',
        'rdf:type' => 'getAllClassesSelect',
        'zooNamespaces' => 'getZooNamespacesSelect',
        'botanyNamespaces' => 'getBotanyNamespacesSelect',
        'accessionNamespaces' => 'getAccessionNamespacesSelect',
        'cultureNamespaces' => 'getCultureNamespacesSelect',
        'country2Alpha' => 'getCountry2Alpha',
        'palaeontologyNamespaces' => 'getPalaeontologyNamespacesSelect',
        'boolean' => 'getBoolean',
        '_exists_' => 'getElasticFields',
    );
    protected $cache;
    protected $om;
    protected $log;

    public function isSpecial($field)
    {
        return isset($this->specials[$field]);
    }

    /**
     * @param string $field
     * @param mixed $notFoundValue
     * @param bool $includeAlias
     *
     * @return bool|mixed
     * @throws Exception\RuntimeException
     * @throws \Exception
     */
    public function getSelect($field, $notFoundValue = false, $includeAlias = true)
    {
        $cache = $this->getCache();
        $cacheKey = self::CACHE_PREFIX . $field;
        $value = [];
        if (strpos($field, 'dataset') !== false || !$cache->hasItem($cacheKey)) {
            if (isset($this->specials[$field])) {
                $method = $this->specials[$field];
                if (!method_exists($this, $method)) {
                    if ($notFoundValue !== false) {
                        return $notFoundValue;
                    }
                    throw new Exception\RuntimeException(sprintf("No values found for %s.", $field));
                }
                $value = $this->$method();
            } else {
                try {
                    $this->getObjectManager()->disableHydrator();
                    $value = $this->buildFromOnto($field);
                } catch (\Exception $e) {
                    if ($notFoundValue !== false) {
                        return $notFoundValue;
                    }
                    throw $e;
                } finally {
                    $this->getObjectManager()->enableHydrator();
                }
            }
            $cache->setItem($cacheKey, $value);
        }
        if ($value === null) {
            throw new Exception\RuntimeException(sprintf("No values found for %s.", $field));
        }
        $value = $cache->getItem($cacheKey);
        if (!$includeAlias && isset($value[self::ALIAS_KEY])) {
            unset($value[self::ALIAS_KEY]);
        }
        return $value;
    }

    protected function getElasticFields()
    {
        /** @var \Elastic\Client\Search $adapter */
        $adapter = $this->serviceLocator->get('Elastic\Client\Search');
        /** @var TypeMapping $mappings */
        return $adapter->getMappings(Specimen::getIndexName(), Specimen::getTypeName())->getProperties();
    }

    private function buildFromOnto($field, $allowEmpty = false)
    {
        $om = $this->getObjectManager();
        $mainModel = $om->fetch($field, false);
        if ($mainModel === null) {
            throw new Exception\RuntimeException(sprintf("No values found for %s in triplestore.", $field));
        }
        $pred = $mainModel->getPredicate(ObjectManager::PREDICATE_TYPE);
        if (!$pred instanceof Predicate) {
            throw new Exception\RuntimeException(sprintf("Unable to determinate the type of '%s'.", $field));
        }
        $type = $pred->current()->getValue();
        if ($type === ObjectManager::OBJ_TYPE_SELECT) {
            $fieldsModels = $mainModel;
        } else if ($type === ObjectManager::OBJ_TYPE_PROPERTY) {
            $rangePred = $mainModel->getPredicate(ObjectManager::PREDICATE_RANGE);
            if ($rangePred === null) {
                throw new Exception\RuntimeException(sprintf("No range found for the field '%s' in triplestore.", $field));
            }
            $range = $rangePred->current()->getValue();
            $fieldsModels = $om->fetch($range, false);
            if ($fieldsModels === null) {
                throw new Exception\RuntimeException(sprintf("Nothing found with range '%s' in triplestore.", $range));
            }
        } else {
            throw new Exception\RuntimeException(sprintf("Unable to determinate the type of '%s'.", $field));
        }

        $ordered = array();
        $alias = [];
        $group = 0;
        foreach ($fieldsModels as $predicate) {
            /** @var \Triplestore\Model\Predicate $predicate */
            $name = $predicate->getName();
            if (strpos($name, 'rdf:_') !== 0) {
                continue;
            }
            $spot = (int)str_replace('rdf:_', '', $name);
            if ($predicate->count() > 1) {
                throw new Exception\RuntimeException("Found ambiguous information for '$name'");
            }
            $value = $predicate->current()->getValue();
            $alias[mb_convert_case($value, MB_CASE_LOWER, 'utf-8')] = $value;
            /** @var Model $optionModel */
            $optionModel = $om->fetch($value, false);
            if ($optionModel !== null) {
                $labelPred = $optionModel->getOrCreatePredicate(ObjectManager::PREDICATE_LABEL);
                if ($labelPred !== null) {
                    $labels = $labelPred->getLiteralValue('en');
                }
                if (empty($labels)) {
                    $labels = $labelPred->getLiteralValue('fi');
                }
            } else {
                $optionModel = new Model();
            }
            if (empty($labels)) {
                $this->getLogger()->alert("Label information is missing for '$value'");
                $labels = array($value);
            }
            $child = null;
            if ($optionModel->getPredicate('rdf:_1') !== null) {
                $child = $this->buildFromOnto($value, true);
                if (isset($child[self::ALIAS_KEY])) {
                    $alias = array_merge($alias, $child[self::ALIAS_KEY]);
                    unset($child[self::ALIAS_KEY]);
                }
            }
            if ($child == null) {
                $ordered[$spot] = array(
                    'key' => $value,
                    'value' => array_pop($labels)
                );
            } else {
                $group++;
                $ordered[$spot] = array(
                    'key' => 'group' . $group,
                    'value' => array(
                        'label' => array_pop($labels),
                        'options' => $child,
                    )
                );
            }
        }
        if (count($ordered) === 0) {
            if ($allowEmpty) {
                return null;
            }
            throw new Exception\RuntimeException(sprintf("No data found for field %s.", $field));
        }
        ksort($ordered, SORT_NUMERIC);
        $return = array();
        foreach ($ordered as $data) {
            $return[$data['key']] = $data['value'];
        }
        $return[self::ALIAS_KEY] = $alias;

        return $return;
    }

    private function array_keys_multi(array $array)
    {
        $keys = array();
        foreach ($array as $key => $value) {
            if ($key === 'label') {
                continue;
            }
            if (is_array($value)) {
                $keys = array_merge($keys, $this->array_keys_multi($value));
            } else {
                $keys[] = $key;
            }
        }

        return $keys;
    }

    protected function getNamespaces($type)
    {
        $results = $this->getNamespaceData();
        $value = [];
        $defaultDomain = rtrim(IdService::DEFAULT_DB_QNAME_PREFIX, ':');
        foreach ($results as $result) {
            $dateType = $result['namespace_type'];
            if (($dateType !== $type && $dateType !== 'all') || !isset($result['namespace_id'])) {
                continue;
            }
            $ns = trim($result['namespace_id']);
            $prefix = $result['qname_prefix'] ?:$defaultDomain;
            if ($ns === null) {
                continue;
            }
            $value[$ns] = $prefix . ':' . $ns;
        }
        return $value;
    }

    public function getNamespaceData()
    {
        $cache = $this->getCache();
        if ($cache->hasItem('namespaces-raw')) {
            return $cache->getItem('namespaces-raw');
        }
        /** @var \Kotka\Options\ExternalsOptions $config */
        $config = $this->getServiceLocator()->get('Kotka\Options\ExternalsOptions');
        $nsWs = $config->getNamespaceWS();
        $json = file_get_contents($nsWs->getUrl());
        $data = json_decode($json, true);
        $cache->setItem('namespaces-raw', $data);
        return $data;
    }

    protected function getZooNamespacesSelect()
    {
        return $this->getNamespaces('zoo');
    }

    protected function getBotanyNamespacesSelect()
    {
        return $this->getNamespaces('botany');
    }

    protected function getAccessionNamespacesSelect()
    {
        return $this->getNamespaces('accession');
    }

    protected function getCultureNamespacesSelect()
    {
        return $this->getNamespaces('culture');
    }

    protected function getPalaeontologyNamespacesSelect()
    {
        return $this->getNamespaces('palaeontology');
    }

    public function getAuthor($taxon, $rank)
    {
        $key = 'author_' . md5($taxon) . '_' . $rank;
        $cache = $this->getCache();
        if ($cache->hasItem($key)) {
            return $cache->getItem($key);
        }
        /** @var Oracle $oracle */
        $oracle = $this->getObjectManager()->getConnection();
        $authors = $oracle->fetchDistinct('MY.author', false, ['MY.taxonRank' => $rank, 'MY.taxon' => $taxon]);
        $cache->setItem($key, $authors);
        return $authors;
    }

    protected function getCollectionSelect()
    {
        $collections = $this->getCollectionService()->getAll(['MY.collectionName', 'MY.metadataStatus', 'MY.internalUseOnly']);
        $options = array();
        foreach ($collections as $collection) {
            /** @var \Kotka\Triple\MYCollection $collection */
            if ($collection->getMYMetadataStatus() === 'MY.metadataStatusHidden' && $collection->getMYInternalUseOnly() !== true) {
                continue;
            }
            $options[$collection->getSubject()] = $collection->getMYCollectionName('en');
        }

        return $options;
    }

    protected function getCollectionGardenAreaSelect() {
        $collections = $this->getCollectionService()->getAll(['MY.collectionName']);
        $options = array();
        foreach ($collections as $collection) {
            /** @var \Kotka\Triple\MYCollection $collection */
            $options[$collection->getSubject()] = $collection->getMYCollectionName('en');
        }

        return $options;
    }

    protected function getDatasetSelect()
    {
        $newKotkaService = $this->getServiceLocator()->get('Kotka\Service\NewKotkaService');
        return $newKotkaService->getDatasetSelect();
    }

    protected function getBoolean() {
        return [
            'true' => 'true',
            'false' => 'false'
        ];
    }

    protected function getOrganizationSelect()
    {
        $newKotkaService = $this->getServiceLocator()->get('Kotka\Service\NewKotkaService');
        return $newKotkaService->getOrganizationSelect();
    }

    protected function getUserSelect()
    {
        return $this->getDownloadUserSelect(false, false);
    }

    protected function getDownloadUserSelect($onlyKnown = true, $addId = true)
    {
        $om = $this->getObjectManager();
        $om->disableHydrator();
        $results = $om->findBy(array(ObjectManager::PREDICATE_TYPE => 'MA.person'), null, null, null,
            array('MA.inheritedName', 'MA.preferredName', 'MA.roleKotka', 'MA.role')
        );
        $om->enableHydrator();
        $return = array();
        foreach ($results as $key => $model) {
            /** @var Model $model */
            if ($onlyKnown) {
                $roles = [];
                if ($model->hasPredicate('MA.role')) {
                    $roles = $model->getPredicate('MA.role')->getResourceValue();
                }
                if ($model->hasPredicate('MA.roleKotka')) {
                    $roles[] = $model->getOrCreatePredicate('MA.roleKotka')->current()->getValue();
                }
                if (count($roles) == 0 || in_array('MA.guest', $roles)) {
                    continue;
                }
            }
            $fullname = array();
            if ($model->hasPredicate('MA.inheritedName')) {
                $fullname[] = $model->getPredicate('MA.inheritedName')->getFirst()->getValue();
            }
            if ($model->hasPredicate('MA.preferredName')) {
                $fullname[] = $model->getPredicate('MA.preferredName')->getFirst()->getValue();
            }
            $return[$key] = implode(', ', $fullname);
            if ($addId) {
                $return[$key] = $return[$key] . ' (' . $key . ')';
            }
            asort($return);
        }

        return $return;
    }

    protected function getCountry2Alpha() {
        /** @var \Kotka\Options\ExternalsOptions $externals */
        $externals = $this->getServiceLocator()->get('Kotka\Options\ExternalsOptions');
        $apiLaji = $externals->getApiLaji();

        $response = $this->getHttpClient()
            ->setUri($apiLaji->getUrl() . 'areas?type=country&pageSize=10000')
            ->setHeaders(['Authorization' => $apiLaji->getPass()])
            ->send();

        $result = [];
        if ($response->isOk()) {
            $data = json_decode($response->getBody(), true);
            foreach ($data['results'] as $area) {
                if (isset($area['countryCodeISOalpha2']))  {
                    $result[$area['countryCodeISOalpha2']] = $area['name'];
                }
            }
        } else {
            throw new \Exception('Failed to fetch countries from api.laji.fi');
        }
        return $result;
    }

    protected function getEditor()
    {
        /** @var \Triplestore\Db\Oracle $connection */
        $connection = $this->getObjectManager()->getConnection();
        $editors = $connection->fetchDistinct('MZ.editor', true);
        $users = $this->getUserSelect();
        $editors = array_intersect_key($users, array_flip($editors));
        asort($editors);
        return $editors;
    }

    protected function getAllClassesSelect()
    {
        return $this->getByType(ObjectManager::OBJ_TYPE_CLASS);
    }

    public function getByType($type)
    {
        $om = $this->getObjectManager();
        $om->disableHydrator();
        $results = $om->findBy(array(ObjectManager::PREDICATE_TYPE => $type), null, null, null, array(ObjectManager::PREDICATE_TYPE));
        $om->enableHydrator();
        $return = array();
        foreach ($results as $key => $model) {
            /** @var Model $model */
            $return[$key] = $model->getSubject();
        }
        return $return;
    }

    /**
     * @return OrganizationService
     */
    private function getOrganizationService()
    {
        if ($this->organizationService === null) {
            $this->organizationService = $this->serviceLocator->get('Kotka\Service\OrganizationService');
        }
        return $this->organizationService;
    }

    /**
     * @return \Kotka\Service\CollectionService
     */
    private function getCollectionService()
    {
        if ($this->collectionService === null) {
            $this->collectionService = $this->serviceLocator->get('Kotka\Service\CollectionService');
        }
        return $this->collectionService;
    }

    /**
     * @return StorageInterface
     */
    private function getCache()
    {
        if ($this->cache === null) {
            $this->cache = $this->serviceLocator->get('perma-cache');
        }
        return $this->cache;
    }

    /**
     * @return ObjectManager
     */
    private function getObjectManager()
    {
        if ($this->om === null) {
            $this->om = $this->serviceLocator->get('Triplestore\ObjectManager');
        }
        return $this->om;
    }

    /**
     * @return LoggerInterface
     */
    private function getLogger()
    {
        if ($this->log === null) {
            $this->log = $this->serviceLocator->get('Logger');
        }
        return $this->log;
    }
}