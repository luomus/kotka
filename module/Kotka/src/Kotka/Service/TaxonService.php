<?php

namespace Kotka\Service;


use Triplestore\Classes\MYIdentificationInterface;
use Triplestore\Db\Oracle;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

class TaxonService implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    const MASTER_CHECKLIST = 'MR.1';

    protected static $cache = [];
    /** @var  Oracle */
    protected $connection;
    protected $initialized = false;

    public static function extractName(MYIdentificationInterface $identification, $withInfra = false, $infraRanks = []) {
        return TaxonService::extractNameFromArray([
            'MYTaxon' => '' . $identification->getMYTaxon(),
            'MYTaxonVerbatim' => '' . $identification->getMYTaxonVerbatim(),
            'MYInfraRank' => '' . $identification->getMYInfraRank(),
            'MYInfraEpithet' => '' . $identification->getMYInfraEpithet()
        ], $withInfra, $infraRanks);
    }

    public static function extractInfra(MYIdentificationInterface $identification, $infraRanks = []) {
        return TaxonService::extractInfraFromArray([
            'MYTaxon' => '' . $identification->getMYTaxon(),
            'MYTaxonVerbatim' => '' . $identification->getMYTaxonVerbatim(),
            'MYInfraRank' => '' . $identification->getMYInfraRank(),
            'MYInfraEpithet' => '' . $identification->getMYInfraEpithet()
        ], $infraRanks);
    }

    public static function extractNameFromArray($identification, $withInfra = false, $infraRanks = []) {
        if (
            empty($identification['MYTaxon']) &&
            empty($identification['MY.taxon']) &&
            empty($identification['MYTaxonVerbatim']) &&
            empty($identification['MY.taxonVerbatim'])
        ) {
            return '';
        }
        $taxon = isset($identification['MYTaxon']) ? $identification['MYTaxon'] : (isset($identification['MY.taxon']) ? $identification['MY.taxon'] : (isset($identification['MYTaxonVerbatim']) ? $identification['MYTaxonVerbatim'] : $identification['MY.taxonVerbatim']));
        if ($withInfra) {
            $infra = TaxonService::extractInfraFromArray($identification, $infraRanks);
            if (!empty($infra)) {
                $taxon .= ' ' . $infra;
            }
        }
        return $taxon;
    }

    public static function extractInfraFromArray($identification, $infraRanks = []) {
        if (
            empty($identification['MYTaxon']) &&
            empty($identification['MY.taxon']) &&
            empty($identification['MYTaxonVerbatim']) &&
            empty($identification['MY.taxonVerbatim'])
        ) {
            return '';
        }
        $taxon = [];
        $rank = isset($identification['MYInfraRank']) ?
            $identification['MYInfraRank'] :
            (isset($identification['MY.infraRank']) ? $identification['MY.infraRank'] : '');
        if (!empty($rank) && isset($infraRanks[$rank])) {
            $taxon[] = $infraRanks[$rank];
        }
        $infra = isset($identification['MYInfraEpithet']) ?
            $identification['MYInfraEpithet'] :
            (isset($identification['MY.infraEpithet']) ? $identification['MY.infraEpithet'] : '');
        if (!empty($infra)) {
            $taxon[] = $infra;
        }
        return implode(' ', $taxon);
    }

    public function hasTaxon($taxon, $checklist = self::MASTER_CHECKLIST)
    {
        if (!is_string($taxon)) {
            return false;
        }
        $upperName = strtoupper($taxon); // Contains only latin characters so this is fine
        $this->init($taxon, $upperName);
        return (isset(self::$cache[$checklist]) && isset(self::$cache[$checklist][$upperName]));
    }

    public function getCurrentFamilyName($name, $checklist = self::MASTER_CHECKLIST) {
        $taxonId = $this->getCurrentTaxonId($name, $checklist);
        if (empty($taxonId)) {
            return $taxonId;
        }
        $sql = "SELECT SCIENTIFICNAME FROM LTKM_LUONTO.RDF_TAXON_PARENTS_CHILDREN WHERE TAXON_QNAME = :taxonID and RANK = 'MX.family'";
        $conn = $this->getConnection();
        $conn->setUseResultSet(true);
        $results = $conn->executeSelectSql($sql, ['taxonID' => $taxonId]);
        foreach ($results as $result) {
            return $result['SCIENTIFICNAME'];
        }
        return '';
    }

    public function getSynonyms($name) {
        $upperName = strtoupper($name);
        if (isset(self::$cache['synonyms'][$upperName])) {
            return self::$cache['synonyms'][$upperName];
        }
        $sql = "SELECT * FROM LTKM_LUONTO.TAXON_SEARCH_MATERIALIZED WHERE QNAME in (
                    select QNAME from LTKM_LUONTO.TAXON_SEARCH_MATERIALIZED where name = :n and NAMETYPE = 'MX.scientificName' AND CHECKLIST = 'MR.1'
                ) AND CHECKLIST = 'MR.1' AND NAMETYPE in (
                    'MX.hasBasionym',
                    'MX.hasObjectiveSynonym',
                    'MX.hasSubjectiveSynonym',
                    'MX.hasHomotypicSynonym',
                    'MX.hasHeterotypicSynonym',
                    'MX.hasAlternativeName',
                    'MX.hasSynonym',
                    'MX.hasMisspelledName',
                    'MX.hasOrthographicVariant',
                    'MX.hasUncertainSynonym'
                )";
        $conn = $this->getConnection();
        $conn->setUseResultSet(true);
        $results = $conn->executeSelectSql($sql, ['n' => $upperName]);
        self::$cache['synonyms'][$upperName] = [];
        foreach ($results as $result) {
            self::$cache['synonyms'][$upperName][] = $result['CASEDNAME'];
        }
        self::$cache['synonyms'][$upperName] = array_unique(self::$cache['synonyms'][$upperName]);
        return self::$cache['synonyms'][$upperName];
    }

    public function getCurrentTaxonId($name, $checklist = self::MASTER_CHECKLIST)
    {
        if ($name === null || $name === '') {
            return '';
        }
        if (!is_string($name)) {
            throw new \Exception('Method getCurrentName is meant only for string values');
        }
        $upperName = strtoupper($name); // Contains only latin characters so this is fine
        $this->init($upperName, $checklist);
        if (isset(self::$cache[$checklist][$upperName])) {
            return self::$cache[$checklist][$upperName]['QNAME'];
        }
        return '';
    }

    public function getCurrentName($name, $checklist = self::MASTER_CHECKLIST)
    {
        if ($name === null || $name === '') {
            return '';
        }
        if (!is_string($name)) {
            throw new \Exception('Method getCurrentName is meant only for string values');
        }
        $upperName = strtoupper($name); // Contains only latin characters so this is fine
        $this->init($upperName, $checklist);
        if (isset(self::$cache[$checklist][$upperName])) {
            return self::$cache[$checklist][$upperName]['SCIENTIFICNAME'];
        }
        return $name;
    }

    public function getCurrentNames($names, $checklist = self::MASTER_CHECKLIST)
    {
        if (empty($names)) {
            return [];
        }
        $upperNames = array_map('strtoupper', $names);
        $this->init($upperNames, $checklist);
        $newNames = array_intersect_key(self::$cache[$checklist], $upperNames);
        return array_column($newNames, 'SCIENTIFICNAME');
    }

    public function getAuthor($taxon, $checklist = self::MASTER_CHECKLIST)
    {
        if ($taxon === null || $taxon === '') {
            return '';
        }
        if (!is_string($taxon)) {
            throw new \Exception('Method getCurrentName is meant only for string values');
        }
        $upperName = strtoupper($taxon); // Contains only latin characters so this is fine
        $this->init($upperName, $checklist);
        if (isset(self::$cache[$checklist][$upperName])) {
            return self::$cache[$checklist][$upperName]['AUTHOR'];
        }
        return '';
    }

    private function init($names, $checklist = self::MASTER_CHECKLIST)
    {
        if (!$this->initialized) {
            if (is_string($names) && isset(self::$cache[$checklist]) && isset(self::$cache[$checklist][$names])) {
                return;
            }
            if (!is_array($names)) {
                $names = [$names];
            }
            $this->initTaxa($names, $checklist, false);
        }
    }

    public function initTaxa(array $names, $checklist = self::MASTER_CHECKLIST, $noNew = true)
    {
        $this->initialized = $noNew;
        $upperNames = array_map('strtoupper', $names);
        $this->findCurrentNames($upperNames, $checklist);
    }

    protected function findCurrentNames($names, $checklist = self::MASTER_CHECKLIST)
    {
        if (!isset(self::$cache[$checklist])) {
            self::$cache[$checklist] = [];
        }
        $searchFor = array_diff($names, array_keys(self::$cache[$checklist]));
        if (empty($searchFor)) {
            return;
        }
        $conn = $this->getConnection();
        $bindQName = true;
        $where = 'CHECKLIST = :checklist AND NAMETYPE in (
                    \'MX.scientificName\',
                    \'MX.hasBasionym\',
                    \'MX.hasObjectiveSynonym\',
                    \'MX.hasSubjectiveSynonym\',
                    \'MX.hasHomotypicSynonym\',
                    \'MX.hasHeterotypicSynonym\',
                    \'MX.hasAlternativeName\',
                    \'MX.hasSynonym\',
                    \'MX.hasMisspelledName\',
                    \'MX.hasOrthographicVariant\',
                    \'MX.hasUncertainSynonym\'
                ) AND NAME';
        $sql = "SELECT * FROM LTKM_LUONTO.TAXON_SEARCH_MATERIALIZED WHERE %s IN (";
        $binds = [];
        $in = [];
        foreach ($searchFor as $key => $name) {
            if (strpos($name, 'MX.') === 0) {
                $where = 'QNAME';
                $bindQName = false;
            }
            $key = 'n' . $key;
            $in[] = ' :' . $key;
            $binds[$key] = $name;
        }
        if ($bindQName) {
            $binds['checklist'] = $checklist;
        }
        $sql = sprintf($sql, $where);
        $sql .= implode(',', $in) . ')';
        $conn->setUseResultSet(true);
        $results = $conn->executeSelectSql($sql, $binds);
        foreach ($results as $result) {
            self::$cache[$result['CHECKLIST']][$result['NAME']] = $result;
            self::$cache[self::MASTER_CHECKLIST][$result['QNAME']] = &self::$cache[$result['CHECKLIST']][$result['NAME']];
        }
    }

    protected function getConnection()
    {
        if ($this->connection === null) {
            $this->connection = $this->getServiceLocator()->get('Triplestore\Oracle');
        }
        return $this->connection;
    }
}