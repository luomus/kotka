<?php

namespace Kotka\Service;

use Exception;
use Kotka\Model\DataTypes;
use Zend\InputFilter\InputFilterInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class ValidationService is for validating the documents.
 * validator inside.
 *
 * @package Kotka\Service
 */
class ValidationService implements ServiceLocatorAwareInterface
{
    const IMPORT_KEY = '__import__';
    const GENERIC_KEY = '__generic__';

    protected $convertService;
    protected $serviceLocator;
    protected $filters = array();
    /** @var  InputFilterInterface */
    protected $errorFilter;
    /** @var  InputFilterInterface */
    protected $warningFilter;
    protected $dataTypes;

    public function __construct()
    {
        $this->dataTypes = new DataTypes();
    }

    /**
     * Return warning messages
     * @return string
     * @throws \Exception
     */
    public function getWarningMessages()
    {
        if ($this->warningFilter === null) {
            return array();
        }

        return $this->warningFilter->getMessages();
    }

    public function getErrorMessages()
    {
        return $this->errorFilter->getMessages();
    }

    public function hasWarning()
    {
        if ($this->warningFilter === null) {
            return false;
        }

        return !$this->warningFilter->isValid();
    }

    public function isValid()
    {
        return $this->errorFilter->isValid();
    }

    public function getFilteredData()
    {
        return $this->errorFilter->getValues();
    }

    public function validate(array & $data, $group = null, $showWarnings = false)
    {
        if ($group === null) {
            $group = InputFilterInterface::VALIDATE_ALL;
        }
        $this->errorFilter = $this->getInputFilter($data['datatype']);
        $this->errorFilter->setData($data)->setValidationGroup($group);
        if ($showWarnings) {
            $this->warningFilter = $this->getInputWarningFilter($data['datatype']);
            if ($this->warningFilter !== null) {
                $this->warningFilter->setData($data)->setValidationGroup($group);
            }

        }

        return $this;
    }

    public function getInputWarningFilter($datatype)
    {
        return $this->getInputFilter($datatype, 'Warning');
    }

    public function getInputFilter($FQClass, $type = '')
    {
        $parts = explode('\\', $FQClass);
        $class = array_pop($parts);
        $filterType = ucfirst(str_replace('specimen', '', $class)) . $type;
        if (!array_key_exists($filterType, $this->filters)) {
            $filter = '\Kotka\InputFilter\\' . $filterType . 'Filter';
            if (!class_exists($filter)) {
                if ($type === 'Warning') {
                    return null;
                }
                throw new Exception(sprintf("Could not find validator for '%s' from '%s'", $class, $filter));
            }
            return $this->serviceLocator->get('InputFilterManager')->get($filter);
        }
        return $this->filters[$filterType];
    }

    /**
     * Sets the service locator
     * @param ServiceLocatorInterface $sl
     */
    public function setServiceLocator(ServiceLocatorInterface $sl)
    {
        $this->serviceLocator = $sl;
    }

    /**
     * Gets the service locator
     * @return ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }
}
