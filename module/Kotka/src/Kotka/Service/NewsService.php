<?php

namespace Kotka\Service;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

class NewsService implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    const NEWS_LOCATION = './data/news.txt';
    const NEW_CACHE_KEY = 'kotka-news';

    /** @var \Zend\Cache\Storage\StorageInterface */
    private $cache;

    public function getNewsAsHtml()
    {
        $cache = $this->getCache();
        if ($cache->hasItem(self::NEW_CACHE_KEY)) {
            return $cache->getItem(self::NEW_CACHE_KEY);
        }
        $mdService = new MarkdownService();
        $md = $this->getNewsAsMarkdown();
        $html = $mdService->mdToHtml($md);
        $cache->setItem(self::NEW_CACHE_KEY, $html);

        return $html;
    }

    public function clearNewsCache()
    {
        return $this->getCache()->removeItem(self::NEW_CACHE_KEY);
    }

    public function getNewsAsMarkdown()
    {
        $md = '';
        if (file_exists(self::NEWS_LOCATION)) {
            $md = file_get_contents(self::NEWS_LOCATION);
        }
        return $md;
    }

    private function getCache()
    {
        if ($this->cache === null) {
            $this->cache = $this->getServiceLocator()->get('cache');
        }
        return $this->cache;
    }

}