<?php

namespace Kotka\Service;


use Kotka\Filter\PrefixFilter;
use Kotka\Form\Element\ArrayCollection;
use Kotka\Form\WarningForm;
use Triplestore\Model\Metadata;
use Triplestore\Model\Model;
use Triplestore\Service\ObjectManager;
use Triplestore\Service\PhpVariableConverter;
use Zend\Filter\AbstractFilter;
use Zend\Form\Element\Collection;
use Zend\Form\Element\Hidden;
use Zend\Form\Element\Select;
use Zend\Form\Fieldset;
use Zend\Form\Form;
use Zend\InputFilter\InputFilterInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Zend\Validator\AbstractValidator;

class FormService implements ServiceLocatorAwareInterface
{

    use ServiceLocatorAwareTrait;

    private $specialForms = [
        'MY.document' => 'MYDocument',
        'MY.gathering' => array('MYDocument' => 'MYGathering'),
        'MY.unit' => array('MYDocument' => array('MYGathering' => 'MYUnit')),
        'MY.identification' => array('MYDocument' => array('MYGathering' => array('MYUnit' => 'MYIdentification'))),
        'MY.typeSpecimen' => array('MYDocument' => array('MYGathering' => array('MYUnit' => 'MYTypeSpecimen'))),
        'MF.sample' => array('MYDocument' => array('MYGathering' => array('MYUnit' => 'MFSample'))),
        'MF.preparationClass' => array('MYDocument' => array('MYGathering' => array('MYUnit' => array('MFSample' => 'MFPreparation')))),
    ];

    private $classes = [
        'MY.document' => 'Specimens',
        'MY.gathering' => 'Collecting Event',
        'MY.unit' => 'Specimen / Observation',
        'MY.identification' => 'Identification',
        'MY.typeSpecimen' => 'Type specimen',
        'MF.sample' => 'Preparation/sample',
        'MF.preparationClass' => 'Preparation',
        'MY.collection' => 'Collection',
        'GX.dataset' => 'Tag',
        'MOS.organization' => 'Organization',
    ];

    public function explainAllForms()
    {
        $data = [];
        /** @var \Triplestore\Options\ModuleOptions $tripleOptions */
        $tripleOptions = $this->getServiceLocator()->get('Triplestore\Options\ModuleOptions');
        $ns = $tripleOptions->getClassNS();
        /** @var \Kotka\Service\FormGeneratorService $formGenerator */
        $formGenerator = $this->serviceLocator->get('Kotka\Service\FormGeneratorService');
        foreach ($this->classes as $class => $name) {
            $inputFilter = null;
            $warningFilter = null;
            $field = PhpVariableConverter::toPhpClassName($class);
            if (isset($this->specialForms[$class])) {
                $tmpName = is_array($this->specialForms[$class]) ? key($this->specialForms[$class]) : $this->specialForms[$class];
                $tmpName = $ns . '\\' . $tmpName;
                /** @var \Kotka\Form\WarningForm $form */
                $form = $formGenerator->getForm(new $tmpName());
                $form->populateValues(
                    [
                        'MYGathering' => [
                            ['MYUnit' => [[
                                'MYTypeSpecimen' => [
                                    ['subject' => '0']
                                ],
                                'MYIdentification' => [
                                    [
                                        'subject' => '0',
                                        'MYTaxonRank' => '',
                                    ]
                                ],
                                'MFSample' => [
                                    [
                                        'subject' => '0',
                                        'MFPreparation' => [['subject' => '0']]
                                    ]
                                ]
                            ]]
                            ]
                        ],
                        'HRBSpecimen' => [
                            ['subject' => 'added']
                        ]
                    ]
                );
                $form->prepare();
                $inputFilter = $form->getInputFilter();
                $warningFilter = $form->getInputWarningFilter();
                if (is_array($this->specialForms[$class])) {
                    list($form, $inputFilter, $warningFilter) = $this->travelToRightLevel($this->specialForms[$class], $form, $inputFilter, $warningFilter);
                }
            } else {
                $className = $ns . '\\' . $field;
                if (!class_exists($className)) {
                    continue;
                }
                $form = $formGenerator->getForm(new $className);

            }
            if (!$form instanceof Fieldset) {
                continue;
            }
            $name = $name . ' (' . str_replace('Class', '', $field) . ')';
            $data[$name] = $this->explainFieldSet($form, $inputFilter, $warningFilter);
        }


        return $data;
    }

    public function explainForm(Form $form)
    {
        $warningFilter = null;
        if ($form instanceof WarningForm) {
            $warningFilter = $form->getInputWarningFilter();
        }
        $inputFilter = $form->getInputFilter();

        return $this->explainFieldSet($form, $inputFilter, $warningFilter);
    }

    public function explainFieldSet(Fieldset $fieldset, InputFilterInterface $inputFilter = null, InputFilterInterface $warningFilter = null)
    {
        if ($inputFilter === null && $fieldset instanceof Form) {
            $fieldset->prepare();
            $inputFilter = $fieldset->getInputFilter();
        }
        if ($warningFilter === null && $fieldset instanceof WarningForm) {
            $fieldset->prepare();
            $warningFilter = $fieldset->getInputWarningFilter();
        }
        if ($fieldset instanceof Collection) {
            $fieldset = $fieldset->getTargetElement();
        }
        /** @var \Triplestore\Service\ObjectManager $om */
        $om = $this->getServiceLocator()->get('Triplestore\ObjectManager');
        $class = explode('\\', get_class($fieldset));
        $class = array_pop($class);
        /** @var \Triplestore\Model\Metadata $metadata */
        $metadata = $om->getMetadataService()->getMetadataFor($class);
        if ($metadata === null) {
            $metadata = $om->getMetadataService()->getMetadataFor($fieldset->getName());
        }
        if (!$metadata instanceof Metadata) {
            throw new \Exception("Aborting could not find metadata for " . get_class($fieldset) . "!");
        }
        $metadata->initAliases();

        $om->disableHydrator();
        $inDomain = $om->findBy([ObjectManager::PREDICATE_DOMAIN => $metadata->getClassName()]);
        if (empty($inDomain)) {
            $inDomain = $om->findBy([ObjectManager::PREDICATE_DOMAIN => $metadata->getClassName() . 'Class']);
        }
        $om->enableHydrator();
        $fields = [];
        $fieldsWithOutLabel = [];
        foreach ($inDomain as $field => $model) {
            if ($field === 'MY.isPartOf' || $metadata->getOrder($field) <= 0) {
                continue;
            }
            $label = $metadata->getLabel($field, 'en', null);
            if (!empty($label)) {
                $fields[$label] = $this->extractFieldData($model, $fieldset, $inputFilter, $metadata, $warningFilter);
            } else {
                //$fieldsWithOutLabel[$field] = $this->extractFieldData($model, $form, $inputFilter, $metadata, $warningFilter);
            }
        }

        return $fields;
    }

    private function travelToRightLevel($item, Fieldset $field, InputFilterInterface $inputFilter, InputFilterInterface $warningFilter = null)
    {
        if (is_array($item)) {
            $nextLevel = current($item);
            $next = is_array($nextLevel) ? key($nextLevel) : $nextLevel;
            $baseClass = 'Kotka\\InputFilter\\' . $next;
            if (!$field->has($next)) {
                $field = $field->get(0);
            }
            if (class_exists($baseClass)) {
                $inputFilter = $this
                    ->getServiceLocator()
                    ->get('InputFilterManager')
                    ->get($baseClass);
            }
            if ($warningFilter !== null) {
                $class = $baseClass . 'WarningFilter';
                if (class_exists($class)) {
                    $warningFilter = new $class();
                } else {
                    $warningFilter = $warningFilter->has($next) ? $warningFilter->get($next) : null;
                }
            }
            return $this->travelToRightLevel($nextLevel, $field->get($next), $inputFilter, $warningFilter);
        }
        if ($field->has(0)) {
            $field = $field->get(0);
        }
        return [$field, $inputFilter, $warningFilter];
    }

    private function extractFieldData(Model $fieldModel, Fieldset $form, InputFilterInterface $inputFilter, Metadata $metadata = null, InputFilterInterface $warningFilter = null)
    {
        $data = [];
        $subject = $fieldModel->getSubject();
        $field = PhpVariableConverter::toPhpMethod($subject);
        if ($metadata !== null) {
            if ($metadata->isRequired($subject)) {
                $data['required'] = true;
            }
            $help = $metadata->getHelp($subject);
            if (!empty($help)) {
                $data['help'] = $help;
            }
        }
        if ($fieldModel->hasPredicate('MZ.info')) {
            $infoPred = $fieldModel->getPredicate('MZ.info');
            $info = $infoPred->getLiteralValue('en');
            if (!empty($info)) {
                $data['info'] = $info;
            }
        }
        $element = null;
        if ($form->has($field)) {
            $element = $form->get($field);
        } else if ($form->has($field . 'En')) {
            $field = $field . 'En';
            $element = $form->get($field);
        }

        $removedPrefix = null;
        if ($inputFilter->has($field) && !$element instanceof ArrayCollection) {
            $validators = method_exists($inputFilter->get($field), 'getValidatorChain') ?
                $inputFilter->get($field)->getValidatorChain()->getValidators() : [];
            $clear = [];
            foreach ($validators as $validatorData) {
                $validator = $validatorData['instance'];
                $explained = $this->explainValidator($validator, $metadata);
                if ($explained !== false) {
                    $clear[get_class($validator)] = $explained;
                }
            }
            if (count($clear) > 0) {
                $data['errors'] = $clear;
            }
            $filters = method_exists($inputFilter->get($field), 'getFilterChain') ?
                $inputFilter->get($field)->getFilterChain()->getFilters() : [];
            $clear = [];
            foreach ($filters as $filter) {
                if ($filter instanceof PrefixFilter) {
                    $removedPrefix = $filter->getPrefix();
                }
                $explained = $this->explainFilter($filter);
                if ($explained !== false) {
                    $clear[get_class($filter)] = $explained;
                }
            }
            if (count($clear) > 0) {
                $data['filters'] = $clear;
            }
        }
        if ($warningFilter !== null && $warningFilter->has($field)) {
            $validators = $warningFilter->get($field)->getValidatorChain()->getValidators();
            $clear = [];
            foreach ($validators as $validatorData) {
                $validator = $validatorData['instance'];
                $explained = $this->explainValidator($validator, $metadata);
                if ($explained !== false) {
                    $clear[get_class($validator)] = $explained;
                }
            }
            if (count($clear) > 0) {
                $data['warnings'] = $clear;
            }
        }


        if ($element !== null) {
            if ($element instanceof Select) {
                $options = $this->getAllOptions($element->getValueOptions(), $removedPrefix);
                $data['options'] = $options;
            }
            if ($element instanceof Hidden) {
                $data['hidden'] = true;
            }
        }

        $data['field'] = $field;
        $data['databaseField'] = $subject;
        if ($fieldModel->hasPredicate(ObjectManager::PREDICATE_RANGE)) {
            $data['type'] = $fieldModel->getPredicate(ObjectManager::PREDICATE_RANGE)->current()->getValue();
        }

        return $data;
    }

    private function explainFilter(AbstractFilter $filter)
    {
        $class = get_class($filter);
        $class = substr($class, strrpos($class, '\\') + 1);
        switch ($class) {
            case 'PrefixFilter':
                $prefix = $filter->getPrefix();
                $return = 'Can be given with "' . $prefix . '" prefix';
                break;
            case 'PrefixRemover':
                $prefix = $filter->getPrefix();
                $return = 'Can be given with or without the "' . $prefix . '" prefix';
                break;
            case 'ToQName':
                $return = 'Can be given with the full uri or just the qname';
                break;
            case 'RomanDateToDate':
                $return = 'Month can be given with the Roman numeral';
                break;
            case 'StringToLower':
            case 'NormalizeDatabaseSelect':
                $return = 'is incasesensitive';
                break;
            case 'ExcelTimeToDateTime':
                //$return = 'With excel date converted to a string';
                //break;
            case 'Callback':
            case 'PregReplace':
            case 'FloatNormalizer':
            case 'ForceArray':
            case 'StringTrim':
            case 'DateTimeFormatter':
            case 'EmptyArray':
            case 'Coordinate':
                return false;
            default:
                $return = 'Unknown filter ' . get_class($filter);
        }
        return $return;
    }

    private function explainValidator(AbstractValidator $validator, Metadata $metadata)
    {
        $class = get_class($validator);
        $class = substr($class, strrpos($class, '\\') + 1);
        switch ($class) {
            case 'TaxonName':
                $explained = 'Value contains dot, comma or number character(s)';
                break;
            case 'IrccNumber':
            case 'IircNumber':
                $explained = 'Value must be valid IRCC number';
                break;
            case 'HasIRCCChecks':
                $explained = 'Is genetic resource but there is no IRCC number given';
                break;
            case 'TaxonRank':
            case 'InDatabaseSelect':
            case 'InArrayCaseInsensitive':
            case 'Explode':
            case 'InArray':
                $explained = 'Value does not belong to the list of allowed values';
                break;
            case 'FloatRange':
                $explained = 'Value does not contain a float or float range';
                break;
            case 'FloatValidator':
            case 'Float':
                $explained = 'Value does not contain a float';
                break;
            case 'Digits':
                $explained = 'Value does not contain only digits';
                break;
            case 'UniqueDatasetName':
                $explained = 'Tag name already exists';
                break;
            case 'UniqueADGroup':
                $explained = 'Ad group is already used in another organization';
                break;
            case 'Date':
                $format = $validator->getFormat();
                $explained = 'Value is not valid date in "' . $format . '" format';
                break;
            case 'DateCompare':
                $direction = $validator->getDirection();
                $to = $validator->getCompareTo();
                if ($to == null) {
                    $to = 'now';
                } else {
                    $to = '"' . $metadata->getLabel($to) . '"';
                }
                $explained = 'Value is not ' . $direction . ' ' . $to;
                break;
            case 'DateMulti':
                $format = $validator->getFormat();
                $format = implode('" or "', $format);
                $explained = 'Value is not valid date in "' . $format . '" format';
                break;
            case 'OneOrTheOther':
                $otherField = $metadata->getLabel(str_replace('En', '', $validator->getOther()));
                $explained = 'This and "' . $otherField . '" are empty';
                break;
            case 'Coordinate':
                $explained = 'Coordinate is not valid';
                break;
            case 'GreaterThan':
                $value = $validator->getMin();
                $explained = 'Value is not greater ' . ($validator->getInclusive() ? 'or equal ' : '') . 'than ' . $value;
                break;
            case 'Between':
                $min = $validator->getMin();
                $max = $validator->getMax();
                $inc = $validator->getInclusive() ? 'inclusive' : 'exclusive';
                $explained = 'Value is not between ' . $min . '-' . $max . ' (' . $inc . ')';
                break;
            /** @noinspection PhpMissingBreakStatementInspection */
            case 'ArrayContains':
                $contain = $validator->getContain();
                if ($contain !== null) {
                    $explained = 'Value does not contain "' . $contain . '"' . (strlen($contain) == 1 ? ' character' : '');
                    break;
                }
            case 'Regex':
                $pattern = $validator->getPattern();
                if ($pattern !== null) {
                    $explained = 'Value does not fit pattern "' . $this->explainReqExp($pattern) . '"';
                } else {
                    return false;
                }
                break;
            case 'BothNotSet':
                $another = $metadata->getLabel($validator->getHasNot());
                $explained = 'Both this and "' . $another . '" have values.';
                break;
            case 'RequiredWhen':
                $another = $metadata->getLabel($validator->getField());
                $explained = 'This is required when ' . $another . ' is not empty.';
                break;
            case 'RequireWithThis':
                $field = $metadata->getLabel($validator->getField());
                $contain = $validator->getWhenContains();
                if ($contain === null) {
                    $explained = 'This is given but there no value in "' . $field . '"';
                } else {
                    $explained = 'This contains "' . $contain . '" but there no value in "' . $field . '"';
                }
                break;
            case 'NotEmpty':
                $explained = 'Value is empty';
                break;
            case 'UserInOrganization':
            case 'DateStep':
            case 'Cycle':
                return false;
            default:
                $explained = 'Unknown validator ' . get_class($validator);
                break;
        }
        return $explained;
    }

    private function explainReqExp($reqexp)
    {
        $transaction = $reqexp;
        if ($reqexp == '{^[^0-9]*, [^0-9]*$}') {
            $transaction = 'lastname, firstname';
        }
        return $transaction;
    }

    private function getAllOptions($options, $removePrefix = null)
    {
        $all = [];
        foreach ($options as $key => $value) {
            if (is_array($value)) {
                $all = array_merge($all, $this->getAllOptions($value, $removePrefix));
            } else {
                if ($key !== 'label') {
                    if ($removePrefix !== null) {
                        $key = str_replace($removePrefix, '', $key);
                    }
                    $all[$key] = $value;
                }
            }
        }
        return $all;
    }
}