<?php

namespace Kotka\Service;

use Kotka\Mapper\MOSOrganizationMapperInterface;
use Kotka\Triple\MAPerson;
use Kotka\Triple\MOSOrganization;

/**
 * Class OrganizationService contains the business logic for organizations.
 * This should be the only place where organizations are handled!
 *
 * This service inherits few common methods from DocumentService
 *
 * @package Kotka\Service
 */
class OrganizationService extends DocumentService implements DocumentServiceInterface
{
    /** @var \Kotka\Mapper\MOSOrganizationMapperInterface */
    protected $organizationMapper;

    /**
     * Initializes new object
     * @param MOSOrganizationMapperInterface $organizationManager
     * @param MAPerson $authenticationService
     * @param $config
     */
    public function __construct(MOSOrganizationMapperInterface $organizationManager, MAPerson $authenticationService, $config)
    {
        parent::__construct($authenticationService, $config);
        $this->organizationMapper = $organizationManager;
    }

    /**
     * Returns the organization with the given id.
     *
     * @param  String $id
     * @return MOSOrganization
     */
    public function getById($id)
    {
        return $this->organizationMapper->find($id);
    }

    /**
     * Returns all organizations
     *
     * @param $fields
     * @return mixed
     */
    public function getAll(array $fields = null)
    {
        return $this->organizationMapper->findBy(array(), array('MOS.orgLevel1' => 'ASC'), null, null, $fields);
    }

    /**
     * Return all organizations in order of abbreviation
     * @return mixed
     */
    public function getAllInOrderByAbbreviation()
    {
        return $this->organizationMapper->findBy(array(), array('MOS.abbreviation' => 'ASC'));
    }

    /**
     * Finds the organization that has been linked to the ad groud id
     *
     * @param  string $adGroup
     * @return array
     */
    public function getAllWithAdGroup($adGroup)
    {
        return $this->organizationMapper->findBy(array('MOS.ad' => $adGroup));
    }

    /**
     * @param $adGroup
     * @return MOSOrganization
     */
    public function getOneWithAdGroup($adGroup)
    {
        return $this->organizationMapper->findOneBy(array('MOS.ad' => $adGroup));
    }

    /**
     * Generates the organization name from organization document.
     *
     * @param MOSOrganization $organization
     * @param bool $addAbbr
     * @return String
     */
    public function getOrganizationName(MOSOrganization $organization, $addAbbr = true)
    {
        if ($organization === null) {
            return '';
        }
        $name = array();
        $fields = array(
            'getMOSOrganizationLevel4',
            'getMOSOrganizationLevel3',
            'getMOSOrganizationLevel2',
            'getMOSOrganizationLevel1');

        foreach ($fields as $method) {
            $value = trim($organization->$method('en'));
            if ($value !== '' && $value !== null) {
                $name[] = $value;
            }
        }
        $name = join(', ', $name);
        if ($addAbbr) {
            $abbreviation = $organization->getMOSAbbreviation();
            if ($abbreviation !== '' && $abbreviation !== null) {
                $name = $abbreviation . ' - ' . $name;
            }
        }
        return $name;
    }

    /**
     * Stores new organization object to the database
     *
     * @param MOSOrganization $organization
     * @return MOSOrganization
     * @throws Exception\DomainException
     */
    public function create($organization)
    {
        if (!$organization instanceof MOSOrganization) {
            throw new Exception\DomainException('Organization service can\'t handle element of type ' . get_class($organization));
        }
        $this->setId($organization);
        $this->setEdit($organization, true);

        return $this->organizationMapper->create($organization);
    }

    /**
     * Updates existing object
     * @param MOSOrganization $organization
     * @return MOSOrganization
     */
    public function update($organization)
    {
        $this->setEdit($organization);

        return $this->organizationMapper->update($organization);
    }

    /**
     * Returns class name
     * @return string class name
     */
    public function getClassName()
    {
        return $this->organizationMapper->getClassName();
    }

}
