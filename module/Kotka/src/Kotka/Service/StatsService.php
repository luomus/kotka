<?php

namespace Kotka\Service;

use Common\Service\IdService;
use Kotka\Model\DataTypes;
use Triplestore\Db\Oracle;
use Triplestore\Service\ObjectManager;
use Zend\Cache\Storage\StorageInterface;

/**
 * Class StatsService is for creating statistical data.
 * @package Kotka\Service
 */
class StatsService
{
    /** @var  \Triplestore\Service\ObjectManager */
    protected $om;

    /** @var  StorageInterface */
    protected $cache;

    /**
     * Initializes the object
     *
     * @param ObjectManager $om
     */
    public function __construct(ObjectManager $om, StorageInterface $cache)
    {
        $this->om = $om;
        $this->cache = $cache;
    }

    public function getDistinctValues(array $fields)
    {
        $sql = "SELECT RESOURCELITERAL, COUNT(*) AS CNT FROM RDF_STATEMENTVIEW WHERE PREDICATENAME = '%s' GROUP BY RESOURCELITERAL ORDER BY CNT DESC";
        $result = [];
        /** @var \Triplestore\Db\Oracle $connection */
        $connection = $this->om->getConnection();
        foreach ($fields as $field) {
            $result[$field] = $connection->executeSelectSql(sprintf($sql, $field));
        }
        return $result;
    }

    /**
     * Creates array that has all the data simple stats page requires
     *
     * @return array
     */
    public function getSimpleStats()
    {
        $datatype = new DataTypes();
        $specimenTypes = $datatype->getSpecimenDatatypes();
        $specimenCount = array();
        $specimenCount['total'] = 0;
        foreach ($specimenTypes as $datatype) {
            $count = $this->om->count(array('MY.datatype' => $datatype));
            $specimenCount[$datatype] = $count;
            $specimenCount['total'] += $count;
        }
        $collections = $this->om->className('\Kotka\Triple\MYCollection')->findBy(array(ObjectManager::PREDICATE_TYPE => 'MY.collection'), null, null, null, array('MY.collectionName', 'MY.collectionSize', 'MY.digitizedSize', 'MY.collectionType'));
        $specimensInCollection = array();
        $collectionCount = array();
        $collectionCount['total']['records'] = 0;
        $collectionCount['total']['digitized'] = 0;
        $collectionCount['total']['resources'] = 0;
        $collectionTypes = array('specimens', 'living', 'monitoring', 'observations', 'publicationdata', 'publication', 'mixed', 'other', '');
        foreach ($collectionTypes as $collection) {
            $collectionCount[$collection]['records'] = 0;
            $collectionCount[$collection]['resources'] = 0;
            $collectionCount[$collection]['digitized'] = 0;
        }
        foreach ($collections as $collection) {
            $collectionSize = $collection->getMYCollectionSize();
            $collectionDigi = $collectionSize * $collection->getMYDigitizedSize() / 100;
            $collectionCount['total']['records']++;
            $collectionCount['total']['resources'] += $collectionSize;
            $collectionCount['total']['digitized'] += $collectionDigi;

            $type = lcfirst(str_replace('MY.collectionType', '', $collection->getMYCollectionType()));
            $collectionCount[$type]['records']++;
            $collectionCount[$type]['resources'] += $collectionSize;
            $collectionCount[$type]['digitized'] += $collectionDigi;

            $name = $collection->getMYCollectionName('en');
            $count = $this->om->count(array('MY.collectionID' => $collection->getSubject()));
            if ($count) {
                $specimensInCollection[$name] = $count;
            }
        }

        $transactions = $this->om->className('\Kotka\Triple\HRATransaction')->findBy(array(ObjectManager::PREDICATE_TYPE => 'HRA.transaction'), null, null, null, array('HRA.transactionType'));
        $transactionCount = array();
        $transactionCount['total'] = 0;
        $transactionTypes = array('loanIncoming', 'giftIncoming', 'exchangeIncoming', 'loanOutgoing', 'giftOutgoing', 'exchangeOutgoing');
        foreach ($transactionTypes as $transactionType) {
            $transactionCount[$transactionType] = 0;
        }
        foreach ($transactions as $transaction) {
            $transactionCount['total']++;
            $type = lcfirst(str_replace('HRA.transactionType', '', $transaction->getHRATransactionType()));
            $transactionCount[$type]++;
        }

        return array(
            'specimens' => $specimenCount,
            'collections' => $collectionCount,
            'transactions' => $transactionCount,
            'specimens-in-collection' => $specimensInCollection,
        );
    }

    public function getSpecimensInCollections($statsYear)
    {
        $cacheKey = 'reports_' . $statsYear;
        if ($this->cache->hasItem($cacheKey)) {
            return $this->cache->getItem($cacheKey);
        }
        $stats = $this->_getSpecimensInCollections($statsYear);
        $this->cache->setItem($cacheKey, $stats);

        return $stats;
    }

    private function _getSpecimensInCollections($statsYear)
    {
        /** @var Oracle $connection */
        $connection = $this->om->getConnection();
        $dbName = $connection->getDatabase();
        $sql = "SELECT
                  count(documents.SUBJECTNAME) AS count,
                  collection.OBJECTNAME AS collectionID,
                  collectionName.RESOURCELITERAL as collectionName
                FROM $dbName.RDF_STATEMENTVIEW documents
                  JOIN $dbName.RDF_STATEMENTVIEW collection ON (documents.SUBJECTNAME = collection.SUBJECTNAME AND collection.PREDICATENAME = 'MY.collectionID')
                  JOIN $dbName.RDF_STATEMENTVIEW collectionName ON (collection.OBJECTNAME = collectionName.SUBJECTNAME AND collectionName.PREDICATENAME = 'MY.collectionName'  AND collectionName.LANGCODEFK = 'en')
                WHERE documents.PREDICATENAME = 'rdf:type'
                  AND documents.OBJECTNAME = 'MY.document'
                GROUP BY collection.OBJECTNAME, collectionName.RESOURCELITERAL";
        $resultTotal = $connection->executeSelectSql($sql);
        $sql = "SELECT
                  count(documents.SUBJECTNAME) AS count,
                  collection.OBJECTNAME AS collectionID,
                  acquisitionDate.RESOURCELITERAL AS acquisitionDate
                FROM $dbName.RDF_STATEMENTVIEW documents
                  JOIN $dbName.RDF_STATEMENTVIEW createddates ON (documents.SUBJECTNAME = createddates.SUBJECTNAME AND createddates.PREDICATENAME = 'MZ.dateCreated')
                  JOIN $dbName.RDF_STATEMENTVIEW collection ON (documents.SUBJECTNAME = collection.SUBJECTNAME AND collection.PREDICATENAME = 'MY.collectionID')
                  LEFT JOIN $dbName.RDF_STATEMENTVIEW acquisitionDate ON (documents.SUBJECTNAME = acquisitionDate.SUBJECTNAME AND acquisitionDate.PREDICATENAME = 'MY.acquisitionDate')
                WHERE documents.PREDICATENAME = 'rdf:type'
                  AND documents.OBJECTNAME = 'MY.document'
                  AND substr(createddates.RESOURCELITERAL, 1, 4) = :year
                GROUP BY collection.OBJECTNAME, acquisitionDate.RESOURCELITERAL";
        $resultYear = $connection->executeSelectSql($sql, ['year' => $statsYear]);
        $sql = "SELECT
                  documents.SUBJECTNAME AS subject,
                  collection.OBJECTNAME AS collectionID,
                  typeVerification.OBJECTNAME AS typeVerification,
                  typeStatus.OBJECTNAME as typeStatus,
                  picture.OBJECTNAME as picture
                FROM $dbName.RDF_STATEMENTVIEW documents
                  JOIN $dbName.RDF_STATEMENTVIEW createddates ON (documents.SUBJECTNAME = createddates.SUBJECTNAME AND createddates.PREDICATENAME = 'MZ.dateCreated')
                  JOIN $dbName.RDF_STATEMENTVIEW collection ON (documents.SUBJECTNAME = collection.SUBJECTNAME AND collection.PREDICATENAME = 'MY.collectionID')
                  JOIN $dbName.RDF_STATEMENTVIEW gathering ON (documents.SUBJECTNAME = gathering.OBJECTNAME AND gathering.PREDICATENAME = 'MY.isPartOf')
                  JOIN $dbName.RDF_STATEMENTVIEW unit ON (gathering.SUBJECTNAME = unit.OBJECTNAME AND unit.PREDICATENAME = 'MY.isPartOf')
                  JOIN $dbName.RDF_STATEMENTVIEW unitChild ON (unit.SUBJECTNAME = unitChild.OBJECTNAME AND unitChild.PREDICATENAME = 'MY.isPartOf')
                  JOIN $dbName.RDF_STATEMENTVIEW type ON (unitChild.SUBJECTNAME = type.SUBJECTNAME AND type.PREDICATENAME = 'rdf:type' AND type.OBJECTNAME = 'MY.typeSpecimen')
                  JOIN $dbName.RDF_STATEMENTVIEW typeStatus ON (type.SUBJECTNAME = typeStatus.SUBJECTNAME AND typeStatus.PREDICATENAME = 'MY.typeStatus')
                  LEFT JOIN $dbName.RDF_STATEMENTVIEW typeVerification ON (type.SUBJECTNAME = typeVerification.SUBJECTNAME AND typeVerification.PREDICATENAME = 'MY.typeVerification')
                  LEFT JOIN $dbName.RDF_STATEMENTVIEW picture ON (documents.SUBJECTURI = picture.RESOURCELITERAL AND picture.PREDICATENAME = 'MM.documentURI')
                WHERE documents.PREDICATENAME = 'rdf:type'
                      AND documents.OBJECTNAME = 'MY.document'
                      AND substr(createddates.RESOURCELITERAL, 1, 4) = :year
                ORDER BY type.STATEMENTID DESC";
        $resultTypeSpecimens = $connection->executeSelectSql($sql, ['year' => $statsYear]);
        $resultSorter = [];
        $result = [];
        $result['totals'] = [
            'name' => 'All collections totals',
            'total' => 0,
            'year' => 0,
            'before' => 0,
            'type_verified' => 0,
            'type_other_verified' => 0,
            'type_with_picture' => 0
        ];
        //intentionally so that totals would be last in the sorted results
        $resultSorter['totals'] = 'öö';
        //Count totals
        foreach ($resultTotal as $data) {
            $count = (int)$data['COUNT'];
            $result['totals']['total'] += $count;
            $result[$data['COLLECTIONID']] = [
                'uri' => IdService::getUri($data['COLLECTIONID']),
                'name' => $data['COLLECTIONNAME'],
                'total' => $count,
                'year' => 0,
                'before' => 0,
                'type_verified' => 0,
                'type_other_verified' => 0,
                'type_with_picture' => 0
            ];
            $resultSorter[$data['COLLECTIONID']] = $data['COLLECTIONNAME'];
        }

        //Count by acquisition date
        foreach ($resultYear as $data) {
            if (!isset($result[$data['COLLECTIONID']])) {
                $result[$data['COLLECTIONID']] = [
                    'year' => 0,
                    'before' => 0,
                ];
            }
            $year = 0;
            $before = 0;
            $acquisition = $data['ACQUISITIONDATE'];
            $count = (int)$data['COUNT'];
            if ($acquisition === null) {
                $year += $count;
            } else {
                preg_match('/\d{4}/', $acquisition, $matches);
                if (is_array($matches) && isset($matches[0]) && $matches[0] == $statsYear) {
                    $year += $count;
                } else {
                    $before += $count;
                }
            }
            $result['totals']['before'] += $before;
            $result['totals']['year'] += $year;
            $result[$data['COLLECTIONID']]['before'] += $before;
            $result[$data['COLLECTIONID']]['year'] += $year;
        }

        $processed = [];
        //Determinate if type is correct type
        foreach ($resultTypeSpecimens as $data) {
            $subject = $data['SUBJECT'];
            $verification = $data['TYPEVERIFICATION'];
            $status = $data['TYPESTATUS'];
            $picture = $data['PICTURE'];
            if (!isset($processed[$subject])) {
                $processed[$subject] = [
                    'collection' => $data['COLLECTIONID'],
                    'verified' => $verification == 'MY.typeVerificationVerified',
                    'status' => $status,
                    'picture' => $picture !== null,
                ];
                continue;
            }
            if ($processed[$subject]['verified'] === false &&
                $verification == 'MY.typeVerificationVerified'
            ) {
                $processed[$subject]['verified'] = true;
                $processed[$subject]['status'] = $status;
            }

        }
        foreach ($processed as $data) {
            $verified = (int)$data['verified'];
            $otherVerified = (int)!$data['verified'];
            $picture = (int)$data['picture'];
            if ($data['status'] == 'MY.typeStatusNo') {
                continue;
            }
            $result['totals']['type_verified'] += $verified;
            $result['totals']['type_other_verified'] += $otherVerified;
            $result['totals']['type_with_picture'] += $picture;
            $result[$data['collection']]['type_verified'] += $verified;
            $result[$data['collection']]['type_other_verified'] += $otherVerified;
            $result[$data['collection']]['type_with_picture'] += $picture;
        }
        array_multisort($resultSorter, SORT_ASC, SORT_LOCALE_STRING, $result);

        return $result;
    }

    public function getSpecimenAddsByUser($from, $to, $limit = false)
    {
        /** @var Oracle $connection */
        $connection = $this->om->getConnection();
        $dbName = $connection->getDatabase();
        $sql = "SELECT
                  creator.OBJECTNAME as creator,
                  count(documents.SUBJECTNAME) AS count
                FROM $dbName.RDF_STATEMENTVIEW documents
                  JOIN $dbName.RDF_STATEMENTVIEW datecreated ON (documents.SUBJECTNAME = datecreated.SUBJECTNAME AND datecreated.PREDICATENAME = 'MZ.dateCreated')
                  JOIN $dbName.RDF_STATEMENTVIEW creator ON (documents.SUBJECTNAME = creator.SUBJECTNAME AND creator.PREDICATENAME = 'MZ.creator')
                WHERE documents.PREDICATENAME = 'rdf:type'
                      AND documents.OBJECTNAME = 'MY.document'
                      AND TO_DATE(substr(datecreated.RESOURCELITERAL, 1, 19), 'YYYY-MM-DD\"T\"hh24:mi:ss') BETWEEN TO_DATE(:t1,'YYYY-MM-DD') AND TO_DATE(:t2,'YYYY-MM-DD')
                GROUP BY creator.OBJECTNAME
                ORDER BY count DESC";
        $results = $connection->executeSelectSql($sql, ['t1' => $from, 't2' => $to], false, $limit);
        $return = [];
        foreach ($results as $result) {
            $return[$result['CREATOR']] = $result['COUNT'];
        }

        return $return;
    }

    public function getSpecimenAddsByMonth($limit = 6)
    {
        /** @var Oracle $connection */
        $connection = $this->om->getConnection();
        $dbName = $connection->getDatabase();
        $sql = "SELECT
                  substr(datecreated.RESOURCELITERAL, 1, 7) as month,
                  count(documents.SUBJECTNAME) as count
                FROM $dbName.RDF_STATEMENTVIEW documents
                  JOIN $dbName.RDF_STATEMENTVIEW datecreated ON (documents.SUBJECTNAME = datecreated.SUBJECTNAME AND datecreated.PREDICATENAME = 'MZ.dateCreated')
                  JOIN $dbName.RDF_STATEMENTVIEW creator ON (documents.SUBJECTNAME = creator.SUBJECTNAME AND creator.PREDICATENAME = 'MZ.creator')
                WHERE documents.PREDICATENAME = 'rdf:type'
                      AND documents.OBJECTNAME = 'MY.document'
                GROUP BY substr(datecreated.RESOURCELITERAL, 1, 7)
                ORDER BY month DESC";

        $results = $connection->executeSelectSql($sql, [], false, $limit);
        $return = [];
        foreach ($results as $result) {
            $return[$result['MONTH']] = $result['COUNT'];
        }

        return $return;
    }

}
