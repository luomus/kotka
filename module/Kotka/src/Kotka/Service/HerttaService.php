<?php

namespace Kotka\Service;


use Common\Service\IdService;
use Importer\Service\ConverterService;
use Kotka\Model\Count;
use Kotka\Model\ExcelColumn;
use Kotka\Model\ExcelSheet;
use Kotka\Model\TaxonResult;
use Kotka\Stdlib\Iterator\IdentificationIterator;
use Triplestore\Classes\MOSOrganizationInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

class HerttaService implements ServiceLocatorAwareInterface
{

    use ServiceLocatorAwareTrait;

    const SKIP = '_skip_';
    const VALUE_AS_IS = 'concatenate';
    const FIRST_EXISTING = 'firstExisting';

    protected $fields = [
        'Eliöryhmä' => [
            'type' => 'taxonGroup',
            'match' => 'MY.taxon'
        ],
        'Alaryhmä' => self::SKIP,
        'Tieteellinen nimi (TAX)' => [
            'type' => 'tax',
            'level' => 'identification',
            'fields' => ['MYTaxon', 'MYAuthor'],
            'match' => 'MY.taxon, MY.author'
        ],
        'MX koodi' => [
            'type' => 'MXCode',
            'match' => 'MY.taxon'
        ],
        'Suomenkielinen nimi' => self::SKIP,
        'Kunta' => [
            'type' => 'municipality',
            'match' => 'MY.municipality'
        ],
        'Havaintopaikan nimi' => [
            'type' => 'locality',
            'match' => 'MY.localityID, MY.locality'
        ],
//        'Koordinaattijärjestelmä' => [
//            'type' => self::VALUE_AS_IS,
//            'level' => 'gathering',
//            'fields' => ['MYCoordinateSystem']
//        ],
        'P-koord' => [
            'type' => 'coordinate',
            'index' => 0,
            'match' => 'MY.latitude'
        ],
        'I-koord' => [
            'type' => 'coordinate',
            'index' => 1,
            'match' => 'MY.longitude'
        ],
        'Virhesäde m' => [
            'type' => self::VALUE_AS_IS,
            'level' => 'gathering',
            'fields' => ['MYCoordinateRadius'],
            'match' => 'MY.coordinateRadius'
        ],

        //'Karttalehti' => self::SKIP,
        'Havaintopaikan kuvaus' => [
            'type' => self::VALUE_AS_IS,
            'level' => 'gathering',
            'fields' => ['MYLocalityDescription', 'MYHabitatDescription'],
            'match' => '[several]'
        ],
        'Havaintopaikan tila' => [
            'type' => 'dateStatus'
        ],
        'Alku pvm' => [
            'type' => self::VALUE_AS_IS,
            'fields' => ['MYDateBegin'],
            'match' => 'MY.dateBegin'
        ],
        'Loppu pvm' => [
            'type' => self::VALUE_AS_IS,
            'fields' => ['MYDateEnd'],
            'match' => 'MY.dateEnd'
        ],
        'Havaitsija/kerääjä' => [
            'type' => 'leg',
            'match' => 'MY.leg'
        ],
        'Havaintolähde' => [
            'type' => 'recordBasis',
            'match' => 'MY.recordBasis'
        ],
        'Lähteen selite' => [
            'type' => 'recordSource'
        ],
        'Kokoelma' => [
            'type' => 'abbr',
            'match' => 'MOS.abbreviation'
        ],
        
        'Kokoelman nimi' => [
            'type' => 'collectionAbbreviation',
            'match' => 'MY.collectionID'
        ],
        
        
        //'Muu kokoelma' => self::SKIP,
        'Keruutunnus' => [
            'type' => self::VALUE_AS_IS,
            'fields' => ['MYLegID'],
            'match' => 'MY.legID'
        ],
        'Näytteen tunnus' => [
            'type' => 'uri',
            'match' => 'subject'
        ],
        'Määrittäjä' => [
            'type' => 'det',
            'level' => 'identification',
            'fields' => ['MYDet', 'MYDetDate'],
            'match' => 'MY.det, MY.detDate'
        ],
        'Määrä' => [
            'type' => 'amount',
            'match' => 'MY.count'
        ],
        'Määrän yksikkö' => [
            'type' => 'lifestage',
//            'type' => self::VALUE_AS_IS,
//            'level' => 'unit',
//            'fields' => ['MYLifeStage']
            'match' => 'MY.lifeStage'
        ],
        'Sukupuoli' => [
            'type' => 'sex',
            'match' => 'MY.sex'
        ],
        'Havainnon kuvaus' => self::SKIP,
        'Havainnon lisätieto' => [
            'type' => self::VALUE_AS_IS,
            'fields' => ['MYNotes'],
            'match' => 'MY.notes'
        ],
        'Isäntälaji' => [
//            'type' => 'host'
            'type' => self::VALUE_AS_IS,
            'level' => 'gathering',
            'fields' => ['MYSubstrate'],
            'match' => 'MY.substrate'
        ],
//        'Seuralaislajit' => [
//            'type' => 'following'
//        ],
        'Omistaja' => [
            'type' => self::VALUE_AS_IS,
            'level' => 'document',
            'fields' => ['MZOwner'],
            'match' => 'MZ.owner'
        ],
        'Tallentaja' => [
            'type' => 'name',
            'fields' => ['MZCreator', 'MZEditor', 'MYEditor'],
            'match' => '[several]',
        ],
        'Huom' => [
            'type' => 'unreliable',
        ],
        'Näytteen lisätiedot' => [
            'type' => self::VALUE_AS_IS,
            'level' => 'unit',
            'fields' => ['MYNotes'],
            'match' => 'MY.notes'
        ],
        'Eliömaakunta' => [
            'type' => 'bioGeographic',
            'match' => 'MY.biologicalProvince'
        ],
        //'Suojelualue' => self::SKIP,
        //'Natura-alue' => self::SKIP,
    ];

    protected $amountUnits = [
        'FU' => 'itiöemä',
        'BR' => 'yksilö'
    ];

    protected $abbrCache = [];

    protected $cols;
    protected $conversions = [];
    /** @var  ConverterService */
    protected $converterService;
    /** @var  CoordinateService */
    protected $coordinateService;
    protected $municipalityService;
    protected $collectionService;
    protected $specimenService;
    protected $specimenCache = [];
    private $countAnalyzer;

    protected $skippedRecordTypes = [
        'MY.recordBasisHumanObservation',
        'MY.recordBasisDrawing',
        'MY.recordBasisVideo',
        'MY.recordBasisPhoto',
        'MY.recordBasisLiterature',
    ];

    public function __construct()
    {
        $this->countAnalyzer = new Count();
    }

    public function convertSpecimenArrayToHerttaSheet($data)
    {
        /** @var \Kotka\Service\FormElementService $formElementService */
        $formElementService = $this->getServiceLocator()->get('Kotka\Service\FormElementService');
        $this->converterService = new ConverterService();
        $this->coordinateService = $this->getServiceLocator()->get('Kotka\Service\CoordinateService');
        $this->conversions['name'] = $formElementService->getSelect('MZ.editor');
        $cnt = 0;
        $sheet = $this->getSheet();
        $identificationIterator = new IdentificationIterator();
        $identificationIterator->setSkippedRecordTypes($this->skippedRecordTypes);
        foreach ($data as $document) {
            $identificationIterator->init($document);
            foreach ($identificationIterator as list($document, $gathering, $unit, $identification, $type)) {
                foreach ($this->fields as $field => $instruction) {
                    $value = $this->extractValue($instruction, $document, $gathering, $unit, $identification, $type);
                    if ($value !== false) {
                        /** @var ExcelColumn $col */
                        $col = $this->cols[$field];
                        $col->setData($value, $cnt);
                    }
                }
                $cnt++;
            }
        }
        //var_dump($sheet->toArray());exit();
        return $sheet;
    }

    protected function extractValue($instruction, $document, $gathering, $unit, $identification, $type)
    {
        if ($instruction === self::SKIP) {
            return false;
        } elseif (is_array($instruction)) {
            switch ($instruction['type']) {
                case 'uri':
                    return IdService::getUri($document['subject']);
                    break;
                /** @noinspection PhpMissingBreakStatementInspection */
                case 'det':
                    if (isset($identification['MYDet'])) {
                        $identification['MYDet'] = $this->nameConversion($identification['MYDet']);
                    }
                    if (isset($identification['MYDetDate'])) {
                        preg_match('/\b(\d{4})\b/', $identification['MYDetDate'], $matches);
                        if (isset($matches[0])) {
                            $identification['MYDetDate'] = $matches[0];
                        }
                    }
                /** @noinspection PhpMissingBreakStatementInspection */
                case 'tax':
                    if (isset($identification['MYAuthor'])) {
                        $identification['MYAuthor'] = trim(preg_replace("/[0-9]+/i", "", $identification['MYAuthor']));
                    }
                case self::VALUE_AS_IS:
                    $level = $this->getLevels($instruction);
                    $delimiter = isset($instruction['delimiter']) ? $instruction['delimiter'] : ' ';
                    $subDelimiter = isset($instruction['subDelimiter']) ? $instruction['subDelimiter'] : ' ';
                    $value = $this->concatenate($instruction['fields'], $level, $delimiter, $subDelimiter, $document, $gathering, $unit, $identification, $type);
                    break;
                case 'MXCode':
                    $taxon = $this->getTaxonData($identification['MYTaxon']);
                    if ($taxon instanceof TaxonResult) {
                        $value = $taxon->getQName();
                    }
                    break;
                case 'name':
                    $levels = $this->getLevels($instruction);
                    $value = $this->first($instruction['fields'], $levels, $document, $gathering, $unit, $identification, $type);
                    if (isset($this->conversions['name'][$value])) {
                        $value = $this->nameConversion($this->conversions['name'][$value]);
                    }
                    break;
                case self::FIRST_EXISTING:
                    $levels = $this->getLevels($instruction);
                    $value = $this->first($instruction['fields'], $levels, $document, $gathering, $unit, $identification, $type);
                    break;
                case 'leg':
                    if (isset($gathering['MYLeg'])) {
                        $legs = [];
                        foreach ($gathering['MYLeg'] as $leg) {
                            $legs[] = $this->nameConversion($leg);
                        }
                        return implode(', ', $legs);
                    }
                    break;
                case 'recordBasis':
                    if (isset($unit['MYRecordBasis'])) {
                        return $unit['MYRecordBasis'] === 'MY.recordBasisPreservedSpecimen' ? 'N' : 'M';
                    }
                    break;
                case 'recordSource':
                    if (isset($unit['MYRecordBasis']) && $unit['MYRecordBasis'] !== 'MY.recordBasisPreservedSpecimen') {
                        $value = 'maastohavainto';
                    }
                    break;
                case 'amount':
                    if (isset($unit['MYCount']) && is_numeric($unit['MYCount'])) {
                        return $unit['MYCount'];
                    }
                    break;
                case 'constant':
                    return $instruction['value'];
                    break;
                case 'bioGeographic':
                    if (isset($gathering['MYBiologicalProvince'])) {
                        $value = $this->converterService->get('bioGeographic', $gathering['MYBiologicalProvince'], $gathering['MYBiologicalProvince'], false, 0);
                    }
                    break;
                case 'sex':
                    if (isset($unit['MYSex'])) {
                        $value = $this->converterService->get('sex', substr($unit['MYSex'],-1), substr($unit['MYSex'],-1), false, 0);
                    }
                    break;
                case 'lifestage':
                    if (isset($unit['MYLifeStage'])) {
                        $value = substr($unit['MYLifeStage'],12);
                    }
                    break;
                case 'locality':
                    if (substr($gathering['MYLocalityID'],0,7) === 'hertta:') {
                        $value = substr($gathering['MYLocalityID'],7);
                    } else {
                        $value = $gathering['MYLocality'];
                    }
                    break;
                case 'dateStatus':
                    if (isset($gathering['MYDateBegin'])) {
                        preg_match('/\b(\d{4})\b/', $gathering['MYDateBegin'], $matches);
                        if (isset($matches[0])) {
                            return (((int)$matches[0]) < 1980) ? '?' : '+';
                        }
                    }
                    break;
 
// "case host" is old code, maybe not needed after changes in Apr 2016 /dare
 
                case 'host':
                    if (isset($document['MYRelationship'])) {
                        $relations = $document['MYRelationship'];
                        foreach($relations as $relation) {
                            if (strpos($relation, 'host:') !== false) {
                                $host = trim(str_replace('host:', '', $relation));
                                $helper = $this->getServiceLocator()->get('viewhelpermanager')->get('ResolveHostName');
                                $value = $helper($host);
                                break;
                            }
                        }
                    }
                    break;
                case 'coordinate':
                    if (isset($gathering['MYLatitude']) && isset($gathering['MYLongitude']) && isset($gathering['MYCoordinateSystem'])) {
                        $result = $this->coordinateService->convertToYKJ(
                            $gathering['MYLatitude'],
                            $gathering['MYLongitude'],
                            $gathering['MYCoordinateSystem']
                        );
                        $value = $result[$instruction['index']];
                    }
                    break;
                case 'municipality':
                    if (isset($gathering['MYMunicipality'])) {
                        $ms = $this->getMunicipalityService();
                        $value = $ms->getCurrentMunicipality($gathering['MYMunicipality'], $gathering['MYMunicipality']);
                    }
                    break;
                case 'collectionAbbreviation':
                    if (isset($document['MYCollectionID'])) {
                        $cs = $this->getCollectionService();
                        $value = $cs->getCollectionAbbreviation($document['MYCollectionID']);
                    }
                    break;

                case 'collectionName':
                    if (isset($document['MYCollectionID'])) {
                        $cs = $this->getCollectionService();
                        $value = $cs->getCollectionName($document['MYCollectionID']);
                    }
                    break;


                case 'unreliable':
                    $value = '<sekundääridataa Kotkasta, ei saa muokata>';
                    if (isset($document['MYUnreliableFields']) && !empty($document['MYUnreliableFields'])) {
                        $value = 'This specimen has some unreliable fields. ' . $value;
                    }
                    break;
                case 'taxonGroup':
                    $taxon = $this->getTaxonData($identification['MYTaxon']);
                    if ($taxon instanceof TaxonResult) {
                        $value = $taxon->getTaxonGroupAbbr();
                    }
                    break;
                case 'amountUnit':
                    if (empty($unit['MYCount'])) {
                        $value = '1';
                    } else {
                        $value = $this->countAnalyzer->refresh($unit['MYCount'])->getTotal();
                        $value = $value === 0 ? 1 : $value;
                    }
                    $taxon = $this->getTaxonData($identification['MYTaxon']);
                    if ($taxon instanceof TaxonResult) {
                        $abbr = $taxon->getTaxonGroupAbbr();
                        if (isset($this->amountUnits[$abbr])) {
                            $value .= ' ' .$this->amountUnits[$abbr];
                        }
                    }
                    break;
                case 'following':
                    if (count($gathering['MYUnit']) > 1) {
                        $identifications = $this->getAllIdentifications($gathering);
                        $taxon = $identification['MYTaxon'];
                        if (isset($identifications[$taxon])) {
                            unset($identifications[$taxon]);
                        }
                        $value = implode(', ', array_keys($identifications));
                    }
                    break;
                case 'abbr':
                    $value = '';
                    if (isset($document['MZOwner'])) {
                        if (!isset($this->abbrCache[$document['MZOwner']])) {
                            $organization = $this
                                ->getServiceLocator()
                                ->get('Kotka\Service\OrganizationService')
                                ->getById($document['MZOwner']);
                            $this->abbrCache[$document['MZOwner']] = $organization instanceof MOSOrganizationInterface && !empty($organization->getMOSAbbreviation()) ?
                                    $organization->getMOSAbbreviation() : '';
                        }
                        $value = $this->abbrCache[$document['MZOwner']];
                    }
                    if (empty($value)) {
                        if ($document['MYDatatype'] === 'zoospecimen') {
                            $value = 'MUU-E';
                        } elseif ($document['MYDatatype'] === 'botanyspecimen') {
                            $value = 'MUU-K';
                        }
                    }
                    break;
                default:
                    throw new \Exception('Could not find type ' . $instruction['type']);
            }
            if (!empty($value)) {
                return $value;
            }
        }
        return false;
    }

    private function getAllIdentifications($gathering)
    {
        $result = [];
        if (isset($gathering['MYUnit'])) {
            foreach ($gathering['MYUnit'] as $unit) {
                if (isset($unit['MYIdentification']) &&
                    isset($unit['MYIdentification'][0]) &&
                    isset($unit['MYIdentification'][0]['MYTaxon'])
                ) {
                    $result[$unit['MYIdentification'][0]['MYTaxon']] = true;
                }
            }
        }
        return $result;
    }

    private function getLevels($instruction)
    {
        $levels = ['identification', 'type', 'unit', 'gathering', 'document'];
        if (isset($instruction['levels'])) {
            if (!is_array($instruction['levels'])) {
                $instruction['levels'] = [$instruction['levels']];
            }
            $levels = $instruction['levels'];
        }
        return $levels;
    }

    private function nameConversion($name)
    {
        if (strpos($name, ',') !== false) {
            $parts = explode(',', $name, 2);
            return trim($parts[0]) . ' ' . trim($parts[1]);
        }
        return $name;
    }

    protected function first($fields, $levels, $document, $gathering, $unit, $identification, $type)
    {
        foreach ($levels as $level) {
            $domain = $$level;
            foreach ($fields as $field) {
                if (isset($domain[$field])) {
                    return $domain[$field];
                }
            }
        }
        return false;
    }

    protected function concatenate($fields, $levels, $delimiter, $subDelimiter, $document, $gathering, $unit, $identification, $type)
    {
        $values = [];
        if (!is_array($fields)) {
            $fields = [$fields];
        }
        foreach ($levels as $level) {
            $domain = $$level;
            foreach ($fields as $field) {
                if (isset($domain[$field])) {
                    if (is_array($domain[$field])) {
                        $values[] = implode($subDelimiter, $domain[$field]);
                    } else {
                        $values[] = $domain[$field];
                    }
                }
            }
        }
        return implode($delimiter, $values);
    }

    protected function getMunicipalityService()
    {
        if ($this->municipalityService === null) {
            $this->municipalityService = new MunicipalityService();
            $this->municipalityService->setServiceLocator($this->getServiceLocator());

        }
        return $this->municipalityService;
    }

    /**
     * @return CollectionService
     */
    protected function getCollectionService()
    {
        if ($this->collectionService === null) {
            $this->collectionService = $this->getServiceLocator()->get('Kotka\Service\CollectionService');
        }
        return $this->collectionService;
    }

    protected function getSheet()
    {
        $sheet = new ExcelSheet();
        foreach ($this->fields as $field => $instruction) {
            $col = new ExcelColumn();
            $col->setLabel($field);
            if (isset($instruction['match'])) {
                $col->setFullField($instruction['match']);
            }
            $sheet->addColumn($col);
            $this->cols[$field] = $col;
        }
        return $sheet;
    }

    protected function getTaxonData($scientificName)
    {
        if (isset($this->specimenCache[$scientificName])) {
            return $this->specimenCache[$scientificName];
        }
        $sc = $this->getSpecimenService();
        $this->specimenCache[$scientificName] = $sc->getTaxonData($scientificName);

        return $this->specimenCache[$scientificName];
    }

    /**
     * @return SpecimenService
     */
    protected function getSpecimenService()
    {
        if ($this->specimenService === null) {
            $this->specimenService = $this
                ->getServiceLocator()
                ->get('Kotka\Service\SpecimenService');
        }
        return $this->specimenService;
    }

}