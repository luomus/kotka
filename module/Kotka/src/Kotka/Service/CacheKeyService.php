<?php
namespace Kotka\Service;

use Kotka\Controller\PickerController;
use Zend\Mvc\Router\RouteMatch;

class CacheKeyService
{

    const REFRESH_PREFIX = 'refreshOn';
    const FILE_CACHE = 'file';

    const METADATA = 'metadata';

    const COLLECTION_INDEX = 'https/collections-index';
    const TRANSACTION_INDEX = 'https/transactions-index';
    const DATASET_INDEX = 'https/datasets-index';
    const DATASET_ADD = 'https/datasets-add';
    const ORGANIZATIONS_INDEX = 'https/organizations-index';

    private static $cache = array(
        self::METADATA => 'metadata',
        self::COLLECTION_INDEX => 'page-collection-index',
        self::TRANSACTION_INDEX => 'page-transaction-index',
        self::DATASET_INDEX => 'page-datasets-index',
        self::ORGANIZATIONS_INDEX => 'page-organizations-index',
    );

    public static function getCacheKeyFromRoute(RouteMatch $match)
    {
        $key = $match->getMatchedRouteName() . '-' . $match->getParam('action');
        return CacheKeyService::getCacheKey($key);
    }

    public static function getCacheKey($key)
    {
        if (!isset(self::$cache[$key])) {
            throw new \Exception("Cache key '$key' is not defined!\nYou might want to check that the key exists in the CacheKeyService.");
        }
        return self::$cache[$key];
    }

    public function refreshOnGXDataset()
    {
        return array(
            self::$cache[self::DATASET_INDEX],
            FormElementService::CACHE_PREFIX . 'MY.datasetID',
            FormElementService::CACHE_PREFIX . 'MF.datasetID',
            FormElementService::CACHE_PREFIX . 'MOS.datasetID',
            self::FILE_CACHE => array(),
        );
    }

    public function refreshOnMYCollection()
    {
        return array(
            self::$cache[self::COLLECTION_INDEX],
            FormElementService::CACHE_PREFIX . 'MY.collectionID',
            FormElementService::CACHE_PREFIX . 'HRA.collectionID',
            FormElementService::CACHE_PREFIX . 'MF.collectionID',
            FormElementService::CACHE_PREFIX . 'MY.collectionID-GardenArea',
            self::FILE_CACHE => array(),
        );
    }

    public function refreshOnMOSOrganization()
    {
        return array(
            self::$cache[self::ORGANIZATIONS_INDEX],
            PickerController::PICKER_CACHE_PREFIX . 'string',
            FormElementService::CACHE_PREFIX . 'HRA.correspondentOrganization',
            FormElementService::CACHE_PREFIX . 'PUU.seedsExchangedInstitution',
            FormElementService::CACHE_PREFIX . 'MZ.owner',
            FormElementService::CACHE_PREFIX . 'MZ.ownerID',
            FormElementService::CACHE_PREFIX . 'MY.acquiredFromOrganization',
            FormElementService::CACHE_PREFIX . 'MY.acquiredFromOrganizationID',
            self::FILE_CACHE => array(),
        );
    }

    public function refreshOnHRATransaction()
    {
        return array(
            self::$cache[self::TRANSACTION_INDEX],
            self::FILE_CACHE => array(),
        );
    }

} 