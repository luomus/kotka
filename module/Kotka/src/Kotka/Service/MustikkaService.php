<?php

namespace Kotka\Service;

use Api\Service\ImageService;
use Common\Service\IdService;
use Kotka\Model\DataTypes;
use Kotka\Options\ExternalsOptions;
use Sabre\Xml\Reader;
use Sabre\Xml\Service;
use Sabre\Xml\Writer;
use Zend\Http\Client;
use Zend\Http\Request;
use Zend\Log\LoggerInterface;

/**
 * Class MustikkaService communicates between kotka and mustikka
 *
 * @package Kotka\Service
 */
class MustikkaService
{
    const DATETIME_FORMAT = 'U';

    private $external;
    private $logger;
    private $imageService;
    private $colectionService;

    private $client;

    /**
     * Initializes the object
     *
     * @param ExternalsOptions $externals
     * @param LoggerInterface $logger
     * @param ImageService $imageService
     */
    public function __construct(ExternalsOptions $externals, LoggerInterface $logger, ImageService $imageService, CollectionService $collectionService)
    {
        $this->external = $externals;
        $this->logger = $logger;
        $this->imageService = $imageService;
        $this->colectionService = $collectionService;
    }

    /**
     * Sends specimen to the mustikka.
     *
     * @param string $subject
     * @param null|string $user qname
     * @return bool true on success
     */
    public function sendSpecimen($subject, $user = null)
    {
        $userId = is_string($user) && strpos($user, 'MA.') !== false ?
            str_replace('MA.', '', $user) : 0;

        $client = $this->getHttpClient();
        $mustikka = $this->external->getMustikka();
        $uri = sprintf($mustikka->getUrl(), $subject, $userId);
        $client->setUri($uri);
        $client->setAuth($mustikka->getUser(), $mustikka->getPass(), Client::AUTH_BASIC);
        $client->setMethod(Request::METHOD_POST);

        try {
            $response = $client->send();
        } catch (\Exception $e) {
            $this->logger->warn("Failed to send: " . $subject . " error message:\n" . $e->getMessage());
            return false;
        }
        return $response->isSuccess();
    }

    public function sendSpecimens($subjects)
    {
        if (empty($subjects) || !is_array($subjects)) {
            return false;
        }
        $client = $this->getHttpClient();
        $client->resetParameters();
        $triple = $this->external->getTriplestoreApi();
        $url = $triple->getUrl() . '/%s?resulttype=tree';
        $uri = sprintf($url, str_replace(IdService::DEFAULT_DB_QNAME_PREFIX, '', implode('+', $subjects)));
        $client->setUri($uri);
        $client->setAuth($triple->getUser(), $triple->getPass(), Client::AUTH_BASIC);
        try {
            $response = $client->send();
        } catch (\Exception $e) {
            $this->logger->warn("Failed to fetch RDF XML from triplestore api error message:\n" . $e->getMessage());
            return false;
        }
        if (!$response->isOk()) {
            $this->logger->warn(
                "Status code from triplestore api was something else than expected:\n" .
                "response body: " . $response->getBody() . "\n" .
                "response code: (" . $response->getStatusCode() . ")"
            );
            return false;
        }
        try {
            $xml = $this->analyzeXml($response->getBody());
        } catch (\Exception $e) {
            $this->logger->crit(
                "Failed to prepare document for sending:\n" .
                "message: " . $e->getMessage() . "\n" .
                "trace: (" . $e->getTraceAsString() . ")"
            );
            return false;
        }
        if ($xml === false) {
            return true;
        }

        $client->resetParameters();
        $lajiEtl = $this->external->getLajiETL();
        $client->setUri($lajiEtl->getUrl());
        $client->setHeaders(['Authorization' => $lajiEtl->getPass()]);
        $client->setMethod(Request::METHOD_POST);
        $client->setRawBody($xml);
        $client->setEncType('application/rdf+xml;charset=utf-8');
        try {
            $response = $client->send();
        } catch (\Exception $e) {
            $this->logger->warn("Failed to send data to laji-ETL:\n" . $e->getMessage());
            return false;
        }
        if ($response->getStatusCode() !== 200) {
            $this->logger->warn(
                "When sending status code from laji-ETL was something else than expected:\n" .
                "response body: " . $response->getBody() . "\n" .
                "response code: (" . $response->getStatusCode() . ")"
            );
            return false;
        }
        return true;
    }

    public function getHttpClient()
    {
        if ($this->client === null) {
            $this->client = new Client(
                null,
                array(
                    'adapter' => 'Zend\Http\Client\Adapter\Curl',
                    'sslverifypeer' => false
                )
            );
        }
        return $this->client;
    }

    public function deleteSpecimen($uri)
    {
        if (empty($uri)) {
            return true;
        }
        $lajiEtl = $this->external->getLajiETL();

        $client = $this->getHttpClient();
        $client->setUri($lajiEtl->getUrl());
        $client->setHeaders(['Authorization' => $lajiEtl->getPass()]);
        $client->setMethod(Request::METHOD_DELETE);
        try {
            $response = $client->setParameterGet([
                'documentId' => IdService::getUri($uri)
            ])->send();
        } catch (\Exception $e) {
            $this->logger->warn("Failed to delete data from laji-ETL:\n" . $e->getMessage());
            return false;
        }
        if ($response->getStatusCode() !== 200) {
            $this->logger->warn(
                "When deleting status code from laji-ETL was something else than expected:\n" .
                "response body: " . $response->getBody() . "\n" .
                "response code: (" . $response->getStatusCode() . ")"
            );
            return false;
        }
        return true;
    }

    private function analyzeXml($xml) {
        $namespaceMap = IdService::getAllNS(true, true);
        $reader = new Reader();
        $reader->namespaceMap = $namespaceMap;
        $reader->xml($xml);
        $data = $reader->parse();

        $check = $this->analyzeDocuments($data);
        if ($check === false) {
            return false;
        }

        $writer = new Writer();
        $writer->namespaceMap = $reader->namespaceMap;
        $writer->openMemory();
        $writer->setIndent(false);
        $writer->startDocument('1.0', 'utf-9');
        $writer->write($data);
        $result = $writer->outputMemory();
        $result = "<?xml version='1.0' encoding='utf-8'?>\n" . $result;
        return $result;
    }

    private function analyzeDocuments(&$data) {
        if (isset($data['name']) && $data['name'] === '{http://tun.fi/}MY.document') {
            if ($this->belongsToHiddenCollection($data['value']) || !$this->shouldSendDocument($data['value'])) {
                return false;
            }
            $this->removePrivateNotes($data['value']);
            $this->addPicture($data['value'], $data['attributes']['{http://www.w3.org/1999/02/22-rdf-syntax-ns#}about']);
            return true;
        }
        IF (is_array($data) && isset($data['value'])) {
            foreach($data['value'] as $key => &$value) {
                if ($this->analyzeDocuments($value) === false) {
                    unset($data['value'][$key]);
                }
            }
            if (count($data['value']) === 0) {
                return false;
            }
        }
        return true;
    }

    private function removePrivateNotes(&$document) {
        if (!is_array($document)) {
            return;
        }
        foreach ($document as $idx => $item) {
            if ($item['name'] === '{http://tun.fi/}MY.privateNotes') {
                array_splice($document, $idx, 1);
            }
        }
    }

    private function addPicture(&$document, $id) {
        $pics = $this->imageService->getImages($id);
        if (!isset($pics[0]) || !isset($pics[0]['num_images']) || $pics[0]['num_images'] == 0) {
            return;
        }
        $pics = array_pop($pics);
        if (!isset($pics['images']) || count($pics['images']) === 0) {
            return;
        }
        $images = [];
        $imageMap = [
            'id' => 'MM.imageID',
            'thumbnail' => 'MM.thumbnailURL',
            'web_url' => 'MM.fullURL',
            'square_thumbnail' => 'MM.squareThumbnailURL',
            'large' => 'MM.largeURL',
            'tags' => 'MM.keyword',
            'license' => 'MZ.intellectualRights',
            'licenseID' => 'MZ.intellectualRightsID',
            'copyright_owner' => 'MZ.intellectualOwner',
            'photographer_name' => 'MM.capturerVerbatim'
        ];
        foreach($pics['images'] as $image) {
            $mmImage = [];
            foreach($imageMap as $key => $value) {
                if (isset($image[$key]) && !empty($image[$key])) {
                    if (!is_array($image[$key])) {
                        $image[$key] = [$image[$key]];
                    }
                    foreach($image[$key] as $v) {
                        $mmImage[] = [
                            'name' => $value,
                            'value' => $v
                        ];
                    }
                }
            }
            $images[] = [
                'name' => 'MM.image',
                'value' => $mmImage
            ];
        }
        $document[] = [
            'name' => 'MZ.images',
            'value' => $images
        ];
    }

    private function shouldSendDocument($document) {
        foreach ($document as $set) {
            if (isset($set['name']) && $set['name'] === '{http://tun.fi/}MY.datatype') {
                return $set['value'] !== DataTypes::ACCESSION;
            }
        }
        return true;
    }

    private function belongsToHiddenCollection($document) {
        foreach ($document as $set) {
            if (isset($set['name']) && $set['name'] === '{http://tun.fi/}MY.collectionID') {
                $collection = $this->colectionService->getById($set['attributes']['{http://www.w3.org/1999/02/22-rdf-syntax-ns#}resource']);
                if ($collection === null || $collection->getMYMetadataStatus() === 'MY.metadataStatusHidden') {
                    return true;
                }
            }
        }
        return false;
    }
}
