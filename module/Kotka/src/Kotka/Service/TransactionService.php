<?php

namespace Kotka\Service;

use Common\Service\IdService;
use Kotka\Mapper\HRATransactionMapperInterface;
use Kotka\Triple\HRATransaction;
use Kotka\Triple\KotkaEntityInterface;
use Kotka\Triple\MAPerson;
use Triplestore\Classes\HRAPermitInterface;
use Triplestore\Classes\HRATransactionInterface;
use Triplestore\Model\Model;
use Triplestore\Service\PhpVariableConverter;
use Triplestore\Stdlib\OntologyInterface;

/**
 * Class TransactionService contains the business logic for transactions.
 * This should be the only place where transactions are handled!
 *
 * This service inherits few common methods from DocumentService
 *
 * @package Kotka\Service
 */
class TransactionService extends DocumentService implements DocumentServiceInterface
{
    const TMP_FILE_PREFIX = "__tmp_file__";
    const TMP_FILE_INFO_SEP = "_____";

    /** @var \Kotka\Mapper\HRATransactionMapperInterface */
    protected $transactionMapper;
    protected $user;
    /** @var \Kotka\Service\SequenceService */
    protected $seqService;
    /** @var \Kotka\Service\FileService */
    protected $fileService;

    /**
     * Initializes the object
     * @param FileService $fileService
     * @param SequenceService $seqService
     * @param HRATransactionMapperInterface $transactionMapper
     * @param MAPerson $authenticationService
     * @param $idGenerator
     */
    public function __construct(
        FileService $fileService,
        SequenceService $seqService,
        HRATransactionMapperInterface $transactionMapper,
        MAPerson $authenticationService,
        $idGenerator
    )
    {
        $this->seqService = $seqService;
        $this->fileService = $fileService;
        $this->transactionMapper = $transactionMapper;
        parent::__construct($authenticationService, $idGenerator);
    }

    /**
     * Return all the transactions
     * @param $fields
     * @return array of transactions
     */
    public function getAll(array $fields = null)
    {
        return $this->transactionMapper->findBy(array(), null, null, null, $fields);
    }

    /**
     * Gets transaction based on the uri
     * @param $id
     * @return HRATransaction
     */
    public function getById($id)
    {
        /** @var HRATransaction $transaction */
        return $this->transactionMapper->find(explode('#', $id)[0]);
    }

    /**
     * Stores the new transaction to the database
     *
     * @param KotkaEntityInterface $transaction
     * @return HRATransaction
     * @throws Exception\DomainException
     */
    public function create($transaction)
    {
        if (!$transaction instanceof HRATransaction) {
            throw new Exception\DomainException('Transaction service can\'t handle element of type ' . get_class($transaction));
        }
        $this->setId($transaction);
        $this->setEdit($transaction, true);
        $this->prepareChildren($transaction, $transaction->getSubject());
        $this->prepareLoan($transaction);

        return $this->transactionMapper->create($transaction);
    }

    /**
     * Updates existing transaction to the database
     *
     * @param $transaction
     * @return HRATransaction|mixed
     * @throws Exception\DomainException
     */
    public function update($transaction)
    {
        if (!$transaction instanceof HRATransaction) {
            throw new Exception\DomainException('Transaction service can\'t handle element of type ' . get_class($transaction));
        }
        $this->setEdit($transaction);
        $this->prepareChildren($transaction, $transaction->getSubject());
        $this->prepareLoan($transaction);
        return $this->transactionMapper->update($transaction);
    }

    public function storeTmpFile($filename, $currentLocation, $place, $id, $fromUpload = true) {
        $this->fileService->setSubDirectory($place);
        if (!empty($filename) && !$this->fileService->store($id, $filename, $currentLocation, $fromUpload)) {
            throw new \Exception('Failed to save permit document');
        }
        return $filename;
    }

    /**
     * This creates the linkings beatween the document and it's child elements.
     *
     * @param $document
     * @param $root
     * @param null $parent
     * @throws \Exception
     */
    private function prepareChildren($document, $root, $parent = null)
    {
        if ($parent === null) {
            $parent = $root;
        }
        if ($document instanceof OntologyInterface) {
            $subject = $document->getSubject();
            if ($subject === null || $subject === '') {
                if ($document instanceof HRAPermitInterface) {
                    $document->setSubject($this->getSubId($root));
                    // $document->setMZDateCreated(new \DateTime());
                } else {
                    $this->setId($document, false);
                }
            }
            if ($document instanceof HRAPermitInterface) {
                $document->setMYIsPartOf($parent);
                $file = $document->getHRAPermitFile();
                if (is_array($file) && isset($file['name'])) {
                    $document->setHRAPermitFile($this->storeTmpFile($file['name'], $file['tmp_name'], 'permits', $document->getSubject()));
                } else if (is_string($file) && strpos($file, TransactionService::TMP_FILE_PREFIX) === 0) {
                    $this->fileService->setSubDirectory('tmp');
                    $parts = explode(TransactionService::TMP_FILE_INFO_SEP, $file);
                    $name = array_pop($parts);
                    $dir = $this->fileService->getDirectory(str_replace(TransactionService::TMP_FILE_PREFIX, '', $parts[0]));
                    $document->setHRAPermitFile($this->storeTmpFile($name, $dir . $file, 'permits', $document->getSubject(), false));
                }
            }
        }

        $childMethods = null;
        if ($document instanceof HRATransactionInterface) {
            $childMethods = array('getHRAPermit');
        } else {
            return;
        }

        foreach ($childMethods as $childMethod) {
            $has = $document->$childMethod();
            if (is_array($has)) {
                foreach ($has as $doc) {
                    $this->prepareChildren($doc, $root, $parent);
                }
            } else {
                $this->prepareChildren($has, $root, $parent);
            }

        }
    }

    public function getSubId($root) {
        $start = IdService::getQName($root, true) . "#";
        return $start . $this->seqService->next($start);
    }

    private function prepareLoan(HRATransaction $transaction)
    {
        $method = 'set' . PhpVariableConverter::toPhpMethod(KotkaEntityInterface::RESTRICTION_PREDICATE);
        $transaction->$method(KotkaEntityInterface::RESTRICTION_PRIVATE);
    }

    /**
     * Returns class name
     * @return string class name
     */
    public function getClassName()
    {
        return $this->transactionMapper->getClassName();
    }

    /**
     * Return the loan qname that has the item on loan or null if item is not loaned
     *
     * @param string $subject
     * @return null|string
     */
    public function isLoaned($subject)
    {
        $results = $this->transactionMapper->findBy(array('HRB.away' => IdService::getQName($subject, true)), null, null, null, array('HRA.transactionType'));
        if (count($results) == 0) {
            $results = $this->transactionMapper->findBy(array('HRB.away' => IdService::getUri($subject)), null, null, null, array('HRA.transactionType'));
        }
        if (count($results) == 0) {
            $results = $this->transactionMapper->findBy(array('HRB.away' => IdService::getQNameWithoutPrefix($subject)), null, null, null, array('HRA.transactionType'));
        }
        if (count($results) == 0) {
            $results = $this->transactionMapper->findBy(array('HRB.away' => IdService::getQName($subject)), null, null, null, array('HRA.transactionType'));
        }
        if (count($results) == 0) {
            return null;
        }
        foreach ($results as $subject => $model) {
            $value = '';
            if ($model instanceof Model) {
                $predicate = $model->getPredicate('HRA.transactionType');
                $value = $predicate->getFirst()->getValue();
            } elseif ($model instanceof HRATransaction) {
                $value = $model->getHRATransactionType();
            }
            if ($value === 'HRA.transactionTypeLoanOutgoing') {
                return $subject;
            }
        }
        return null;
    }

    public function findTransactionsWithDocument($subject) {
        $fields = ['HRA.transactionType', 'HRA.transactionStatus', 'HRA.transactionRequestReceived', 'HRA.availableForGeneticResearch', 'HRA.geneticResourceRightsAndObligations'];
        $results = [];
        $rowToData = function(HRATransactionInterface $row, $type) {
            $typeParts = explode('.', $type);
            $type = array_pop($typeParts);
            return array_filter([
                'uri' => IdService::getUri($row->getSubject()),
                'type' => $type,
                'HRA.transactionType' => $row->getHRATransactionType(),
                'HRA.transactionStatus' => $row->getHRATransactionStatus(),
                'HRA.transactionRequestReceived' => $row->getHRATransactionRequestReceived() ? $row->getHRATransactionRequestReceived()->format('Y-m-d') : '',
                'HRA.availableForGeneticResearch' => $row->getHRAAvailableForGeneticResearch(),
                'HRA.geneticResourceRightsAndObligations' => $row->getHRAGeneticResourceRightsAndObligations()
            ]);
        };
        $fetchData = function($type) use ($subject, $rowToData, $fields, &$results) {
            foreach ($this->transactionMapper->findBy(array($type => IdService::getQName($subject, true)), null, null, null, $fields) as $row) {
                $results[] = $rowToData($row, $type);
            }
            foreach ($this->transactionMapper->findBy(array($type => IdService::getUri($subject)), null, null, null, $fields) as $row) {
                $results[] = $rowToData($row, $type);
            }
            foreach ($this->transactionMapper->findBy(array($type => IdService::getQNameWithoutPrefix($subject)), null, null, null, $fields) as $row) {
                $results[] = $rowToData($row, $type);
            }
            foreach ($this->transactionMapper->findBy(array($type => IdService::getQName($subject)), null, null, null, $fields) as $row) {
                $results[] = $rowToData($row, $type);
            }
        };

        $fetchData('HRB.away');
        $fetchData('HRB.returned');

        usort($results, function($a, $b) {
            return strcmp($b['HRA.transactionRequestReceived'], $a['HRA.transactionRequestReceived']);
        });
        return $results;
    }
}
