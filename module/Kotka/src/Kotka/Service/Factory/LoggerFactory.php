<?php

namespace Kotka\Service\Factory;

use Kotka\Log\Writer\TeamCity;
use Kotka\Service\ErrorHandlingService;
use Zend\Log\Filter\Priority;
use Zend\Log\Logger;
use Zend\Log\Writer\Stream;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class LoggerFactory is factory class for creating logger.
 *
 * @package Kotka\Service\Factory
 */
class LoggerFactory implements FactoryInterface
{
    const LOG_LEVEL = Logger::INFO;
    const TC_LOG_LEVEL = Logger::WARN;

    /**
     * Creates the service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return ErrorHandlingService
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $logFile = getenv('DISPLAY_EXCEPTIONS') === 'true' ? 'php://stdout' : './data/logs/' . date('Y-m-d') . '.log';
        $logger = new Logger();
        $writer = new Stream($logFile);

        $logger->addWriter($writer);
        $writer->addFilter(new Priority(self::LOG_LEVEL));

        $config = $serviceLocator->get('Config');
        if (isset($config['logs']) && $config['logs']['teamcity']) {
            $tcWriter = new TeamCity($config['logs']['teamcity']);
            $logger->addWriter($tcWriter);
            $tcWriter->addFilter(new Priority(self::TC_LOG_LEVEL));
        }

        return $logger;
    }
}
