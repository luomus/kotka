<?php

namespace Kotka\Service\Factory;

use Kotka\Service\TransactionService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class TransactionServiceFactory is factory class for creating
 * transaction service.
 *
 * @package Kotka\Service\Factory
 */
class TransactionServiceFactory implements FactoryInterface
{
    /**
     * Creates the service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return TransactionService
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $transactionMapper = $serviceLocator->get('Kotka\Mapper\HRATransactionMapperInterface');
        $authentication = $serviceLocator->get('user');
        $idGen = $serviceLocator->get('Common\Service\IdService');

        return new TransactionService(
            $serviceLocator->get('Kotka\Service\FileService'),
            $serviceLocator->get('Kotka\Service\SequenceService'),
            $transactionMapper,
            $authentication,
            $idGen
        );
    }
}
