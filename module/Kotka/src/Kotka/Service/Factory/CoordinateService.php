<?php
namespace Kotka\Service\Factory;

use Kotka\Http\HttpClientTrait;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class CoordinateService implements FactoryInterface
{
    use HttpClientTrait;

    /**
     * @param ServiceLocatorInterface $serviceLocator
     * @return \Kotka\Service\CoordinateService
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /** @var \Kotka\Options\ExternalsOptions $externals */
        $externals = $serviceLocator->get('Kotka\Options\ExternalsOptions');
        $coordinate = $externals->getCoordinate();
        $client = $this->getHttpClient();
        $url = $coordinate->getUrl();
        $client->setAuth($coordinate->getUser(),$coordinate->getPass());

        return new \Kotka\Service\CoordinateService($client, $url);
    }
}