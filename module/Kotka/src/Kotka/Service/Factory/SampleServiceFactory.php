<?php

namespace Kotka\Service\Factory;

use Kotka\Service\SampleService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class SampleServiceFactory is factory class for creating
 * sample service.
 *
 * @package Kotka\Service\Factory
 */
class SampleServiceFactory implements FactoryInterface
{
    /**
     * Creates the service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return SampleService
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {

        $sampleMapper = $serviceLocator->get('Kotka\Mapper\MFSampleMapperInterface');
        $authentication = $serviceLocator->get('user');
        $idGen = $serviceLocator->get('Common\Service\IdService');

        return new SampleService($sampleMapper, $authentication, $idGen);
    }
}
