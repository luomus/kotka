<?php
namespace Kotka\Service\Factory;

use Kotka\Service\ExcelService;
use Triplestore\Service\ObjectManager;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ExcelServiceFactory implements FactoryInterface
{
    /**
     * @see \Zend\ServiceManager\FactoryInterface::createService()
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $config = $serviceLocator->get('config');
        if (!isset($config['excel']) || !isset($config['excel']['directory'])) {
            throw new \Exception('Missing configuration excel directory');
        }
        /** @var ObjectManager $om */
        $om = $serviceLocator->get('TripleStore/ObjectManager');
        return new ExcelService($config['excel']['directory'], $om->getMetadataService(), $serviceLocator);
    }
}