<?php

namespace Kotka\Service\Factory;

use Kotka\Service\OrganizationService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class OrganizationServiceFactory is factory class for creating
 * organization service.
 *
 * @package Kotka\Service\Factory
 */
class OrganizationServiceFactory implements FactoryInterface
{
    /**
     * Creates the service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return OrganizationService
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $organizationMapper = $serviceLocator->get('Kotka\Mapper\MOSOrganizationMapperInterface');
        $authentication = $serviceLocator->get('user');
        $idGen = $serviceLocator->get('Common\Service\IdService');

        return new OrganizationService($organizationMapper, $authentication, $idGen);
    }
}
