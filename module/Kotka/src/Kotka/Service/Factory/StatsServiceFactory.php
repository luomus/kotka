<?php

namespace Kotka\Service\Factory;

use Kotka\Service\StatsService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class StatsServiceFactory is factory class for creating
 * stats service.
 *
 * @package Kotka\Service\Factory
 */
class StatsServiceFactory implements FactoryInterface
{
    /**
     * Creates the service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return StatsService
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $om = $serviceLocator->get('TripleStore\ObjectManager');
        $cache = $serviceLocator->get('perma-cache');

        return new StatsService($om, $cache);
    }
}
