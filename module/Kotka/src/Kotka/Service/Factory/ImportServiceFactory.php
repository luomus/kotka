<?php

namespace Kotka\Service\Factory;

use Kotka\Service\ImportService;
use Kotka\Service\ValidationService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class ImportServiceFactory is factory class for creating
 * import service.
 *
 * @package Kotka\Service\Factory
 */
class ImportServiceFactory implements FactoryInterface
{
    /**
     * Creates the service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return ImportService
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /** @var ValidationService $validationService */
        $validationService = $serviceLocator->get('Kotka\Service\ValidationService');

        return new ImportService($serviceLocator, $validationService);
    }
}
