<?php

namespace Kotka\Service\Factory;

use Kotka\Service\SpecimenService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class SpecimenServiceFactory is factory class for creating
 * specimen service.
 *
 * @package Kotka\Service\Factory
 */
class SpecimenServiceFactory implements FactoryInterface
{
    /**
     * Creates the service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return SpecimenService
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return new SpecimenService(
            $serviceLocator->get('Kotka\Service\SequenceService'),
            $serviceLocator,
            $serviceLocator->get('Kotka\Mapper\MYDocumentMapperInterface'),
            $serviceLocator->get('SlmQueue\Queue\QueuePluginManager')->get('default'),
            $serviceLocator->get('user'),
            $serviceLocator->get('Kotka\Service\CoordinateService'),
            $serviceLocator->get('Common\Service\IdService')
        );
    }
}
