<?php
namespace Kotka\Service\Factory;

use Zend\Mail\Transport;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class MailTransport implements FactoryInterface
{
    /**
     * @see \Zend\ServiceManager\FactoryInterface::createService()
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $config = $serviceLocator->get('Config');
        $type = $config['mailer']['type'];
        if ($type == 'smtp') {
            $transport = new Transport\Smtp();
            $options = new Transport\SmtpOptions($config['mailer']['smtp_options']);
            $transport->setOptions($options);
        } elseif ($type == 'file') {
            $transport = new Transport\File();
            $options = new Transport\FileOptions($config['mailer']['file_options']);
            $transport->setOptions($options);
        } else {
            throw new \RuntimeException("Unable to create service type " . $type);
        }

        return $transport;
    }
}