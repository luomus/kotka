<?php

namespace Kotka\Service\Factory;

use Kotka\Service\CollectionService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class CollectionServiceFactory is factory class for creating
 * collection service.
 *
 * @package Kotka\Service\Factory
 */
class CollectionServiceFactory implements FactoryInterface
{
    /**
     * Creates the service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return CollectionService
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $collectionMapper = $serviceLocator->get('Kotka\Mapper\MYCollectionMapperInterface');
        $authentication = $serviceLocator->get('user');
        $idGen = $serviceLocator->get('Common\Service\IdService');

        return new CollectionService($collectionMapper, $authentication, $idGen);
    }
}
