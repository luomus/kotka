<?php

namespace Kotka\Service\Factory;

use Kotka\Service\ErrorHandlingService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class ErrorHandlingService is factory class for creating
 * error handling service.
 *
 * @package Kotka\Service\Factory
 */
class ErrorHandlingServiceFactory implements FactoryInterface
{
    /**
     * Creates the service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return ErrorHandlingService
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $logger = $serviceLocator->get('Logger');

        return new ErrorHandlingService($logger);
    }
}
