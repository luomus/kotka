<?php

namespace Kotka\Service\Factory;

use Kotka\Service\UserStatsService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class UserStatsServiceFactory is factory class for creating
 * stats service.
 *
 * @package Kotka\Service\Factory
 */
class UserStatsServiceFactory implements FactoryInterface
{
    /**
     * Creates the service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return UserStatsService
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $om = $serviceLocator->get('TripleStore\ObjectManager');
        $cache = $serviceLocator->get('cache');

        return new UserStatsService($om, $cache);
    }
}
