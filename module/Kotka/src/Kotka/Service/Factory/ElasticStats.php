<?php
namespace Kotka\Service\Factory;

use Kotka\Service\ElasticStats as Stats;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ElasticStats implements FactoryInterface
{
    /**
     * @see \Zend\ServiceManager\FactoryInterface::createService()
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $config = $serviceLocator->get('config');
        $cache = $serviceLocator->get('cache');
        $logger = $serviceLocator->get('logger');

        return new Stats($config['elastic_search'], $cache, $logger);
    }
}