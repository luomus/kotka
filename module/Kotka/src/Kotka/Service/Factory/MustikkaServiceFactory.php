<?php

namespace Kotka\Service\Factory;

use Kotka\Service\MustikkaService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class MustikkaServiceFactory is factory class for creating
 * mustikka service.
 *
 * @package Kotka\Service\Factory
 */
class MustikkaServiceFactory implements FactoryInterface
{
    /**
     * Creates the service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return MustikkaService
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $externals = $serviceLocator->get('Kotka\Options\ExternalsOptions');
        $logger = $serviceLocator->get('Logger');
        /** @var \Api\Service\ImageService $imageService */
        $imageService = $serviceLocator->get('Api\Service\ImageService');
        $collectionService = $serviceLocator->get('Kotka\Service\CollectionService');

        return new MustikkaService($externals, $logger, $imageService, $collectionService);
    }
}
