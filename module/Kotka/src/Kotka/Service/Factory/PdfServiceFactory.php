<?php

namespace Kotka\Service\Factory;

use Kotka\Service\PdfService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class PdfServiceFactory is factory class for creating
 * pdf service.
 *
 * @package Kotka\Service\Factory
 */
class PdfServiceFactory implements FactoryInterface
{
    /**
     * Creates the service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return PdfService
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $pdf = $serviceLocator->get('tcpdf');
        $fes = $serviceLocator->get('Kotka\Service\FormElementService');
        $config = $serviceLocator->get('Config');

        return new PdfService($pdf, $fes, $config['pdf_converted']);
    }
}
