<?php
namespace Kotka\Service\Factory;

use Common\Service\IdService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class IdServiceFactory implements FactoryInterface
{

    /**
     * Creates the service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return IdService
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $seqGen = $serviceLocator->get('Triplestore\Service');

        return new IdService($seqGen);
    }

} 