<?php

namespace Kotka\Service\Factory;

use Kotka\Service\SequenceService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class SequenceServiceFactory is factory class for creating
 * sequence service.
 *
 * @package Kotka\Service\Factory
 */
class SequenceServiceFactory implements FactoryInterface
{
    /**
     * Creates the service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return SequenceService
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /** @var \Triplestore\Service\ObjectManager $om */
        $om = $serviceLocator->get('TripleStore\ObjectManager');

        return new SequenceService($om);
    }
}
