<?php

namespace Kotka\Service\Factory;

use Kotka\Service\DatasetService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class DatasetServiceFactory is factory class for creating
 * dataset service.
 *
 * @package Kotka\Service\Factory
 */
class DatasetServiceFactory implements FactoryInterface
{
    /**
     * Creates the service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return DatasetService
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {

        $datasetMapper = $serviceLocator->get('Kotka\Mapper\GXDatasetMapperInterface');
        $authentication = $serviceLocator->get('user');
        $idGen = $serviceLocator->get('Common\Service\IdService');

        return new DatasetService($datasetMapper, $authentication, $idGen);
    }
}
