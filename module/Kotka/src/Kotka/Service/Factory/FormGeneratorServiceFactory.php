<?php
namespace Kotka\Service\Factory;

use Kotka\Service\FormGeneratorService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class FormGeneratorServiceFactory implements FactoryInterface
{

    protected $instance;

    /**
     * Creates the service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return FormGeneratorService
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $cache = $serviceLocator->get('perma-cache');
        $formElementManager = $serviceLocator->get('FormElementManager');
        $validatorManager = $serviceLocator->get('ValidatorManager');
        $filterManager = $serviceLocator->get('FilterManager');
        /** @var \Triplestore\Options\ModuleOptions $tripleOptions */
        $tripleOptions = $serviceLocator->get('Triplestore\Options\ModuleOptions');
        $InputFilterManager = $serviceLocator->get('InputFilterManager');

        return new FormGeneratorService($cache, $formElementManager, $validatorManager, $filterManager, $tripleOptions->getClassNS(), $InputFilterManager);
    }

}