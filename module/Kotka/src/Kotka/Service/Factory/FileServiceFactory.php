<?php

namespace Kotka\Service\Factory;

use Kotka\Service\FileService;
use Kotka\Service\PdfService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class FileServiceFactory is factory class for creating
 * file service.
 *
 * @package Kotka\Service\Factory
 */
class FileServiceFactory implements FactoryInterface
{
    const PATH = './data/upload/';

    /**
     * Creates the service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return PdfService
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return new FileService(self::PATH);
    }
}
