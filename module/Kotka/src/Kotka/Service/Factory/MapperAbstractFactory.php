<?php

namespace Kotka\Service\Factory;

use Zend\ServiceManager\AbstractFactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class MapperAbstractFactory this is ser
 *
 * @package Kotka\Service\Factory
 */
class MapperAbstractFactory implements AbstractFactoryInterface
{
    /**
     * Determine if we can create a service with name
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @param $name
     * @param $requestedName
     * @return bool
     */
    public function canCreateServiceWithName(ServiceLocatorInterface $serviceLocator, $name, $requestedName)
    {
        return (substr($requestedName, -15) === 'MapperInterface');
    }

    /**
     * @inheritdoc
     */
    public function createServiceWithName(ServiceLocatorInterface $serviceLocator, $name, $requestedName)
    {
        /** @var \Triplestore\Service\ObjectManager $objectManager */
        $objectManager = $serviceLocator->get('Triplestore\ObjectManager');

        // Mapper names given are under the form "Kotka\Mapper\SomethingMapperInterface", so we need to
        // remove the MapperInterface part
        $parts = explode('\\', $requestedName);
        $entityName = substr(end($parts), 0, -15);

        return $objectManager->getRepository($entityName);
    }

}
