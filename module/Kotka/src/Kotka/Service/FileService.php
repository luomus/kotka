<?php

namespace Kotka\Service;


class FileService
{

    private $uploadDirectory;
    private $subDirectory;
    private $errorMsg;

    public function __construct($directory)
    {
        $this->setUploadDirectory($directory);
    }

    public function getErrorMessage()
    {
        return $this->errorMsg;
    }

    /**
     * @return string
     */
    public function getUploadDirectory()
    {
        return $this->uploadDirectory;
    }

    /**
     * @param string $uploadDirectory
     */
    public function setUploadDirectory($uploadDirectory)
    {
        $this->uploadDirectory = rtrim($uploadDirectory, '\\/') . DIRECTORY_SEPARATOR;
    }

    public function setSubDirectory($directory)
    {
        $directory = $this->sanitizeFilename($directory);
        $this->subDirectory = rtrim($directory, '\\/') . DIRECTORY_SEPARATOR;
    }

    public function getDirectory($id) {
        return $this->directoryPath($id);
    }

    public function store($id, $filename, $source, $fromUpload = true)
    {
        $filename = $this->sanitizeFilename($filename);
        $directory = $this->directoryPath($id, true);
        if (!is_writable($directory)) {
            throw new \Exception("Writing permission denied to $directory");
        }
        $fullPath = $directory . $filename;
        if (file_exists($fullPath)) {
            $this->errorMsg = 'File already exists!';

            return false;
        }

        $result = $this->_move($source, $fullPath, $fromUpload);
        if (!$result) {
            $this->errorMsg = 'Writing to Kotka failed!';
        }
        return $result;
    }

    protected function _move($source, $fullPath, $fromUpload = true)
    {
        if ($fromUpload) {
            return move_uploaded_file($source, $fullPath);
        } else {
            return rename($source,  $fullPath);
        }
    }

    public function delete($id, $filename)
    {
        $directory = $this->directoryPath($id);
        if ($directory === false) {
            return false;
        }
        $fullPath = realpath($directory . $filename);
        if (!file_exists($fullPath)) {
            return true;
        }
        return $this->_delete($fullPath);
    }

    protected function _delete($fullPath)
    {
        return unlink($fullPath);
    }

    public function getFileList($id)
    {
        $directory = $this->directoryPath($id);
        if ($directory === false) {
            return array();
        }
        $files = scandir($directory);
        if ($files === false) {
            return array();
        }
        array_shift($files);
        array_shift($files);

        return $files;
    }

    public function getFile($id, $filename)
    {
        $filename = $this->sanitizeFilename($filename);
        $directory = $this->directoryPath($id);
        $fullPath = $directory . $filename;
        if ($directory === false || !file_exists($fullPath)) {
            return false;
        }
        return $fullPath;
    }

    private function directoryPath($id, $create = false)
    {
        $id = $this->sanitizeFilename($id);
        $parts = str_split($id, 2);
        $path = $this->uploadDirectory;
        $path .= $this->subDirectory;
        $path .= array_shift($parts);
        $path .= array_shift($parts);
        foreach ($parts as $part) {
            $path .= $part . DIRECTORY_SEPARATOR;
            if (!is_dir($path)) {
                if ($create) {
                    mkdir($path);
                } else {
                    return false;
                }
            }
        }
        return $path;
    }

    private function sanitizeFilename($filename)
    {
        return str_replace(['..', '\\', '/'], '', $filename);
    }

} 