<?php

namespace Kotka\Service;

use Common\Service\IdService;
use Kotka\Enum\Document as DocumentEnum;
use Kotka\Filter\PrefixFilter;
use Kotka\Filter\ToQName;
use Kotka\Form\Element\ArrayCollection;
use Kotka\Form\Element\PrefixText;
use Kotka\Form\MYDocument;
use Kotka\Model\ExcelColumn;
use Kotka\Model\ExcelSheet;
use Kotka\Model\ExcelStyles;
use Kotka\Stdlib\ArrayUtils;
use Triplestore\Service\MetadataService;
use Triplestore\Stdlib\SubjectAwareInterface;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\EventManager\EventManagerAwareTrait;
use Zend\Form\Element;
use Zend\Form\Fieldset;
use Zend\Form\Form;
use Zend\Form\FormInterface;
use Zend\Http\Headers;
use Zend\Http\Response;
use Zend\InputFilter\CollectionInputFilter;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\InputInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ExcelService implements EventManagerAwareInterface
{

    use EventManagerAwareTrait;

    const ROWS_PREPARED = 300;
    private $generatedPath;
    /** @var  \Zend\Form\Form */
    private $form;
    private $metaData;
    private $filename;
    private $sl;

    private $skip = array(
        'MYIsPartOf' => 1,
        'MZOwner' => 1,
        'subject' => 1,
        'MYDatatype' => 1,
        'MYWgs84Latitude' => 1,
        'MYWgs84Longitude' => 1,
        'MYInMustikka' => 1,
    );

    public function __construct($path, MetadataService $metadataService, ServiceLocatorInterface $sl)
    {
        $this->generatedPath = $path;
        $this->metaData = $metadataService->getMetadataFor(DocumentEnum::TYPE);
        $this->metaData->initAliases();
        $this->sl = $sl;
    }

    public function setSkip(array $skip) {
        $this->skip = array_flip($skip);
    }

    public function setForm(Form $form)
    {
        $this->form = $form;
    }

    public function getForm()
    {
        return $this->form;
    }

    public function getResponse($file = null, $name = null)
    {
        if ($file === null) {
            $file = $this->filename;
        }
        if ($name === null) {
            $name = $this->filename;
        }
        $headers = new Headers();
        $response = new Response();
        if (!file_exists($file)) {
            $response->setStatusCode(500);
            return $response;
        }
        $content = file_get_contents($file);
        $headers->clearHeaders()
            ->addHeaderLine('Content-type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
            ->addHeaderLine('Content-Disposition', 'attachment; filename="' . $name . '"')
            ->addHeaderLine("Content-Length", strlen($content));
        $response->setHeaders($headers);
        $response->setContent($content);

        return $response;
    }

    public function generateExcelFromArray($data, $filename, $splitSubject = false, $addHeaderRow = false, $writeProtectPassword = false)
    {
        $flats = array();
        $allValues = array();
        /** @var \Kotka\Form\MYDocument $form */
        $form = $this->form;
        $this->filename = $filename;
        foreach ($data as $row) {
            if (!is_array($row)) {
                $row = $this->extractData($row);
            }
            $row['subject'] = IdService::getQName($row['subject']);
            $this->getEventManager()->trigger('pre-add', $this, ['data' => &$row]);
            if ($splitSubject) {
                if (strpos($row['subject'], '.') !== false) {
                    $parts = explode('.', $row['subject'], 2);
                    $row['MYNamespaceID'] = $parts[0];
                    $row['MYObjectID'] = $parts[1];
                } else if (strpos($row['subject'], ':') !== false) {
                    $parts = explode(':', $row['subject'], 2);
                    $row['MYNamespaceID'] = $parts[0] . ':';
                    $row['MYObjectID'] = $parts[1];
                }
            }
            if (isset($row['MYGathering']) && isset($row['MYGathering'][0]) && !isset($row['MYGathering'][0]['MYCountry'])) {
                $row['MYGathering'][0]['MYCountry'] = '';
            }
            $itemData = ArrayUtils::array_remove_null($row);
            $flats[] = ArrayUtils::flatten_with_keys($itemData);
            $allValues = array_replace_recursive($allValues, $itemData);
        }
        $form->setData($allValues, false);
        if ($this->form instanceof MYDocument) {
            $form->setType('zoo');
        }
        $form->isValid();
        $inputFilter = $form->getInputFilter();
        $excelSheet = new ExcelSheet();
        $this->extractColumns($form, $inputFilter, $excelSheet, $flats);
        $this->generateExcelFromSheet($excelSheet, $filename, $addHeaderRow, $writeProtectPassword);

        return file_exists($filename);
    }

    protected function extractData($data)
    {
        if ($this->form === null) {
            throw new \Exception('Could not extract the data from the array!');
        }
        $form = clone $this->form;
        $form->setObject($data);
        $form->isValid();
        return $form->getData(FormInterface::VALUES_AS_ARRAY);

        return $result;
    }

    public function generateHerttaExcelFromArray($data, $filename)
    {
        $herttaService = new HerttaService();
        $this->filename = $filename;
        $herttaService->setServiceLocator($this->sl);
        $excelSheet = $herttaService->convertSpecimenArrayToHerttaSheet($data);
        $excel = new \PHPExcel();
        $excel->getProperties()->setCreator("Kotka")
            ->setLastModifiedBy("Kotka")
            ->setTitle("Kotka Hertta export")
            ->setSubject("Hertta export")
            ->setDescription("Specimens exported from Kotka in Hertta format")
            ->setKeywords("kotka hertta export")
            ->setCategory("kotka hertta export");
        $excel->setActiveSheetIndex(0);
        $sheet = $excel->getActiveSheet();
        $sheet ->getStyle("A1:AJ1")->getFont()->setSize(6);
        $sheet->setTitle('Specimens');
        $sheet->fromArray($excelSheet->toArray(true), NULL, 'A1');
        $sheet->freezePane('A3');
        $this->writeExcel2007($excel, $filename);

        return file_exists($filename);
    }

    public function generateExcelFromSheet(ExcelSheet $excelSheet, $filename, $addHeaderRow = false, $writeProtectPassword = false)
    {

        $cacheMethod = \PHPExcel_CachedObjectStorageFactory::cache_in_memory;
        $cacheSettings = array('memoryCacheSize' => '128MB');
        \PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);
        $this->filename = $filename;

        $excel = new \PHPExcel();
        $excel->getProperties()->setCreator("Kotka")
            ->setLastModifiedBy("Kotka")
            ->setTitle("Kotka")
            ->setSubject("Kotka specimens")
            ->setDescription("List of specimens")
            ->setKeywords("kotka")
            ->setCategory("Kotka");
        $excel->setActiveSheetIndex(0);
        $sheet = $excel->getActiveSheet();
        $sheet->setTitle('Specimens');
        $sheet->fromArray($excelSheet->toArray($addHeaderRow), NULL, 'A1');
        if ($addHeaderRow) {
            //$sheet->getRowDimension(1)->setVisible(false);
            $sheet->freezePane('C3');
        } else {
            $sheet->freezePane('C2');
        }

        $styles = new ExcelStyles();
        foreach ($excelSheet as $col => $column) {
            /** @var \Kotka\Model\ExcelColumn $column */
            $col = \PHPExcel_Cell::stringFromColumnIndex($col);
            $field = $column->getField();
            $style = $styles->getColumnStyle($field, null, $column->getFirstOfGroup());
            $colRange = $col . '1:' . $col . ($column->count() + 1);
            if ($style) {
                $sheet->getStyle($colRange)->applyFromArray($style);
            }
            $sheet->getColumnDimension($col)->setWidth($styles->getColWidth($field));
        }

        $col = \PHPExcel_Cell::stringFromColumnIndex($excelSheet->count() - 1);
        $colRange = 'A:' . $col;
        $sheet->getStyle($colRange)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        $colRange = 'A1:' . $col . ($addHeaderRow ? '2' : '1');
        $sheet->getStyle($colRange)->applyFromArray($styles->getHeaderStyle(ExcelStyles::DEFAULT_KEY));
        if ($writeProtectPassword !== false) {
            $sheet->getProtection()->setPassword($writeProtectPassword);
            $sheet->getProtection()->setSheet(true);
        }
        $this->writeExcel2007($excel, $filename);

        return file_exists($filename);
    }

    private function extractColumns(Fieldset $form, $filter, ExcelSheet $sheet, $data, $path = '', $labelPrefix = '', $split = false)
    {
        foreach ($form as $element) {
            $name = $element->getName();
            if (isset($this->skip[$name])) {
                if ($name === 'subject' && preg_match('/\[MFSample\]\[[0-9]+\]$/', $path)) {
                    // Keep samples subject!
                } else {
                    continue;
                }
            }
            $curFilter = null;
            if ($filter instanceof InputFilterInterface && $filter->has($name)) {
                $curFilter = $filter->get($name);
            }

            $label = $this->getLabel($element->getLabel(), $name, $labelPrefix);
            $newPath = $path == '' ? $name : $path . '[' . $name . ']';
            if ($element instanceof ArrayCollection) {
                $cnt = $element->count();
                for ($i = 0; $i < $cnt; $i++) {
                    $colName = $newPath . '[' . $i . ']';
                    if ($col = $this->getCol($name, $label, $colName, $data, $split, $curFilter)) {
                        $sheet->addColumn($col);
                    }
                }
            } elseif ($element instanceof Fieldset) {
                $newLabelPrefix = $labelPrefix;
                if (is_numeric($name)) {
                    $newLabelPrefix = $newLabelPrefix . ($name + 1) . '.';
                    $split = true;
                    $curFilter = $filter;
                }
                if ($filter instanceof CollectionInputFilter) {
                    $curFilter = $filter->getInputFilter();
                }
                $this->extractColumns($element, $curFilter, $sheet, $data, $newPath, $newLabelPrefix, $split);
            } else {
                $value = $element->getValue();
                if ($value !== null) {
                    if (is_array($value)) {
                        foreach ($value as $key => $v) {
                            $colName = $newPath . '[' . $key . ']';
                            if ($col = $this->getCol($name, $label, $colName, $data, $split, $curFilter)) {
                                $sheet->addColumn($col);
                            }
                        }
                    } else {
                        if ($col = $this->getCol($name, $label, $newPath, $data, $split, $curFilter)) {
                            $sheet->addColumn($col);
                        }
                    }
                }
            }
            $split = false;
        }
    }

    private function getLabel($label, $default, $prefix)
    {
        if (empty($label)) {
            $label = $default;
        }
        return trim($prefix . ' ' . $label);
    }

    private function getCol($field, $label, $path, $data, $split, $filter)
    {
        $colData = array();
        $hasData = false;
        foreach ($data as $key => $value) {
            if (isset($value[$path])) {
                if (is_bool($value[$path])) {
                  $colData[$key] = $value[$path] === true ? 'true' : 'false';
                } else {
                  $colData[$key] = $value[$path];
                }
                $hasData = true;
            } else {
                $colData[$key] = '';
            }
        }
        if (!$hasData) {
            return false;
        }
        $col = new ExcelColumn();
        $col->setField($field);
        $col->setFullField($path);
        $col->setLabel($label);
        $col->setData($colData);
        $col->setFirstOfGroup($split);
        if ($prefix = $this->getPrefixFromFilter($filter)) {
            $col->setPrefix($prefix);
        }
        return $col;
    }

    public function generateFromParams($params)
    {
        $paramHandler = new ExcelParams();
        $paramHandler->setParams($params);
        $params = $paramHandler->getProcessedParams();
        $lang = $paramHandler->getLanguage();
        $styles = new ExcelStyles();
        $cacheMethod = \PHPExcel_CachedObjectStorageFactory::cache_in_memory;
        $cacheSettings = array('memoryCacheSize' => '32MB');
        \PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

        $excel = new \PHPExcel();
        $excel->getProperties()->setCreator("Kotka")
            ->setLastModifiedBy("Kotka")
            ->setTitle("Kotka")
            ->setSubject("import sheet")
            ->setDescription("Excel import sheet for Kotka specimens")
            ->setKeywords("kotka import")
            ->setCategory("Kotka import");

        $excel->setActiveSheetIndex(0);
        $sheet = $excel->getActiveSheet();
        $sheet->setTitle('Data');
        $max = self::ROWS_PREPARED + 2;
        $validated = array();

        $uris = '';
        foreach ($params as $key => $param) {
            if ($key == ExcelParams::DATA) {
                continue;
            }
            if (isset($param['uri'])) {
                $uris .= $param['uri'];
            }
        }

        $filename = $this->getFileName($uris, $lang);
        $i = -1;
        $split = false;
        $col = 'A';

        /** @var \Zend\Form\Form $specimenForm */
        $specimenForm = $this->form;
        //$specimenForm->prepare();
        $data = $this->getBaseParams($params[ExcelParams::DATA]);
        $specimenForm->setData($data, false);
        $specimenForm->isValid();
        unset($params[ExcelParams::DATA]);

        foreach ($params as $param) {
            if ($param == ExcelParams::SPLIT) {
                $split = true;
                continue;
            }
            $uri = $param['uri'];
            $prefix = $param['prefix'] . ' ';
            $elem = $param['data'];
            list($element, $filter, $elemName) = $this->getElem($specimenForm, $specimenForm->getInputFilter(), $elem);
            if (!$element) {
                continue;
            }
            $i++;
            $col = \PHPExcel_Cell::stringFromColumnIndex($i);
            $field = substr($uri, strrpos($uri, '['));
            $field = str_replace(array('[', ']'), '', $field);
            $level = $this->getTopLevel($uri, true);

            $cell = $sheet->getCell($col . 1);
            if ($element instanceof ArrayCollection) {
                $uri .= '[0]';
            }
            $cell->setValue($uri);
            $cell = $sheet->getCell($col . 2);
            // Need to fetch label from meta data service since the requested language can be other than what was used on the form
            $label = $this->metaData->getLabel($elemName, $lang, !empty($element->getLabel()) ? $element->getLabel() : null);
            if ($label == null) {
                $label = $this->searchLabel($elemName, $element);
            }
            $cell->setValue($prefix . $label);
            $style = $styles->getColumnStyle($field, $level, $split);
            $colRange = $col . '1:' . $col . self::ROWS_PREPARED;
            if ($style) {
                $sheet->getStyle($colRange)->applyFromArray($style);
                $split = false;
            }
            $style = $styles->getHeaderStyle($field);
            $sheet->getStyleByColumnAndRow($i, 2)->applyFromArray($style);
            $sheet->getColumnDimensionByColumn($i)->setWidth($styles->getColWidth($field));

            if (!$element instanceof Element\Select) {
                continue;
            }
            $options = $this->getOptions($element, $filter, $field);
            $validatorCol = \PHPExcel_Cell::stringFromColumnIndex(count($validated));
            $validated[$validatorCol] = $options;
            $objValidation = new \PHPExcel_Cell_DataValidation();
            $objValidation->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
            $objValidation->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
            $objValidation->setAllowBlank(true);
            $objValidation->setShowInputMessage(true);
            $objValidation->setShowErrorMessage(true);
            $objValidation->setShowDropDown(true);
            $objValidation->setErrorTitle('Input error');
            $objValidation->setError('Value is not in list.');
            $objValidation->setPromptTitle('Pick from list');
            $objValidation->setPrompt('Please pick a value from the drop-down list.');

            $row = count($options);
            $objValidation->setFormula1('validator!$' . $validatorCol . '$1:$' . $validatorCol . '$' . $row);

            $range = $col . '3:' . $col . $max;
            $sheet->setDataValidation($range, $objValidation);
        }
        $colRange = 'A:' . $col;
        $sheet->getStyle($colRange)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_TEXT);

        //$sheet->getRowDimension(1)->setVisible(false);
        $sheet->freezePane('C3');

        if (count($validated) > 0) {
            $sheet = $excel->createSheet();
            $sheet->setTitle('validator');
            foreach ($validated as $validatorCol => $values) {
                $spot = 1;
                foreach ($values as $value => $label) {
                    $coordinate = $validatorCol . $spot;
                    $sheet->setCellValue($coordinate, $value);
                    $spot++;
                }
            }
            $sheet->getProtection()->setSheet(true);
        }
        $this->writeExcel2007($excel, $filename);
        return $filename;
    }

    public function writeExcel2007($excel, $filename)
    {
        /** @var \PHPExcel_Writer_Excel2007 $writer */
        $writer = \PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
        $writer->setPreCalculateFormulas(false);
        $writer->save($filename);
    }

    private function getBaseParams($params)
    {
        $params['MYCollectionID'] = 'HR.121';
        if (isset($params['MYGathering'])) {
            foreach ($params['MYGathering'] as $key => $value) {
                $params['MYGathering'][$key]['MYCountry'] = 'FI';
                if (isset($params['MYGathering'][$key]['MYUnit'])) {
                    foreach ($params['MYGathering'][$key]['MYUnit'] as $key2 => $value2) {
                        $params['MYGathering'][$key]['MYUnit'][$key2]['MYRecordBasis'] = 'MY.recordBasisPreservedSpecimen';
                    }
                }
            }
        }
        return $params;
    }

    protected function searchLabel($field, Element $element)
    {
        if ($element instanceof PrefixText) {
            $options = $element->getValueOptions();
            $lcField = lcfirst($field);
            if (isset($options[$lcField])) {
                return $options[$lcField];
            }
        }
        return $field;
    }

    public static function getTopLevel($label, $includePseudoLevels = false)
    {
        $level = 'document';
        if (strpos($label, 'MFSample')) {
            $level = 'sample';
        } else if (strpos($label, 'MYTypeSpecimen')) {
            $level = 'type';
        } else if (strpos($label, 'MYIdentification') !== false) {
            $level = 'identification';
        } else if ($includePseudoLevels && strpos($label, 'MYMeasurement') !== false) {
            $level = 'measurement';
        } else if (strpos($label, 'MYUnit') !== false) {
            $level = 'unit';
        } else if (strpos($label, 'MYGathering') !== false) {
            $level = 'gathering';
        }
        return $level;
    }

    private function getPrefixFromFilter($filter) {
        list($prefix, $quessDomain) = $this->analyzeFilter($filter);
        return $prefix;
    }

    private function analyzeFilter($filter) {
        $prefix = false;
        $guessDomain = false;
        if ($filter instanceof InputInterface) {
            $filters = $filter->getFilterChain()->getFilters();
            foreach ($filters as $filter) {
                if ($filter instanceof PrefixFilter) {
                    $prefix = $filter->getPrefix();
                    break;
                }
                if ($filter instanceof ToQName) {
                    $guessDomain = $filter->isFindCorrectDomain();
                    break;
                }
            }
        }
        return [$prefix, $guessDomain];
    }

    private function getOptions(Element\Select $select, $filter, $field)
    {
        $options = $this->getAllOptions($select->getValueOptions());
        list($prefix,$guessDomain) = $this->analyzeFilter($filter);
        $result = array();
        foreach ($options as $key => $value) {
            if ($prefix !== false) {
                $key = str_replace($prefix, '', $key);
            }
            if ($guessDomain) {
                $key = str_replace([
                    IdService::QNAME_PREFIX_TUN,
                    IdService::QNAME_PREFIX_LUOMUS
                ], '', $key);
            }
            $result[$key] = $value;
        }
        if (in_array($field, [DocumentEnum::FORM_COLLECTION_ID, DocumentEnum::FORM_DATASET_ID])) {
            ksort($result, SORT_NATURAL);
        }
        return $result;
    }

    private function getAllOptions($options)
    {
        $all = array();
        foreach ($options as $key => $value) {
            if (is_array($value)) {
                $all = array_merge($all, $this->getAllOptions($value));
            } else {
                if ($key !== 'label') {
                    $all[$key] = $value;
                }
            }
        }
        return $all;
    }

    private function getElem($form, $filter = null, $location)
    {
        $key = key($location);
        if (!$form instanceof Fieldset || !$form->has($key)) {
            return false;
        }
        if ($filter instanceof InputFilter) {
            if ($filter->has($key)) {
                $filter = $filter->get($key);
                if ($filter instanceof CollectionInputFilter) {
                    $filter = $filter->getInputFilter();
                }
            } else if (!is_numeric($key)) {
                $filter = null;
            }
        }
        if (is_array($location[$key])) {
            return $this->getElem($form->get($key), $filter, $location[$key]);
        }
        return array($form->get($key), $filter, $key);
    }

    private function getFileName($uris, $lang)
    {
        $path = $this->generatedPath . DIRECTORY_SEPARATOR . 'import_' . md5($uris . $lang) . '.xlsx';

        return $path;
    }

} 