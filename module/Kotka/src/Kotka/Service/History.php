<?php

namespace Kotka\Service;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class History
 *
 * @package Kotka\Service
 */
class History implements ServiceLocatorAwareInterface
{
    private $serviceLocator;

    public function getHistoryVersion($subject, $timestamp)
    {
        /** @var \Triplestore\Service\ObjectManager $om */
        $om = $this->getServiceLocator()->get('Triplestore\ObjectManager');

        return $om->fetchFromHistory($subject, $timestamp);
    }

    /**
     * Sets the service locator
     * @param ServiceLocatorInterface $sl
     */
    public function setServiceLocator(ServiceLocatorInterface $sl)
    {
        $this->serviceLocator = $sl;
    }

    /**
     * Gets the service locator
     * @return ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }
}
