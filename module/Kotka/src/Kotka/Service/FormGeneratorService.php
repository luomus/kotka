<?php
namespace Kotka\Service;

use Kotka\Service\Exception;
use Zend\Cache\Storage\StorageInterface;
use Zend\Filter\FilterChain;
use Zend\Filter\FilterPluginManager;
use Zend\Form\Annotation\AnnotationBuilder;
use Zend\Form\Factory;
use Zend\Form\Form;
use Zend\Stdlib\ArrayUtils;
use Zend\Validator\ValidatorChain;

class FormGeneratorService
{

    const CACHE_PREFIX = 'form_';

    protected $filterNameSpace = 'Kotka\\InputFilter\\';
    protected $suffix = 'WarningFilter';
    protected $formClassNs;

    /** @var \Zend\Cache\Storage\StorageInterface */
    private $cache;
    /** @var  \Zend\Form\FormElementManager */
    private $formElementManager;

    /** @var FilterPluginManager */
    protected $filterManager;

    public function __construct(StorageInterface $cache, $formElementManager, $validatorManager, $filterManager, $formClassNs, $InputFilterManager)
    {
        $this->cache = $cache;
        $this->formElementManager = $formElementManager;
        $this->validatorManager = $validatorManager;
        $this->filterManager = $filterManager;
        $this->formClassNs = $formClassNs;
        $this->InputFilterManager = $InputFilterManager;
    }

    public function getForm($class)
    {
        /** @var  \Kotka\Form\WarningForm $form */
        if (is_object($class)) {
            $class = get_class($class);
        } elseif (is_string($class)) {
            if (strpos($class, '\\') === false) {
                $class = '\\' . $this->formClassNs . '\\' . $class;
            }
        } else {
            throw new Exception\InvalidArgumentException("Could not create form from '" . gettype($class) . "'.");
        }
        $name = self::CACHE_PREFIX . str_replace('\\', '', $class);
        $factory = new Factory($this->formElementManager);
        $filterChain = new FilterChain();
        $validatorChain = new ValidatorChain();
        $filterChain->setPluginManager($this->filterManager);
        $validatorChain->setPluginManager($this->validatorManager);
        $factory->getInputFilterFactory()
            ->setDefaultValidatorChain($validatorChain)
            ->setDefaultFilterChain($filterChain);

        //$this->cache->removeItem($name);
        if (!$this->cache->hasItem($name)) {
            $builder = new AnnotationBuilder();
            $builder->setFormFactory($factory);
            $spec = ArrayUtils::iteratorToArray($builder->getFormSpecification($class));
            $spec['warningFilter'] = $this->getWarningForm($class);
            $this->cache->setItem($name, $spec);
        }
        $spec = $this->cache->getItem($name);
        $form = $factory->createForm($spec);
        if ($spec['warningFilter'] !== null) {
            $warningClass = $spec['warningFilter'];
            $form->setInputWarningFilter(
                $this->InputFilterManager->get($warningClass)
            );
        }
        if (method_exists($form, 'postInit')) {
            $form->postInit();
        }
        $this->addInputFilter($form, $class);
        //$form->setPreferFormInputFilter(true);
        //$form->setUseInputFilterDefaults(true);

        return $form;
    }

    private function addInputFilter(Form $form, $class)
    {
        $class = $this->getClassName($class);
        $filter = $this->filterNameSpace . $class;
        if (class_exists($filter)) {
            $form->setInputFilter($this->InputFilterManager->get($filter));
        }
    }

    private function getWarningForm($class)
    {
        $class = $this->getClassName($class);
        $full = $this->filterNameSpace . $class . $this->suffix;
        if (class_exists($full)) {
            return $full;
        }
        return null;
    }

    private function getClassName($class)
    {
        $pos = strrpos($class, '\\');
        if ($pos !== false) {
            $class = substr($class, $pos + 1);
        }
        return $class;
    }

}