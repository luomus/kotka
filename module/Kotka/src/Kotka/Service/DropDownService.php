<?php

namespace Kotka\Service;


class DropDownService
{

    private $formElementService;

    private $map = [
        'dataset' => 'MY.datasetId',
        'collection' => 'MY.collectionID',
        'creator' => 'MZ.editor',
        'editor' => 'MZ.editor',
    ];

    private $useKeysAsLabel = [
        'collectionID' => true
    ];

    public function __construct(FormElementService $formElementService)
    {
        $this->formElementService = $formElementService;
    }

    public function getDropDown($field, $notFoundValue = [])
    {
        if (isset($this->map[$field])) {
            $field = $this->map[$field];
        }
        $result = $this->formElementService->getSelect($field, $notFoundValue);
        if ($result === $notFoundValue && strpos($field, '.') === false) {
            $result = $this->formElementService->getSelect('MY.' . $field, $notFoundValue);
            if ($result === $notFoundValue) {
                $result = $this->formElementService->getSelect('MZ.' . $field, $notFoundValue);
            }
        }
        return $result;
    }

    public function getAllLabels($field, $notFoundValue = [])
    {
        $useKeys = false;
        if (isset($this->useKeysAsLabel[$field])) {
            $useKeys = $this->useKeysAsLabel[$field];
        }
        $data = $this->getDropDown($field, $notFoundValue);

        return $this->_getAllLabels($data, $useKeys);
    }

    private function _getAllLabels($data, $useKeysAsLabel)
    {
        if (!is_array($data)) {
            return $data;
        }
        if (isset($data[FormElementService::ALIAS_KEY])) {
            unset($data[FormElementService::ALIAS_KEY]);
        }
        return $this->getValueOptionsValues($data, ($useKeysAsLabel ? 'OptionValue' : 'OptionLabel'));
    }

    protected function getValueOptionsValues($options, $method)
    {
        $values = [];
        $method = 'get' . $method;
        foreach ($options as $key => $optionSpec) {
            if (is_array($optionSpec) && array_key_exists('options', $optionSpec)) {
                foreach ($optionSpec['options'] as $nestedKey => $nestedOptionSpec) {
                    $values[] = $this->$method($nestedKey, $nestedOptionSpec);
                }
                continue;
            }

            $values[] = $this->$method($key, $optionSpec);
        }
        return $values;
    }

    protected function getOptionValue($key, $optionSpec)
    {
        return is_array($optionSpec) ? $optionSpec['value'] : $key;
    }

    protected function getOptionLabel($key, $optionSpec)
    {
        return is_array($optionSpec) ? $optionSpec['label'] : $optionSpec;
    }

}