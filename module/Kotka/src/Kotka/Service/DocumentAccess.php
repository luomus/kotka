<?php
namespace Kotka\Service;


use Kotka\Enum\Document;
use Kotka\Triple\MYDocument;
use Triplestore\Classes\MAPersonInterface as MAPerson;
use Triplestore\Model\Model;
use Triplestore\Model\Predicate;
use Triplestore\Service\ObjectManager;
use Triplestore\Service\PhpVariableConverter;
use User\Enum\User;

class DocumentAccess
{
    const ALLOW_DELETE_FOR = '-2 week';

    const VALUE_TYPE_LITERAL = 'Literal';
    const VALUE_TYPE_RESOURCE = 'Resource';

    const MODE_PUBLIC = 'public';
    const MODE_PROTECTED = 'protected';
    const MODE_PRIVATE = 'private';

    /**
     * @param $model
     * @param MAPerson $user
     * @return bool
     */
    public static function canDelete($model, MAPerson $user)
    {
        $created = self::getValue($model, Document::PREDICATE_DATE_CREATED);
        if (is_string($created)) {
            $created = new \DateTime($created);
        } else if (!$created instanceof \DateTime) {
            return false;
        }
        $past = new \DateTime();
        $past->modify(self::ALLOW_DELETE_FOR);
        if ($past > $created) {
            return false;
        }
        $creator = self::getValue($model, Document::PREDICATE_CREATOR);
        $role = $user->getMARoleKotka();
        if ($creator === $user->getSubject() || $role === User::ROLE_ADMIN) {
            return true;
        }
        return false;
    }

    /**
     * Return true if the document can be viewed
     *
     * @param Model|array $model
     * @param MAPerson $user
     * @param $onlyByPerson bool
     * @return bool
     */
    public static function canView($model, MAPerson $user, $onlyByPerson = false)
    {
        $role = $user->getMARoleKotka();
        if ($role == User::ROLE_MEMBER || $role === User::ROLE_ADVANCED || $role == User::ROLE_ADMIN) {
            return true;
        }
        if ($onlyByPerson) {
            return false;
        }

        return self::isPublic($model);
    }

    /**
     * Return true if the use is allowed to edit the model
     *
     * @param Model|array $model
     * @param MAPerson $user
     * @return bool
     */
    public static function canEdit($model, MAPerson $user)
    {
        $organizations = $user->getMAOrganisation();
        if ($model === null || count($organizations) == 0 || isset($organizations[User::GUEST_ORGANIZATION])) {
            return false;
        }
        $organization = self::getValue($model, Document::PREDICATE_OWNER);
        return empty($organization) || array_key_exists($organization, $organizations);
    }

    /**
     * Returns true if document data is public
     *
     * @param $data
     * @return bool
     */
    public static function isPublic($data)
    {
        $type = self::getType($data);
        if ($type === null) {
            return false;
        }
        return self::MODE_PUBLIC === self::getPublicityRestriction($data);
    }

    public static function getPublicityRestriction($data)
    {
        if ($data === null) {
            return self::MODE_PUBLIC;
        }
        $publicity = self::getValue($data, Document::PREDICATE_PUBLICITY_RESTRICTION, self::MODE_PUBLIC);
        if ($publicity === Document::PUBLICITY_PUBLIC || $publicity === self::MODE_PUBLIC) {
            return self::MODE_PUBLIC;
        } elseif ($publicity === Document::PUBLICITY_PROTECTED || $publicity === self::MODE_PROTECTED) {
            return self::MODE_PROTECTED;
        }
        return self::MODE_PRIVATE;
    }

    public static function getType($data)
    {
        return self::getValue($data, ObjectManager::PREDICATE_TYPE);
    }

    /**
     * Gets the value from the model or array and returns it
     * You should use the key value with dot since that can return data from any type of model
     *
     * Please note that if you use Model as data then if there is more than one object in the predicate
     * you'll be getting an array and if there is only one then you'll get a string
     *
     * @param $data
     * @param $key
     * @param mixed $emptyValue
     * @param null $type
     * @param null $lang
     * @return null|string|array|$emptyValue
     */
    public static function getValue($data, $key, $emptyValue = null, $type = null, $lang = null)
    {
        if ($data instanceof Model) {
            $predicate = $data->getPredicate($key);
            if ($predicate instanceof Predicate) {
                if ($type !== null) {
                    $method = 'get' . $type . 'Value';
                    $result = $predicate->$method($lang);
                    $cnt = count($result);
                    if ($cnt === 0) {
                        return $emptyValue;
                    } else if ($cnt === 1) {
                        return array_pop($result);
                    }
                    return $result;
                }
                return $predicate->getFirst()->getValue();
            }
        } else if ($data instanceof MYDocument) {
            $key = PhpVariableConverter::toPhpMethod($key);
            $method = 'get' . ucfirst($key);
            return $data->$method();
        } else if (is_array($data)) {
            if (!isset($data[$key])) {
                $key = PhpVariableConverter::toPhpMethod($key);
                if (!isset($data[$key])) {
                    return $emptyValue;
                }
            }
            if ($type === null || $type === self::VALUE_TYPE_RESOURCE) {
                return $data[$key];
            }
            if (isset($data[$key][$lang])) {
                return $data[$key][$lang];
            }
        }
        return $emptyValue;
    }
}