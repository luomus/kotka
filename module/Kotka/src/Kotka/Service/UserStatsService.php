<?php

namespace Kotka\Service;

use Elastic\Query\Aggregate;
use Elastic\Query\Query;
use Kotka\ElasticExtract\Specimen;
use Zend\Cache\Storage\StorageInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

/**
 * Class UserStatsService is for creating statistical data.
 * @package Kotka\Service
 */
class UserStatsService implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    const DATE_FORMAT = 'Y-m-d';
    const SINCE = '2014-03';

    protected $cache;

    public function getFrontPageStats()
    {
        $cacheKey = 'userSpecimenStats';
        $this->getCache()->removeItem($cacheKey);
        if ($this->getCache()->hasItem($cacheKey)) {
            $data = $this->getCache()->getItem($cacheKey);
        } else {
            $data = [];
            $data['usageThisMonth'] = $this->findSpecimenAddsInMonth(date('Y'), date('n'), 10);
            $data['usageThisYear'] = $this->getSpecimenAddsByUserInYear(date('Y'));
            $data['usageTotal'] = $this->getSpecimenAddsByUser();
            $data['allTime'] = $this->findSpecimenAddsByMonth(10);
            $this->getCache()->setItem($cacheKey, $data);
        }
        return $data;
    }

    public function findSpecimenAddsThisWeek($limit = 10)
    {
        $to = new \DateTime();
        $to->add(new \DateInterval('P1D'));
        $from = clone $to;
        $from->modify('Monday this week');
        $result = $this->getSpecimenAddsByUser($limit, $from, $to);

        return $result;
    }

    public function findSpecimenAddsInMonth($year, $month, $limit = 10)
    {
        $from = new \DateTime();
        $from->setDate($year, $month, 1);
        $to = clone $from;
        $to->add(new \DateInterval('P1M'));
        $result = $this->getSpecimenAddsByUser($limit, $from, $to);

        return $result;
    }

    public function getSpecimenAddsByUserInYear($year, $limit = 10)
    {
        $from = new \DateTime();
        $from->setDate($year, 1, 1);
        $to = new \DateTime();
        $to->setDate($year, 12, 31);
        $result = $this->getSpecimenAddsByUser($limit, $from, $to);

        return $result;
    }

    public function getSpecimenAddsByUser($limit = 10, \DateTime $from = null, \DateTime $to = null)
    {
        /** @var \Elastic\Client\Search $adapter */
        $adapter = $this->serviceLocator->get('Elastic\Client\Search');
        $queryStr = 'firstInDocument: true';
        if ($from !== null && $to !== null) {
            $queryStr .= ' AND dateCreated: [' . $from->format('Y-m-d') . ' TO ' . $to->format('Y-m-d') . ']';
        }
        $query = new Query();
        $query->setIndex(Specimen::getIndexName());
        $query->setQueryString($queryStr);
        $query->addAggregate(new Aggregate(Aggregate::TYPE_TERMS, 'creator.raw', 'creator', $limit));

        $results = $adapter->query($query)->execute();
        $data = [];
        foreach ($results->getAggregations() as $field => $aggr) {
            if (!isset($aggr['buckets'])) {
                continue;
            }
            foreach ($aggr['buckets'] as $value) {
                $data[$value['key']] = $value['doc_count'];
            }
        }
        return $data;
    }

    public function findSpecimenAddsByMonth($monthsBack = 6)
    {
        /** @var \Elastic\Client\Search $adapter */
        $adapter = $this->serviceLocator->get('Elastic\Client\Search');
        $query = new Query();
        $query->setIndex(Specimen::getIndexName());
        $query->setQueryString('firstInDocument: true');
        $aggr = new Aggregate(Aggregate::TYPE_DATE_HISTOGRAM, 'dateCreated', 'dateCreated');
        $aggr->setInterval('1M');
        $aggr->setFormat('yyyy-MM');
        $query->addAggregate($aggr);

        $results = $adapter->query($query)->execute();
        $data = [];
        $cumulative = 0;
        foreach ($results->getAggregations() as $field => $aggr) {
            if (!isset($aggr['buckets'])) {
                continue;
            }
            foreach ($aggr['buckets'] as $value) {
                $key = substr($value['key_as_string'], 0, 7);
                $cumulative += $value['doc_count'];
                $data[$key] = $cumulative;
            }
        }
        return array_reverse(array_slice(array_reverse($data), 0, $monthsBack));
    }

    /**
     * @return StorageInterface
     */
    protected function getCache()
    {
        if ($this->cache === null) {
            $this->cache = $this->getServiceLocator()->get('cache');
        }
        return $this->cache;
    }


}
