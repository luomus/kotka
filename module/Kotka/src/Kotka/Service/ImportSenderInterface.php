<?php
namespace Kotka\Service;


interface ImportSenderInterface
{

    public function markAsReceived($id);

} 