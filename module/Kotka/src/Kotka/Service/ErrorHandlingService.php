<?php

namespace Kotka\Service;

use Zend\Log\LoggerInterface;

/**
 * Class ErrorHandling
 * takes care of logging exceptions encountered during runtime.
 * @package Kotka\Service
 */
class ErrorHandlingService
{
    protected $logger;

    /**
     * Initializes the object
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * This method is for logging exceptions.
     * @param \Exception $ex
     */
    public function logException(\Exception $ex)
    {
        $trace = $ex->getTraceAsString();
        $log = '';
        do {
            $log .= sprintf(
                "%s:%d %s (%d) [%s]",
                $ex->getFile(),
                $ex->getLine(),
                $ex->getMessage(),
                $ex->getCode(),
                get_class($ex)
            );
        } while ($ex = $ex->getPrevious());
        $log .= sprintf("\nTrace:\n%s", $trace);
        $this->logger->crit($log);
    }

}