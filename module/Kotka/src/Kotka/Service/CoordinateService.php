<?php
namespace Kotka\Service;

use Kotka\Http\HttpClientTrait;
use Zend\Http\Client;
use proj4php\Proj4php;
use proj4php\Proj;
use proj4php\Point;

class CoordinateService
{
    use HttpClientTrait;

    private static $spot = -1; // This is used to keep track of cache spot. It's -1 because we'll add 1 to it before using it
    const CACHE_SIZE = 10; // Size of the cache that keeps the whole result from the coordinate ws

    const SYSTEM_WSG84 = 'wgs84';
    const SYSTEM_WGS84DMS = 'wgs84dms';
    const SYSTEM_YKJ = 'ykj';
    const SYSTEM_EUREF = 'etrs-tm35fin';

    private $cache = []; // Holds only target and source coordinates
    private $lastKeys = [];
    private $lastData = []; // Holds the full results from the coordinate converter

    protected $posibleTypes = array(
        'ykj' => 'ykj',
        'wgs84' => 'wgs84-decimal',
        'wgs84dms' => 'wgs84-decimal',
        'kkj' => 'kkj-decimal',
        'etrs-tm35fin' => 'etrs-tm35fin',
    );

    protected $externalOptions;

    protected $urlTemplate;

    /**
     * CoordinateService constructor.
     * @param Client $client
     * @param $urlTemplate
     */
    public function __construct(Client $client, $urlTemplate)
    {
        $this->setHttpClient($client);
        $this->urlTemplate = $urlTemplate;
    }

    public function convertToEuref($n, $e, $sourceSystem)
    {
        return $this->convertToSystem($n, $e, $sourceSystem, 'euref');
    }

    public function convertToWgs84($n, $e, $sourceSystem, $location = 'c', $order = 'lat-lng')
    {
        $result = $this->convertToSystem($n, $e, $sourceSystem, 'wgs84-decimal', $location);
        if ($result !== null) {
            $result[0] = self::roundWgsDegree($result[0]);
            $result[1] = self::roundWgsDegree($result[1]);
            if ($order !== 'lat-lng') {
                $result = array_reverse($result);
            }
        }
        return $result;
    }

    public function convertToWgs84Box($n, $e, $sourceSystem, $order = 'lat-lng')
    {
        $swCoordinates = $this->convertToWgs84($n, $e, $sourceSystem, 'sw', $order);
        $neCoordinates = $this->convertToWgs84($n, $e, $sourceSystem, 'ne', $order);
        if ($neCoordinates === null || $swCoordinates === null) {
            return null;
        }
        return [$swCoordinates, $neCoordinates];
    }

    public function convertToYKJ($n, $e, $sourceSystem)
    {
        return $this->convertToSystem($n, $e, $sourceSystem, 'ykj');
    }

    private function convertToSystem($n, $e, $sourceSystem, $targetSystem, $location = 'c')
    {
        if (!$this->normalizeCoordinates($n, $e, $sourceSystem, $location)) {
            return null;
        }
        if (isset($this->posibleTypes[$targetSystem])) {
            $targetSystem = $this->posibleTypes[$targetSystem];
        }
        if ($sourceSystem === $targetSystem) {
            return [$n, $e];
        }
        return $this->getCoordinates($n, $e, $sourceSystem, $targetSystem);
    }

    private function getCoordinates($n, $e, $system, $target)
    {
        $systemKey = $n . ':' . $e . ':' . $system;
        $cacheKey = $systemKey . ':' . $target;
        if (array_key_exists($cacheKey, $this->cache)) {
            return $this->cache[$cacheKey];
        }
        $spot = array_search($systemKey, $this->lastKeys);
        if ($spot === false) {
            $spot = self::$spot = (++self::$spot) % self::CACHE_SIZE;
            $client = $this->getHttpClient();
            $url = $this->urlTemplate;
            $url = str_replace('%lat%', $n, $url);
            $url = str_replace('%lon%', $e, $url);
            $url = str_replace('%type%', $system, $url);
            $response = $client->setUri($url)->send();
            if (!$response->isOk()) {
                throw new \Exception('Communication to coordinate service failed');
            }
            $this->lastData[$spot] = json_decode($response->getBody(), true);
            $this->lastKeys[$spot] = $systemKey;
        }
        $raw = $this->lastData[$spot];
        $this->cache[$cacheKey] = null;
        if (is_array($raw) && isset($raw['conversion-response']) && isset($raw['conversion-response'][$target])) {
            $coordinates = $raw['conversion-response'][$target];
            if (isset($coordinates['lat']) && isset($coordinates['lon'])) {
                $this->cache[$cacheKey] = [0 => $coordinates['lat'], 1 => $coordinates['lon']];
            }
        }
        return $this->cache[$cacheKey];
    }

    public function normalizeCoordinateSystem($system)
    {
        if (strpos($system, 'MY.coordinateSystem') === 0) {
            $system = str_replace('MY.coordinateSystem', '', $system);
        }
        return lcfirst($system);
    }

    private function normalizeCoordinates(&$n, &$e, &$system, $location = 'c')
    {
        if (empty($n) || empty($e) || empty($system)) {
            return false;
        }
        $n = str_replace(',', '.', trim($n));
        $e = str_replace(',', '.', trim($e));
        $system = $this->normalizeCoordinateSystem($system);
        if (isset($this->posibleTypes[$system])) {
            $system = $this->posibleTypes[$system];
        }
        switch ($system) {
            case 'ykj':
                $nLen = strlen($n);
                if ($nLen < 7) {
                    switch ($location) {
                        case 'c':
                            $n = $n . '5';
                            $e = $e . '5';
                            break;
                        case 'ne':
                            $n++;
                            $e++;
                    }

                }
                $n = str_pad($n, 7, '0');
                $e = str_pad($e, 7, '0');
                break;
            case 'wgs84-decimal':
                $n = $this->dms2degrees($n, 'S');
                $e = $this->dms2degrees($e, 'W');
                break;
            case 'etrs-tm35fin':
                $nLen = strlen($n);
                $eLen = strlen($e);
                if ($nLen == $eLen && strpos($e, '8') === 0) {
                    $e = substr($e, 1);
                }
                if ($nLen !== 7) {
                    return false;
                }
            /*
             *   This can be used if accuracy is used in coordinates.
             *
             *   This relies on the fact that n coordinate must always have 7 characters
             *   if above is false assumption then this cannot be used

            $accuracy = 7 - $nLen;
            if ($accuracy) {
                $n = $n . str_repeat('5', $accuracy);
                $e = $e . str_repeat('5', $accuracy);
            }
            */
        }
        return true;
    }

    public function dms2degrees($dms, $negCharacter = '')
    {
        $parts = preg_split('/[^0-9\.,]/i', $dms, -1, PREG_SPLIT_NO_EMPTY);
        $flip = 1;
        if (preg_match('/[' . $negCharacter . '\-]/', $dms)) {
            $flip = -1;
        }
        $deg = isset($parts[0]) ? (float)$parts[0] : 0;
        $min = isset($parts[1]) ? (float)$parts[1] : 0;
        $sec = isset($parts[2]) ? (float)$parts[2] : 0;

        return '' . ($flip * ($deg + ((($min * 60) + ($sec)) / 3600)));
    }

    public static function roundWgsDegree($value, $decimals = 6)
    {
        $dotPos = strpos($value, '.');
        if ($dotPos !== false) {
            return substr($value, 0, ($dotPos + $decimals + 1));
        }
        return $value;
    }

    public static function addSuffixToWgs(&$lat, &$lon)
    {
        $lat = strpos($lat, '-') === 0 ? substr($lat, 1) . ' S' : $lat . ' N';
        $lon = strpos($lon, '-') === 0 ? substr($lon, 1) . ' W' : $lon . ' E';
    }

    public static function ykjAccuracy($coordinate, $radius = 0)
    {
        $radius = abs((int)$radius);
        $len = strlen($coordinate);
        switch ($len) {
            case 3:
                return 7200 + $radius; // meters
            case 4:
                return 720 + $radius;
            case 5:
                return 72 + $radius;
            case 6:
                return 8 + $radius;
            case 7:
                return $radius > 0 ? $radius : 1;
            default:
                return '';
        }

    }

    public static function ykj2wgs84($n, $e, $coordRadius = 0, $format = "dd", $wgsLat = null, $wgsLon = null)
    {
        $proj4 = new Proj4php();
        $proj4->addDef('EPSG:2393', '+proj=tmerc +lat_0=0 +lon_0=27 +k=1 +x_0=3500000 +y_0=0 +ellps=intl +towgs84=-96.0617,-82.4278,-121.7535,4.80107,0.34543,-1.37646,1.4964 +units=m +no_defs');
        $projYkj = new Proj('EPSG:2393', $proj4);
        $projWgs84 = new Proj('WGS84', $proj4);

        $ret = [];
        $ret['accuracy'] = self::ykjAccuracy($n, $coordRadius);

        if ($wgsLat !== null && $wgsLon !== null) {
            $array = ['N' => $wgsLat, 'E' => $wgsLon];
        } else {
            $n = $n . "5000000";
            $n = substr($n, 0, 7);

            $e = $e . "5000000";
            $e = substr($e, 0, 7);
            
            $transformed = $proj4->transform($projWgs84, new Point($e, $n, $projYkj))->toArray();
            $array = ['N' => $transformed[1], 'E' => $transformed[0]];
        }

        $n = round($array['N'], 6);
        $e = round($array['E'], 6);

        if ("dms" == $format) {
            $ret['N'] = self::DDtoDMS($n);
            $ret['E'] = self::DDtoDMS($e);
        } else { // default
            $ret['N'] = $n;
            $ret['E'] = $e;
        }

        return $ret;
    }

    public static function DDtoDMS($dd)
    {

        // Converts decimal longitude / latitude to DMS
        // ( Degrees / minutes / seconds )

        // This is the piece of code which may appear to
        // be inefficient, but to avoid issues with floating
        // point math we extract the integer part and the float
        // part by using a string function.

        $vars = explode(".", $dd);
        $deg = $vars[0];
        if (isset($vars[1])) {
            $tempma = "0." . $vars[1];
            $tempma = $tempma * 3600;
            $min = floor($tempma / 60);
            $sec = floor($tempma - ($min * 60));
        } else {
            $min = 0;
            $sec = 0;
        }
        return "{$deg}°$min'$sec\"";
    }
}