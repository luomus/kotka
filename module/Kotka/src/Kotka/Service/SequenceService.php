<?php
namespace Kotka\Service;

use Kotka\Controller\PickerController;
use Triplestore\Db\Oracle;
use Triplestore\Service\ObjectManager;
use Zend\Mvc\Router\RouteMatch;

class SequenceService
{
    private $om;

    public function __construct(ObjectManager $om)
    {
        $this->om = $om;
    }

    /**
     * @param $key
     * @return string
     */
    public function next($key) {
        /** @var Oracle $connection */
        $connection = $this->om->getConnection();
        $dbName = $connection->getDatabase();
        $connection->startTransaction();
        $sql = "
        DECLARE
            rc sys_refcursor;
        BEGIN
            MERGE INTO $dbName.KOTKA_SEQ trg
                USING (SELECT :k1 AS key, 1 AS value FROM DUAL) src ON (src.key = trg.key)
                WHEN NOT MATCHED THEN INSERT(key, value) VALUES
                    (src.key, src.value)
                WHEN MATCHED THEN UPDATE
                    SET trg.value = trg.value + 1;
            OPEN rc FOR SELECT * FROM $dbName.KOTKA_SEQ WHERE key = :k1;
            dbms_sql.return_result(rc);
        END;";
        $data = $connection->executeSelectSql($sql, ['k1' => $key]);
        $connection->commit();
        foreach ($data as $row) {
            return $row['VALUE'];
        }
        return null;
    }

    public function current($key) {
        /** @var Oracle $connection */
        $connection = $this->om->getConnection();
        $dbName = $connection->getDatabase();
        $sql = "SELECT * from $dbName.KOTKA_SEQ where key = :k1";
        $data = $connection->executeSelectSql($sql, ['k1' => $key]);
        return isset($data) && isset($data[0]) ? $data[0]['VALUE'] : null;
    }

}