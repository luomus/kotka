<?php
namespace Kotka\Mapper;

use Kotka\Triple\MFSample;
use Triplestore\Stdlib\ObjectRepository;

/**
 * Interface MFSampleMapperInterface hides the database logic behind this interface
 * so that this can be used as common entry point for all the database connections
 *
 * @package Kotka\Mapper
 */
interface MFSampleMapperInterface extends ObjectRepository
{
    /**
     * Dataset create method
     *
     * @param  MFSample $sample
     * @return MFSample
     */
    public function create(MFSample $sample);

    /**
     * Dataset update method
     * @param  MFSample $dataset
     * @return MFSample
     */
    public function update(MFSample $dataset);

}
