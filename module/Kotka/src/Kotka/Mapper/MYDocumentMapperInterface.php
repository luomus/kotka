<?php
namespace Kotka\Mapper;

use Kotka\Triple\MYDocument;
use Triplestore\Stdlib\ObjectRepository;

/**
 * Interface SpecimenMapperInterface hides the database logic behind this interface
 * so that this can be used as common entry point for all the database connections
 *
 * @package Kotka\Mapper
 */
interface MYDocumentMapperInterface extends ObjectRepository
{

    /**
     * Creates new specimen
     * @param  MYDocument $specimen
     * @return MYDocument
     */
    public function create(MYDocument $specimen);

    /**
     * Updates existing specimen
     * @param  MYDocument $specimen
     * @return MYDocument
     */
    public function update(MYDocument $specimen);

}
