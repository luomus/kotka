<?php
namespace Kotka\Mapper;

use Kotka\Triple\HRATransaction;
use Triplestore\Stdlib\ObjectRepository;

/**
 * Interface HRATransactionMapperInterface hides the database logic behind this interface
 * so that this can be used as common entry point for all the database connections
 *
 * @package Kotka\Mapper
 */
interface HRATransactionMapperInterface extends ObjectRepository
{
    /**
     * Creates new transaction
     *
     * @param  HRATransaction $transaction
     * @return HRATransaction
     */
    public function create(HRATransaction $transaction);

    /**
     * Updates existing transaction
     *
     * @param  HRATransaction $transaction
     * @return HRATransaction
     */
    public function update(HRATransaction $transaction);

}
