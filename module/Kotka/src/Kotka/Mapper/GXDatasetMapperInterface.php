<?php
namespace Kotka\Mapper;

use Kotka\Triple\GXDataset;
use Triplestore\Stdlib\ObjectRepository;

/**
 * Interface GXDatasetMapperInterface hides the database logic behind this interface
 * so that this can be used as common entry point for all the database connections
 *
 * @package Kotka\Mapper
 */
interface GXDatasetMapperInterface extends ObjectRepository
{
    /**
     * Dataset create method
     *
     * @param  GXDataset $dataset
     * @return GXDataset
     */
    public function create(GXDataset $dataset);

    /**
     * Dataset update method
     * @param  GXDataset $dataset
     * @return GXDataset
     */
    public function update(GXDataset $dataset);

}
