<?php
namespace Kotka\Mapper;

use Kotka\Triple\MYCollection;
use Triplestore\Stdlib\ObjectRepository;

/**
 * Interface MYCollectionMapperInterface hides the database logic behind this interface
 * so that this can be used as common entry point for all the database connections
 *
 * @package Kotka\Mapper
 */
interface MYCollectionMapperInterface extends ObjectRepository
{
    /**
     * Collections create method
     *
     * @param  MYCollection $collection
     * @return MYCollection
     */
    public function create(MYCollection $collection);

    /**
     * Collection update method
     *
     * @param  MYCollection $collection
     * @return MYCollection
     */
    public function update(MYCollection $collection);

}
