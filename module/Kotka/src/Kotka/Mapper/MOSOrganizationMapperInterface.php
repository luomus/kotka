<?php
namespace Kotka\Mapper;

use Kotka\Triple\MOSOrganization;
use Triplestore\Stdlib\ObjectRepository;

/**
 * Interface OrganizationMapperInterface hides the database logic behind this interface
 * so that this can be used as common entry point for all the database connections
 *
 * @package Kotka\Mapper
 */
interface MOSOrganizationMapperInterface extends ObjectRepository
{
    /**
     * Creates new organization
     *
     * @param  MOSOrganization $organization
     * @return MOSOrganization
     */
    public function create(MOSOrganization $organization);

    /**
     * Updates existing organization
     *
     * @param  MOSOrganization $organization
     * @return MOSOrganization
     */
    public function update(MOSOrganization $organization);

}
