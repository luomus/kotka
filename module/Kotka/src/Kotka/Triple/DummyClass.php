<?php

namespace Kotka\Triple;

use Triplestore\Model\Model;

class DummyClass extends KotkaEntity
{

    public function setMZDateCreated($datetime)
    {

    }

    public function getMZDateCreated()
    {

    }

    public function setMZDateEdited($datetime)
    {

    }

    public function getMZDateEdited()
    {

    }

    public function setMZEditor($name)
    {

    }

    public function getMZEditor()
    {

    }

    public function setMZCreator($organizations)
    {

    }

    public function getMZCreator()
    {

    }

    public function setMZOwner($owner)
    {

    }

    public function getMZOwner()
    {

    }

    /**
     * Exchange internal values from provided Model
     *
     * @param  Model $array
     *
     * @return void
     */
    public function exchangeModel(Model $array)
    {

    }

    /**
     * Return an Model representation of the object
     *
     * @return Model
     */
    public function getModelCopy()
    {

    }

    /**
     * Returns the rdf:type value
     *
     * @return mixed
     */
    public function getType()
    {

    }
}