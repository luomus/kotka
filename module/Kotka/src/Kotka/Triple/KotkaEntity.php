<?php

namespace Kotka\Triple;

use Common\Service\IdService;
use Triplestore\Stdlib\AbstractOntology;

abstract class KotkaEntity extends AbstractOntology implements KotkaEntityInterface
{

    public function getUri()
    {
        $subject = $this->getSubject();
        if ($subject === null || $subject == '') {
            return null;
        }
        return IdService::getUri($this->getSubject());
    }

    public function setUri($uri)
    {
        $subject = IdService::getQName($uri, true);
        $this->setSubject($subject);

        return $this;
    }
}