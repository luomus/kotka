<?php
namespace Kotka\Triple;

use Triplestore\Stdlib\SubjectAwareInterface;

interface KotkaEntityInterface extends SubjectAwareInterface
{

    const RESTRICTION_PREDICATE = 'MZ.publicityRestrictions';
    const RESTRICTION_PUBLIC = 'MZ.publicityRestrictionsPublic';
    const RESTRICTION_PRIVATE = 'MZ.publicityRestrictionsPrivate';

    public function setUri($uri);

    public function getUri();

    public function setMZDateCreated($datetime);

    public function getMZDateCreated();

    public function setMZDateEdited($datetime);

    public function getMZDateEdited();

    public function setMZEditor($name);

    public function getMZEditor();

    public function setMZCreator($organizations);

    public function getMZCreator();

    public function setMZOwner($owner);

    public function getMZOwner();

} 