<?php
namespace Kotka\Job;

use ImageApi\Model\PictureMeta;
use ImageApi\Service\ImageService;
use ImageApi\Util\ArrayUtil;
use Kotka\Form\ImageUpload;
use Kotka\Service\EmailService;
use Kotka\Service\SpecimenService;
use Kotka\Validator\File\ImageName;
use Kotka\Validator\ImagesPath;
use RuntimeException;
use SlmQueue\Job\AbstractJob;
use Zend\Log\LoggerInterface;
use Zend\Validator\File\IsImage;
use Zend\Validator\File\Size;
use Zend\Validator\ValidatorChain;
use Zend\Validator\ValidatorInterface;

class Images extends AbstractJob
{

    const DONE_DIRECTORY = 'Done';

    private $imageService;
    private $specimenService;
    private $logger;
    private $mailer;
    private $skipped = [];
    private $filesToBeSkipped = ['..', '.', 'Thumbs.db'];

    protected $content = array();

    public function __construct(ImageService $imageService = null, SpecimenService $specimenService = null, LoggerInterface $logger = null, EmailService $mailer = null)
    {
        $this->imageService = $imageService;
        $this->specimenService = $specimenService;
        $this->logger = $logger;
        $this->mailer = $mailer;
    }

    public function execute()
    {
        $data = $this->getContent();
        $email = [];
        if (!isset($data['meta']) || !isset($data['path']) || !isset($data['format']) || !isset($data['email']) || !isset($data['name'])) {
            $message = 'Image job failed! Missing job data!';
            $this->logger->err($message . ' ' . json_encode($data));
            throw new RuntimeException($message);
        }
        $email['name'] = $data['name'];
        $email['path'] = $data['path'];
        $userID = $data['userID'];
        $this->mailer->addTo($data['email'], $data['name']);
        $this->mailer->setSubject('Kotka image import');

        $path = ImagesPath::UserToLocalFile($data['path']) . DIRECTORY_SEPARATOR;
        if (!is_dir($path)) {
            $message = "Image job failed! Path '$path' is not a directory!";
            $this->logger->err($message);
            $this->mailer->sendTemplateEmail('image-import', $email);
            throw new RuntimeException($message);
        }
        if (!is_writable($path)) {
            $message = "Image job failed! Path '$path' is not readable!";
            $this->logger->err($message);
            $this->mailer->sendTemplateEmail('image-import', $email);
            throw new RuntimeException($message);
        }
        $files = array_diff(scandir($path), $this->filesToBeSkipped);
        $validator = $this->getFileValidator();
        $this->prepareDoneDirectory($path);
        $errors = [];
        foreach ($files as $file) {
            $meta = new PictureMeta();
            $meta->exchangeArray($data['meta']);
            $meta->setUploadedBy($userID);
            if ($this->processFile($file, $data['format'], $path, $validator, $meta) === false) {
                $errors[] = $file;
            }  else {
              $documentIds = $meta->getDocumentIds();

              foreach ($documentIds as $documentId) {
                  $pos = strpos($documentId, 'MX.');
                  if ($pos === false || $pos !== 0) {
                      isset($documents[$documentId]) ? $documents[$documentId]++ : $documents[$documentId] = 1;
                  }
              }
           }
        }
        try {
          if ($documents) {
            $document_keys = array_keys($documents);
            $this->specimenService->imageUploadResend($document_keys);
          }
        } catch (\Exception $e) {
          $this->logger->err("Image job failed to start document resend! " . $e->getMessage());
        }
        if (count($this->skipped) > 0) {
            $email['skipped'] = $this->skipped;
        }
        $email['errors'] = $errors;
        try {
            $this->mailer->sendTemplateEmail('image-import', $email);
        } catch (\Exception $e) {
            $this->logger->err("Image job emailing failed to {$data['email']}! " . $e->getMessage());
        }

        return null;
    }

    private function processFile($file, $format, $path, ValidatorInterface $validator, \ImageApi\Model\PictureMeta $meta)
    {
        $fullPath = $path . $file;
        if (is_dir($fullPath)) {
            return true;
        }
        if ($validator->isValid($fullPath)) {
            $meta->setOriginalFilename($file);
            if ($this->imageService->uploadPicture($fullPath, $meta, $format, true) === false) {
                $reason = $this->imageService->getResponseMessage();
                $this->logger->err("Image job Failed to add file '$file' reason: $reason");
                return false;
            } else {
                if (rename($fullPath, $path . self::DONE_DIRECTORY . DIRECTORY_SEPARATOR . $file) === false) {
                    $this->logger->warn("Image job file $file fail");
                }
            }
        } else {
            $this->skipped[] = $file . ' reason: ' . ArrayUtil::multi_implode(', ', $validator->getMessages());
            $this->logger->info("Image job file $file failed file validators " . json_encode($validator->getMessages()));
        }
        return true;
    }

    private function prepareDoneDirectory($path)
    {
        $donePath = $path . self::DONE_DIRECTORY;
        if (is_dir($donePath)) {
            return;
        }
        if (mkdir($path . self::DONE_DIRECTORY) === false) {
            throw new RuntimeException('Image job failed! Unable to create ' . self::DONE_DIRECTORY . ' in path ' . $path);
        }
    }

    private function getFileValidator()
    {
        $chain = new ValidatorChain();
        $isImage = new IsImage();
        $fileSize = new Size();
        $fileSize->setMax(ImageUpload::MAX_FILE_SIZE);
        $imgName = new ImageName();

        $chain->attach($fileSize);
        $chain->attach($isImage);
        $chain->attach($imgName);

        return $chain;
    }
} 