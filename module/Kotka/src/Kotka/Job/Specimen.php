<?php
namespace Kotka\Job;


use ImageApi\Model\Media;
use ImageApi\Service\ImageService;
use Kotka\Enum\Document as DocumentEnum;
use Common\Service\IdService;
use Kotka\Service\MustikkaService;
use Kotka\Service\SpecimenService;
use Kotka\Triple\MYDocument;
use SlmQueue\Job\AbstractJob;
use Triplestore\Model\Object;
use Triplestore\Model\Predicate;
use Triplestore\Service\ObjectManager;
use Triplestore\Stdlib\Hydrator\HydratorInterface;

class Specimen extends AbstractJob
{

    const WGS_84_LAT_PREDICATE = 'MY.wgs84Latitude';
    const WGS_84_LON_PREDICATE = 'MY.wgs84Longitude';

    private $objectManager;
    private $mustikkaService;
    private $specimenService;
    private $imageService;

    protected $content = array();

    public function __construct(ObjectManager $objectManager = null, MustikkaService $mustikkaService = null, SpecimenService $specimenService = null, ImageService $imageService = null)
    {
        $this->objectManager = $objectManager;
        $this->mustikkaService = $mustikkaService;
        $this->specimenService = $specimenService;
        $this->imageService = $imageService;
    }

    public function setSubject($qname)
    {
        $this->content['id'] = $qname;

        return $this;
    }

    public function setResendToMustikka($resend)
    {
        $this->content['resend'] = (bool)$resend;

        return $this;
    }

    public function execute()
    {
        $objectManager = $this->objectManager;
        $objectManager->clear();
        $objectManager->getConnection()->setKeepHistory(false);
        $payload = $this->getContent();
        $id = $payload['id'];
        $resend = isset($payload['resend']);
        /** @var MYDocument $specimen */
        $specimen = $this->specimenService->getById($id);
        if (is_null($specimen)) {
            return;
        }

        // Add WGS84 coordinates to gatherings
        $gatherings = $specimen->getMYGathering();
        foreach ($gatherings as $gathering) {
            /** @var \Kotka\Triple\MYGathering $gathering */
            if ($this->specimenService->setGatheringWgsCoordinate($gathering)) {
                $model = $gathering->getModelCopy();
                $objectManager->updatePredicate($model->getSubject(), $model->getPredicate(self::WGS_84_LAT_PREDICATE));
                $objectManager->updatePredicate($model->getSubject(), $model->getPredicate(self::WGS_84_LON_PREDICATE));
            }
        }

        // Change images publicity if different from specimen
        $secret = !SpecimenService::isPublic($specimen);
        $medias = $this->imageService->getImages(IdService::getUri($id));
        if (is_array($medias)) {
            foreach ($medias as $media) {
                /** @var Media $media */
                if ($media->getMeta()->getSecret() != $secret) {
                    $media->getMeta()->setSecret($secret);
                    $this->imageService->updateMeta($media->getId(), $media->getMeta());
                }
            }
        }
    }

} 