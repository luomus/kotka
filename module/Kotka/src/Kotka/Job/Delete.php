<?php
namespace Kotka\Job;


use Elastic\Client\Index;
use Elastic\Query\Query;
use Kotka\ElasticExtract\Specimen as SpecimenExtract;
use Kotka\ElasticExtract\Sample as SampleExtract;
use Kotka\ElasticExtract\Branch as BranchExtract;
use Common\Service\IdService;
use Kotka\Service\MustikkaService;
use Kotka\Service\SpecimenService;
use Kotka\Triple\MYDocument;
use SlmQueue\Job\AbstractJob;
use Triplestore\Service\ObjectManager;
use Zend\Db\Adapter\Adapter;

class Delete extends AbstractJob
{

    /** @var ObjectManager */
    private $objectManager;
    /** @var MustikkaService */
    private $mustikkaService;
    /** @var SpecimenService */
    private $specimenService;
    /** @var Adapter */
    private $adapter;
    /** @var Index */
    private $indexer;

    protected $content = array();

    public function __construct(ObjectManager $objectManager = null, MustikkaService $mustikkaService = null, SpecimenService $specimenService = null, Adapter $adapter = null, Index $indexer = null)
    {
        $this->objectManager = $objectManager;
        $this->mustikkaService = $mustikkaService;
        $this->specimenService = $specimenService;
        $this->adapter = $adapter;
        $this->indexer = $indexer;
    }

    public function setSubject($qname)
    {
        $this->content['id'] = $qname;

        return $this;
    }

    public function execute()
    {
        $objectManager = $this->objectManager;
        $mustikkaService = $this->mustikkaService;
        $payload = $this->getContent();
        $id = $payload['id'];
        $specimen = $this->specimenService->getById($id);
        if (!$specimen instanceof MYDocument) {
            return;
        }

        // Make sure that deletion is really wanted!
        $toBeDeleted = $specimen->getMZScheduledForDeletion();
        if ($toBeDeleted !== 'true' && $toBeDeleted !== true) {
            return;
        }

        // Delete from triplestore
        $objectManager->remove($specimen);
        $objectManager->commit();

        // Delete from elastic specimen index
        $queryStr = 'documentQName: ("' . IdService::getQName($id) . '")';
        $query = new Query(SpecimenExtract::getIndexName(), SpecimenExtract::getTypeName());
        $query->setQueryString($queryStr);
        try {
            $this->indexer->deleteByQuery($query);
        } catch (\Exception $e) {
        }

        // Sample index
        $query = new Query(SampleExtract::getIndexName(), SampleExtract::getTypeName());
        $query->setQueryString($queryStr);
        try {
            $this->indexer->deleteByQuery($query);
        } catch (\Exception $e) {
        }

        // Branch index
        $query = new Query(BranchExtract::getIndexName(), BranchExtract::getTypeName());
        $query->setQueryString($queryStr);
        try {
            $this->indexer->deleteByQuery($query);
        } catch (\Exception $e) {
        }

        // Delete from Mustikka
        $mustikkaService->deleteSpecimen(IdService::getUri($id));
    }

} 