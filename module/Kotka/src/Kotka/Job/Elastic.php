<?php
namespace Kotka\Job;


use Elastic\Client\Index;
use Elastic\Extract\ExtractInterface;
use Elastic\Query\Query;
use Kotka\ElasticExtract\Branch;
use Kotka\ElasticExtract\Sample;
use Kotka\ElasticExtract\Specimen as SpecimenExtract;
use Common\Service\IdService;
use SlmQueue\Job\AbstractJob;
use SlmQueue\Queue\QueueAwareInterface;
use SlmQueue\Queue\QueueAwareTrait;
use Zend\Db\Adapter\Adapter;
use Zend\Log\Logger;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class Elastic extends AbstractJob implements QueueAwareInterface
{

    use QueueAwareTrait;

    const MAX_SIZE = 50;

    /** @var Adapter */
    private $adapter;
    /** @var Index */
    private $indexer;
    /** @var ServiceLocatorInterface */
    private $serviceLocator;

    /** @var Logger */
    private $logger;

    private $cache = [];

    protected $content = array();

    public function __construct(Adapter $adapter = null, Index $indexer = null, ServiceLocatorInterface $serviceLocator = null,  Logger $logger = null)
    {
        $this->adapter = $adapter;
        $this->indexer = $indexer;
        $this->serviceLocator = $serviceLocator;
        $this->logger = $logger;
    }

    public function execute()
    {
        $adapter = $this->adapter;
        $indexer = $this->indexer;
        $payload = $this->getContent();
        $ids = $payload['ids'];
        $type = isset($payload['type']) ? $payload['type'] : 'Specimen';
        $extractor = $this->getExtractor($type);
        if ($type === 'Specimen') {
            $this->addMustikkaJob($ids);
        }

        array_walk($ids, array($this, 'toQName'));
        if (!$extractor instanceof ExtractInterface || empty($ids)) {
            $this->logger->info('ES: No indexable data found to!');
            return;
        }
        $delete = new Query($extractor::getIndexName(), $extractor::getTypeName());
        $queryStr = 'documentQName: ("' . implode('" OR "', $ids) . '")';
        $delete->setQueryString($queryStr);
        try {
            $indexer->deleteByQuery($delete);
        } catch (\Exception $e) {
            $this->logger->info('ES: Deleting failed', $e);
        }

        if ($type === 'Specimen') {
            $delete = new Query(Sample::getIndexName(), Sample::getTypeName());
            $queryStr = 'documentQName: ("' . implode('" OR "', $ids) . '")';
            $delete->setQueryString($queryStr);
            try {
                $indexer->deleteByQuery($delete);
            } catch (\Exception $e) {
                $this->logger->info('ES: Deleting sample failed', $e);
            }

            $delete = new Query(Branch::getIndexName(), Branch::getTypeName());
            $queryStr = 'accessionID: ("' . implode('" OR "', $ids) . '")';
            $delete->setQueryString($queryStr);
            try {
                $indexer->deleteByQuery($delete);
            } catch (\Exception $e) {
                $this->logger->info('ES: Deleting branch failed', $e);
            }
        }

        try {
            list($sql, $params) = $this->getInClause($ids);
            $results = $adapter->query($sql)->execute($params);
            $indexer->indexResults($results, $extractor);
        } catch (\Exception $e) {
            $this->logger->crit('ES: Failed to add to Elastic search!', $e);
        }
    }

    private function toQName(&$value)
    {
        $value = IdService::getQName($value);
    }

    private function getExtractor($type)
    {
        $className = '\Kotka\ElasticExtract\\' . ucfirst($type);
        if (!isset($this->cache[$className])) {
            if (class_exists($className)) {
                $instance = new $className;
            } else {
                $instance = new SpecimenExtract();
            }
            if ($instance instanceof ServiceLocatorAwareInterface) {
                $instance->setServiceLocator($this->serviceLocator);
            }
            if (method_exists($instance, 'init')) {
                $instance->init();
            }
            $this->cache[$className] = $instance;
        }
        return $this->cache[$className];
    }

    private function getInClause($array)
    {
        $params = [];
        $idx = 0;
        foreach($array as $value) {
            $key = ':ESV' . ($idx++);
            $params[$key] = str_replace(IdService::DEFAULT_DB_QNAME_PREFIX, '', $value);
        }
        $sql = implode(',',array_keys($params));
        return [$sql, $params];
    }

    private function addMustikkaJob($ids)
    {
        $job = new Mustikka();
        $job->setSubjects($ids);
        $this->getQueue()->push($job);
    }

} 