<?php
namespace Kotka\Job;


use Elastic\Client\Index;
use Elastic\Query\Query;
use Kotka\ElasticExtract\Specimen as SpecimenExtract;
use Common\Service\IdService;
use Kotka\Log\Formatter\PriorityConditional;
use Kotka\Service\ImportService;
use Kotka\Service\MustikkaService;
use Kotka\Service\SpecimenService;
use Kotka\Triple\MAPerson;
use SlmQueue\Job\AbstractJob;
use Triplestore\Classes\MAPersonInterface;
use Triplestore\Service\ObjectManager;
use Zend\Db\Adapter\Adapter;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\Log\Formatter\Simple;
use Zend\Log\Logger;
use Zend\Log\Writer\Stream;
use Zend\Stdlib\SplPriorityQueue;

class ImportExcel extends AbstractJob implements ListenerAggregateInterface
{
    const DONE_STRING = '<!--DONE-->';
    const VALIDATION_ERROR_STRING = '<!--VALIDATION ERROR-->';
    const ERROR_STRING = '<!--ERROR-->';

    /** @var ImportService */
    private $importService;
    private $logger;
    private $om;
    private $user;
    /** @var Logger */
    private $userLog;

    protected $content = array();

    public function __construct(ImportService $importService = null, Logger $logger = null,  ObjectManager $om = null, $user = null)
    {
        $this->importService = $importService;
        $this->logger = $logger;
        $this->om = $om;
        $this->user = $user;
    }

    public function execute()
    {
        $payload = $this->getContent();
        $file = $payload['import']['tmp_name'];
        $sheet = isset($payload['sheet']) && $payload['sheet'] !== '' ? $payload['sheet'] : 0;
        $importService = $this->importService;
        $importService->setOptions([]); // reset the options give!
        $importService->setMaxRows((isset($payload['huge']) && $payload['huge']) ?
            ImportService::MAX_HUGE_ROW_SIZE :
            ImportService::MAX_EXCEL_ROW_SIZE
        );
        $data = $importService->Excel2Array($file, $sheet);
        if (isset($payload['override']) && ($payload['override'] === '1' || $payload['override'] === 1 || $payload['override'] === true)) {
            $this->logger->notice($payload['user'] . " Import is overriding!");
            $importService->setOptions([ImportService::OPTION_USER_FROM_DATA]);
        }

        $this->userLog = $this->getUserLog($payload['user']);
        if (!is_array($data)) {
            $this->logger->err('Unable to load data for the job to handle it! File: ' . $file . ' sheet:' . $sheet);
            $this->userLog->crit('Unable to load data <strong>nothing imported</strong>!<br>Caused by ' . $importService->getErrorCause() . self::DONE_STRING . self::ERROR_STRING);
            $this->clearWriteLock();
            return;
        }
        $this->switchToUser($payload['user'], $payload['organization'], $payload['defaultNS']);
        $success = $importService->validateArray($data, $payload['datatype'], $payload['organization']);

        if (!$success) {
            $this->userLog->crit('Data contained errors <strong>nothing imported</strong>!');
            $this->userLog->debug('Following errors found:<br>' . $this->getErrorMessages($data) . self::DONE_STRING . self::VALIDATION_ERROR_STRING);
            $this->clearWriteLock();
            return;
        }
        try {
            $em = $importService->getEventManager();
            $em->clearListeners('import');
            $importService->getEventManager()->attachAggregate($this);
            $importService->importArray($data);
        } catch(\Exception $e) {
            $this->logger->err('Saving import failed! ' . $e->getMessage());
            $this->userLog->crit('<strong>Unable to save rest!</strong>' . self::DONE_STRING . self::ERROR_STRING);
        }
        $this->clearWriteLock();
    }

    private function getErrorMessages($data) {
        $result = '';
        if (!is_array($data)) {
            return $result;
        }
        $result = '<table class="table">';
        foreach ($data as $document) {
            if (!isset($document['errors'])) {
                continue;
            }
            $result .= sprintf('<tr><td>%s</td><td>%s</td></tr>',
                $this->getSubject($document),
                $this->getErrors($document, $document['errors'])
            );
        }
        $result .= '</table>';
        return $result;
    }

    private function getSubject($document) {
        if (isset($document['subject'])) {
            return $document['subject'];
        } elseif (isset($document['MYNamespaceID']) && isset($document['MYObjectID'])) {
            return $document['MYNamespaceID'] . '.' . $document['MYObjectID'];
        }
        return 'no id';
    }

    private function getErrors($document, $errors, $parent = '', $result = '') {
        foreach ($errors as $location => $value) {
            if (isset($document[$location])) {
                $result .= $this->getErrors(
                    $document[$location],
                    $value,
                    $parent === '' ? $location : $parent . '[' .$location . ']'
                ) . '<br>';
            } else {
                $result .= $parent . ':<br>' . join('<br>', $this->getErrorMsgs($value));
            }
        }
        return $result;
    }

    private function getErrorMsgs($errors, &$msg = []) {
        if (is_string($errors)) {
            $msg[] = $errors;
        } elseif (is_array($errors)) {
            foreach ($errors as $message) {
                $this->getErrorMsgs($message, $msg);
            }
        }
        return $msg;
    }

    private function getUserLog($qname) {
        $writer = new Stream(self::getUserImportLogFile($qname));
        $format = new PriorityConditional([
            Logger::CRIT => '<div class="alert alert-danger">%message%</div>',
            Logger::ERR  => '<div class="user-log error">%message% <spcn class="badge badge-danger">failed</spcn> <small>(%timestamp%)</small></div>',
            Logger::INFO => '<div class="user-log success"><a href="/view?uri=%message%">%message%</a> <spcn class="badge badge-success">ok</spcn> <small>(%timestamp%)</small></div>',
            PriorityConditional::DEFAULT_KEY => '%message%',
        ]);
        $writer->setFormatter($format);
        $logger = new Logger();
        $queue = new SplPriorityQueue();
        $queue->insert($writer, 1);
        $logger->setWriters($queue);

        return $logger;
    }

    private function switchToUser($qname, $organization, $defaultNS) {
        if (is_array($qname)) {
            $qname = current($qname);
        }
        $qname = trim($qname);
        if ($this->user instanceof MAPersonInterface && $this->user->getSubject() === $qname) {
            $this->user->setMADefaultQNamePrefix($defaultNS);
            return;
        }
        if (!$this->user instanceof MAPersonInterface) {
            echo "user is not person!!!!\n";
            return;
        }
        $this->om->enableHydrator();
        $newUser = $this->om->className(MAPerson::class)->fetch($qname);
        if ($newUser instanceof MAPersonInterface) {
            $methods = get_class_methods($this->user);
            foreach($methods as $setMethod) {
                if (strpos($setMethod, 'set') === 0) {
                    if ($setMethod === 'setMAOrganisation') {
                        $organizations = $newUser->getMAOrganisation();
                        if (!is_array($organizations)) {
                            $organizations = [];
                        }
                        $result = [];
                        foreach ($organizations as $org) {
                            $result[$org] = $org;
                        }
                        $this->user->setMAOrganisation($result);
                    } else {
                        $getMethod = 'get' . substr($setMethod, 3);
                        $this->user->$setMethod($newUser->$getMethod());
                    }
                }
            }
        } else {
            echo "Couldn't find user '$qname'\n";
            $this->user->setSubject(IdService::getQName($qname, true));
            $this->user->setMAOrganisation([]);
        }
        $this->user->setMADefaultQNamePrefix($defaultNS);
    }

    public static function getUserImportLogFile($qname) {
        return './data/user-logs/import/' . $qname . '.log';
    }

    private function clearWriteLock() {
        if (!$this->userLog instanceof Logger) {
            return;
        }
        unset($this->userLog);
    }

    /**
     * Attach one or more listeners
     *
     * Implementors may add an optional $priority argument; the EventManager
     * implementation will pass this to the aggregate.
     *
     * @param EventManagerInterface $events
     *
     * @return void
     */
    public function attach(EventManagerInterface $events)
    {
        $events->attach('import', array($this, 'userLog'));
    }

    public function userLog($e) {
        $params = $e->getParams();
        switch($params['action']) {
            case 'success':
                $this->userLog->info(IdService::getUri($params['subject']));
                break;
            case 'failed':
                $this->logger->crit('Import job failed to save ' . $params['subject']);
                $this->userLog->err(IdService::getUri($params['subject']) . self::ERROR_STRING);
                break;
            case 'done':
                $this->userLog->debug(self::DONE_STRING);
                break;
        }
    }

    /**
     * Detach all previously attached listeners
     *
     * @param EventManagerInterface $events
     *
     * @return void
     */
    public function detach(EventManagerInterface $events)
    {

    }
}