<?php
namespace Kotka\Job;

use Zend\Db\Adapter\Adapter;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class DeleteFactory implements FactoryInterface
{

    public function createService(ServiceLocatorInterface $sl)
    {
        /** @var \Kotka\Service\MustikkaService $mustikkaService */
        $mustikkaService = $sl->getServiceLocator()->get('Kotka\Service\MustikkaService');
        $objectManager = $sl->getServiceLocator()->get('Triplestore\ObjectManager');
        $specimenService = $sl->getServiceLocator()->get('Kotka\Service\SpecimenService');
        /** @var Adapter $adapter */
        $adapter = $sl->getServiceLocator()->get('Triplestore\Db\Adapter\Array');
        /** @var \Elastic\Client\Index $indexer */
        $indexer = $sl->getServiceLocator()->get('Elastic\Client\Index');

        return new Delete($objectManager, $mustikkaService, $specimenService, $adapter, $indexer);
    }

} 