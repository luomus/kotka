<?php
namespace Kotka\Job;

use Zend\Log\Logger;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class MustikkaFactory implements FactoryInterface
{

    public function createService(ServiceLocatorInterface $sl)
    {
        $parentServiceLocator = $sl->getServiceLocator();
        /** @var \Kotka\Service\MustikkaService $mustikkaService */
        $mustikkaService = $parentServiceLocator->get('Kotka\Service\MustikkaService');
        /** @var /Triplestore\Service\ObjectManager $objectManager */
        $objectManager = $parentServiceLocator->get('Triplestore\ObjectManager');
        /** @var Logger $logger */
        $logger = $parentServiceLocator->get('Logger');

        return new Mustikka($objectManager, $mustikkaService, $logger);
    }

} 