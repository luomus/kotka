<?php
namespace Kotka\Job;

use Zend\Db\Adapter\Adapter;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ElasticFactory implements FactoryInterface
{

    public function createService(ServiceLocatorInterface $sl)
    {
        /** @var ServiceLocatorInterface $parentServiceLocator */
        $parentServiceLocator = $sl->getServiceLocator();
        /** @var \Elastic\Client\Index $indexer */
        $indexer = $parentServiceLocator->get('Elastic\Client\Index');
        /** @var Adapter $adapter */
        $adapter = $parentServiceLocator->get('Triplestore\Db\Adapter\Array');
        /** @var \SlmQueue\Queue\QueueInterface $queue */
        $queue = $parentServiceLocator->get('SlmQueue\Queue\QueuePluginManager')->get('default');
        /** @var Logger $logger */
        $logger = $parentServiceLocator->get('Logger');

        $job = new Elastic($adapter, $indexer, $parentServiceLocator, $logger);
        $job->setQueue($queue);

        return $job;
    }

} 