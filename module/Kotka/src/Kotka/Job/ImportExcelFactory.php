<?php
namespace Kotka\Job;

use Zend\Log\Logger;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ImportExcelFactory implements FactoryInterface
{

    public function createService(ServiceLocatorInterface $sl)
    {
        $parentServiceLocator = $sl->getServiceLocator();
        /** @var \Kotka\Service\ImportService $importService */
        $importService = $parentServiceLocator->get('Kotka\Service\ImportService');
        /** @var Logger $logger */
        $logger = $parentServiceLocator->get('Logger');
        /** @var \Triplestore\Service\ObjectManager $om */
        $om = $parentServiceLocator->get('Triplestore\ObjectManager');
        $user = $parentServiceLocator->get('user');

        return new ImportExcel($importService, $logger, $om, $user);
    }

} 