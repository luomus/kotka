<?php
namespace Kotka\Job;

use Kotka\Controller\SearchController;
use Kotka\DataGrid\ElasticGrid;
use Common\Service\IdService;
use Kotka\Log\Formatter\PriorityConditional;
use SlmQueue\Job\AbstractJob;
use Zend\Log\Logger;
use Zend\Log\Writer\Stream;
use Zend\Stdlib\SplPriorityQueue;
use Kotka\Stdlib\Hydrator\OntologyHydrator;
use Kotka\Stdlib\Hydrator\Strategy\DateTimeStrategy;
use Kotka\Triple\MYDocument;
use Triplestore\Stdlib\Hydrator\DateTime;

class ExportExcel extends AbstractJob
{
    const DONE_STRING = '<!--DONE-->';
    const WORKING_STRING = '<!--WORKING-->';
    const ERROR_STRING = '<!--ERROR-->';

    private $grid;
    private $sampleGrid;
    private $adapter;
    private $logger;
    private $excelService;
    private $specimenService;
    /** @var Logger */
    private $userLog;
    private $formGenerator;

    protected $content = array();

    public function __construct(ElasticGrid $grid = null, ElasticGrid $sampleGrid = null, $adapter = null, Logger $logger = null, \Kotka\Service\ExcelService $excelService = null, $formGenerator = null, $specimenService = null)
    {
        $this->grid = $grid;
        $this->sampleGrid = $sampleGrid;
        $this->logger = $logger;
        $this->adapter = $adapter;
        $this->excelService = $excelService;
        $this->formGenerator = $formGenerator;
        $this->specimenService = $specimenService;
    }

    public function execute()
    {
        $payload = $this->getContent();
        if (!isset($payload['export-params'])) {
            $this->logger->err('No query parameters found that could be used to export excel.', $payload);
            return;
        }
        if (!isset($payload['user']) || empty($payload['user'])) {
            $this->logger->err('No user information given! Place log out and log in to try again.', $payload);
            return;
        }
        $grid = isset($payload['export-params']['link']) ? $this->sampleGrid : $this->grid;
        $this->userLog = $this->getUserLog($payload['user']);
        $grid->prepareGrid($payload['export-params'], $this->adapter, []);
        try {
            $documents = $grid->getDocument(['documentQName']);
        } catch (\Exception $e) {
            $this->logger->err('Unable to fetch users params for export.', $payload);
            $this->userLog->crit('Excel generating failed because of invalid search params!' . self::DONE_STRING . self::ERROR_STRING);
            $this->clearWriteLock();
            return;
        }
        $count = $documents->count();
        $results = [];
        if ($count > SearchController::MAX_EXPORT) {
            $this->userLog->crit('Could not generate the excel because there was too many documents (max is ' . SearchController::MAX_EXPORT . ')!' . self::DONE_STRING . self::ERROR_STRING);
            $this->clearWriteLock();
            return;
        } else if ($count === 0) {
            $this->userLog->crit('Could not generate the excel because there was no documents found with the given search options!' . self::DONE_STRING . self::ERROR_STRING);
            $this->clearWriteLock();
            return;
        }

        foreach ($documents as $result) {
            $uri = IdService::getQName($result['documentQName'], true);
            $results[$uri] = $uri;
        }

        $specimenList = $this->getSpecimensAsArray($results);
        $fullFilename = self::getUserExportExcel($payload['user']);
        $this->excelService->setForm($this->getSpecimenForm());
        $method = 'generate' . ucfirst(isset($payload['type']) ? $payload['type'] : '') . 'ExcelFromArray';
        $success = $this->excelService->$method($specimenList, $fullFilename, true, true);
        $base = '/specimens/search';
        if (!$success) {
            $this->logger->err('Unable to fetch users params for export.', $payload);
            $this->userLog->crit('Excel generating failed because of invalid search params!' . self::DONE_STRING . self::ERROR_STRING);
            $this->clearWriteLock();
        }
        if (isset($payload['export-params']['export'])) {
            unset($payload['export-params']['export']);
        }
        if (isset($payload['export-params']['link'])) {
            $base = $payload['export-params']['link'];
            unset($payload['export-params']['link']);
        } else {
            $base .= '?' . http_build_query($payload['export-params']);
        }
        $this->userLog->debug('<p><strong>File generated.</strong> You can download the exported specimens <a href="/tools/export-download">here</a>.</p>', $payload);
        $this->userLog->debug('<p>Export is based on <a href="' . $base . '">this search</a>.</p>' . ExportExcel::DONE_STRING, $payload);
        $this->clearWriteLock();
    }

    public static function getUserExportLog($usersQName) {
        return './data/user-logs/export/' . str_replace('.', '', $usersQName) . '.log';
    }

    public static function getUserExportExcel($usersQName, $onlyFileName = false) {
        if ($onlyFileName) {
            return 'search-export-' . str_replace('.', '', $usersQName) . '.xlsx';
        }
        return './data/user-logs/export/' . self::getUserExportExcel($usersQName, true);
    }

    private function getSpecimensAsArray($documents)
    {
        /** @var \Kotka\Service\SpecimenService $specimenService */
        $specimens = $this->specimenService->getById($documents);
        if (!is_array($specimens)) {
            $specimens = [];
        }
        $hydrator = new OntologyHydrator();
        $dateStrategy = new DateTimeStrategy();
        $dateStrategy->setFormat(DateTime::FORMAT);
        $hydrator->addStrategy('MZDateEdited', $dateStrategy);
        $specimenList = array();
        foreach ($specimens as $specimen) {
            if ($specimen instanceof MYDocument) {
                if ($specimen->getMZScheduledForDeletion() === true) {
                    continue;
                }
                $specimenList[] = $hydrator->extract($specimen);
            }
        }
        return $specimenList;
    }

    private function getSpecimenForm()
    {
        $form = $this->formGenerator->getForm(new MYDocument());

        return $form;
    }

    private function getUserLog($qname) {
        $filename = self::getUserExportLog($qname);
        if (file_exists($filename)) {
            unlink($filename);
        }
        $writer = new Stream($filename);
        $format = new PriorityConditional([
            Logger::CRIT => '<div class="alert alert-danger">%message%</div>',
            Logger::ERR  => '<div class="user-log error">%message% <spcn class="badge badge-danger">failed</spcn> <small>(%timestamp%)</small></div>',
            Logger::INFO => '<div class="user-log success"><a href="/view?uri=%message%">%message%</a> <spcn class="badge badge-success">ok</spcn> <small>(%timestamp%)</small></div>',
            PriorityConditional::DEFAULT_KEY => '%message%',
        ]);
        $writer->setFormatter($format);
        $logger = new Logger();
        $queue = new SplPriorityQueue();
        $queue->insert($writer, 1);
        $logger->setWriters($queue);

        return $logger;
    }

    private function clearWriteLock() {
        if (!$this->userLog instanceof Logger) {
            return;
        }
        unset($this->userLog);
    }
}