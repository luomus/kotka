<?php
namespace Kotka\Job;

use Kotka\DataGrid\ElasticGrid;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ExportExcelFactory implements FactoryInterface
{

    public function createService(ServiceLocatorInterface $sl)
    {
        $parentServiceLocator = $sl->getServiceLocator();
        /** @var \Kotka\DataGrid\ElasticGrid $grid */
        $grid = new ElasticGrid(new \Kotka\ElasticExtract\Specimen());
        $grid->setServiceLocator($parentServiceLocator);

        $sampleGrid = new ElasticGrid(new \Kotka\ElasticExtract\Sample());
        $grid->setServiceLocator($parentServiceLocator);

        return new ExportExcel(
            $grid,
            $sampleGrid,
            $parentServiceLocator->get('Elastic\Client\Search'),
            $parentServiceLocator->get('Logger'),
            $parentServiceLocator->get('Kotka\Service\ExcelService'),
            $parentServiceLocator->get('Kotka\Service\FormGeneratorService'),
            $parentServiceLocator->get('Kotka\Service\SpecimenService')
        );
    }

} 