<?php
namespace Kotka\Job;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ImagesFactory implements FactoryInterface
{

    public function createService(ServiceLocatorInterface $sl)
    {
        $imageService = $sl->getServiceLocator()->get('ImageService');
        $specimenService = $sl->getServiceLocator()->get('Kotka\Service\SpecimenService');
        $logger = $sl->getServiceLocator()->get('logger');
        $mailer = $sl->getServiceLocator()->get('Kotka\Service\EmailService');

        return new Images($imageService, $specimenService, $logger, $mailer);
    }

} 