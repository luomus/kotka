<?php
namespace Kotka\Job;

use Kotka\Service\MustikkaService;
use SlmQueue\Job\AbstractJob;
use SlmQueue\Queue\QueueAwareInterface;
use SlmQueue\Queue\QueueAwareTrait;
use Triplestore\Service\ObjectManager;
use Zend\Log\Logger;

const MAX_RETRY = 20;

class Mustikka extends AbstractJob implements QueueAwareInterface
{

    use QueueAwareTrait;

    /** @var ObjectManager */
    private $objectManager;
    /** @var MustikkaService */
    private $mustikkaService;
    /** @var Logger */
    private $logger;

    protected $content = array();

    public function __construct(ObjectManager $objectManager = null, MustikkaService $mustikkaService = null, Logger $logger = null)
    {
        $this->objectManager = $objectManager;
        $this->mustikkaService = $mustikkaService;
        $this->logger = $logger;
    }

    public function setSubjects($QNames)
    {
        if (!is_array($QNames)) {
            $QNames = [$QNames];
        }
        $this->content['ids'] = $QNames;

        return $this;
    }

    public function execute()
    {
        $payload = $this->getContent();
        $ids = $payload['ids'];
        $retry = isset($payload['retry']) ? $payload['retry'] : 0;
        if (count($ids) == 0 || KOTKA_ENV !== 'production') {
            return;
        }
        $result = $this->mustikkaService->sendSpecimens($ids);
        if (!$result) {
            if ($retry <= MAX_RETRY) {
                $this->addMustikkaJob($ids, ++$retry);
            } else {
                $this->logger->crit('Giving up sending data to LAJI_ETL!', ['payload' => $payload]);
            }
        }
    }

    private function addMustikkaJob($ids, $retry = 1)
    {
        $job = new Mustikka();
        $job->setContent([
            'ids' => $ids ,
            'retry' => $retry
        ]);
        $this->getQueue()->push($job, [
            'delay' => 600 * $retry,
            'ttl' => 300
        ]);
    }

} 