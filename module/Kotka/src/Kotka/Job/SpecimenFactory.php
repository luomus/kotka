<?php
namespace Kotka\Job;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class SpecimenFactory implements FactoryInterface
{

    public function createService(ServiceLocatorInterface $sl)
    {
        /** @var \Kotka\Service\MustikkaService $mustikkaService */
        $mustikkaService = $sl->getServiceLocator()->get('Kotka\Service\MustikkaService');
        $objectManager = $sl->getServiceLocator()->get('Triplestore\ObjectManager');
        $specimenService = $sl->getServiceLocator()->get('Kotka\Service\SpecimenService');
        $imageService = $sl->getServiceLocator()->get('ImageService');

        return new Specimen($objectManager, $mustikkaService, $specimenService, $imageService);
    }

} 