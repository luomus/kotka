<?php

namespace Kotka\Model;


class Util
{

    const DATETIME_FORMAT = 'd.m.Y';
    const NAME_REGEX_PATTERN = '{^[^0-9]*, [^0-9]*$}';

    private static $cache = [];

    public static function getFieldFromQueryStr($str)
    {
        if (isset(self::$cache[$str])) {
            return self::$cache[$str];
        }
        $pats = explode('[', $str);
        $candidate = array_pop($pats);
        $candidate = rtrim($candidate, ']');
        while (is_numeric($candidate)) {
            $candidate = array_pop($pats);
            $candidate = rtrim($candidate, ']');
        }
        self::$cache[$str] = $candidate;
        return $candidate;
    }

    public static function array_to_str($array, $addKey = 2, $level = 0, $separator = array(":\n  ", "\n    ", ":\n      ", " ", ". ", ". "))
    {
        if (!is_array($array)) {
            return $array;
        }
        $sep = '; ';
        if (isset($separator[$level])) {
            $sep = $separator[$level];
        }
        $nextLevel = $level + 1;
        foreach ($array as $key => $value) {
            if ($level <= $addKey) {
                $array[$key] = $key . $sep . self::array_to_str($value, $addKey, $nextLevel, $separator);
            } else {
                $array[$key] = $sep . self::array_to_str($value, $addKey, $nextLevel, $separator);
            }
        }
        return implode($sep, $array);
    }

    public static function array_keys_multi(array $array, $includeNumeric = false, array $skip = null)
    {
        $keys = array();
        foreach ($array as $key => $value) {
            if (!is_integer($key) || $includeNumeric) {
                if ($skip === null || !in_array($key, $skip)) {
                    $keys[] = $key;
                }
            }
            if (is_array($array[$key])) {
                $keys = array_merge($keys, self::array_keys_multi($array[$key]));
            }
        }

        return $keys;
    }

    public static function extract_messages($array)
    {
        $return = array();
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                self::line($return, $value, $key, 0);
            } else {
                $return[$key] = $value;
            }

        }

        return $return;
    }

    public static function endsWith($haystack, $needle)
    {
        $length = strlen($needle);
        if ($length == 0) {
            return true;
        }

        return (substr($haystack, -$length) === $needle);
    }

    private static function line(&$result, $arr, $path, $level)
    {
        if (is_array($arr)) {
            foreach ($arr as $key => $value) {
                if (is_array($value)) {
                    self::line($result, $value, $path . '[' . $key . ']', $level + 1);
                } else {
                    $result[$path] = $value;
                }
            }
        }
    }

} 