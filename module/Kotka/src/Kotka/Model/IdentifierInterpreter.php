<?php

namespace Kotka\Model;


use Common\Service\IdService;

class IdentifierInterpreter
{
    private $hasData = false;
    private $range = false;
    private $onlyNS = false;
    private $onlyOId = false;

    private $ns;
    private $domain;
    private $documentId;
    private $rangeStart;
    private $rangeEnd;
    private $objectLetter = '';

    public function __construct($id, $defaultDomain = null, $tryRange = true)
    {
        $object = trim($id);
        if (strpos($object, 'http://') === 0) {
            $object = IdService::getQName($object);
        } elseif (empty($object)) {
            return;
        }
        $this->hasData = true;

        $parts = explode(':', $object);
        if (IdService::hasQNamePrefix($parts[0])) {
            $this->domain = $parts[0];
        } elseif ($defaultDomain !== null) {
            $this->domain = rtrim($defaultDomain,':');
        }
        $object = IdService::getQNameWithoutPrefix($object);
        if ($tryRange && strpos($object, '-') !== false) {
            $this->range = true;
            $from = explode('-', $object);
            while(count($from) > 2) {
                if (strpos($from[0], '.') !== false) {
                    break;
                }
                $partOfNs = array_shift($from);
                $from[0] = $partOfNs . '-' . $from[0];
            }
            if (strpos($from[0], '.') !== false) {
                list($ns, $oid) = explode('.', $from[0], 2);
                $this->ns = $ns;
            } else {
                $oid = $from[0];
            }
            preg_match('/^([a-z]+)/i', $oid, $matches);
            if (isset($matches[1])) {
                $this->objectLetter = $matches[1];
            }
            $this->rangeStart = $this->clearObjectID($oid);
            if (empty($from[1])) {
                if (empty($oid)) {
                    $this->range = false;
                    $this->documentId = $object;
                    return;
                }
                $this->rangeEnd = PHP_INT_MAX;
            } else {
                if (!preg_match('/[0-9]/', $oid)) {
                    $this->range = false;
                    $this->documentId = $object;
                    return;
                }
                $this->rangeEnd = $this->clearObjectID($from[1]);
            }
            if ($this->rangeStart > $this->rangeEnd) {
                list($this->rangeEnd, $this->rangeStart) = [$this->rangeStart, $this->rangeEnd];
            }

        } else {
            if (strpos($object, '.') === false) {
                if (is_numeric($object)) {
                    $this->onlyOId = true;
                    $this->documentId = $object;
                } else {
                    $this->onlyNS = true;
                    $this->ns = $object;
                }
            } else {
                $this->documentId = $object;
            }
        }
    }

    public function getRangeSize()
    {
        return ($this->rangeEnd - $this->rangeStart);
    }

    public function getRange($useUri = false)
    {
        $start = $this->getRangeStart();
        $end = $this->getRangeEnd();
        $uri = '';
        if ($this->hasDomain()) {
            $uri .= $this->domain . ':';
        }
        $uri .= (empty($this->ns)? '' : $this->ns. '.') . $this->objectLetter;
        if ($useUri) {
            $uri = IdService::getUri($uri);
        }
        foreach (range($start, $end) as $obj) {
            yield $uri . $obj;
        }
    }

    /**
     * @return boolean
     */
    public function isHasData()
    {
        return $this->hasData;
    }

    /**
     * @return boolean
     */
    public function isRange()
    {
        return $this->range;
    }

    /**
     * @return boolean
     */
    public function isOnlyNS()
    {
        return $this->onlyNS;
    }

    /**
     * @return boolean
     */
    public function isOnlyOId()
    {
        return $this->onlyOId;
    }

    /**
     * @return mixed|string
     */
    public function getNs()
    {
        return $this->ns;
    }

    public function hasDomain()
    {
        return ($this->domain !== null);
    }

    /**
     * @return mixed
     */
    public function getDomain()
    {
        return $this->domain;
    }

    public function setDomain($domain)
    {
        $this->domain = $domain;
    }

    /**
     * @return mixed|string
     */
    public function getDocumentId()
    {
        return $this->documentId;
    }

    /**
     * @return int
     */
    public function getRangeStart()
    {
        return $this->rangeStart;
    }

    /**
     * @return int
     */
    public function getRangeEnd()
    {
        return $this->rangeEnd;
    }

    /**
     * @return string
     */
    public function getObjectLetter()
    {
        return $this->objectLetter;
    }

    protected function clearObjectID($oid)
    {
        $oid = str_replace('-', '', $oid);
        return (int)filter_var($oid, FILTER_SANITIZE_NUMBER_INT);
    }

}