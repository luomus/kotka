<?php

namespace Kotka\Model\Label;

use Kotka\Model\Label;
use Kotka\Triple\MYDocument;
use Kotka\Triple\MYGathering;

class Helper
{

    private $document;

    public function getAssociatedTaxa(MYGathering $gathering)
    {
        $associated = [];
        $units = $gathering->getMYUnit();
        if (empty($units)) {
            return $associated;
        }
        foreach ($units as $unit) {
            $identifications = $unit->getMYIdentification();
            if (empty($identifications)) {
                continue;
            }
            foreach ($identifications as $i) {
                $taxon = $i->getMYTaxon();
                $associated[$taxon] = true;
                break;
            }
        }
        return array_keys($associated);
    }

    public function getLeg($legs, $verbatim, $short = false)
    {
        if ($short) {
            if (!is_array($legs) || empty($legs)) {
                return '';
            }
            $leg = '';
            if (count($legs) == 1) {
                $leg .= $legs[0];
            } elseif (count($legs) == 2) {
                $leg .= $this->shortenName($legs[0]) . ' & ' . $this->shortenName($legs[1]);
            } elseif (count($legs) == 3) {
                $leg .= $this->shortenName($legs[0]) . ', ' . $this->shortenName($legs[1]) . ' & ' . $this->shortenName($legs[2]);
            } else {
                $leg .= $legs[0] . '&nbsp;&nbsp;<em>et.al.</em>';
            }
            return "<span class='leg'>" . $leg . "&nbsp;&nbsp;leg.</span>";
        }
        if (!empty($verbatim)) {
            return $verbatim;
        }

        $return = '';
        if (is_array($legs)) {
            $return = implode('; ', $legs);
        }
        return $return;
    }

    private function shortenName($name)
    {
        $wrap = '<span class="name">%s</span>';
        $parts = explode(',', $name, 2);
        if (count($parts) != 2) {
            return sprintf($wrap, $name);
        }
        $i = substr(trim($parts[1]), 0, 1);
        $name = $i . '. ' . $parts[0];
        return sprintf($wrap, $name);
    }

    public function roundTo($number, $dec)
    {
        return number_format((float) $number, $dec, '.', '');
    }

    public function swapLeg($legs) {
        return array_map(function($leg) {
            $parts = explode(',', $leg, 2);
            if (count($parts) != 2) {
                return $leg;
            }
            return $parts[1] . ' ' . $parts[0];
        }, $legs);
    }

    public function getSex($sex)
    {
        $sex = str_replace('MY.sex', '', $sex);
        if ("M" == $sex) {
            $sex = "♂";
        } elseif ("F" == $sex) {
            $sex = "♀";
        }
        return $sex;
    }

    public function getAlt($alt)
    {
        if (empty($alt)) {
            return '';
        }
        return "Alt " . $alt . " m asl. / ";
    }

    public function getProvince($biologicalProvince, $administrativeProvince)
    {
        $province = $biologicalProvince;
        $province = empty($province) ?
            $administrativeProvince :
            $province;

        return $province;
    }

    public function getDet($det)
    {
        if (!empty($det)) {
            $namePieces = explode(", ", $det);
            $det = '';
            if (isset($namePieces[1])) {
                $det .= mb_substr($namePieces[1], 0, 1);
            }
            $det .= ". " . $namePieces[0] . " det.";
        } else {
            $det = "unknown det.";
        }
        return $det;
    }

    /**
     * @deprecated Use \Kotka\View\Helper\Label\Coordinate instead
     */
    public function getCoordinateText($system, $lat, $lon, $latWgs, $lonWgs, $radius, $short = false)
    {
        $system = lcfirst(str_replace('MY.coordinateSystem', '', $system));
        $this->convertCoordinate($system, $lat, $lon);
        if ($short) {
            if (empty($lat)) {
                return '';
            } elseif ("ykj" == $system) {
                return "ykj: " . $lat . ":" . $lon;
            } elseif ("wgs84" == $system || "wgs84dms" == $system) {
                return $lat . ", " . $lon;
            } else {
                return $system . ": " . $lat . ", " . $lon;
            }
        }
        if (empty($system)) {
            return '';
        }
        if ("ykj" == $system && !empty($lat) && !empty($lon)) {
            $wgs84 = $this->ykj2wgs84($lat, $lon, $radius, "dms", $latWgs, $lonWgs);

            return "YKJ: " . $lat . ":" . $lon . " (Grid 27° E)<br />
                WGS84: " . $wgs84['N'] . ", " . $wgs84['E'] . " (accuracy ±" . $wgs84['accuracy'] . " meters)";
        }

        $accuracy = "";
        if (!empty($radius)) {
            $accuracy = ", accuracy " . $radius . " meters ";
        }

        return "Coordinates: " . $lat . ", " . $lon . $accuracy . " (" . $system . ")";
    }

    /**
     * @deprecated Use \Kotka\View\Helper\Label\Coordinate instead
     *
     * TODO: Varsinainen getCoordinateText korvattava tällä, kun uusi koordinaattiformaatti on hyväksytty (YKJ vs. Grid 27 E)
     */
    public function getCoordinateText_v2($system, $lat, $lon, $latWgs, $lonWgs, $radius, $short = false)
    {
        $system = lcfirst(str_replace('MY.coordinateSystem', '', $system));
        $this->convertCoordinate($system, $lat, $lon);
        if ($short) {
            if (empty($lat)) {
                return '';
            } elseif ("ykj" == $system) {
                return "ykj: " . $lat . ":" . $lon;
            } elseif ("wgs84" == $system || "wgs84dms" == $system) {
                return $lat . ", " . $lon;
            } else {
                return $system . ": " . $lat . ", " . $lon;
            }
        }
        if (empty($system)) {
            return '';
        }
        if ("ykj" == $system && !empty($lat) && !empty($lon)) {
            $wgs84 = $this->ykj2wgs84($lat, $lon, $radius, "dms", $latWgs, $lonWgs);

            if (!empty($radius)) {
                $accuracy = ", accuracy ± $radius m";
            } else {
                $accuracy = "";
            }
            return $lat . ":" . $lon . " (YKJ" . $accuracy . ")<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                " . $wgs84['N'] . ", " . $wgs84['E'] . " (wgs84, accuracy ±" . ($wgs84['accuracy'] + $radius) . " m)";
        }

        $accuracy = "";
        if (!empty($radius)) {
            $accuracy = ", accuracy " . $radius . " m ";
        }

        return "Coordinates: " . $lat . ", " . $lon . $accuracy . " (" . $system . ")";
    }

    public function getShortCoordinateText($system, $lat, $lon, $delimiter = ':', $before = false, $systemWrap = ' (%s) ')
    {
        $system = lcfirst(str_replace('MY.coordinateSystem', '', $system));
        $this->convertCoordinate($system, $lat, $lon);
        $result = '';
        if ($before && !empty($system)) {
            $result .= sprintf($systemWrap, $system);
        }
        if (!empty($lat) && !empty($lon)) {
            $result .= $lat . $delimiter . $lon;
        }
        if (!$before && !empty($system)) {
            $result .= sprintf($systemWrap, $system);
        }
        return $result;
    }

    /**
     * @deprecated use \Kotka\Service\CoordinateService instead
     * @param $system
     * @param $lat
     * @param $lon
     */
    private function convertCoordinate($system, &$lat, &$lon)
    {
        if ("wgs84" == $system || "wgs84dms" == $system) {
            if (strpos($lat, '-') === 0) {
                $lat = substr($lat, 1) . ' S';
            } else {
                $lat .= ' N';
            }
            if (strpos($lon, '-') === 0) {
                $lon = substr($lon, 1) . ' W';
            } else {
                $lon .= ' E';
            }
        }
    }

    /**
     * @param MYDocument $document
     * @deprecated use Unreliable fields filter instead
     */
    public function markUnreliableFields(MYDocument $document)
    {
        $unreliableFields = $document->getMYUnreliableFields();
        if (empty($unreliableFields)) {
            return;
        }
        $unreliableFields = explode("\n", $unreliableFields);
        $allFields = [];
        foreach ($unreliableFields as $unreliable) {
            $allFields[] = $unreliable . '=1';
        }
        $data = [];
        parse_str(implode('&', $allFields), $data);
        $this->markUnreliable($document, $data);
    }

    private function markUnreliable(& $object, $fields)
    {
        foreach ($fields as $key => $value) {
            if ($value == 1) {
                if (is_array($object)) {
                    $object[$key] = '?' . $object[$key];
                    continue;
                }
                $setMethod = 'set' . $key;
                $getMethod = 'get' . $key;
                if (method_exists($object, $setMethod)) {
                    $curvalue = $object->$getMethod();
                    if (is_array($curvalue)) {
                        array_walk($curvalue, function (&$value) {
                            $value = '?' . $value;
                        });
                    } else {
                        $curvalue = '?' . $curvalue;
                    }
                    $object->$setMethod($curvalue);
                    continue;
                }
            } else if (is_numeric($key)) {
                if (isset($object[$key])) {
                    $this->markUnreliable($object[$key], $value);
                }
            } else {
                $getMethod = 'get' . $key;
                if (method_exists($object, $getMethod)) {
                    $child = $object->$getMethod();
                    $this->markUnreliable($child, $value);
                }
            }
        }
    }

    // ---------------------------------------------------------------------------------------
    // Returns date elements in an array

    public function extractDatePieces($date, $inputFormat = "finnish")
    {
        $ret = array(
            'day' => null,
            'month' => null,
            'year' => null
        );
        if ("finnish" == $inputFormat) {
            $pieces = explode(".", $date);
            if (isset($pieces[0])) {
                $ret['day'] = ltrim($pieces[0], 0);
            }
            if (isset($pieces[1])) {
                $ret['month'] = ltrim($pieces[1], 0);
            }
            if (isset($pieces[2])) {
                $ret['year'] = $pieces[2];
            }

            return $ret;
        } elseif ("internationalWithoutDashes" == $inputFormat) {
            $ret['day'] = ltrim(substr($date, 6, 2), 0);
            $ret['month'] = ltrim(substr($date, 4, 2), 0);
            $ret['year'] = substr($date, 0, 4);

            return $ret;
        }
        return $ret;
    }

    // ---------------------------------------------------------------------------------------
    // Converts dates to labels format, with roman month numbers

    public function dates2roman($date_begin, $date_end, $monthType = 'roman')
    {
        $ret = "";

        // Extract date pieces
        $dateBeginArray = $this->extractDatePieces($date_begin);
        $dateEndArray = $this->extractDatePieces($date_end);

        // Convert month to Roman numeral
        $month[1] = $monthType === 'roman' ? 'I' : 'Jan';
        $month[2] = $monthType === 'roman' ? 'II' : 'Feb';
        $month[3] = $monthType === 'roman' ? 'III' : 'Feb';
        $month[4] = $monthType === 'roman' ? 'IV' : 'Apr';
        $month[5] = $monthType === 'roman' ? 'V' : 'May';
        $month[6] = $monthType === 'roman' ? 'VI' : 'Jun';
        $month[7] = $monthType === 'roman' ? 'VII' : 'Jul';
        $month[8] = $monthType === 'roman' ? 'VIII' : 'Aug';
        $month[9] = $monthType === 'roman' ? 'IX' : 'Sep';
        $month[10] = $monthType === 'roman' ? 'X' : 'Oct';
        $month[11] = $monthType === 'roman' ? 'XI' : 'Nov';
        $month[12] = $monthType === 'roman' ? 'XII' : 'Dec';

        if ($monthType === 'roman' || $monthType === 'abbr') {
            if (isset($month[$dateBeginArray['month']])) {
                $dateBeginArray['month'] = $month[$dateBeginArray['month']];
            }
            if (isset($month[$dateEndArray['month']])) {
                $dateEndArray['month'] = $month[$dateEndArray['month']];
            }
        }

        // Jos molemmat päivät tyhjiä
        if (empty($date_begin) && empty($date_end)) {
            $ret .= "";
        } // Jos loppupäivää ei ole, tai se on sama kuin alkupäivä
        elseif (empty($date_end) || $date_end == $date_begin) {
            $ret .= $dateBeginArray['day'] . "." . $dateBeginArray['month'] . "." . $dateBeginArray['year'];
        } // Jos alku- ja loppupäivät erilaiset
        else {
            // Jos vuodet samoja, tulostetaan se vain kerran
            if ($dateBeginArray['year'] == $dateEndArray['year']) {
                // Jos kuukaudet samoja, tulostetaan se vain kerran
                if ($dateBeginArray['month'] == $dateEndArray['month']) {
                    // Koko kuukausi, jossa 31 päivää
                    if ($dateBeginArray['day'] == 1 && $dateEndArray['day'] == 31) {
                        $ret .= $dateBeginArray['month'] . "." . $dateBeginArray['year'];
                    } // Koko kuukausi, jossa 30 päivää
                    elseif ($dateBeginArray['day'] == 1 && $dateEndArray['day'] == 30 && $this->is30($dateBeginArray['month'])) {
                        $ret .= $dateBeginArray['month'] . "." . $dateBeginArray['year'];
                    } // Koko helmikuu
                    elseif ($dateBeginArray['month'] == $month[2] && $dateBeginArray['day'] == 1 && ($dateEndArray['day'] == 28 || $dateEndArray['day'] == 29)) {
                        $ret .= $dateBeginArray['month'] . "." . $dateBeginArray['year'];
                    } // Tulostetaan pp.-pp.kk.vvvv
                    else {
                        $ret .= $dateBeginArray['day'] . ".-" . $dateEndArray['day'] . "." . $dateBeginArray['month'] . "." . $dateBeginArray['year'];
                    }
                } // Jos kuukaudet erilaiset
                else {
                    // Jos kyseessä koko vuoden aikaväli, tulostetaan vain vuosi
                    if ($dateBeginArray['day'] == 1 && $dateEndArray['day'] == 31 && $dateBeginArray['month'] == $month[1] && $dateEndArray['month'] == $month[12]) {
                        $ret = $dateBeginArray['year'];
                    } else {
                        $ret .= $dateBeginArray['day'] . "." . $dateBeginArray['month'] . ".-" . $dateEndArray['day'] . "." . $dateEndArray['month'] . "." . $dateBeginArray['year'];
                    }
                }
            } // Jos vuodetkin erilaiset, tulostetaan molemmat päivämäärät
            else {
                $ret .= $dateBeginArray['day'] . "." . $dateBeginArray['month'] . "." . $dateBeginArray['year'];
                $ret .= "-" . $dateEndArray['day'] . "." . $dateEndArray['month'] . "." . $dateEndArray['year'];
            }
        }

        if (trim($ret) == "--") {
            $ret = "";
        }

        return $ret;
    }

    public function is30($monthRoman)
    {
        $months["IV"] = TRUE;
        $months["VI"] = TRUE;
        $months["IX"] = TRUE;
        $months["XI"] = TRUE;

        if (isset($months[$monthRoman])) {
            return TRUE;
        } else {
            return FALSE;
        }

    }

    // ---------------------------------------------------------------------------------------
    // Muuttaa yhtenäiskoordinaatit wgs84-muotoon ja virhesäteeksi

    /**
     * use \Kotka\Service\CoordinateService::ykj2wgs84 instead
     * @param $n
     * @param $e
     * @param int $coordRadius
     * @param string $format
     * @param null $wgsLat
     * @param null $wgsLon
     * @return mixed
     */
    public function ykj2wgs84($n, $e, $coordRadius = 0, $format = "dd", $wgsLat = null, $wgsLon = null)
    {
        $accuracy = strlen($n);

        switch ($accuracy) {
            case 3:
                $accuracy = "7200";
                break;
            case 4:
                $accuracy = "720";
                break;
            case 5:
                $accuracy = "72";
                break;
            case 6:
                $accuracy = "8";
                break;
            case 7:
                $accuracy = "1";
                break;
        }
        /*
        3 = 7000 m
        4 = 700 m
        5 = 70 m
        6 = 7 m
        7 = 1 m
        */

        $n = $n . "5000000";
        $n = substr($n, 0, 7);

        $e = $e . "5000000";
        $e = substr($e, 0, 7);


        if ($wgsLat !== null && $wgsLon !== null) {
            $array = ['N' => $wgsLat, 'E' => $wgsLon];
        } else {
            // This service does not exist anymore -- hopefully this helper is not used anywhere
            $url = "http://koivu.luomus.fi/projects/coordinateservice/?north=$n&east=$e&orig_system=ykj&format=json";
            $json = file_get_contents($url);
            $array = json_decode($json, TRUE);
        }

        $n = round($array['N'], 6);
        $e = round($array['E'], 6);

        if ("dms" == $format) {
            $ret['N'] = $this->DDtoDMS($n);
            $ret['E'] = $this->DDtoDMS($e);
        } else { // default
            $ret['N'] = $n;
            $ret['E'] = $e;
        }

        $ret['accuracy'] = $accuracy + $coordRadius;

        return $ret;
    }

    // ---------------------------------------------------------------------------------------
    //
    // http://www.web-max.ca/PHP/misc_6.php

    /**
     * use \Kotka\Service\CoordinateService::DDtoDMS instead
     * @param $dd
     * @return string
     */
    public function DDtoDMS($dd)
    {

        // Converts decimal longitude / latitude to DMS
        // ( Degrees / minutes / seconds )

        // This is the piece of code which may appear to
        // be inefficient, but to avoid issues with floating
        // point math we extract the integer part and the float
        // part by using a string function.

        $vars = explode(".", $dd);
        $deg = $vars[0];
        $tempma = "0." . $vars[1];

        $tempma = $tempma * 3600;
        $min = floor($tempma / 60);
        $sec = floor($tempma - ($min * 60));

//		return array("deg"=>$deg,"min"=>$min,"sec"=>$sec);
        return "{$deg}°$min'$sec\"";
    }

    public function getInfra($rank, $epithet)
    {
        if (empty($epithet)) {
            return '';
        }
        $result = $epithet;
        if (!empty($rank)) {
            if ($rank === 'ssp.') {
                $result = '<em>' . $result . '</em>';
            }
            $result = $rank . ' ' . $result;
        }
        return $result;
    }


    /**
     * @return MYDocument|null
     */
    public function getDocument()
    {
        return $this->document;
    }

    /**
     * @param MYDocument $document
     */
    public function setDocument(MYDocument $document)
    {
        $this->document = $document;
    }

    /**
     * @param $names
     * @param int $length
     * @param string $delimiter
     * @return string
     */
    public function concatenateAndTruncateNames($names, $length = 70, $delimiter = ', ')
    {
        $nameString = [];
        foreach ($names as $nro => $name) {
            if (!empty($name)) {
                $nameString [] = $name;
            }
        }
        $nameString = implode($delimiter, $nameString);

        // Cut if too long
        if (strlen($nameString) > $length) {
            $ret = mb_substr($nameString, 0, $length, 'utf8');
            $ret .= "...";
        } else {
            $ret = $nameString;
        }

        return $ret;
    }

    public function getMeasurements($measurements) {
        $result = [];

        if ($measurements === null) {
          return $result;
        }

        foreach ($measurements as $measurement) {
            $parts = explode(':',$measurement, 2);
            if (!isset($parts[1]) || empty(trim($parts[1]))) {
                continue;
            }
            $result[] = ['measurement' => $parts[0], 'value' => $parts[1]];
        }
        return $result;
    }

    /**
     * @param array $names
     * @param int $length
     */
    public function logoBase64()
    {
        $path = 'public/img/luomus_en_black_400.png';
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data = file_get_contents($path);
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
        return $base64;
    }


}
