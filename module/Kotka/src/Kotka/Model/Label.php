<?php

namespace Kotka\Model;

use Common\Service\IdService;

class Label
{

    private $qname;
    private $id;
    private $domain;
    private $objectID;
    private $taxon;
    private $taxonRank;
    private $genusQualifier;
    private $speciesQualifier;
    private $collectionID;
    private $infraRank;
    private $infraEpithet;
    private $infraAuthor;
    private $publication;
    private $identificationNotes;
    private $associatedTaxa;
    private $author;
    private $measurement = [];
    private $additionalIDs = [];
    private $observationID;
    private $sex;
    private $age;
    private $lifeStage;
    private $country;
    private $municipality;
    private $locality;
    private $localityDescription;
    private $localityVerbatim;
    private $habitatClassification;
    private $habitatDescription;
    private $biologicalProvince;
    private $administrativeProvince;
    private $originalCatalogueNumber;
    private $system;
    private $latitude;
    private $longitude;
    private $wgs84Latitude;
    private $wgs84Longitude;
    private $coordinateRadius;
    private $alt;
    private $depth;
    private $count;
    private $populationAbundance;
    private $substrate;
    private $decayStage;
    private $dateBegin;
    private $dateEnd;
    private $dateVerbatim;
    private $leg;
    private $legID;
    private $legVerbatim;
    private $det;
    private $detDate;
    private $detYear;
    private $host;
    private $ring;
    private $causeOfDeath;
    private $ipen;
    private $taxonVerbatim;
    private $relationship;
    private $DBH;
    private $exsiccatum;
    private $sampleAdditionalIDs = [];
    private $bold = [];
    private $transcriberNotes;
    private $sec;
    private $higherGeography;


    public function asKeyValueArray() {
        $vars = get_object_vars($this);
        $result = [];
        foreach ($vars as $key => $value) {
            if ($value !== null && $value !== '') {
                $result[$key] = $value;
            }
        }
        return $result;
    }

    /**
     * @return array
     */
    public function getSampleAdditionalIDs()
    {
        return $this->sampleAdditionalIDs;
    }

    /**
     * @param array $sampleAdditionalIDs
     */
    public function setSampleAdditionalIDs($sampleAdditionalIDs)
    {
        $this->sampleAdditionalIDs = $sampleAdditionalIDs;
    }

    private $rows = [];

    /**
     * @return string
     */
    public function getSex()
    {
        return $this->sex;
    }

    /**
     * @param string $sex
     */
    public function setSex($sex)
    {
        $this->sex = $sex;
    }

    /**
     * @return mixed
     */
    public function getLifeStage()
    {
        return $this->lifeStage;
    }

    /**
     * @param mixed $lifeStage
     */
    public function setLifeStage($lifeStage)
    {
        $this->lifeStage = $lifeStage;
    }

    /**
     * @return string
     */
    public function getQname()
    {
        return $this->qname;
    }

    /**
     * @param string $qname
     */
    public function setQname($qname)
    {
        $this->qname = $qname;
    }

    public function getUri()
    {
        return IdService::getUri($this->qname);
    }

    /**
     * @return string
     */
    public function getCollectionID()
    {
        return $this->collectionID;
    }

    /**
     * @return array
     */
    public function getMeasurement()
    {
        return $this->measurement;
    }

    /**
     * @param array $measurement
     */
    public function setMeasurement($measurement)
    {
        $this->measurement = $measurement;
    }

    /**
     * @param string $collectionID
     */
    public function setCollectionID($collectionID)
    {
        $this->collectionID = $collectionID;
    }

    /**
     * @return string
     */
    public function getTaxon()
    {
        return $this->taxon;
    }

    /**
     * @param string $taxon
     */
    public function setTaxon($taxon)
    {
        if (isset($this->associatedTaxa) && ($index = array_search($taxon, $this->associatedTaxa)) !== false) {
            unset($this->associatedTaxa[$index]);
        }
        $this->taxon = $taxon;
    }

    /**
     * @return string
     */
    public function getInfraRank()
    {
        return $this->infraRank;
    }

    /**
     * @param string $infraRank
     */
    public function setInfraRank($infraRank)
    {
        $this->infraRank = $infraRank;
    }

    /**
     * @return string
     */
    public function getInfraEpithet()
    {
        return $this->infraEpithet;
    }

    /**
     * @param string $infraEpithet
     */
    public function setInfraEpithet($infraEpithet)
    {
        $this->infraEpithet = $infraEpithet;
    }

    /**
     * @return string
     */
    public function getInfraAuthor()
    {
        return $this->infraAuthor;
    }

    /**
     * @param string $infraAuthor
     */
    public function setInfraAuthor($infraAuthor)
    {
        $this->infraAuthor = $infraAuthor;
    }

    /**
     * @return string
     */
    public function getAssociatedTaxa()
    {
        return $this->associatedTaxa;
    }

    /**
     * @param string $associatedTaxa
     */
    public function setAssociatedTaxa($associatedTaxa)
    {
        $this->associatedTaxa = $associatedTaxa;
    }

    /**
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param string $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * @return string
     */
    public function getMunicipality()
    {
        return $this->municipality;
    }

    /**
     * @param string $municipality
     */
    public function setMunicipality($municipality)
    {
        $this->municipality = $municipality;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getSystem()
    {
        return $this->system;
    }

    /**
     * @param string $system
     */
    public function setSystem($system)
    {
        $this->system = $system;
    }

    /**
     * @return string
     */
    public function getLocality()
    {
        return $this->locality;
    }

    /**
     * @param string $locality
     */
    public function setLocality($locality)
    {
        $this->locality = $locality;
    }

    /**
     * @return string
     */
    public function getLocalityDescription()
    {
        return $this->localityDescription;
    }

    /**
     * @param string $localityDescription
     */
    public function setLocalityDescription($localityDescription)
    {
        $this->localityDescription = $localityDescription;
    }

    /**
     * @return string
     */
    public function getLocalityVerbatim()
    {
        return $this->localityVerbatim;
    }

    /**
     * @param string $localityVerbatim
     */
    public function setLocalityVerbatim($localityVerbatim)
    {
        $this->localityVerbatim = $localityVerbatim;
    }


    /**
     * @return string
     */
    public function getHabitatClassification()
    {
        return $this->habitatClassification;
    }

    /**
     * @param string $habitatClassification
     */
    public function setHabitatClassification($habitatClassification)
    {
        $this->habitatClassification = $habitatClassification;
    }

    /**
     * @return string
     */
    public function getHabitatDescription()
    {
        return $this->habitatDescription;
    }

    /**
     * @param string $habitatDescription
     */
    public function setHabitatDescription($habitatDescription)
    {
        $this->habitatDescription = $habitatDescription;
    }

    /**
     * @return string
     */
    public function getBiologicalProvince()
    {
        return $this->biologicalProvince;
    }

    /**
     * @param string $biologicalProvince
     */
    public function setBiologicalProvince($biologicalProvince)
    {
        $this->biologicalProvince = $biologicalProvince;
    }


    /**
     * @return string
     */
    public function getAdministrativeProvince()
    {
        return $this->administrativeProvince;
    }

    /**
     * @param string $administrativeProvince
     */
    public function setAdministrativeProvince($administrativeProvince)
    {
        $this->administrativeProvince = $administrativeProvince;
    }

    /**
     * @return string
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @param string $longitude
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    }

    /**
     * @return string
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @param string $latitude
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    }

    /**
     * @return string
     */
    public function getWgs84Latitude()
    {
        return $this->wgs84Latitude;
    }

    /**
     * @param string $wgs84Latitude
     */
    public function setWgs84Latitude($wgs84Latitude)
    {
        $this->wgs84Latitude = $wgs84Latitude;
    }

    /**
     * @return string
     */
    public function getWgs84Longitude()
    {
        return $this->wgs84Longitude;
    }

    /**
     * @param string $wgs84Longitude
     */
    public function setWgs84Longitude($wgs84Longitude)
    {
        $this->wgs84Longitude = $wgs84Longitude;
    }

    /**
     * @return string
     */
    public function getCoordinateRadius()
    {
        return $this->coordinateRadius;
    }

    /**
     * @param string $coordinateRadius
     */
    public function setCoordinateRadius($coordinateRadius)
    {
        $this->coordinateRadius = $coordinateRadius;
    }

    /**
     * @return string
     */
    public function getAlt()
    {
        return $this->alt;
    }

    /**
     * @param string $alt
     */
    public function setAlt($alt)
    {
        $this->alt = $alt;
    }

    /**
     * @return string
     */
    public function getDepth()
    {
        return $this->depth;
    }

    /**
     * @param string $depth
     */
    public function setDepth($depth)
    {
        $this->depth = $depth;
    }

    /**
     * @return string
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @param string $count
     */
    public function setCount($count)
    {
        $this->count = $count;
    }


    /**
     * @return string
     */
    public function getPopulationAbundance()
    {
        return $this->populationAbundance;
    }

    /**
     * @param string $populationAbundance
     */
    public function setPopulationAbundance($populationAbundance)
    {
        $this->populationAbundance = $populationAbundance;
    }

    /**
     * @return string
     */
    public function getSubstrate()
    {
        return $this->substrate;
    }

    /**
     * @param string $substrate
     */
    public function setSubstrate($substrate)
    {
        $this->substrate = $substrate;
    }

    /**
     * @return string
     */
    public function getDecayStage()
    {
        return $this->decayStage;
    }

    /**
     * @param string $decayStage
     */
    public function setDecayStage($decayStage)
    {
        $this->decayStage = $decayStage;
    }

    /**
     * @return string
     */
    public function getTaxonRank()
    {
        return $this->taxonRank;
    }

    /**
     * @param string $taxonRank
     */
    public function setTaxonRank($taxonRank)
    {
        $this->taxonRank = $taxonRank;
    }

    /**
     * @return string
     */
    public function getGenusQualifier()
    {
        return $this->genusQualifier;
    }

    /**
     * @param string $genusQualifier
     */
    public function setGenusQualifier($genusQualifier)
    {
        $this->genusQualifier = $genusQualifier;
    }

    /**
     * @return string
     */
    public function getSpeciesQualifier()
    {
        return $this->speciesQualifier;
    }

    /**
     * @param string $speciesQualifier
     */
    public function setSpeciesQualifier($speciesQualifier)
    {
        $this->speciesQualifier = $speciesQualifier;
    }


    /**
     * @return string
     */
    public function getDateBegin()
    {
        return $this->dateBegin;
    }

    /**
     * @param string $dateBegin
     */
    public function setDateBegin($dateBegin)
    {
        $this->dateBegin = $dateBegin;
    }

    /**
     * @return string
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * @param string $dateEnd
     */
    public function setDateEnd($dateEnd)
    {
        $this->dateEnd = $dateEnd;
    }

    /**
     * @return string
     */
    public function getDateVerbatim()
    {
        return $this->dateVerbatim;
    }

    /**
     * @param string $dateVerbatim
     */
    public function setDateVerbatim($dateVerbatim)
    {
        $this->dateVerbatim = $dateVerbatim;
    }


    /**
     * @return string
     */
    public function getLeg()
    {
        return $this->leg;
    }

    /**
     * @param string $leg
     */
    public function setLeg($leg)
    {
        $this->leg = $leg;
    }

    /**
     * @return string
     */
    public function getLegID()
    {
        return $this->legID;
    }

    /**
     * @param string $legID
     */
    public function setLegID($legID)
    {
        $this->legID = $legID;
    }

    /**
     * @return string
     */
    public function getLegVerbatim()
    {
        return $this->legVerbatim;
    }

    /**
     * @param string $legVerbatim
     */
    public function setLegVerbatim($legVerbatim)
    {
        $this->legVerbatim = $legVerbatim;
    }

    /**
     * @return string
     */
    public function getDet()
    {
        return $this->det;
    }

    /**
     * @param string $det
     */
    public function setDet($det)
    {
        $this->det = $det;
    }

    /**
     * @return string
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @param string $host
     */
    public function setHost($host)
    {
        $this->host = $host;
    }

    /**
     * @return array
     */
    public function getRows()
    {
        return $this->rows;
    }

    /**
     * @param array $rows
     */
    public function setRows(array $rows)
    {
        $this->rows = $rows;
    }

    /**
     * @return array
     */
    public function getPublication()
    {
        return $this->publication;
    }

    /**
     * @param array $publication
     */
    public function setPublication($publication)
    {
        if (empty($publication)) {
            return;
        }
        if (!is_array($publication)) {
            $publication = [$publication];
        }
        $this->publication = $publication;
    }

    /**
     * @return string
     */
    public function getDetDate()
    {
        return $this->detDate;
    }

    /**
     * @param string $detDate
     */
    public function setDetDate($detDate)
    {
        $this->detDate = $detDate;
    }

    /**
     * @return string
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * @param string $age
     */
    public function setAge($age)
    {
        $this->age = $age;
    }

    /**
     * @return string
     */
    public function getRing()
    {
        return $this->ring;
    }

    /**
     * @param string $ring
     */
    public function setRing($ring)
    {
        $this->ring = $ring;
    }

    /**
     * @return string
     */
    public function getOriginalCatalogueNumber()
    {
        return $this->originalCatalogueNumber;
    }

    /**
     * @param string $originalCatalogueNumber
     */
    public function setOriginalCatalogueNumber($originalCatalogueNumber)
    {
        $this->originalCatalogueNumber = $originalCatalogueNumber;
    }

    /**
     * @return string
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * @param string $domain
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getObjectID()
    {
        return $this->objectID;
    }

    /**
     * @param string $objectID
     */
    public function setObjectID($objectID)
    {
        $this->objectID = $objectID;
    }

    /**
     * @return string
     */
    public function getCauseOfDeath()
    {
        return $this->causeOfDeath;
    }

    /**
     * @param string $causeOfDeath
     */
    public function setCauseOfDeath($causeOfDeath)
    {
        $this->causeOfDeath = $causeOfDeath;
    }

    /**
     * @return array
     */
    public function getAdditionalIDs()
    {
        return $this->additionalIDs;
    }

    /**
     * @param array $additionalIDs
     */
    public function setAdditionalIDs($additionalIDs)
    {
        $this->additionalIDs = $additionalIDs;
    }

    /**
     * @return string
     */
    public function getObservationID()
    {
        return $this->observationID;
    }

    /**
     * @param string $observationID
     */
    public function setObservationID($observationID)
    {
        $this->observationID = $observationID;
    }

    /**
     * @return mixed
     */
    public function getIpen()
    {
        return $this->ipen;
    }

    /**
     * @param mixed $ipen
     */
    public function setIpen($ipen)
    {
        $this->ipen = $ipen;
    }

    /**
     * @return mixed
     */
    public function getTaxonVerbatim()
    {
        return $this->taxonVerbatim;
    }

    /**
     * @param mixed $taxonVerbatim
     */
    public function setTaxonVerbatim($taxonVerbatim)
    {
        $this->taxonVerbatim = $taxonVerbatim;
    }

    /**
     * @return mixed
     */
    public function getRelationship()
    {
        return $this->relationship;
    }

    /**
     * @param mixed $relationship
     */
    public function setRelationship($relationship)
    {
        $this->relationship = $relationship;
    }

    /**
     * @return mixed
     */
    public function getDBH()
    {
        return $this->DBH;
    }

    /**
     * @param mixed $DBH
     */
    public function setDBH($DBH)
    {
        $this->DBH = $DBH;
    }

    /**
     * @return mixed
     */
    public function getExsiccatum()
    {
        return $this->exsiccatum;
    }

    /**
     * @param mixed $exsiccatum
     */
    public function setExsiccatum($exsiccatum)
    {
        $this->exsiccatum = $exsiccatum;
    }

    /**
     * @return mixed
     */
    public function getDetYear()
    {
        return $this->detYear;
    }

    /**
     * @param mixed $detYear
     */
    public function setDetYear($detYear): void
    {
        $this->detYear = $detYear;
    }

    /**
     * @return mixed
     */
    public function getIdentificationNotes()
    {
        return $this->identificationNotes;
    }

    /**
     * @param mixed $identificationNotes
     */
    public function setIdentificationNotes($identificationNotes): void
    {
        $this->identificationNotes = $identificationNotes;
    }

    /**
     * @return array
     */
    public function getBold()
    {
        return $this->bold;
    }

    /**
     * @param array $bold
     */
    public function setBold($bold): void
    {
        $this->bold = $bold;
    }

    /**
     * @return string
     */
    public function getTranscriberNotes()
    {
        return $this->transcriberNotes;
    }

    /**
     * @param string $transcriberNotes
     */
    public function setTranscriberNotes($transcriberNotes): void
    {
        $this->transcriberNotes = $transcriberNotes;
    }


    /**
     * @return string
     */
    public function getSec()
    {
        return $this->sec;
    }

    /**
     * @param string $sec
     */
    public function setSec($sec): void
    {
        $this->sec = $sec;
    }

    /**
     * @return string
     */
    public function getHigherGeography()
    {
        return $this->higherGeography;
    }

    /**
     * @param string $higherGeography
     */
    public function setHigherGeography($higherGeography): void
    {
        $this->higherGeography = $higherGeography;
    }
}