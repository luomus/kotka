<?php

namespace Kotka\Model;


class ExcelSheet implements \Countable, \Iterator
{

    private $cols = array();
    private $maxRows = 0;

    public function addColumn(ExcelColumn $col)
    {
        $rows = $col->count();
        if ($rows > $this->maxRows) {
            $this->maxRows = $rows;
        }
        $this->cols[] = $col;
    }

    public function getDataRowCount()
    {
        $max = 0;
        foreach ($this->cols as $col) {
            $cnt = count($col);
            if ($cnt > $max) {
                $max = $cnt;
            }
        }
        return $max;
    }

    public function toArray($withHeaders = false)
    {
        $result = array();
        $rows = $this->getDataRowCount();
        foreach ($this->cols as $col => $data) {
            /** @var \Kotka\Model\ExcelColumn $data */
            $label = $data->getLabel();
            $content = $data->getData();
            $cnt = 1;
            if ($withHeaders) {
                $result[$cnt][$col] = $data->getFullField();
                $cnt++;
            }
            $result[$cnt][$col] = $label;
            $cnt++;
            for ($i = 0; $i < $rows; $i++) {
                $value = null;
                if (isset($content[$i])) {
                    $value = $content[$i];
                }
                $result[$i + $cnt][$col] = $value;
            }
        }
        return $result;
    }

    public function count()
    {
        return count($this->cols);
    }


    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Return the current element
     *
     * @link http://php.net/manual/en/iterator.current.php
     * @return mixed Can return any type.
     */
    public function current()
    {
        return current($this->cols);
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Move forward to next element
     *
     * @link http://php.net/manual/en/iterator.next.php
     * @return void Any returned value is ignored.
     */
    public function next()
    {
        next($this->cols);
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Return the key of the current element
     *
     * @link http://php.net/manual/en/iterator.key.php
     * @return mixed scalar on success, or null on failure.
     */
    public function key()
    {
        return key($this->cols);
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Checks if current position is valid
     *
     * @link http://php.net/manual/en/iterator.valid.php
     * @return boolean The return value will be casted to boolean and then evaluated.
     *       Returns true on success or false on failure.
     */
    public function valid()
    {
        return key($this->cols) !== null;
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Rewind the Iterator to the first element
     *
     * @link http://php.net/manual/en/iterator.rewind.php
     * @return void Any returned value is ignored.
     */
    public function rewind()
    {
        reset($this->cols);
    }
}