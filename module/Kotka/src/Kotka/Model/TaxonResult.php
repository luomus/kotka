<?php

namespace Kotka\Model;


use Zend\Db\Adapter\Driver\ResultInterface;

class TaxonResult
{

    private $qname;
    private $rank;
    private $scientificName;
    private $finnishName;
    private $parents = [];
    private $children = [];

    private $taxonGroupAbbr = '';

    private $taxonGroupsAbbr = [
        'MX.53062' => 'FU',
        'MX.44109' => 'BR'
    ];

    /**
     * @param $result
     * @throws \Exception
     */
    public function __construct($result)
    {
        if ($result instanceof ResultInterface) {
            $checked = false;
            foreach ($result as $data) {
                if (!$checked && (
                        !array_key_exists('STEP', $data) ||
                        !array_key_exists('SCIENTIFICNAME', $data) ||
                        !array_key_exists('FINNISHNAME', $data) ||
                        !array_key_exists('RANK', $data) ||
                        !array_key_exists('STEP_QNAME', $data)
                    )
                ) {
                    throw new \Exception('Input data is missing key that is used with taxon result!');
                }
                $checked = true;
                $this->checkTaxonGroupAbbr($data['STEP_QNAME']);
                if ($data['STEP'] === '0') {
                    $this->qname = $data['STEP_QNAME'];
                    $this->scientificName = $data['SCIENTIFICNAME'];
                    $this->finnishName = $data['FINNISHNAME'];
                    $this->rank = $data['RANK'];
                } else {
                    $index = ((int)$data['STEP']) - 1;
                    $tmp = [
                        'scientificName' => $data['SCIENTIFICNAME'],
                        'rank' => $data['RANK'],
                        'qname' => $data['STEP_QNAME'],
                    ];
                    if ($index < 0) {
                        $this->parents[$index] = $tmp;
                    } else {
                        $this->children[] = $tmp;
                    }
                }
            }
        }
    }

    private function checkTaxonGroupAbbr($qname)
    {
        if (isset($this->taxonGroupsAbbr[$qname])) {
            $this->taxonGroupAbbr = $this->taxonGroupsAbbr[$qname];
        }
    }

    /**
     * @return string
     */
    public function getQName()
    {
        return $this->qname;
    }

    /**
     * @return string
     */
    public function getRank()
    {
        return $this->rank;
    }

    /**
     * @return string
     */
    public function getScientificName()
    {
        return $this->scientificName;
    }

    /**
     * @return array
     */
    public function getParents()
    {
        return $this->parents;
    }

    /**
     * @return array
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @return string
     */
    public function getTaxonGroupAbbr()
    {
        return $this->taxonGroupAbbr;
    }

    /**
     * @return string
     */
    public function getFinnishName()
    {
        return $this->finnishName;
    }

}