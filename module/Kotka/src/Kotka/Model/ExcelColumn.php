<?php

namespace Kotka\Model;


class ExcelColumn implements \Countable
{

    private $field;
    private $fullField;

    private $label;

    private $data = [];

    private $prefix;

    private $firstOfGroup = false;

    private $allowedValues;

    public function getField()
    {
        return $this->field;
    }

    public function setField($field)
    {
        $this->field = $field;
    }

    /**
     * @return null|string
     */
    public function getFullField()
    {
        return $this->fullField;
    }

    /**
     * @param string $fullField
     */
    public function setFullField($fullField)
    {
        $this->fullField = $fullField;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param mixed $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        if ($this->prefix !== null) {
            foreach ($this->data as $key => $value) {
                $this->data[$key] = str_replace($this->prefix, '', $value);
            }
        }
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data, $key = null)
    {
        if ($key === null) {
            $this->data = $data;
        } else {
            $this->data[$key] = $data;
        }
    }

    /**
     * @return mixed
     */
    public function getPrefix()
    {
        return $this->prefix;
    }

    /**
     * @param mixed $prefix
     */
    public function setPrefix($prefix)
    {
        $this->prefix = $prefix;
    }

    /**
     * @return mixed
     */
    public function getAllowedValues()
    {
        if ($this->prefix !== null) {
            foreach ($this->allowedValues as $key => $value) {
                $this->allowedValues[$key] = str_replace($this->prefix, '', $value);
            }
        }
        return $this->allowedValues;
    }

    /**
     * @param mixed $allowedValues
     */
    public function setAllowedValues($allowedValues)
    {
        $this->allowedValues = $allowedValues;
    }

    /**
     * @return mixed
     */
    public function getFirstOfGroup()
    {
        return $this->firstOfGroup;
    }

    /**
     * @param mixed $firstOfGroup
     */
    public function setFirstOfGroup($firstOfGroup)
    {
        $this->firstOfGroup = $firstOfGroup;
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * Count elements of an object
     *
     * @link http://php.net/manual/en/countable.count.php
     * @return int The custom count as an integer.
     *       </p>
     *       <p>
     *       The return value is cast to an integer.
     */
    public function count()
    {
        return count($this->data);
    }
}