<?php

namespace Kotka\Model;


class ExcelStyles
{

    const DEFAULT_KEY = '__default__';
    const DEFAULT_WIDTH = '20';

    const LIGHT_BLUE = 'B7DEE8';
    const LIGHT_GRAY = 'E6E0EC';
    const LIGHT_GREEN = 'D7E4BD';
    const LIGHT_RED = 'F2DCDB';
    const LIGHT_PURPLE = 'CCC1DA';
    const LIGHT_YELLOW = 'FFFF99';
    const RED = 'C00000';

    private $requiredHeaderTextColor = self::RED;

    private $colWidth = array(
        ''
    );

    private $styledHeaders = array(
        self::DEFAULT_KEY => array(
            'borders' => array(
                'bottom' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_DOUBLE,
                    'color' => array('rgb' => '000000')
                ),
            ),
            'fill' => array(
                'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => self::LIGHT_GREEN)
            ),
            'font' => array(
                'bold' => true,
            )
        ),
        'MYNamespaceID' => array(
            'borders' => array(
                'bottom' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_DOUBLE,
                    'color' => array('rgb' => '000000')
                ),
            ),
            'fill' => array(
                'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => self::LIGHT_RED)
            ),
            'font' => array(
                'bold' => true,
                'color' => array('rgb' => self::RED)
            )
        ),
        'MYObjectID' => array(
            'borders' => array(
                'bottom' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_DOUBLE,
                    'color' => array('rgb' => '000000')
                ),
            ),
            'fill' => array(
                'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => self::LIGHT_RED)
            ),
            'font' => array(
                'bold' => true,
                'color' => array('rgb' => self::RED)
            )
        ),
        'MYCollectionID' => array(
            'borders' => array(
                'bottom' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_DOUBLE,
                    'color' => array('rgb' => '000000')
                ),
            ),
            'fill' => array(
                'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => self::LIGHT_GREEN)
            ),
            'font' => array(
                'bold' => true,
                'color' => array('rgb' => self::RED)
            )
        ),
        'MYCountry' => array(
            'borders' => array(
                'bottom' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_DOUBLE,
                    'color' => array('rgb' => '000000')
                ),
            ),
            'fill' => array(
                'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => self::LIGHT_GREEN)
            ),
            'font' => array(
                'bold' => true,
                'color' => array('rgb' => self::RED)
            )
        ),
        'MYHigherGeography' => array(
            'borders' => array(
                'bottom' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_DOUBLE,
                    'color' => array('rgb' => '000000')
                ),
            ),
            'fill' => array(
                'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => self::LIGHT_GREEN)
            ),
            'font' => array(
                'bold' => true,
                'color' => array('rgb' => self::RED)
            )
        ),
        'MYRecordBasis' => array(
            'borders' => array(
                'bottom' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_DOUBLE,
                    'color' => array('rgb' => '000000')
                ),
            ),
            'fill' => array(
                'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => self::LIGHT_GREEN)
            ),
            'font' => array(
                'bold' => true,
                'color' => array('rgb' => self::RED)
            )
        ),
    );

    private $levelStyles = array();

    private $styledColumns = array();

    private $split = array(
        'borders' => array(
            'left' => array(
                'style' => \PHPExcel_Style_Border::BORDER_DOUBLE,
            ),
        ),
    );

    public function getHeaderStyle($field)
    {
        if (isset($this->styledHeaders[$field])) {
            return $this->styledHeaders[$field];
        }
        return $this->styledHeaders[self::DEFAULT_KEY];
    }

    public function getColumnStyle($field, $level = null, $split = false)
    {
        if (!$split && !isset($this->styledColumns[$field]) && !isset($this->levelStyles[$level])) {
            return false;
        }
        $return = array();
        if ($split) {
            $return = $this->split;
        }
        if ($level !== null && isset($this->levelStyles[$level])) {
            $return = array_replace_recursive($this->levelStyles[$level], $return);
        }
        if (isset($this->styledColumns[$field])) {
            $return = array_replace_recursive($this->styledColumns[$field], $return);
        }
        return $return;
    }

    public function getColWidth($field)
    {
        if (isset($this->colWidth[$field])) {
            return $this->colWidth[$field];
        }
        return self::DEFAULT_WIDTH;
    }

    public function getRequiredHeaderTextColor()
    {
        return $this->requiredHeaderTextColor;
    }

} 