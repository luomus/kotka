<?php

namespace Kotka\Model;


class Count
{
    private $value;

    private $female = 0;
    private $male = 0;
    private $other = 0;


    public function __construct($value = null)
    {
        if ($value !== null) {
            $this->refresh($value);
        }
    }

    public function refresh($value)
    {
        $this->female = 0;
        $this->male = 0;
        $this->other = 0;
        if (!is_string($value)) {
            return $this;
        }
        if (preg_match('#(\d+\s?f)#i', $value, $matches)) {
            $this->female = (int) filter_var($matches[1], FILTER_SANITIZE_NUMBER_INT);
            $value = str_replace($matches[1], '', $value);
        }
        if (preg_match('#(\d+\s?m)#i', $value, $matches)) {
            $this->male = (int) filter_var($matches[1], FILTER_SANITIZE_NUMBER_INT);
            $value = str_replace($matches[1], '', $value);
        }
        $parts = explode('-', $value, 2);
        $cnt = isset($parts[1]) ? $parts[1] : $parts[0];
        $this->other = (int) filter_var($cnt, FILTER_SANITIZE_NUMBER_INT);
        if (preg_match('#(useita|many|muutama)#i', $value)) {
            $this->other += 2;
        }
        if (preg_match('#(yksi|one)#i', $value)) {
            $this->other += 1;
        }
        return $this;
    }

    public function getTotal()
    {
        return $this->female + $this->male + $this->other;
    }

    /**
     * @return int
     */
    public function getFemale()
    {
        return $this->female;
    }

    /**
     * @param int $female
     */
    public function setFemale($female)
    {
        $this->female = $female;
    }

    /**
     * @return int
     */
    public function getMale()
    {
        return $this->male;
    }

    /**
     * @param int $male
     */
    public function setMale($male)
    {
        $this->male = $male;
    }

    /**
     * @return int
     */
    public function getOther()
    {
        return $this->other;
    }

    /**
     * @param int $other
     */
    public function setOther($other)
    {
        $this->other = $other;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }




}