<?php

namespace Kotka\Model;

/**
 * Class DataTypes holds information about all the datatypes in Kotka
 * @package Kotka\Model
 */
class DataTypes
{
    const COLLECTION = 'metadata';
    const DATASET = 'dataset';
    const ORGANIZATION = 'organization';
    const TRANSACTION = 'transaction';

    const ZOOLOGICAL = 'zoospecimen';
    const BOTANICAL = 'botanyspecimen';
    const ACCESSION = 'accession';
    const PALAEONTOLOGY = 'palaeontology';
    const CULTURE = 'culture';

    protected $documentTypes = array(
        self::COLLECTION => 'MYCollection',
        self::DATASET => 'Dataset',
        self::ORGANIZATION => 'MOSOrganization',
        self::TRANSACTION => 'Transaction'
    );

    protected $specimenDocumentTypes = array(
        self::BOTANICAL => 'Specimen',
        self::ZOOLOGICAL => 'Specimen',
        self::ACCESSION => 'Specimen',
        self::PALAEONTOLOGY => 'Specimen',
        self::CULTURE => 'Specimen',
    );

    /**
     * Returns the internal document type used based on the datatype
     *
     * @param $datatype
     * @return string
     */
    public function getDocumentTypeFromDatatype($datatype)
    {
        if (isset($this->specimenDocumentTypes[$datatype])) {
            return $this->specimenDocumentTypes[$datatype];
        }

        if (!isset($this->documentTypes[$datatype])) {
            return false;
        }

        return $this->documentTypes[$datatype];
    }

    /**
     * Returns all the known datatypes
     * @return array
     */
    public function getDatatypes()
    {
        return array_merge(array_keys($this->documentTypes), array_keys($this->specimenDocumentTypes));
    }

    /**
     * Return only the specimen datatypes
     * @return array
     */
    public function getSpecimenDatatypes()
    {
        return array_keys($this->specimenDocumentTypes);
    }

}
