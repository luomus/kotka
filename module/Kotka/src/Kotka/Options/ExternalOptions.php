<?php

namespace Kotka\Options;


use Zend\Stdlib\AbstractOptions;

class ExternalOptions extends AbstractOptions
{

    /** @var string */
    protected $url;
    /** @var string */
    protected $user;
    /** @var string */
    protected $pass;

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param string $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return string
     */
    public function getPass()
    {
        return $this->pass;
    }

    /**
     * @param string $pass
     */
    public function setPass($pass)
    {
        $this->pass = $pass;
    }

}