<?php

namespace Kotka\Options;


use Zend\Stdlib\AbstractOptions;

class ExternalsOptions extends AbstractOptions
{

    /** @var ExternalOptions */
    protected $mustikka;

    /** @var ExternalOptions */
    protected $lajiETL;

    /** @var ExternalOptions */
    protected $tipuApi;

    /** @var ExternalOptions */
    protected $coordinate;

    /** @var ExternalOptions */
    protected $triplestoreApi;

    /** @var ExternalOptions */
    protected $namespaceWS;

    /** @var ExternalOptions */
    protected $idGenerator;

    /** @var ExternalOptions */
    protected $digitariumApi;

    /** @var ExternalOptions */
    protected $apiLaji;

    /** @var ExternalOptions */
    protected $newKotka;

    public function __construct($options = null)
    {
        foreach ($options as $key => $option) {
            $options[$key] = new ExternalOptions($option);
        }
        parent::__construct($options);
    }

    /**
     * @return ExternalOptions
     */
    public function getNewKotka()
    {
        return $this->newKotka;
    }

    /**
     * @param ExternalOptions $newKotka
     */
    public function setNewKotka($newKotka)
    {
        $this->newKotka = $newKotka;
    }

    /**
     * @return ExternalOptions
     */
    public function getApiLaji()
    {
        return $this->apiLaji;
    }

    /**
     * @param ExternalOptions $apiLaji
     */
    public function setApiLaji($apiLaji)
    {
        $this->apiLaji = $apiLaji;
    }

    /**
     * @return ExternalOptions
     */
    public function getMustikka()
    {
        return $this->mustikka;
    }

    /**
     * @param ExternalOptions $mustikka
     */
    public function setMustikka($mustikka)
    {
        $this->mustikka = $mustikka;
    }

    /**
     * @return ExternalOptions
     */
    public function getLajiETL()
    {
        return $this->lajiETL;
    }

    /**
     * @param ExternalOptions $lajiETL
     */
    public function setLajiETL($lajiETL)
    {
        $this->lajiETL = $lajiETL;
    }

    /**
     * @return ExternalOptions
     */
    public function getTipuApi()
    {
        return $this->tipuApi;
    }

    /**
     * @param ExternalOptions $tipuApi
     */
    public function setTipuApi($tipuApi)
    {
        $this->tipuApi = $tipuApi;
    }

    /**
     * @return ExternalOptions
     */
    public function getCoordinate()
    {
        return $this->coordinate;
    }

    /**
     * @param ExternalOptions $coordinate
     */
    public function setCoordinate($coordinate)
    {
        $this->coordinate = $coordinate;
    }

    /**
     * @return ExternalOptions
     */
    public function getTriplestoreApi()
    {
        return $this->triplestoreApi;
    }

    /**
     * @param ExternalOptions $triplestoreApi
     */
    public function setTriplestoreApi($triplestoreApi)
    {
        $this->triplestoreApi = $triplestoreApi;
    }

    /**
     * @return ExternalOptions
     */
    public function getNamespaceWS()
    {
        return $this->namespaceWS;
    }

    /**
     * @param ExternalOptions $namespaceWS
     */
    public function setNamespaceWS($namespaceWS)
    {
        $this->namespaceWS = $namespaceWS;
    }

    /**
     * @return ExternalOptions
     */
    public function getIdGenerator()
    {
        return $this->idGenerator;
    }

    /**
     * @param ExternalOptions $idGenerator
     */
    public function setIdGenerator($idGenerator)
    {
        $this->idGenerator = $idGenerator;
    }

    /**
     * @return ExternalOptions
     */
    public function getDigitariumApi()
    {
        return $this->digitariumApi;
    }

    /**
     * @param ExternalOptions $digitariumApi
     */
    public function setDigitariumApi($digitariumApi)
    {
        $this->digitariumApi = $digitariumApi;
    }
}