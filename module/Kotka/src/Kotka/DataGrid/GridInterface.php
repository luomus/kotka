<?php
namespace Kotka\DataGrid;


interface GridInterface
{

    public function prepareGrid($data);

    public function getGrid();

    public function setGrid($grid);

} 