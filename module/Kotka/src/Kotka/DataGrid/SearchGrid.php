<?php

namespace Kotka\DataGrid;


use Kotka\DataGrid\Type\URI;
use Common\Service\IdService;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\Sql\Predicate\Expression;
use Zend\Db\Sql\Predicate\Predicate;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\TableIdentifier;
use Zend\Db\Sql\Where;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use ZfcDatagrid\Column;

class SearchGrid extends AbstractGrid implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    const DEFAULT_PAGE_SIZE = 25;
    const MIN_TAXON_LIKE_SEARCH = 4;
    const MIN_LEG_LIKE_SEARCH = 3;
    const DATASETS_PARAMETER_FK = 212;

    private $adapter;
    /** @var  Select */
    private $select;
    private $criteria;
    private $rowCount;
    private $sql;
    private $urlCol;
    private $fields;

    public function prepareGrid($criteria, AdapterInterface $adapter = null, $fields = null)
    {
        if ($adapter !== null) {
            $this->adapter = $adapter;
        }
        $this->criteria = $criteria;
        $this->select = $this->CriteriaToSelect($criteria);
        $this->fields = $fields;
    }

    public function getGrid($renderer = null)
    {
        if ($this->grid == null) {
            $this->setGrid($this->getServiceLocator()->get('zfcDatagrid'));
        }
        if ($renderer !== null) {
            $this->grid->setRendererName($renderer);
        }
        $this->addJoins();
        $this->initGrid();
        return parent::getGrid();
    }

    /**
     * @return Select
     */
    public function getSelect()
    {
        return $this->select;
    }

    /**
     * @return \Zend\Db\Adapter\Driver\ResultInterface
     */
    public function getDocument()
    {
        $select = clone $this->select;
        $select->reset(Select::LIMIT);
        $select->reset(Select::OFFSET);
        //$select->reset(Select::JOINS);
        $select->reset(Select::COLUMNS);
        $select->columns(array('DocumentID' => 'DocumentID'));

        $sql = $this->getSql();
        $statement = $sql->prepareStatementForSqlObject($select);

        return $statement->execute();
    }

    public function getCount()
    {
        if ($this->rowCount !== null) {
            return $this->rowCount;
        }

        $select = clone $this->select;
        $select->reset(Select::LIMIT);
        $select->reset(Select::OFFSET);
        $select->reset(Select::JOINS);
        $select->columns(array('DocumentID' => 'DocumentID'));

        $countSelect = new Select;
        $countSelect->columns(array('c' => new Expression('COUNT(1)')));
        $countSelect->from(array('original_select' => $select));

        $sql = $this->getSql();
        $statement = $sql->prepareStatementForSqlObject($countSelect);
        $result = $statement->execute();
        $row = $result->current();

        $this->rowCount = $row['c'];

        return $this->rowCount;
    }

    private function getSql()
    {
        if ($this->sql === null) {
            $this->sql = new Sql($this->adapter);
        }
        return $this->sql;
    }

    private function addJoins()
    {
        $taxon = new TableIdentifier('Target', 'Olap');
        $family = new TableIdentifier('Taxon', 'Olap');
        $location = new TableIdentifier('Location', 'Olap');
        $this->select
            ->join(array(
                't' => $taxon
            ), 't.TargetID = u.TargetFK', 'TargetName', Select::JOIN_LEFT)
            ->join(array(
                'f' => $family
            ), 'f.TaxonID = u.FamilyFK', 'FullName', Select::JOIN_LEFT)
            ->join(array(
                'l' => $location
            ), 'l.LocationID = u.LocationFK', 'LocationText', Select::JOIN_LEFT);

    }

    private function CriteriaToSelect($criteria)
    {
        $basicFilter = [
            'country' => 'u.CountryFK',
            'biog' => 'u.ProvinceFK',
            'adm' => 'u.ProvinceFK',
            'municipality' => 'u.CountyFK',
        ];

        $unit = new TableIdentifier('Unit', 'Olap');
        $select = new Select();
        $where = new Where();
        $idGen = new IdService();

        $select->from(array('u' => $unit));
        $where->equalTo('u.SourceFK', 31);

        // Taxon
        if (!empty($criteria['taxon'])) {
            $row = null;
            $term = $criteria['taxon'];

            // If no asterix then will try to find result from taxon table
            if (strpos($term, '*') === false) {
                $taxon = new TableIdentifier('Taxon', 'Olap');
                $taxonSelect = new Select($taxon);
                $taxonSelect->columns(array('TaxonID', 'RankFK'));
                $taxonSelect->where->equalTo('FullName', $criteria['taxon']);

                $sql = $this->getSql();
                $statement = $sql->prepareStatementForSqlObject($taxonSelect);
                $result = $statement->execute();
                $row = $result->current();
            }

            // If no result found so far we'll use the target table to try to find results
            if ($row == null) {
                $term = $this->getLikeFromAsterix($term, self::MIN_TAXON_LIKE_SEARCH);
                if ($term) {
                    $taxon = new TableIdentifier('Target', 'Olap');
                    $taxonSelect = new Select($taxon);
                    $taxonSelect->columns(array('TargetID'));
                    $taxonSelect->where->like('TargetName', $term);
                    $sql = $this->getSql();
                    $statement = $sql->prepareStatementForSqlObject($taxonSelect);
                    $results = $statement->execute();
                    $in = [];
                    foreach ($results as $result) {
                        $in[] = $result['TargetID'];
                    }
                    if (count($in)) {
                        $where->and->in('u.TargetFK', $in);
                    } else {
                        $this->noResults($where);
                    }
                }
            } else {
                // Taxon table had match so we'll use that one
                /** @var \Kotka\Service\FormElementService $formElementService */
                $formElementService = $this->getServiceLocator()->get('Kotka\Service\FormElementService');
                $ranks = $formElementService->getSelect('Olap.TaxonRank');
                if (isset($ranks[$row['RankFK']])) {
                    $predicate = $ranks[$row['RankFK']];
                    $where->and->equalTo('u.' . $predicate, $row['TaxonID']);
                } else {
                    $where->and->equalTo('u.TaxonFK', $row['TaxonID']);
                }
            }
        }

        // Basics
        foreach ($basicFilter as $filer => $field) {
            if (!empty($criteria[$filer])) {
                $this->andWhereEqualTo($where, $field, $criteria[$filer]);
            }
        }

        // Editor
        if (!empty($criteria['editor'])) {
            $userId = str_replace('MA.', '', $criteria['editor']);
            $this->andWhereEqualTo($where, 'u.UserID', $userId);
        }

        // Collection
        if (!empty($criteria['collection'])) {
            $project = new TableIdentifier('Project', 'Olap');
            $select->join(array(
                'p' => $project
            ), 'p.ProjectID = u.ProjectFK', 'CollectionURI', Select::JOIN_LEFT);
            $collection = $criteria['collection'];
            $collection = is_array($collection) ?
                array_map(array($idGen, 'getUri'), $collection) :
                $idGen::getUri($collection);
            $this->andWhereEqualTo($where, 'p.CollectionURI', $collection);
        }

        // Datasets
        if (!empty($criteria['dataset'])) {
            $datasetFacts = new TableIdentifier('DatasetFact', 'Olap');
            $sub1 = new Select($datasetFacts);
            $sWhere = new Where();
            $this->andWhereEqualTo($sWhere, 'LowerValueText', $criteria['dataset']);
            $sub1->columns(array('DatasetFK'))->where($sWhere);
            $where->and->in('u.DatasetFK', $sub1);
        }

        // Leg
        if (!empty($criteria['leg'])) {
            $leg = $criteria['leg'];

            $agent = new TableIdentifier('Agent', 'Olap');
            $sub1 = new Select($agent);
            $sWhere = new Where();
            if (strpos($leg, '*') !== false) {
                $leg = $this->getLikeFromAsterix($leg, self::MIN_LEG_LIKE_SEARCH);
                if ($leg == false) {
                    throw new \Exception('Too sort like search in leg');
                }
                $sWhere->like('FullName', $leg);
            } else {
                $sWhere->equalTo('FullName', $leg);
            }
            $sub1->columns(array('AgentID'))->where($sWhere);

            $teamAgents = new TableIdentifier('TeamAgent', 'Olap');
            $sub2 = new Select($teamAgents);
            $sWhere = new Where();
            $sWhere->in('AgentFK', $sub1);
            $sub2->columns(array('TeamFK'))->where($sWhere);

            $where->and->in('TeamFK', $sub2);

        }

        // Ykj
        if (!empty($criteria['ykj'])) {
            list($lat, $lon) = explode(':', trim($criteria['ykj']));
            $latTimer = pow(10, (7 - strlen($lat)));
            $lonTimer = pow(10, (7 - strlen($lon)));
            $lat = (int)$lat;
            $lon = (int)$lon;
            $where->and
                ->lessThanOrEqualTo('u.YKJNMax', ($lat + 1) * $latTimer)
                ->greaterThanOrEqualTo('u.YKJNMax', $lat * $latTimer)
                ->lessThanOrEqualTo('u.YKJEMax', ($lon + 1) * $lonTimer)
                ->greaterThanOrEqualTo('u.YKJEMax', $lon * $lonTimer);
        }

        // Gathering date
        if (!empty($criteria['date'])) {
            $partsRange = explode('/', $criteria['date']);
            $to = $from = explode('-', $partsRange[0], 3);
            $partCnt = count($from);
            if (isset($partsRange[1])) {
                $to = explode('-', $partsRange[1], 3);
                for ($i = $partCnt, $c = count($to); $i > $c; $i--) {
                    $idx = 3 - $i;
                    array_unshift($to, $from[$idx]);
                }
            }
            $range = [];
            switch ($partCnt) {
                case 1:
                    $range['from'] = $from[0] . '0000';
                    $range['to'] = $to[0] . 9999;
                    break;
                case 2:
                    $range['from'] = $from[0] . $from[1] . '00';
                    $range['to'] = $to[0] . $to[1] . 99;
                    break;
                case 3:
                default:
                    $range['from'] = $from[0] . $from[1] . $from[2];
                    $range['to'] = $to[0] . $to[1] . $to[2];
                    break;
            }
            $where->and
                ->greaterThanOrEqualTo('u.DateBeginFk', $range['from'])
                ->lessThanOrEqualTo('u.DateEndFk', $range['to']);
        }

        // Identifier
        if (!empty($criteria['identifier'])) {
            $objects = $criteria['identifier'];
            $objects = str_replace(array("\r", "\n"), array('', ','), $objects);
            $objects = explode(',', $objects);
            $in = [];
            $between = [];
            $like = [];
            foreach ($objects as $object) {
                $object = trim($object);
                if (strpos($object, '-') !== false) {
                    $from = explode('-', $object);
                    if (empty($from[0]) || empty($from[1])) {
                        continue;
                    }
                    list($ns, $oid) = explode('.', $from[0], 2);
                    if (empty($ns) || empty($oid)) {
                        continue;
                    }
                    $start = $from[0];
                    $nsLen = strlen($ns . '.');
                    $endLen = strlen($ns . '.' . $from[1]);
                    while (($len = strlen($start)) < $endLen) {
                        $len -= $nsLen;
                        $end = $ns . '.' . str_repeat('9', $len);
                        $between[] = [
                            'from' => $idGen::getUri($start),
                            'to' => $idGen::getUri($end),
                        ];
                        $start = $ns . '.1' . str_repeat('0', $len);
                    }
                    $between[] = [
                        'from' => $idGen::getUri($start),
                        'to' => $idGen::getUri($ns . '.' . $from[1]),
                    ];
                } else if (!empty($object)) {
                    if (strpos($object, '.') === false) {
                        $like[] = $idGen::getUri($object);
                    } else {
                        $in[] = $idGen::getUri($object);
                    }

                }
            }
            $identifiers = new Predicate();
            if (count($between)) {
                foreach ($between as $range) {
                    $identifier = new Predicate();
                    $exp = new Expression('u.DocumentID BETWEEN ? AND ? AND LEN(u.DocumentID) = ?', $range['from'], $range['to'], strlen($range['from']));
                    $identifier->addPredicate($exp);
                    $identifiers->orPredicate($identifier);
                }
            }
            if (count($in)) {
                $identifiers->or->in('u.DocumentID', $in);
            }
            if (count($like)) {
                foreach ($like as $ns) {
                    $identifier = new Predicate();
                    $identifier->like('u.DocumentID', $ns . '.%');
                    $identifiers->orPredicate($identifier);
                }
            }
            $where->andPredicate($identifiers);
        }
        // EO Identifier

        // Locality
        if (!empty($criteria['locality'])) {
            $like = str_replace('*', '%', $criteria['locality']);
            $where->and->like('l.locationText', $like);
        }

        $select->order(array(new Expression('LEN(u.DocumentID)'), 'u.DocumentID'));
        $select->where($where);

        return $select;
    }

    private function noResults(Where $where)
    {
        $where->and->addPredicate(new Expression('1 = 0'));
    }

    private function getLikeFromAsterix($string, $maxLength = null)
    {
        $termStart = strpos($string, '*') === 0;
        $termEnd = strpos($string, '*', 1) === (strlen($string) - 1);
        $term = str_replace('*', '', $string);
        if (is_int($maxLength) && strlen($term) < $maxLength) {
            return false;
        }
        if ($termStart) {
            $term = '%' . $term;
        }
        if ($termEnd) {
            $term .= '%';
        }

        return $term;
    }

    private function andWhereEqualTo(Where $where, $field, $value)
    {
        if (is_array($value)) {
            if (count($value) > 1) {
                $where->and->in($field, $value);
            } else {
                $value = array_pop($value);
                $where->and->equalTo($field, $value);
            }
        } else if (is_string($value)) {
            $where->and->equalTo($field, $value);
        } else {
            throw new \Exception("Invalid value given as search parameter");
        }
    }

    public function addSpotParameter($type = 'document')
    {
        $this->getUrlColumn()->addSpot($type);
    }

    private function initGrid()
    {
        $perPage = (isset($this->criteria['perPage'])) ? $this->criteria['perPage'] : self::DEFAULT_PAGE_SIZE;
        $perPage = $perPage > 100 || $perPage < 1 ? self::DEFAULT_PAGE_SIZE : $perPage;
        $page = (isset($this->criteria['page'])) ? $this->criteria['page'] : 1;

        /** @var \ZfcDatagrid\Datagrid $dataGrid */
        $this->grid->setTitle('Specimens');
        $this->grid->setDefaultItemsPerPage($perPage);
        $this->grid->setUserFilterDisabled();
        $this->grid->setDataSource($this->select, $this->adapter);

        $this->getUrlColumn()->setPage($page);

        if ($this->fields !== null) {
            foreach ($this->fields as $field => $tableId) {
                $col = new Column\Select($field, $tableId);
                $col->setLabel($field);
                $this->grid->addColumn($col);
            }
            return;
        }

        $col = new Column\Select('DocumentID', 'u');
        $col->setLabel('Identifier');
        $col->setWidth(10);
        $col->setType($this->getUrlColumn());
        //$col->setSortDefault(1, 'ASC');
        $this->grid->addColumn($col);

        $col = new Column\Select('FullName', 'f');
        $col->setLabel('Family');
        $col->setWidth(25);
        $this->grid->addColumn($col);

        $col = new Column\Select('TargetName', 't');
        $col->setLabel('Taxon');
        $col->setWidth(25);
        $this->grid->addColumn($col);

        $col = new Column\Select('LocationText', 'l');
        $col->setLabel('Location');
        $col->setWidth(15);
        $this->grid->addColumn($col);

        $dateType = new Column\Type\DateTime('Ymd', \IntlDateFormatter::SHORT);
        $col = new Column\Select('DateBeginFK', 'u');
        $col->setLabel('Gathering start');
        $col->setWidth(15);
        $col->setType($dateType);
        $this->grid->addColumn($col);

        $dateType = new Column\Type\DateTime('Ymd', \IntlDateFormatter::SHORT);
        $col = new Column\Select('DateEndFK', 'u');
        $col->setLabel('Gathering end');
        $col->setWidth(15);
        $col->setType($dateType);
        $this->grid->addColumn($col);

        $col = new Column\Select('userID', 'u');
        $col->setLabel('Editor');
        $col->setWidth(20);
        $col->setReplaceValues($this->getUsersArray());
        $this->grid->addColumn($col);
    }

    private function getUrlColumn()
    {
        if ($this->urlCol === null) {
            $this->urlCol = new URI();
            $this->urlCol->setAddView(true);
        }
        return $this->urlCol;
    }

    private function getUsersArray()
    {
        /** @var \Kotka\Service\FormElementService $elementService */
        $elementService = $this->getServiceLocator()->get('Kotka\Service\FormElementService');
        $users = $elementService->getSelect('MA.person', array());
        $users = array_flip(str_replace('MA.', '', array_flip($users)));
        return $users;
    }
}