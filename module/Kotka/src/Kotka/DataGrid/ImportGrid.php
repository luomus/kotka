<?php

namespace Kotka\DataGrid;


use Kotka\Controller\ToolsController;
use Kotka\DataGrid\Type\URI;
use Kotka\Enum\Document;
use Kotka\Service\ImportService;
use Kotka\Service\ValidationService;
use Kotka\Stdlib\ArrayUtils;
use ZfcDatagrid\Column;
use ZfcDatagrid\Column\Style;
use ZfcDatagrid\Column\Type;
use ZfcDatagrid\Filter;

class ImportGrid extends AbstractGrid
{

    private $labels;

    /**
     * @return null|array
     */
    public function getLabels()
    {
        return $this->labels;
    }

    /**
     * @param array $labels
     */
    public function setLabels(array $labels)
    {
        $this->labels = $labels;
    }

    public function mergeMessages(&$item, $messages, $text, $color = null)
    {
        $ret = array();
        $textWrap = '%s';
        if ($color !== null) {
            $textWrap = '<span class="import-error" style="color:' . $color . '">%s</span>';
        }
        foreach ($messages as $spot => $value) {
            if (!isset($item[$spot]) || $spot === 'subject' || !is_array($item)) {
                $ret[$spot] = $value;
                continue;
            }
            if (!is_array($value)) {
                $item .= sprintf($textWrap, $text . $value);
                continue;
            }
            $nextKey = key($value);
            if ($nextKey === 'subject') {
                $ret[$spot] = $value;
                continue;
            }
            if (!isset($item[$spot][$nextKey])) {
                $msgs = array();
                $break = false;
                foreach ($value as $msg) {
                    if (is_array($msg)) {
                        $ret[$spot] = array();
                        $ret[$spot][$nextKey] = $msg;
                        $break = true;
                    }
                    $msgs[] = $msg;
                }
                if ($break) {
                    continue;
                }
                if (isset($item[$spot]) && is_string($item[$spot])) {
                    $item[$spot] .= sprintf($textWrap, $text . implode('<br>', $msgs));
                    continue;
                } else if (isset($item[$spot][0]) && is_string($item[$spot][0])) {
                    $item[$spot][0] .= sprintf($textWrap, $text . implode('<br>', $msgs));
                    continue;
                }
            }
            $ret[$spot] = $this->mergeMessages($item[$spot], $messages[$spot], $text, $color);
        }

        return $ret;
    }

    public function prepareGrid($data, $showAll = false, $uriAsText = false)
    {
        $allKeys = array();
        $flats = array();
        $cnt = 0;
        foreach ($data as $key => $item) {
            if ($key === ImportService::RESET_KEY) {
                continue;
            }
            $errors = null;
            $warnings = null;
            if (isset($item['errors'])) {
                $errors = $item['errors'];
                unset($item['errors']);
            }
            if (isset($item['warnings'])) {
                $warnings = $item['warnings'];
                unset($item['warnings']);
            }
            if (isset($item['_model_'])) {
                unset($item['_model_']);
            }
            $flats[$cnt] = ArrayUtils::flatten_with_keys($item);
            $allKeys += array_flip(array_keys($flats[$cnt]));
            if ($errors !== null) {
                $flats[$cnt]['errors'] = $errors;
                $allKeys['errors'] = true;
            }
            if ($warnings !== null) {
                $flats[$cnt]['warnings'] = $warnings;
                $allKeys['warnings'] = true;
            }
            $cnt++;
        }

        $this->grid->setTitle('Imported data');
        if ($showAll) {
            $this->grid->setDefaultItemsPerPage(count($data));
        } else {
            $this->grid->setDefaultItemsPerPage(100);
        }
        $this->grid->setUserFilterDisabled();
        $this->grid->setDataSource($flats);

        if (isset($allKeys[ImportService::IMPORT_STATUS_KEY])) {
            unset($allKeys[ImportService::IMPORT_STATUS_KEY]);
        }

        if (isset($allKeys[ToolsController::TYPE_KEY])) {
            $col = new Column\Select(ToolsController::TYPE_KEY);
            $col->setUserSortDisabled();
            $col->setLabel('Type');
            $col->setWidth(10);
            $this->grid->addColumn($col);
            unset($allKeys[ToolsController::TYPE_KEY]);
        }

        if (isset($allKeys['subject'])) {
            $col = new Column\Select('subject');
            $col->setUserSortDisabled();
            $col->setLabel('Uri');
            $col->setWidth(10);
            $type = new URI();
            $type->setAsText($uriAsText);
            $type->setAddView(true);
            $col->setType($type);
            $this->grid->addColumn($col);
            unset($allKeys['subject']);
        }

        if (isset($allKeys['errors'])) {
            $col = new Column\Select('errors');
            $col->setUserSortDisabled();
            $col->setLabel('Errors');
            $col->setWidth(10);
            $col->setType(new Column\Type\Error());
            $col->setSortDefault(1, 'DESC');
            $this->grid->addColumn($col);
            unset($allKeys['errors']);
        }
        if (isset($allKeys['warnings'])) {
            $col = new Column\Select('warnings');
            $col->setUserSortDisabled();
            $col->setLabel('Warnings');
            $col->setWidth(10);
            $col->setType(new Column\Type\Error());
            $this->grid->addColumn($col);
            unset($allKeys['warnings']);
        }
        if (isset($allKeys[ValidationService::GENERIC_KEY])) {
            unset($allKeys[ValidationService::GENERIC_KEY]);
        }
        if (isset($allKeys[ValidationService::IMPORT_KEY])) {
            unset($allKeys[ValidationService::IMPORT_KEY]);
        }
        if (isset($allKeys['MZOwner'])) {
            unset($allKeys['MZOwner']);
        }
        if (isset($allKeys['MYDatatype'])) {
            unset($allKeys['MYDatatype']);
        }
        $unreliable = false;
        if (isset($allKeys['MYUnreliableFields'])) {
            $unreliable = $allKeys['MYUnreliableFields'];
            unset($allKeys['MYUnreliableFields']);
        }

        foreach ($allKeys as $value => $index) {
            $label = $this->getLabel($value);
            $col = new Column\Select($value);
            $col->setLabel($label);
            $col->setWidth(5);
            $col->setUserSortDisabled();

            $style = new Style\BackgroundColor(242, 222, 222);
            $style->addByValue($col, ToolsController::CELL_HAS_ERRORS, Filter::LIKE);
            $col->addStyle($style);

            $this->grid->addColumn($col);
        }

        if ($unreliable) {
            $col = new Column\Select('MYUnreliableFields');
            $col->setLabel('MYUnreliableFields');
            $col->setWidth(5);
            $col->setUserSortDisabled();

            $style = new Style\BackgroundColor(242, 222, 222);
            $style->addByValue($col, ToolsController::CELL_HAS_ERRORS, Filter::LIKE);
            $col->addStyle($style);

            $this->grid->addColumn($col);
        }

    }

    private function getLabel($field)
    {
        if ($this->labels !== null) {
            if (isset($this->labels[$field])) {
                return $this->labels[$field];
            }
            return $field;
        }
        return $this->getLabelFromHeader(explode(']', $field));
    }

    private function getLabelFromHeader($parts)
    {
        $label = array_pop($parts);
        if (empty($label) && count($parts) > 0) {
            $label = array_pop($parts);
        }
        $label = ltrim($label, '[');
        $start = explode('[', $label);
        $label = array_shift($start);
        if (is_numeric($label)) {
            return $this->getLabelFromHeader($parts);
        }
        return $label;
    }
}