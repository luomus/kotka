<?php

namespace Kotka\DataGrid;

class SeedGrid extends TypeGrid
{
    const DEFAULT_PAGE_SIZE = 100000;

    protected $fields = [
        'documentID',
        'family',
        'provenance',
        'IPEN',
        'acceptedTaxon',
        'country',
        'biologicalProvince',
        'municipality',
        'locality',
        'localityDescription',
        'latitude',
        'longitude',
        'habitatDescription',
        'populationAbundance',
        'gatheringNotes',
        'leg',
        'dateBegin',
        'taxon',
        'TaxonAndInfra',
        'author',
        'infraRank',
        'infraEpithet',
        'infraAuthor'
    ];

}