<?php
namespace Kotka\DataGrid;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ImportGridFactory implements FactoryInterface
{
    /**
     * @see \Zend\ServiceManager\FactoryInterface::createService()
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $class = new ImportGrid();
        $class->setGrid($serviceLocator->get('zfcDatagrid'));

        return $class;
    }
}