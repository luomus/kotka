<?php
namespace Kotka\DataGrid;


use ZfcDatagrid\Datagrid;

abstract class AbstractGrid implements GridInterface
{

    /** @var  Datagrid */
    protected $grid;
    protected $perPage = 100;

    public function setGrid($grid)
    {
        $this->grid = $grid;
    }

    public function getGrid($render = true)
    {
        if ($render) {
            $this->grid->render();
        }
        return $this->grid;
    }

} 