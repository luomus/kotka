<?php

namespace Kotka\DataGrid\Type;


use ZfcDatagrid\Column\Type\AbstractType;

class GeoPointType extends AbstractType
{

    const MAP_HTML = '<img src="/maps?latitude=%s&longitude=%s&color=cacaca&scale=0.4&dot=1.5" style="width:150px;height:auto;">';

    protected $centerLat;
    protected $centerLon;

    /**
     * @param null|float $lat
     * @param null|float $lon
     */
    public function __construct($lat = null, $lon = null)
    {
        $this->centerLat = $lat;
        $this->centerLon = $lon;
    }

    /**
     * @return float
     */
    public function getCenterLat()
    {
        return $this->centerLat;
    }

    /**
     * @param float $centerLat
     */
    public function setCenterLat($centerLat)
    {
        $this->centerLat = $centerLat;
    }

    /**
     * @return float
     */
    public function getCenterLon()
    {
        return $this->centerLon;
    }

    /**
     * @param float $centerLon
     */
    public function setCenterLon($centerLon)
    {
        $this->centerLon = $centerLon;
    }

    /**
     * Get the type name
     *
     * @return string
     */
    public function getTypeName()
    {
        return "generic";
    }

    /**
     * Convert the value from the source to the value, which the user will see
     *
     * @param  string $val
     * @return string
     */
    public function getUserValue($val)
    {
        if (is_array($val)) {
            if (isset($val['lat']) && isset($val['lon']) && !URI::$onlyText) {
                $val = sprintf(self::MAP_HTML, $val['lat'], $val['lon']);
            } else {
                $val = implode('; ', $val);
            }

        }
        return $val;
    }

}