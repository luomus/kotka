<?php

namespace Kotka\DataGrid\Type;


use ZfcDatagrid\Column\Type\AbstractType;

class GenericType extends AbstractType
{

    /**
     * Get the type name
     *
     * @return string
     */
    public function getTypeName()
    {
        return "generic";
    }

    /**
     * Convert the value from the source to the value, which the user will see
     *
     * @param  string $val
     * @return string
     */
    public function getUserValue($val)
    {
        if (is_array($val)) {
            $val = implode('; ', $val);
        }
        return $val;
    }

}