<?php
namespace Kotka\DataGrid\Type;

use Common\Service\IdService;

class SampleURI extends URI
{

    public function getTypeName()
    {
        return 'sample-uri';
    }

    /**
     * Convert a value into an array
     *
     * @param mixed $value
     * @param bool $addView
     * @param bool $addEdit
     * @return array
     */
    public function getUserValue($value, $addView = null, $addEdit = null)
    {
        if (empty($value)) {
            return '';
        }

        $uri = IdService::getUri($value);
        $parts = IdService::getUriInParts($uri);
        $viewStr = sprintf('%s<br><span>%s</span>', $this->escaper->escapeHtml($parts[0]), $this->escaper->escapeHtml($parts[1]));
        if (self::$onlyText) {
            return $uri;
        }
        $viewStr = sprintf('<a href="' . $this->viewUrl .  '">%s</a>', $uri, $viewStr);
        return sprintf('<table class="document-uri-wrapper"><tr><td class="document-uri">%s</td></tr></table>', $viewStr);
    }
}
