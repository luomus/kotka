<?php
namespace Kotka\DataGrid\Type;

use Common\Service\IdService;
use Zend\Escaper\Escaper;
use ZfcDatagrid\Column\Type\AbstractType;

class URI extends AbstractType
{
    /** @var bool This is used to indicate that the value should caontain only text */
    public static $onlyText = false;

    protected static $spot = 1;

    protected $editUrl = '/specimens/edit?uri=%s';
    protected $viewUrl = '/view?uri=%s';
    protected $addView = false;
    protected $addEdit = true;
    protected $addSpot = false;
    protected $asText  = false;
    protected $page;
    protected $escaper;

    public function __construct()
    {
        $this->escaper = new Escaper('utf-8');
    }

    public function __invoke($value, $addView = null, $addEdit = null)
    {
        return $this->getUserValue($value, $addView, $addEdit);
    }

    /**
     * @return string
     */
    public function getEditUrl()
    {
        return $this->editUrl;
    }

    /**
     * @param string $editUrl
     */
    public function setEditUrl($editUrl)
    {
        $this->editUrl = urldecode($editUrl);
    }

    public function getTypeName()
    {
        return 'uri';
    }

    /**
     * @return string
     */
    public function getViewUrl()
    {
        return $this->viewUrl;
    }

    /**
     * @param string $viewUrl
     */
    public function setViewUrl($viewUrl)
    {
        $this->viewUrl = urldecode($viewUrl);
    }

    /**
     * @return boolean
     */
    public function getAddView()
    {
        return $this->addView;
    }

    /**
     * @param boolean $addView
     */
    public function setAddView($addView)
    {
        $this->addView = $addView;
    }

    /**
     * @return boolean
     */
    public function getAddEdit()
    {
        return $this->addEdit;
    }

    /**
     * @param boolean $addEdit
     */
    public function setAddEdit($addEdit)
    {
        $this->addEdit = $addEdit;
    }

    /**
     * @return boolean
     */
    public function isAsText()
    {
        return $this->asText;
    }

    /**
     * @param boolean $asText
     */
    public function setAsText($asText)
    {
        $this->asText = $asText;
    }

    /**
     * Adds spot parameter to urls
     * @param $addSpot
     */
    public function addSpot($addSpot)
    {
        $this->addSpot = $addSpot;
    }

    public function setPage($page)
    {
        $this->page = $page;
    }

    /**
     * Convert a value into an array
     *
     * @param mixed $value
     * @param bool $addView
     * @param bool $addEdit
     * @return array
     */
    public function getUserValue($value, $addView = null, $addEdit = null)
    {
        if (empty($value)) {
            return '';
        }
        $uri = IdService::getUri($value);
        if ($this->asText) {
            return $uri;
        }
        $addView = $addView === null ? $this->addView : $addView;
        $addEdit = $addEdit === null ? $this->addEdit : $addEdit;
        $parts = IdService::getUriInParts($uri);
        $page = $this->page !== null ? $this->page : '';
        $spot = $this->addSpot !== false ? ('&type=' . $this->addSpot . '&page=' . $page . '&spot=' . self::$spot++) : '';
        $viewStr = sprintf('%s<br><span>%s</span>', $this->escaper->escapeHtml($parts[0]), $this->escaper->escapeHtml($parts[1]));
        $editStr = '';
        if (self::$onlyText) {
            return $uri;
        }
        if ($addView) {
            $viewStr = sprintf('<a href="' . $this->viewUrl . $spot . '">%s</a>', $uri, $viewStr);
        }
        if ($addEdit) {
            $editStr = sprintf('<td class="edit-link"><a href="' . $this->editUrl . $spot . '"><i class="fa fa-edit btn btn-primary"></i></a></td>', $uri);
        }
        return sprintf('<table class="document-uri-wrapper"><tr>%s<td class="document-uri">%s</td></tr></table>', $editStr, $viewStr);
    }
}
