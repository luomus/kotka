<?php
namespace Kotka\DataGrid\Type;

use Common\Service\IdService;
use Zend\Escaper\Escaper;
use ZfcDatagrid\Column\Type\AbstractType;

class BranchURI extends AbstractType
{
    /** @var bool This is used to indicate that the value should caontain only text */
    public static $onlyText = false;

    private static $spot = 1;

    private $viewUrl = '/accessions?uri=%s';
    private $escaper;

    public function __construct()
    {
        $this->escaper = new Escaper('utf-8');
    }

    public function __invoke($value, $addView = null, $addEdit = null)
    {
        return $this->getUserValue($value, $addView, $addEdit);
    }

    public function getTypeName()
    {
        return 'branch-uri';
    }

    /**
     * @return string
     */
    public function getViewUrl()
    {
        return $this->viewUrl;
    }

    /**
     * @param string $viewUrl
     */
    public function setViewUrl($viewUrl)
    {
        $this->viewUrl = urldecode($viewUrl);
    }

    /**
     * Convert a value into an array
     *
     * @param mixed $value
     * @param bool $addView
     * @param bool $addEdit
     * @return array
     */
    public function getUserValue($value, $addView = null, $addEdit = null)
    {
        if (empty($value)) {
            return '';
        }
        $uri = IdService::getUri($value);
        $parts = IdService::getUriInParts($uri);
        $viewStr = sprintf('%s<br><span>%s</span>', $this->escaper->escapeHtml($parts[0]), $this->escaper->escapeHtml($parts[1]));
        if (self::$onlyText) {
            return $uri;
        }
        $viewStr = sprintf('<a href="' . $this->viewUrl .  '">%s</a>', $uri, $viewStr);
        return sprintf('<table class="document-uri-wrapper"><tr><td class="document-uri">%s</td></tr></table>', $viewStr);
    }
}
