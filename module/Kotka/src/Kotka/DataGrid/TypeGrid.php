<?php

namespace Kotka\DataGrid;


use Elastic\Client\Search;
use Elastic\Grid\DataSource;
use Elastic\Result\TypeMapping;
use Kotka\DataGrid\Type\GenericType;
use Kotka\DataGrid\Type\GeoPointType;
use Kotka\ElasticExtract\Specimen;
use Common\Service\IdService;
use ZfcDatagrid\Column;
use ZfcDatagrid\Renderer\Json\Renderer;

class TypeGrid extends ElasticGrid
{

    const DEFAULT_PAGE_SIZE = 1000;

    protected $fields = [
        'typeStatus',
        'typeSpecies',
        'typeAuthor',
        'typeVerification',
        'taxon',
        'author',
        'collection'
    ];

    public function prepareGrid($criteria, Search $search = null)
    {
        if ($search === null) {
            throw new \Exception("You need to specify search adapter to the typeGrid!");
        }
        $this->search = $search;
        $this->mappings = $search->getMappings(Specimen::getIndexName(), Specimen::getTypeName());
        $this->criteria = $criteria;
        $this->query = $this->CriteriaToQuery($criteria);
    }

    public function CriteriaToQuery($criteria)
    {
        $qname = isset($criteria['uri']) ? IdService::getQName($criteria['uri'], true) : 'HR.128';
        if (!isset($criteria['s']) || !is_array($criteria['s'])) {
            $criteria['s'] = [];
        }
        $criteria['s'][] = 'collectionTree: "' . $qname . '"';
        return parent::CriteriaToQuery($criteria);
    }

    public function getGrid()
    {
        if ($this->grid == null) {
            $this->setGrid($this->serviceLocator->get('zfcDatagrid'));
            Renderer::$params = $this->criteria;
            $this->grid->setRendererName('json');
        }
        return parent::getGrid();
    }

    protected function initGrid()
    {
        $perPage = (isset($this->criteria['perPage'])) ? $this->criteria['perPage'] : self::DEFAULT_PAGE_SIZE;
        $perPage = $perPage > 100 || $perPage < 1 ? self::DEFAULT_PAGE_SIZE : $perPage;
        $page = (isset($this->criteria['page'])) ? $this->criteria['page'] : 1;

        /** @var \ZfcDatagrid\Datagrid $dataGrid */
        $this->grid->setTitle('Results');
        $this->grid->setDefaultItemsPerPage($perPage);
        $this->grid->setUserFilterDisabled();
        $this->grid->setDataSource(new DataSource($this->query, $this->search, $this->mappings));

        $this->getUrlColumn()->setPage($page);

        $col = new Column\Select('typeSpecies' . TypeMapping::RAW_SUFFIX);
        $col->setHidden(true);
        $col->setWidth(10);
        $col->setSortDefault();
        $this->grid->addColumn($col);

        $col = new Column\Select('documentQName');
        $col->setLabel('Identifier');
        $col->setType($this->getUrlColumn());
        $this->grid->addColumn($col);

        if ($this->fields !== null) {
            foreach ($this->fields as $field) {
                $type = $this->mappings->getPropertyType($field);
                if ($type === null) {
                    continue;
                }
                $label = ucfirst($field);
                $col = new Column\Select($field);
                $col->setLabel($label);
                $this->grid->addColumn($col);
                if ($type == 'geo_point') {
                    $col->setUserSortDisabled(true);
                    $col->setType(new GeoPointType());
                } else {
                    $col->setType(new GenericType());
                }
            }
            return;
        }
    }
}