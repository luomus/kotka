<?php

namespace Kotka\DataGrid;


use Elastic\Client\Search;
use Elastic\Extract\ExtractInterface;
use Elastic\Grid\DataSource;
use Elastic\Query\Aggregate;
use Elastic\Query\Filter\GeoDistanceFilter;
use Elastic\Query\Query;
use Elastic\Result\TypeMapping;
use Kotka\DataGrid\Type\GenericType;
use Kotka\DataGrid\Type\GeoPointType;
use Kotka\DataGrid\Type\URI;
use Kotka\ElasticExtract\Specimen;
use Kotka\Model\IdentifierInterpreter;
use Zend\Db\Sql\Select;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use ZfcDatagrid\Column;

class ElasticGrid extends AbstractGrid implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    const CRITERIA_IDENTIFIER = 'identifier';
    const DEFAULT_PAGE_SIZE = 25;
    const MIN_TAXON_LIKE_SEARCH = 4;
    const MIN_LEG_LIKE_SEARCH = 3;
    const DATASETS_PARAMETER_FK = 212;
    const MAX_IDS = 1000;

    /** @var Search */
    protected $search;
    /** @var Query */
    protected $query;
    protected $criteria;
    protected $urlCol;
    protected $fields;
    /** @var TypeMapping */
    protected $mappings;
    protected $unMeaningFul = [
        'loc' => '',
        'identifier' => '',
        'q' => '',
        'itemsPerPage' => self::DEFAULT_PAGE_SIZE,
        'page' => ['', '1'],
        'format' => '',
        'export' => '',
        'perPage' => '',
        'sortBy' => '',
        'sortDirection' => '',
        'labelFields' => 'all',
    ];

    protected $centerLat;
    protected $centerLon;
    protected $extract;

    public function __construct(ExtractInterface $extract = null)
    {
        $this->extract = $extract === null ? new Specimen() : $extract;
    }

    public function prepareGrid($criteria, Search $search = null, $fields = null)
    {
        if ($search !== null) {
            $extract = $this->extract;
            $this->search = $search;
            $this->mappings = $search->getMappings($extract::getIndexName(), $extract::getTypeName());
        }
        $this->criteria = $criteria;
        $this->query = $this->CriteriaToQuery($criteria);
        $this->fields = $fields;
    }

    public function getGrid($renderer = null, $render = true)
    {
        if ($this->grid == null) {
            $this->setGrid($this->serviceLocator->get('zfcDatagrid'));
        }
        if ($renderer !== null) {
            $this->grid->setRendererName($renderer);
        }
        $this->initGrid();
        return parent::getGrid($render);
    }

    /**
     * @return Select
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * @param array|null $fields
     * @return \Elastic\Result\Result
     * @throws \Exception
     */
    public function getDocument(array $fields = null)
    {
        $query = clone $this->query;
        $query->setSize(100000);
        $query->clearAggregate();
        $query->addSort('objectID', Query::SORT_ASC);
        if ($fields !== null) {
            $query->setFields($fields);
        }
        return $this->search->query($query)->execute();
    }

    public function addSpotParameter($type = 'document')
    {
        $this->getUrlColumn()->addSpot($type);
    }

    /**
     * @return array
     */
    public function getCriteria()
    {
        return $this->criteria;
    }

    /**
     * @param array $criteria
     */
    public function setCriteria(array $criteria)
    {
        $this->criteria = $criteria;
    }

    public function getMeaningfulParams($params)
    {
        if (!is_array($params)) {
            return $params;
        }
        foreach ($this->unMeaningFul as $field => $value) {
            if (
                isset($params[$field]) && (
                    $value === 'all' ||
                    is_string($params[$field]) && (
                        (is_array($value) && in_array($params[$field], $value)) ||
                        (!is_array($value) && $params[$field] === '' . $value)
                    )
                )
            ) {
                unset($params[$field]);
            }
        }
        return $params;
    }

    public function CriteriaToQuery($criteria)
    {
        $this->centerLat = null;
        $this->centerLon = null;
        $query = new Query();
        $extract = $this->extract;
        $query->setIndex($extract::getIndexName());
        if (!empty($criteria['term'])) {
            if (!is_array($criteria['term'])) {
                $criteria['term'] = [$criteria['term']];
            }
            foreach ($criteria['term'] as $field) {
                $query->addAggregate(new Aggregate('terms', $field, ucfirst($field)));
            }
        }

        $search = [];
        $filters = [];
        if (!empty($criteria['s'])) {
            if (!is_array($criteria['s'])) {
                $criteria['s'] = [$criteria['s']];
            }
            $filters = $criteria['s'];
        }
        if (!empty($criteria['q'])) {
            $filters[] = $criteria['q'];
        }
        if (isset($criteria['accepted']) && $criteria['accepted'] == "1") {
            $search[] = 'accepted: true';
        }
        if (isset($criteria['pic']) && $criteria['pic'] == "1") {
            $search[] = 'hasPicture: true';
        }
        foreach ($filters as $filter) {
            $search[] = $filter;
        }
        if (isset($criteria['types']) && $criteria['types'] == "1") {
            $search[] = 'isType: true';
        }
        if (isset($criteria['primary']) && $criteria['primary'] == "1") {
            $search[] = 'primarySpecimen: true';
        }

        // Expects the geo point to be in format lat,lon,distance
        if (isset($criteria['loc']) && !empty($criteria['loc'])) {
            $parts = explode(',', $criteria['loc']);
            if (count($parts) === 3) {
                $this->centerLat = floatval($parts[0]);
                $this->centerLon = floatval($parts[1]);
                $filter = new GeoDistanceFilter('wgs84Location', $this->centerLat, $this->centerLon, $parts[2]);
                $query->addFilter($filter);
            }
        }

        $objects = isset($criteria[self::CRITERIA_IDENTIFIER]) ? trim($criteria[self::CRITERIA_IDENTIFIER]) : '';
        if (!empty($objects)) {
            $objects = str_replace(array("\r", "\n"), array('', ','), $objects);
            $objects = str_replace(',,', ',', $objects);
            $objects = explode(',', $objects);
            $ids = [];
            if (count($objects) > self::MAX_IDS) {
                throw new \Exception("Too many identifiers specified! Max is " . self::MAX_IDS . ". Try to use ranges instead");
            }
            foreach ($objects as $object) {
                $interpreter = new IdentifierInterpreter($object, null, $criteria['identifierAsRange'] === '1');
                $queryStr = '';
                if ($interpreter->hasDomain()) {
                    $queryStr .= '(domain: ' . $interpreter->getDomain() . ') AND ';
                }
                if ($interpreter->isRange()) {
                    $queryStr .= sprintf('objectID: [%s TO %s]', $interpreter->getRangeStart(), $interpreter->getRangeEnd());
                    if (!empty($interpreter->getNs())) {
                        $queryStr .= ' AND namespace: ' . $interpreter->getNs();
                    }
                } else {
                    if ($interpreter->isOnlyNS()) {
                        $queryStr .= 'namespace: ' . $interpreter->getNs();
                    } else if ($interpreter->isOnlyOId() && !empty($interpreter->getNs())) {
                        $queryStr .= 'objectID: ' . $interpreter->getNs();
                    } else {
                        $queryStr .= 'documentID: "' . $interpreter->getDocumentId() . '"';
                    }
                }
                $ids[] = '(' . $queryStr . ')';
            }
            if (!empty($ids)) {
                $search[] = implode(' OR ', $ids);
            }
        }
        $search = array_filter($search);
        if (empty($search)) {
            $search[] = '*';
        }
        $queryStr = '(' . implode(') AND (', $search) . ')';
        $query->setQueryString($queryStr);

        return $query;
    }

    protected function clearObjectID($oid)
    {
        $oid = str_replace('-', '', $oid);
        return (int)filter_var($oid, FILTER_SANITIZE_NUMBER_INT);
    }

    protected function initGrid()
    {
        $perPage = (isset($this->criteria['perPage'])) ? $this->criteria['perPage'] : self::DEFAULT_PAGE_SIZE;
        $perPage = $perPage > 100 || $perPage < 1 ? self::DEFAULT_PAGE_SIZE : $perPage;
        $page = (isset($this->criteria['page'])) ? $this->criteria['page'] : 1;

        /** @var \ZfcDatagrid\Datagrid $dataGrid */
        $this->grid->setTitle('Results');
        $this->grid->setDefaultItemsPerPage($perPage);
        $this->grid->setUserFilterDisabled();
        $this->grid->setDataSource(new DataSource($this->query, $this->search, $this->mappings));

        $urlCol = $this->getUrlColumn();
        if (method_exists($urlCol, 'setPage')) {
            $urlCol->setPage($page);
        }

        $col = new Column\Select('objectID');
        $col->setHidden(true);
        $col->setWidth(10);
        $col->setSortDefault(1, 'ASC');
        $this->grid->addColumn($col);

        $col = new Column\Select('documentQName');
        $col->setLabel('Identifier');
        $col->setWidth(10);
        $col->setType($this->getUrlColumn());
        $this->grid->addColumn($col);

        if ($this->fields !== null) {
            foreach ($this->fields as $field) {
                if ($field === 'accessionID') {
                    $col = new Column\Select('accessionID');
                    $type = new URI();
                    $type->setAddView(true);
                    $type->setAddEdit(false);
                    $col->setLabel('AccessionID');
                    $col->setWidth(10);
                    $col->setType($type);
                    $this->grid->addColumn($col);
                    continue;
                }
                $type = $this->mappings->getPropertyType($field);
                if ($type === null) {
                    continue;
                }
                $label = ucfirst($field);
                $col = new Column\Select($field);
                $col->setLabel($label);

                $col->setWidth(20);
                $this->grid->addColumn($col);
                if ($type == 'geo_point') {
                    if ($this->centerLat === null || $this->centerLon === null) {
                        $col->setUserSortDisabled(true);
                        $col->setSortActive(null);
                    }
                    $col->setType(new GeoPointType($this->centerLat, $this->centerLon));
                } else {
                    $col->setType(new GenericType());
                }
            }
            return;
        }
    }

    public function setUrlColumn(Column\Type\AbstractType $col) {
        $this->urlCol = $col;
    }

    protected function getUrlColumn()
    {
        if ($this->urlCol === null) {
            $this->urlCol = new URI();
            $this->urlCol->setAddView(true);
        }
        return $this->urlCol;
    }
}