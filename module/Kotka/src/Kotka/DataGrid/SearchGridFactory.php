<?php
namespace Kotka\DataGrid;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class SearchGridFactory implements FactoryInterface
{
    /**
     * @see \Zend\ServiceManager\FactoryInterface::createService()
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $class = new SearchGrid();
        $class->setServiceLocator($serviceLocator);

        return $class;
    }
}