<?php

namespace Kotka\Listener;


use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class UserActionListenerFactory implements FactoryInterface
{
    /**
     * {@inheritDoc}
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $auth = $serviceLocator->get('auth');
        $user = $serviceLocator->get('user');
        $cache = $serviceLocator->get('cache');
        $permaCache = $serviceLocator->get('perma-cache');
        $logger = $serviceLocator->get('Logger');
        return new UserActionListener($logger, $user, $auth, $cache, $permaCache, $serviceLocator);
    }

} 