<?php

namespace Kotka\Listener;


use Common\Service\IdService;
use ImageApi\Model\PictureMeta;
use Kotka\Job\Elastic;
use Kotka\Job\Mustikka;
use Kotka\Service\CacheKeyService;
use Kotka\Service\SpecimenService;
use Kotka\Triple\MAPerson;
use Kotka\Triple\MYDocument;
use SlmQueue\Queue\QueueInterface;
use User\Enum\User;
use Zend\Authentication\AuthenticationService;
use Zend\Cache\Storage\StorageInterface;
use Zend\Console\Console;
use Zend\EventManager\Event;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\Mvc\Router\Http\RouteMatch;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

/***
 * Class UserActionListener this listens to user actions and logs and invalidates the needed caches
 *
 * @package Kotka\Listener
 */
class UserActionListener implements ListenerAggregateInterface, ServiceLocatorAwareInterface
{

    use ServiceLocatorAwareTrait;

    const EDIT_LOG = "User '%s' edited %s (%s) from %s";
    const ADD_LOG = "User '%s' added %s (%s) from %s";
    const DELETE_LOG_LINE = "User '%s' deleted %s (%s) from %s";
    const USER_LOGIN_LOG = "User '%s' just logged in";
    const ACCESS_DENIED = "User '%s' with role '%s' is denied access to route '%s'";

    /**
     * @var \Zend\Stdlib\CallbackHandler[]
     */
    protected $listeners = array();

    /** @var  \Zend\Log\LoggerInterface */
    protected $logger;

    /** @var  MAPerson */
    protected $user;

    /** @var  AuthenticationService */
    protected $auth;

    /** @var  StorageInterface */
    protected $permaCache;

    /** @var  StorageInterface */
    protected $cache;

    /** @var  QueueInterface */
    protected $defaultQueue;

    /**
     * Attach one or more listeners
     *
     * Implementors may add an optional $priority argument; the EventManager
     * implementation will pass this to the aggregate.
     *
     * @param EventManagerInterface $events
     *
     * @return void
     */
    public function attach(EventManagerInterface $events)
    {
        $sharedEventManager = $events->getSharedManager();
        $this->listeners[] = $sharedEventManager->attach('login', 'success', array($this, 'onSuccessfulLogin'));
        $this->listeners[] = $sharedEventManager->attach('user', 'deny', array($this, 'onAccessDenied'));
        $this->listeners[] = $sharedEventManager->attach('add', '*', array($this, 'onAdd'));
        $this->listeners[] = $sharedEventManager->attach('edit', '*', array($this, 'onEdit'));
        $this->listeners[] = $sharedEventManager->attach('delete', '*', array($this, 'onDelete'));
        $this->listeners[] = $sharedEventManager->attach('image', '*', array($this, 'onImageEvent'));
    }

    /**
     * Detach all previously attached listeners
     *
     * @param EventManagerInterface $events
     *
     * @return void
     */
    public function detach(EventManagerInterface $events)
    {
        foreach ($this->listeners as $index => $listener) {
            if ($events->detach($listener)) {
                unset($this->listeners[$index]);
            }
        }
    }

    /**
     * Action that is trickered when item is edited
     * @param Event $event
     */
    public function onEdit(Event $event)
    {
        $this->onChange($event, self::EDIT_LOG);
    }

    /**
     * Action that is trickered when item is added
     * @param Event $event
     */
    public function onAdd(Event $event)
    {
        $this->onChange($event, self::ADD_LOG);
    }

    public function onDelete(Event $event)
    {
        $this->onChange($event, self::DELETE_LOG_LINE);
    }

    private function onChange(Event $event, $line)
    {
        $className = substr(strrchr($event->getName(), '\\'), 1);
        $logLine = sprintf($line,
            $event->getParam('user'),
            $event->getParam('subject'),
            $className,
            $this->getClientIpAddress()
        );
        $this->getLogger()->notice($logLine);
        $this->inValidateCache($className);
    }

    public function onAccessDenied(Event $event)
    {
        $name = $this->getUser()->getSubject() === null ? 'guest' : $this->getUser()->getSubject();
        /** @var RouteMatch $match */
        $match = $event->getParam('match');
        $logLine = sprintf(self::ACCESS_DENIED,
            $name,
            $this->getUser()->getMARoleKotka(),
            $match->getMatchedRouteName()
        );
        $this->getLogger()->notice($logLine);
    }

    /**
     * Events that takes place when user logs in
     */
    public function onSuccessfulLogin()
    {
        $user = $user = $this->getAuth()->getIdentity();
        if ($user instanceof MAPerson) {
            $logLine = sprintf(self::USER_LOGIN_LOG, $user->getSubject());
            $this->getLogger()->notice($logLine);
        } else {
            $this->getLogger()->notice('User logged in without identity!');
        }
    }

    public function onImageEvent(Event $event)
    {
        $meta = null;
        $documents = null;
        switch ($event->getName()) {
            case 'upload':
            case 'update':
                $meta = $event->getParam('meta');
                if (!$meta instanceof PictureMeta) {
                    $event->stopPropagation(true);
                    return 'No metadata send with the request!';
                }
                $documents = $meta->getDocumentIds();
                break;
            case 'remove':
                if (!$this->isUserAdmin() && !$this->isUserAdvanced()) {
                    $event->stopPropagation(true);
                    return 'You need to be an admin or advanced user to delete pictures';
                }
                return null;
            case 'show':
                //$document = $event->getParam('documentId');
                return null;
        }

        // see if document is public
        $secret = false;
        if (!empty($documents)) {
            /** @var \Kotka\Service\SpecimenService $specimenService */
            $specimenService = $this->serviceLocator->get('Kotka\Service\SpecimenService');
            $documents = $specimenService->getById($documents);
            $updatable = [];
            if (is_array($documents)) {
                foreach ($documents as $document) {
                    if ($document instanceof MYDocument) {
                        array_push($updatable, $document->getSubject());
                        /*
                        if (!$this->canEdit($document->getMZOwner(), $this->getUser()->getMAOrganisation())) {
                            $event->stopPropagation(true);
                            return ['code' => 403, 'status' => "You do not have rights the add pictures to " . $qname];
                        }
                        */
                        if (!SpecimenService::isPublic($document)) {
                            $secret = true;
                            break;
                        }
                    }
                }
            }
            if (count($updatable) > 0) {
                $queue = $this->getDefaultQueue();

                $job = new Mustikka();
                $job->setSubjects($updatable);
                $queue->push($job);

                $job = new Elastic();
                $job->setContent(array('ids' => IdService::getQName($updatable)));
                $queue->push($job);
            }
        }
        $meta->setSecret($secret);

        // Rest of these are only for user http
        if (Console::isConsole()) {
            return null;
        }

        // add logged in user qname to metadata
        if ($meta->getUploadedBy() === null) {
            $meta->setUploadedBy($this->getUser()->getSubject());
        }

        // Make sure that the user is logged in to use image api
        if (!$this->isUserMember() && !$meta->isAllowLoggedOut()) {
            $event->stopPropagation(true);
            return 'You have to be logged in to use image api';
        }
        return null;
    }

    private function isUserMember()
    {
        $user = $this->getAuth()->getIdentity();
        if ($user instanceof MAPerson) {
            if ($user->getMARoleKotka() === User::ROLE_GUEST || $user->getMARoleKotka() === null) {
                return false;
            }
        } else {
            return false;
        }
        return true;
    }

    private function isUserAdmin()
    {
        $user = $this->getAuth()->getIdentity();
        if ($user instanceof MAPerson) {
            if ($user->getMARoleKotka() === User::ROLE_ADMIN) {
                return true;
            }
        }
        return false;
    }

    private function isUserAdvanced()
    {
        $user = $this->getAuth()->getIdentity();
        if ($user instanceof MAPerson) {
            if ($user->getMARoleKotka() === User::ROLE_ADVANCED) {
                return true;
            }
        }
        return false;
    }

    private function canEdit($documentOrganization, $userOrganization)
    {
        if ($userOrganization === null || count($userOrganization) == 0) {
            return false;
        }
        return empty($documentOrganization) || array_key_exists($documentOrganization, $userOrganization);
    }

    /**
     * Invalidates caches
     * @param $className
     */
    private function inValidateCache($className)
    {
        $cache = $this->serviceLocator->get('cache');
        $permaCache = $this->serviceLocator->get('perma-cache');
        $cacheKeyHandler = new CacheKeyService();
        $method = CacheKeyService::REFRESH_PREFIX . $className;
        if (!method_exists($cacheKeyHandler, $method)) {
            return;
        }
        $removeKeys = $cacheKeyHandler->$method();
        if (isset($removeKeys[CacheKeyService::FILE_CACHE])) {
            $cache->flush();
            unset($removeKeys[CacheKeyService::FILE_CACHE]);
        }
        $permaCache->removeItems($removeKeys);
        $this->getLogger()->debug("Deleting cache keys '" . implode(', ', $removeKeys) . "'");
    }

    /**
     * Returns information about remote address
     * @return string
     */
    private function getClientIpAddress()
    {
        if (isset($_SERVER['REMOTE_ADDR'])) {
            return $_SERVER['REMOTE_ADDR'];
        }
        return 'command line';
    }

    private function getAuth()
    {
        if ($this->auth === null) {
            $this->auth = $this->getServiceLocator()->get('auth');
        }
        return $this->auth;
    }

    private function getUser()
    {
        if ($this->user === null) {
            $this->user = $this->getServiceLocator()->get('user');
        }
        return $this->user;
    }

    private function getLogger()
    {
        if ($this->logger === null) {
            $this->logger = $this->getServiceLocator()->get('Logger');
        }
        return $this->logger;
    }
    /**
     * @return QueueInterface
     */
    private function getDefaultQueue()
    {
        if ($this->defaultQueue === null) {
            $this->defaultQueue = $this->getServiceLocator()->get('SlmQueue\Queue\QueuePluginManager')->get('default');
        }

        return $this->defaultQueue;
    }


}