<?php

namespace Kotka\View\Helper;

use DateInterval;
use DateTime;
use Zend\View\Helper\AbstractHelper;

/**
 * Class Time
 * @package Kotka\View\Helper
 */
class Time extends AbstractHelper
{

    /**
     * This return the given datetime object in the wanted format.
     * If the object is null then this is returned.
     *
     * toString method in this class ensures that the output given
     * can be echoed in the view.
     *
     * @param DateTime|string $datetime
     * @param string $format
     * @return $this|string
     */
    public function __invoke($datetime = null, $format = 'd.m.Y H:i.s')
    {
        if ($datetime === null) {
            return $this;
        }
        if ($datetime instanceof DateTime) {
            return $datetime->format($format);
        } else if (is_string($datetime)) {
            $datetime = new DateTime($datetime);
            return $datetime->format($format);
        }
        return '';
    }


    /**
     * This compares two date times and returns the difference in the format wanted.
     * If one (either $form or $to) is empty datetime now is used in it's place.
     *
     * For interval format see:
     *    http://www.php.net/manual/en/dateinterval.format.php
     *
     * This return an empty string on failure. This is because in that way we cen
     * make sure that the view can handle anything that this function returns.
     *
     * @param  DateTime $from
     * @param  DateTime $to
     * @param  string $format
     * @param  string $sub
     * @return string
     */
    public function diff(DateTime $from = null, DateTime $to = null, $format = null, $sub = null)
    {
        if ($from === null && $to === null) {
            return '';
        }
        if ($from === null) {
            $from = new DateTime();
        }
        if ($to === null) {
            $to = new DateTime();
        }
        if ($format === null) {
            $format = '%r%a';
        }
        if ($sub !== null) {
            $to->sub(new DateInterval($sub));
        }

        $interval = $to->diff($from);

        if ($interval === false) {
            return '';
        }

        return $interval->format($format);
    }

    public function __toString()
    {
        return '';
    }

}
