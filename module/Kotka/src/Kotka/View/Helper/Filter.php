<?php

namespace Kotka\View\Helper;


use Zend\Filter\FilterInterface;
use Zend\View\Helper\AbstractHelper;


/**
 * Class Filter
 *
 * Filters the value given value. Filter can be an filter interface array or an view helper
 *
 * @package Kotka\View\Helper
 */
class Filter extends AbstractHelper
{

    const NO_FILTER_VALUE = '__no_filter_value__';

    private $filters = array();

    private $clearValue = null;

    /**
     * When parameters are given this will try to convert the value
     * If this is called without any parameters then this will return self
     *
     * @param string $field
     * @param string $value
     * @param null|string $noMatch to return this value if no match
     *
     * @return $this|string|null
     */
    public function __invoke($field = null, $value = null, $noMatch = '')
    {
        if ($field === null) {
            return $this;
        }
        if (!isset($this->filters[$field]) || $value === null || $value === '') {
            return $value;
        }
        $filter = $this->filters[$field];
        $filteredValue = $this->getFilteredValue($filter, $value);
        if ($filteredValue === self::NO_FILTER_VALUE && $this->clearValue !== null) {
            $pos = strpos($value, $this->clearValue);
            if ($pos !== false) {
                $clear = $this->clearValue;
                $clearedValue = str_replace($clear, '', $value);
                $filteredValue = $this->getFilteredValue($filter, $clearedValue);
                if ($filteredValue === self::NO_FILTER_VALUE) {
                    return $noMatch;
                }
                $filteredValue = $pos === 0 ? $clear . $filteredValue : $filteredValue . $clear;
            }
        }
        if ($filteredValue === self::NO_FILTER_VALUE) {
            return $noMatch;
        }

        return $filteredValue;
    }

    private function getFilteredValue($filter, $value)
    {
        if ($filter instanceof FilterInterface) {
            return $filter->filter($value);
        } else if (is_string($filter)) {
            return $this->getView()->$filter($value);
        } else if (is_array($filter)) {
            if (is_array($value)) {
                $result = [];
                foreach ($value as $idx => $val) {
                    $result[$idx] = $this->arrayFilter($filter, $val, $val);
                }
                return $result;
            } else {
                return $this->arrayFilter($filter, $value);
            }
        } else {
            error_log('Invalid field filter given. Returning unfiltered data');
        }
        return self::NO_FILTER_VALUE;
    }

    private function arrayFilter($filter, $value, $noMatch = self::NO_FILTER_VALUE)
    {
        if (isset($filter[$value])) {
            return $filter[$value];
        }
        // We'll end up here if the filter is a select with multiple levels
        foreach ($filter as $content) {
            if (is_array($content)) {
                if (isset($content[$value])) {
                    return $content[$value];
                } else if (isset($content['options'][$value])) {
                    return $content['options'][$value];
                }
            }
        }
        return $noMatch;
    }

    /**
     * Adds a new filter to a field
     *
     * If filter
     *  is string then the corresponding view helper is called
     *  is array then the value is used as key and array value is returned
     *  implements FilterInterface then filter method is called with the value and the result is returned
     *
     * @param string $field
     * @param FilterInterface|string|array $filter
     *
     * @return $this
     */
    public function addFilter($field, $filter)
    {
        $this->filters[$field] = $filter;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getClearValue()
    {
        return $this->clearValue;
    }

    /**
     * @param mixed $clearValue
     */
    public function setClearValue($clearValue)
    {
        $this->clearValue = $clearValue;
    }

    public function hasFilter($field) {
        return isset($this->filters[$field]);
    }


}