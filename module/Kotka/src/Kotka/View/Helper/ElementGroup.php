<?php

namespace Kotka\View\Helper;

use Zend\Form\Element;
use Zend\View\Helper\AbstractHelper;

/**
 * Class ElementGroup
 *
 * @package Kotka\View\Helper
 */
class ElementGroup extends AbstractHelper
{

    private $templates = array();
    private $total = array();
    private $counter = array();
    private $elements = array();

    public function __invoke(Element $element = null)
    {
        if ($element == null) {
            return $this;
        }
        $name = $element->getName();
        $pos = strrchr($name, '[');
        if ($pos !== false) {
            $name = str_replace(array('[', ']'), '', $pos);
        }
        if (!isset($this->elements[$name])) {
            return false;
        }
        $template = $this->elements[$name];
        $this->templates[$template][$name] = $element;
        $this->counter[$template]++;
        if ($this->counter[$template] == $this->total[$template]) {
            $values = $this->templates[$template];
            $spot = strpos($template, ':');
            if ($spot !== false) {
                $template = substr($template, $spot + 1);
            }
            return $this->getView()->partial($template, $values);
        }
        return '';
    }

    public function addElement($elementName, $template)
    {
        $this->checkTempalate($template);
        $this->templates[$template][$elementName] = true;
        $this->elements[$elementName] = $template;
        $this->total[$template]++;

        return $this;
    }

    public function setValue($variable, $value, $template)
    {
        $this->checkTempalate($template);
        $this->templates[$template][$variable] = $value;

        return $this;
    }

    public function reset()
    {
        foreach ($this->counter as $key => $value) {
            $this->counter[$key] = 0;
        }

        return $this;
    }

    private function checkTempalate($template)
    {
        if (!isset($this->templates[$template])) {
            $this->templates[$template] = array();
            $this->total[$template] = 0;
            $this->counter[$template] = 0;
        }
    }
}
