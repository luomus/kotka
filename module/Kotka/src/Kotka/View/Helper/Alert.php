<?php

namespace Kotka\View\Helper;

use Zend\View\Helper\AbstractHelper;

/**
 * Class Alert is used to renders alert boxes in view
 * The actual html is in the partial element alert.phtml
 * @package Kotka\View\Helper
 */
class Alert extends AbstractHelper
{
    const TYPE_SUCCESS = 'success';
    const TYPE_INFO = 'info';
    const TYPE_WARNING = 'warning';
    const TYPE_DANGER = 'danger';

    protected $types = array(
        self::TYPE_DANGER,
        self::TYPE_INFO,
        self::TYPE_SUCCESS,
        self::TYPE_WARNING
    );

    public function __invoke($msg, $type = self::TYPE_DANGER, $closeButton = true)
    {
        if (!in_array($type, $this->types)) {
            throw new \Exception("Invalid type given '$type'.");
        }
        $vars = array(
            'message' => $msg,
            'type' => $type,
            'closeButton' => $closeButton,
        );

        return $this->view->render('kotka/partial/element/alert', $vars);
    }

}
