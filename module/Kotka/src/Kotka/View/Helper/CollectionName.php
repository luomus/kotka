<?php

namespace Kotka\View\Helper;

use Kotka\Service\CollectionService;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\View\Helper\AbstractHelper;

/**
 * Class CollectionName view helper for finding collection name based on the uri
 * @package Kotka\View\Helper
 */
class CollectionName extends AbstractHelper implements ServiceLocatorAwareInterface
{
    protected $serviceLocator;

    private static $cache = [
        'name' => [],
        'abbr' => []
    ];

    /**
     * Finds the collection name
     *
     * @param $id
     * @param string $type
     * @return mixed
     * @throws \Exception
     */
    public function __invoke($id, $type = 'name')
    {
        if (!isset(self::$cache[$id])) {
            self::$cache[$id] = $this->getCollectionName($id);
        }
        if (!array_key_exists($type, self::$cache[$id])) {
            throw new \Exception("Invalid type $type given.");
        }
        return self::$cache[$id][$type];
    }

    private function getCollectionName($id)
    {
        $return = ['name' => '', 'abbr' => ''];
        /** @var CollectionService $cs */
        $cs = $this->serviceLocator->getServiceLocator()->get('Kotka\Service\CollectionService');
        /** @var \Kotka\Triple\MYCollection $collection */
        $collection = $cs->getById($id);
        if ($collection === null) {
            return $return;
        }
        $return['name'] = $collection->getMYCollectionName('en');
        $return['abbr'] = $collection->getMYAbbreviation();

        return $return;
    }

    /**
     * Sets service locator
     * @param ServiceLocatorInterface $sl
     */
    public function setServiceLocator(ServiceLocatorInterface $sl)
    {
        $this->serviceLocator = $sl;
    }

    /**
     * Gets service locator
     * @return ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

}
