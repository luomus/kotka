<?php
namespace Kotka\View\Helper\Label;

use Kotka\Service\CoordinateService;
use Zend\Form\Element;
use Zend\View\Helper\AbstractHelper;


/**
 * Class Coordinate is a view helper that creates the coordinate lines for labels
 *
 * @package Kotka\View\Helper\Label
 */
class Coordinate extends AbstractHelper
{

    /** @var string */
    protected $type = 'long'; // default type used

    /**
     * Used to form the coordinate line for the labels
     *
     * Keys withing the array tells for witch system the line is.
     * If there is no line for the system searched for then the default key is used
     * Currently the systems are ykj, wgs84, wgs84dms, kkj, etrs-tm35fin, dd, dms
     *
     * Note: default key should be present en every type!
     *
     * Used variables:
     *  %system% => $system value
     *  %lat% => $lat value
     *  %lon% => $lon value
     *  %wgsN% => $wgsN value
     *  %wgsE% => $wgsE value
     *  %radius% => $radius value
     *  %accuracy% => calculated value based on inaccuracy in coordinates and radius
     *
     * Every value can be augmented using augment array
     *
     * @var array
     */
    protected $coordinates = [
        'short' => [
            'ykj' => 'ykj: %lat%:%lon%',
            'wgs84' => '%lat%, %lon%',
            'default' => '%system%: %lat%, %lon%',
        ],
        'long' => [
            'ykj' => 'YKJ: %lat%:%lon% (Grid 27° E)<br />
                          WGS84: %wgsN%, %wgsE%%accuracy%"',
            'default' => 'Coordinates: %lat%, %lon%%accuracy% (%system%)',
            'augment' => ['accuracy' => ', accuracy ± %accuracy% m'],
        ],
        'new' => [
            'ykj' => '%lat%:%lon% (YKJ)<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                          %wgsN%, %wgsE% (wgs84%accuracy%)',
            'default' => 'Coordinates: %lat%, %lon%%accuracy% (%system%)',
            'augment' => ['accuracy' => ', accuracy %accuracy% m'],
        ],
        'turku' => [
            'ykj' => 'Finnish uniform grid (YKJ): %lat%:%lon%',
            'default' => 'Coordinates (%system%): %lat%, %lon%%accuracy% ',
            'augment' => ['accuracy' => ', accuracy ± %accuracy% m'],
        ]
    ];

    /**
     * List of variables use in the messages
     * @var array
     */
    protected $variables = [
        'system', 'lat', 'lon', 'wgsN', 'wgsE', 'radius', 'accuracy'
    ];


    /**
     * Converts the given parameters into coordinate line or returns this is system is null
     *
     * @param $system
     * @param $lat
     * @param $lon
     * @param $wgsN
     * @param $wgsE
     * @param $radius
     * @param null $type
     * @return $this|string
     */
    public function __invoke($system = null, $lat = null, $lon = null, $wgsN = null, $wgsE = null, $radius = null, $type = null)
    {
        // If the system is null you'll get this class
        if ($system === null) {
            return $this;
        }

        // Sets the type if it's given
        if ($type !== null) {
            $this->setType($type);
        }

        // Convert the system into internal form
        $system = strtolower(str_replace('MY.coordinateSystem', '', $system));

        // Make the necessary changes to the coordinates
        $accuracy = '';
        $this->prepareCoordinates($system, $lat, $lon, $wgsN, $wgsE, $radius, $accuracy);

        // Augment the coordinate variables
        $coordinateSet = $this->getCoordinateSet();
        if (isset($coordinateSet['augment'])) {
            foreach ($coordinateSet['augment'] as $field => $aug) {
                if (!empty($$field)) {
                    $$field = str_replace('%' . $field . '%', $$field, $aug);
                }
            }
        }

        // Form the line and return it
        $line = isset($coordinateSet[$system]) ? $coordinateSet[$system] : $coordinateSet['default'];
        $search = [];
        $replace = [];
        foreach ($this->variables as $var) {
            $search[] = '%' . $var . '%';
            $replace[] = $$var;
        }
        return str_replace($search, $replace, $line);
    }

    /**
     * Makes the changes needed to the coordinates
     *
     * @param $system
     * @param $lat
     * @param $lon
     * @param $wgsN
     * @param $wgsE
     * @param $radius
     * @param $accuracy
     */
    protected function prepareCoordinates($system, &$lat, &$lon, &$wgsN, &$wgsE, &$radius, &$accuracy)
    {
        switch ($system) {
            case 'wgs84':
            case 'wgs84dms':
                CoordinateService::addSuffixToWgs($lat, $lon);
                break;
            case 'ykj':
                $wgs = CoordinateService::ykj2wgs84($lat, $lon, $radius, 'dms', $wgsN, $wgsE);
                if (isset($wgs['N'])) {
                    $wgsN = $wgs['N'];
                }
                if (isset($wgs['E'])) {
                    $wgsE = $wgs['E'];
                }
                if (isset($wgs['accuracy'])) {
                    $accuracy = $wgs['accuracy'];
                }
                break;
        }
    }

    /**
     * Returns the type of string lists used
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Sets the string list type
     *
     * @param $type
     * @throws \Exception
     */
    public function setType($type)
    {
        if (!isset($this->coordinates[$type])) {
            throw new \Exception('Invalid coordinate type given to Coordinate view helper when setting type');
        }
        $this->type = $type;
    }

    /**
     * Returns the array of different kind of strings used for the coordinates
     *
     * @return array
     */
    public function getCoordinateSet()
    {
        return $this->coordinates[$this->type];
    }

    /**
     * Sets the given coordinate type string set to the given array
     *
     * Note: this will change default type to the type given here!
     * (so you don't need to call setType after this)
     *
     * @param $type
     * @param array $message
     * @throws \Exception
     */
    public function setCoordinateSet($type, array $message)
    {
        if (!isset($message['default'])) {
            throw New \Exception("You'r missing default key in the message set!");
        }
        $this->coordinates[$type] = $message;
        $this->setType($type);
    }

    /**
     * This is just to make sure that casting to string works.
     * User might end up here if coordinate system is null.
     * @return string
     */
    public function __toString()
    {
        return '';
    }
} 