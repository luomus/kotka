<?php
namespace Kotka\View\Helper\Label;


class Field
{
    const PROVINCE = 'province';
    const MUNICIPALITY = 'municipality';
    const COUNTRY = 'country';
    const COORDINATE = 'coordinate';
    const TAXON = 'taxon';
    const LEG = 'leg';
    const LOCALITY = 'locality';
    const LOCALITY_DESCRIPTION = 'localityDescription';
    const DET_YEAR = 'det-year';

    private $allFields = [
        self::MUNICIPALITY => true,
        self::LOCALITY => true,
        self::LOCALITY_DESCRIPTION => true,
        self::PROVINCE => true,
        self::COUNTRY => true,
        self::COORDINATE => true,
        self::TAXON => true,
        self::LEG => true,
        self::DET_YEAR => true,
    ];

    private $fields = [];

    public function __invoke($field = null)
    {
        if ($field === null) {
            return $this;
        }
        if (!isset($this->allFields[$field])) {
            throw new \Exception('Could not find field "'
                . $field
                . '". Should be one of '
                . implode(', ', array_keys($this->allFields)) . '.');
        }
        return isset($this->fields[$field]);
    }

    public function getAllFields()
    {
        return array_keys($this->allFields);
    }

    /**
     * @return array
     */
    public function getFields()
    {
        return array_keys($this->fields);
    }

    /**
     * @param $fields
     * @throws \Exception
     */
    public function setFields($fields)
    {
        if (is_string($fields)) {
            $fields = [$fields];
        }
        if (!is_array($fields)) {
            return;
        }
        $this->fields = [];
        foreach ($fields as $field) {
            if (empty($field)) {
                continue;
            }
            if (isset($this->allFields[$field])) {
                $this->fields[$field] = true;
            } else {
                throw new \Exception('Invalid field given in the set of fields! Now "'
                    . $field
                    . '" given when expecting one of '
                    . implode(', ', array_keys($this->allFields)) . '.');
            }
        }
    }

}