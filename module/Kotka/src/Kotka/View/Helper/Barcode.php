<?php
namespace Kotka\View\Helper;

use Zend\Barcode\Barcode as ZendBarcode;
use Zend\View\Helper\AbstractHelper;

class Barcode extends AbstractHelper
{


    public function __invoke($text, $type = 'code39')
    {
        $barcodeOptions = [
            'text' => $text,
            'drawText' => false,
            'withQuietZones' => false
        ];
        $redererOptions = [];
        $barcodeResource = ZendBarcode::draw(
            $type, 'image', $barcodeOptions, $redererOptions
        );
        ob_start();
        imagejpeg($barcodeResource);
        $contents = ob_get_contents();
        ob_end_clean();
        $img = '<image width="100%" height="100%" src="data:image/png;base64,' . base64_encode($contents) . '"/>';
        return $img;
    }
} 