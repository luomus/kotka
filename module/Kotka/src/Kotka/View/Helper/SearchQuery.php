<?php

namespace Kotka\View\Helper;


use Zend\View\Helper\AbstractHelper;

class SearchQuery extends AbstractHelper
{

    public function __invoke($query)
    {
        $q = [];
        if (is_array($query)) {
            if (count($query) == 1) {
                $q['q'] = key($query) . ': "' . current($query) . '"';
            } else {
                $q['s'] = [];
                foreach ($query as $key => $value) {
                    $q['s'][] = $key . ': "' . $value . '"';
                }
            }
        } else {
            $q = ['q' => $query];
        }
        return $this->getView()->url('https/search_specimens', [], ['query' => $q]);
    }

} 