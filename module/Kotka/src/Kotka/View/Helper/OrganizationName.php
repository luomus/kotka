<?php

namespace Kotka\View\Helper;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\View\Helper\AbstractHelper;

/**
 * Class OrganizationName this is used in the view to convert the id of the organization
 * to the actual name of the organization.
 *
 * In order to achieve this it uses the OrganizationService to fetch the data about the
 * organization.
 *
 * @see \Kotka\Service\OrganizationService
 *
 * @package Kotka\View\Helper
 */
class OrganizationName extends AbstractHelper implements ServiceLocatorAwareInterface
{
    protected $serviceLocator;

    const FORMAT_LONG = 'long';
    const FORMAT_LONG_WITHOUT_ABBR = 'long-no-abbr';
    const FORMAT_SHORT = 'short';

    /**
     * This returns the organization name.
     * Example
     *  University of Helsinki, Finnish Museum of Natural History, General Services Unit, IT Team
     *
     * If the short parameter is true then the form fill be either abbreviation or
     * the level1 name of the organization if organization has no abbreviation
     *
     * Short version is used in organization column in transaction listings
     * @see http://kotka.luomus.fi/transactions
     *
     *
     * @param $id
     * @param  string $format
     * @return string
     */
    public function __invoke($id, $format = self::FORMAT_LONG)
    {
        /** @var \Kotka\Triple\MOSOrganization $organization */
        /** @var \Kotka\Service\OrganizationService $os */
        if ($id === null || $id == '') {
            return '';
        }
        /** @var \Triplestore\Service\ObjectManager $om */
        $om = $this->serviceLocator->getServiceLocator()->get('Triplestore\ObjectManager');
        $om->enableHydrator();
        $os = $this->serviceLocator->getServiceLocator()->get('Kotka\Service\OrganizationService');
        $organization = $os->getById($id);

        if ($organization === null) {
            return '';
        }

        if ($format === true) {
            $format = self::FORMAT_SHORT;
        }

        switch ($format) {
            case (self::FORMAT_SHORT):
                if ($organization->getMOSAbbreviation() !== null && $organization->getMOSAbbreviation() != '') {
                    return $organization->getMOSAbbreviation();
                }
                return $organization->getMOSOrganizationLevel1('en');
            case (self::FORMAT_LONG_WITHOUT_ABBR):
                return $os->getOrganizationName($organization, false);
            default:
                return $os->getOrganizationName($organization);
        }


    }

    /**
     * Sets the service locator
     * @param ServiceLocatorInterface $sl
     */
    public function setServiceLocator(ServiceLocatorInterface $sl)
    {
        $this->serviceLocator = $sl;
    }

    /**
     * Gets the service locator
     * @return ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

}
