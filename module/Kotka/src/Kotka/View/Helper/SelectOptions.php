<?php

namespace Kotka\View\Helper;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Zend\View\Helper\AbstractHelper;

/**
 * Class SelectOptions
 *
 *
 *
 * @package Kotka\View\Helper
 */
class SelectOptions extends AbstractHelper implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    /** @var \Kotka\Service\FormElementService */
    private $fes;

    /**
     * Returns options for the field
     *
     * If there isn't any options empty array is returned
     *
     * @param $field
     * @return array
     */
    public function __invoke($field)
    {
        return $this->getFormElementService()->getSelect($field, array($field => $field), false);
    }

    /**
     * Returns the form element service
     *
     * @return \Kotka\Service\FormElementService
     */
    public function getFormElementService()
    {
        if ($this->fes === null) {
            $this->fes = $this->getServiceLocator()->getServiceLocator()->get('Kotka\Service\FormElementService');
        }
        return $this->fes;
    }
}
