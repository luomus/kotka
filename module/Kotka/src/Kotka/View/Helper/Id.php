<?php
namespace Kotka\View\Helper;

use Zend\Form\Element;
use Zend\View\Helper\AbstractHelper;

class Id extends AbstractHelper
{
    public function __invoke($value)
    {
        if ($value instanceof Element) {
            $value = $value->getName();
        }
        return str_replace(array('[', ']', '.', '/', ':', ' ', '(', ')'), '_', $value);
    }
} 