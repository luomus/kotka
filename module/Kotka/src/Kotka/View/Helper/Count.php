<?php

namespace Kotka\View\Helper;

use Zend\View\Helper\AbstractHelper;

/**
 * Class Count counts the elements in collection
 * @package Kotka\View\Helper
 */
class Count extends AbstractHelper
{

    /**
     * This is runned when invoked in view like this
     * $this->Count($obj, 'Count')
     *
     * @param $obj
     * @param $method
     * @param  int $min number to return if none found
     * @return int
     */
    public function __invoke($obj, $method = null, $min = 1)
    {
        if ($obj === null) {
            return $min;
        }
        if (is_array($obj)) {
            return count($obj);
        }
        if ($obj instanceof \Countable) {
            return $obj->count();
        }
        if (null === $method) {
            return $min;
        }
        if (!method_exists($obj, $method)) {
            return $min;
        }
        $countable = $obj->$method();
        if (is_array($countable)) {
            return count($countable);
        }
        if (is_object($countable)) {
            if (method_exists($countable, 'count')) {
                return $countable->count();
            }
        }

        return $min;
    }

}
