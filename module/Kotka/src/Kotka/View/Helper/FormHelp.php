<?php

namespace Kotka\View\Helper;

use Kotka\Service\CollectionService;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\View\Helper\AbstractHelper;

/**
 * @package Kotka\View\Helper
 */
class FormHelp extends AbstractHelper implements ServiceLocatorAwareInterface
{
    private static $metadataService;
    protected $serviceLocator;

    private static $cache = [];

    public function __invoke($field, $wrap = true)
    {
        if (!isset(self::$cache[$field])) {
            self::$cache[$field] = $this->getInfoText($field);
        }
        if (empty(self::$cache[$field])) {
            return '';
        }
        if ($wrap) {
            $attr = [
                'class="generic-help fa fa-question-circle hidden-xs"',
                'title="' . self::$cache[$field] . '"',
                'data-toggle="tooltip"',
                'data-placement="right"',
                'data-container="body"'
            ];
            return sprintf('<i %s></i>', implode(' ', $attr));
        }
        return self::$cache[$field];
    }

    private function getInfoText($property)
    {
        return $this->getMetadataService()->getAllProperties()->getHelp($property);
    }

    /**
     * @return \Triplestore\Service\MetadataService
     */
    private function getMetadataService() {
        if (!FormHelp::$metadataService) {
            FormHelp::$metadataService = $this->getServiceLocator()->getServiceLocator()->get('Triplestore\ObjectManager')->getMetadataService();
        }
        return FormHelp::$metadataService;
    }

    /**
     * Sets service locator
     * @param ServiceLocatorInterface $sl
     */
    public function setServiceLocator(ServiceLocatorInterface $sl)
    {
        $this->serviceLocator = $sl;
    }

    /**
     * Gets service locator
     * @return ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

}
