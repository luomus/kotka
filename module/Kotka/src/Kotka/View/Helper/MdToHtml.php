<?php
namespace Kotka\View\Helper;


use Kotka\Service\MarkdownService;
use Zend\View\Helper\AbstractHelper;

class MdToHtml extends AbstractHelper
{
    private static $service;

    public function __invoke($value)
    {
        $mdService = $this->getMDService();
        $html = $mdService->mdToHtml($value);
        $html = str_replace('<a href="', '<a target="_blank" href="', $html);

        return $html;
    }

    /**
     * @return MarkdownService
     */
    private function getMDService() {
        if (!MdToHtml::$service) {
            MdToHtml::$service = new MarkdownService();
        }
        return MdToHtml::$service;
    }
}