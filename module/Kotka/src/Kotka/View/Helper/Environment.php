<?php

namespace Kotka\View\Helper;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Zend\View\Helper\AbstractHelper;

/**
 * Class Environment
 * @package Kotka\View\Helper
 */
class Environment extends AbstractHelper implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    private static $env;

    /**
     * This is run when the class is invoked
     * and returns the environment value defined in the configs
     * @return string
     */
    public function __invoke()
    {
        if (self::$env === null) {
            $this->initEnvironment();
        }
        return self::$env;
    }

    private function initEnvironment()
    {
        $config = $this->getServiceLocator()->getServiceLocator()->get('config');
        if (isset($config['environment'])) {
            self::$env = $config['environment'];
            return;
        }
        self::$env = '';
    }
}
