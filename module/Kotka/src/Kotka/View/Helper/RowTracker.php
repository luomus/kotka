<?php
namespace Kotka\View\Helper;

use Zend\View\Helper\AbstractHelper;

class RowTracker extends AbstractHelper
{

    const RESOLUTION = 10;

    private $maxRowsCollection = 100;
    private $currentCollection = 0;
    private $emptyLineHeight = 0;
    private $wideWidth = 1.5;
    private $thinWidth = 0.3;
    private $charPerRow = 100;
    private $currentItem = 0;
    private $minRowsItem = 0;
    private $itemSpace = 0;
    private $line = '';
    private $currentMax = 0;


    public function __invoke($text = null, $hasHtml = false)
    {
        if ($text === null) {
            return $this;
        }
        if ($hasHtml) {
            $this->line .= strip_tags($text) . ' ';
        } else {
            $this->line .= $text . ' ';
        }


        return $text;
    }

    public function endLine($charPerRow = null, $hasHtml = false, $charBuffer = null)
    {
        if ($charPerRow === null) {
            $charPerRow = $this->charPerRow;
        }
        if ($hasHtml) {
            $line = strip_tags($this->line);
        } else {
            $line = $this->line;
        }
        $line = trim($line);

        if (empty($line)) {
            $this->currentItem += $this->emptyLineHeight;
            return;
        }

        $this->line = '';
        $len = strlen($line);

        $capCnt = $this->countWide($line);
        $thinCnt = $this->countThin($line);
        $len = ($len - $capCnt - $thinCnt) + ($this->wideWidth * $capCnt) + ($this->thinWidth * $thinCnt);

        $add = ceil($len / $charPerRow);
        $add *= self::RESOLUTION;
        $this->currentItem += $add;
    }

    public function isOverThreshold()
    {
        if ($this->currentItem < $this->minRowsItem) {
            $this->currentItem = $this->minRowsItem;
        }
        if ($this->currentMax < $this->currentItem) {
            $this->currentMax = $this->currentItem;
        }

        $this->currentCollection += $this->currentItem;
        $this->currentCollection += $this->itemSpace;
        $this->currentItem = 0;

        if (($this->currentCollection + $this->currentMax) >= $this->maxRowsCollection) {
            $this->currentCollection = 0;
            $this->currentMax = 0;
            return true;
        }
        return false;
    }

    public function addRows($cnt)
    {
        $this->currentItem += ((int)$cnt) * self::RESOLUTION;
    }

    /**
     * This is rough estimate on how many characters fits to single line.
     *
     * <strong>Please note that this is not same as how many characters can one line hold.</strong>
     *
     * To get best possible result use average line width of those elements that are fitted to multiple lines.
     *
     * @param int $charPerRow
     * @return self
     */
    public function setCharPerRow($charPerRow)
    {
        $this->charPerRow = $charPerRow;

        return $this;
    }

    /**
     * @param float $maxRowsCollection
     * @return self
     */
    public function setMaxRowsCollection($maxRowsCollection)
    {
        $this->maxRowsCollection = ceil($maxRowsCollection * self::RESOLUTION);

        return $this;
    }

    /**
     * @param float $minRowsItem
     * @return self
     */
    public function setMinRowsItem($minRowsItem)
    {
        $this->minRowsItem = ceil($minRowsItem * self::RESOLUTION);

        return $this;
    }

    /**
     * Sets the amount of extra space needed for capital letters (Default: 0.3)
     *
     * @param float $space
     */
    public function setCapitalLetterExtra($space)
    {
        $this->wideWidth = (float)$space;
    }

    public function setEmptyLineHeight($height)
    {
        $this->emptyLineHeight = ceil($height * self::RESOLUTION);

        return $this;
    }

    /**
     * @param float $itemSpace
     * @return self
     */
    public function setItemSpace($itemSpace)
    {
        $this->itemSpace = ceil($itemSpace * self::RESOLUTION);

        return $this;
    }

    public function __toString()
    {
        return '';
    }

    private function countWide($string)
    {
        return strlen(preg_replace('{[^ABCDEFGHKLMNOPQRSTUVXYZÄÖÅ]+}', '', $string));
    }

    private function countThin($string)
    {
        return strlen(preg_replace('{[^ijlI]+}', '', $string));
    }

} 