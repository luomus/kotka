<?php

namespace Kotka\View\Helper;

use Triplestore\Classes\MYCollectionInterface;
use Triplestore\Model\Model;
use Triplestore\Service\ObjectManager;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Zend\View\Helper\AbstractHelper;

/**
 * Class ParentCollection handel picking relevant data from the paretnt collections
 * @package Kotka\View\Helper
 */
class ParentCollection extends AbstractHelper implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    /**
     * @var \Triplestore\Service\ObjectManager
     */
    static $om;

    /**
     * Pick field from the current data
     *
     * @param MYCollectionInterface|null $current
     * @param string $field
     * @param Filter|null $filter
     * @param string $join
     * @param bool $stopWhenFound
     * @return string
     */
    public function __invoke(MYCollectionInterface $current = null, $field = '', Filter $filter = null, $join = '; ', $stopWhenFound = true)
    {
        if (!$current) {
            return '';
        }
        if ($filter === null) {
            $filter = function($field, $value) {
                return $value;
            };
        }
        $result = [];
        $this->pickValuesFromParent($current->getMYIsPartOf(), $field, $stopWhenFound, $result, $filter);

        return implode($join, $result);
    }

    private function pickValuesFromParent($parent, $field, $stopWhenFond, &$result, $filter) {
        $om = $this->getObjectManager();
        $om->disableHydrator();
        $model = $om->fetch($parent, false);
        $om->enableHydrator();
        if (!$model instanceof Model) {
            return;
        }
        if ($model->hasPredicate($field)) {
            $pred = $model->getPredicate($field);
            foreach($pred as $object) {
                /** @var Object $object */
                $result[] = $filter($field, $object->getValue());
            }
        }

        if (($stopWhenFond && count($result) > 0) || !$model->hasPredicate(ObjectManager::PREDICATE_PARENT)) {
            return;
        }
        $this->pickValuesFromParent(
            $model->getPredicate(ObjectManager::PREDICATE_PARENT)->current()->getValue(),
            $field, $stopWhenFond, $result, $filter
        );
    }

    /**
     * @return \Triplestore\Service\ObjectManager
     */
    private function getObjectManager() {
        if (!self::$om) {
            self::$om = $this->getServiceLocator()
                ->getServiceLocator()->get('Triplestore/ObjectManager');
        }
        return self::$om;
    }

}
