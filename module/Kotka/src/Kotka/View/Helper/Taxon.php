<?php

namespace Kotka\View\Helper;

use Common\Service\IdService;
use Kotka\Service\TaxonService;
use Triplestore\Classes\MYDocumentInterface;
use Triplestore\Classes\MYGatheringInterface;
use Triplestore\Classes\MYIdentificationInterface;
use Triplestore\Classes\MYUnitInterface;
use Zend\View\Helper\AbstractHelper;

class Taxon extends AbstractHelper
{
    const TYPE_NAME = 'name';
    const TYPE_AUTHOR = 'author';
    const TYPE_NAME_WITH_INFRA = 'withInfra';
    const TYPE_INFRA = 'infra';

    private static $cache = [];
    private $infraRanks;

    public function __invoke($mixed, $type = self::TYPE_NAME, $infraRanks = false)
    {
        $this->infraRanks = $infraRanks;
        if (method_exists($mixed, 'getSubject')) {
            $cacheKey = $mixed->getSubject();
            if (!isset(self::$cache[$cacheKey])) {
                self::$cache[$cacheKey] = $this->extractIdentificationFromObject($mixed);
            }
        } elseif (is_array($mixed) && isset($mixed['subject'])) {
            $cacheKey = $mixed['subject'];
            if (!isset(self::$cache[$cacheKey])) {
                self::$cache[$cacheKey] = $this->extractIdentificationFromArray($mixed);
            }
        } elseif (is_array($mixed) && isset($mixed['uri'])) {
            $cacheKey = IdService::getQName($mixed['uri']);
            if (!isset(self::$cache[$cacheKey])) {
                self::$cache[$cacheKey] = $this->extractIdentificationFromArray($mixed);
            }
        } else {
            return $mixed;
        }
        switch ($type) {
            case (self::TYPE_NAME):
                return self::$cache[$cacheKey][self::TYPE_NAME];
            case (self::TYPE_AUTHOR):
                return self::$cache[$cacheKey][self::TYPE_AUTHOR];
            case (self::TYPE_NAME_WITH_INFRA):
                return implode(' ', array_filter([
                    self::$cache[$cacheKey][self::TYPE_NAME],
                    self::$cache[$cacheKey][self::TYPE_INFRA]]
                ));
            case (self::TYPE_INFRA):
                return self::$cache[$cacheKey][self::TYPE_INFRA];
            default:
                throw new \Exception('Invalid type given to Taxon view helper');
        }
    }

    private function extractIdentificationFromArray($data) {
        if (isset($data['MY.taxon']) || isset($data['MY.taxonVerbatim']) || isset($data['MYTaxon']) || isset($data['MYTaxonVerbatim'])) {
            return [
                self::TYPE_NAME   => $this->extractNameFromArray($data, false),
                self::TYPE_AUTHOR => isset($data['MY.author']) ? $data['MY.author'] : (isset($data['MYAauthor']) ? $data['MY.author'] : ''),
                self::TYPE_INFRA => $this->extractInfraFromArray($data),
            ];
        } else if (isset($data['has'])) {
            $result = [];
            foreach ($data['has'] as $sub) {
                $type = $sub['rdf:type'];
                if (!isset($result[$type])) {
                    $result[$type] = [];
                }
                array_push($result[$type], $sub);
            }
            if (isset($result['MY.gathering'])) {
                return $this->extractIdentificationFromArray(current($result['MY.gathering']));
            } else if (isset($result['MY.unit'])) {
                return $this->extractIdentificationFromArray(current($result['MY.unit']));
            } else if (isset($result['MY.identification'])) {
                return $this->extractIdentificationFromArray(
                    current(\Zend\Filter\StaticFilter::execute($result['MY.identification'], 'Kotka\Filter\SortIdentifications'))
                );
            } else {
                return $this->extractIdentificationFromArray(current($data['has']));
            }

        } else if (isset($data['MY.gathering'])) {
            return $this->extractIdentificationFromArray(current($data['MY.gathering']));
        } else if (isset($data['MYGathering'])) {
            return $this->extractIdentificationFromArray(current($data['MYGathering']));
        } else if (isset($data['MY.unit'])) {
            return $this->extractIdentificationFromArray(current($data['MY.unit']));
        } else if (isset($data['MYUnit'])) {
            return $this->extractIdentificationFromArray(current($data['MUnit']));
        } else if (isset($data['MY.identification'])) {
            return $this->extractIdentificationFromArray(current(\Zend\Filter\StaticFilter::execute($data['MY.identification'], 'Kotka\Filter\SortIdentifications')));
        } else if (isset($data['MYIdentification'])) {
            return $this->extractIdentificationFromArray(current(\Zend\Filter\StaticFilter::execute($data['MYIdentification'], 'Kotka\Filter\SortIdentifications')));
        } else {
            return [
                self::TYPE_NAME   => '',
                self::TYPE_AUTHOR => '',
                self::TYPE_INFRA => '',
            ];
        }
    }

    private function extractIdentificationFromObject($object) {
        if ($object instanceof MYIdentificationInterface) {
            return [
                self::TYPE_NAME   => '' . $this->extractName($object, false),
                self::TYPE_AUTHOR => '' . $object->getMYAuthor(),
                self::TYPE_INFRA => '' . $this->extractInfra($object),
            ];
        } else if ($object instanceof MYDocumentInterface) {
            return $this->extractIdentificationFromObject(current($object->getMYGathering()));
        } else if ($object instanceof MYGatheringInterface) {
            return $this->extractIdentificationFromObject(current($object->getMYUnit()));
        } else if ($object instanceof MYUnitInterface) {
            return $this->extractIdentificationFromObject(current($object->getMYIdentification()));
        } else {
            return [
                self::TYPE_NAME   => '',
                self::TYPE_AUTHOR => '',
                self::TYPE_INFRA => ''
            ];
        }
    }

    private function extractNameFromArray($identification, $withInfra = false) {
        return TaxonService::extractNameFromArray($identification, $withInfra, $this->infraRanks);
    }

    private function extractName(MYIdentificationInterface $identification, $withInfra = false) {
        return TaxonService::extractName($identification, $withInfra, $this->infraRanks);
    }

    private function extractInfraFromArray($identification) {
        return TaxonService::extractInfraFromArray($identification, $this->infraRanks);
    }

    private function extractInfra(MYIdentificationInterface $identification) {
        return TaxonService::extractInfra($identification, $this->infraRanks);
    }


} 