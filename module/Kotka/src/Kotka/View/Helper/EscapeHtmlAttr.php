<?php

namespace Kotka\View\Helper;

use Zend\Cache\Storage\StorageInterface;
use Zend\Escaper\Escaper;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\View\Helper\AbstractHelper;

/**
 *
 */
class EscapeHtmlAttr extends AbstractHelper implements ServiceLocatorAwareInterface
{
    protected $serviceLocator;

    public function __invoke($data)
    {

        if ($data === null || $data == '') {
            return '';
        }
        $cacheKey = 'form-' . md5($data);
        /** @var StorageInterface $cache */
        $cache = $this->serviceLocator->getServiceLocator()->get('Kotka\Cache');
        if ($cache->hasItem($cacheKey)) {
            return $cache->getItem($cacheKey);
        }
        $escaper = new Escaper();
        $data = $escaper->escapeHtmlAttr($data);
        $cache->setItem($cacheKey, $data);

        return $data;
    }

    /**
     * Sets the service locator
     * @param ServiceLocatorInterface $sl
     */
    public function setServiceLocator(ServiceLocatorInterface $sl)
    {
        $this->serviceLocator = $sl;
    }

    /**
     * Gets the service locator
     * @return ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

}
