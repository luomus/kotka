<?php

namespace Kotka\View\Helper;

use Kotka\Service\DatasetService;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\View\Helper\AbstractHelper;

/**
 * Class DatasetName view helper for finding dataset name based on the uri
 * @package Kotka\View\Helper
 */
class DatasetName extends AbstractHelper implements ServiceLocatorAwareInterface
{
    protected $serviceLocator;

    private static $cache = [
        'name' => []
    ];

    /**
     * Finds the dataset name
     *
     * @param $id
     * @param $glue
     * @return mixed
     * @throws \Exception
     */
    public function __invoke($id, $glue = '; ')
    {
        if (is_array($id)) {
            return join($glue, array_map([$this, "__invoke"], $id));
        }
        if (!isset(self::$cache[$id])) {
            self::$cache[$id] = $this->getDatasetName($id);
        }
        return self::$cache[$id];
    }

    private function getDatasetName($id)
    {
        /** @var DatasetService $cs */
        $cs = $this->serviceLocator->getServiceLocator()->get('Kotka\Service\DatasetService');
        /** @var \Kotka\Triple\GXDataset $dataset */
        $dataset = $cs->getById($id);
        if ($dataset === null) {
            return '';
        }
        return $dataset->getGXDatasetName('en');
    }

    /**
     * Sets service locator
     * @param ServiceLocatorInterface $sl
     */
    public function setServiceLocator(ServiceLocatorInterface $sl)
    {
        $this->serviceLocator = $sl;
    }

    /**
     * Gets service locator
     * @return ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

}
