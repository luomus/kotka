<?php

namespace Kotka\View\Helper;

use Kotka\Service\TaxonService;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Zend\View\Helper\AbstractHelper;

class CurrentTaxon extends AbstractHelper implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    const TYPE_NAME = 'name';
    const TYPE_TAXON_ID = 'taxonID';
    const TYPE_FAMILY = 'family';
    const TYPE_AUTHOR = 'author';
    const TYPE_EXISTS = 'has';

    /** @var  TaxonService */
    private $taxonService;

    public function __invoke($taxon, $type = self::TYPE_NAME, $checklist = TaxonService::MASTER_CHECKLIST)
    {
        if (!is_string($taxon) || $taxon === '') {
            return '';
        }
        switch ($type) {
            case (self::TYPE_NAME):
                return $this->getTaxonService()->getCurrentName($taxon, $checklist);
            case (self::TYPE_AUTHOR):
                return $this->getTaxonService()->getAuthor($taxon, $checklist);
            case (self::TYPE_EXISTS):
                return $this->getTaxonService()->hasTaxon($taxon, $checklist);
            case (self::TYPE_FAMILY):
                return $this->getTaxonService()->getCurrentFamilyName($taxon, $checklist);
            case (self::TYPE_TAXON_ID):
                return $this->getTaxonService()->getCurrentTaxonId($taxon, $checklist);
            default:
                throw new \Exception('Invalid type given to Taxon view helper');
        }
    }

    /**
     * @return TaxonService
     */
    protected function getTaxonService()
    {
        if ($this->taxonService === null) {
            $this->taxonService = $this->getServiceLocator()
                ->getServiceLocator()
                ->get('Kotka\Service\TaxonService');
        }
        return $this->taxonService;
    }

} 