<?php

namespace Kotka\View\Helper;

use Kotka\Service\SpecimenService;
use Kotka\Triple\MYIdentification;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Zend\View\Helper\AbstractHelper;

/**
 * Class ResolveHostName
 *
 * @package Kotka\View\Helper
 */
class ResolveHostName extends AbstractHelper implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    /** @var SpecimenService */
    private $specimenService;

    /**
     * Returns options for the field
     *
     * If there isn't any options empty array is returned
     *
     * @param $host
     * @return string
     */
    public function __invoke($host)
    {
        $specimenService = $this->getSpecimenService();
        $specimen = $specimenService->getById($host);
        if ($specimen == null) {
            return $host;
        }
        $identifications = $specimenService->getAllIdentifications($specimen);
        if (count($identifications) == 1) {
            /** @var MYIdentification $identification */
            $identification = $identifications[0];
            $taxon = $identification->getMYTaxon();
            if (!empty($taxon)) {
                return $taxon;
            }
        }
        return $host;
    }

    /**
     * Fetches the specimen service
     *
     * @return SpecimenService
     */
    public function getSpecimenService()
    {
        if ($this->specimenService === null) {
            $this->specimenService = $this->getServiceLocator()->getServiceLocator()->get('Kotka\Service\SpecimenService');
        }
        return $this->specimenService;
    }

}
