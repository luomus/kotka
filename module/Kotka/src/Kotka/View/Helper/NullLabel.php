<?php

namespace Kotka\View\Helper;

use Triplestore\Model\Model;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\View\Helper\AbstractHelper;

/**
 * Class NullLabel is used to fetch label with null lang code
 *
 * @package Kotka\View\Helper
 */
class NullLabel extends AbstractHelper implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    private static $triplestore;
    private static $cache = [];

    public function __invoke($key)
    {
        if (isset(self::$cache[$key])) {
            return self::$cache[$key];
        }
        $label = '';
        $triplestore = $this->getTriplstore();
        $triplestore->disableHydrator();
        $model = $this->getTriplstore()->fetch($key, false);
        $triplestore->enableHydrator();
        if ($model instanceof Model) {
            $cand = $model->getOrCreatePredicate('rdfs:label')->getLiteralValue();
            if (isset($cand[0])) {
                $label = $cand[0];
            }
        }
        self::$cache[$key] = $label;
        return $label;
    }

    /**
     * @return \Triplestore\Service\ObjectManager
     */
    private function getTriplstore() {
        if (!self::$triplestore) {
            self::$triplestore = $this->serviceLocator->getServiceLocator()->get('Triplestore\ObjectManager');
        }
        return self::$triplestore;
    }

}
