<?php

namespace Kotka\View\Helper;

use Kotka\Filter\UnreliableFields;
use Kotka\Triple\MYDocument;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\View\Helper\AbstractHelper;

/**
 * Class MarkUnreliableFields
 * @package Kotka\View\Helper
 */
class MarkUnreliableFields extends AbstractHelper implements ServiceLocatorAwareInterface
{
    const POS_PREPEND = 0;
    const POS_APPEND = 1;

    private $mark = '?';
    private $position = self::POS_PREPEND;

    /** @var  UnreliableFields */
    private static $filter;
    private $serviceLocator;

    /**
     * Invoking this adds specified unreliable marking to the given object or array
     *
     * @param null|array|MYDocument $object
     * @return $this|null
     * @throws \Zend\View\Exception\BadMethodCallException
     */
    public function __invoke(& $object = null)
    {
        if ($object === null) {
            return $this;
        }
        $filter = $this->getFilter();
        $object = $filter->filter($object);
        return null;
    }

    /**
     * @return string
     */
    public function getMark()
    {
        return $this->getFilter()->getMark();
    }

    /**
     * @param $mark
     * @return MarkUnreliableFields
     */
    public function setMark($mark)
    {
        $this->getFilter()->setMark($mark);

        return $this;
    }

    /**
     * @return int
     */
    public function getPosition()
    {
        return $this->getFilter()->getPosition();
    }

    /**
     * @param $position
     * @return MarkUnreliableFields
     */
    public function setPosition($position)
    {
        $this->getFilter()->setPosition($position);
        return $this;
    }


    /**
     * Initializes the predicate object
     */
    private function getFilter()
    {
        if (self::$filter === null) {
            self::$filter = $this->serviceLocator
                ->getServiceLocator()
                ->get('FilterManager')
                ->get('UnreliableFields');
            self::$filter->setMark($this->mark);
            self::$filter->setPosition($this->position);
        }
        return self::$filter;
    }

    /**
     * Sets service locator
     * @param ServiceLocatorInterface $sl
     */
    public function setServiceLocator(ServiceLocatorInterface $sl)
    {
        $this->serviceLocator = $sl;
    }

    /**
     * Gets service locator
     * @return ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

}
