<?php

namespace Kotka\View\Helper;

use Kotka\Model\Label;
use Triplestore\Classes\MYIdentificationInterface;
use Zend\View\Helper\AbstractHelper;

class FullTaxon extends AbstractHelper
{
    private $speciesAfterFirstSpace = [
        'cf',
        'cf.',
        'af',
        'af.',
        's.str',
        's.str.',
        's. str',
        's. str.',
    ];


    public function __invoke($taxon = null, $genusQualifier = null, $speciesQualifier = null)
    {
        if (empty($taxon)) {
            return $taxon;
        }

        if ($taxon instanceof Label) {
            list($taxon, $genusQualifier, $speciesQualifier) = $this->explodeLabel($taxon);
        } elseif ($taxon instanceof MYIdentificationInterface) {
            list($taxon, $genusQualifier, $speciesQualifier) = $this->explodeIdentification($taxon);
        }

        $taxonParts = array_filter(explode(' ', $taxon));

        if (!empty($genusQualifier) && !preg_match('/\b'.$genusQualifier.'\b/i',$taxon)) {
            array_splice($taxonParts, 1, 0, $genusQualifier);
        }

        if (!empty($speciesQualifier) && !preg_match('/\b'.$speciesQualifier.'\b/i',$taxon)) {
            if (in_array(mb_strtolower($speciesQualifier, 'utf-8'), $this->speciesAfterFirstSpace)) {
                array_splice($taxonParts, 1, 0, $speciesQualifier);
            } else {
                array_push($taxonParts, $speciesQualifier);
            }

        }
        return implode(' ', $taxonParts);
    }

    private function explodeLabel(Label $label) {
        return [$label->getTaxon(), $label->getGenusQualifier(), $label->getSpeciesQualifier()];
    }

    private function explodeIdentification(MYIdentificationInterface $identification) {
        return [
            $identification->getMYTaxon(),
            $identification->getMYGenusQualifier(),
            $identification->getMYSpeciesQualifier()
        ];
    }

} 