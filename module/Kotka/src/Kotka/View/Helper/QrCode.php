<?php
namespace Kotka\View\Helper;

use Endroid\QrCode\QrCode as EndroidQRcode;
use Zend\View\Helper\AbstractHelper;

class QrCode extends AbstractHelper
{

    const PHPQR_PREFIX = 'php_qr:';

    protected $imageType = 'png';

    public function __invoke($text, $width = 200, $class = 'barcode', $padding = 4, $moduleSize = 3)
    {
        if (strpos($text, self::PHPQR_PREFIX) === 0) {
            $text = str_replace(self::PHPQR_PREFIX, '', $text);
            if (!class_exists('PHPQRcode', false)) {
                require __DIR__ . '/../../../../lib/phpqrcode/phpqrcode.php';
            }
            ob_start();
            \PHPQRcode::png($text, false, PHPQR_ECLEVEL_M, $moduleSize, $padding);
            $image = ob_get_clean();
        } else {
            $qr = new EndroidQRcode();
            $qr->setText($text);
            $qr->setSize($width);
            $qr->setPadding($padding);
            $qr->setModuleSize($moduleSize);
            $qr->setErrorCorrection(EndroidQRcode::LEVEL_MEDIUM);
            $image = $qr->get($this->imageType);
        }
        $img = '<img style="width:100%; height:100%" alt="barcode" src="data:image/' . $this->imageType . ';base64,' . base64_encode($image) . '" />';
        /*
                $svg = '<svg class="'.$class.'" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">'.
                        '<image width="100%" height="100%" xlink:href="data:image/' . $this->imageType . ';base64,' . base64_encode($image) . '"/>'.
                       '</svg>';
        */
        return $img;
    }
} 