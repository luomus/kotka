<?php

namespace Kotka\Form;

use Kotka\Stdlib\Hydrator\Reflection;
use Kotka\Stdlib\Hydrator\Strategy\DateTimeStrategy;
use Kotka\Triple\PUUBranch as Branch;
use Zend\Form\Form;
use Zend\InputFilter\InputFilterProviderInterface;
use Kotka\Service\CoordinateService;
use Kotka\Validator\Coordinate;
use Kotka\Validator\RequireWithThis;

/**
 * Class PUUBranch
 * @package Kotka\Form
 */
class PUUBranch extends Form implements InputFilterProviderInterface
{

    /**
     * Creates the form with all the necessary fields in it
     */
    public function init()
    {
        $this
            ->setHydrator(new Reflection())
            ->setObject(new Branch());
    }

    public function postInit()
    {
        $this->remove('subject');
        $dateStrategy = new DateTimeStrategy();
        $dateStrategy->setOptions(array(
            'format' => 'Y-m-d'
        ));
        $this->hydrator->addStrategy('PUUDate', $dateStrategy);
    }

    /**
     * Should return an array specification compatible with
     * {@link Zend\InputFilter\Factory::createInputFilter()}.
     *
     * @return array
     */
    public function getInputFilterSpecification()
    {
        return array(
            'PUUExists' => array(
                'required' => false,
                'continue_if_empty' => true,
                'allow_empty' => true,
                'filters' => array(
                    array(
                        'name' => 'boolean'
                    )
                )
            ),
            'PUUAccessionID' => array(
                'require' => true
            ),
            'PUUWgs84Latitude' => array(
                'required' => false,
                'validators' => array(
                    array(
                        'name' => 'Coordinate',
                        'options' => array(
                            'coordinateSystem' => CoordinateService::SYSTEM_WSG84,
                            'otherOfPair' => 'PUUWgs84Longitude',
                            'direction' => Coordinate::LATITUDE,
                            'messages' => array(
                                Coordinate::OUT_OF_BOUNDS => 'Latitude out of bounds.',
                                Coordinate::NOT_NUMERIC => 'Latitude must be a number.'
                            ),
                        )
                    ),
                    array(
                        'name' => 'RequireWithThis',
                        'options' => array(
                            'field' => 'PUUWgs84Longitude',
                            'messages' => array(
                                RequireWithThis::MISSING => 'Longitude should be set, when latitude is given.'
                            ),
                        ),
                    )
                )
            ),
            'PUUWgs84Longitude' => array(
                'required' => false,
                'validators' => array(
                    array(
                        'name' => 'Coordinate',
                        'options' => array(
                            'coordinateSystem' => CoordinateService::SYSTEM_WSG84,
                            'otherOfPair' => 'PUUWgs84Latitude',
                            'direction' => Coordinate::LONGITUDE,
                            'messages' => array(
                                Coordinate::OUT_OF_BOUNDS => 'Longitude out of bounds.',
                                Coordinate::NOT_NUMERIC => 'Longitude must be a number.'
                            ),
                        )
                    ),
                    array(
                        'name' => 'RequireWithThis',
                        'options' => array(
                            'field' => 'PUUWgs84Latitude',
                            'messages' => array(
                                RequireWithThis::MISSING => 'Latitude should be set, when longitude is given.'
                            ),
                        ),
                    )
                )
            ),
        );
    }
}
