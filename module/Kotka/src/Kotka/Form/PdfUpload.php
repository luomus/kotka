<?php

namespace Kotka\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilterProviderInterface;

/**
 * Class File is the form used for file importing
 * @package Kotka\Form
 */
class PdfUpload extends Form implements InputFilterProviderInterface
{
    public function __construct($name = null, $options = array())
    {
        parent::__construct($name, $options);
        $this->init();
    }


    /**
     * Creates the form with all the necessary fields in it
     */
    public function init()
    {
        parent::__construct('file-upload');
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'form-horizontal');

        $this->add(array(
            'name' => 'file',
            'type' => 'file',
            'options' => array(
                'label' => 'file'
            ),
            'attributes' => array(
                'required' => true
            )
        ));

    }

    /**
     * Should return an array specification compatible with
     * {@link Zend\InputFilter\Factory::createInputFilter()}.
     *
     * @return array
     */
    public function getInputFilterSpecification()
    {
        return array(
            'file' => array(
                'validators' => array(
                    array(
                        'name' => 'Zend\Validator\File\Size',
                        'options' => array(
                            'max' => '2MB',
                            'messages' => array(
                                \Zend\Validator\File\Size::TOO_BIG => "Maximum allowed size for file is '%max%' but '%size%' detected."
                            )
                        )
                    ),
                    array(
                        'name' => 'Zend\Validator\File\MimeType',
                        'options' => array(
                            'mimeType' => 'application/pdf',
                            'messages' => array(
                                \Zend\Validator\File\MimeType::FALSE_TYPE => "Only PDF files please"
                            )
                        )
                    )
                )
            )
        );
    }


}
