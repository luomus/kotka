<?php

namespace Kotka\Form\Factory;


use Kotka\Form\GenericLabel;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class GenericLabelFactory implements FactoryInterface
{
    /**
     * {@inheritDoc}
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $form = new GenericLabel();
        $form->setServiceLocator($serviceLocator);
        $warnings = $serviceLocator
            ->getServiceLocator()
            ->get('InputFilterManager')
            ->get('Kotka\InputFilter\GenericLabelWarning');
        $form->setInputWarningFilter($warnings);

        return $form;
    }
}