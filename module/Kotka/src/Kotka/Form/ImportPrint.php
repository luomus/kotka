<?php

namespace Kotka\Form;

use Zend\Form\Form;

/**
 * Class ImportPrint is the form used to select fields wanted on the labels and the type of labels
 * @package Kotka\Form
 */
class ImportPrint extends Form
{
    /**
     * Creates the form with all the necessary fields in it
     */
    public function init()
    {
        $this->setAttribute('class', 'form-horizontal');

        $this->add(array(
            'name' => 'format',
            'options' => array(
                'label' => 'Display results as',
            ),
            'attributes' => array(
                'id' => 'labelFormat',
            ),
            'type' => 'Kotka\Form\Element\LabelFormat'
        ));

        $this->add(array(
            'name' => 'sample-format',
            'options' => array(
                'label' => 'Display results as',
            ),
            'attributes' => array(
                'id' => 'SampleLabelFormat',
            ),
            'type' => 'Kotka\Form\Element\SampleLabelFormat'
        ));

        $this->add(array(
            'name' => 'doubleSided',
            'type' => 'checkbox',
            'attributes' => array(
                'class' => 'doubleSided'
            ),
            'options' => [
                'label' => '2-sided (flip horizontally)',
                'use_hidden_element' => false
            ]
        ));

        $this->add(array(
            'name' => 'labelFields',
            'type' => 'Kotka\Form\Element\LabelFields',
            'attributes' => array(
                'id' => 'labelFields',
                'multiple' => true,
                'style' => 'width:250px; height: 155px;',
                'value' => array('country', 'province', 'locality', 'coordinate', 'municipality', 'leg')
            ),
            'options' => array(
                'label' => 'Optional fields on insect labels',
                'template' => 'kotka/partial/element/select2'
            )
        ));

    }
}
