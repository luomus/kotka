<?php

namespace Kotka\Form;

use Zend\InputFilter\InputFilterProviderInterface;

/**
 * Class MOSOrganization
 * @package Kotka\Form
 */
class MOSOrganization extends WarningForm implements InputFilterProviderInterface
{

    /**
     * Should return an array specification compatible with
     * {@link Zend\InputFilter\Factory::createInputFilter()}.
     *
     * @return array
     */
    public function getInputFilterSpecification()
    {
        return array(
            'MZOwner' => array(
                'required' => true,
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\NotEmpty::IS_EMPTY => 'You have to specify the owner of the record',
                            ),
                        ),
                        'break_chain_on_failure' => true
                    ),
                    array(
                        'name' => 'UserInOrganization',
                    )
                )
            ),
            'MOSOrganizationLevel1En' => array(
                'required' => true,
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\NotEmpty::IS_EMPTY => 'Level 1 (organization) must be set.'
                            )
                        )
                    )
                )
            ),
            'MOSAbbreviation' => array(
                'required' => true,
                'allow_empty' => true,
                'continue_if_empty' => true,
                'validators' => array(
                    array(
                        'name' => 'RequiredWhen',
                        'options' => array(
                            'field' => 'MOSAbbreviationExplanation',
                            'type' => \Kotka\Validator\RequiredWhen::NOT_EMPTY,
                            'messages' => array(
                                \Kotka\Validator\RequiredWhen::MISSING => 'Abbreviation must be given when abbreviation source is selected.'
                            ),
                        ),
                    )
                )
            ),
            'MOSAbbreviationExplanation' => array(
                'required' => true,
                'allow_empty' => true,
                'continue_if_empty' => true,
                'validators' => array(
                    array(
                        'name' => 'RequiredWhen',
                        'break_chain_on_failure' => true,
                        'options' => array(
                            'field' => 'MOSAbbreviation',
                            'type' => \Kotka\Validator\RequiredWhen::NOT_EMPTY,
                            'messages' => array(
                                \Kotka\Validator\RequiredWhen::MISSING => 'Abbreviation source must be selected when abbreviation is given.'
                            ),
                        ),
                    )
                )
            ),
            'MOSAd' => array(
                'required' => false,
                'validators' => array(
                    array(
                        'name' => 'UniqueADGroup',
                    )
                )
            ),
            'MOSDatasetID' => array(
                'required' => false,
                'filters' => array(
                    array(
                        'name' => 'ForceArray',
                    ),
                    array(
                        'name' => 'EmptyArray',
                    ),
                    array(
                        'name' => 'ToQName'
                    ),
                ),
                'validators' => array(
                    array(
                        'name' => 'InDatabaseSelect',
                        'options' => array(
                            'field' => 'MOS.datasetID'
                        )
                    )
                )
            )
        );
    }
}
