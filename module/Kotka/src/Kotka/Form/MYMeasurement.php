<?php
namespace Kotka\Form;

use Kotka\Stdlib\Hydrator\Reflection;
use Kotka\Triple\MYMeasurement as Measurement;
use Zend\Form\Fieldset;

/**
 * Class MYMeasurement
 * @package Kotka\Form
 */
class MYMeasurement extends Fieldset
{

    /**
     * Creates the form with all the necessary fields in it
     */
    public function init()
    {
        parent::init();
        $this
            ->setHydrator(new Reflection())
            ->setObject(new Measurement());
    }

    public function postInit()
    {
        $this->remove('subject');
    }
}