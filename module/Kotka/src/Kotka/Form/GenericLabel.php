<?php

namespace Kotka\Form;


use Zend\Form\Form;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

class GenericLabel extends WarningForm implements ServiceLocatorAwareInterface, InputFilterProviderInterface
{
    use ServiceLocatorAwareTrait;

    public function init()
    {
        parent::__construct('importExcel');
        $this->setAttribute('method', 'get');
        $this->setAttribute('class', 'form-horizontal');


        $this->add(array(
            'name' => 'identifier',
            'type' => 'hidden',
            'options' => array(
                'label' => 'Identifier range',
            ),
            'attributes' => array(
                'class' => 'form-control',
                'placeholder' => 'eg. HT.30-33, tun:JAA.300-400, luomus:JA.1-10',
                'title' => 'eg. HT.30-33, tun:JAA.300-400, luomus:JA.1-10',
                'id' => 'identifier',
                'tabindex' => 1
            ),
        ));

        $this->add(array(
            'name' => 'fulluri',
            'type' => 'checkbox',
            'options' => array(
                'label' => 'full uri in QR Code',
            ),
            'attributes' => array(
                'id' => 'full-uri',
                'value' => true,
                'class' => 'form-control-static'
            )
        ));

        $this->add(array(
            'name' => 'qr-size',
            'type' => 'select',
            'options' => array(
                'label' => 'Size of the QR Code',
                'value_options' => [
                    '10.2' => '10mm',
                    '8.2' => '8mm',
                ]
            ),
            'attributes' => array(
                'id' => 'qr-size',
                'value' => '9',
                'class' => 'form-control-static'
            )
        ));

        $this->add(array(
            'name' => 'width',
            'type' => 'number',
            'options' => array(
                'label' => 'Width (in mm)',
            ),
            'attributes' => array(
                'value' => '30',
                'min' => '15',
                'max' => '200',
            )
        ));

        $this->add(array(
            'name' => 'row1',
            'type' => 'text',
            'options' => array(
                'label' => '1. row',
            )
        ));

        $this->add(array(
            'name' => 'row2',
            'type' => 'text',
            'options' => array(
                'label' => '2. row',
            )
        ));

        $this->add(array(
            'name' => 'row3',
            'type' => 'text',
            'options' => array(
                'label' => '3. row',
            )
        ));

        $this->add(array(
            'name' => 'row4',
            'type' => 'text',
            'options' => array(
                'label' => '4. row',
            )
        ));

        $this->add(array(
            'name' => 'row5',
            'type' => 'text',
            'options' => array(
                'label' => '5. row',
            )
        ));

    }

    /**
     * Should return an array specification compatible with
     * {@link Zend\InputFilter\Factory::createInputFilter()}.
     *
     * @return array
     */
    public function getInputFilterSpecification()
    {
        return array(
            'identifier' => array(
                'continue_if_empty' => true,
                'validators' => array(
                    array(
                        'name' => 'Kotka\Validator\IdentifierRange',
                        'options' => array(
                            'limit' => 1000
                        )
                    )
                )
            ),
            'width' => array(
                'required' => true
            )
        );
    }
}