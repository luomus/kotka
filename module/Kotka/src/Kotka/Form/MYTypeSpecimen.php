<?php

namespace Kotka\Form;

use Kotka\Stdlib\Hydrator\Reflection;
use Kotka\Triple\MYTypeSpecimen as TypeSpecimen;
use Zend\Form\Fieldset;

/**
 * Class MYTypeSpecimen
 * @package Kotka\Form
 */
class MYTypeSpecimen extends Fieldset
{

    /**
     * Creates the form with all the necessary fields in it
     */
    public function init()
    {
        parent::init();
        $this
            ->setHydrator(new Reflection())
            ->setObject(new TypeSpecimen());
    }

    public function postInit()
    {
        $this->remove('subject');
    }
}
