<?php

namespace Kotka\Form;

use Kotka\Stdlib\Hydrator\Reflection;
use Kotka\Triple\MYUnit as Unit;
use Zend\Form\Fieldset;

/**
 * Class MYUnit
 * @package Kotka\Form
 */
class MYUnit extends Fieldset
{

    /**
     * Creates the form with all the necessary fields in it
     */
    public function init()
    {
        parent::init();
        $this
            ->setHydrator(new Reflection())
            ->setObject(new Unit());
    }

    public function postInit()
    {
        $this->remove('subject');
        $ident = $this->get('MYIdentification');
        $target = $ident->getTargetElement();
        $target->postInit();

        $type = $this->get('MYTypeSpecimen');
        $target = $type->getTargetElement();
        $target->postInit();

        $type = $this->get('MFSample');
        $target = $type->getTargetElement();
        $target->postInit();
    }
}
