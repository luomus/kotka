<?php
namespace Kotka\Form;

use Kotka\Stdlib\Hydrator\Reflection;
use Kotka\Triple\MFSample as Sample;
use Zend\Form\Fieldset;

/**
 * Class MFSample
 * @package Kotka\Form
 */
class MFSample extends Fieldset
{

    /**
     * Creates the form with all the necessary fields in it
     */
    public function init()
    {
        parent::init();
        $this
            ->setHydrator(new Reflection())
            ->setObject(new Sample());
    }

    public function postInit()
    {
        $this->get('subject')->setLabel('Preparation/sample id');
        $prep = $this->get('MFPreparation');
        $target = $prep->getTargetElement();
        $target->postInit();
    }
}