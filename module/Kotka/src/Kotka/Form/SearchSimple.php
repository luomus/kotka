<?php

namespace Kotka\Form;

use Zend\Form\Form;

class SearchSimple extends Form
{

    /**
     * Creates the form with all the necessary fields in it
     */
    public function init()
    {
        parent::__construct('simple_search');
        $this->setAttribute('class', 'form-horizontal');

        $this->add(array(
            'name' => 'taxon',
            'type' => 'text',
            'attributes' => array(
                'id' => 'qb-taxon',
                'placeholder' => '.',
                'class' => 'query-builder',
            ),
            'options' => array(
                'allow-new' => true,
                'label' => 'Taxon',
                'template' => 'kotka/partial/element/select2',
                'source' => '/picker/taxon/autocomplete',
            )
        ));

        $this->add(array(
            'name' => 'originalSpecimenID',
            'type' => 'text',
            'attributes' => array(
                'placeholder' => '.',
                'id' => 'qb-originalSpecimenID',
                'multiple' => true,
                'class' => 'query-builder',
                'data-elastic' => 'originalSpecimenID.raw',
            ),
            'options' => array(
                'label' => 'Original specimen id',
            ),
        ));

        $this->add(array(
            'name' => 'collection',
            'type' => 'DatabaseSelect',
            'attributes' => array(
                'id' => 'qb-collection',
                'multiple' => true,
                'class' => 'query-builder',
            ),
            'options' => array(
                'label' => 'Collections',
                'field' => 'MY.collectionID',
                'template' => 'kotka/partial/element/select2'
            )
        ));

        $this->add(array(
            'name' => 'sub-collection',
            'type' => 'checkbox',
            'attributes' => array(
                'id' => 'qb-sub-collection',
                'class' => 'query-builder',
            ),
            'options' => array(
                'label' => 'Include collections',
                'help' => 'Include all specimens that belong to or are part of the selected collection'
            )
        ));

        $this->add(array(
            'name' => 'dataset',
            'type' => 'DatabaseSelect',
            'attributes' => array(
                'id' => 'qb-dataset',
                'multiple' => true,
                'class' => 'query-builder',
                'data-elastic' => 'datasetID',
            ),
            'options' => array(
                'label' => 'Tags',
                'field' => 'MY.datasetID',
                'template' => 'kotka/partial/element/select2'
            )
        ));

        /*
                $this->add(array(
                    'name' => 's[]',
                    'type' => 'DatabaseSelect',
                    'attributes' => array(
                        'id' => 'simple-country',
                        'data-elastic' => 'country.raw',
                        'multiple' => true,
                    ),
                    'options' => array(
                        'label' => 'Country',
                        'field' => 'MY.country',
                        'template' => 'kotka/partial/element/select2',
                        'class' => 'query-builder',
                    ),
                ));
        */
        $this->add(array(
            'name' => 'editor',
            'type' => 'DatabaseSelect',
            'attributes' => array(
                'data-elastic' => 'editor.raw',
                'id' => 'qb-editor',
                'placeholder' => 'Editor'
            ),
            'options' => array(
                'label' => 'Editor',
                'field' => 'MZ.editor',
                'template' => 'kotka/partial/element/select2',
                'class' => 'query-builder',
                'value-as-key' => true
            ),
        ));

        $this->add(array(
            'name' => 'creator',
            'type' => 'DatabaseSelect',
            'attributes' => array(
                'data-elastic' => 'creator.raw',
                'id' => 'qb-creator',
                'placeholder' => 'Creator'
            ),
            'options' => array(
                'label' => 'Creator',
                'field' => 'MZ.editor',
                'template' => 'kotka/partial/element/select2',
                'class' => 'query-builder',
                'value-as-key' => true
            ),
        ));

        $this->add(array(
            'name' => 'leg',
            'type' => 'text',
            'attributes' => array(
                'placeholder' => '.',
                'id' => 'qb-leg',
                'multiple' => true,
                'class' => 'query-builder',
                'data-elastic' => 'leg.raw',
            ),
            'options' => array(
                'label' => 'Leg',
                'template' => 'kotka/partial/element/select2',
                'source' => '/picker/leg/autocomplete',
            ),
        ));

        $this->add(array(
            'name' => 'municipality',
            'type' => 'text',
            'attributes' => array(
                'placeholder' => '.',
                'id' => 'qb-municipality',
                'multiple' => true,
                'class' => 'query-builder',
                'data-elastic' => 'municipality.raw',
            ),
            'options' => array(
                'label' => 'Finnish municipality',
                'template' => 'kotka/partial/element/select2',
                'source' => '/picker/municipality/autocomplete',
            ),
        ));


        $this->add(array(
            'name' => 'locality',
            'type' => 'text',
            'attributes' => array(
                'placeholder' => '.',
                'id' => 'qb-locality',
                'multiple' => true,
                'class' => 'query-builder',
                'data-elastic' => 'locality.raw',
            ),
            'options' => array(
                'label' => 'Locality name',
                'template' => 'kotka/partial/element/select2',
                'source' => '/picker/locality/autocomplete',
            ),
        ));

    }

    public function ykjValidator($value)
    {
        list($lat, $lon) = explode(':', $value);
        if ($lon === null) {
            return false;
        }
        $latLen = strlen($lat);
        $lonLen = strlen($lon);
        if ($latLen < 3 || $latLen > 7) {
            return false;
        }
        if ($latLen != $lonLen) {
            return false;
        }
        if (!is_numeric($lat) || !is_numeric($lon)) {
            return false;
        }
        $fullLat = (int)($lat . str_repeat('0', 7 - $latLen));
        $fullLon = (int)($lon . str_repeat('0', 7 - $lonLen));
        if ($fullLat < 6600000 || $fullLat > 7800000 || $fullLon < 3000000 || $fullLon > 3800000) {
            return false;
        }
        return true;
    }

    public function dateValidator($value)
    {
        $parts = explode('/', $value);
        $partCnt = count($parts);
        if ($partCnt > 2) {
            return false;
        }
        $dateParts = explode('-', $parts[0]);
        $lengths = [4, 2, 2];
        foreach ($dateParts as $idx => $part) {
            if (!isset($lengths[$idx]) || strlen($part) != $lengths[$idx] || !is_numeric($part)) {
                return false;
            }
        }
        if ($partCnt == 2) {
            $dateEndParts = explode('-', $parts[1]);
            $dateEndCount = count($dateEndParts);
            $offset = count($dateParts) - $dateEndCount;
            foreach ($dateEndParts as $idx => $part) {
                $idx += $offset;
                if (!isset($lengths[$idx]) || strlen($part) != $lengths[$idx] || !is_numeric($part)) {
                    return false;
                }
            }
            $last = array_pop($dateParts);
            for ($i = $dateEndCount; $i > 1; $i--) {
                $last = array_pop($dateParts);
            }
            $first = array_shift($dateEndParts);
            if ((int)$last > (int)$first) {
                return false;
            }
        }

        return true;
    }

}