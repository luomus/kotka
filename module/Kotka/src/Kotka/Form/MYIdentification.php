<?php

namespace Kotka\Form;

use Kotka\Stdlib\Hydrator\Reflection;
use Kotka\Triple\MYIdentification as Identification;
use Zend\Form\Fieldset;

/**
 * Class MYIdentification
 * @package Kotka\Form
 */
class MYIdentification extends Fieldset
{

    /**
     * Creates the form with all the necessary fields in it
     */
    public function init()
    {
        parent::init();
        $this
            ->setHydrator(new Reflection())
            ->setObject(new Identification());
    }

    public function postInit()
    {
        $this->remove('subject');
    }
}
