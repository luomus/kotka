<?php

namespace Kotka\Form\View\Helper;

use Zend\Form\ElementInterface;
use Zend\Form\View\Helper\FormElementErrors as ZendElementError;

class FormElementErrors extends ZendElementError
{

    public function render(ElementInterface $element, array $attributes = array())
    {
        $html = parent::render($element, $attributes);
        if ($html === '') {
            return '';
        }

        return '<div class="row errors-list"><div class="col-sm-12">' . $html . '</div></div>';
    }

} 