<?php
namespace Kotka\Form\View\Helper;

use Kotka\Form\Element\ArrayCollection;
use Zend\Form\ElementInterface;

class FormElement extends \Zend\Form\View\Helper\FormElement
{

    const ARRAY_PARTIAL = 'kotka/partial/element/collection';

    protected $noFormControl = array('file', 'checkbox', 'radio', 'submit', 'multi_checkbox', 'static', 'button', 'hidden');


    /**
     * Invoke helper as function
     *
     * Proxies to {@link render()}.
     *
     * @param  ElementInterface|null $element
     * @param string|null $elementClass
     * @return string|FormElement
     */
    public function __invoke(ElementInterface $element = null, $elementClass = null)
    {
        if (!$element) {
            return $this;
        }

        return $this->render($element, $elementClass);
    }

    public function render(ElementInterface $element, $elementClass = null)
    {
        $renderer = $this->getView();
        $type = $element->getAttribute('type');
        $options = $element->getOptions();
        $elementClass = $elementClass === null ? TBConf::get(TBConf::INPUT_CLASS) : $elementClass;

        if (!in_array($type, $this->noFormControl)) {
            $classes = $element->getAttribute('class');
            if ($elementClass !== '') {
                if ($classes) {
                    if (strpos($classes, $elementClass) === false) {
                        $element->setAttribute('class', trim($classes . ' ' . $elementClass));
                    }
                } else {
                    $element->setAttribute('class', $elementClass);
                }
            }
        }

        if ($element instanceof ArrayCollection) {
            $helper = $renderer->plugin('form_array_collection');
            return $helper($element, $elementClass);
            //return $this->view->render(self::ARRAY_PARTIAL, array('element' => $element));
        }

        if (isset($options['template'])) {
            $template = $options['template'];
            unset($options['template']);
            $element->setOptions($options);

            return $this->view->render($template, array('element' => $element));
        }

        return parent::render($element);
    }

} 