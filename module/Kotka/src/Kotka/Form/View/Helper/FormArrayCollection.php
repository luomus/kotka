<?php

namespace Kotka\Form\View\Helper;

use Kotka\Form\Element\ArrayCollection as CollectionElement;
use RuntimeException;
use Zend\Form\Element;
use Zend\Form\ElementInterface;
use Zend\Form\FieldsetInterface;
use Zend\Form\View\Helper\AbstractHelper;
use Zend\View\Helper\AbstractHelper as BaseAbstractHelper;

class FormArrayCollection extends AbstractHelper
{
    /**
     * If set to true, collections are automatically wrapped around a fieldset
     *
     * @var bool
     */
    protected $shouldWrap = false;

    /**
     * The name of the default view helper that is used to render sub elements.
     *
     * @var string
     */
    protected $defaultElementHelper = 'formelement';

    /**
     * The view helper used to render sub elements.
     *
     * @var AbstractHelper
     */
    protected $elementHelper;

    protected $addHtml = '<div class="col-sm-1 no-gutter"><a href="#" class="add btn btn-success" tabindex="-1"><i class="fa fa-plus"></i></a></div>';

    protected $delHtml = '<div class="input-group" data-removable>%s<span class="input-group-btn"><button class="btn btn-default remove-on-click" type="button" tabindex="-1">&times;</button></span></div>';

    /**
     * The view helper used to render sub fieldsets.
     *
     * @var AbstractHelper
     */
    protected $fieldsetHelper;

    /**
     * Invoke helper as function
     *
     * Proxies to {@link render()}.
     *
     * @param  ElementInterface|null $element
     * @param  bool $wrap
     * @return string|FormArrayCollection
     */
    public function __invoke(ElementInterface $element = null, $wrap = false)
    {
        if (!$element) {
            return $this;
        }

        $this->setShouldWrap($wrap);

        return $this->render($element);
    }

    /**
     * Render a collection by iterating through all fieldsets and elements
     *
     * @param  ElementInterface $element
     * @return string
     */
    public function render(ElementInterface $element)
    {
        $renderer = $this->getView();
        if (!method_exists($renderer, 'plugin')) {
            // Bail early if renderer is not pluggable
            return '';
        }

        $markup = '';
        $templateMarkup = '';
        $addMarkup = '';
        $delMarkup = '%s';
        $rowClass = 'row';
        $escapeHtmlHelper = $this->getEscapeHtmlHelper();
        $elementHelper = $this->getElementHelper();
        $fieldsetHelper = $this->getFieldsetHelper();
        $allowRemove = $element->allowRemove();
        if ($element instanceof CollectionElement) {
            if ($element->allowAdd()) {
                $addMarkup = $this->addHtml;
            }
            if ($allowRemove) {
                $delMarkup = $this->delHtml;
            }
            if ($element->getTargetElement() instanceof Element\Hidden) {
                $delMarkup = '%s';
                $addMarkup = '';
                $rowClass = '';
            }
            if ($element->shouldCreateTemplate()) {
                $templateMarkup = $this->renderTemplate($element, $delMarkup);
            }
        }
        $cnt = 0;
        $count = $element->getCount();
        foreach ($element->getIterator() as $elementOrFieldset) {
            if ($elementOrFieldset->getValue() === null && $cnt >= $count) {
                continue;
            }
            $cnt++;
            $elementOrFieldset->setLabel('');
            if ($elementOrFieldset instanceof Element\Hidden) {
                $delMarkup = '%s';
                $addMarkup = '';
            }
            if ($elementOrFieldset instanceof FieldsetInterface) {
                $markup .= sprintf($delMarkup, $fieldsetHelper($elementOrFieldset));
            } elseif ($elementOrFieldset instanceof ElementInterface) {
                $targetClasses = $elementOrFieldset->getAttribute('class');
                $elementClasses = $element->getAttribute('class');
                $elementOrFieldset->setAttribute('class', trim($elementClasses . ' ' . $targetClasses));
                $markup .= sprintf($delMarkup, $elementHelper($elementOrFieldset));
            }
        }

        $markup = sprintf('<input type="hidden" class="reset" name="%s[0]" />
        <div class="repeating %s">
        <div class="col-sm-11" data-count="%s" data-placeholder="%s">%s%s</div>
        %s
        </div>',
            $element->getName(),
            $rowClass,
            $cnt,
            $element->getTemplatePlaceholder(),
            $templateMarkup,
            $markup,
            $addMarkup
        );

        return $markup;
    }

    /**
     * Only render a template
     *
     * @param  CollectionElement $collection
     * @param  string
     * @return string
     */
    public function renderTemplate(CollectionElement $collection, $wrap = '%s')
    {
        $elementHelper = $this->getElementHelper();
        $escapeHtmlAttribHelper = $this->getEscapeHtmlAttrHelper();
        $templateMarkup = '';

        $elementOrFieldset = $collection->getTemplateElement();
        if ($elementOrFieldset instanceof FieldsetInterface) {
            $templateMarkup .= sprintf($wrap, $this->render($elementOrFieldset));
        } elseif ($elementOrFieldset instanceof ElementInterface) {
            $templateMarkup .= sprintf($wrap, $elementHelper($elementOrFieldset));
        }

        return sprintf(
            '<span data-template="%s"></span>',
            $escapeHtmlAttribHelper($templateMarkup)
        );
    }

    /**
     * If set to true, collections are automatically wrapped around a fieldset
     *
     * @param  bool $wrap
     * @return FormArrayCollection
     */
    public function setShouldWrap($wrap)
    {
        $this->shouldWrap = (bool)$wrap;
        return $this;
    }

    /**
     * Get wrapped
     *
     * @return bool
     */
    public function shouldWrap()
    {
        return $this->shouldWrap;
    }

    /**
     * Sets the name of the view helper that should be used to render sub elements.
     *
     * @param  string $defaultSubHelper The name of the view helper to set.
     * @return FormArrayCollection
     */
    public function setDefaultElementHelper($defaultSubHelper)
    {
        $this->defaultElementHelper = $defaultSubHelper;
        return $this;
    }

    /**
     * Gets the name of the view helper that should be used to render sub elements.
     *
     * @return string
     */
    public function getDefaultElementHelper()
    {
        return $this->defaultElementHelper;
    }

    /**
     * Sets the element helper that should be used by this collection.
     *
     * @param  AbstractHelper $elementHelper The element helper to use.
     * @return FormArrayCollection
     */
    public function setElementHelper(AbstractHelper $elementHelper)
    {
        $this->elementHelper = $elementHelper;
        return $this;
    }

    /**
     * Retrieve the element helper.
     *
     * @return AbstractHelper
     * @throws RuntimeException
     */
    protected function getElementHelper()
    {
        if ($this->elementHelper) {
            return $this->elementHelper;
        }

        if (method_exists($this->view, 'plugin')) {
            $this->elementHelper = $this->view->plugin($this->getDefaultElementHelper());
        }

        if (!$this->elementHelper instanceof BaseAbstractHelper) {
            // @todo Ideally the helper should implement an interface.
            throw new RuntimeException('Invalid element helper set in FormCollection. The helper must be an instance of AbstractHelper.');
        }

        return $this->elementHelper;
    }

    /**
     * Sets the fieldset helper that should be used by this collection.
     *
     * @param  AbstractHelper $fieldsetHelper The fieldset helper to use.
     * @return FormArrayCollection
     */
    public function setFieldsetHelper(AbstractHelper $fieldsetHelper)
    {
        $this->fieldsetHelper = $fieldsetHelper;
        return $this;
    }

    /**
     * Retrieve the fieldset helper.
     *
     * @return FormArrayCollection
     */
    protected function getFieldsetHelper()
    {
        if ($this->fieldsetHelper) {
            return $this->fieldsetHelper;
        }

        return $this;
    }
}
