<?php
namespace Kotka\Form\View\Helper;


class TBConf
{
    const LABEL_CLASS = 'label_class';
    const ROW_WRAPPER = 'row_wrapper';
    const INPUT_NO_LABEL = 'no_label';
    const INPUT_WRAPPER = 'input_wrapper';
    const INPUT_CLASS = 'input_class';

    private static $classes = array(
        self::LABEL_CLASS => 'col-sm-3 control-label',
        self::ROW_WRAPPER => 'form-group',
        self::INPUT_WRAPPER => 'col-sm-5',
        self::INPUT_NO_LABEL => 'col-sm-offset-2 col-sm-5',
        self::INPUT_CLASS => 'form-control',
    );

    public static function get($name)
    {
        if (isset(self::$classes[$name])) {
            return self::$classes[$name];
        }
        return '';
    }

    public static function set($name, $value)
    {
        self::$classes[$name] = $value;
    }
} 