<?php
namespace Kotka\Form\View\Helper;

use Kotka\Form\Element\ArrayCollection;
use Zend\Form\Element\Button;
use Zend\Form\Element\Hidden;
use Zend\Form\ElementInterface;
use Zend\Form\Exception;
use Zend\Form\Fieldset;

class FormRow extends \Zend\Form\View\Helper\FormRow
{

    /**
     * The class that is added to element that have errors
     *
     * @var string
     */
    protected $inputErrorClass = 'has-error';


    protected $rowClass = null;

    protected $elementClass = null;

    protected $labelClass = null;

    /**
     * Markup for required elements
     *
     * @var string
     */
    protected $requiredHtml = '<em class="required"><i class="fa fa-asterisk"></i></em>';

    public function __invoke(ElementInterface $element = null, $labelPosition = null, $renderErrors = null, $partial = null, $rowClass = null, $elementClass = null, $labelClass = null)
    {
        if (!$element) {
            return $this;
        }
        $changed = array();
        if ($labelClass !== null) {
            $changed['LabelClass'] = $this->getLabelClass();
            $this->setLabelClass($labelClass);
        }
        if ($rowClass !== null) {
            $changed['RowClass'] = $this->getRowClass();
            $this->setRowClass($rowClass);
        }
        if ($elementClass !== null) {
            $changed['ElementClass'] = $this->getElementClass();
            $this->setElementClass($elementClass);
        }
        $html = parent::__invoke($element, $labelPosition, $renderErrors, $partial);
        if (count($changed)) {
            foreach ($changed as $key => $value) {
                $method = 'set' . $key;
                $this->$method($value);
            }
        }
        return $html;
    }

    /**
     * Utility form helper that renders a label (if it exists), an element and errors
     *
     * @param  ElementInterface $element
     * @param  null|string $labelPosition
     * @throws \Zend\Form\Exception\DomainException
     * @return string
     */
    public function render(ElementInterface $element, $labelPosition = null)
    {
        $escapeHtmlHelper = $this->getEscapeHtmlHelper();
        $labelHelper = $this->getLabelHelper();
        $elementHelper = $this->getElementHelper();
        $elementErrorsHelper = $this->getElementErrorsHelper();

        $label = $element->getLabel();
        $inputErrorClass = $this->getInputErrorClass();
        $rowWrapper = $this->rowClass === null ? TBConf::get(TBConf::ROW_WRAPPER) : $this->rowClass;
        $inputWrapper = $this->elementClass === null ? TBConf::get(TBConf::INPUT_WRAPPER) : $this->elementClass;
        $elementErrors = '';
        $isFieldset = $this->isFieldSet($element);

        $required = ($element->hasAttribute('required') ? $element->getAttribute('required') : false);
        $help = $element->getOption('help');

        if ($required) {
            $element->setAttribute('required', false);
        }
        if ($element instanceof Hidden) {
            $rowWrapper = '';
            $inputWrapper = '';
            $help = '';
            $label = '';
        }
        if ($element instanceof ArrayCollection) {
            $rowWrapper = $this->rowClass;
            if ($element->getTargetElement() instanceof Hidden) {
                $rowWrapper = '';
                $inputWrapper = '';
            }
        }

        // Does this element have errors ?
        if (count($element->getMessages()) > 0 && !empty($inputErrorClass)) {
            $classAttributes = ($element->hasAttribute('class') ? $element->getAttribute('class') . ' ' : '');
            $classAttributes = $classAttributes . $inputErrorClass;

            $element->setAttribute('class', $classAttributes);
        }

        if ($this->partial) {
            $vars = array(
                'element' => $element,
                'label' => $label,
                'labelAttributes' => $this->labelAttributes,
                'labelPosition' => $this->labelPosition,
                'renderErrors' => $this->renderErrors,
            );

            return $this->view->render($this->partial, $vars);
        }

        if ($this->renderErrors) {
            $elementErrors = $elementErrorsHelper->render($element, array(), TBConf::get(TBConf::INPUT_NO_LABEL));
            if (!empty($elementErrors) && !$isFieldset) {
                $rowWrapper .= ' ' . $this->inputErrorClass;
            }
        }

        $elementString = $elementHelper->render($element);
        $elementString = sprintf('<div %s>', $this->createAttributesString(array('class' => $inputWrapper)))
            . $elementString
            . ($isFieldset ? '' : $elementErrors)
            . '</div>';

        if (isset($label) && '' !== $label) {
            $label = $escapeHtmlHelper($label);

            $labelAttributes = $element->getLabelAttributes();

            if (empty($labelAttributes)) {
                $labelAttributes = $this->labelAttributes;
            }

            // Multicheckbox elements have to be handled differently as the HTML standard does not allow nested
            // labels. The semantic way is to group them inside a fieldset
            $type = $element->getAttribute('type');
            if ($type === 'multi_checkbox' || $type === 'radio') {
                $markup = sprintf(
                    '<fieldset><legend>%s</legend>%s</fieldset>',
                    $label,
                    $elementString);
            } else {
                if ($element->hasAttribute('id')) {
                    $labelOpen = '';
                    $labelClose = '';
                    $label = $labelHelper($element, null, null, $this->labelClass);
                } else {
                    $labelOpen = $labelHelper->openTag($labelAttributes, $this->labelClass);
                    $labelClose = $labelHelper->closeTag();
                }

                if ($label !== '' && !$element->hasAttribute('id')) {
                    $label = '<span>' . $label . '</span>';
                }

                if ($required) {
                    $elementString = $this->requiredHtml . $elementString;
                }

                if ($help) {
                    $elementString .= $this->help($help);
                }

                // Button element is a special case, because label is always rendered inside it
                if ($element instanceof Button) {
                    $labelOpen = $labelClose = $label = '';
                }

                switch ($this->labelPosition) {
                    case self::LABEL_PREPEND:
                        $markup = $labelOpen . $label . $labelClose . $elementString;
                        break;
                    case self::LABEL_APPEND:
                    default:
                        $markup = $elementString . $labelOpen . $label . $labelClose;
                        break;
                }
            }
        } else {
            $markup = $elementString;
        }
        if ($rowWrapper !== '') {
            $markup = sprintf('<div %s>', $this->createAttributesString(array('class' => $rowWrapper)))
                . $markup
                . '</div>';
        }


        return $markup;
    }

    /**
     * Return html used for rendering required field
     * @return string
     */
    public function required()
    {
        return $this->requiredHtml;
    }

    public function help($help)
    {
        if ($help instanceof ElementInterface) {
            $help = $help->getOption('help');
        }
        $attr = array(
            'class="form-help fa fa-question-circle hidden-xs"',
            'title="' . $help . '"',
            'data-toggle="tooltip"',
            'data-placement="right"',
            'data-container="body"'
        );
        return sprintf('<i %s></i>', implode(' ', $attr));
    }

    /**
     * @return string
     */
    public function getRequiredHtml()
    {
        return $this->requiredHtml;
    }

    /**
     * @param string $requiredHtml
     *
     * @return self
     */
    public function setRequiredHtml($requiredHtml)
    {
        $this->requiredHtml = $requiredHtml;

        return $this;
    }

    /**
     * @return string
     */
    public function getElementClass()
    {
        return $this->elementClass;
    }

    /**
     * @param string $elementClass
     *
     * @return self
     */
    public function setElementClass($elementClass)
    {
        TBConf::set(TBConf::INPUT_WRAPPER, $elementClass);
        $this->elementClass = $elementClass;

        return $this;
    }

    /**
     * @return string
     */
    public function getLabelClass()
    {
        return $this->labelClass;
    }

    /**
     * @param string $labelClass
     *
     * @return self
     */
    public function setLabelClass($labelClass)
    {
        TBConf::set(TBConf::LABEL_CLASS, $labelClass);
        $this->labelClass = $labelClass;

        return $this;
    }

    /**
     * @return string
     */
    public function getRowClass()
    {
        return $this->rowClass;
    }

    /**
     * @param string $rowClass
     *
     * @return self
     */
    public function setRowClass($rowClass)
    {
        $this->rowClass = $rowClass;

        return $this;
    }


    private function isFieldSet($element)
    {
        return $element instanceof Fieldset;
    }
}
