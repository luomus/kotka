<?php

namespace Kotka\Form;

use Kotka\Stdlib\Hydrator\Reflection;
use Kotka\Triple\HRBSpecimen as Specimen;
use Zend\Form\Fieldset;

/**
 * Class HRBSpecimen
 * @package Kotka\Form
 */
class HRBSpecimen extends Fieldset
{
    /**
     * Creates the form with all the necessary fields in it
     */
    public function init()
    {
        parent::init();
        $this
            ->setHydrator(new Reflection())
            ->setObject(new Specimen());
    }
}
