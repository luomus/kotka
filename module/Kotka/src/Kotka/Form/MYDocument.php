<?php
namespace Kotka\Form;

use Zend\Form\Form;
use Zend\Form\FormInterface;

/**
 * Class MYDocument
 * @package Kotka\Form
 */
class MYDocument extends WarningForm
{

    protected $type = 'Specimen';

    public function postInit()
    {
        /** @var \Zend\Form\Element\Collection $gatherings */
        $gatherings = $this->get('MYGathering');
        $target = $gatherings->getTargetElement();
        $target->postInit();
    }

    /**
     * Creates the form with all the necessary fields in it
     */
    public function init()
    {
        parent::init();

        $this->add(array(
            'name' => 'history',
            'type' => 'Kotka\Form\Element\History',
            'attributes' => array(
                'id' => 'history',
                'class' => 'no-change-tracker',
            ),
            'options' => array(
                'label' => 'History',
                'empty_option' => 'timestamps of changes...',
                'class' => 'MY.document',
                'exclude' => ['MY.inMustikka']
            )
        ));

        $this->add(array(
            'name' => 'MYNamespaceID',
            'type' => 'text',
            'attributes' => array(
                'id' => 'namespaceID',
                'class' => 'MYNamespaceID MYDocument',
                'placeholder' => 'Namespace ID',
            ),
            'options' => array(
                'label' => 'Namespace ID',
                'help' => "Namespace of the existing ID, if the specimen has one already. E.g. for 'GZ.101' it is 'GZ'."
            )
        ));

        $this->add(array(
            'name' => 'MYObjectID',
            'type' => 'text',
            'attributes' => array(
                'id' => '_objectID',
                'class' => 'MYObjectID MYDocument',
                'placeholder' => 'Object ID'
            ),
            'options' => array(
                'label' => 'Object ID',
                'help' => "ID of the specimen, if it has one already. E.g. for 'GZ.101' it is '101'."
            )
        ));

        $this->add(array(
            'name' => 'MZDateEdited',
            'type' => 'hidden',
            'attributes' => array(
                'class' => 'MZDateEdited MYDocument',
            ),
        ));

        $this->add(array(
            'name' => 'MYProjectId',
            'type' => 'ArrayCollection',
            'attributes' => array(
                'id' => 'additionalIDs'
            ),
            'options' => array(
                'count' => 1,
                'should_create_template' => false,
                'allow_add' => true,
                'allow_remove' => true,
                'target_element' => array(
                    'name' => 'MYProjectId',
                    'type' => 'hidden',
                )
            ),
        ));
    }

    /**
     * Sets the data and populates the form.
     *
     * If there is data filters that are ran when this method is called.
     * You can prevent filter running by setting the second parameter to false
     * or by setting the filters to null using setDataFilter method
     *
     * @param array|\ArrayAccess|\Traversable $data
     * @param boolean $filter
     * @return Form|FormInterface
     */
    public function setData($data, $filter = true)
    {
        if ($filter) {
            $data = $this->getInputFilter()->preFilter($data);
        }

        return parent::setData($data);
    }

    /**
     * @return string
     */
    public function getType()
    {
        return \Kotka\InputFilter\MYDocument::getType();
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        \Kotka\InputFilter\MYDocument::setType($type);
    }
}
