<?php

namespace Kotka\Form;

use Kotka\Stdlib\Hydrator\Reflection;
use Kotka\Triple\MYGathering as Gathering;
use Zend\Form\Fieldset;

/**
 * Class Gathering
 * @package Kotka\Form
 */
class MYGathering extends Fieldset
{

    /**
     * Creates the form with all the necessary fields in it
     */
    public function init()
    {
        parent::init();
        $this
            ->setHydrator(new Reflection())
            ->setObject(new Gathering());
    }

    public function postInit()
    {
        $this->remove('subject');
        $unit = $this->get('MYUnit');
        $target = $unit->getTargetElement();
        $target->postInit();
    }
}
