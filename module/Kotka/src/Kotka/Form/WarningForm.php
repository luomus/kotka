<?php

namespace Kotka\Form;

use Zend\Filter\FilterInterface;
use Zend\Form\Element;
use Zend\Form\ElementInterface;
use Zend\Form\Exception;
use Zend\Form\Fieldset;
use Zend\Form\FieldsetInterface;
use Zend\Form\Form;
use Zend\Form\FormInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\Stdlib\ArrayUtils;

/**
 * Class WarningForm is a form that can handle warning messages
 * @package Kotka\Form
 */
class WarningForm extends Form
{
    protected $dataFilter;
    protected $warningFilter;
    protected $warningMessages = array();
    protected $bindOnErrors = false;

    public function getMessages($elementName = null)
    {
        if ($elementName === null) {
            $formMessages = parent::getMessages();
            $filterMessages = $this->getInputFilter()->getMessages();
            return array_replace_recursive($filterMessages, $formMessages);
        }
        return parent::getMessages($elementName);
    }

    /**
     * Set the input filter used by this form
     *
     * @param  InputFilterInterface $inputFilter
     * @return FormInterface
     */
    public function setInputWarningFilter(InputFilterInterface $inputFilter = null)
    {
        $this->warningFilter = $inputFilter;
        return $this;
    }

    /**
     * Retrieve input warning filter used by this form
     *
     * @return null|InputFilterInterface
     */
    public function getInputWarningFilter()
    {
        return $this->warningFilter;
    }

    /**
     * Sets the filter that is ran when setData method is called
     *
     * @param FilterInterface $filter
     * @return $this
     */
    public function setDataFilter(FilterInterface $filter = null)
    {
        $this->dataFilter = $filter;

        return $this;
    }

    /**
     * Return the filter used when calling setData method on the form
     *
     * @return FilterInterface|null
     */
    public function getDataFilter()
    {
        return $this->dataFilter;
    }

    /**
     * Set a hash of element names/messages to use when warning validation fails
     *
     * @param  array|\Traversable $messages
     * @return Element|ElementInterface|FieldsetInterface
     * @throws Exception\InvalidArgumentException
     */
    public function setWarningMessages($messages)
    {
        if (!is_array($messages) && !$messages instanceof \Traversable) {
            throw new Exception\InvalidArgumentException(sprintf(
                '%s expects an array or Traversable object of messages; received "%s"',
                __METHOD__,
                (is_object($messages) ? get_class($messages) : gettype($messages))
            ));
        }

        $msg = array();
        foreach ($messages as $key => $messageSet) {
            if ($this->has($key)) {
                $msg[$key] = $messageSet;
            }
        }
        $this->warningMessages = $msg;

        return $this;
    }

    /**
     * Get validation warning messages, if any
     *
     * Returns a hash of element names/messages for all elements failing
     * validation, or, if $elementName is provided, messages for that element
     * only.
     *
     * @param  null|string $elementName
     * @return array|\Traversable
     * @throws Exception\InvalidArgumentException
     */
    public function getWarningMessages($elementName = null)
    {
        if (null === $elementName) {
            return $this->warningMessages;
        }

        if (!$this->has($elementName)) {
            throw new Exception\InvalidArgumentException(sprintf(
                'Invalid element name "%s" provided to %s',
                $elementName,
                __METHOD__
            ));
        }

        if (in_array($elementName, $this->warningMessages)) {
            return $this->warningMessages[$elementName];
        }
        return array();

    }

    public function hasWarnings()
    {
        return count($this->warningMessages) > 0;
    }

    public function prepareValidationGroup(FieldsetInterface $formOrFieldset, array $data, array &$validationGroup)
    {
        // Data in the validation group is already organized so that it can be used
        // Default behaviour would overwrite the values of children in case
        // input data would have different fields in different children.
    }

    public function isValid()
    {
        $result = parent::isValid();
        $warningFilter = $this->getInputWarningFilter();
        if ($warningFilter === null) {
            return $result;
        }
        $warningFilter->setData($this->data);
        $warningFilter->setValidationGroup(InputFilterInterface::VALIDATE_ALL);

        $warnings = $warningFilter->isValid();
        if (!$warnings) {
            $messages = $this->array_filter_recursive($warningFilter->getMessages());
            $this->setWarningMessages($messages);
        }

        return $result;
    }

    public function reset()
    {
        $messages = $this->getInputFilter()->getMessages();
        if (!empty($messages)) {
            $this->resetElementMessages($this, $messages);
        }
        $warningFilter = $this->getInputWarningFilter();
        if ($warningFilter === null) {
            return;
        }
        $this->setWarningMessages(array());
    }

    public function setBindOnErrors($bool)
    {
        $this->bindOnErrors = (bool)$bool;
    }

    private function resetElementMessages(Fieldset $fieldset, $messages)
    {
        foreach ($messages as $key => $value) {
            if ($fieldset->has($key)) {
                $elem = $fieldset->get($key);
                $elem->setMessages(array());
                if ($elem instanceof Fieldset) {
                    $this->resetElementMessages($elem, $value);
                }
            }
        }
    }

    /**
     * Bind values to the bound object
     *
     * @param array $values
     * @return mixed
     */
    public function bindValues(array $values = array())
    {
        $original = $this->isValid;
        if ($this->bindOnErrors) {
            $this->isValid = true;
        }
        parent::bindValues($values);
        if ($this->bindOnErrors) {
            $this->isValid = $original;
        }
    }

    public function postInit()
    {
    }

    private function array_filter_recursive($input)
    {
        foreach ($input as &$value) {
            if (is_array($value)) {
                $value = $this->array_filter_recursive($value);
            }
        }
        return array_filter($input);
    }

    public function getLastName($name)
    {
        $parts = explode('[', $name);
        return str_replace(']', '', array_pop($parts));
    }

}
