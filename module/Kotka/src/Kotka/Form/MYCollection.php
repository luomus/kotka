<?php

namespace Kotka\Form;

use Zend\InputFilter\InputFilterProviderInterface;

/**
 * Class MYCollection
 * @package Kotka\Form
 */
class MYCollection extends WarningForm implements InputFilterProviderInterface
{

    /**
     * Should return an array specification compatible with
     * {@link Zend\InputFilter\Factory::createInputFilter()}.
     *
     * @return array
     */
    public function getInputFilterSpecification()
    {
        return array(
            'MZOwner' => array(
                'required' => true,
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\NotEmpty::IS_EMPTY => 'You have to specify the owner of the record',
                            ),
                        ),
                        'break_chain_on_failure' => true
                    ),
                    array(
                        'name' => 'UserInOrganization',
                    )
                )
            )
        );
    }
}
