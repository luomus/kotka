<?php

namespace Kotka\Form;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class Search
 * @package Kotka\Form
 */
class Search extends ImportPrint implements ServiceLocatorAwareInterface, InputFilterAwareInterface
{

    private $serviceLocator;
    private $formElementService;

    protected $inputFilter;

    /**
     * Creates the form with all the necessary fields in it
     */
    public function init()
    {
        parent::__construct('form_defaultGrid');
        parent::init();
        $this->setAttribute('class', 'form-horizontal');

        $this->add(array(
            'name' => 'identifier',
            'type' => 'textarea',
            'options' => array(
                'label' => 'Identifier',
            ),
            'attributes' => array(
                'class' => 'form-control',
                'style' => 'resize: none;',
                'placeholder' => 'identifier: eg. HT.30-33 or HT.31,HT.42,HT.50',
                'rows' => 1,
                'id' => 'identifier',
                'tabindex' => 1
            ),
        ));

        $this->add(array(
            'name' => 'identifierAsRange',
            'type' => 'checkbox',
            'options' => array(
                'label' => 'Identifier as range',
                'help' => "If this checkbox is checked then Kotka tries to interpret the identifier search field as a range. If you want to do a range search with an object id containing non-numeric characters omit all non-numeric characters. For example if you want to search between id range JA.HA12-13 and JA.HA12-17 and you would need to write it in this format JA.1213-1217"
            ),
            'attributes' => array(
                'value' => true
            )
        ));

        $this->add(array(
            'name' => 'q',
            'type' => 'textarea',
            'options' => array(
                'label' => 'Search',
            ),
            'attributes' => array(
                'rows' => 1,
                'style' => 'resize: none;',
                'class' => 'form-control',
                'placeholder' => 'search terms',
                'id' => 'search',
                'tabindex' => 2
            ),
        ));

        $this->add(array(
            'name' => 'accepted',
            'type' => 'checkbox',
            'options' => array(
                'label' => 'Only accepted',
                'help' => "Search only latest or preferred identifications"
            ),
            'attributes' => array(
                'value' => true
            )
        ));

        $this->add(array(
            'name' => 'primary',
            'type' => 'checkbox',
            'options' => array(
                'label' => 'Only primary specimens'
            )
        ));

        $this->add(array(
            'name' => 'pic',
            'type' => 'checkbox',
            'options' => array(
                'label' => 'Only with picture'
            )
        ));

        $this->add(array(
            'name' => 'types',
            'type' => 'checkbox',
            'options' => array(
                'label' => 'Only type specimens'
            )
        ));

        $this->add(array(
            'name' => 'loc',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 'loc',
            )
        ));

        $this->add(array(
            'name' => 'perPage',
            'type' => 'select',
            'options' => array(
                'value_options' => array(
                    10 => 10,
                    25 => 25,
                    50 => 50,
                    100 => 100,
                ),
            ),
        ));

        $this->add(array(
            'name' => 'perPage',
            'type' => 'hidden'
        ));

        $this->add(array(
            'name' => 'page',
            'type' => 'hidden'
        ));

        $this->add(array(
            'name' => 'sortBy',
            'type' => 'hidden'
        ));

        $this->add(array(
            'name' => 'sortDirection',
            'type' => 'hidden'
        ));

        $this->add(array(
            'name' => 'search',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Search',
                'class' => 'btn btn-primary btn-block'
            )
        ));
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $inputFilter->add(array(
                'name' => 'identifier',
                'required' => false,
                'filters' => array(
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'Kotka\Validator\SearchIdentifier',
                    ),
                ),
            ));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    protected function getFieldOptions($field)
    {
        if ($this->formElementService === null) {
            $this->formElementService = $this->serviceLocator->getServiceLocator()->get('Kotka\Service\FormElementService');
        }

        return $this->formElementService->getSelect($field);
    }

    /**
     * Set service locator
     *
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    /**
     * Get service locator
     *
     * @return ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }


}
