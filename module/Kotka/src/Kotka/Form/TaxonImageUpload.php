<?php

namespace Kotka\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilterProviderInterface;

/**
 * Class TaxonImageUpload is the form used to upload multiple image
 * @package Kotka\Form
 */
class TaxonImageUpload extends Form implements InputFilterProviderInterface
{
    const MAX_FILE_SIZE = '350MB';
    const ANY_FILE_FORMAT = '(.*)';

    /**
     * Creates the form with all the necessary fields in it
     */
    public function init()
    {
        parent::__construct('imageUpload');
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'form-horizontal');

        $this->add(array(
            'name' => 'format',
            'type' => 'Select',
            'options' => array(
                'label' => 'Filename format',
                'value_options' => array(
                    self::ANY_FILE_FORMAT => 'anything.extension',
                )
            ),
        ));

        $this->add(array(
            'name' => 'images',
            'type' => 'file',
            'options' => array(
                'label' => 'Files',
            ),
            'attributes' => array(
                'multiple' => true,
                'id' => 'images',
                'class' => 'form-control'
            )
        ));
    }

    /**
     * Should return an array specification compatible with
     * {@link Zend\InputFilter\Factory::createInputFilter()}.
     *
     * @return array
     */
    public function getInputFilterSpecification()
    {
        return array(
            'images' => array(
                'required' => true,
                'validators' => array(
                    array(
                        'name' => 'Zend\Validator\File\Size',
                        'options' => array(
                            'max' => self::MAX_FILE_SIZE,
                            'messages' => array(
                                \Zend\Validator\File\Size::TOO_BIG => "Maximum allowed size for file is '%max%' but '%size%' detected."
                            )
                        )
                    ),
                    array(
                        'name' => 'Zend\Validator\File\IsImage',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\File\IsImage::FALSE_TYPE => "Only images please. Now '%type%' detected"
                            )
                        )
                    )
                )
            )
        );
    }
}
