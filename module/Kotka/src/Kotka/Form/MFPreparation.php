<?php
namespace Kotka\Form;

use Kotka\Stdlib\Hydrator\Reflection;
use Kotka\Triple\MFPreparation as Preparation;
use Zend\Form\Fieldset;

/**
 * Class MFPreparation
 * @package Kotka\Form
 */
class MFPreparation extends Fieldset
{

    /**
     * Creates the form with all the necessary fields in it
     */
    public function init()
    {
        parent::init();
        $this
            ->setHydrator(new Reflection())
            ->setObject(new Preparation());
    }

    public function postInit()
    {
        $this->remove('subject');
    }
}