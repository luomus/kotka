<?php

namespace Kotka\Form;

use Kotka\InputFilter\ImportExcelFilter;
use Kotka\Service\ImportService;
use Zend\Form\Form;

/**
 * Class ImportExcel is the form used in tools > import
 * @package Kotka\Form
 */
class ImportExcel extends Form
{
    /**
     * Creates the form with all the necessary fields in it
     */
    public function init()
    {
        parent::__construct('importExcel');
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'form-horizontal');

        $this->setInputFilter(new ImportExcelFilter());

        $datatypes = [
            'zoospecimen' => 'Zoological specimens',
            'botanyspecimen' => 'Botanical specimens',
            'culture' => 'Cultures',
            'accession' => 'Accession',
            'palaeontology' => 'Palaeontology'
        ];

        $this->add(array(
            'name' => 'datatype',
            'type' => 'select',
            'attributes' => array(
                'id' => 'datatype',
                'required' => true
            ),
            'options' => array(
                'label' => 'Datatype',
                'empty_option' => 'Select',
                'value_options' => $datatypes
            ),
        ),[
            'priority' => 10
        ]);

        $this->add(array(
            'name' => 'organization',
            'type' => 'Kotka\Form\Element\OwnerOrganization',
            'options' => array(
                'label' => 'Owner of records',
                'empty_option' => 'Select'
            ),
            'attributes' => array(
                'id' => 'organization',
                'required' => true
            )
        ),[
            'priority' => 9
        ]);

        $this->add(array(
            'name' => 'import',
            'type' => 'file',
            'options' => array(
                'template' => 'kotka/partial/element/file',
                'label' => 'Browse for file'
            ),
            'attributes' => array(
                'required' => true
            )
        ),[
            'priority' => 7
        ]);

        $this->add(array(
            'name' => 'huge',
            'type' => 'checkbox',
            'options' => array(
                'label' => 'Huge import (MAX ' . ImportService::MAX_HUGE_ROW_SIZE . ' skips preview)'
            ),
            'attributes' => [
                'style' => 'margin-top:10px;'
            ]
        ),[
            'priority' => 5
        ]);
    }

    public function addAdvancedFields() {
        $this->add(array(
            'name' => 'override',
            'type' => 'checkbox',
            'options' => array(
                'label' => 'Override user data using columns in excel'
            ),
            'attributes' => [
                'style' => 'margin-top:10px;'
            ]
        ),[
            'priority' => 6
        ]);
    }

}
