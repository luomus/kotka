<?php

namespace Kotka\Form;

use Kotka\Stdlib\Hydrator\Reflection;
use Kotka\Stdlib\Hydrator\Strategy\DateTimeStrategy;
use Kotka\Triple\PUUEvent as Event;
use Zend\Form\Form;
use Zend\InputFilter\InputFilterProviderInterface;

/**
 * Class PUUEvent
 * @package Kotka\Form
 */
class PUUEvent extends Form implements InputFilterProviderInterface
{

    /**
     * Creates the form with all the necessary fields in it
     */
    public function init()
    {
        $this
            ->setHydrator(new Reflection())
            ->setObject(new Event());
    }

    public function postInit()
    {
        $this->remove('subject');
        $dateStrategy = new DateTimeStrategy();
        $dateStrategy->setOptions(array(
            'format' => 'Y-m-d'
        ));
        $this->hydrator->addStrategy('PUUDate', $dateStrategy);
    }

    /**
     * Should return an array specification compatible with
     * {@link Zend\InputFilter\Factory::createInputFilter()}.
     *
     * @return array
     */
    public function getInputFilterSpecification()
    {
        return array(
            'PUUDate' => array(
                'required' => false,
                'filters' => array(
                    array(
                        'name' => 'Null',
                    )
                ),
                'validators' => array(
                    array(
                        'name' => 'date',
                        'break_chain_on_failure' => true,
                        'options' => array(
                            'format' => 'Y-m-d',
                        )
                    )
                )
            ),
            'PUUEstimatedSeedQuantity' => array(
                'required' => false,
                'validators' => array(
                    array(
                        'name' => 'Digits',
                        'break_chain_on_failure' => true,
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\Digits::NOT_DIGITS => 'Quantity must be a whole number (integer).'
                            )
                        )
                    )
                )
            ),
            'PUUNumberOfSeedsTested' => array(
                'required' => false,
                'validators' => array(
                    array(
                        'name' => 'Digits',
                        'break_chain_on_failure' => true,
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\Digits::NOT_DIGITS => 'Number seed tested must be a whole number (integer).'
                            )
                        )
                    )
                )
            ),
            'PUUNumberOfSeedsInfested' => array(
                'required' => false,
                'validators' => array(
                    array(
                        'name' => 'Digits',
                        'break_chain_on_failure' => true,
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\Digits::NOT_DIGITS => 'Number seed infested must be a whole number (integer).'
                            )
                        )
                    )
                )
            ),
            'PUUNumberOfSeedsMouldy' => array(
                'required' => false,
                'validators' => array(
                    array(
                        'name' => 'Digits',
                        'break_chain_on_failure' => true,
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\Digits::NOT_DIGITS => 'Number seed mouldy must be a whole number (integer).'
                            )
                        )
                    )
                )
            ),
            'PUUTestPassed' => array(
                'required' => false,
            ),
            'PUUPercentageSeedsFull' => array(
                'required' => false,
                'validators' => array(
                    array(
                        'name' => 'Digits',
                        'break_chain_on_failure' => true,
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\Digits::NOT_DIGITS => 'Percentage seeds full must be a whole number (integer).'
                            )
                        )
                    )
                )
            ),
            'PUUNumberOfSeedsFull' => array(
                'required' => false,
                'validators' => array(
                    array(
                        'name' => 'Digits',
                        'break_chain_on_failure' => true,
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\Digits::NOT_DIGITS => 'Number seed full must be a whole number (integer).'
                            )
                        )
                    )
                )
            ),
            'PUUNumberOfSeedsPartFull' => array(
                'required' => false,
                'validators' => array(
                    array(
                        'name' => 'Digits',
                        'break_chain_on_failure' => true,
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\Digits::NOT_DIGITS => 'Number seed part-full must be a whole number (integer).'
                            )
                        )
                    )
                )
            ),
            'PUUNumberOfSeedsEmpty' => array(
                'required' => false,
                'validators' => array(
                    array(
                        'name' => 'Digits',
                        'break_chain_on_failure' => true,
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\Digits::NOT_DIGITS => 'Number seeds empty must be a whole number (integer).'
                            )
                        )
                    )
                )
            ),
            'PUUViabilityPercentage' => array(
                'required' => false,
                'validators' => array(
                    array(
                        'name' => 'Between',
                        'break_chain_on_failure' => true,
                        'options' => array(
                            'min' => 0,
                            'max' => 100,
                            'messages' => array(
                                \Zend\Validator\Between::NOT_BETWEEN => 'Viability percentage must be between 0...100.'
                            )
                        )
                    ),
                    array(
                        'name' => 'Digits',
                        'break_chain_on_failure' => true,
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\Digits::NOT_DIGITS => 'Viability percentage must be a whole number (integer).'
                            )
                        )
                    )
                )
            ),
            'PUUGerminationPercentage' => array(
                'required' => false,
                'validators' => array(
                    array(
                        'name' => 'Between',
                        'break_chain_on_failure' => true,
                        'options' => array(
                            'min' => 0,
                            'max' => 100,
                            'messages' => array(
                                \Zend\Validator\Between::NOT_BETWEEN => 'Germination percentage must be between 0...100.'
                            )
                        )
                    ),
                    array(
                        'name' => 'Digits',
                        'break_chain_on_failure' => true,
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\Digits::NOT_DIGITS => 'Germination percentage must be whole number (integer).'
                            )
                        )
                    )
                )
            ),
            'PUUBranchID' => array(
                'require' => true
            )
        );
    }
}
