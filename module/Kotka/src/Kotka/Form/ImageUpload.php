<?php

namespace Kotka\Form;

use ImageApi\Options\ImageServerConfiguration;
use Kotka\Validator\ImagesPath;
use Zend\Form\Form;
use Zend\InputFilter\InputFilterProviderInterface;

/**
 * Class ImagesUpload is the form used to upload multiple image
 * @package Kotka\Form
 */
class ImageUpload extends Form implements InputFilterProviderInterface
{
    const MAX_FILE_COUNT = 100;
    const MAX_FILE_SIZE = '350MB';
    const DEFAULT_FILE_FORMAT = '(MZH_)?(?P<documentId>.*?)(_.*\..*|\.[^\.]*)';

    /**
     * Creates the form with all the necessary fields in it
     */
    public function init()
    {
        parent::__construct('imageUpload');
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'form-horizontal');

        $this->add(array(
            'name' => 'format',
            'type' => 'Select',
            'options' => array(
                'label' => 'Filename format',
                'value_options' => array(
                    self::DEFAULT_FILE_FORMAT => '[identifier]_anything.extension',
                )
            ),
        ));

        $this->add(array(
            'name' => 'path',
            'type' => 'text',
            'options' => array(
                'label' => 'Directory path',
                'help' => 'Path to network directory where the files are located'
            ),
            'attributes' => array(
                'placeholder' => ImagesPath::IMAGE_USER_PATH . 'SUBFOLDER'
            )
        ));

        $this->add(array(
            'name' => 'images',
            'type' => 'file',
            'options' => array(
                'label' => 'Files',
            ),
            'attributes' => array(
                'multiple' => true,
                'id' => 'images',
                'class' => 'form-control'
            )
        ));
    }

    /**
     * Should return an array specification compatible with
     * {@link Zend\InputFilter\Factory::createInputFilter()}.
     *
     * @return array
     */
    public function getInputFilterSpecification()
    {
        return array(
            'path' => array(
                'required' => true,
                'continue_if_empty' => true,
                'validators' => array(
                    array(
                        'name' => 'Kotka\Validator\ImagesPath'
                    ),
                    array(
                        'name' => 'BothNotSet',
                        'options' => array(
                            'hasNot' => 'images'
                        )
                    ),
                    array(
                        'name' => 'OneOrTheOther',
                        'options' => array(
                            'other' => 'images'
                        )
                    ),
                    array(
                        'name' => 'Kotka\Validator\File\ImageName'
                    )
                )
            ),
            'images' => array(
                'required' => false,
                'validators' => array(
                    array(
                        'name' => 'Zend\Validator\File\Size',
                        'options' => array(
                            'max' => self::MAX_FILE_SIZE,
                            'messages' => array(
                                \Zend\Validator\File\Size::TOO_BIG => "Maximum allowed size for file is '%max%' but '%size%' detected."
                            )
                        )
                    ),
                    array(
                        'name' => 'Zend\Validator\File\IsImage',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\File\IsImage::FALSE_TYPE => "Only images please. Now '%type%' detected"
                            )
                        )
                    ),
                    array(
                        'name' => 'Kotka\Validator\File\ImageName'
                    ),
                    /*
                    array(
                        'name' => 'Zend\Validator\File\Count', // This validator is not working at the moment!
                        'options' => array(
                            'max' => self::MAX_FILE_COUNT,
                            'messages' => array(
                                \Zend\Validator\File\Count::TOO_MANY => "Too many images, maximum '%max%' are allowed. Please use path option to upload your images or upload them in smaller sets."
                            )
                        )
                    )
                    */
                )
            )
        );
    }
}
