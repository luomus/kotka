<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/zf2 for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Kotka\Form\Element;

use Admin\Stdlib\Hydrator\ArrayHydrator;
use Traversable;
use Zend\Form\Element;
use Zend\Form\Exception;
use Zend\Stdlib\ArrayUtils;

/**
 * Class ArrayCollection is for zf > 2.3 to handles simple collections in forms.
 * For example array collections
 *
 * @package Kotka\Form\Element
 */
class ArrayCollection extends Element\Collection
{
    public function extract()
    {
        if ($this->object instanceof Traversable) {
            $this->object = ArrayUtils::iteratorToArray($this->object);
        }

        return $this->object;
    }

    public function populateValues($data)
    {
        if (!is_array($data)) {
            $data = array($data);
        }
        parent::populateValues($data);
    }

}