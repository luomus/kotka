<?php

namespace Kotka\Form\Element;

use DateTime;
use Traversable;
use Zend\Form\ElementInterface;
use Zend\Form\Exception;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Stdlib\ArrayUtils;


/**
 * Class History
 * @package Kotka\Form\Element
 */
class History extends Select implements ServiceLocatorAwareInterface
{
    private $serviceLocator;

    protected $field;

    protected $options = [
        'class' => null,
        'exclude' => null,
    ];

    /**
     *
     * Set options for an element. Accepted options are:
     * - label: label to associate with the element
     * - label_attributes: attributes to use when the label is rendered
     * _ empty_option: should an empty option be prepended to the options
     *
     * @param array|Traversable $options
     * @return void|\Zend\Form\Element\Select|ElementInterface
     * @throws \Zend\Form\Exception\InvalidArgumentException
     */
    public function setOptions($options)
    {
        if ($options instanceof Traversable) {
            $options = ArrayUtils::iteratorToArray($options);
        } elseif (!is_array($options)) {
            throw new Exception\InvalidArgumentException(
                'The options parameter must be an array or a Traversable'
            );
        }
        if (array_key_exists('value_options', $options)) {
            unset($options['value_options']);
        }

        parent::setOptions($options);
    }

    public function setClass($class)
    {
        $this->options['class'] = $class;
    }

    public function getClass()
    {
        return $this->options['class'];
    }

    public function setExclude(array $exclude)
    {
        $this->options['exclude'] = $exclude;
    }

    public function getExclude()
    {
        return $this->options['exclude'];
    }

    public function setValueOptions(array $options)
    {
        $class = $this->options['class'];
        $exclude = $this->options['exclude'];
        if (isset($options['class'])) {
            $class = $options['class'];
        }
        if (isset($options['exclude'])) {
            $exclude = $options['exclude'];
        }
        if (isset($options['subject'])) {
            /** @var \Triplestore\Service\ObjectManager $om */
            $om = $this->getServiceLocator()->getServiceLocator()->get('Triplestore\ObjectManager');
            $options = $om->getHistoryVersions($options['subject'], $class, $exclude);
            arsort($options);
            foreach ($options as $key => $value) {
                $date = DateTime::createFromFormat('YmdHis', $value);
                $options[$key] = $date->format('Y-m-d H:i:s');
            }
            parent::setValueOptions($options);
        }
    }

    /**
     * Sets the service locator
     * @param ServiceLocatorInterface $sl
     */
    public function setServiceLocator(ServiceLocatorInterface $sl)
    {
        $this->serviceLocator = $sl;
    }

    /**
     * Gets the service locator
     * @return ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

}
