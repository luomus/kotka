<?php

namespace Kotka\Form\Element;

use Common\Service\IdService;
use Zend\Form\Element\Text;

class Resource extends Text
{

    public function setValue($value)
    {
        if (!empty($value)) {
            $value = IdService::getUri($value);
        }
        parent::setValue($value);
    }
}