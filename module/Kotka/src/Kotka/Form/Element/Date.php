<?php

namespace Kotka\Form\Element;

use Zend\Form\Element\Date as ZendDate;

class Date extends ZendDate
{

    /**
     * Provide default input rules for this element
     *
     * Attaches default validators for the datetime input.
     *
     * @return array
     */
    public function getInputSpecification()
    {
        return array(
            'name' => $this->getName(),
            'required' => false,
            'filters' => array(
                array('name' => 'Zend\Filter\StringTrim'),
            ),
        );
    }
}