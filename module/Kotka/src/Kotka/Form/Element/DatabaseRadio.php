<?php

namespace Kotka\Form\Element;

use Kotka\Service\FormElementService;
use Traversable;
use Zend\Form\Element\Radio as ZendRadio;
use Zend\Form\ElementInterface;
use Zend\Form\Exception;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Stdlib\ArrayUtils;


/**
 * Class DatabaseRadio
 * @package Kotka\Form\Element
 */
class DatabaseRadio extends ZendRadio implements ServiceLocatorAwareInterface
{
    private $serviceLocator;

    protected $field;

    protected $alias = [];

    /**
     *
     * Set options for an element. Accepted options are:
     * - label: label to associate with the element
     * - label_attributes: attributes to use when the label is rendered
     * - value_options: list of values and labels for the radio options
     * _ empty_option: should an empty option be prepended to the options
     * _ field: name of the field you want to get from the database (matches the subject in the triplestore)
     *
     * @param array|Traversable $options
     * @return void|\Zend\Form\Element\Select|ElementInterface
     * @throws \Zend\Form\Exception\InvalidArgumentException
     */
    public function setOptions($options)
    {
        if ($options instanceof Traversable) {
            $options = ArrayUtils::iteratorToArray($options);
        } elseif (!is_array($options)) {
            throw new Exception\InvalidArgumentException(
                'The options parameter must be an array or a Traversable'
            );
        }

        if (isset($options['field'])) {
            $this->setField($options['field']);
        }
        if (!isset($options['value_options']) && !isset($this->options['value_options'])) {
            /** @var \Kotka\Service\FormElementService $elemSerice */
            $elemService = $this->serviceLocator->getServiceLocator()->get('Kotka\Service\FormElementService');
            $valueOptions = $elemService->getSelect($this->field);
            if (isset($valueOptions[FormElementService::ALIAS_KEY])) {
                $this->alias = $valueOptions[FormElementService::ALIAS_KEY];
                unset($valueOptions[FormElementService::ALIAS_KEY]);
            }
            $options['value_options'] = $valueOptions;
        }

        parent::setOptions($options);
    }

    public function getField()
    {
        return $this->field;
    }

    public function setField($field)
    {
        $this->field = $field;

        return $this;
    }

    /**
     * Sets the service locator
     * @param ServiceLocatorInterface $sl
     */
    public function setServiceLocator(ServiceLocatorInterface $sl)
    {
        $this->serviceLocator = $sl;
    }

    /**
     * Gets the service locator
     * @return ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

}
