<?php

namespace Kotka\Form\Element;


class TaxonRank extends DatabaseSelect
{

    const REMOVE_AFTER = 'MX.species';

    public static $include = [
        'MX.hybrid' => true,
        'MX.group' => true,
        'MX.infragenericHybrid' => true,
        'MX.intergenericHybrid' => true,
        'MX.nothospecies' => true,
        'MX.grex' => true,
    ];

    public function setOptions($options)
    {
        parent::setOptions($options, true);
        $remove = true;
        foreach ($this->valueOptions as $key => $value) {
            if ($key === self::REMOVE_AFTER) {
                $remove = false;
            }
            if ($remove && !isset(self::$include[$key])) {
                $this->unsetValueOption($key);
                continue;
            }
        }
    }

} 