<?php

namespace Kotka\Form\Element;

use Zend\Form\Element\Select as ZendSelect;

/**
 * Class DisplayFormat
 * @package Kotka\Form\Element
 */
class LabelFormat extends ZendSelect
{
    /**
     * Makes the display format select
     */
    public function __construct()
    {
        parent::__construct('display_format');
        $this->setValueOptions(array(
            'labelDesigner' => 'Label Designer',
            'bryophyte' => 'bryophytes',
            'vascular' => 'vascular plants',
            //'insecta' => 'insecta',
            'insecta_v2' => 'insecta version 2',
            //'insecta_det' => 'insecta det',
            'insecta_det_v2' => 'insecta det version 2',
            'minimal' => 'minimal',
            'herma' => 'HERMA 4607',
            'temporary' => 'temporary',
            'kuopio_botanical' => 'Kuopio botanical',
            'invertebrate_box' => 'Invertebrate box',
            'invertebrate_jar' => 'Invertebrate jar',
            'invertebrate_tube' => 'Invertebrate tube',
            'invertebrate_tube_qr' => 'Invertebrate tube QR',
            'vertebrate_1' => 'Vertebrate collection',
            'vertebrate_2' => 'Vertebrate entry',
            'qr_taxon' => 'QR + taxon',
            'slide' => 'Slide',
            'bryophyte_v2' => 'bryophytes version 2',
            'bryophyte_v3' => 'bryophytes version 3',
            'oulu_fungi' => 'Oulu fungi',
            'oulu_zoo' => 'Oulu zoo',
            'bryophyte_turku' => 'Turku botanical',
            'bryophyte_turku_orig' => 'Turku (no abundance)',
            //'mollusca' => 'mollusca',
        ));

    }
}
