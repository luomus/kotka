<?php

namespace Kotka\Form\Element;

use Kotka\Validator\InArrayCaseInsensitive;
use Zend\Validator\Explode as ExplodeValidator;

/**
 * Class Language
 * @package Kotka\Form\Element
 */
class Language extends Select
{

    private static $mapping = [
        'fi' => 'finnish',
        'en' => 'english',
        'sv' => 'swedish',
        'la' => 'latin',
    ];

    public static $languages = [
        'finnish' => 'Finnish',
        'english' => 'English',
        'swedish' => 'Swedish',
        'sami' => 'Sami',
        'russian' => 'Russian',
        'estonian' => 'Estonian',
        'german' => 'German',
        'french' => 'French',
        'spanish' => 'Spanish',
        'chinese' => 'Chinese',
        'latin' => 'Latin',
        'mixed' => 'mixed',
        'other' => 'other',
    ];

    /**
     * Makes the language select
     */
    public function init()
    {
        $this->setValueOptions(self::$languages);
    }

    public function setValue($value)
    {
        $value = strtolower($value);
        if (isset(self::$mapping[$value])) {
            $value = self::$mapping[$value];
        }
        parent::setValue($value);
    }

    public static function getAllLanguages()
    {
        return array_merge(self::$languages, self::$mapping);
    }

    /**
     * Get validator
     *
     * @return \Zend\Validator\ValidatorInterface
     */
    protected function getValidator()
    {
        if (null === $this->validator && !$this->disableInArrayValidator()) {
            $validator = new InArrayCaseInsensitive(array(
                'haystack' => array_keys(self::getAllLanguages()),
                'strict' => false
            ));

            if ($this->isMultiple()) {
                $validator = new ExplodeValidator(array(
                    'validator' => $validator,
                    'valueDelimiter' => null, // skip explode if only one value
                ));
            }

            $this->validator = $validator;
        }
        return $this->validator;
    }


}
