<?php

namespace Kotka\Form\Element;


class FreeSelect extends Select
{

    public function setValue($value)
    {
        if ($value !== null && $value !== '' && $value !== []) {
            $valueOptions = $this->getValueOptions();
            $values = $value;
            if (is_string($values)) {
                $values = [$values];
            }
            $hasNew = false;
            foreach ($values as $v) {
                if (!in_array($v, $valueOptions)) {
                    $hasNew = true;
                    $valueOptions[$v] = $v;
                }
            }
            if ($hasNew) {
                $this->setValueOptions($valueOptions);
            }
        }
        parent::setValue($value);
        return $this;
    }


    /**
     * Provide default input rules for this element
     *
     * @return array
     */
    public function getInputSpecification()
    {
        $spec = array(
            'name' => $this->getName(),
            'required' => false,
        );

        return $spec;
    }

}