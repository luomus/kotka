<?php

namespace Kotka\Form\Element;

use Zend\Form\Element\Select as ZendSelect;

/**
 * Class DisplayFormat
 * @package Kotka\Form\Element
 */
class DisplayFormat extends ZendSelect
{
    /**
     * Makes the display format select
     */
    public function __construct()
    {
        parent::__construct('display_format');
        $this->setValueOptions(array(
            'table' => 'text table',
            'label_bryophyte' => 'LABELS: bryophytes',
            'label_vascular' => 'LABELS: vascular plants',
            'label_insecta' => 'LABELS: insecta',
            'label_insecta_det' => 'LABELS: insecta det',
            'label_temporary' => 'LABELS: temporary',
        ));

    }
}
