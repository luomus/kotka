<?php

namespace Kotka\Form\Element;

use Zend\Form\Element\Select as ZendSelect;

/**
 * Class SampleLabelFormat
 * @package Kotka\Form\Element
 */
class SampleLabelFormat extends ZendSelect
{
    /**
     * Makes the display format select
     */
    public function __construct()
    {
        parent::__construct('sample_display_format');
        $this->setValueOptions(array(
            'labelDesigner' => 'Label Designer',
            'dna_v1' => 'DNA v1'
        ));

    }
}
