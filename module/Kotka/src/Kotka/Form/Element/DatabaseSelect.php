<?php

namespace Kotka\Form\Element;

use Kotka\Service\FormElementService;
use Traversable;
use Zend\Form\ElementInterface;
use Zend\Form\Exception;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Zend\Stdlib\ArrayUtils;


/**
 * Class DatabaseSelect
 * @package Kotka\Form\Element
 */
class DatabaseSelect extends Select implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    /**
     * @var bool
     */
    protected $disableInArrayValidator = true;

    protected $field;

    public function setValue($value)
    {
        if (is_bool($value)) {
            $value = $value ? 'true' : 'false';
        }
        return parent::setValue($value);
    }


    /**
     *
     * Set options for an element. Accepted options are:
     * - label: label to associate with the element
     * - label_attributes: attributes to use when the label is rendered
     * - value_options: list of values and labels for the select options
     * _ empty_option: should an empty option be prepended to the options
     * _ field: name of the field you want to get from the database (matches the subject in the triplestore)
     *
     * @param array|Traversable $options
     * @param boolean $reverse
     * @return void|\Zend\Form\Element\Select|ElementInterface
     * @throws \Zend\Form\Exception\InvalidArgumentException
     */
    public function setOptions($options, $reverse = false)
    {
        if ($options instanceof Traversable) {
            $options = ArrayUtils::iteratorToArray($options);
        } elseif (!is_array($options)) {
            throw new Exception\InvalidArgumentException(
                'The options parameter must be an array or a Traversable'
            );
        }

        if (isset($options['field'])) {
            $this->setField($options['field']);
        }
        if (!isset($options['value_options']) && !isset($this->options['value_options'])) {
            /** @var \Kotka\Service\FormElementService $elemService */
            $elemService = $this->serviceLocator->getServiceLocator()->get('Kotka\Service\FormElementService');
            $valueOptions = $elemService->getSelect($this->field);
            if (!is_array($valueOptions)) {
                $valueOptions = [];
            }
            if ($reverse) {
                $valueOptions = array_reverse($valueOptions);
            }
            if (isset($valueOptions[FormElementService::ALIAS_KEY])) {
                unset($valueOptions[FormElementService::ALIAS_KEY]);
            }
            $options['value_options'] = $valueOptions;
        }

        parent::setOptions($options);
    }

    public function getField()
    {
        return $this->field;
    }

    public function setField($field)
    {
        $this->field = $field;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isValueAsKey()
    {
        return $this->valueAsKey;
    }

    /**
     * @param boolean $valueAsKey
     */
    public function setValueAsKey($valueAsKey)
    {
        $this->valueAsKey = (boolean)$valueAsKey;
    }
}
