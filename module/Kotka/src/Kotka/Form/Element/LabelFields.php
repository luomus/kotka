<?php

namespace Kotka\Form\Element;

use Zend\Form\Element\Select as ZendSelect;

/**
 * Class LabelFields
 * @package Kotka\Form\Element
 */
class LabelFields extends ZendSelect
{

    public function setValue($value)
    {
        parent::setValue($value);
    }

    /**
     * Makes the display format select
     */
    public function __construct()
    {
        parent::__construct('label_fields');
        $this->setValueOptions(array(
            'taxon' => 'Taxon and author',
            'country' => 'Country',
            'municipality' => 'Municipality',
            'province' => 'Province',
            'locality' => 'Locality',
            'localityDescription' => 'Locality description',
            'coordinate' => 'Coordinates',
            'leg' => 'Leg',
            'det-year' => 'Det year'
        ));

    }
}
