<?php

namespace Kotka\Form\Element;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

/**
 * Class Organization
 * @package Kotka\Form\Element
 */
class OwnerOrganization extends Select implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    /**
     * Makes the organizations select
     * Fetches all the needed data from the database
     */
    public function init()
    {
        $parentLocator = $this->getServiceLocator()->getServiceLocator();
        /** @var \Kotka\Triple\MAPerson $user */
        $user = $this->serviceLocator->getServiceLocator()->get('user');
        $organizations = $user->getMAOrganisation();
        if (is_array($organizations)) {
            $this->setValueOptions($organizations);
        }
        $this->setEmptyOption('Select');
        if (count($organizations) == 1) {
            $this->setValue(key($organizations));
        }
    }
}
