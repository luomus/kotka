<?php

namespace Kotka\Form\Element;

use Zend\Form\Element\Select as ZendSelect;
use Zend\Form\Exception;
use Zend\Stdlib\ArrayUtils;


/**
 * Class Select
 * @package Kotka\Form\Element
 */
class Select extends ZendSelect
{

    protected $valueAsKey = false;

    /**
     *
     * Set options for an element. Accepted options are:
     * - label: label to associate with the element
     * - label_attributes: attributes to use when the label is rendered
     * - value_options: list of values and labels for the select options
     * _ empty_option: should an empty option be prepended to the options
     * _ field: name of the field you want to get from the database (matches the subject in the triplestore)
     *
     * @param array|Traversable $options
     * @param boolean $reverse
     * @return void|\Zend\Form\Element\Select|ElementInterface
     * @throws \Zend\Form\Exception\InvalidArgumentException
     */
    public function setOptions($options, $reverse = false)
    {
        if ($options instanceof \Traversable) {
            $options = ArrayUtils::iteratorToArray($options);
        } elseif (!is_array($options)) {
            throw new Exception\InvalidArgumentException(
                'The options parameter must be an array or a Traversable'
            );
        }
        if (isset($options['value-as-key'])) {
            $this->setValueAsKey($options['value-as-key']);
        }
        if ($this->valueAsKey && isset($options['value_options']) && !isset($this->options['value_options'])) {
            $options['value_options'] = array_combine($options['value_options'], $options['value_options']);
        }

        parent::setOptions($options);
    }

    /**
     * @return boolean
     */
    public function isValueAsKey()
    {
        return $this->valueAsKey;
    }

    /**
     * @param boolean $valueAsKey
     */
    public function setValueAsKey($valueAsKey)
    {
        $this->valueAsKey = $valueAsKey;
    }

    public function getInputSpecification()
    {
        $spec = parent::getInputSpecification();
        $spec['required'] = false;

        return $spec;
    }

}
