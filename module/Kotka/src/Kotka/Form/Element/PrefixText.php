<?php

namespace Kotka\Form\Element;

use Kotka\Validator\StartsWith;
use Zend\Di\ServiceLocator;
use Zend\Form\Element\Text;
use Zend\Form\Exception;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Zend\Stdlib\ArrayUtils;

class PrefixText extends Text implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    protected $field;
    protected $valuePrefix;
    protected $value_options = [];

    public function setOptions($options)
    {
        parent::setOptions($options);
        if (isset($this->options['value_options'])) {
            $this->setValueOptions($this->options['value_options']);
        }
        if (isset($this->options['value_prefix'])) {
            $this->setValuePrefix($this->options['value_prefix']);
        }
        if (isset($this->options['field'])) {
            $this->setField($this->options['field']);
        }
        $this->options['template'] = 'kotka/partial/element/prefix';
    }

    /**
     * @return mixed
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * @param mixed $field
     */
    public function setField($field)
    {
        if ($this->field !== $field) {
            /** @var ServiceLocator $parentSL */
            /** @var \Kotka\Service\FormElementService $eService */
            $parentSL = $this->getServiceLocator()->getServiceLocator();
            $eService = $parentSL->get('Kotka\Service\FormElementService');
            $values = $eService->getSelect($field, false, false);
            $keys = StartsWith::removeFieldPrefix($values, $field, $this->valuePrefix);
            $this->setValueOptions(array_combine($keys, $values));
        }
        $this->field = $field;
    }

    /**
     * @return mixed
     */
    public function getValuePrefix()
    {
        return $this->valuePrefix;
    }

    /**
     * @param mixed $valuePrefix
     */
    public function setValuePrefix($valuePrefix)
    {
        $this->valuePrefix = $valuePrefix;
    }

    /**
     * @return array
     */
    public function getValueOptions()
    {
        return $this->value_options;
    }

    /**
     * @param array $value_options
     */
    public function setValueOptions(array $value_options)
    {
        $this->value_options = $value_options;
        $this->attributes['data-prefixes'] = implode('|', array_keys($this->value_options));
    }
}