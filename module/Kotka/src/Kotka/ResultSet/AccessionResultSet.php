<?php

namespace Kotka\ResultSet;


use Elastic\Result\Result as ElasticResult;
use Common\Service\IdService;
use Kotka\Stdlib\Hydrator\ClassNamespaceMethods;
use Kotka\Triple\PUUBranch;
use Kotka\Triple\PUUEvent;

class AccessionResultSet
{

    const MAX_LEVEL = 500;

    protected $branches = [];
    protected $level = [];
    protected $hydrator;
    protected $current = 0;
    protected $masterQName;
    protected $rootQName;
    protected $events = [];
    protected $collections;

    public function initByArray($root, array $branches, array $events, $collections)
    {
        $this->setRoot($root);
        $this->collections = $collections;
        usort($branches, [$this, 'sortBranches']);
        foreach ($branches as $branch) {
            if (!$branch instanceof PUUBranch) {
                continue;
            }
            $qname = IdService::getQName($branch->getSubject());
            $this->branches[$qname] = $branch;
        }
        foreach ($events as $event) {
            if (!$event instanceof PUUEvent) {
                continue;
            }
            $parent = IdService::getQName($event->getPUUBranchID());
            if (!isset($this->events[$parent])) {
                $this->events[$parent] = [];
            }
            $this->events[$parent][] = $event;
        }
    }

    public function getBranch($id)
    {
        if (isset($this->branches[$id])) {
            return $this->branches[$id];
        }
        return null;
    }

    public function getBranches()
    {
        return $this->branches;
    }

    public function setRoot($qname)
    {
        $this->rootQName = $qname;
    }

    public function getEventIterator($master)
    {
        if (!isset($this->events[$master])) {
            return new \ArrayIterator();
        }
        return new \ArrayIterator($this->events[$master]);
    }

    private function sortBranches($a, $b) {
        if (!$a instanceof PUUBranch) {
            return -1;
        }
        if (!$b instanceof PUUBranch) {
            return 1;
        }
        $baseA = $a->getPUUExists() ? 0 : 10000000;
        $baseB = $b->getPUUExists() ? 0 : 10000000;
        $baseA += $this->getLocationByCollectionID($a->getPUUCollectionID());
        $baseB += $this->getLocationByCollectionID($b->getPUUCollectionID());
        return $baseA - $baseB;
    }

    private function getLocationByCollectionID($collectionID) {
        if (!isset($collectionID) || !isset($this->collections[$collectionID])) {
            return 0;
        }
        $res = filter_var($this->collections[$collectionID], FILTER_SANITIZE_NUMBER_INT);

        return is_numeric($res) ? $res : 0;
    }
}