<?php

namespace Kotka\PrograssBar;

use Zend\ProgressBar\ProgressBar;

class DummyProgressBar extends ProgressBar
{

    public function __construct()
    {
    }

    public function next($diff = 1, $text = null)
    {
    }

    public function update($value = null, $text = null)
    {
    }

    public function finish()
    {
    }
}