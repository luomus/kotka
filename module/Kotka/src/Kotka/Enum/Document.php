<?php

namespace Kotka\Enum;


abstract class Document
{
    const TYPE = 'MY.document';

    const PREDICATE_PUBLICITY_RESTRICTION = 'MZ.publicityRestrictions';
    const PREDICATE_DATE_CREATED = 'MZ.dateCreated';
    const PREDICATE_CREATOR = 'MZ.creator';
    const PREDICATE_OWNER = 'MZ.owner';
    const PREDICATE_PRIMARY_DATA_LOCATION = 'MY.primaryDataLocation';
    const PREDICATE_COLLECTION_ID = 'MY.collectionID';
    const PREDICATE_IN_MUSTIKKA = 'MY.inMustikka';
    const PREDICATE_UNRELIABLE_FIELDS = 'MY.unreliableFields';

    const PREDICATE_PARENT = 'MY.isPartOf';

    const PUBLICITY_DEFAULT = 'MZ.publicityRestrictionsPublic';
    const PUBLICITY_PUBLIC = 'MZ.publicityRestrictionsPublic';
    const PUBLICITY_PROTECTED = 'MZ.publicityRestrictionsProtected';
    const PUBLICITY_PRIVATE = 'MZ.publicityRestrictionsPrivate';

    const FORM_OBJECT_ID = 'MYObjectID';
    const FORM_NAMESPACE_ID = 'MYNamespaceID';
    const FORM_COLLECTION_ID = 'MYCollectionID';
    const FORM_DATASET_ID = 'MYDatasetID';

}