<?php

namespace Kotka\ElasticExtract;

use Elastic\Extract\Autocomplete;
use Elastic\Extract\ExtractInterface;
use Triplestore\Classes\Hydrator\MFSampleDb;
use Triplestore\Classes\MFSampleInterface;
use Triplestore\Classes\MYCollectionInterface;
use Triplestore\Classes\MYIdentificationInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;

class Sample extends Specimen implements ServiceLocatorAwareInterface, ExtractInterface
{

    static $sample;

    protected $specimenService;
    protected $collectionService;
    protected $collectionCache = [];

    const INDEX = 'kotka_sample';
    const TYPE = 'sample';


    protected $copyOpenedIDTo = [
        'MF.collectionID' => 'collectionID',
        'MF.datasetID' => 'datasetID',
    ];

    protected $open = [
        'MF.collectionID' => 'MF.collectionID',
        'MF.datasetID' => 'MF.datasetID',
        'MZ.creator' => 'MA.person',
        'MZ.editor' => 'MA.person',
        'MF.preparationMaterials' => 'MF.preparationMaterials',
        'MF.preparationProcess' => 'MF.preparationProcess',
        'MF.preparationType' => 'MF.preparationType',
        'MF.preservation' => 'MF.preservation',
        'MF.quality' => 'MF.quality',
        'MF.qualityCheckMethod' => 'MF.qualityCheckMethod',
        'MF.recordBasis' => 'MF.recordBasis',
        'MF.recordParts' => 'MF.recordParts',
        'MF.material' => 'MF.material',
        'MF.status' => 'MF.status',
        'MF.elutionMedium' => 'MF.elutionMedium',
    ];


    protected $map = [
        'MF.collectionID' => 'collection',
        'MF.datasetID' => 'dataset',
    ];

    protected $autocomplete = [
        'taxon' => true,
        'dataset' => true,
        'collection' => true,
    ];

    protected $fromDocument = [
        'acquisitionDate',
        'author',
        'age',
        'biologicalProvince',
        'country',
        'collection',
        'collectionID',
        'collectionTree',
        'creator',
        'dataset',
        'datasetID',
        'dateBegin',
        'dateEnd',
        'documentURI',
        'documentID',
        'documentQName',
        'documentLocation',
        'municipality',
        'editor',
        'infraEpithet',
        'isType',
        'leg',
        'legID',
        'lifeStage',
        'locality',
        'objectID',
        'originalSpecimenID',
        'ring',
        'sex',
        'taxa',
        'taxon',
        'taxonAndInfra',
        'taxonVerbatim',
    ];

    public function __construct()
    {
        parent::__construct();
        $this->autocompleteExtract = new Autocomplete();
        $this->autocompleteType = Autocomplete::getTypeName($this);
        $this->hydrator = new MFSampleDb();
    }

    public function getMapping()
    {
        return [
            'dynamic_templates' => [
                [
                    'generic' => [
                        'match' => '*',
                        'mapping' => [
                            'type' => 'text',
                            'fields' => ['raw' => ['type' => 'keyword']],
                        ]
                    ],
                ]
            ],
            'date_detection' => false,
            'properties' => [
                'eventDate' => [
                    'type' => 'date',
                    'format' => BASE::FORMAT_DATETIME . '||date||yyyy-MM-dd\'T\'HH:mm:ss||yyyy-MM-dd||yyyy'
                ],
                "acquisitionDate" => [
                    "type" => "date",
                    "format" => "date",
                    "fields" => ['raw' => ['type' => 'keyword']]
                ],
                "collectionID" => [
                    "type" => "keyword"
                ],
                "collectionTree" => [
                    "type" => "text",
                    "include_in_all" => false
                ],
                "documentURI" => [
                    "type" => "keyword"
                ],
                "documentID" => [
                    "type" => "keyword"
                ],
                "documentQName" => [
                    "type" => "keyword"
                ],
                "objectID" => [
                    "type" => "long",
                    "include_in_all" => false
                ],
                'measurement' => [
                    "type" => "object",
                    "include_in_all" => false
                ],
                'sampleMeasurement' => [
                    "type" => "object",
                    "include_in_all" => false
                ],
                "sampleDocumentURI" => [
                    "type" => "keyword"
                ],
                "sampleDocumentID" => [
                    "type" => "keyword"
                ],
                "sampleDocumentQName" => [
                    "type" => "keyword"
                ],
                "sampleObjectID" => [
                    "type" => "long",
                    "include_in_all" => false
                ],
                "taxa" => [
                    "type" => "text",
                    "include_in_all" => false
                ],
                "sampleDateCreated" => [
                    "type" => "date",
                    "format" => BASE::FORMAT_DATETIME . '||date||yyyy-MM-dd\'T\'HH:mm:ss||yyyy'
                ],
            ]
        ];
    }

    public function addMFSampleArray(array $samples, &$bulk, $document)
    {
        foreach ($samples as $sample) {
            if ($sample instanceof MFSampleInterface) {
                $this->addMFSample($sample, $bulk, $document);
            }
        }
    }

    public function addMFSample(MFSampleInterface $sample, &$bulk, $document)
    {
        $data = $this->hydrator->extract($sample);
        $data['subject'] = $sample->getSubject();
        return $this->addToBulk($data, $bulk, $document);
    }

    public function addToBulk($sample, &$bulk, $document = null)
    {
        $this->init();
        if (empty($sample)) {
            return;
        }
        if ($sample instanceof MFSampleInterface) {
            return $this->addMFSample($sample, $bulk, $document);
        } elseif (is_array($sample)) {
            return $this->prepareSampleRow($sample, $bulk, $document);
        } else {
            throw new \Exception("Cannot find a way to index branch to elastic search.");
        }
    }

    protected function prepareSampleRow($sample, &$bulk, $document)
    {
        // Clear empty values
        $sample = array_filter($sample);
        $subject = $sample['subject'];

        // Add collection data
        $this->addCollectionData($sample, $document);

        // Move preparations data to root level
        $preparationSkip = ['subject', 'type', 'MY.isPartOf'];
        if (isset($sample['MF.preparationClass']) && is_array($sample['MF.preparationClass'])) {
            foreach ($sample['MF.preparationClass'] as $preparation) {
                foreach ($preparation as $k => $v) {
                    if (in_array($k, $preparationSkip)) {
                        continue;
                    }
                    if (!isset($sample[$k])) {
                        $sample[$k] = [];
                    } else if (!is_array($sample[$k])) {
                        $sample[$k] = [$sample[$k]];
                    }
                    if (is_array($v)) {
                        foreach ($v as $val) {
                            $sample[$k][] = $val;
                        }
                    } else {
                        $sample[$k][] = $v;
                    }
                }
            }
            unset($sample['MF.preparationClass']);
        }

        // Parse subject
        $this->parseSubject($sample['subject'], $sample);
        unset($sample['subject']);
        unset($sample['type']);

        // Analyze measurement class
        if (isset($sample['MY.measurementClass'])) {
            $measurementResult = [];
            foreach ($sample['MY.measurementClass'] as $measurementClass) {
                foreach ($measurementClass as $measurement => $value) {
                    if (in_array($measurement, ['subject', 'type', 'MY.isPartOf'])) {
                        continue;
                    }
                    $measurementParts = explode('.', $measurement);
                    $fieldNoDot = array_pop($measurementParts);
                    $measurementResult[$fieldNoDot] = $value;
                }
            }
            $sample['MY.measurement'] = $measurementResult;
            unset($sample['MY.measurementClass']);
        }

        // Open the values
        $this->openIds($sample);

        // Map keys to new ones
        foreach ($this->map as $from => $to) {
            if (isset($sample[$from])) {
                $sample[$to] = $sample[$from];
                unset($sample[$from]);
            }
        }

        // Remove ns
        $data = $this->removeNS($sample);

        if (isset($data['dateCreated'])) {
            $data['dateCreated'] = $this->DateTimeToElasticFormat($data['dateCreated']);
        }

        // Add sample prefix
        foreach ($data as $field => $value) {
            if (strpos($field, 'sample') !== 0) {
                $newKey = 'sample' . ucfirst($field);
                $data[$newKey] = $data[$field];
                unset($data[$field]);
            }
        }

        // Copy values from document
        foreach ($this->fromDocument as $key) {
            if (isset($document) && isset($document[$key])) {
                $data[$key] = $document[$key];
            }
        }

        $this->autocompleteExtract->addToBulk($data, $bulk, $this->autocompleteType, $this->fetchAutocompleteFields($data));

        $bulk['body'][] = [
            'index' => [
                '_index' => self::getIndexName(),
                '_type' => self::getTypeName(),
                '_id' => $subject
            ]
        ];
        $bulk['body'][] = $data;

        return $data;
    }

    private function addCollectionData(&$data, $document) {
        if (!isset($data['MF.collectionID']) && isset($document['collectionID'])) {
            $data['MF.collectionID'] = $document['collectionID'];
        }
        if (!isset($data['MF.collectionID'])) {
            return;
        }
        $collections = $this->getCollections($data['MF.collectionID']);
        if ($collections !== null) {
            $data['sampleCollectionTree'] = $collections;
        }
        if (!array_key_exists($data['MF.collectionID'], $this->collectionCache)) {
            $this->collectionCache[$data['MF.collectionID']] = $this->getCollectionService()->getById($data['MF.collectionID']);
        }
        $collection = isset($this->collectionCache[$data['MF.collectionID']]) ? $this->collectionCache[$data['MF.collectionID']] : $data['MF.collectionID'];

        if ($collection instanceof MYCollectionInterface) {
            $data['collection'] = $collection->getMYCollectionName('en');
            $data['collectionNumber'] = preg_replace('/[^0-9]/', '', $data['collection']);
        }
    }

    private function removeNS($document) {
        $data = [];
        foreach ($document as $key => $value) {
            $dotPos = strpos($key, '.');
            if ($dotPos) {
                $key = substr($key, $dotPos + 1);
            } else if (strpos($key, 'PUU') === 0) {
                $key = lcfirst(substr($key, 3));
            }
            $data[$key] = $value;
        }
        return $data;
    }

    private function fetchAutocompleteFields($document) {
        $autocompleteFields = [];
        foreach ($this->autocomplete as $key => $value) {
            if (isset($document[$key]) && $value === true) {
                $autocompleteFields[] = $key;
            }
        }
        return $autocompleteFields;
    }

    /**
     * @return \Kotka\Service\CollectionService
     */
    private function getCollectionService() {
        if (!$this->collectionService) {
            $this->collectionService = $this->getServiceLocator()->get('Kotka\Service\CollectionService');
        }
        return $this->collectionService;
    }
}