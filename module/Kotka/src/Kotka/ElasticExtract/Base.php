<?php

namespace Kotka\ElasticExtract;


use Common\Service\IdService;
use Elastic\Client\Index;
use Elastic\Extract\ExtractInterface;

abstract class Base implements ExtractInterface
{

    const INDEX = 'kotka';
    const FORMAT_DATETIME = "yyyy-MM-dd HH:mm:ss";

    protected $init = [];


    public function getSettings()
    {
        return [
            'number_of_shards' => Index::SHARDS,
            'number_of_replicas' => Index::REPLICAS,
        ];
    }

    protected function parseSubject($subject, &$row)
    {
        $qnameParts = explode(':', $subject);
        if (!isset($qnameParts[1])) {
            array_unshift($qnameParts, rtrim(IdService::DEFAULT_DB_QNAME_PREFIX, ':'));
        }

        $row['documentURI'] = IdService::getUri($subject);
        $row['documentID'] = $qnameParts[1];
        $row['documentQName'] = $qnameParts[0] . ':' . $qnameParts[1];

        $subjectParts = explode('.', $qnameParts[1]);
        $row['domain'] = $qnameParts[0];
        $row['namespace'] = $subjectParts[0];
        if (isset($subjectParts[1])) {
            $subjectParts[1] = str_replace('-', '', $subjectParts[1]);
            $row['objectID'] = (int)filter_var($subjectParts[1], FILTER_SANITIZE_NUMBER_INT);
        }
    }

    protected function DateToElasticFormat($date, $fill = null)
    {
        $targetFormat = 'Y-m-d';
        if (empty($date)) {
            return null;
        }
        $datetime = null;
        if (is_string($date)) {
            if (strlen($date) <= 4) {
                if ($fill !== null) {
                    $date = $fill . $date;
                } else {
                    $date = '01.01.' . $date;
                }
            }
            try {
                $datetime = \DateTime::createFromFormat('d.m.Y', $date);
                if ($datetime === false) {
                    $datetime = \DateTime::createFromFormat('Y-m-d', $date);
                }
                if ($datetime === false) {
                    $datetime = new \DateTime($date);
                }
            } catch (\Exception $e) {

            }
        } elseif ($date instanceof \DateTime) {
            return $date->format($targetFormat);
        }

        if ($datetime instanceof \DateTime) {
            return $datetime->format($targetFormat);
        }
        return null;
    }

    public function init()
    {

    }

    protected function DateTimeToElasticFormat($datetime)
    {
        if (strpos($datetime, '+') !== false) {
            $datetime = substr($datetime, 0, -5);
        }
        $datetime = str_replace('T', ' ', $datetime);
        return $datetime;
    }

    public function getRelatedExtracts()
    {
        return [];
    }

}