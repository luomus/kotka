<?php

namespace Kotka\ElasticExtract;

use Elastic\Query\Query;
use Zend\Json\Json;

include_once "./module/Kotka/lib/gisconverter/gisconverter.php";

abstract class KML extends Location
{

    protected static $decoder;

    public function addToBulk($object, &$bulk)
    {
        $data = [];
        if (!$object instanceof \DOMElement) {
            throw new \Exception('Invalid type date given to extractor');
        }

        $placeData = $object->getElementsByTagName('SchemaData')->item(0);
        list($names, $descriptions) = $this->getNameData($placeData);
        if (empty($names['fi'])) {
            throw new \Exception('Could not find name from the given DOM element');
        }
        $border = $object->getElementsByTagName('MultiGeometry');
        if ($border->length == 0) {
            $border = $object->getElementsByTagName('Polygon');
        }
        if ($border->length > 1) {
            throw new \Exception("Border has two polygon objects byt it's not marked as MultiGeometry object");
        }
        $border = $border->item(0);
        $border = $this->kmlToGeoJson($object->ownerDocument->saveHTML($border));
        $this->fixBoarders($border);
        if (isset($border['type'])) {
            $border['type'] = strtolower($border['type']);
        }
        $data['name'] = $names;
        $data['description'] = $descriptions;
        $data['border'] = $border;
        $data['boundingBox'] = ['type' => 'envelope', 'coordinates' => $this->getBoundingBoxCoordinates($border)];

        if ($this->exists($data['name']['fi'])) {
            return;
        }
        parent::addToBulk($data, $bulk);
    }

    private function getNameData(\DOMElement $placeData) {
        $placeData = $placeData->getElementsByTagName('SimpleData');
        $len = $placeData->length;
        $names = ['fi' => ''];
        $descriptions = [];
        for($i = 0; $i<$len; $i++) {
            $data = $placeData->item($i);
            $value = $data->nodeValue;
            if (!$data->hasAttributes() || empty($value)) {
                continue;
            }
            $attr = $data->attributes;
            $attrValue = $attr->item(0)->nodeValue;
            if ($attrValue === 'MAAKUNTALYH_FI') {
                $names['fi'] = $value;
            } elseif ($attrValue === 'MAAKUNTALYH_LAT') {
                $names['la'] = $value;
            } elseif ($attrValue === 'MAAKUNTA_FI') {
                $descriptions['fi'] = $value;
            } elseif ($attrValue === 'MAAKUNTA_LAT') {
                $descriptions['la'] = $value;
            } elseif ($attrValue === 'MAAKUNTA_ENG') {
                $descriptions['en'] = $value;
            }
        }
        return [$names, $descriptions];
    }

    private function kmlToGeoJson($kml) {
        if (self::$decoder === null) {
            self::$decoder = new \gisconverter\KML();
        }
        return Json::decode(self::$decoder->geomFromText($kml)->toGeoJSON(), Json::TYPE_ARRAY);
    }


    private function exists($name) {
        /** @var \Elastic\Client\Search $search */
        $search = $this->serviceLocator->get('Elastic\Client\Search');
        $query = new Query(self::getIndexName(), self::getTypeName());
        $queryStr = 'name.fi:"' . $name . '"';
        $query->setQueryString($queryStr);
        $query->setFields(['name']);
        $result = $search->query($query)->execute();
        return $result->count() > 0;
    }

    private function fixBoarders(& $coordinates) {
        $check = false;
        foreach ($coordinates as $key => &$value) {
            if (is_numeric($value[0]) && is_numeric($value[1])) {
                $check = true;
                if (!isset($coordinates[($key - 1)])) {
                    continue;
                }
                $prev = $coordinates[($key - 1)];
                if ($prev[0] === $value[0] && $prev[1] === $value[1]) {
                    unset($coordinates[$key]);
                }
            } elseif (is_array($value)) {
                $this->fixBoarders($value);
            }
        }
        if ($check) {
            $coordinates = array_values($coordinates);
        }
    }
}