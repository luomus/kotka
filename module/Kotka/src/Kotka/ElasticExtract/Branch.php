<?php

namespace Kotka\ElasticExtract;

use Elastic\Extract\Autocomplete;
use Elastic\Extract\ExtractInterface;
use Kotka\Service\TaxonService;
use Triplestore\Classes\Hydrator\PUUBranchDb;
use Triplestore\Classes\MYCollectionInterface;
use Triplestore\Classes\MYGatheringInterface;
use Triplestore\Classes\MYIdentificationInterface;
use Triplestore\Classes\MYUnitInterface;
use Triplestore\Classes\PUUBranchInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;

class Branch extends Specimen implements ServiceLocatorAwareInterface, ExtractInterface
{

    static $branch;

    protected $specimenService;
    protected $collectionService;
    protected $collectionCache = [];

    const INDEX = 'kotka_branch';
    const TYPE = 'branch';


    protected $open = [
        'PUU.eventType' => 'PUU.eventType',
        'PUU.lifeStage' => 'PUU.lifeStage',
        'MY.taxonRank' => 'MY.taxonRank',
        'MY.infraRank' => 'MY.infraRank',
        'MY.datasetID' => 'MY.datasetID',
        'MY.acquiredFromOrganization' => 'MY.acquiredFromOrganization',
        'MY.provenance' => 'MY.provenance',
        'PUU.seedsExchangedInstitution' => 'PUU.seedsExchangedInstitution',
    ];

    protected $copyOpenedIDTo = [
        'MY.projectId' => 'datasetID',
        'MY.datasetID' => 'datasetID',
        'MY.collectionID' => 'collectionID',
        'MZ.owner' => 'ownerID',
        'MY.acquiredFromOrganization' => 'acquiredFromOrganizationID',
        'PUU.seedsExchangedInstitution' => 'PUU.seedsExchangedInstitutionID',
    ];

    // Will remove new lines from string values
    protected $removeNewLine = [
      'gardenLocationNotes'
  ];

  protected $autocomplete = [
      'taxon' => true,
      'collection' => true,
      'branchLocation' => true,
      'branchGardenLocationNotes' => true,
  ];

    protected $branchPrefix = [
        'leg',
        'notes',
        'subLocation',
        'exists',
        'wgs84Longitude',
        'wgs84Latitude',
        'gardenLocationNotes',
    ];

    public function __construct()
    {
        parent::__construct();
        $this->autocompleteExtract = new Autocomplete();
        $this->autocompleteType = Autocomplete::getTypeName($this);
        $this->hydrator = new PUUBranchDb();
    }

    public function getMapping()
    {
        return [
            'dynamic_templates' => [
                [
                    'generic' => [
                        'match' => '*',
                        'mapping' => [
                            'type' => 'text',
                            'fields' => ['raw' => ['type' => 'keyword']],
                        ]
                    ],
                ]
            ],
            'date_detection' => false,
            'properties' => [
                'eventDate' => [
                    'type' => 'date',
                    'format' => BASE::FORMAT_DATETIME . '||date||yyyy-MM-dd\'T\'HH:mm:ss||yyyy-MM-dd'
                ],
                "collectionID" => [
                    "type" => "keyword"
                ],
                "branchLocationID" => [
                    "type" => "keyword"
                ],
                "collectionTree" => [
                    "type" => "text",
                    "include_in_all" => false
                ],
                "branchLocationTree" => [
                    "type" => "text",
                    "include_in_all" => false
                ],
                "datasetID" => [
                    "type" => "keyword"
                ],
                "documentURI" => [
                    "type" => "keyword"
                ],
                "documentID" => [
                    "type" => "keyword"
                ],
                "documentQName" => [
                    "type" => "keyword"
                ],
                "taxa" => [
                    "type" => "text",
                    "include_in_all" => false
                ]
            ]
        ];
    }

    public function addPUUBranchArray(array $documents, &$bulk)
    {
        foreach ($documents as $document) {
            if ($document instanceof PUUBranchInterface) {
                $this->addPUUBranch($document, $bulk);
            }
        }
    }

    /**
     * @param PUUBranchInterface $document
     * @param $bulk
     * @throws \Exception
     */
    public function addPUUBranch(PUUBranchInterface $document, &$bulk)
    {
        $data = $this->hydrator->extract($document);
        $data['subject'] = $document->getSubject();
        $this->addToBulk($data, $bulk);
    }

    public function addToBulk($document, &$bulk)
    {
        $this->init();
        if (empty($document)) {
            return;
        }
        if ($document instanceof PUUBranchInterface) {
            $this->addPUUBranch($document, $bulk);
        } elseif (is_array($document)) {
            $this->prepareSampleRow($document, $bulk);
        } else {
            throw new \Exception("Cannot find a way to index branch to elastic search.");
        }
    }

    protected function prepareSampleRow($document, &$bulk)
    {
        // Clear empty values
        $document = array_filter($document);
        if (!isset($document['PUU.collectionID'])) {
            return;
        }
        $subject = $document['subject'];

        // Add taxon linking
        $this->addDocumentData($document);

        // Add collection data
        $this->addCollectionData($document);

        // Parse subject
        $this->parseSubject($document['subject'], $document);
        unset($document['subject']);

        // Open the values
        $this->openIds($document);

        // Map keys to new ones
        foreach ($this->map as $from => $to) {
            if (isset($document[$from])) {
                $document[$to] = $document[$from];
                unset($document[$from]);
            }
        }

        // Remove ns
        $data = $this->removeNS($document);

        // Add event data
        $events = $this->getBranchEvents($subject);
        if (count($events)) {
            array_map(function($event) use (&$data) { return $this->prepareEvent($event, $data); }, $events);
            $data['branchAmount'] = $this->getAccessionService()->getTotal($events);
        }

        if (isset($data['eventType'])) {
            unset($data['eventType']);
        }
        if (isset($data['eventEventType'])) {
            $data['eventType'] = $data['eventEventType'];
            unset($data['eventEventType']);
        }

        if (isset($data['children'])) {
            unset($data['children']);
        }
        if (isset($data['date']) && $data['date'] instanceof \DateTime) {
            $data['date'] = $data['date']->format('Y-m-d');
        }

        if (isset($data['location'])) {
            $data['subLocation'] = $data['location'];
            unset($data['location']);
        }

        if (isset($data['collection'])) {
            $data['branchLocation'] = $data['collection'];
            $data['branchLocationID'] = $data['collectionID'];
            $data['branchLocationTree'] = $data['collectionTree'];
        }

        foreach ($this->branchPrefix as $key) {
            if (array_key_exists($key, $data)) {
                $newKey = 'branch' . ucfirst($key);
                $data[$newKey] = $data[$key];
                unset($data[$key]);
            }
        }

        // Remove new lines from text area
        foreach ($this->removeNewLine as $field) {
            if (!isset($data[$field]) || !is_string($data[$field])) {
                continue;
            }
            $data[$field] = str_replace(["\n", "\r"], [' ', ''], $data[$field]);
        }
      

        $this->autocompleteExtract->addToBulk($data, $bulk, $this->autocompleteType, $this->fetchAutocompleteFields($data));

        $bulk['body'][] = [
            'index' => [
                '_index' => self::getIndexName(),
                '_type' => self::getTypeName(),
                '_id' => $subject
            ]
        ];
        $bulk['body'][] = $data;
    }

    private function addCollectionData(&$data) {
        $collections = $this->getCollections($data['PUU.collectionID']);
        if ($collections !== null) {
            $data['collectionTree'] = $collections;
        }
        if (!array_key_exists($data['PUU.collectionID'], $this->collectionCache)) {
            $this->collectionCache[$data['PUU.collectionID']] = $this->getCollectionService()->getById($data['PUU.collectionID']);
        }
        $collection = $this->collectionCache[$data['PUU.collectionID']];

        if ($collection instanceof MYCollectionInterface) {
            $data['collection'] = $collection->getMYCollectionName('en');
            $data['collectionNumber'] = preg_replace('/[^0-9]/', '', $data['collection']);
        }
    }

    private function addDocumentData(&$data) {
        if (!isset($data['PUU.accessionID'])) {
            return;
        }
        $specimenService = $this->getSpecimentService();
        $document = $specimenService->getById($data['PUU.accessionID']);
        if ($document === null) {
            return;
        }
        $identifications = $specimenService->getAllIdentifications($document);
        usort($identifications, array('\Kotka\Filter\SortIdentifications', 'cpmIdentificationObject'));
        $taxon = '';
        $taxonRank = '';
        $taxonVerbatim = '';
        $taxa = [];
        $infraRank = '';
        $infraEpithet = '';
        $taxonAndInfra = '';
        $identificationNotes = '';
        foreach ($identifications as $identification) {
            /** @var MYIdentificationInterface $identification */
            $taxon = $identification->getMYTaxon();
            if (!isset($taxon)) {
                continue;
            }
            $identificationNotes = $identification->getMYIdentificationNotes();
            $taxonRank = $identification->getMYTaxonRank();
            $taxonVerbatim = $identification->getMYTaxonVerbatim();
            $infraEpithet = $identification->getMYInfraEpithet();
            $infraRank = $identification->getMYInfraRank();
            $taxa = $this->getTaxonTree($taxon);
            if (isset($taxa['acceptedTaxon']) && !empty($taxa['acceptedTaxon'])) {
                $data['acceptedTaxon'] = $taxa['acceptedTaxon'];
                $data['synonyms'] = $this->getSynonyms($taxa['acceptedTaxon']);
            }
            if (isset($taxa['family']) && !empty($taxa['family'])) {
                $data['family'] = $taxa['family'];
            }
            $taxonAndInfra = TaxonService::extractName($identification, true, $this->cache['MY.infraRank']);
            break;
        }
        if (isset($this->cache['MY.taxonRank']) && isset($this->cache['MY.taxonRank'][$taxonRank])) {
            $taxonRank = $this->cache['MY.taxonRank'][$taxonRank];
        }
        if (isset($this->cache['MY.infraRank']) && isset($this->cache['MY.infraRank'][$infraRank])) {
            $infraRank = $this->cache['MY.infraRank'][$infraRank];
        }
        $data['identificationNotes'] = $identificationNotes;
        $data['taxon'] = $taxon;
        $data['taxonRank'] = $taxonRank;
        $data['taxonVerbatim'] = $taxonVerbatim;
        $data['infraEpithet'] = $infraEpithet;
        $data['infraRank'] = $infraRank;
        $data['taxonAndInfra'] = $taxonAndInfra;
        $data['taxa'] = isset($taxa) && isset($taxa['taxon']) ? $taxa['taxon'] : [];

        $data['originalSpecimenID'] = $document->getMYOriginalSpecimenID();
        $data['MY.datasetID'] = $document->getMYDatasetID();
        $data['plannedLocation'] = $document->getMYPlannedLocation();
        $data['MY.acquiredFromOrganization'] = $document->getMYAcquiredFromOrganization();

        foreach ($document->getMYGathering() as $gathering) {
            /** @var MYGatheringInterface $gathering */
            $data['country'] = $gathering->getMYCountry();
            $data['municipality'] = $gathering->getMYMunicipality();
            $data['locality'] = $gathering->getMYLocality();
            $data['localityDescription'] = $gathering->getMYLocalityDescription();

            foreach ($gathering->getMYUnit() as $unit) {
                /** @var MYUnitInterface $unit */
                $data['MY.provenance'] = $unit->getMYProvenance();
                break;
            }

            break;
        }

        $collection = $this->getCollectionService()->getById($document->getMYCollectionID());
        $data['accessionCollection'] = $collection->getMYCollectionName('en');
    }

    private function prepareEvent($event, &$document)
    {
        if (isset($event['amount'])) {
            $event['amountInterpreted'] = intval($event['amount']);
        }
        unset($event['subject']);


        // Open the values
        $this->openIds($event);

        $event = $this->removeNS($event);
        foreach ($event as $key => $value) {
            $newKey = 'event' . ucfirst($key);
            if (!isset($document[$newKey])) {
                $document[$newKey] = [];
            }
            if (is_array($value)) {
                $document[$newKey] = array_merge($document[$newKey], $value);
            } else {
                $document[$newKey][] = $value;
            }
        }
    }

    private function removeNS($document) {
        $data = [];
        foreach ($document as $key => $value) {
            $dotPos = strpos($key, '.');
            if ($dotPos) {
                $key = substr($key, $dotPos + 1);
            } else if (strpos($key, 'PUU') === 0) {
                $key = lcfirst(substr($key, 3));
            }
            $data[$key] = $value;
        }
        return $data;
    }

    private function fetchAutocompleteFields($document) {
        $autocompleteFields = [];
        foreach ($this->autocomplete as $key => $value) {
            if (isset($document[$key]) && $value === true) {
                $autocompleteFields[] = $key;
            }
        }
        return $autocompleteFields;
    }

    private function getBranchEvents($id) {
        $accessionService = $this->getAccessionService();
        $events = $accessionService->getEventsByBranch($id, true);
        $accessionService->sortEvents($events);

        return $events;
    }

    /**
     * @return \Kotka\Service\SpecimenService
     */
    private function getSpecimentService() {
        if (!isset($this->specimenService)) {
            $this->specimenService = $this->getServiceLocator()->get('Kotka\Service\SpecimenService');
        }
        return $this->specimenService;
    }

    /**
     * @return \Kotka\Service\CollectionService
     */
    private function getCollectionService() {
        if (!$this->collectionService) {
            $this->collectionService = $this->getServiceLocator()->get('Kotka\Service\CollectionService');
        }
        return $this->collectionService;
    }
}