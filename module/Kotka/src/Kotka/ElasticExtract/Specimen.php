<?php

namespace Kotka\ElasticExtract;


use Elastic\Extract\Autocomplete;
use Elastic\Extract\ExtractInterface;
use Elastic\Query\Query;
use Kotka\Service\AccessionService;
use Kotka\Service\FormElementService;
use Common\Service\IdService;
use Kotka\Service\TaxonService;
use Triplestore\Classes\PUUBranchInterface;
use Zend\Cache\Storage\StorageInterface;
use Zend\Filter\StaticFilter;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

class Specimen extends Base implements ServiceLocatorAwareInterface, ExtractInterface
{
    use ServiceLocatorAwareTrait;

    const TYPE = 'specimen';

    protected $hydrator;
    protected $cache = [];
    protected $suggestions = [];
    protected $taxa;
    protected $synonyms = [];
    protected $collections;
    protected $db;
    protected $accessionService;
    protected $taxonService;
    protected $autocompleteExtract;
    protected $sampleExtract;
    protected $branchExtract;
    protected $autocompleteType;

    protected $open = [
        'MY.plantStatusCode' => 'MY.plantStatusCode',
        'MY.fruitType' => 'MY.fruitType',
        'MY.seedMorphology' => 'MY.seedMorphology',
        'MY.seedMaturity' => 'MY.seedMaturity',
        'MY.microbiologicalRiskGroup' => 'MY.microbiologicalRiskGroup',
        'MY.earliestEpochOrLowestSeries' => 'MY.earliestEpochOrLowestSeries',
        'MY.latestEpochOrHighestSeries' => 'MY.latestEpochOrHighestSeries',
        'MY.taxonRank' => 'MY.taxonRank',
        'MY.samplingMethod' => 'MY.samplingMethod',
        'MY.infraRank' => 'MY.infraRank',
        'MY.typeStatus' => 'MY.typeStatus',
        'MY.typeVerification' => 'MY.typeVerification',
        'MZ.editor' => 'MA.person',
        'MZ.creator' => 'MA.person',
        'MY.collectionID' => 'MY.collectionID',
        'MY.projectId' => 'MY.datasetID',
        'MY.datasetID' => 'MY.datasetID',
        'MY.preservation' => 'MY.preservation',
        'MY.sex' => 'MY.sex',
        'MY.recordBasis' => 'MY.recordBasis',
        'MY.lifeStage' => 'MY.lifeStage',
        'MY.georeferenceSource' => 'MY.georeferenceSource',
        'MY.coordinateSource' => 'MY.coordinateSource',
        'MY.coordinateSystem' => 'MY.coordinateSystem',
        'MY.status' => 'MY.status',
        'MZ.publicityRestrictions' => 'MZ.publicityRestrictions',
        'MY.provenance' => 'MY.provenance',
        'MZ.owner' => 'MZ.owner',
        'MY.acquiredFromOrganization' => 'MY.acquiredFromOrganization',
    ];
    private $copyFromSample = [
        'sampleAdditionalIDs',
        'samplePreparationType',
        'sampleMaterial',
        'sampleCollection',
        'sampleCollectionID'
    ];
    protected $copyOpenedIDTo = [
        'MY.projectId' => 'datasetID',
        'MY.datasetID' => 'datasetID',
        'MY.collectionID' => 'collectionID',
        'MZ.owner' => 'ownerID',
        'MY.acquiredFromOrganization' => 'acquiredFromOrganizationID',
    ];
    protected $map = [
        'MY.collectionID' => 'collection',
        'MY.projectId' => 'dataset',
        'MY.datasetID' => 'dataset',
        'MY.editor' => 'transcriber'
    ];
    /**
     * These will be indexed to field.key as a value array
     * ex.
     * field MY.measurement = [wingCm:10, wingCm:9]
     * will be indexed to
     * measurement.wingCm = [10,9]
     *
     * @var array
     */
    protected $keyValueFields = [
        'MY.relationship' => ':',
    ];
    protected $multi = [
        'MY.datasetID' => true
    ];
    protected $remove = [
        'MY.gathering',
        'MY.unit',
        'MY.identification',
        'MY.typeSpecimen',
        'MF.sample',
        'MF.preparationClass',
        'MY.isPartOf',
        //'MY.inMustikka',
        'MY.notes',
        //'MY.unreliableFields',
        'type',
        'subject'
    ];

    protected $removePrefix = [
        'datasetID',
        'collectionID',
    ];

    // Will remove new lines from string values
    protected $removeNewLine = [
        'localityVerbatim',
        'localityDescription',
        'labelsVerbatim',
        'editNotes',
        'documentNotes',
        'gatheringNotes',
        'unitNotes',
        'identificationNotes',
        'sampleHistory',
    ];

    protected $autocomplete = [
        'datatype' => true,
        'language' => true,
        //'URL' => 'indexUrl',
        'acquiredFrom' => true,
        'branchLocation' => true,
        'administrativeProvince' => true,
        'age' => true,
        'author' => true,
        'biologicalProvince' => true,
        'chemistry' => true,
        'cladVerbatim' => true,
        'collection' => true,
        'coordinateNotes' => true,
        'coordinatesVerbatim' => true,
        'country' => true,
        'county' => true,
        'creator' => true,
        'dataset' => true,
        'dataSource' => true,
        'det' => true,
        'detVerbatim' => true,
        'documentLocation' => true,
        'documentNotes' => true,
        'sampleHistory' => true,
        'editNotes' => true,
        'editor' => true,
        'entered' => true,
        'exsiccatum' => true,
        'gatheringNotes' => true,
        'genusQualifier' => true,
        'habitatClassification' => true,
        'habitatDescription' => true,
        'higherGeography' => true,
        'host' => true,
        'identificationNotes' => true,
        'infraAuthor' => true,
        'infraEpithet' => true,
        'labelsVerbatim' => true,
        'leg' => true,
        'legID' => true,
        'legVerbatim' => true,
        'locality' => true,
        'localityDescription' => true,
        'localityVerbatim' => true,
        'populationAbundance' => true,
        'preparations' => true,
        'primaryDataLocation' => true,
        'municipality' => true,
        'publication' => true,
        'sec' => true,
        'speciesQualifier' => true,
        'taxon' => true,
        'taxonVerbatim' => true,
        'transcriber' => true,
        'typeAuthor' => true,
        'typeBasionymePubl' => true,
        'typeNotes' => true,
        'typePubl' => true,
        'typeSpecies' => true,
        'typeSubspecies' => true,
        'typeSubspeciesAuthor' => true,
        'typif' => true,
        'unitNotes' => true,
        'endangeredStatus' => true,
        'genbankDescription' => true,
        'boldDescription' => true,
    ];

    protected $images;

    public function getMapping()
    {
        return [
            "dynamic_templates" => [
                [
                    "generic" => [
                        "match" => "*",
                        "mapping" => [
                            'type' => 'text',
                            "fields" => ['raw' => ['type' => 'keyword']],
                        ]
                    ],
                ]
            ],
            "date_detection" => false,
            'properties' => [
                "accepted" => [
                    "type" => "boolean"
                ],
                "firstInDocument" => [
                    "type" => "boolean"
                ],
                "acquisitionDate" => [
                    "type" => "date",
                    "format" => "date",
                    "fields" => ['raw' => ['type' => 'keyword']]
                ],
                "branchLocationTree" => [
                    "type" => "text",
                    "include_in_all" => false
                ],
                "branchLocationID" => [
                    "type" => "keyword"
                ],
                "accessionLocationID" => [
                    "type" => "keyword"
                ],
                "branchID" => [
                    "type" => "keyword"
                ],
                "sampleExists" => [
                    "type" => "boolean"
                ],
                "collectionID" => [
                    "type" => "keyword"
                ],
                "collectionTree" => [
                    "type" => "text",
                    "include_in_all" => false
                ],
                "datasetID" => [
                    "type" => "keyword"
                ],
                "dateBegin" => [
                    "type" => "date",
                    "format" => "date",
                    "fields" => ['raw' => ['type' => 'keyword', 'index' => 'not_analyzed']]
                ],
                "dateCreated" => [
                    "type" => "date",
                    "format" => BASE::FORMAT_DATETIME . '||date||yyyy-MM-dd\'T\'HH:mm:ss'
                ],
                "dateEdited" => [
                    "type" => "date",
                    "format" => BASE::FORMAT_DATETIME . '||date||yyyy-MM-dd\'T\'HH:mm:ss'
                ],
                "dateEnd" => [
                    "type" => "date",
                    "format" => "date",
                    "fields" => ['raw' => ['type' => 'keyword', 'index' => 'not_analyzed']]
                ],
                "documentURI" => [
                    "type" => "keyword"
                ],
                "documentID" => [
                    "type" => "keyword"
                ],
                "documentQName" => [
                    "type" => "keyword"
                ],
                "gatheringCount" => [
                    "type" => "integer",
                    "include_in_all" => false
                ],
                "hasPicture" => [
                    "type" => "boolean",
                    "include_in_all" => false
                ],
                'pictureCount' => [
                    "type" => "integer",
                    "include_in_all" => false
                ],
                "identificationCount" => [
                    "type" => "integer",
                    "include_in_all" => false
                ],
                "isType" => [
                    "type" => "boolean",
                    "include_in_all" => false
                ],
                'measurement' => [
                    "type" => "object",
                    "include_in_all" => false
                ],
                'relationship' => [
                    "type" => "object",
                    "include_in_all" => false
                ],
                "namespace" => [
                    "type" => "keyword",
                    "include_in_all" => false
                ],
                "objectID" => [
                    "type" => "long",
                    "include_in_all" => false
                ],
                "typeCount" => [
                    "type" => "integer",
                    "include_in_all" => false
                ],
                "unitCount" => [
                    "type" => "integer",
                    "include_in_all" => false
                ],
                "wgs84Latitude" => [
                    "type" => "float",
                    "fields" => ['raw' => ['type' => 'keyword']],
                    "include_in_all" => false
                ],
                "wgs84Location" => [
                    "type" => "geo_point",
                    "include_in_all" => false
                ],
                "wgs84Longitude" => [
                    "type" => "float",
                    "fields" => ['raw' => ['type' => 'keyword']],
                    "include_in_all" => false
                ],
            ]
        ];
    }

    protected $first = false;

    public function __construct()
    {
        $this->autocompleteExtract = new Autocomplete();
        $this->autocompleteType = Autocomplete::getTypeName($this);

    }

    public function getSettings()
    {
        $base = parent::getSettings();
        $base["analysis"] = [
            "index" => [
                "max_result_window"  => 2000000
            ],
            "filter" => [
                "kotka_folding" => [
                    "type" => "icu_folding",
                    "unicodeSetFilter" => "[^åäöÅÄÖ]"
                ]
            ],
            "analyzer" => array_merge(
                [
                    "default" => [
                        "tokenizer" => "standard",
                        "filter" => ["standard", "lowercase", "kotka_folding"]
                    ]
                ]
            )
        ];
        return $base;
    }

    protected function exists($uri) {
        /** @var \Elastic\Client\Search $search */
        $search = $this->serviceLocator->get('Elastic\Client\Search');
        $query = new Query(self::getIndexName(), self::getTypeName());
        $queryStr = 'documentURI:"' . $uri . '"';
        $query->setQueryString($queryStr);
        $query->setFields(['documentURI']);
        $query->setSize(1);
        $result = $search->query($query)->execute();
        return $result->count() > 0;
    }

    public function addToBulk($document, &$bulk)
    {
        $this->init();
        if (!isset($document['subject'])) {
            return;
        }

        /*
        if ($this->exists(IdService::getUri($document['subject']))) {
            return;
        }
        */
        if (isset($document['MY.datatype']) && $document['MY.datatype'] === 'accession') {
            $this->addBranchData($document, $bulk);
        }
        $this->first = true;
        $document['gatheringCount'] = 0;
        $document['unitCount'] = 0;
        $document['identificationCount'] = 0;
        $document['typeCount'] = 0;
        $id = $document['subject'];
        if (isset($document['MY.gathering'])) {
            $document['gatheringCount'] = count($document['MY.gathering']);
            foreach ($document['MY.gathering'] as $gathering) {
                $id = $gathering['subject'];
                if (isset($gathering['MY.unit'])) {
                    $document['unitCount'] = count($gathering['MY.unit']);
                    foreach ($gathering['MY.unit'] as $unit) {
                        $id = $unit['subject'];
                        $identTypes = [];
                        $unit['sampleExists'] = isset($unit['MF.sample']);
                        if (isset($unit['MY.typeSpecimen'])) {
                            $document['typeCount'] = count($unit['MY.typeSpecimen']);
                            foreach ($unit['MY.typeSpecimen'] as $k => $type) {
                                $identTypes[$k] = $type;
                            }
                        }
                        $hasPreferred = null;
                        if (isset($unit['MY.identification'])) {
                            $document['identificationCount'] = count($unit['MY.identification']);
                            if ($document['identificationCount'] > 1) {
                                $unit = StaticFilter::execute($unit, 'Kotka\Filter\SortIdentifications');
                            }
                            $hasPreferred = false;
                            foreach ($unit['MY.identification'] as $k => $identification) {
                                $identification['taxonAndInfra'] = TaxonService::extractNameFromArray($identification, true, $this->cache['MY.infraRank']);
                                if (!$hasPreferred) {
                                    $identification['accepted'] = true;
                                    $hasPreferred = true;
                                }
                                if (isset($identTypes[$k])) {
                                    $identTypes[$k] = array_replace($identTypes[$k], $identification);
                                } else {
                                    $identTypes[$k] = $identification;
                                }
                            }
                        }
                        if (count($identTypes)) {
                            foreach ($identTypes as $identType) {
                                $id = $identType['subject'];
                                $this->prepareRow($bulk, $id, $document, $gathering, $unit, $identType);
                            }
                        } else {
                            $this->prepareRow($bulk, $id, $document, $gathering, $unit);
                        }
                    }
                } else {
                    $this->prepareRow($bulk, $id, $document, $gathering);
                }
            }
        } else {
            $this->prepareRow($bulk, $id, $document);
        }

    }

    /**
     * @param $bulk
     * @param $uniqueID
     * @param $document
     * @param array $gathering
     * @param array $unit
     * @param array $identType
     * @throws \Exception
     */
    protected function prepareRow(&$bulk, $uniqueID, $document, $gathering = [], $unit = [], $identType = [])
    {
        $this->suggestions = [];
        // Store notes to new key so that all of them are saved
        if (isset($document['MY.notes'])) {
            $document['documentNotes'] = $document['MY.notes'];
            unset($document['MY.notes']);
        }
        if (isset($gathering['MY.notes'])) {
            $gathering['gatheringNotes'] = $gathering['MY.notes'];
            unset($gathering['MY.notes']);
        }
        if (isset($unit['MY.notes'])) {
            $unit['unitNotes'] = $unit['MY.notes'];
            unset($unit['MY.notes']);
        }
        if (isset($identType['MY.notes'])) {
            $identType['identificationNotes'] = $identType['MY.notes'];
            unset($identType['MY.notes']);
        }

        // Merge all array into one
        $row = array_replace($document, $gathering, $unit, $identType);

        // Parse collection tree
        if (isset($row['MY.collectionID'])) {
            $row['collectionTree'] = $this->getCollections($row['MY.collectionID']);
        }

        // Parse subject
        $this->parseSubject($document['subject'], $row);

        // Check & Parse location
        $this->checkWGSLocation($row);
        if (isset($row['MY.wgs84Latitude']) && isset($row['MY.wgs84Longitude'])) {
            $dot = strpos($row['MY.wgs84Latitude'], '.');
            $lat = (float)substr($row['MY.wgs84Latitude'], 0, $dot + 7);
            $dot = strpos($row['MY.wgs84Longitude'], '.');
            $lon = (float)substr($row['MY.wgs84Longitude'], 0, $dot + 7);
            $row['wgs84Location'] = ['lat' => $lat, 'lon' => $lon];
        }

        // Parse Taxon
        if (isset($row['MY.taxon'])) {
            $result = $this->getTaxonTree($row['MY.taxon']);
            if (isset($result['taxon']) && !empty($result['taxon'])) {
                $row['taxa'] = $result['taxon'];
            }
            if (isset($result['acceptedTaxon']) && !empty($result['acceptedTaxon'])) {
                $row['acceptedTaxon'] = $result['acceptedTaxon'];
                $row['synonyms'] = $this->getSynonyms($result['acceptedTaxon']);
            }
            if (isset($result['family']) && !empty($result['family'])) {
                $row['family'] = $result['family'];
            }
            if (isset($result['endangeredStatus'])) {
                $row['endangeredStatus'] = $result['endangeredStatus'];
            }
            $row['initialLetterOfGenus'] = mb_substr($row['MY.taxon'], 0, 1);
        }

        // Open the key value fields
        foreach ($this->keyValueFields as $field => $delimiter) {
            $fieldParts = explode('.', $field);
            $fieldNoDot = array_pop($fieldParts);
            if (isset($row[$field])) {
                if (isset($row[$fieldNoDot])) {
                    unset($row[$fieldNoDot]);
                }
                foreach ($row[$field] as $key => $value) {
                    $parts = explode($delimiter, $value, 2);
                    if (count($parts) === 2 && $parts[1] !== '') {
                        if (!isset($row[$fieldNoDot])) {
                            $row[$fieldNoDot] = [];
                        }
                        if (!isset($row[$fieldNoDot][$parts[0]])) {
                            $row[$fieldNoDot][$parts[0]] = [];
                        }
                        $row[$fieldNoDot][$parts[0]][] = $parts[1];
                    } else {
                        unset($row[$field][$key]);
                    }
                }
            }
        }

        // Analyze measurement class
        if (isset($row['MY.measurementClass'])) {
            $measurementResult = [];
            foreach ($row['MY.measurementClass'] as $measurementClass) {
                foreach ($measurementClass as $measurement => $value) {
                    if (in_array($measurement, ['subject', 'type', 'MY.isPartOf'])) {
                        continue;
                    }
                    $measurementParts = explode('.', $measurement);
                    $fieldNoDot = array_pop($measurementParts);
                    $measurementResult[$fieldNoDot] = $value;
                }
            }
            $row['MY.measurement'] = $measurementResult;
            unset($row['MY.measurementClass']);
        }

        // Fields that need special treatment
        if (isset($row['MY.bold'])) {
            $row['boldID'] = [];
            $row['boldDescription'] = [];
            foreach ($row['MY.bold'] as $key => $value) {
                $ids = explode(';', $value);
                foreach ($ids as $id) {
                    $parts = explode(':', $id, 2);
                    if (count($parts) == 2) {
                        $row['boldID'][] = trim($parts[0]);
                        $row['boldDescription'][] = trim($parts[1]);
                    } else {
                        $row['boldID'][] = trim($parts[0]);
                    }
                }
            }
        }
        if (isset($row['MY.genbank'])) {
            $row['genbankID'] = [];
            $row['genbankDescription'] = [];
            foreach ($row['MY.genbank'] as $key => $value) {
                $ids = explode(';', $value);
                foreach ($ids as $id) {
                    $parts = explode(':', $id, 2);
                    if (count($parts) == 2) {
                        $row['genbankID'][] = trim($parts[0]);
                        $row['genbankDescription'][] = trim($parts[1]);
                    } else {
                        $row['genbankID'][] = trim($parts[0]);
                    }
                }
            }
        }

        // Check if it's type specimen
        $row['isType'] = (isset($row['MY.typeStatus']) && $row['MY.typeStatus'] !== 'MY.typeStatusNo');

        // Open the values
        $this->openIds($row);

        // Check if has a picture
        $cnt = $this->getPictureCount($document['subject']);
        $row['hasPicture'] = ($cnt > 0);
        $row['pictureCount'] = $cnt;

        // Remove un needed
        foreach ($this->remove as $field) {
            if (isset($row[$field])) {
                unset($row[$field]);
            }
        }

        // Map keys to new ones
        foreach ($this->map as $from => $to) {
            if (isset($row[$from])) {
                $row[$to] = $row[$from];
                unset($row[$from]);
            }
        }

        // Remove ns && add autocomplete fields
        $data = [];
        $autocompleteFields = [];
        foreach ($row as $key => $value) {
            $dotPos = strpos($key, '.');
            $suggestions = null;
            if ($dotPos) {
                $key = substr($key, $dotPos + 1);
            }
            $data[$key] = $value;
            if (isset($this->autocomplete[$key])) {
                if ($this->autocomplete[$key] === true) {
                    $autocompleteFields[] = $key;
                }
            }
        }

        // remove domain prefix
        foreach($this->removePrefix as $field) {
            if (!isset($data[$field])) {
                continue;
            }
            $data[$field] = $this->removePrefix($data[$field]);
        }

        // These have to be done after facet building!

        // Parse gathering date
        if (isset($data['dateBegin'])) {
            if (!isset($data['dateEnd'])) {
                $data['dateEnd'] = $data['dateBegin'];
            }
            $data['dateBegin'] = $this->DateToElasticFormat($data['dateBegin']);
        }
        if (isset($data['dateEnd'])) {
            $data['dateEnd'] = $this->DateToElasticFormat($data['dateEnd'], '31.12');
        }

        // Parse create & edit times
        if (isset($data['dateCreated'])) {
            $data['dateCreated'] = $this->DateTimeToElasticFormat($data['dateCreated']);
        }
        if (isset($data['dateEdited'])) {
            $data['dateEdited'] = $this->DateTimeToElasticFormat($data['dateEdited']);
        }

        // Parse acquisition date
        if (isset($data['acquisitionDate'])) {
            $data['acquisitionDate'] = $this->DateToElasticFormat($data['acquisitionDate']);
        }

        // Remove new lines from text area
        foreach ($this->removeNewLine as $field) {
            if (!isset($data[$field]) || !is_string($data[$field])) {
                continue;
            }
            $data[$field] = str_replace(["\n", "\r"], [' ', ''], $data[$field]);
        }

        // Marks the first
        $data['firstInDocument'] = $this->first;
        if ($this->first) {
            $this->first = false;
        }
        if (isset($data['sampleExists']) && $data['sampleExists']) {
            if ((isset($data['accepted']) && $data['accepted']) || $data['identificationCount'] === 0) {
                $this->addSampleData($data, $bulk, $unit['MF.sample']);
            }
            unset($data['sample']);
        }

        $this->autocompleteExtract->addToBulk($data, $bulk, $this->autocompleteType, $autocompleteFields);

        $bulk['body'][] = [
            'index' => [
                '_index' => self::getIndexName(),
                '_type' => self::getTypeName(),
                '_id' => $uniqueID
            ]
        ];
        $bulk['body'][] = $data;
    }

    protected function openIds(&$row) {
        foreach ($this->open as $field => $db) {
            if (!isset($row[$field])) {
                continue;
            }
            if (isset($this->multi[$field])) {
                foreach ($row[$field] as $key => $value) {
                    if (isset($this->copyOpenedIDTo[$field])) {
                        $row[$this->copyOpenedIDTo[$field]][$key] = $value;
                    }
                    if (is_array($value)) {
                        $row[$field][$key] = [];
                        foreach ($value as $val) {
                            if (isset($this->cache[$db]) && isset($this->cache[$db][$val])) {
                                $row[$field][$key][] = $this->cache[$db][$val];
                            }
                        }
                    } else {
                        if (isset($this->cache[$db]) && isset($this->cache[$db][$value])) {
                            $row[$field][$key] = $this->cache[$db][$value];
                        }
                    }
                }
            } else {
                if (isset($this->copyOpenedIDTo[$field])) {
                    $row[$this->copyOpenedIDTo[$field]] = $row[$field];
                }
                if (isset($this->cache[$db]) && isset($this->cache[$db][$row[$field]])) {
                    $row[$field] = $this->cache[$db][$row[$field]];
                }
            }
        }
    }

    /**
     * @param $document
     * @param $bulk
     * @param $samples
     * @throws \Exception
     */
    protected function addSampleData(&$document, &$bulk, $samples) {

        if (is_array($samples)) {
            foreach ($this->copyFromSample as $key) {
                $document[$key] = [];
            }
            foreach ($samples as $sample) {
                $result = $this->getSampleExtract()->addToBulk($sample, $bulk, $document);
                foreach ($this->copyFromSample as $key) {
                    if (isset($result[$key])) {
                        if (is_array($result[$key])) {
                            $document[$key] = array_merge($document[$key], $result[$key]);
                        } else {
                            array_push($document[$key], $result[$key]);
                        }
                    }
                }
            }
            foreach ($this->copyFromSample as $key) {
                $document[$key] = $this->getUniqueValues($document[$key]);
            }
        }
    }

    private function getUniqueValues($value) {
        return array_values(array_unique(array_filter($value)));
    }

    /**
     * @param $document
     * @param $bulk
     * @throws \Exception
     */
    protected function addBranchData(&$document, &$bulk)
    {
        /** @var AccessionService $branchService */
        $accessionService = $this->getAccessionService();
        $results = $accessionService->getAllBranchesByRoot($document['subject'], false);
        $document['branchLocationTree'] = [];
        $document['branchLocationID'] = [];
        $document['branchLocation'] = [];
        $document['branchExistsInLocationID'] = [];
        $document['branchExistsInLocation'] = [];
        $document['branchID'] = [];
        $document['branchSubLocation'] = [];
        $document['branchNotes'] = [];
        $exists = false;
        $branches = $results->getBranches();
        foreach ($branches as $branch) {
            /** @var PUUBranchInterface $branch */
            $document['branchLocationID'][] = $branch->getPUUCollectionID();
            if (isset($this->cache['MY.collectionID'][$branch->getPUUCollectionID()])) {
                $document['branchLocation'][] = $this->cache['MY.collectionID'][$branch->getPUUCollectionID()];
                $document['branchLocationTree'] = array_merge($document['branchLocationTree'], $this->getCollections($branch->getPUUCollectionID()));
            }
            if ($branch->getPUUExists()) {
                $document['branchExistsInLocationID'][] = $branch->getPUUCollectionID();
                if (isset($this->cache['MY.collectionID'][$branch->getPUUCollectionID()])) {
                    $document['branchExistsInLocation'][] = $this->cache['MY.collectionID'][$branch->getPUUCollectionID()];
                }
                $exists = true;
            }
            $document['branchID'][] = $branch->getSubject();
            $document['branchSubLocation'][] = $branch->getPUULocation();
            $document['branchNotes'][] = $branch->getPUUNotes();
            $this->getBranchExtract()->addPUUBranch($branch, $bulk);
        }
        $document['branchLocationTree'] = $this->getUniqueValues($document['branchLocationTree']);
        $document['branchLocationID'] = $this->getUniqueValues($document['branchLocationID']);
        $document['branchLocation'] = $this->getUniqueValues($document['branchLocation']);
        $document['branchExistsInLocationID'] = $this->getUniqueValues($document['branchExistsInLocationID']);
        $document['branchExistsInLocation'] = $this->getUniqueValues($document['branchExistsInLocation']);
        $document['branchID'] = $this->getUniqueValues($document['branchID']);
        $document['branchSubLocation'] = $this->getUniqueValues($document['branchSubLocation']);
        $document['branchNotes'] = $this->getUniqueValues($document['branchNotes']);
        $document['branchExists'] = $exists;
    }

    protected function removePrefix($value) {
        if (empty($value)) {
            return $value;
        }
        if (is_array($value)) {
            $value = array_map(array($this, 'removePrefix'), $value);
        }
        if (is_string($value)) {
            $pos = strpos(':', $value);
            if ($pos !== false) {
                return substr($value, $pos + 1);
            }
        }
        return $value;
    }

    public function init()
    {
        if (isset($this->init['all'])) {
            return;
        }
        $this->initCache();
        $this->initImages();
        $this->initTaxa();
        $this->initCollectionTree();
        $this->init['all'] = true;
    }

    public function getRelatedExtracts()
    {
        return [
            $this->autocompleteExtract,
            $this->getBranchExtract(),
            $this->getSampleExtract()
        ];
    }

    protected function getTaxonTree($taxon)
    {
        if (isset($this->taxa[$taxon])) {
            return $this->taxa[$taxon];
        }
        $acceptedTaxon = $this->getAcceptedTaxon($taxon);
        if (isset($this->taxa[$acceptedTaxon])) {
            return $this->taxa[$acceptedTaxon];
        }
        return null;
    }

    protected function getAcceptedTaxon($taxon) {
        $upperTaxon = strtoupper($taxon);
        if (!isset($this->synonyms[$upperTaxon])) {
            $this->synonyms[$upperTaxon] = $this->getTaxonService()->getCurrentName($upperTaxon);
        }
        return $this->synonyms[$upperTaxon];
    }

    protected function getSynonyms($taxon) {
        return $this->getUniqueValues($this->getTaxonService()->getSynonyms($taxon));
    }

    /**
     * @return \Kotka\Service\TaxonService
     */
    protected function getTaxonService()
    {
        if ($this->taxonService === null) {
            $this->taxonService = $this->getServiceLocator()->get('Kotka\Service\TaxonService');
        }
        return $this->taxonService;
    }

    protected function getPictureCount($subject)
    {
        $uri = IdService::getUri($subject);
        return isset($this->images[$uri]) ? $this->images[$uri] : 0;
    }

    protected function getCollections($collection)
    {
        if (isset($this->collections[$collection])) {
            return $this->collections[$collection];
        }
        return null;
    }

    protected function initCollectionTree()
    {
        if (isset($this->init['collection'])) {
            return;
        }
        $cache = $this->getCache();
        $cacheKey = 'extract-collection';
        $cache->removeItem($cacheKey);
        if (!$cache->hasItem($cacheKey)) {
            $partOf = [];
            $collections = [];
            $connection = $this->getDb();
            $dbName = $connection->getDatabase();
            $connection->setUseResultSet(true);
            $sql = "SELECT c.SUBJECTNAME, p.OBJECTNAME FROM $dbName.RDF_STATEMENTVIEW c
                    LEFT JOIN $dbName.RDF_STATEMENTVIEW p ON (c.SUBJECTNAME = p.SUBJECTNAME AND p.PREDICATENAME = 'MY.isPartOf')
                    WHERE c.PREDICATENAME = 'rdf:type' AND c.OBJECTNAME = 'MY.collection'";
            $results = $connection->executeSelectSql($sql);
            foreach ($results as $result) {
                $partOf[$result['SUBJECTNAME']] = $result['OBJECTNAME'];
            }
            foreach ($partOf as $collection => $parent) {
                $collections[$collection] = $this->getCollectionList($collection, $partOf, [$collection]);
            }
            $cache->setItem($cacheKey, $collections);
        }
        $this->collections = $cache->getItem($cacheKey);
        $this->init['collection'] = true;
    }

    protected function getCollectionList($root, $partOd, $result = [])
    {
        if (!isset($partOd[$root])) {
            return $result;
        }
        $result[] = $partOd[$root];
        return $this->getCollectionList($partOd[$root], $partOd, $result);
    }

    protected function initTaxa()
    {
        if (isset($this->init['taxa'])) {
            return;
        }
        $cache = $this->getCache();
        $cacheKey = 'extract-taxa';
        if (!$cache->hasItem($cacheKey)) {
            $endangered = $this->getEndangered();
            $taxa = [];
            $connection = $this->getDb();
            $connection->setUseResultSet(true);
            $names = [];
            $sql = "SELECT
                      STEP,
                      SCIENTIFICNAME,
                      RANK,
                      TAXON_QNAME
                    FROM LTKM_LUONTO.RDF_TAXON_PARENTS_CHILDREN WHERE TAXON_QNAME IN (
                      SELECT TAXON_QNAME
                      FROM LTKM_LUONTO.RDF_TAXON_NAMES
                      WHERE SOURCE = 'MR.1'
                    ) AND STEP > -1 ORDER BY STEP";
            $results = $connection->executeSelectSql($sql);
            foreach ($results as $result) {
                $qname = $result['TAXON_QNAME'];
                if ($result['STEP'] == 0) {
                    $names[$qname] = $result['SCIENTIFICNAME'];
                    $taxa[$names[$qname]]['acceptedTaxon'] = $names[$qname];
                    if (isset($endangered[$qname])) {
                        $taxa[$names[$qname]]['endangeredStatus'] = $endangered[$qname];
                    }
                }
                if (!isset($names[$qname])) {
                    continue;
                }
                $name = $names[$qname];
                if ($result['RANK'] === 'MX.family') {
                    $taxa[$names[$qname]]['family'] = $result['SCIENTIFICNAME'];
                }
                $taxa[$name]['taxon'][] = $result['SCIENTIFICNAME'];
            }
            $cache->setItem($cacheKey, $taxa);
        }
        $this->taxa = $cache->getItem($cacheKey);
        $this->init['taxa'] = true;
    }

    protected function getEndangered()
    {
        $cache = $this->getCache();
        $cacheKey = 'endangered-species';
        if (!$cache->hasItem($cacheKey)) {
            /** @var \Kotka\Service\FormElementService $formElementService */
            $formElementService = $this->getServiceLocator()->get('Kotka\Service\FormElementService');
            $connection = $this->getDb();
            $connection->setUseResultSet(true);
            $sql = "SELECT SUBJECTNAME as \"qname\", OBJECTNAME as \"level\" FROM LTKM_LUONTO.RDF_STATEMENTVIEW where PREDICATENAME = 'MX.redListStatus2019Finland'
                        AND OBJECTNAME IN (
                          'MX.iucnEX', 'MX.iucnEW', 'MX.iucnRE', 'MX.iucnCR', 'MX.iucnEN','MX.iucnVU','MX.iucnNT'
                        )
                    UNION
                    SELECT SUBJECTNAME as \"qname\", OBJECTNAME as \"level\" FROM LTKM_LUONTO.RDF_STATEMENTVIEW where PREDICATENAME = 'MX.redListStatus2015Finland'
                    AND OBJECTNAME IN (
                          'MX.iucnEX', 'MX.iucnEW', 'MX.iucnRE', 'MX.iucnCR', 'MX.iucnEN','MX.iucnVU','MX.iucnNT'
                      )";
            $results = $connection->executeSelectSql($sql);
            $labels = $formElementService->getSelect('MX.redListStatus2019Finland');
            $endangered = [];
            foreach ($results as $result) {
                $label = $result['level'];
                if (isset($labels[$label])) {
                    $label = $labels[$label];
                }
                $endangered[$result['qname']] = $label;
            }
            $cache->setItem($cacheKey, $endangered);
        }
        return $cache->getItem($cacheKey);
    }

    protected function initImages()
    {
        if (isset($this->init['images'])) {
            return;
        }
        $cache = $this->getCache();
        $cacheKey = 'extract-images';
        if (!$cache->hasItem($cacheKey)) {
            $connection = $this->getDb();
            $dbName = $connection->getDatabase();
            $connection->setUseResultSet(true);
            $sql = "SELECT RESOURCELITERAL as \"document\", count(*) AS \"count\" FROM LTKM_LUONTO.RDF_STATEMENTVIEW WHERE PREDICATENAME = 'MM.documentURI' AND RESOURCELITERAL in (
                      SELECT SUBJECTURI from $dbName.RDF_STATEMENTVIEW WHERE PREDICATENAME = 'rdf:type' AND OBJECTNAME = 'MY.document'
                    ) GROUP BY RESOURCELITERAL";
            $results = $connection->executeSelectSql($sql);
            $images = [];
            foreach ($results as $result) {
                $images[$result['document']] = $result['count'];
            }
            $cache->setItem($cacheKey, $images);
        }

        $this->images = $cache->getItem($cacheKey);
        $this->init['images'] = true;
    }

    protected function initCache()
    {
        if (isset($this->init['cache'])) {
            return;
        }
        /** @var \Triplestore\Service\ObjectManager $om */
        $om = $this->getServiceLocator()->get('Triplestore\ObjectManager');
        /** @var \Triplestore\Model\Properties $properties */
        $properties = $om->getMetadataService()->getAllProperties();
        /** @var \Kotka\Service\FormElementService $elementService */
        $elementService = $this->getServiceLocator()->get('Kotka\Service\FormElementService');

        foreach ($this->open as $field => $db) {
            $param = $properties->getOriginal($field) ?: $field;
            if ($properties->hasMany($param)) {
                $this->multi[$field] = true;
            }
            if (!isset($this->cache[$db])) {
                $values = $elementService->getSelect($db);
                if (isset($values[FormElementService::ALIAS_KEY])) {
                    unset($values[FormElementService::ALIAS_KEY]);
                }
                $this->cache[$db] = $values;
            }
        }
        $this->init['cache'] = true;
    }

    protected function getBranchExtract() {
        if (!$this->branchExtract) {
            $this->branchExtract = new Branch();
            $this->branchExtract->setServiceLocator($this->getServiceLocator());
        }
        return $this->branchExtract;
    }

    /**
     * @return Sample
     */
    protected function getSampleExtract() {
        if (!$this->sampleExtract) {
            $this->sampleExtract = new Sample();
            $this->sampleExtract->setServiceLocator($this->getServiceLocator());
        }
        return $this->sampleExtract;
    }

    /**
     * @return \Triplestore\Db\Oracle
     */
    protected function getDb()
    {
        if ($this->db === null) {
            /** @var \Triplestore\Service\Objectmanager $om */
            $om = $this->getServiceLocator()->get('Triplestore\ObjectManager');
            $this->db = $om->getConnection();
        }
        return $this->db;
    }

    /**
     * @return AccessionService
     */
    protected function getAccessionService()
    {
        if ($this->accessionService === null) {
            $this->accessionService = $this->getServiceLocator()->get('Kotka\Service\AccessionService');
        }
        return $this->accessionService;
    }

    /**
     * @return StorageInterface
     */
    protected function getCache()
    {
        return $this->serviceLocator->get('Cache');
    }

    public static function getTypeName()
    {
        return static::TYPE;
    }

    public static function getIndexName()
    {
        return static::INDEX;
    }

    protected function checkWGSLocation(&$row)
    {
        if (isset($row['MY.wgs84Latitude']) &&
            (!($row['MY.wgs84Latitude'] <= 90.0 && $row['MY.wgs84Latitude'] >= -90.0) ||
            !is_numeric($row['MY.wgs84Latitude']))
        ) {
            unset($row['MY.wgs84Latitude']);
            if (isset($row['MY.wgs84Longitude'])) {
                unset($row['MY.wgs84Longitude']);
            }
            return;
        }
        if (isset($row['MY.wgs84Longitude']) &&
            (!($row['MY.wgs84Longitude'] <= 180.0 && $row['MY.wgs84Longitude'] >= -180.0) ||
            !is_numeric($row['MY.wgs84Longitude']))
        ) {
            unset($row['MY.wgs84Longitude']);
            if (isset($row['MY.wgs84Latitude'])) {
                unset($row['MY.wgs84Latitude']);
            }
        }

    }

    /**
     * @return string
     */
    public function getIndex()
    {
        return static::INDEX;
    }
}