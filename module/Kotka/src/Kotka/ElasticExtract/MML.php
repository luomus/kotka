<?php

namespace Kotka\ElasticExtract;


use Elastic\Extract\AbstractExtract;
use Elastic\Query\Query;
use Zend\Db\Adapter\Driver\Pdo\Pdo;
use Zend\Dom\Document;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

abstract class MML extends Location
{
    protected static $type = Municipality::TYPE;

    private static $coordinates = [
        'lat' => [],
        'lon' => [],
    ];

    private $langConversions = [
        'fin' => 'fi',
        'swe' => 'sv',
        'eng' => 'en'
    ];

    private static $adapter;

    public function addToBulk($object, &$bulk)
    {
        $data = [];
        if (!$object instanceof \DOMElement) {
            throw new \Exception('Invalid type date given to extractor');
        }

        $name = $this->getName($object);
        if (empty($name)) {
            throw new \Exception('Could not find name from the given DOM element');
        }
        $data['name'] = $name;
        if (empty($data['name']['fi']) || $this->exists($data['name']['fi'])) {
            //echo "\nexists\n\n";
            return;
        }
        $data['border'] = $this->getBorders($object);
        $data['boundingBox'] = ['type' => 'envelope', 'coordinates' => $this->getBoundingBoxCoordinates($object)];
        parent::addToBulk($data, $bulk);
    }

    private function exists($name) {
        /** @var \Elastic\Client\Search $search */
        $search = $this->serviceLocator->get('Elastic\Client\Search');
        $query = new Query(self::getIndexName(), self::getTypeName());
        $queryStr = 'name.fi:"' . $name . '"';
        $query->setQueryString($queryStr);
        $query->setFields(['name']);
        $result = $search->query($query)->execute();
        if ($result->count() > 1) {
            echo "\n\nDOUBLE $name " . $result->count() . "\n\n";
        }
        return $result->count() > 0;
    }

    protected function getNationalLevel(\DOMElement $object)
    {
        $national = $this->getNodeValue(['nationalLevel'], $object);
        if (count($national) > 0) {
            return array_pop($national);
        }
        return '';
    }

    protected function getName($object)
    {
        $nameElements = $object->getElementsByTagName('name');
        $names = [];
        foreach ($nameElements as $nameElement) {
            if (!$nameElement instanceof \DOMElement) {
                continue;
            }
            $lang = $this->getNodeValue(['language'], $nameElement);
            $lang = array_pop($lang);
            $value = $this->getNodeValue(['SpellingOfName', 'text'], $nameElement);
            $value = array_pop($value);
            if (count($value) > 1) {
                throw new \Exception('Cannot values that have multiple values');
            }
            if (isset($this->langConversions[$lang])) {
                $names[$this->langConversions[$lang]] = $value;
            } else {
                throw new \Exception('Invalid language found! ' . $lang);
            }
        }
        return $names;
    }

    protected function getBorders(\DOMElement $domElement)
    {
        $locationElements = $this->getDomElements(['geometry', 'surfaceMember' => ['xlink:role' => 'main']], $domElement);
        $enclaveElements = $this->getDomElements(['geometry', 'surfaceMember' => ['xlink:role' => 'enclave']], $domElement);
        $result = [];
        $externalPolygon = [];
        $multi = [];
        $existing = [];
        $idx = 0;
        if (count($locationElements) > 1) {
            throw new \Exception('There is too many main location elements found!!!');
        }
        foreach ($locationElements as $element) {
            $points = $this->getNodeValue(['exterior', 'posList'], $element);
            foreach ($points as $point) {
                $parts = explode(' ', $point);
                while (!empty($parts)) {
                    $lon = array_shift($parts);
                    $lat = array_shift($parts);
                    $coordinates = $this->convertEurefToWgs($lat, $lon);
                    $check = implode(':', $coordinates);
                    $externalPolygon[$idx] = $coordinates;
                    if (isset($existing[$check]) && $existing[$check] !== 0) {
                        $multi[] = array_splice($externalPolygon, $existing[$check]);
                        $idx = count($externalPolygon);
                        $externalPolygon[$idx] = $coordinates;
                    } else {
                        $existing[implode(':', $coordinates)] = $idx;
                    }
                    $idx++;
                }
            }
        }
        $result[] = $externalPolygon;

        foreach ($locationElements as $element) {
            $interior = $this->getDomElements(['interior'], $element);
            foreach ($interior as $interiorElement) {
                $points = $this->getNodeValue(['posList'], $interiorElement);
                $internalPolygon = [];
                foreach ($points as $point) {
                    $parts = explode(' ', $point);
                    while (!empty($parts)) {
                        $lon = array_shift($parts);
                        $lat = array_shift($parts);
                        $internalPolygon[] = $this->convertEurefToWgs($lat, $lon);
                    }
                }
                if (count($internalPolygon)) {
                    $result[] = $internalPolygon;
                }
            }
        }

        if (count($multi)) {
            $coordinates = [$result];
            foreach ($multi as $coord) {
                $coordinates[] = [$coord];
            }
            $main = ['type' => 'multipolygon', 'coordinates' => $coordinates];
        } else {
            $main = ['type' => 'polygon', 'coordinates' => $result];
        }

        $geometries = [$main];

        foreach ($enclaveElements as $element) {
            $exterior = $this->getDomElements(['exterior'], $element);
            foreach ($exterior as $ext) {
                $points = $this->getNodeValue(['posList'], $ext);
                $satellite = [];
                foreach ($points as $point) {
                    $parts = explode(' ', $point);
                    while (!empty($parts)) {
                        $lon = array_shift($parts);
                        $lat = array_shift($parts);
                        $coordinates = $this->convertEurefToWgs($lat, $lon);
                        $satellite[] = $coordinates;
                    }
                }
                if (count($satellite)) {
                    $geometries[] = ['type' => 'polygon', 'coordinates' => [$satellite]];
                }
            }

        }

        if (count($geometries) > 1) {
            return [
                'type' => 'geometrycollection',
                'geometries' => $geometries
            ];
        }

        return $main;
    }

    private function getDomElements(array $path, \DOMElement $domElement)
    {
        $origKey = key($path);
        $level = array_shift($path);
        $hasAttr = false;
        $attr = '';
        $attrValue = '';
        if (is_array($level)) {
            $hasAttr = true;
            $attr = key($level);
            $attrValue = $level[$attr];
            $level = $origKey;
        }
        $elements = $domElement->getElementsByTagName($level);
        $result = [];
        foreach ($elements as $element) {
            if ($element instanceof \DOMElement) {
                if ($hasAttr) {
                    $role = $element->getAttribute($attr);
                    if ($role !== $attrValue) {
                        continue;
                    }
                }
                if (empty($path)) {
                    $result[] = $element;
                } else {
                    $result = array_merge($result, $this->getDomElements($path, $element));
                }
            }
        }
        return $result;
    }


    protected function getNodeValue(array $path, \DOMElement $domElement, &$result = [])
    {
        $result = [];
        foreach ($this->getDomElements($path, $domElement) as $element) {
            $result[] = $element->nodeValue;
        }
        return $result;
    }

    protected function getBoundingBoxCoordinates($object)
    {
        $lower = $this->getNodeValue(['boundedBy', 'lowerCorner'], $object);
        $box = [];
        foreach ($lower as $i) {
            $box[] = $this->getCoordinate($i);
        }
        $upper = $this->getNodeValue(['boundedBy', 'upperCorner'], $object);
        foreach ($upper as $i) {
            $box[] = $this->getCoordinate($i);
        }
        return $box;
    }

    protected function getCoordinate($data)
    {
        list($lon, $lat) = explode(' ', $data);
        return $this->convertEurefToWgs($lat, $lon);
    }

    protected function convertEurefToWgs($lat, $lon)
    {
        $key = $lat . ':' . $lon;
        if (isset(self::$coordinates[$key])) {
            return self::$coordinates[$key];
        }
        $query = "select Luonnos2.MML.GetPointLat(2,1,?,?) as lat, Luonnos2.MML.GetPointLon(2,1,?,?) as lon";
        $adapter = $this->getAdapter();
        $results = $adapter->query($query, [$lat, $lon, $lat, $lon]);
        foreach ($results as $result) {
            return [(float)$result['lon'], (float)$result['lat']];
        }
        throw new \Exception('Could not convert coordinates to wgs!');
    }

    /**
     * @return Pdo
     */
    protected function getAdapter()
    {
        if (self::$adapter === null) {
            self::$adapter = $this->getServiceLocator()->get('mustikka_adapter');;
        }
        return self::$adapter;
    }
}