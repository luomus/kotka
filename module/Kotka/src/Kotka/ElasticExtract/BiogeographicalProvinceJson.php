<?php

namespace Kotka\ElasticExtract;


class BioGeoGraphicalProvinceJson extends Location
{
    const TYPE = 'biogeographicalProvince';

    protected static $type = self::TYPE;

    private static $adapter;

    private static $coordinates = [
        'lat' => [],
        'lon' => [],
    ];

    /**
     * Indexes natura areas geojsons feature group object
     *
     * @param $object
     * @param $bulk
     * @throws \Exception
     */
    public function addToBulk($object, &$bulk)
    {
        $data = [];
        if (!isset($object['type']) || $object['type'] !== 'Feature' || !isset($object['properties'])
            || !isset($object['geometry'])
        ) {
            throw new \Exception("Object given doesn't seem to be geoJsons feature object");
        }
        $prop = $object['properties'];
        $data['name'] = ['fi' => ''];
        $data['description'] = [];
        if (isset($prop['MAAKUNTALYH_FI'])) {
            $data['name']['fi'] = $prop['MAAKUNTALYH_FI'];
        }
        if (isset($prop['MAAKUNTALYH_LAT'])) {
            $data['name']['la'] = $prop['MAAKUNTALYH_LAT'];
        }
        if (isset($prop['MAAKUNTA_FI'])) {
            $data['description']['fi'] = $prop['MAAKUNTA_FI'];
        }
        if (isset($prop['MAAKUNTA_LAT'])) {
            $data['description']['la'] = $prop['MAAKUNTA_LAT'];
        }
        if (isset($prop['MAAKUNTA_E'])) {
            $data['description']['en'] = $prop['MAAKUNTA_E'];
        }
        //$this->fixBoarders($object['geometry']);
        $data['border'] = $object['geometry'];
        $data['boundingBox'] = ['type' => 'envelope', 'coordinates' => $this->getBoundingBoxCoordinates($data['border'])];
        $data['qname'] = isset($prop['naturatunn']) ? 'syke:' . $prop['naturatunn'] : '';

        parent::addToBulk($data, $bulk);
    }


    private function fixBoarders(& $coordinates) {
        $check = false;
        foreach ($coordinates as $key => &$value) {
            if (is_numeric($value[0]) && is_numeric($value[1])) {
                $value[0] = $this->roundNumbers($value[0]);
                $value[1] = $this->roundNumbers($value[1]);
                $check = true;
                if (!isset($coordinates[($key - 1)])) {
                    continue;
                }
                $prev = $coordinates[($key - 1)];
                if ($prev[0] === $value[0] && $prev[1] === $value[1]) {
                    unset($coordinates[$key]);
                }
            } elseif (is_array($value)) {
                $this->fixBoarders($value);
            }
        }
        if ($check) {
            $coordinates = array_values($coordinates);
        }
    }

    private function roundNumbers($value) {
        return $value;
        //return number_format($value, 6);
    }

}