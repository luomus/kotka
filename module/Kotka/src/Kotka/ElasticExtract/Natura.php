<?php

namespace Kotka\ElasticExtract;


use Elastic\Query\Query;
use Zend\Db\Adapter\Driver\Pdo\Pdo;

class Natura extends Location
{
    const TYPE = 'natura';

    protected static $type = self::TYPE;

    protected static $addQname = false;

    private static $coordinates = [
        'lat' => [],
        'lon' => [],
    ];

    private static $adapter;

    /**
     * Indexes natura areas geojsons feature group object
     *
     * @param $object
     * @param $bulk
     * @throws \Exception
     */
    public function addToBulk($object, &$bulk)
    {
        $data = [];
        if (!isset($object['type']) || $object['type'] !== 'Feature' || !isset($object['properties'])
            || !isset($object['geometry'])
        ) {
            throw new \Exception("Object given doesn't seem to be geoJsons feature object");
        }
        $prop = $object['properties'];
        if (isset($prop['Nimi'])) {
            $data['name'] = [
                'fi' => $prop['Nimi']
            ];
        }
        if (!isset($prop['naturatunn'])) {
            throw new \Exception("Missing natura id!");
        }

        //$this->convertToWgs($object['geometry']);
        $data['border'] = $object['geometry'];
        $data['boundingBox'] = ['type' => 'envelope', 'coordinates' => $this->getBoundingBoxCoordinates($data['border'])];
        $data['qname'] = 'syke:' . $prop['naturatunn'];

        if ($this->exists($data['qname'])) {
            return;
        }

        //$filename = 'natura-' . $prop['Nimi'] . '.json';
        //file_put_contents($filename, json_encode($data['border']));

        parent::addToBulk($data, $bulk);
        sleep(30);
    }

    private function exists($qname) {
        /** @var \Elastic\Client\Search $search */
        $search = $this->serviceLocator->get('Elastic\Client\Search');
        $query = new Query(self::getIndexName(), self::getTypeName());
        $queryStr = 'qname:"' . $qname . '"';
        $query->setQueryString($queryStr);
        $query->setFields(['qname']);
        $result = $search->query($query)->execute();
        return $result->count() > 0;
    }

    private function convertToWgs(&$border) {
        if (is_array($border)) {
            if (isset($border[0]) && is_numeric($border[0])) {
                $border = $this->getCoordinate($border);
            } else {
                foreach($border as &$value) {
                    $this->convertToWgs($value);
                }
            }
        }
    }


    protected function getCoordinate($data)
    {
        return $this->convertEurefToWgs($data[1], $data[0]);
    }

    protected function convertEurefToWgs($lat, $lon)
    {
        $key = $lat . ':' . $lon;
        if (isset(self::$coordinates[$key])) {
            return self::$coordinates[$key];
        }
        $query = "select Luonnos2.MML.GetPointLat(2,1,?,?) as lat, Luonnos2.MML.GetPointLon(2,1,?,?) as lon";
        $adapter = $this->getAdapter();
        $results = $adapter->query($query, [$lat, $lon, $lat, $lon]);
        foreach ($results as $result) {
            return [(float)$result['lon'], (float)$result['lat']];
        }
        throw new \Exception('Could not convert coordinates to wgs!');
    }

    /**
     * @return Pdo
     */
    protected function getAdapter()
    {
        if (self::$adapter === null) {
            self::$adapter = $this->getServiceLocator()->get('mustikka_adapter');;
        }
        return self::$adapter;
    }

}