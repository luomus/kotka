<?php

namespace Kotka\ElasticExtract;


class Region extends MML
{
    const TYPE = 'region';

    protected static $type = self::TYPE;

    protected static $addQname = false;

    public function addToBulk($object, &$bulk)
    {
        $nationalLevel = parent::getNationalLevel($object);
        if ($nationalLevel !== '3rdOrder') {
            return;
        }

        parent::addToBulk($object, $bulk);
    }


}