<?php

namespace Kotka\ElasticExtract;

use Elastic\Extract\AbstractExtract;
use Zend\Json\Json;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

abstract class Location extends AbstractExtract implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    protected static $index = 'location_1';

    protected static $addQname = true;

    protected static $areaCache;

    public function getMapping()
    {
        return [
            "date_detection" => false,
            "_source" => [
                "excludes" => [
                    "border",
                ]
            ],
            'properties' => [
                "name" => [
                    "type" => "object",
                    "properties" => [
                        'fi' => ['type' => 'string'],
                        'en' => ['type' => 'string'],
                        'sv' => ['type' => 'string'],
                        'la' => ['type' => 'string'],
                    ]
                ],
                "description" => [
                    "type" => "object",
                    "properties" => [
                        'fi' => ['type' => 'string'],
                        'en' => ['type' => 'string'],
                        'sv' => ['type' => 'string'],
                        'la' => ['type' => 'string'],
                    ]
                ],
                "boundingBox" => [
                    "type" => "geo_shape"
                ],
                "border" => [
                    "type" => "geo_shape",
                    "tree"=> "geohash",
                    "precision"=> "5m"
                ],
                "qname" => [
                    "type" => "string"
                ]
            ]
        ];
    }

    public function init()
    {
        if (self::$areaCache === null) {
            $this->initAreaCache();
        }
    }

    protected function initAreaCache() {
        /** @var \Triplestore\Service\ObjectManager $om */
        $om = $this->serviceLocator->get('Triplestore\ObjectManager');
        $om->disableHydrator();
        $results = $om->findBy(['rdf:type' => 'ML.area'], null, null, null, ['rdfs:label', 'ML.areaType']);
        $cache = [
            Municipality::TYPE => [],
            BioGeoGraphicalProvince::TYPE => []
        ];
        foreach($results as $qname => $result) {
            /** @var \Triplestore\Model\Model $result */
            $type = $result->getOrCreatePredicate('ML.areaType')->current()->getValue();
            if ($type === 'ML.municipality') {
                $type  = Municipality::TYPE;
                $value = mb_strtolower($result->getPredicate('rdfs:label')->getLiteralValue('fi')[0], 'UTF-8');
            } else if ($type === 'ML.biogeographicalProvince') {
                $type  = BioGeoGraphicalProvince::TYPE;
                $value = mb_strtolower($result->getPredicate('rdfs:label')->getLiteralValue('fi')[0], 'UTF-8');
            } else if ($type === 'ML.country') {
                continue;
            } else {
                continue;
            }
            $cache[$type][$value] = $qname;
        }
        self::$areaCache = $cache;
    }


    public function addToBulk($data, &$bulk)
    {
        if (static::$addQname && isset($data['name'])) {
            $nameSrc = static::$type === BioGeoGraphicalProvince::TYPE ? 'description' : 'name';
            $data['qname'] = $this->getUriFromNames($data[$nameSrc]);
        }
        //file_put_contents('geo/geo_' . $data['name']['fi'] . '.json', Json::encode($data['border']));
        parent::addToBulk($data, $bulk);
    }

    protected function getUriFromNames($names) {
        if (!isset($names['fi'])) {
            throw new \Exception('MISSING FINNISH NAME!!!');
        }
        $fiName = mb_strtolower($names['fi'], 'UTF-8');
        if (!isset(self::$areaCache[static::$type][$fiName])) {
            //self::$areaCache[static::$type][$fiName] = $fiName;
            throw new \Exception("Couldn't find qname to " . static::$type . ": " . $fiName);
        }
        return self::$areaCache[static::$type][$fiName];
    }


    protected function getBoundingBoxCoordinates($border)
    {
        $minLat = 180;
        $minLon = 180;
        $maxLat = -180;
        $maxLon = -180;
        if (!isset($border['coordinates'])) {
            throw new \Exception('missing coordinate data from the geometry');
        }
        $this->findBounds($border['coordinates'], $minLat, $minLon, $maxLat, $maxLon);

        return [[$minLon, $minLat],[$maxLon, $maxLat]];
    }

    private function findBounds($coordinates, &$minLat, &$minLon, &$maxLat, &$maxLon) {
        foreach($coordinates as $value) {
            if (is_array($value)) {
                if (is_numeric($value[0]) && is_numeric($value[1])) {
                    if ($value[0] > $maxLon) {
                        $maxLon = $value[0];
                    }
                    if ($value[0] < $minLon) {
                        $minLon = $value[0];
                    }
                    if ($value[1] > $maxLat) {
                        $maxLat = $value[1];
                    }
                    if ($value[1] < $minLat) {
                        $minLat = $value[1];
                    }
                } else {
                    $this->findBounds($value, $minLat, $minLon, $maxLat, $maxLon);
                }
            }
        }
    }


}