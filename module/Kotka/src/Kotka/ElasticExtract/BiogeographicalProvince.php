<?php

namespace Kotka\ElasticExtract;


class BioGeoGraphicalProvince extends KML
{
    const TYPE = 'biogeographicalProvince';

    protected static $type = self::TYPE;
    protected static $borders = [];

    public function __construct()
    {
        $json = json_decode(file_get_contents('./FI_FMNH_BGR_20161222_4326.geojson'), true);
        foreach ($json['features'] as $feature) {
            if (isset($feature['properties']['MAAKUNTALYH_FI'])) {
                $feature['geometry']['type'] = strtolower($feature['geometry']['type']);
                self::$borders[$feature['properties']['MAAKUNTALYH_FI']] = $feature['geometry'];
            } else {
                throw new \Exception('Missing MAAKUNTA_LYH');
            }
        }
    }


    protected function prepareData(&$data) {
        if (self::$borders[$data['name']['fi']]) {
            $data['border'] = self::$borders[$data['name']['fi']];
        } else {
            throw new \Exception('No border for ' . self::$borders[$data['name']['fi']]);
        }
    }

}