<?php

namespace Kotka\ElasticExtract;


class Municipality extends MML
{
    const TYPE = 'municipality';

    protected static $type = self::TYPE;

    public function addToBulk($object, &$bulk)
    {
        $nationalLevel = parent::getNationalLevel($object);
        if ($nationalLevel !== '4thOrder') {
            return;
        }

        parent::addToBulk($object, $bulk);
    }


}