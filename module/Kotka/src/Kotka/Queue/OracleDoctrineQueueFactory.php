<?php

namespace Kotka\Queue;

use SlmQueueDoctrine\Options\DoctrineOptions;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * DoctrineQueueFactory
 */
class OracleDoctrineQueueFactory implements FactoryInterface
{
    /**
     * {@inheritDoc}
     */
    public function createService(ServiceLocatorInterface $serviceLocator, $name = '', $requestedName = '')
    {
        $parentLocator = $serviceLocator->getServiceLocator();

        $config = $parentLocator->get('Config');
        $queuesOptions = $config['slm_queue']['queues'];
        $options = isset($queuesOptions[$requestedName]) ? $queuesOptions[$requestedName] : array();
        $queueOptions = new DoctrineOptions($options);

        /** @var $connection \Doctrine\DBAL\Connection */
        $connection = $parentLocator->get($queueOptions->getConnection());
        $jobPluginManager = $parentLocator->get('SlmQueue\Job\JobPluginManager');

        $queue = new OracleDoctrineQueue($connection, $queueOptions, $requestedName, $jobPluginManager);

        return $queue;
    }
}
