<?php

namespace Kotka\Validator;

use Zend\Validator\AbstractValidator;

/**
 * Class NotSample
 * @package Kotka\Validator
 */
class NotSample extends AbstractValidator
{
    const SAMPLE = 'sample';

    protected $messageTemplates = array(
        self::SAMPLE => 'Object ID cannot end with "_S[numbers]"!',
    );

    /**
     * Checks if the uri build from the pieces already exists in the database
     *
     * @param mixed $value
     * @param null $context
     * @return bool
     * @throws Exception\DomainException
     */
    public function isValid($value, $context = null)
    {
        if (empty($value) || !is_string($value)) {
            return true;
        }
        if (preg_match('/_S[0-9]+$/', $value)) {
            $this->setValue($value);
            $this->error(self::SAMPLE);
            return false;
        }
        return true;
    }
}
