<?php

namespace Kotka\Validator;

use Common\Service\IdService;
use Kotka\InputFilter\MYDocument;
use Kotka\Service\FormElementService;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Zend\Validator\AbstractValidator;

/**
 * Class AllowedNamespace
 * @package Kotka\Validator
 */
class AllowedNamespace extends AbstractValidator implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    const NOT_ALLOWED = 'notAllowed';
    const MISSING_DATATYPE = 'missingDataType';

    /** @var FormElementService */
    protected $formElementService;
    protected $type;

    protected $messageTemplates = array(
        self::NOT_ALLOWED => "Namespace '%value%' is not allowed in %datatype% datatype or domain!",
        self::MISSING_DATATYPE => "Datatype is missing. Cannot determinate if namespace is correct without it."
    );
    protected $messageVariables = ['datatype' => 'type'];

    private $datatypeSwitch = [
        'zoo' => 'zoological',
        'botany' => 'botanical',
    ];

    /**
     * @return string|null
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @inheritdoc
     */
    public function isValid($value, $context = null)
    {
        $this->setValue($value);
        if (empty($value)) {
            $value = IdService::getDefaultQNamePrefix() . ':';
        } else if (strpos($value, ':') === false) {
            $value = IdService::getDefaultQNamePrefix() . ':' . $value;
        }
        $type = $this->type === null ? MYDocument::getType() : $this->type;
        if (empty($type)) {
            throw new Exception\InvalidArgumentException('Unknown type');
        }
        // Allow all digitarium namespaces
        if (strpos($value, 'E') === 0) {
            return true;
        }
        if ($type == 'Specimen') {
            if (!isset($context['MYDatatype'])) {
                $this->error(self::MISSING_DATATYPE);

                return false;
            }
            $type = $context['MYDatatype'];
        }
        $field = str_replace('specimen', '', $type) . 'Namespaces';
        if (!in_array($value, $this->getFieldAllowedValues($field))) {
            // This is to allow import editing of HT ns
            // When web form is sent the value of MZDateEdited is null so the if statement bellow will be false
            // This allows user to add specimens to HT ns if he adds MZDateEdited field to import,
            // but this was acceptable risk sine by default this field is not included in import excels
            $reg = '{^' . IdService::DOMAIN_PREFIXES_PART . '?HT$}';
            if (isset($context['MZDateEdited']) && !empty($context['MZDateEdited']) && preg_match($reg, $value)) {
                return true;
            }
            $origType = $this->type;
            $this->type = $type;
            if (isset($this->datatypeSwitch[$this->type])) {
                $this->type = $this->datatypeSwitch[$this->type];
            }
            $this->error(self::NOT_ALLOWED);
            $this->type = $origType;

            return false;
        }

        return true;
    }

    /**
     * @param $field
     * @return array
     */
    protected function getFieldAllowedValues($field)
    {
        if ($this->formElementService === null) {
            $this->formElementService = $this->serviceLocator->getServiceLocator()->get('Kotka\Service\FormElementService');
        }

        return $this->formElementService->getSelect($field, []);
    }
}
