<?php

namespace Kotka\Validator;

use Zend\Validator\AbstractValidator;

/**
 * Class PreferredIdentification
 * @package Kotka\Validator
 */
class PreferredIdentification extends AbstractValidator
{
    const TOO_MANY = 'tooMany';

    protected $messageTemplates = array(
        self::TOO_MANY => "Only one identification should be chosen as preferred",
    );

    /**
     * @inheritdoc
     */
    public function isValid($value, $context = null)
    {
        if (!isset($context['MYGathering'])) {
            return true;
        }
        foreach ($context['MYGathering'] as $gathering) {
            if (!isset($gathering['MYUnit'])) {
                continue;
            }
            foreach ($gathering['MYUnit'] as $unit) {
                if (!isset($unit['MYIdentification'])) {
                    return true;
                }
                $preferred = 0;
                foreach ($unit['MYIdentification'] as $identification) {
                    if (isset($identification['MYPreferredIdentification']) && $identification['MYPreferredIdentification'] == 'yes') {
                        $preferred++;
                    }
                }
                if ($preferred > 1) {
                    $this->error(self::TOO_MANY);

                    return false;
                }
            }
        }
        return true;
    }

}
