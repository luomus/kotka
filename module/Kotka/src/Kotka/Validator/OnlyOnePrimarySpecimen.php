<?php

namespace Kotka\Validator;

use Zend\Validator\AbstractValidator;

/**
 * Class NotSample
 * @package Kotka\Validator
 */
class OnlyOnePrimarySpecimen extends AbstractValidator
{
    const PRIMARY = 'primay';
    const NOT_ARRAY = 'not array';

    protected $messageTemplates = array(
        self::PRIMARY => 'Only one primary specimen allowed.',
        self::NOT_ARRAY => 'MY.Unit not an array.',
    );

    /**
     * Checks if there are more than one specimens marked as primary in the document
     *
     * @param mixed $value
     * @param mixed $context
     * @return bool
     * @throws Exception\DomainException
     */
    public function isValid($value, $context = null)
    {
        if (!is_array($context)) {
            $this->error(self::NOT_ARRAY);
            return false;
        }

        if (!isset($context['MYUnit'])) {
            return true;
        }

        $primaryCount = 0;

        foreach ($context['MYUnit'] as $unit) {
            if (isSet($unit['MYPrimarySpecimen']) && $unit['MYPrimarySpecimen'] === 'true') {
                $primaryCount++;
            }
        }

        if ($primaryCount <= 1) {
            return true;
        }

        $this->error(self::PRIMARY);
        return false;
    }
}