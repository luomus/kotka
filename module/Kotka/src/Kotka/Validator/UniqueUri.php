<?php

namespace Kotka\Validator;

use Common\Service\IdService;
use Triplestore\Service\ObjectManager;
use Triplestore\Service\PhpVariableConverter;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Validator\AbstractValidator;

/**
 * Class UniqueUriValidator
 * @package Kotka\Validator
 */
class UniqueUri extends AbstractValidator implements ServiceLocatorAwareInterface
{
    const NOT_UNIQUE = 'notUnique';
    const MODIFIED_INTERNALLY = 'modifiedInternally';

    private $serviceLocator;
    /** @var  \Triplestore\Service\ObjectManager */
    private $om;

    protected $messageTemplates = array(
        self::NOT_UNIQUE => "Uri '%value%' already exists! Please choose another number for this specimen.",
        self::MODIFIED_INTERNALLY => "<em>This specimen has been updated in Kotka while you edited it with Excel. Please make your changes using entry form.</em>",
    );

    /**
     * Checks if the uri build from the pieces already exists in the database
     *
     * @param mixed $value
     * @param null $context
     * @return bool
     * @throws Exception\DomainException
     */
    public function isValid($value, $context = null)
    {
        if (empty($context['MYNamespaceID']) && empty($context['MYObjectID'])) {
            return true;
        }

        $oId = isset($context['MYObjectID']) ? $context['MYObjectID'] : null;
        $nId = isset($context['MYNamespaceID']) ? $context['MYNamespaceID'] : null;
        if (null == $oId) {
            return true;
        }

        $idGenerator = new IdService();
        $idGenerator->generateFromPieces($oId, $nId, 'MY.document');
        $subject = IdService::getQName($idGenerator->getFQDN());
        $key = PhpVariableConverter::toPhpMethod(ObjectManager::PREDICATE_EDITED);
        $predicate = $this->getOM()->getPredicate($subject, ObjectManager::PREDICATE_EDITED);

        $this->setValue($idGenerator->getFQDN());
        if ($predicate === null || $predicate->count() == 0) {
            return true;
        }
        if (isset($context[$key])) {
            if ($predicate === null || $predicate->count() > 0) {
                /** @var \Triplestore\Model\Predicate $predicate */
                $edited = $predicate->getFirst()->getValue();
                if ($edited === $context[$key]) {
                    return true;
                } else {
                    $this->error(self::MODIFIED_INTERNALLY);

                    return false;
                }
            }
        }
        $this->error(self::NOT_UNIQUE);

        return false;

    }

    private function getOM()
    {
        if ($this->om === null) {
            $this->om = $this->serviceLocator->getServiceLocator()->get('Triplestore\ObjectManager');
        }
        return $this->om;
    }

    /**
     * Set service locator
     *
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    /**
     * Get service locator
     *
     * @return ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }
}
