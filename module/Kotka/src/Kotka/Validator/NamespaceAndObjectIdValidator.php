<?php

namespace Kotka\Validator;

use Kotka\Service\ValidationService;
use Zend\Validator\AbstractValidator;

/**
 * Class CountryOrGeoValidator
 * @package Kotka\Validator
 */
class NamespaceAndObjectIdValidator extends AbstractValidator
{
    const BOTH_MISSING = 'bothMissing';
    const ONLY_NS_GIVEN = 'onlyNsGiven';

    protected $messageTemplates = array(
        self::BOTH_MISSING => "Both Namespace ID and Specimen code must be set.",
        self::ONLY_NS_GIVEN => "If you set Namespace ID, you have to set Specimen code also.",
    );

    /**
     * @inheritdoc
     */
    public function isValid($value, $context = null)
    {
        $import = isset($context[ValidationService::IMPORT_KEY]);
        if (empty($context['namespaceID']) && empty($context['objectID'])) {
            if ($import) {
                $this->error(self::BOTH_MISSING);
                return false;
            }
        }

        if (!empty($context['namespaceID']) && empty($context['objectID'])) {
            $this->error(self::ONLY_NS_GIVEN);

            return false;
        }

        return true;
    }

}
