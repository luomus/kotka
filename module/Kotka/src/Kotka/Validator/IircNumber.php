<?php

namespace Kotka\Validator;

use Zend\Validator\AbstractValidator;

class IircNumber extends AbstractValidator
{

    const INVALID_IDENTIFIER = 'invalid_identifier';
    const REMOTE_CHECK_FAILED = 'remote_check_failed';

    /**
     * @var array
     */
    protected $messageTemplates = array(
        self::REMOTE_CHECK_FAILED => "ABSCH API didn't respond in time",
        self::INVALID_IDENTIFIER => "Invalid IIRC number '%value%' given.",
    );

    public function isValid($value)
    {
        if (empty($value)) {
            return true;
        }
        if (is_string($value)) {
            $match = [];
            if (preg_match('/^ABSCH-IRCC-[A-Z]{2}-([0-9]{6,7})-[1-9]$/', $value, $match)) {
                if (!$this->remoteValidate($match[1])) {
                    return false;
                }
            } else {
                $this->setValue($value);
                $this->error(self::INVALID_IDENTIFIER);

                return false;
            }
        }
        return true;
    }

    protected function remoteValidate($value)
    {
        $ctx = stream_context_create(array('http'=>
            array(
                'timeout' => 30
            )
        ));
        file_get_contents("https://api.cbd.int/api/v2013/documents/${value}?include-deleted=true", false, $ctx);
        $headers = $this->parseHeaders($http_response_header);
        if (isset($headers['response_code']) && $headers['response_code'] === 200) {
            return true;
        }

        $this->setValue($value);
        $this->error(isset($headers['response_code']) ? self::INVALID_IDENTIFIER : self::REMOTE_CHECK_FAILED);

        return false;
    }

    private function parseHeaders( $headers )
    {
        $head = array();
        foreach( $headers as $k=>$v )
        {
            $t = explode( ':', $v, 2 );
            if( isset( $t[1] ) )
                $head[ trim($t[0]) ] = trim( $t[1] );
            else
            {
                $head[] = $v;
                if( preg_match( "#HTTP/[0-9\.]+\s+([0-9]+)#",$v, $out ) )
                    $head['response_code'] = intval($out[1]);
            }
        }
        return $head;
    }

}