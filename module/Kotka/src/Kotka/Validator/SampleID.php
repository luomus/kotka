<?php

namespace Kotka\Validator;

use Common\Service\IdService;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Zend\Validator\AbstractValidator;

/**
 * Class SampleID
 * @package Kotka\Validator
 */
class SampleID extends AbstractValidator implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    const NOT_UNIQUE = 'notUnique';
    const NOT_SPECIMEN = 'notSpecimen';
    const INVALID = 'invalid';
    const INVALID_ENDING = 'invalid_ending';

    const ENDING_REGEXP = '/^S[0-9]{1,5}$/';

    /** @var  \Triplestore\Service\ObjectManager */
    private $om;

    private $documentTypeValidator;

    protected $messageTemplates = array(
        self::NOT_UNIQUE => "Uri '%value%' already exists! Please choose another number for this sample.",
        self::NOT_SPECIMEN => "Specimen with id '%value%' could not be found",
        self::INVALID => "Sample ID is not in correct format. Place use format <specimen id>_S<numbers> (ex. http://id.luomus.fi/JA.1_S1)",
        self::INVALID_ENDING => "Specimen ID has to end in '_S<number>' (ex. _S123).",
    );

    /**
     * Checks if the specimen id is unique and that the specimen exists in kotka
     *
     * @param mixed $value
     * @param null $context
     * @return bool
     * @throws Exception\DomainException
     */
    public function isValid($value, $context = null)
    {
        if (empty($value)) {
            return true;
        }

        $parts = explode('_', $value);
        if (count($parts) < 2) {
            $this->setValue($value);
            $this->error(self::INVALID);

            return false;
        }
        $ending = array_pop($parts); // This removes the last elem from the id
        if (!preg_match(self::ENDING_REGEXP, $ending)) {
            $this->setValue($value);
            $this->error(self::INVALID_ENDING);

            return false;
        }

        $documentTypeValidator = $this->getDocumentTypeValidator();
        $documentTypeValidator->setType('MF.sample');
        $documentTypeValidator->setExists(false);
        $specimen = IdService::getQName(implode('-', $parts), true);
        if (!$documentTypeValidator->isValid($value)) {
            $this->setValue($value);
            $this->error(self::NOT_UNIQUE);

            return false;
        }


        $documentTypeValidator->setType('MY.document');
        $documentTypeValidator->setExists(true);
        if ($documentTypeValidator->isValid($specimen)) {
            return true;
        }
        $this->setValue($specimen);
        $this->error(self::NOT_SPECIMEN);

        return false;
    }

    /**
     * @return DocumentType
     */
    private function getDocumentTypeValidator()
    {
        if ($this->documentTypeValidator === null) {
            $this->documentTypeValidator = $this->getServiceLocator()->get('Kotka\Validator\DocumentType');
        }
        return $this->documentTypeValidator;
    }

}
