<?php

namespace Kotka\Validator;

use Triplestore\Service\ObjectManager;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Validator\AbstractValidator;

/**
 * Class Cycle validator chekcs that the field value doesn't greate cycle in the hierarcy
 *
 * @package Kotka\Validator
 */
class Cycle extends AbstractValidator implements ServiceLocatorAwareInterface
{
    const BECOMES_CYCLE = 'becomeCycle';

    private $serviceLocator;

    protected $messageTemplates = array(
        self::BECOMES_CYCLE => "Value '%value%' is making cycle in the hierarcy!",
    );

    protected $predicate;

    public function __construct($options = array())
    {
        if ($options instanceof \Traversable) {
            $options = iterator_to_array($options);
        } elseif (!is_array($options)) {
            $options = func_get_args();
            $temp['predicate'] = array_shift($options);
            $options = $temp;
        }

        if (array_key_exists('predicate', $options)) {
            $this->setPredicate($options['predicate']);
        }
        parent::__construct($options);
    }

    /**
     * Returns the predicate that is used for checking if cycle exists
     *
     * @return string
     */
    public function getPredicate()
    {
        return $this->predicate;
    }

    /**
     * Sets the predicate that is used to travel the hierarcy
     * @param string $predicate
     *
     * @return $this
     */
    public function setPredicate($predicate)
    {
        $this->predicate = $predicate;

        return $this;
    }

    /**
     * Checks if the value makes a cycle to the hieararcy
     *
     * @param mixed $value
     * @param array $context
     * @return bool
     * @throws Exception\InvalidArgumentException
     */
    public function isValid($value, $context = array())
    {
        if ($this->predicate === null) {
            throw new Exception\InvalidArgumentException('You have forgotten to tell the validator which predicate to use');
        }
        if (empty($value)) {
            return true;
        }
        if (!is_string($value)) {
            throw new Exception\InvalidArgumentException('Value should be sting!');
        }
        $parents = array($value => true);
        if (isset($context['subject']) && $context['subject'] !== null && $context['subject'] !== '') {
            $subject = $context['subject'];
            if (isset($parents[$subject])) {
                $this->setValue($value);
                $this->error(self::BECOMES_CYCLE);

                return false;
            }
            $parents[$subject] = true;
        }

        /** @var \Triplestore\Service\ObjectManager $om */
        $om = $this->serviceLocator->getServiceLocator()->get('Triplestore\ObjectManager');
        $om->disableHydrator();
        $isCycle = $this->isCycle($om, $this->predicate, $value, $parents);
        $om->enableHydrator();
        if ($isCycle) {
            $this->setValue($value);
            $this->error(self::BECOMES_CYCLE);

            return false;
        }
        return true;
    }

    private function isCycle(ObjectManager $om, $predicate, $value, $parents = array())
    {
        $parent = $om->fetch($value, false);
        if ($parent === null) {
            return false;
        }
        $partOfPred = $parent->getPredicate($predicate);
        if ($partOfPred === null) {
            return false;
        }
        foreach ($partOfPred as $object) {
            $newValue = $object->getValue();
            if (isset($parents[$newValue]) || $newValue == $value) {
                return true;
            }
            $parents[$newValue] = true;
            if ($this->isCycle($om, $predicate, $newValue, $parents)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Set service locator
     *
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    /**
     * Get service locator
     *
     * @return ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }
}
