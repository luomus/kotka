<?php

namespace Kotka\Validator;

use Zend\Validator\AbstractValidator;

class HasIRCCChecks extends AbstractValidator
{
    const MISSING_IIRC = 'missing_iirc';
    const MISSING_OTHER = 'missing_other';
    const MISSING_GENERIC_RESEARCH_ALLOWED = 'missing_research';

    /**
     * @var array
     */
    protected $messageTemplates = array(
        self::MISSING_GENERIC_RESEARCH_ALLOWED => "Since under Nagoya this must be filled.",
        self::MISSING_IIRC => "Since available for genetic research you have must tell the IRCC number if you have it.",
        self::MISSING_OTHER => "Since available for genetic research and no IRCC number you must fill in acquisition date, acquisition country, description, source and you have to have PIC and MAT permits!",
    );

    public function isValid($value, $context = null)
    {
        $underNagoya = $context['HRAUnderNagoya'] === true || $context['HRAUnderNagoya'] === 'true';
        if ($underNagoya && empty($value)) {
            $this->setValue($value);
            $this->error(self::MISSING_GENERIC_RESEARCH_ALLOWED);
            return false;
        }
        if (!$underNagoya || empty($value) || $value === 'HRA.availableForGeneticResearchNo' || empty($context)) {
            return true;
        }
        if ($context['HRAHasIRCC'] === 'true' || $context['HRAHasIRCC'] === true) {
            if (empty($context['HRAIRCC'])) {
                $this->setValue($value);
                $this->error(self::MISSING_IIRC);
                return false;
            }
        } else if (
            empty($context['HRAGeneticResourceAcquisitionCountry']) ||
            empty($context['HRAGeneticResourceAcquisitionDate']) ||
            empty($context['HRAGeneticResourceDescription']) ||
            empty($context['HRAGeneticResourceSource']) ||
            !$this->hasPermit($context['HRAPermit'], 'HRA.permitTypePIC') ||
            !$this->hasPermit($context['HRAPermit'], 'HRA.permitTypeMAT')
        ) {
            $this->setValue($value);
            $this->error(self::MISSING_OTHER);
            return false;
        }
        return true;
    }

    private function hasPermit($permits, $type) {
        $result = false;
        if (is_array($permits)) {
            foreach ($permits as $permit) {
                if ($permit['HRAPermitType'] === $type) {
                    $result = true;
                    break;
                }
            }
        }

        return $result;
    }

}