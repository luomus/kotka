<?php

namespace Kotka\Validator;

use Common\Service\IdService;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Validator\AbstractValidator;

/**
 * Class InDatabaseSelect
 * @package Kotka\Validator
 */
class UserInOrganization extends AbstractValidator implements ServiceLocatorAwareInterface
{
    const NOT_IN_ORGANIZATION = 'notInOrganization';
    const NOT_IN_ANY_ORGANIZATION = 'notInAnyOrganization';
    const USER_ORGANIZATIONS_NOT_ARRAY = 'organizationsNotArray';

    private $serviceLocator;
    /** @var  \Triplestore\Service\ObjectManager */
    private $om;

    protected $messageTemplates = array(
        self::NOT_IN_ORGANIZATION => "User not in owner organization",
        self::NOT_IN_ANY_ORGANIZATION => "User is not in any organization",
        self::USER_ORGANIZATIONS_NOT_ARRAY => "Invalid user organization type",
    );

    /**
     * Checks if the user is in the organization
     *
     * @param mixed $value
     * @param null|array $context
     * @return bool
     * @throws Exception\DomainException
     */
    public function isValid($value, $context = null)
    {
        $user = $this->serviceLocator->getServiceLocator()->get('user');
        $organizations = $user->getMAOrganisation();
        if (!is_array($organizations)) {
            $this->error(self::USER_ORGANIZATIONS_NOT_ARRAY);

            return false;
        }

        if (!isset($organizations[$value])) {
            $this->error(empty($organizations) ? self::NOT_IN_ANY_ORGANIZATION : self::NOT_IN_ORGANIZATION);

            return false;
        }

        $oId = isset($context['MYObjectID']) ? $context['MYObjectID'] : null;
        $nId = isset($context['MYNamespaceID']) ? $context['MYNamespaceID'] : null;
        if (null == $oId) {
            return true;
        }

        $idGenerator = new IdService();
        $idGenerator->generateFromPieces($oId, $nId, 'MY.document');
        $predicate = $this->getOM()->getPredicate(IdService::getQName($idGenerator->getFQDN()), 'MZ.owner');
        $owners = $predicate === null ? [] : $predicate->getResourceValue();
        foreach ($owners as $owner) {
            if (!isset($organizations[$owner])) {
                $this->error(empty($organizations) ? self::NOT_IN_ANY_ORGANIZATION : self::NOT_IN_ORGANIZATION);
                return false;
            }
        }
        return true;
    }

    private function getOM()
    {
        if ($this->om === null) {
            $this->om = $this->getServiceLocator()->getServiceLocator()->get('Triplestore\ObjectManager');
        }
        return $this->om;
    }

    /**
     * Set service locator
     *
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    /**
     * Get service locator
     *
     * @return ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }
}
