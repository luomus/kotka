<?php

namespace Kotka\Validator;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Validator\AbstractValidator;

/**
 * Class UniqueADGroup
 * @package Kotka\Validator
 */
class UniqueADGroup extends AbstractValidator implements ServiceLocatorAwareInterface
{
    const NOT_UNIQUE = 'notUnique';

    private $serviceLocator;

    protected $messageTemplates = array(
        self::NOT_UNIQUE => "AD Group already in use by '%value%'!",
    );

    /**
     * Checks if the uri build from the pieces already exists in the database
     *
     * @param mixed $value
     * @param null $context
     * @return bool
     * @throws Exception\DomainException
     */
    public function isValid($value, $context = null)
    {
        /** @var \Kotka\Service\OrganizationService $organizationService */
        $organizationService = $this->serviceLocator->getServiceLocator()->get('Kotka\Service\OrganizationService');
        $organization = $organizationService->getOneWithAdGroup($value);
        if ($organization !== null && $organization->getSubject() != $context['subject']) {
            $this->setValue($organization->getUri());
            $this->error(self::NOT_UNIQUE);

            return false;
        }

        return true;
    }

    /**
     * Set service locator
     *
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    /**
     * Get service locator
     *
     * @return ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }
}
