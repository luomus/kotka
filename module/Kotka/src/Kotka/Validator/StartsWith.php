<?php

namespace Kotka\Validator;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Validator\AbstractValidator;

/**
 * Class StartWith
 * @package Kotka\Validator
 */
class StartsWith extends AbstractValidator implements ServiceLocatorAwareInterface
{
    const NOT_STARTING = 'notStarting';
    const INVALID_FORMAT = 'invalidFormat';

    protected $serviceLocator;
    protected static $elementService;
    protected static $cache = array();

    protected $messageTemplates = array(
        self::NOT_STARTING => "Value '%value%' is not starting as expected",
        self::INVALID_FORMAT => "The input does not appear to be in valid format"
    );

    /** @var string */
    protected $field;
    /** @var string */
    protected $fieldPrefix;
    /** @var string */
    protected $string;
    /** @var string */
    protected $delimiter = ':';

    public function setField($datatype)
    {
        $this->field = $datatype;
    }

    public function getField()
    {
        return $this->field;
    }

    /**
     * @return string
     */
    public function getFieldPrefix()
    {
        return $this->fieldPrefix;
    }

    /**
     * @param string $fieldPrefix
     */
    public function setFieldPrefix($fieldPrefix)
    {
        $this->fieldPrefix = $fieldPrefix;
    }

    /**
     * @return string
     */
    public function getString()
    {
        return $this->string;
    }

    /**
     * @param string $string
     */
    public function setString($string)
    {
        $this->string = $string;
    }

    /**
     * @return string
     */
    public function getDelimiter()
    {
        return $this->delimiter;
    }

    /**
     * @param string $delimiter
     */
    public function setDelimiter($delimiter)
    {
        $this->delimiter = $delimiter;
    }

    /**
     * Checks if the uri build from the pieces already exists in the database
     *
     * @param mixed $value
     * @param null $context
     * @return bool
     * @throws Exception\DomainException
     */
    public function isValid($value, $context = null)
    {
        if (is_array($value)) {
            $value = array_filter($value);
        }
        if (empty($value)) {
            return true;
        }
        $field = $this->getField();
        $string = $this->getString();
        if ($field !== null) {
            $allowed = $this->getAllowedValues($field);
        } else if ($string !== null) {
            $allowed = [$string];
        } else {
            throw new Exception\DomainException(sprintf("No field or start given."));
        }
        if (is_string($value)) {
            $value = [$value];
        } else if (!is_array($value)) {
            $this->error(self::INVALID_FORMAT);
            return false;
        }
        foreach ($value as $check) {
            if (!$this->validate($check, $allowed, $this->getDelimiter())) {
                $this->setValue($check);
                $this->error(self::NOT_STARTING);
                return false;
            }
        }
        return true;
    }

    private function validate($value, $allowed, $delimiter)
    {
        if ($delimiter === '') {
            foreach ($allowed as $good) {
                if (strpos($value, $good) === 0) {
                    return true;
                }
            }
        } else {
            $parts = explode($delimiter, $value, 2);
            if (isset($parts[0]) && in_array($parts[0], $allowed)) {
                return true;
            }
        }
        return false;
    }

    protected function getAllowedValues($field)
    {
        if (isset(self::$cache[$field])) {
            return self::$cache[$field];
        }
        $fieldPrefix = $this->getFieldPrefix();
        $values = $this->getFormElementService()->getSelect($field);
        self::$cache[$field] = self::removeFieldPrefix($values, $field, $fieldPrefix);

        return self::$cache[$field];
    }

    public static function removeFieldPrefix($values, $field, $fieldPrefix = null)
    {
        $remove = isset($fieldPrefix) ? $fieldPrefix : $field;
        $values = array_map(function ($k) use ($remove) {
            return lcfirst(str_replace($remove, '', $k));
        }, array_keys($values));
        return $values;
    }

    protected function getFormElementService()
    {
        if (self::$elementService === null) {
            self::$elementService = $this->serviceLocator->getServiceLocator()->get('Kotka\Service\FormElementService');
        }
        return self::$elementService;
    }

    /**
     * Set service locator
     *
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    /**
     * Get service locator
     *
     * @return ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }
}
