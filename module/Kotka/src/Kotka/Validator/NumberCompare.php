<?php
namespace Kotka\Validator;

use Traversable;
use Zend\Validator\AbstractValidator;

/**
 * Class NumberCompare
 * Validates that the values are correctly in order
 * @package Kotka\Validator
 */
class NumberCompare extends AbstractValidator
{

    private static $romanFilter;

    const NUMBERS_IN_WRONG_ORDER = 'numbersInWrongOrder';
    const INVALID = 'numberInvalid';
    const INVALID_COMPARE_TO = 'compareToInvalid';
    const MISSING_COMPARE_TO = 'missingCompareTo';
    const GREATER_THAN = 'greater than';
    const LESS_THAN = 'less than';

    /**
     * Validation failure message template definitions
     *
     * @var array
     */
    protected $messageTemplates = array(
        self::INVALID => "Invalid input %value% given, integer or float expected",
        self::INVALID_COMPARE_TO => "Invalid input %compareTo% give, integer or float expected",
        self::NUMBERS_IN_WRONG_ORDER => "%value% should be %direction% %compareTo%",
        self::MISSING_COMPARE_TO => "Missing compare to %compareTo% value",
    );

    protected $constants  = array(
        self::GREATER_THAN,
        self::LESS_THAN,
    );

    /**
     * Required compareTo
     * @var string|null
     */
    protected $compareTo;

    /**
     * Required direction
     *
     * @var string|null
     */
    protected $direction;

    /**
     * @var array
     */
    protected $messageVariables = array(
        'value' => 'value',
        'compareTo' => 'compareTo',
        'direction' => 'direction',
    );

    /**
     * Sets validator options
     *
     * @param  string|array|Traversable $options OPTIONAL
     */
    public function __construct($options = array())
    {
        if ($options instanceof Traversable) {
            $options = iterator_to_array($options);
        } elseif (!is_array($options)) {
            $options = func_get_args();
            $temp['compareTo'] = array_shift($options);
            $temp['direction'] = array_shift($options);
            $options = $temp;
        }

        if (array_key_exists('compareTo', $options)) {
            $this->setCompareTo($options['compareTo']);
        }

        if (array_key_exists('direction', $options)) {
            $this->setDirection($options['direction']);
        } else {
            $this->setDirection(self::GREATER_THAN);
        }
        parent::__construct($options);
    }

    /**
     * Returns the compareTo option
     *
     * @return string|null
     */
    public function getCompareTo()
    {
        return $this->compareTo;
    }

    /**
     * Sets the compareTo option
     *
     * @param  string $compareTo
     * @throws Exception\InvalidArgumentException
     * @return NumberCompare
     */
    public function setCompareTo($compareTo = null)
    {
        if (!isset($compareTo) || empty($compareTo)) {
            throw new Exception\InvalidArgumentException('Missing compareTo');
        }

        $this->compareTo = $compareTo;
        return $this;
    }

    /**
     * Returns the direction option
     *
     * @return string|null
     */
    public function getDirection()
    {
        return $this->direction;
    }

    /**
     * Sets the direction option
     *
     * @param  string $direction
     * @throws Exception\InvalidArgumentException
     * @return NumberCompare
     */
    public function setDirection($direction = null)
    { 
        if (!in_array($direction, $this->constants)) {
            throw new Exception\InvalidArgumentException('Unknown direction');
        }
        $this->direction = $direction;
        return $this;
    }

    public function isValid($value, $context = null)
    {
        if (!is_numeric($value)) {
            $this->error(self::INVALID);
            return false;  
        }

        $compareTo = $this->getCompareTo();
        $direction = $this->getDirection();

        if (!isset($context[$compareTo]) || empty($context[$compareTo])) {
            $this->error(self::MISSING_COMPARE_TO);
            return false;
        }

        $compareTo = $context[$compareTo];

        if(!is_numeric($compareTo)) {
            $this->error(self::INVALID_COMPARE_TO);
        }

        $hasError = ($direction == self::GREATER_THAN && $value < $compareTo)
            || ($direction == self::LESS_THAN && $value > $compareTo);

        if ($hasError) {
            $this->error(self::NUMBERS_IN_WRONG_ORDER, $value);

            return false;
        }
        return true;

    }
}