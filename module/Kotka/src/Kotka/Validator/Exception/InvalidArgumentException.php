<?php

namespace Kotka\Validator\Exception;

class InvalidArgumentException extends \InvalidArgumentException
{
}