<?php

namespace Kotka\Validator;

use Common\Service\IdService;
use Triplestore\Model\Predicate;
use Triplestore\Service\ObjectManager;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Zend\Validator\AbstractValidator;

/**
 * Class DocumentType
 * @package Kotka\Validator
 */
class DocumentType extends AbstractValidator implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    const MISSING_TYPE = 'missingType';
    const EXISTS_TYPE = 'existsType';
    const MISSING_TYPE_KEY = 'missingTypeKey';

    /** @var  \Triplestore\Service\ObjectManager */
    private $om;

    /** @var String */
    protected $type;
    /** @var bool */
    protected $exists = true;

    protected $messageTemplates = array(
        self::MISSING_TYPE => "Uri '%value%' was not found.",
        self::EXISTS_TYPE => "Uri '%value%' already exits",
        self::MISSING_TYPE_KEY => 'Validator is missing type.',
    );

    /**
     * Checks if the given uri is of specified type
     *
     * @param mixed $value
     * @param null $context
     * @return bool
     * @throws Exception\DomainException
     */
    public function isValid($value, $context = null)
    {
        if (empty($value)) {
            return true;
        }
        $type = $this->getType();
        if (empty($type)) {
            $this->error(self::MISSING_TYPE_KEY);
            return false;
        }
        $exists = $this->isExists();
        if (is_string($value)) {
            $value = [$value];
        }
        if (is_array($value)) {
            foreach ($value as $val) {
                if (!$this->validType($val, $type, $exists)) {
                    $this->setValue($val);
                    $this->error($exists ? self::MISSING_TYPE : self::EXISTS_TYPE);
                    return false;
                }
            }
        }
        return true;
    }

    private function validType($uri, $type, $exists)
    {
        $om = $this->getOM();
        $predicate = $om->getPredicate(IdService::getQName($uri, true), ObjectManager::PREDICATE_TYPE);
        if ($predicate instanceof Predicate && in_array($type, $predicate->getResourceValue())) {
            return $exists;
        }
        return !$exists;
    }

    /**
     * @return boolean
     */
    public function isExists()
    {
        return $this->exists;
    }

    /**
     * @param boolean $exists
     */
    public function setExists($exists)
    {
        $this->exists = (bool)$exists;
    }

    /**
     * @return String
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param String $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    private function getOM()
    {
        if ($this->om === null) {
            $this->om = $this->serviceLocator->getServiceLocator()->get('Triplestore\ObjectManager');
        }
        return $this->om;
    }

}
