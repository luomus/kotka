<?php

namespace Kotka\Validator;

use Traversable;
use Zend\Validator\AbstractValidator;

/**
 * Class RequireWithThis
 * @package Kotka\Validator
 */
class RequireWithThis extends AbstractValidator
{
    const MISSING = 'missing';

    protected $isFile;
    protected $field;
    protected $whenContains;
    // use all lowercase characters in these.
    protected $emptyValues = array(
        'undefined'
    );


    protected $messageTemplates = array(
        self::MISSING => "Required element is missing.",
    );

    /**
     * Sets validator options
     *
     * @param  string|array|Traversable $options OPTIONAL
     */
    public function __construct($options = array())
    {
        if ($options instanceof Traversable) {
            $options = iterator_to_array($options);
        } elseif (!is_array($options)) {
            $options = func_get_args();
            $temp['field'] = array_shift($options);
            $temp['when_contains'] = array_shift($options);
            $temp['is_file'] = array_shift($options);
            $options = $temp;
        }

        if (array_key_exists('field', $options)) {
            $this->setField($options['field']);
        }
        if (array_key_exists('is_file', $options)) {
            $this->setIsFile($options['is_file']);
        }
        if (array_key_exists('when_contains', $options)) {
            $this->setWhenContains($options['when_contains']);
        }
        parent::__construct($options);
    }

    public function setField($field)
    {
        $this->field = $field;

        return $this;
    }

    public function getField()
    {
        return $this->field;
    }

    public function setWhenContains($value)
    {
        $this->whenContains = $value;

        return $this;
    }

    public function getWhenContains()
    {
        return $this->whenContains;
    }

    /**
     * @return boolean
     */
    public function getIsFile()
    {
        return $this->isFile;
    }

    /**
     * @param boolean $isFile
     */
    public function setIsFile($isFile)
    {
        $this->isFile = $isFile;
    }


    /**
     * @inheritdoc
     */
    public function isValid($value, $context = null)
    {
        $field = $this->getField();
        $whenContains = $this->getWhenContains();
        if (null === $field) {
            return true;
        }
        if (null !== $whenContains && strpos($value, $whenContains) === false) {
            return true;
        }
        if (!isset($context[$field]) || empty($context[$field]) || !$this->fileCheck($context[$field])) {
            $this->error(self::MISSING);

            return false;
        }
        return true;
    }

    private function fileCheck($value) {
        if (!$this->isFile || !is_array($value)) {
            return true;
        }
        return !empty($value['name']) && !empty($value['tmp_name']);
    }

}
