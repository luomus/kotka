<?php
namespace Kotka\Validator;

/**
 * Class FloatRange
 * Validates that the value is in correct float range format
 *
 * @package Kotka\Validator
 */
class FloatRange extends FloatValidator
{

    const INVALID_RANGE = 'invalidRange';
    const NEGATIVE_VALUE = 'negativeValue';

    /**
     * Digits filter used for validation
     *
     * @var \Zend\Filter\Digits
     */
    protected static $filter = null;

    protected $separator = '-';

    /**
     * Validation failure message template definitions
     *
     * @var array
     */
    protected $messageTemplates = array(
        self::NOT_FLOAT => "The input must contain only float",
        self::NOT_DIGITS => "The input must contain only digits",
        self::STRING_EMPTY => "The input is an empty string",
        self::INVALID => "Invalid type given. String, integer or float expected",
        self::INVALID_RANGE => "Invalid range given",
        self::NEGATIVE_VALUE => "The input can't contain negative values",
    );

    /**
     * @return string
     */
    public function getSeparator()
    {
        return $this->separator;
    }

    /**
     * @param string $separator
     */
    public function setSeparator($separator)
    {
        $this->separator = $separator;
    }

    public function isValid($value, $context = null)
    {
        if (!is_string($value) && !is_int($value) && !is_float($value)) {
            $this->error(self::INVALID);
            return false;
        }

        $separator = $this->getSeparator();
        $allow_negative = isset($this->getOptions()['allow-negative']) && $this->getOptions()['allow-negative'];

        if ($separator === '-') {
            $sep_count = substr_count($value, $separator);
            if (!$allow_negative && (strpos($value, $separator) === 0 || $sep_count > 1)) {
                $this->setValue($value);
                $this->error(self::NEGATIVE_VALUE);
                return false;
            }
            if ($sep_count <= 1) {
                if(strpos($value, $separator) === 0) {
                    $parts = array(
                      $value,
                    );
                } else {
                    $parts = explode($separator, $value, 2);
                }
            } else if ($sep_count > 3) {
                $this->setValue($value);
                $this->error(self::INVALID_RANGE);
                return false;
            } else {
                //find separator position, first char should be either number or negative sign so can be ignored
                $sep_pos = strpos($value, $separator, 1);
                $parts = array(
                    substr($value, 0, $sep_pos),
                    substr($value, $sep_pos + 1),
                );
            }
        } else {
            if (!$allow_negative && substr_count($value, '-') !== 0) {
                $this->setValue($value);
                $this->error(self::NEGATIVE_VALUE);
                return false;
            }
            $parts = explode($separator, $value, 2);
        }

        if (isset($parts[0]) && isset($parts[1])) {
            if (!parent::isValid($parts[0]) || !parent::isValid($parts[1])) {
                $this->setValue($value);
                $this->error(self::INVALID_RANGE);
                return false;
            }
            return true;
        }

        return parent::isValid($value);
    }
} 