<?php

namespace Kotka\Validator\File;

use Kotka\Form\ImageUpload;
use Common\Service\IdService;
use Kotka\Validator\ImagesPath;
use Zend\Validator\AbstractValidator;

/**
 * Class ImagesPath
 * @package Kotka\Validator
 */
class ImageName extends AbstractValidator
{
    const INCORRECT_FORMAT = 'incorrectFormat';
    const NO_FORMAT_IN_DIR = 'noFormatInDir';
    const MISSING_FILENAME = 'missingFilename';
    const MISSING_DIRECTORY = 'missingDirectory';
    const NAME_DELIMITER = '_';

    protected $messageTemplates = array(
        self::INCORRECT_FORMAT => "Filename '%value%' is not in correct format! Make sure that it has the identifier in the name.",
        self::NO_FORMAT_IN_DIR => "There wasn't any file in correct filename format in '%value%'.",
        self::MISSING_FILENAME => "Missing filename!",
        self::MISSING_DIRECTORY => "Could not find the directory"
    );

    /**
     * @inheritdoc
     */
    public function isValid($value, $context = null)
    {
        if (empty($value)) {
            return true;
        }

        $format = isset($context['format']) ? $context['format'] : ImageUpload::DEFAULT_FILE_FORMAT;
        $file = $value;
        $single = false;
        if (is_array($value) && isset($value['name'])) {
            $file = $value['name'];
            $single = true;
        } elseif (is_string($value) && is_file($value)) {
            $file = basename($value);
            $single = true;
        }

        if ($single) {
            $this->setValue($file);
            if ($this->isValidName($file, $format)) {
                return true;
            }
            $this->error(self::INCORRECT_FORMAT);
            return false;
        } else if (is_string($value)) {
            $this->setValue($value);
            $path = ImagesPath::UserToLocalFile($value);
            if ($path === false) {
                $this->error(self::MISSING_DIRECTORY);
                return false;
            }
            $files = array_diff(scandir($path), array('..', '.'));
            foreach ($files as $fileneme) {
                if ($this->isValidName($fileneme, $format)) {
                    return true;
                }
            }
            $this->error(self::NO_FORMAT_IN_DIR);
            return false;
        }
        $this->error(self::MISSING_FILENAME);
        return false;
    }

    private function isValidName($file, $format)
    {
        $file = basename($file);
        preg_match('/^' . $format . '$/', $file, $matches);
        if (!isset($matches['documentId'])
            || empty($matches['documentId'])
            || !preg_match(IdService::ID_REGEXP, $matches['documentId'])
        ) {
            return false;
        }
        return true;
    }
}
