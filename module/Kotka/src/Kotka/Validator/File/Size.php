<?php

namespace Kotka\Validator\File;

use Zend\Validator\File\Size as ZendSize;
use Zend\Validator\Exception;

/**
 * Class ImagesPath
 * @package Kotka\Validator
 */
class Size extends ZendSize
{
    /**
     * @inheritdoc
     */
    public function isValid($value, $file = null)
    {
        if (is_string($value) && is_array($file)) {
            $file = isset($file['tmp_name']) ? $file['tmp_name'] : '';
        } elseif (is_array($value)) {
            if (!isset($value['tmp_name'])) {
                throw new Exception\InvalidArgumentException(
                    'Value array must be in $_FILES format'
                );
            }
            $file = $value['tmp_name'];
        } else {
            $file = $value;
        }
        if (empty($file)) {
            return true;
        }

        return parent::isValid($value);
    }
}
