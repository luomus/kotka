<?php

namespace Kotka\Validator;

use Zend\Validator\AbstractValidator;

/**
 * Class Measurement
 * @package Kotka\Validator
 */
class Measurement extends AbstractValidator
{
    const INVALID_FORMAT = 'invalidFormat';
    const NOT_NUMBER = 'notNumber';

    /**
     * Validation failure message template definitions
     *
     * @var array
     */
    protected $messageTemplates = array(
        self::INVALID_FORMAT => "The input does not appear to be in valid format",
        self::NOT_NUMBER => "Measurement value '%value%' is not in correct format. Should be a number with a dot as decimal separator."
    );

    /**
     * @inheritdoc
     */
    public function isValid($value)
    {
        if (is_array($value)) {
            $value = array_filter($value);
        }
        if (empty($value)) {
            return true;
        }
        if (is_string($value)) {
            $value = [$value];
        } else if (!is_array($value)) {
            $this->error(self::INVALID_FORMAT);
            return false;
        }
        foreach ($value as $val) {
            $pos = strpos($val, ':');
            $val = substr($val, $pos + 1);
            if (!$this->validate($val)) {
                $this->setValue($val);
                $this->error(self::NOT_NUMBER);
                return false;
            }
        }
        return true;
    }

    private function validate($number)
    {
        /*
         * Due to indexing this value to Elastic we've decided to not to allow ranges
        $parts  = explode('-', $number);
        if (count($parts) === 2) {
            return $this->validate(trim($parts[0])) && $this->validate(trim($parts[1]));
        }
        */
        return preg_match('/^[0-9]*\.?[0-9]*$/', $number);
    }

}
