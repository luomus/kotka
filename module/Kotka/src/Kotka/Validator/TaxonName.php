<?php
namespace Kotka\Validator;


use Zend\Validator\AbstractValidator;
use Zend\Validator\Exception;

class TaxonName extends AbstractValidator
{

    const INVALID_NAME = 'invalidName';
    const INVALID_RANK = 'invalidRank';

    protected $messageTemplates = array(
        self::INVALID_NAME => "Taxon name contains unallowed characters",
        self::INVALID_RANK => "Please record taxon names below species level to the infra fields",
    );


    /**
     * Returns true if and only if $value meets the validation requirements
     *
     * If $value fails validation, then this method returns false, and
     * getMessages() will return an array of messages that explain why the
     * validation failed.
     *
     * @param  mixed $value
     * @return bool
     * @throws Exception\RuntimeException If validation of $value is impossible
     */
    public function isValid($value)
    {
        if (!is_scalar($value)) {
            return true;
        }
        if (preg_match('{[^0-9a-zëåäö\.,\-\s\(\)]+$}i', $value)) {
            $this->setValue($value);
            $this->error(self::INVALID_NAME);
            return false;
        }
        if (preg_match('{\b(var\.|f(\.|orma)?|s(ub)?sp(\.|ecies)?)+\s}i', $value)) {
            $this->setValue($value);
            $this->error(self::INVALID_RANK);
            return false;
        }
        return true;
    }
}