<?php

namespace Kotka\Validator;

use Zend\Validator\AbstractValidator;

/**
 * Class BothHasNot
 * @package Kotka\Validator
 */
class BothNotSet extends AbstractValidator
{
    const BOTH_SET = 'bothSet';
    const NO_CONTEXT = 'noContext';

    protected $hasNot;

    /**
     * @var array
     */
    protected $messageVariables = array(
        'hasNot' => 'hasNot'
    );


    protected $messageTemplates = array(
        self::BOTH_SET => "Both %value% and %hasNot% cannot be set at the same time.",
        self::NO_CONTEXT => "Missing context"
    );


    /**
     * Sets validator options
     *
     * @param  string|array|\Traversable $options OPTIONAL
     */
    public function __construct($options = array())
    {
        if ($options instanceof \Traversable) {
            $options = iterator_to_array($options);
        } elseif (!is_array($options)) {
            $options = func_get_args();
            $temp['hasNot'] = array_shift($options);
            $options = $temp;
        }

        if (array_key_exists('hasNot', $options)) {
            $this->setHasNot($options['hasNot']);
        }

        parent::__construct($options);
    }

    /**
     * Returns the hasNot option
     *
     * @return string|null
     */
    public function getHasNot()
    {
        return $this->hasNot;
    }

    /**
     * Sets the hasNot option
     *
     * @param  string $hasNot
     * @return self
     */
    public function setHasNot($hasNot = null)
    {
        $this->hasNot = $hasNot;
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function isValid($value, $context = null)
    {
        if ($value === null || trim($value) == '') {
            return true;
        }

        if (!is_array($context)) {
            $this->error(self::NO_CONTEXT);

            return false;
        }

        $hasNot = $this->getHasNot();
        if (!isset($context[$hasNot])) {
            return true;
        }

        if (is_string($context[$hasNot]) && trim($context[$hasNot]) == '') {
            return true;
        } else if (is_array($context[$hasNot]) && $this->isEmptyArray($context[$hasNot])) {
            return true;
        }

        $this->setValue(array_search($value, $context));
        $this->error(self::BOTH_SET);

        return false;
    }

    private function isEmptyArray($array)
    {
        if (empty($array)) {
            return true;
        }
        if (isset($array[0]) && isset($array[0]['name']) && empty($array[0]['name'])) {
            return true;
        }
        return false;
    }

}
