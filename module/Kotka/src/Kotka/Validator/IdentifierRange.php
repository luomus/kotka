<?php

namespace Kotka\Validator;


use Kotka\Model\IdentifierInterpreter;
use Zend\Validator\AbstractValidator;

class IdentifierRange extends AbstractValidator
{
    const INVALID = 'invalid';
    const NOT_RANGE = 'not_range';
    const OVER_LIMIT = 'over_limit';
    const ENC_NOT_NUMBERIC = 'end_not_numeric';

    /**
     * @var array
     */
    protected $messageTemplates = array(
        self::INVALID => 'Invalid type given. String expected',
        self::NOT_RANGE => "'%value%' is not a correct kind of range",
        self::OVER_LIMIT => "Range is over the limit of %limit% ids",
        self::ENC_NOT_NUMBERIC => "End of the range '%value%' is not numeric"
    );

    /**
     * @var array
     */
    protected $messageVariables = array(
        'limit' => 'limit'
    );

    protected $limit;

    public function isValid($value)
    {
        if (is_string($value)) {
            if (!$this->validate($value)) {
                return false;
            }
        } else {
            $this->error(self::INVALID);

            return false;
        }
        return true;
    }

    protected function validate($value)
    {
        $interpreter = new IdentifierInterpreter($value);
        $this->setValue($value);
        if ($interpreter->isRange()) {
            $parts = explode('-', $value);
            $last = array_pop($parts);
            if (!empty($last) && !is_numeric($last)) {
                $this->error(self::ENC_NOT_NUMBERIC);
                return false;
            }
            if ($this->limit !== null && $this->limit < $interpreter->getRangeSize()) {
                $this->error(self::OVER_LIMIT);
                return false;
            }
            return true;
        }
        $this->error(self::NOT_RANGE);
        return false;
    }

    /**
     * @return int
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     */
    public function setLimit($limit)
    {
        $this->limit = (int)$limit;
    }
}