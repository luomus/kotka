<?php

namespace Kotka\Validator;


use Common\Service\IdService;
use Zend\Validator\AbstractValidator;
use Zend\Validator\Regex;

class SearchIdentifier extends AbstractValidator
{

    const INVALID_IDENTIFIER = 'invalid_identifier';
    const INVALID = 'invalid';

    /**
     * @var array
     */
    protected $messageTemplates = array(
        self::INVALID_IDENTIFIER => "Invalid identifier '%value%' given.",
        self::INVALID => 'Invalid type given. String expected',
    );

    private $nsValidator;
    private $rangeValidator;
    private $identifierValidator;

    public function isValid($value)
    {
        if (empty($value)) {
            return true;
        }
        $parts = explode(',', $value);
        foreach ($parts as $part) {
            if (!$this->validate(trim($part))) {
                return false;
            }
        }
        return true;
    }

    protected function validate($value)
    {
        if ($this->getNSValidator()->isValid($value)) {
            return true;
        }
        if ($this->getRangeValidator()->isValid($value)) {
            return true;
        }
        if ($this->getIdentificationValidator()->isValid($value)) {
            return true;
        }
        $this->setValue($value);
        $this->error(self::INVALID_IDENTIFIER);

        return false;
    }

    private function getNSValidator()
    {
        if ($this->nsValidator === null) {
            $this->nsValidator = new Regex(IdService::NS_REGEXP);
        }
        return $this->nsValidator;
    }

    private function getRangeValidator()
    {
        if ($this->rangeValidator === null) {
            $this->rangeValidator = new IdentifierRange();
        }
        return $this->rangeValidator;
    }

    private function getIdentificationValidator()
    {
        if ($this->identifierValidator === null) {
            $this->identifierValidator = new Identifier();
        }
        return $this->identifierValidator;
    }

}