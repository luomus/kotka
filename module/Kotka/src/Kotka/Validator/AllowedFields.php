<?php

namespace Kotka\Validator;

use Zend\Validator\AbstractValidator;

/**
 * Class AllowedFields
 * @package Kotka\Validator
 */
class AllowedFields extends AbstractValidator
{
    const NOT_ALLOWED = 'notAllowed';
    const INVALID_FIELDS = 'invalidFields';

    private $fields;

    private $skip_fields = array(
        'subject' => true,
        'kotka_return' => true,
        'kotka_carryforward' => true,
        'kotka_clear' => true,
        'kotka_copy' => true,
        'status' => true,

    );

    protected $messageTemplates = array(
        self::NOT_ALLOWED => "Disallowed field(s) used: %value%",
        self::INVALID_FIELDS => "Invalid type given"
    );

    public function __construct($options = null)
    {
        if ($options instanceof \Traversable) {
            $options = iterator_to_array($options);
        } elseif (!is_array($options)) {
            $options = func_get_args();
            $temp['fields'] = array_shift($options);
            $options = $temp;
        }

        if (array_key_exists('fields', $options)) {
            $this->setFields($options['fields']);
        }
        parent::__construct($options);
    }

    public function setFields($fields)
    {
        if (!is_array($fields)) {
            return;
        }
        $this->fields = $fields;
    }

    public function getFields()
    {
        return $this->fields;
    }

    /**
     * @inheritdoc
     */
    public function isValid($value, $context = null)
    {
        $fields = $this->fields;
        if (empty($fields)) {
            $this->error(self::INVALID_FIELDS);

            return false;
        }

        $missed = array();
        foreach ($context as $field => $value) {
            if (empty($value)) {
                continue;
            }
            if (!isset($fields[$field]) && !isset($this->skip_fields[$field])) {
                $missed[] = $field;
            }
        }

        if (count($missed) > 0) {
            $this->error(self::NOT_ALLOWED, implode(', ', $missed));

            return false;
        }

        return true;
    }

}
