<?php

namespace Kotka\Validator;

use Common\Service\IdService;
use Zend\Validator\AbstractValidator;
use Zend\Validator\Regex;

class Identifier extends AbstractValidator
{

    const INVALID_IDENTIFIER = 'invalid_identifier';
    const INVALID = 'invalid';

    /**
     * @var array
     */
    protected $messageTemplates = array(
        self::INVALID => 'Invalid type given. String expected',
        self::INVALID_IDENTIFIER => "Invalid identifier '%value%' given.",
    );

    private $uriValidator;
    private $idValidator;

    public function __construct($options = null)
    {
        parent::__construct($options);
        $this->uriValidator = new Regex(IdService::URI_REGEXP);
        $this->idValidator = new Regex(IdService::ID_REGEXP);
    }

    public function isValid($value)
    {
        if (is_string($value)) {
            if (!$this->validate($value)) {
                return false;
            }
        } else {
            $this->error(self::INVALID);

            return false;
        }
        return true;
    }

    protected function validate($value)
    {
        if ($this->getIdValidator()->isValid($value) || $this->getUriValidator()->isValid($value)) {
            return true;
        }
        $this->setValue($value);
        $this->error(self::INVALID_IDENTIFIER);

        return false;
    }

    private function getIdValidator()
    {
        if ($this->idValidator === null) {
            $this->idValidator = new Regex(IdService::ID_REGEXP);
        }
        return $this->idValidator;
    }

    private function getUriValidator()
    {
        if ($this->uriValidator === null) {
            $this->uriValidator = new Regex(IdService::URI_REGEXP);
        }
        return $this->uriValidator;
    }

}