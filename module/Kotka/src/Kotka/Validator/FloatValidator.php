<?php
namespace Kotka\Validator;

use Zend\Validator\Digits;

/**
 * Class Float
 * Validates that the value is in correct float format
 *
 * @package Kotka\Validator
 */
class FloatValidator extends Digits
{

    const FLOAT_POINT = '.';

    const NOT_FLOAT = 'notFloat';
    const STRING_EMPTY = 'floatStringEmpty';
    const INVALID = 'floatInvalid';

    /**
     * Digits filter used for validation
     *
     * @var \Zend\Filter\Digits
     */
    protected static $filter = null;

    /**
     * Validation failure message template definitions
     *
     * @var array
     */
    protected $messageTemplates = array(
        self::NOT_FLOAT => "The input must contain only float",
        self::NOT_DIGITS => "The input must contain only digits",
        self::STRING_EMPTY => "The input is an empty string",
        self::INVALID => "Invalid type given. String, integer or float expected",
    );

    public function isValid($value, $context = null)
    {

        if (!is_string($value) && !is_int($value) && !is_float($value)) {
            $this->error(self::INVALID);
            return false;
        }

        if ('' === $this->getValue()) {
            $this->error(self::STRING_EMPTY);
            return false;
        }
        $valueString = (string)$value;
        if (strpos($valueString, '-') === 0) {
            $valueString = substr($valueString, 1);
        }
        $parts = explode(self::FLOAT_POINT, $valueString);
        $partCnt = count($parts);
        if ($partCnt == 1) {
            $valid = parent::isValid($parts[0]);
        } elseif ($partCnt > 2) {
            $this->error(self::INVALID);
            $valid = false;
        } else {
            $valid = parent::isValid($parts[0]);
            if ($valid) {
                $valid = parent::isValid($parts[1]);
            }
        }
        if (!$valid) {
            $this->setValue((string)$value);
            $this->error(self::NOT_FLOAT);
        }

        return $valid;
    }
} 