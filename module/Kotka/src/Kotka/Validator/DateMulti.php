<?php
namespace Kotka\Validator;

use Zend\Validator\Date;

/**
 * Class DateCompare
 * Validates the value based on context or with  if compare to key
 *
 * @package Kotka\Validator
 */
class DateMulti extends Date
{

    protected $format = array(self::FORMAT_DEFAULT);

    /**
     * Sets the format option
     *
     * Format cannot be null.  It will always default to 'Y-m-d', even
     * if null is provided.
     *
     * @param  string $format
     * @return Date provides a fluent interface
     */
    public function setFormat($format = self::FORMAT_DEFAULT)
    {
        $format = (empty($format)) ? self::FORMAT_DEFAULT : $format;
        $this->format = (is_array($format)) ? $format : array($format);
        return $this;
    }

    public function isValid($value, $context = null)
    {
        $formats = $this->format;
        if (is_array($formats)) {
            foreach ($formats as $format) {
                $this->format = $format;
                $valid = parent::isValid($value);
                if ($valid) {
                    $this->format = $formats;
                    return true;
                }
            }
            $this->setValue($value);
            $this->error(self::INVALID);
            $this->format = $formats;
            return false;
        }
        return parent::isValid($value);
    }
} 