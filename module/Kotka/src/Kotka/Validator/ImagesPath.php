<?php

namespace Kotka\Validator;

use ImageApi\Options\ImageServerConfiguration as Configuration;
use Zend\Validator\AbstractValidator;

/**
 * Class ImagesPath
 * @package Kotka\Validator
 */
class ImagesPath extends AbstractValidator
{
    const NOT_ACCESSIBLE = 'notWritable';
    const HACK_ATTEMPT = 'hackAttempt';

    const IMAGE_REAL_PATH = '/data/kotka/webkuvat/';
    const IMAGE_USER_PATH = 'P:\\h978\\public_all\\webkuva\\';
    const IMAGE_FQDN_PATH = '\\\\group9.ad.helsinki.fi\\h978\\public_all\\webkuva\\';

    public $userPath = self::IMAGE_USER_PATH;

    private static $userPaths = array(
        self::IMAGE_REAL_PATH,
        self::IMAGE_USER_PATH,
        self::IMAGE_FQDN_PATH
    );

    protected $messageTemplates = array(
        self::NOT_ACCESSIBLE => "Directory '%value%' is not accessible. Please make sure that it's not in read only state and are located in '%path%'",
        self::HACK_ATTEMPT => "Directory '%value%' is not valid",
    );

    protected $messageVariables = array(
        'path' => 'userPath'
    );

    /**
     * @inheritdoc
     */
    public function isValid($value, $context = null)
    {
        if (empty($value)) {
            return true;
        }
        $this->setValue($value);

        $localPath = $this->UserToLocalFile($value);

        if ($localPath === false) {
            $this->error(self::NOT_ACCESSIBLE);
            return false;
        }
        if (strpos(strtolower($localPath), self::IMAGE_REAL_PATH) !== 0) {
            $this->error(self::HACK_ATTEMPT);
            return false;
        }
        if (!is_readable($localPath)) {
            $this->error(self::NOT_ACCESSIBLE);
            return false;
        }
        if (!is_writable($localPath)) {
            $this->error(self::NOT_ACCESSIBLE);
            return false;
        }
        return true;
    }

    public static function UserToLocalFile($file)
    {
        $paths = self::$userPaths;
        $userPath = null;
        foreach ($paths as $path) {
            if (strpos($file, $path) === 0) {
                $userPath = $path;
                break;
            }
        }
        if ($userPath === null) {
            return false;
        }
        $path = str_replace([$userPath, '\\'], [self::IMAGE_REAL_PATH, '/'], $file);
        return realpath($path);
    }

}
