<?php

namespace Kotka\Validator;

use Kotka\Service\CoordinateService;
use Traversable;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Zend\Validator\AbstractValidator;

/**
 * Class Coordinate
 * @package Kotka\Validator
 */
class Coordinate extends AbstractValidator implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    const COORDINATE_SYSTEM_MISSING = 'coordinateSystemInvalid';
    const INVALID_DIRECTION = 'invalidDirection';
    const NOT_NUMERIC = 'notNumeric';
    const OUT_OF_BOUNDS = 'outOfBounds';
    const NOT_EQUAL_LENGTH = 'NOT_EQUAL_LENGTH';

    const LATITUDE = 'latitude';
    const LONGITUDE = 'longitude';

    /**
     * Validation failure message template definitions
     *
     * @var array
     */
    protected $messageTemplates = array(
        self::COORDINATE_SYSTEM_MISSING => "No coordinate system specified.",
        self::INVALID_DIRECTION => "Invalid direction given.",
        self::NOT_NUMERIC => "Coordinate must be an integer.",
        self::OUT_OF_BOUNDS => "Coordinate is out of bounds.",
        self::NOT_EQUAL_LENGTH => "Coordinates must have equal length when using YKJ system",
    );

    /**
     * @var array
     */
    protected $messageVariables = array(
        'direction' => 'direction'
    );

    protected $coordinateSystemConstants = array(
        CoordinateService::SYSTEM_WSG84,
        CoordinateService::SYSTEM_WGS84DMS,
        CoordinateService::SYSTEM_YKJ
    );

    protected $directionConstants = array(
        self::LATITUDE,
        self::LONGITUDE,
    );

    protected $direction;
    protected $coordinateSystem;
    protected $coordinateSystemIn;
    protected $otherOfPair;

    /**
     * Sets validator options
     *
     * @param  string|array|Traversable $options OPTIONAL
     */
    public function __construct($options = array())
    {
        if ($options instanceof Traversable) {
            $options = iterator_to_array($options);
        } elseif (!is_array($options)) {
            $options = func_get_args();
            $temp['direction'] = array_shift($options);
            $temp['coordinateSystem'] = array_shift($options);
            $temp['coordinateSystemIn'] = array_shift($options);
            $temp['otherOfPair'] = array_shift($options);
            $options = $temp;
        }

        if (array_key_exists('coordinateSystem', $options)) {
            $this->setCoordinateSystem($options['coordinateSystem']);
        }
        if (array_key_exists('direction', $options)) {
            $this->setDirection($options['direction']);
        }
        if (array_key_exists('coordinateSystemIn', $options)) {
            $this->setCoordinateSystemIn($options['coordinateSystemIn']);
        }
        if (array_key_exists('otherOfPair', $options)) {
            $this->setOtherOfPair($options['otherOfPair']);
        }
        parent::__construct($options);
    }

    /**
     * @return string
     */
    public function getOtherOfPair()
    {
        return $this->otherOfPair;
    }

    /**
     * @param string $otherOfPair
     */
    public function setOtherOfPair($otherOfPair)
    {
        $this->otherOfPair = $otherOfPair;
    }

    /**
     * Returns the direction option
     *
     * @return string|null
     */
    public function getDirection()
    {
        return $this->direction;
    }

    /**
     * Sets the direction option
     *
     * @param string $direction
     * @return $this
     * @throws Exception\InvalidArgumentException
     */
    public function setDirection($direction = null)
    {

        if (!in_array($direction, $this->directionConstants)) {
            throw new Exception\InvalidArgumentException('Unknown direction');
        }
        $this->direction = $direction;
        return $this;
    }

    /**
     * Returns the coordinateSystem option
     *
     * @return string|null
     */
    public function getCoordinateSystem()
    {
        return $this->coordinateSystem;
    }

    /**
     * Sets the coordinateSystem option
     *
     * @param string $coordinateSystem
     * @return $this
     * @throws Exception\InvalidArgumentException
     */
    public function setCoordinateSystem($coordinateSystem = null)
    {
        if (!in_array($coordinateSystem, $this->coordinateSystemConstants)) {
            throw new Exception\InvalidArgumentException('Unknown coordinateSystem');
        }
        $this->coordinateSystem = $coordinateSystem;
        return $this;
    }

    /**
     * Returns the coordinateSystem in option
     *
     * @return string|null
     */
    public function getCoordinateSystemIn()
    {
        return $this->coordinateSystemIn;
    }

    /**
     * Sets the coordinateSystem in option
     *
     * @param string $coordinateSystemIn
     * @return $this
     * @throws Exception\InvalidArgumentException
     */
    public function setCoordinateSystemIn($coordinateSystemIn = null)
    {
        $this->coordinateSystemIn = $coordinateSystemIn;
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function isValid($value, $context = null)
    {
        $coordinateSystemIn = $this->getCoordinateSystemIn();
        $coordinateSystem = $this->getCoordinateSystem();
        $direction = $this->getDirection();
        $coordinateService = $this->getServiceLocator()->getServiceLocator()->get('Kotka\Service\CoordinateService');
        $this->setValue($value);

        if ($direction === null || !in_array($direction, $this->directionConstants)) {
            $this->error(self::INVALID_DIRECTION);

            return false;
        }

        if ($coordinateSystemIn !== null && isset($context[$coordinateSystemIn]) && !empty($context[$coordinateSystemIn])) {
            $coordinateSystem = $context[$coordinateSystemIn];
            $coordinateSystem = $coordinateService->normalizeCoordinateSystem($coordinateSystem);
            // In case when we don't know the system used
            if (!in_array($coordinateSystem, $this->coordinateSystemConstants)) {
                return true;
            }
        }

        if ($coordinateSystem === null) {
            return true;
        }

        $value = str_replace(',', '.', trim($value));

        if ($coordinateSystem === CoordinateService::SYSTEM_WGS84DMS) {
            $value = $coordinateService->dms2degrees($value);
        }


        if (!is_numeric($value)) {
            $this->error(self::NOT_NUMERIC);

            return false;
        }

        if ($coordinateSystem === CoordinateService::SYSTEM_YKJ) {
            $pair = $this->getOtherOfPair();
            if ($pair && isset($context[$pair])) {
                if (strlen($value) !== strlen($context[$pair])) {
                    $this->error(self::NOT_EQUAL_LENGTH);

                    return false;
                }
            }
            $len = strlen($value);
            if ($len > 7 || $len < 3) {
                $this->error(self::OUT_OF_BOUNDS);

                return false;
            }
            $value = $value . "0000000";
            $value = intval(substr($value, 0, 7));
            if ($direction === self::LATITUDE && ($value > 7790000 || $value < 6600000)) {
                $this->error(self::OUT_OF_BOUNDS);

                return false;
            }

            if ($direction === self::LONGITUDE && ($value > 3750000 || $value < 3040000)) {
                $this->error(self::OUT_OF_BOUNDS);

                return false;
            }

            return true;
        }

        if ($direction === self::LATITUDE && ($value > 90 || $value < -90)) {
            $this->error(self::OUT_OF_BOUNDS);

            return false;
        }

        if ($direction === self::LONGITUDE && ($value > 180 || $value < -180)) {
            $this->error(self::OUT_OF_BOUNDS);

            return false;
        }

        return true;
    }
}
