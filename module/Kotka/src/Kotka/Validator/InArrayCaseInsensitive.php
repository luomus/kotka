<?php

namespace Kotka\Validator;


use Kotka\Stdlib\ArrayUtils;
use Zend\Validator\InArray;

class InArrayCaseInsensitive extends InArray
{

    public function setHaystack(array $haystack)
    {
        $haystack = ArrayUtils::array_change_key_case_unicode($haystack, CASE_LOWER);
        parent::setHaystack($haystack);
    }

    public function isValid($value)
    {
        $value = mb_strtolower($value, 'UTF-8');
        return parent::isValid($value);
    }
}