<?php

namespace Kotka\Validator;

use Traversable;
use Zend\Stdlib\ArrayUtils;
use Zend\Validator\AbstractValidator;

/**
 * Class RequiredWhen
 * @package Kotka\Validator
 */
class RequiredWhen extends AbstractValidator
{

    const NOT_EMPTY = 0x001;
    const ALL = 0x001;

    const MISSING = 'missing';

    private $types = [
        self::NOT_EMPTY => 'not_empty',
    ];

    protected $messageTemplates = array(
        self::MISSING => "Required element is missing.",
    );

    protected $field;
    protected $type;

    /**
     * Sets validator options
     *
     * @param  string|array|Traversable $options OPTIONAL
     */
    public function __construct($options = array())
    {
        if ($options instanceof Traversable) {
            $options = ArrayUtils::iteratorToArray($options);
        }

        if (!is_array($options)) {
            $options = func_get_args();
            $temp = array();
            if (!empty($options)) {
                $temp['type'] = array_shift($options);
            }
            $options = $temp;
        }

        if (is_array($options)) {
            if (!array_key_exists('type', $options)) {
                $detected = 0;
                $found = false;
                foreach ($options as $option) {
                    if (in_array($option, $this->types, true)) {
                        $found = true;
                        $detected += array_search($option, $this->types);
                    }
                }

                if ($found) {
                    $options['type'] = $detected;
                }
            }
        }
        parent::__construct($options);
    }

    public function setField($field)
    {
        $this->field = $field;

        return $this;
    }

    public function getField()
    {
        return $this->field;
    }

    /**
     * Returns the set types
     *
     * @return array
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return int
     */
    public function getDefaultType()
    {
        return $this->calculateTypeValue($this->defaultType);
    }

    /**
     * @param array|int|string $type
     * @return int
     */
    protected function calculateTypeValue($type)
    {
        if (is_array($type)) {
            $detected = 0;
            foreach ($type as $value) {
                if (is_int($value)) {
                    $detected |= $value;
                } elseif (in_array($value, $this->types)) {
                    $detected |= array_search($value, $this->types);
                }
            }

            $type = $detected;
        } elseif (is_string($type) && in_array($type, $this->types)) {
            $type = array_search($type, $this->types);
        }

        return $type;
    }

    /**
     * Set the types
     *
     * @param  int|array $type
     * @throws Exception\InvalidArgumentException
     * @return RequireWithThis
     */
    public function setType($type = null)
    {
        $type = $this->calculateTypeValue($type);

        if (!is_int($type) || ($type < 0) || ($type > self::ALL)) {
            throw new Exception\InvalidArgumentException('Unknown type');
        }

        $this->type = $type;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function isValid($value, $context = null)
    {
        $field = $this->getField();
        $type = $this->getType();
        if (null === $field) {
            return true;
        }
        if (!empty($value)) {
            return true;
        }
        if ($type & self::NOT_EMPTY) {
            if (!isset($context[$field]) || empty($context[$field])) {
                return true;
            }
            $this->error(self::MISSING);
            return false;
        }
        return false;
    }

}
