<?php

namespace Kotka\Validator;

use Zend\Validator\AbstractValidator;

class ConditionalList extends AbstractValidator
{
    const NO_ALLOWED_LIST = 'not_in_list';
    const NOT_IN_HAYSTACK = 'not_in_list';

    /**
     * @var array
     */
    protected $messageTemplates = array(
        self::NO_ALLOWED_LIST => "Could not find allowed values for '%value%'",
        self::NOT_IN_HAYSTACK => "Value '%value%' is not valid",
    );

    /** @var string */
    protected $field;
    /** @var string */
    protected $fieldValuePrefix;

    /** @var array
     *
     * Example:
     * 'MF.preparationTypeSkin' => ['MF.materialBirdStudySkin', 'MF.materialWing', 'MF.materialTail', 'MF.materialWingAndTail', 'MF.materialOther'],
     * 'MF.preparationTypeSkeletal'=> ['MF.materialSkull', 'MF.materialShell', 'MF.materialEntireSkeleton', 'MF.materialBones', 'MF.materialSkullAndBones', 'MF.materialAntlers', 'MF.materialTeeth'],
     * 'MF.preparationTypeMount' => ['MF.materialHead'],
     * 'MF.preparationTypeTissue' => ['MF.materialMuscle', 'MF.materialLiver', 'MF.materialLeaf', 'MF.materialBlood', 'MF.materialLeg', 'MF.materialSkin', 'MF.materialFeather', 'MF.materialOther'],
     * 'MF.preparationTypeTissueEcotoxicology' => ['MF.materialFeather', 'MF.materialEggContent', 'MF.materialOther'],
     * 'MF.preparationTypeLiquid' => ['MF.materialEntireOrganism', 'MF.materialTail', 'MF.materialHead', 'MF.materialBodyParts', 'MF.materialEgg', 'MF.materialOther', 'MF.materialGenitalPreparation'],
     * 'MF.preparationTypeMicroscopeSlide' => ['MF.materialEntireOrganism','MF.materialAppendages', 'MF.materialGenitalPreparation', 'MF.materialSection', 'MF.materialChromosomes'],
     * 'MF.preparationTypeDNAExtract' => ['MF.materialGenomicDNA', 'MF.materialMitochondrialDNA', 'MF.materialChloroplastDNA', 'MF.materialEnvironmentalDNA']
     *
     */
    protected $allowedValues;

    public function isValid($value, $context = null)
    {
        if (empty($value)) {
            return true;
        }

        $relative = isset($context[$this->field]) ? $context[$this->field] : '';

        if (isset($this->fieldValuePrefix) && strpos($relative, $this->fieldValuePrefix) !== 0) {
            $relative = $this->fieldValuePrefix . ucfirst($relative);
        }

        if (!isset($this->allowedValues[$relative])) {
            $this->setValue($value);
            $this->error(self::NO_ALLOWED_LIST);

            return false;
        }

        if (!in_array($value, $this->allowedValues[$relative])) {
            $this->setValue($value);
            $this->error(self::NOT_IN_HAYSTACK);

            return false;
        }

        return true;
    }

    /**
     * @return string
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * @param string $field
     */
    public function setField($field): void
    {
        $this->field = $field;
    }

    /**
     * @return string
     */
    public function getFieldValuePrefix(): string
    {
        return $this->fieldValuePrefix;
    }

    /**
     * @param string $fieldValuePrefix
     */
    public function setFieldValuePrefix(string $fieldValuePrefix): void
    {
        $this->fieldValuePrefix = $fieldValuePrefix;
    }

    /**
     * @return mixed
     */
    public function getAllowedValues()
    {
        return $this->allowedValues;
    }

    /**
     * @param mixed $allowedValues
     */
    public function setAllowedValues($allowedValues): void
    {
        $this->allowedValues = $allowedValues;
    }



}