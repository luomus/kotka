<?php

namespace Kotka\Validator;

use Traversable;
use Zend\Validator\AbstractValidator;

/**
 * Class OneOrTheOther
 * @package Kotka\Validator
 */
class OneOrTheOther extends AbstractValidator
{
    const MISSING = 'missing';
    const INVALID = 'invalid';

    protected $other;

    protected $messageTemplates = array(
        self::MISSING => "Both are missing.",
        self::INVALID => "No enough context provided for validation.",
    );

    /**
     * Sets validator options
     *
     * @param  string|array|Traversable $options OPTIONAL
     */
    public function __construct($options = array())
    {
        if ($options instanceof Traversable) {
            $options = iterator_to_array($options);
        } elseif (!is_array($options)) {
            $options = func_get_args();
            $temp['other'] = array_shift($options);
            $options = $temp;
        }

        if (array_key_exists('other', $options)) {
            $this->setOther($options['other']);
        }
        parent::__construct($options);
    }

    public function setOther($other)
    {
        $this->other = $other;

        return $this;
    }

    public function getOther()
    {
        return $this->other;
    }

    /**
     * @inheritdoc
     */
    public function isValid($value, $context = null)
    {
        $other = $this->getOther();
        if ($context === null) {
            $this->error(self::INVALID);

            return false;
        }
        $otherValue = isset($context[$other]) ? $this->otherValue($context[$other]) : '';
        if (empty($value) && empty($otherValue)) {
            $this->error(self::MISSING);

            return false;
        }

        return true;
    }

    private function otherValue($value)
    {
        if (empty($value)) {
            return '';
        }
        if (isset($value[0]) && isset($value[0]['name']) && empty($value[0]['name'])) {
            return '';
        }
        return $value;
    }

}
