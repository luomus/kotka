<?php

namespace Kotka\Validator;

use Common\Service\IdService;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Validator\AbstractValidator;

/**
 * Class UniqueDatasetName
 * @package Kotka\Validator
 */
class UniqueDatasetName extends AbstractValidator implements ServiceLocatorAwareInterface
{
    const PREDICATE_DATASET_NAME = 'GX.datasetName';
    const NOT_UNIQUE = 'notUnique';

    private $serviceLocator;

    protected $messageTemplates = array(
        self::NOT_UNIQUE => "Dataset name is already in use by '%value%'!",
    );

    /**
     * Checks if the uri build from the pieces already exists in the database
     *
     * @param mixed $value
     * @param null $context
     * @return bool
     * @throws Exception\DomainException
     */
    public function isValid($value, $context = null)
    {
        /** @var \Triplestore\Service\ObjectManager $objectManager */
        $objectManager = $this->getServiceLocator()->getServiceLocator()->get('Triplestore\ObjectManager');
        $objectManager->disableHydrator();
        $models = $objectManager->findBy([self::PREDICATE_DATASET_NAME => $value], null, null, null, [self::PREDICATE_DATASET_NAME]);
        if ($models === null) {
            return true;
        }
        foreach ($models as $model) {
            /** @var \Triplestore\Model\Model $model */
            if ($model->getSubject() != $context['subject']) {
                $this->setValue(IdService::getQName($model->getSubject()));
                $this->error(self::NOT_UNIQUE);

                return false;
            }
        }
        return true;
    }

    /**
     * Set service locator
     *
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    /**
     * Get service locator
     *
     * @return ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }
}
