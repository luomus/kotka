<?php
namespace Kotka\Validator;

use DateTime;
use Kotka\Filter\RomanDateToDate;
use Kotka\Model\Util;
use Traversable;
use Zend\Validator\Date;

/**
 * Class DateCompare
 * Validates the value based on context or with  if compare to key
 *
 * @package Kotka\Validator
 */
class DateCompare extends Date
{

    private static $romanFilter;

    const DATES_IN_WRONG_ORDER = 'datesInWrongOrder';
    const INVALID = 'dateInvalid';
    const INVALID_DATE = 'dateInvalidDate';
    const FALSE_FORMAT = 'dateFalseFormat';
    const MISSING_COMPARE_TO = 'missingCompreTo';

    const BEFORE = 'before';
    const AFTER = 'after';

    /**
     * Validation failure message template definitions
     *
     * @var array
     */
    protected $messageTemplates = array(
        self::INVALID => "Invalid type given. String, integer, array or DateTime expected",
        self::INVALID_DATE => "The input '%value%' does not appear to be a valid date",
        self::FALSE_FORMAT => "The input does not fit the date format '%format%'",
        self::DATES_IN_WRONG_ORDER => "Date %value% is not %direction% %compareTo%",
        self::MISSING_COMPARE_TO => "Missing compare to %compareTo% value"
    );

    protected $constants = array(
        self::BEFORE,
        self::AFTER
    );

    /**
     * Optional compareTo
     * Defaults to now
     *
     * @var string|null
     */
    protected $compareTo = 'today';

    /**
     * Required direction
     *
     * @var string|null
     */
    protected $direction;

    /**
     * @var array
     */
    protected $messageVariables = array(
        'format' => 'format',
        'compareTo' => 'compareTo',
        'direction' => 'direction'
    );

    /**
     * Sets validator options
     *
     * @param  string|array|Traversable $options OPTIONAL
     */
    public function __construct($options = array())
    {
        if (!isset(self::$romanFilter)) {
            self::$romanFilter = new RomanDateToDate();
        }
        if ($options instanceof Traversable) {
            $options = iterator_to_array($options);
        } elseif (!is_array($options)) {
            $options = func_get_args();
            $temp['compareTo'] = array_shift($options);
            $temp['direction'] = array_shift($options);
            $options = $temp;
        }

        if (array_key_exists('compareTo', $options)) {
            $this->setCompareTo($options['compareTo']);
        }

        if (array_key_exists('direction', $options)) {
            $this->setDirection($options['direction']);
        } else {
            $this->setDirection(self::BEFORE);
        }

        parent::__construct($options);
    }

    /**
     * Returns the compareTo option
     *
     * @return string|null
     */
    public function getCompareTo()
    {
        return $this->compareTo;
    }

    /**
     * Sets the compareTo option
     *
     * @param  string $compareTo
     * @return DateCompare
     */
    public function setCompareTo($compareTo = null)
    {
        $this->compareTo = $compareTo;
        return $this;
    }

    /**
     * Returns the direction option
     *
     * @return string|null
     */
    public function getDirection()
    {
        return $this->direction;
    }

    /**
     * Sets the compareTo option
     *
     * @param  string $direction
     * @throws Exception\InvalidArgumentException
     * @return DateCompare
     */
    public function setDirection($direction = null)
    {
        if (!in_array($direction, $this->constants)) {
            throw new Exception\InvalidArgumentException('Unknown direction');
        }
        $this->direction = $direction;
        return $this;
    }

    public function isValid($value, $context = null)
    {
        $hasUnknown = false;
        if (is_string($value) && strpos($value, '????')) {
            // Replace ???? with leap year for date validity check
            $value = str_replace('????', '1582', $value);
            $hasUnknown = true;
        }

        $valid = parent::isValid($value);
        if (!$valid) {
            if ($hasUnknown) {
                $this->setValue(str_replace('1582', '????', $value));
            }
            $this->error(self::INVALID_DATE);

            return false;
        }
        if ($hasUnknown) {
            return true;
        }
        $compareTo = $this->getCompareTo();
        $direction = $this->getDirection();

        if ($compareTo !== 'today') {
            if (!isset($context[$compareTo]) || empty($context[$compareTo])) {
                $this->error(self::MISSING_COMPARE_TO);

                return false;
            }
            $compareToValue = self::$romanFilter->filter($context[$compareTo]);
            $valid = parent::isValid($compareToValue);
            if (!$valid) {
                $this->error(self::INVALID_DATE);

                return false;
            }
            $compareTo = $compareToValue;
        } else {
            $compareTo = new DateTime();
        }

        $date1 = $this->getDateTime($value);
        $date2 = $this->getDateTime($compareTo);
        $this->setValue($date1->format(Util::DATETIME_FORMAT));
        if (!$date1 instanceof DateTime || !$date2 instanceof DateTime) {
            $this->error(self::FALSE_FORMAT);

            return false;
        }

        $hasError = ($direction == self::BEFORE && $date1 > $date2)
            || ($direction == self::AFTER && $date1 < $date2);

        if ($hasError) {
            $this->error(self::DATES_IN_WRONG_ORDER);

            return false;
        }
        return true;

    }

    protected function getDateTime($value)
    {
        $format = $this->getFormat();
        if ($value instanceof DateTime) {
            return $value;
        } elseif (is_int($value)
            || (is_string($value) && null !== $format)
        ) {
            return (is_int($value))
                ? date_create("@$value") // from timestamp
                : DateTime::createFromFormat($format, $value);

        }
        return null;
    }

} 