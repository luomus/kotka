<?php

namespace Kotka\Validator;

use Traversable;
use Zend\Validator\AbstractValidator;

/**
 * Class ArrayContains
 * @package Kotka\Validator
 */
class ArrayContains extends AbstractValidator
{
    const NOT_FOUND = 'notFound';
    const NOT_MATCH = 'notMatch';

    protected $contain;
    protected $pattern;

    protected $messageTemplates = array(
        self::NOT_FOUND => "Value doesn't contain wanted value",
        self::NOT_MATCH => "Could not match pattern with '%value%'",
    );

    /**
     * Sets validator options
     *
     * @param  string|array|Traversable $options OPTIONAL
     */
    public function __construct($options = array())
    {
        if ($options instanceof Traversable) {
            $options = iterator_to_array($options);
        } elseif (!is_array($options)) {
            $options = func_get_args();
            $temp['contain'] = array_shift($options);
            $temp['pattern'] = array_shift($options);
            $options = $temp;
        }

        if (array_key_exists('contain', $options)) {
            $this->setContain($options['contain']);
        }
        if (array_key_exists('pattern', $options)) {
            $this->setPattern($options['pattern']);
        }
        parent::__construct($options);
    }

    public function setPattern($pattern)
    {
        $this->pattern = $pattern;

        return $this;
    }

    public function getPattern()
    {
        return $this->pattern;
    }

    public function setContain($contain)
    {
        $this->contain = $contain;

        return $this;
    }

    public function getContain()
    {
        return $this->contain;
    }

    /**
     * @inheritdoc
     */
    public function isValid($value, $context = null)
    {
        $contain = $this->getContain();
        $pattern = $this->getPattern();
        if (!is_array($value)) {
            $value = array($value);
        }
        foreach ($value as $content) {
            if (empty($content)) {
                continue;
            }
            $this->setValue($content);
            if (null !== $contain) {
                if (strpos($content, $contain) === false) {
                    $this->error(self::NOT_FOUND);

                    return false;
                }
            }
            if (null !== $pattern) {
                if (!preg_match($this->pattern, $content)) {
                    $this->error(self::NOT_MATCH);

                    return false;
                }
            }
        }
        return true;
    }

}
