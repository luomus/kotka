<?php

namespace Kotka\Validator;

use Kotka\Form\Element\TaxonRank as Rank;

class TaxonRank extends InDatabaseSelect
{

    protected function getAllowedValues($field)
    {
        if (isset(self::$cache[$field])) {
            return self::$cache[$field];
        }
        $values = $this->getFormElementService()->getSelect($field);
        $remove = false;
        foreach ($values as $key => $value) {
            if ($remove && !isset(Rank::$include[$key])) {
                unset($values[$key]);
            }
            if ($key === Rank::REMOVE_AFTER) {
                $remove = true;
            }
        }
        self::$cache[$field] = $values;

        return $values;
    }

}