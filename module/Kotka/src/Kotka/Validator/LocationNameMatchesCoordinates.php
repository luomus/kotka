<?php

namespace Kotka\Validator;

use Elastic\Query\Filter\GeoShape;
use Elastic\Query\Query;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Zend\Validator\AbstractValidator;

/**
 * Class LocationNameMatchesCoordinates
 * @package Kotka\Validator
 */
class LocationNameMatchesCoordinates extends AbstractValidator implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    const LOCATION_MISMATCH = 'locationMissmatch';

    protected $messageTemplates = array(
        self::LOCATION_MISMATCH => "Municipality doesn't seem to match the coordinates given! Expecting %expected% but got %value%",
    );

    protected $messageVariables = array(
        'expected' => 'expected'
    );

    protected static $cache = [];

    private $adapter;
    private $coordinateService;

    protected $type = 'municipality';
    protected $expected;

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @inheritdoc
     */
    public function isValid($value, $context = null)
    {
        if (empty($value)) {
            return true;
        }
        try {
            $coordinates = $this->getCoordinates($context);
            if (!$coordinates) {
                return true;
            }
            $this->setValue($value);
            $names = $this->findNames($coordinates);
        } catch (\Exception $e) {
            return true;
        }
        if (empty($names) || in_array($value, $names)) {
            return true;
        }
        if (count($names) > 2) {
            $last = array_pop($names);
            $names = [implode(', ', $names), $last];
        }
        $this->expected = implode(' or ', $names);
        $this->error(self::LOCATION_MISMATCH);
        return false;
    }

    /**
     * Returns the names that match given coordinates
     *
     * @param $coordinates
     * @return mixed
     * @throws \Exception
     */
    private function findNames($coordinates)
    {
        if (isset($coordinates[0]) && is_array($coordinates[0])) {
            $type = 'envelope';
            $key = array_map(function ($el) {
                return implode(':', $el);
            }, $coordinates);
            $key = implode(':', $key);
        } else {
            $type = 'Point';
            $key = implode(':', $coordinates);
        }
        if (isset(self::$cache[$key])) {
            return self::$cache[$key];
        }
        $query = new Query('location', $this->type);
        $query->addFilter(new GeoShape('border', $type, $coordinates));
        $results = $this->getAdapter()->query($query)->execute();
        $expecting = [];
        if ($results->count() > 0) {
            foreach ($results as $result) {
                if (isset($result['name']['fi'])) {
                    $expecting[$result['name']['fi']] = true;
                }
                if (isset($result['name']['sv'])) {
                    $expecting[$result['name']['sv']] = true;
                }
            }
        }
        self::$cache[$key] = array_keys($expecting);
        return self::$cache[$key];
    }

    /**
     * Returns coordinates in gro json format
     * Converts them to wgs if needed
     *
     * @param $context
     * @return array|bool
     */
    private function getCoordinates($context)
    {
        if (isset($context['MYCoordinateSystem']) && $context['MYCoordinateSystem'] !== 'MY.coordinateSystemYkj' &&
            isset($context['MYWgs84Latitude']) && !empty($context['MYWgs84Latitude']) &&
            isset($context['MYWgs84Longitude']) && !empty($context['MYWgs84Longitude'])
        ) {
            return [$context['MYWgs84Longitude'], $context['MYWgs84Latitude']];
        }
        if (isset($context['MYLatitude']) && !empty($context['MYLatitude']) &&
            isset($context['MYLongitude']) && !empty($context['MYLongitude']) &&
            isset($context['MYCoordinateSystem']) && !empty($context['MYCoordinateSystem'])
        ) {
            return $this->getCoordinateServce()->convertToWgs84Box(
                $context['MYLatitude'],
                $context['MYLongitude'],
                $context['MYCoordinateSystem'],
                'lng-lat'
            );
        }
        return false;
    }

    /**
     * @return \Elastic\Client\Search
     */
    private function getAdapter()
    {
        if ($this->adapter === null) {
            $this->adapter = $this->getServiceLocator()->getServiceLocator()->get('Elastic\Client\Search');
        }
        return $this->adapter;
    }

    /**
     * @return \Kotka\Service\CoordinateService
     */
    private function getCoordinateServce()
    {
        if ($this->coordinateService === null) {
            $this->coordinateService = $this->getServiceLocator()->getServiceLocator()->get('Kotka\Service\CoordinateService');
        }
        return $this->coordinateService;
    }

}
