<?php

namespace Kotka\Validator;

use Common\Service\IdService;
use Common\Stdlib\StrUtils;
use Triplestore\Classes\MAPersonInterface;
use User\Enum\User;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Validator\AbstractValidator;

/**
 * Class Secondary
 * @package Kotka\Validator
 */
class Secondary extends AbstractValidator implements ServiceLocatorAwareInterface
{
    const SECONDARY = 'secondary';

    private $serviceLocator;

    protected $messageTemplates = array(
        self::SECONDARY => "Uri '%value%' is marked as secondary data in Kotka! As such it cannot be edited in Kotka",
    );

    /**
     * Checks if the uri build from the pieces already exists in the database
     *
     * @param mixed $value
     * @param null $context
     * @return bool
     * @throws Exception\DomainException
     */
    public function isValid($value, $context = null)
    {
        return true; // allow everyone to edit secondary not just admins
    }

    /**
     * Set service locator
     *
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    /**
     * Get service locator
     *
     * @return ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }
}
