<?php
namespace Kotka\Validator;


use Zend\Validator\AbstractValidator;
use Zend\Validator\Exception;

class UniqueArrayContent extends AbstractValidator
{

    const NOT_UNIQUE = 'notFound';

    private $delimiter = ',';

    protected $messageTemplates = array(
        self::NOT_UNIQUE => "Value '%value%' multiple times in the list",
    );

    /**
     * @return string
     */
    public function getDelimiter()
    {
        return $this->delimiter;
    }

    /**
     * @param string $delimiter
     */
    public function setDelimiter($delimiter)
    {
        $this->delimiter = $delimiter;
    }

    /**
     * Returns true if and only if $value meets the validation requirements
     *
     * If $value fails validation, then this method returns false, and
     * getMessages() will return an array of messages that explain why the
     * validation failed.
     *
     * @param  mixed $value
     * @return bool
     * @throws Exception\RuntimeException If validation of $value is impossible
     */
    public function isValid($value)
    {
        if (!is_array($value)) {
            if (is_string($value)) {
                $value = explode($this->delimiter, $value);
                $value = array_map('trim', $value);
            } else {
                return true;
            }
        }
        $unique = array_unique($value);
        if (count($value) != count($unique)) {
            $unique = array_flip($unique);
            foreach ($value as $key => $val) {
                if (isset($unique[$val])) {
                    unset($unique[$val]);
                    unset($value[$key]);
                }
            }
            $this->setValue(implode(', ', $value));
            $this->error(self::NOT_UNIQUE);
            return false;
        }
        return true;
    }
}