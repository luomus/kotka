<?php

namespace Kotka\Validator;

use Zend\Validator\AbstractValidator;

/**
 * Class LimitOccurrence
 * @package Kotka\Validator
 */
class LimitOccurrence extends AbstractValidator
{
    const INVALID = 'invalid';
    const OVER_THE_LIMIT = 'overTheLimit';

    protected $field;
    protected $withValue;
    protected $limit;

    /**
     * @var array
     */
    protected $messageVariables = array(
        'field' => 'field',
        'withValue' => 'withValue',
        'limit' => 'limit'
    );

    protected $messageTemplates = array(
        self::INVALID => "Invalid limit given.",
        self::OVER_THE_LIMIT => "There is more than %limit% %field%s found.",
    );

    /**
     * Sets validator options
     *
     * @param  string|array|\Traversable $options OPTIONAL
     */
    public function __construct($options = array())
    {
        if ($options instanceof \Traversable) {
            $options = iterator_to_array($options);
        } elseif (!is_array($options)) {
            $options = func_get_args();
            $temp['field'] = array_shift($options);
            $temp['limit'] = array_shift($options);
            $temp['withValue'] = array_shift($options);
            $options = $temp;
        }

        if (array_key_exists('field', $options)) {
            $this->setField($options['field']);
        }
        if (array_key_exists('limit', $options)) {
            $this->setLimit($options['limit']);
        }
        if (array_key_exists('withValue', $options)) {
            $this->setWithValue($options['withValue']);
        }

        parent::__construct($options);
    }

    /**
     * Returns the field option
     *
     * @return string|null
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * Sets the field option
     *
     * @param  string $field
     * @return LimitOccurrence
     */
    public function setField($field = null)
    {
        $this->field = $field;

        return $this;
    }

    /**
     * Return the limit option
     *
     * @return int|null
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * Sets the limit option
     *
     * @param int $limit
     * @return LimitOccurrence
     */
    public function setLimit($limit = null)
    {
        $this->limit = $limit;
        return $this;
    }

    public function getWithValue()
    {
        return $this->withValue;
    }

    public function setWithValue($value = null)
    {
        $this->withValue = $value;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function isValid($value, $context = null)
    {
        $field = $this->getField();
        $limit = $this->getLimit();
        $withValue = $this->getWithValue();

        if ($field === null || $limit === null) {
            $this->error(self::INVALID);

            return false;
        }

        $total = $this->occurrences($context, $field, 0, $limit, $withValue);
        if ($total > $limit) {
            $this->error(self::OVER_THE_LIMIT);

            return false;
        }
        return true;

    }

    protected function occurrences($context, $field, $total, $limit, $withValue)
    {
        if ($total > $limit) {
            return $total;
        }
        if (array_key_exists($field, $context)) {
            if ($withValue !== null) {
                return $context[$field] == $withValue ? ++$total : $total;
            }
            return ++$total;
        }
        foreach ($context as $fields) {
            if (is_array($fields)) {
                $total = $this->occurrences($fields, $field, $total, $limit, $withValue);
            }
        }
        return $total;
    }

}
