<?php

namespace Kotka\Validator;

use Kotka\Service\FormElementService;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Validator\AbstractValidator;

/**
 * Class InDatabaseSelect
 * @package Kotka\Validator
 */
class InDatabaseSelect extends AbstractValidator implements ServiceLocatorAwareInterface
{
    const NOT_IN_HAYSTACK = 'notInHaystack';

    protected $serviceLocator;
    protected static $elementService;
    protected static $cache = array();

    protected $messageTemplates = array(
        self::NOT_IN_HAYSTACK => "Value '%value%' is not valid",
    );

    protected $field;

    public function __construct($options = array())
    {
        if ($options instanceof \Traversable) {
            $options = iterator_to_array($options);
        } elseif (!is_array($options)) {
            $options = func_get_args();
            $temp['field'] = array_shift($options);
            $options = $temp;
        }

        if (array_key_exists('field', $options)) {
            $this->setField($options['field']);
        }
        parent::__construct($options);
    }

    public function setField($datatype)
    {
        $this->field = $datatype;

        return $this;
    }

    public function getField()
    {
        return $this->field;
    }

    /**
     * Checks if the uri build from the pieces already exists in the database
     *
     * @param mixed $value
     * @param null $context
     * @return bool
     * @throws Exception\DomainException
     */
    public function isValid($value, $context = null)
    {
        if ($value === '') {
            return true;
        }
        $field = $this->getField();
        if (null == $field) {
            throw new Exception\DomainException(sprintf("No field given."));
        }
        $allowedValues = $this->getAllowedValues($this->field);
        if (isset($allowedValues['group1'])) {
            $allowedValues = $this->getAllOptions($allowedValues);
        }

        if (is_array($value)) {
            foreach ($value as $v) {
                if (!isset($allowedValues[$v])) {
                    $this->setValue($v);
                    $this->error(self::NOT_IN_HAYSTACK);

                    return false;
                }
            }
            return true;
        }

        if (!isset($allowedValues[$value])) {
            $this->setValue($value);
            $this->error(self::NOT_IN_HAYSTACK);

            return false;
        }

        return true;
    }

    protected function getAllowedValues($field)
    {
        if (isset(self::$cache[$field])) {
            return self::$cache[$field];
        }
        $values = $this->getFormElementService()->getSelect($field);
        self::$cache[$field] = $values;

        return $values;
    }

    protected function getFormElementService()
    {
        if (self::$elementService === null) {
            self::$elementService = $this->serviceLocator->getServiceLocator()->get('Kotka\Service\FormElementService');
        }
        return self::$elementService;
    }

    protected function getAllOptions($array)
    {
        $result = array();
        foreach ($array as $key => $value) {
            if (is_array($value) && $key !== FormElementService::ALIAS_KEY) {
                $result = $result + $value['options'];
            } else {
                $result[$key] = $value;
            }
        }
        return $result;
    }

    /**
     * Set service locator
     *
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    /**
     * Get service locator
     *
     * @return ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    public static function clearCache()
    {
        self::$cache = [];
    }
}
