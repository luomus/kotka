<?php

namespace Kotka\Validator;

use Common\Service\IdService;
use Elastic\Query\Query;
use Kotka\DataGrid\ElasticGrid;
use Kotka\ElasticExtract\Specimen;
use Kotka\Model\IdentifierInterpreter;
use Triplestore\Classes\MAPersonInterface;
use Triplestore\Service\ObjectManager;
use Triplestore\Service\PhpVariableConverter;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Validator\AbstractValidator;

/**
 * Class UniqueRange
 * @package Kotka\Validator
 */
class UniqueRange extends AbstractValidator implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    const NOT_UNIQUE = 'notUnique';

    protected $messageTemplates = array(
        self::NOT_UNIQUE => "Id's %value% already exist in Kotka.",
    );

    /**
     * Checks if the uri build from the pieces already exists in the database
     *
     * @param mixed $value
     * @param null $context
     * @return bool
     * @throws Exception\DomainException
     */
    public function isValid($value, $context = null)
    {
        /** @var ServiceLocatorInterface $parentLocator */
        $parentLocator = $this->getServiceLocator()->getServiceLocator();
        /** @var MAPersonInterface $user */
        $user = $parentLocator->get('user');
        $defaultDomain = $user->getMADefaultQNamePrefix() === null ?
            IdService::QNAME_PREFIX_LUOMUS :
            $user->getMADefaultQNamePrefix();
        $interpreter = new IdentifierInterpreter($value, $defaultDomain);
        /** @var \Elastic\Client\Search $adapter */
        $adapter = $parentLocator->get('Elastic\Client\Search');

        $query = new Query(Specimen::getIndexName(), Specimen::getTypeName());
        $query->setSize(10);
        $query->setFields(['documentQName']);
        $query->addSort('objectID', Query::SORT_ASC);
        $ids = [];
        foreach($interpreter->getRange() as $id) {
            $ids[] = '"'.IdService::getQName($id).'"';
        }
        $query->setQueryString('documentQName: (' . implode(' ', $ids) . ')');

        $documents = $adapter->query($query)->execute();
        if ($documents->count() > 0) {
            $this->setValue($this->getUniqueIds($documents));
            $this->error(self::NOT_UNIQUE);
            return false;
        }
        return true;
    }

    private function getUniqueIds($documents) {
        $ids = [];
        foreach($documents as $document) {
            $ids[IdService::getUri($document['documentQName'])] = true;
        }
        return implode(', ', array_keys($ids));
    }

}
