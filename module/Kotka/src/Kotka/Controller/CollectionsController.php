<?php

namespace Kotka\Controller;

use Common\Service\IdService;
use Kotka\Triple\MYCollection;
use Zend\View\Model\ViewModel;

/**
 * Controller for handling collections.
 *
 * @package Kotka\Controller
 */
class CollectionsController extends AbstractKotkaController
{
    /** @var \Kotka\Service\CollectionService */
    protected $documentService;
    protected $addTitle = 'Add collection';
    protected $editTitle = 'Edit collection';

    /**
     * Action for Displaying the list of collections
     * Template: view/kotka/collecions/index.phtml
     *
     * @return \Zend\View\Model\ViewModel
     */
    public function indexAction()
    {
        return new ViewModel(array(
            'collections' => $this->getDocumentService()->getAll(),
            'lang' => $partOf = $this->getRequest()->getQuery('lang', 'en')
        ));
    }

    /**
     * Action for adding new collection
     * Uses the same template as editAction
     *
     * Template: view/kotka/collections/add.phtml
     *
     * @return \Zend\View\Model\ViewModel
     */
    public function addAction()
    {
        $partOf = $this->getRequest()->getQuery('isPartOf');
        if ($partOf !== null) {
            $partOf = IdService::getQName(urldecode($partOf), true);
            $this->addPrePopulatedField('MYIsPartOf', $partOf);
        }

        return parent::addAction();
    }

    /**
     * Action for editing of the collection data
     *
     * Template: view/kotka/collections/edit.phtml
     *
     * @return \Zend\View\Model\ViewModel
     */
    public function editAction()
    {
        return parent::editAction();

    }

    /**
     * Gives the form used for editing
     *
     * @return \Zend\Form\Form
     */
    public function getEditForm()
    {
        /** @var \Kotka\Service\FormGeneratorService $formGenerator */
        $formGenerator = $this->serviceLocator->get('Kotka\Service\FormGeneratorService');
        $form = $formGenerator->getForm(new MYCollection());

        return $form;
    }

    /**
     * Gives the form used for adding
     *
     * @return \Zend\Form\Form
     */
    public function getAddForm()
    {
        $form = $this->getEditForm();
        $form->setObject(new MYCollection());

        return $form;
    }

    /** @return \Kotka\Service\CollectionService */
    protected function getDocumentService()
    {
        if ($this->documentService === null) {
            $this->documentService = $this->serviceLocator->get('Kotka\Service\CollectionService');
        }

        return $this->documentService;
    }

    protected function getEditTitle()
    {
        return $this->editTitle . ' ' . $this->document->getUri();
    }

}
