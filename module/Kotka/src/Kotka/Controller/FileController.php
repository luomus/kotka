<?php

namespace Kotka\Controller;

use Kotka\Triple\KotkaEntity;
use Zend\Http\Header\Referer;
use Zend\Http\Headers;
use Zend\Http\Request;
use Zend\Http\Response\Stream;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;

/**
 * This class handles file handling
 *
 * @package Kotka\Controller
 */
class FileController extends AbstractRestfulController
{
    /** @var  \Kotka\Service\FileService */
    private $fileService;

    private $subject;
    private $type;

    private $msg;
    private $status = 200;

    public function indexAction()
    {
        $filename = $this->params('filename');
        $method = $this->getRequest()->getMethod();
        $this->type = $this->params('element');
        $this->subject = $this->params('subject');
        if (empty($this->type) || empty($this->subject)) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel();
        }
        $this->getFileService();
        $this->fileService->setSubDirectory($this->type);
        switch (strtolower($method)) {
            case 'get':
                if ($filename !== null) {
                    return $this->get($filename);
                }
                return $this->getList();
            case 'post':
                /** @var Request $request */
                $request = $this->getRequest();
                $postData = array_merge_recursive($request->getPost()->toArray(), $request->getFiles()->toArray());
                if ($request->isXmlHttpRequest()) {
                    return $this->create($postData);
                } else {
                    return $this->webCreate($postData);
                }

            case 'delete':
                return $this->delete($filename);
        }
        $this->getResponse()->setStatusCode(404);
        return new JsonModel();
    }

    public function create($data)
    {
        $this->_create($data);
        $view = new JsonModel();
        if ($this->status > 250) {
            $view->setVariables($this->msg);
        }
        $this->getResponse()->setStatusCode($this->status);
        return $view;
    }

    public function webCreate($data)
    {
        $this->_create($data);
        if ($this->status > 250) {
            $this->flashMessenger()->addErrorMessage($this->msg);
        } else {
            $this->flashMessenger()->addSuccessMessage($this->status);
        }
        return $this->redirect()->toUrl($this->getRefererUri());
    }

    private function _create($data)
    {
        if (!$this->userHasRightsToView()) {
            $this->status = 403;
            $this->msg = ['error' => ["You do not have a permission to add files to this"]];

            return;
        }
        $form = $this->getPdfUploadForm();
        $form->setData($data);
        if ($form->isValid()) {
            $data = $form->getData();
            try {
                $result = $this->fileService->store($this->subject, $data['file']['name'], $data['file']['tmp_name']);
            } catch (\Exception $e) {
                $this->status = 500;
                $this->msg = ['error' => [$e->getMessage()]];

                return;
            }
            if (!$result) {
                $this->status = 500;
                $this->msg = ['error' => [$this->fileService->getErrorMessage()]];

                return;
            }
            $this->msg = 'File created';
            $this->status = 201;
        } else {
            $this->msg = ['error' => $form->getMessages()];
            $this->status = 400;
        }
    }

    public function get($filename)
    {
        if (!$this->userHasRightsToView() && $this->type !== 'tmp') {
            $this->getResponse()->setStatusCode(403);
            return new JsonModel();
        }
        $file = $this->fileService->getFile($this->subject, $filename);
        if ($file === false) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel();
        }

        $response = new Stream();
        $response->setStream(fopen($file, 'r'));
        $response->setStatusCode(200);
        $response->setStreamName($filename);

        $headers = new Headers();
        $headers->addHeaders(array(
            'Content-Disposition' => 'attachment; filename="' . $filename . '"',
            'Content-Type' => 'application/octet-stream',
            'Content-Length' => filesize($file)
        ));
        $response->setHeaders($headers);
        return $response;

    }

    public function getList()
    {
        $view = new JsonModel();
        if (!$this->userHasRightsToView()) {
            $this->getResponse()->setStatusCode(403);
            return new JsonModel();
        }
        $view->setVariables($this->fileService->getFileList($this->subject));

        return $view;
    }

    public function delete($filename)
    {
        $view = new JsonModel();
        if (!$this->userHasRightsToView()) {
            $this->getResponse()->setStatusCode(403);
            return $view;
        }
        if ($this->fileService->delete($this->subject, $filename)) {
            $this->getResponse()->setStatusCode(204);
        } else {
            $this->getResponse()->setStatusCode(500);
        }

        return $view;
    }

    private function userHasRightsToView()
    {
        $user = $this->serviceLocator->get('user');
        $organizations = $user->getMAOrganisation();
        if (!is_array($organizations)) {
            $organizations = array();
        }
        $documentService = $this->getCorrespondingDocumentService($this->type);
        if ($documentService === false) {
            return false;
        }
        $document = $documentService->getById($this->subject);
        if (!$document instanceof KotkaEntity) {
            return false;
        }
        $organization = $document->getMZOwner();
        return array_key_exists($organization, $organizations) || empty($organization);
    }


    private function getFileService()
    {
        if ($this->fileService === null) {
            $this->fileService = $this->getServiceLocator()->get('Kotka\Service\FileService');
        }
        return $this->fileService;
    }

    /**
     * Return the import form
     *
     * @return \Kotka\Form\PdfUpload
     */
    protected function getPdfUploadForm()
    {
        return $this->serviceLocator->get('FormElementManager')->get('Kotka\Form\PdfUpload');
    }

    /**
     * @param $type
     * @return bool|\Kotka\Service\DocumentServiceInterface
     */
    private function getCorrespondingDocumentService($type)
    {
        if (isset($this->serviceMap[$type])) {
            $type = $this->serviceMap[$type];
        }
        $candidate = 'Kotka\Service\\' . ucfirst($type) . 'Service';
        $sl = $this->getServiceLocator();
        if ($sl->has($candidate)) {
            return $sl->get($candidate);
        }
        return false;
    }

    private function getRefererUri()
    {
        $referer = $this->getRequest()->getHeader('Referer');
        if ($referer instanceof Referer) {
            return $referer->getUri();
        }
        return '/';
    }

}
