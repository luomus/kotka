<?php

namespace Kotka\Controller;

use Kotka\ElasticExtract\Specimen;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

/**
 * This class is used for the small pickers used in Kotka
 * for example when selecting organization for transaction or
 * collection for specimen
 *
 * @package Kotka\Controller
 */
class PickerController extends AbstractActionController
{
    const PICKER_CACHE_PREFIX = 'picker_';
    const MAX = 20;

    /**
     * @var \Kotka\Service\CollectionService
     */
    protected $collectionService;

    /**
     * @var \Kotka\Service\OrganizationService
     */
    protected $organizationService;

    /**
     * @var \Zend\Db\Sql\Sql
     */
    protected $sql;

    protected $cache;

    public function autocompleteAction()
    {
        $term = $this->getRequest()->getQuery('term');
        $type = $this->getEvent()->getRouteMatch()->getParam('type');

        /** @var \Elastic\Service\AutocompleteService $autocompleteService */
        $autocompleteService = $this->getServiceLocator()->get('Elastic\Service\AutocompleteService');
        $autocompleteService->setExtract(new Specimen());
        $results = $autocompleteService->autocomplete($type, $term, 30);

        if (empty($results)) {
            return new JsonModel([]);
        }
        $data = [];
        foreach ($results as $result) {
            $data[] = array(
                'id' => $result,
                'text' => $result,
            );
        }
        return new JsonModel($data);
    }

    /**
     * This is for handling requests that have something to do with organizations
     * @return ViewModel
     */
    public function organizationsAction()
    {
        $type = $this->getEvent()->getRouteMatch()->getParam('type');
        $term = $this->getRequest()->getQuery('term');
        $cache = $this->getCache();
        $key = self::PICKER_CACHE_PREFIX . $type;
        if (empty($term)) {
            return new JsonModel();
        }
        if (!$cache->hasItem($key)) {
            $organizations = $this->getOrganizationService()->getAll();
            $result = array();
            foreach ($organizations as $organization) {
                /** @var \Kotka\Triple\MOSOrganization $organization */
                if ('string' == $type) {
                    $result[$organization->getMOSOrganizationLevel1('en')] = $organization->getMOSOrganizationLevel1('en');
                } else {
                    $result[$organization->getSubject()] = $this->getOrganizationService()->getOrganizationName($organization);
                }
            }
            $cache->setItem($key, $result);
        }
        $organizations = $cache->getItem($key);
        $data = array();
        foreach ($organizations as $key => $value) {
            if (strpos(strtolower($value), strtolower($term)) !== false) {
                $data[] = array(
                    'id' => $key,
                    'text' => $value,
                );
            }
        }

        return new JsonModel($data);
    }

    /**
     * Return collection service
     *
     * @return \Kotka\Service\CollectionService
     */
    protected function getCollectionService()
    {
        if ($this->collectionService === null) {
            $this->collectionService = $this->serviceLocator->get('Kotka\Service\CollectionService');
        }

        return $this->collectionService;
    }

    /**
     * Return collection service
     *
     * @return \Zend\Cache\Storage\StorageInterface
     */
    protected function getCache()
    {
        if ($this->cache === null) {
            $this->cache = $this->serviceLocator->get('cache');
        }

        return $this->cache;
    }

    /**
     * Return organization service
     *
     * @return \Kotka\Service\OrganizationService
     */
    protected function getOrganizationService()
    {
        if ($this->organizationService === null) {
            $this->organizationService = $this->serviceLocator->get('Kotka\Service\OrganizationService');
        }

        return $this->organizationService;
    }

}
