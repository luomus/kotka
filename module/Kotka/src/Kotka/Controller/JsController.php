<?php

namespace Kotka\Controller;

use Common\Db\SequenceGeneratorInterface;
use Elastic\Client\Search;
use Elastic\Extract\ExtractInterface;
use Elastic\Query\Aggregate;
use Elastic\Query\Query;
use Elastic\Result\TypeMapping;
use Kotka\DataGrid\ElasticGrid;
use Kotka\ElasticExtract\Branch;
use Kotka\ElasticExtract\Sample;
use Kotka\ElasticExtract\Specimen;
use Common\Service\IdService;
use Kotka\Service\NavigationService;
use Kotka\Triple\MAPerson;
use User\Enum\User as UserEnum;
use User\View\Helper\User;
use Zend\Escaper\Escaper;
use Zend\Form\FormInterface;
use Zend\Http\Request;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Paginator\Paginator;
use Zend\View\Model\JsonModel;
use ZfcDatagrid\Renderer\Json\Renderer;

/**
 * Class JsController
 * Is responsible for communicating with javascript
 *
 * @package Kotka\Controller
 */
class JsController extends AbstractActionController
{
    /**
     * This send the initial data for the javascript
     *
     * @return jsonModel
     */
    public function initAction()
    {
        /** @var MAPerson $user */
        $user = $this->getServiceLocator()->get('user');
        $email = $user->getMAEmailAddress();
        if (is_array($email)) {
            $email = array_pop($email);
        }
        $admin = '';
        if (in_array($user->getMARoleKotka(), [UserEnum::ROLE_ADMIN, UserEnum::ROLE_ADVANCED])) {
            $role = str_replace('MA.', '', $user->getMARoleKotka());
            $admin = " ($role) ";
        }
        /** @var User $helper */
        $helper = $this->getServiceLocator()->get('viewhelpermanager')->get('User');
        $data = array(
            'user' => array(
                'username' => $user->getMALTKMLoginName() . $admin,
                'fullname' => $helper(User::FULLNAME),
                'email' => $email
            ));
        return new JsonModel($data);
    }

    public function historyAction()
    {
        $history = $this->getRequest()->getQuery('history', false);
        $type = $this->getRequest()->getQuery('type', false);
        $uri = $this->getRequest()->getQuery('uri', false);
        $uri = IdService::getQName($uri);
        if ($history === false || $type === false || $uri === false) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(['status' => 'Missing required parameter']);
        }
        /** @var \Kotka\Service\FormGeneratorService $formGenerator */
        $formGenerator = $this->getServiceLocator()->get('Kotka\Service\FormGeneratorService');
        /** @var \Triplestore\Service\ObjectManager $om */
        $om = $this->getServiceLocator()->get('Triplestore\ObjectManager');
        $document = $om->fetchFromHistory($uri, $history, $type);
        $form = $formGenerator->getForm($document);
        $form->bind($document);
        $form->isValid();
        $formData = $form->getData(FormInterface::VALUES_AS_ARRAY);


        return new JsonModel(['status' => 'ok', 'data' => $formData]);
    }

    public function aggregateSampleAction() {
        return $this->aggregateAction(new Sample(), SearchController::SESSION_SAMPLE_NAVIGATE_KEY);
    }

    public function aggregateBranchAction() {
        return $this->aggregateAction(new Branch(), SearchController::SESSION_BRANCH_NAVIGATE_KEY);
    }

    public function aggregateAction(ExtractInterface $extract = null, $queryCache = SearchController::SESSION_NAVIGATE_KEY)
    {
        if ($extract === null) {
            $extract = new Specimen();
        }
        $params = $this->Store()->fetch($queryCache);
        /** @var \Elastic\Client\Search $adapter */
        $adapter = $this->serviceLocator->get('Elastic\Client\Search');
        /** @var TypeMapping $types */
        $types = $adapter->getMappings($extract::getIndexName(), $extract::getTypeName());
        $grid = new ElasticGrid($extract);
        $query = $grid->CriteriaToQuery($params);
        $query->clearAggregate();
        $aggr = $this->getRequest()->getQuery('aggr', []);
        $size = (int)$this->getRequest()->getQuery('size', 10);
        if (empty($aggr) || !is_array($aggr)) {
            return new JsonModel([]);
        }
        if ($size < 1 || $size > 100) {
            $size = 10;
        }
        foreach ($aggr as $field) {
            $type = $types->getPropertyType(str_replace(TypeMapping::RAW_SUFFIX, '', $field));
            if ($type === null) {
                continue;
            }
            if ($type === 'geo_point') {
                // TODO: deside how to filter results with this
            } else {
                $query->addAggregate(new Aggregate(Aggregate::TYPE_TERMS, $field, $field, $size));
            }
        }
        $results = $adapter->query($query)->execute();
        $data = [];
        $aggrs = $results->getAggregations();
        $rawAggr = $results->getResource()['aggregations'];
        foreach ($aggrs as $field => $aggr) {
            if (!isset($aggr['buckets'])) {
                continue;
            }
            $type = $types->getPropertyType($field);
            $cntKey = $field . '_count';
            $data[$field] = [
                'total' => isset($rawAggr[$cntKey]) ? $rawAggr[$cntKey]['value'] : 0,
                'results' => []
            ];
            foreach ($aggr['buckets'] as $value) {
                $fieldData = [];
                if ($type === 'date') {
                    $fieldData['key'] = addslashes($value['key_as_string']);
                    $fieldData['doc_count'] = $value['doc_count'];
                } else {
                    $fieldData['key'] = addslashes($value['key']);
                    $fieldData['doc_count'] = $value['doc_count'];
                }
                $data[$field]['results'][] = $fieldData;
            }
        }
        return new JsonModel($data);
    }

    public function sampleAutocompleteAction()
    {
        return $this->autocompleteAction(new Sample());
    }

    public function branchAutocompleteAction()
    {
        return $this->autocompleteAction(new Branch());
    }

    public function autocompleteAction(ExtractInterface $extract = null)
    {
        if ($extract === null) {
            $extract = new Specimen();
        }
        /** @var Search $adapter */
        $adapter = $this->serviceLocator->get('Elastic\Client\Search');
        /** @var TypeMapping $mappings */
        $mappings = $adapter->getMappings($extract::getIndexName(), $extract::getTypeName());
        $query = new Query($extract::getIndexName(), $extract::getTypeName());
        $q = $this->getRequest()->getQuery('q', false);
        $field = $this->getRequest()->getQuery('field', '_all');
        if ($q === false || ($field !== '_all' && !$mappings->hasProperty($field))) {
            return new JsonModel([]);
        }
        $query->setWildcard($q . '*', $field);
        $query->setSize(10);
        $results = $adapter->query($query)->execute();
        return new JsonModel($results);
    }

    public function navigateAction()
    {
        $page = $this->getRequest()->getQuery('page', false);
        $spot = $this->getRequest()->getQuery('spot', false);
        $uri = $this->getRequest()->getQuery('uri', false);
        $type = $this->getRequest()->getQuery('type', 'document');
        if ($spot === false || $page === false) {
            return new JsonModel([]);
        }
        $info = $this->getInfoByType($type);
        $params = $this->Store()->fetch($info[1]);
        $params['page'] = $page;
        Renderer::$params = $params;
        /** @var \Elastic\Client\Search $adapter */
        $adapter = $this->serviceLocator->get('Elastic\Client\Search');
        $gridWrap = new ElasticGrid($info[0]);
        $gridWrap->setServiceLocator($this->getServiceLocator());
        $fields = isset($params['fields']) ? $params['fields'] : [];
        $gridWrap->prepareGrid($params, $adapter, $fields);
        $grid = $gridWrap->getGrid('json');
        $result = $grid->getResponse();
        /** @var Paginator $paginator */
        $paginator = $result['paginator'];
        $navigator = new NavigationService($type);
        $prev = $navigator->findNextDifferent($paginator, $page, $spot, $uri, true);
        $next = $navigator->findNextDifferent($paginator, $page, $spot, $uri);

        return new JsonModel(['prev' => $prev, 'next' => $next, 'type' => $type]);
    }

    private function getInfoByType($type) {
        if ($type === 'sample') {
            return [new Sample(), SearchController::SESSION_SAMPLE_NAVIGATE_KEY];
        } elseif ($type === 'branch') {
            return [new Branch(), SearchController::SESSION_BRANCH_NAVIGATE_KEY];
        }
        return [new Specimen(), SearchController::SESSION_NAVIGATE_KEY];
    }

    public function authorAction()
    {
        $taxon = $this->getRequest()->getQuery('taxon', false);
        $rank = $this->getRequest()->getQuery('rank', false);
        if (!$taxon || !$rank) {
            return new JsonModel([]);
        }

        /** @var \Kotka\Service\FormElementService $elementService */
        $elementService = $this->getServiceLocator()->get('Kotka\Service\FormElementService');
        $authors = $elementService->getAuthor($taxon, $rank);

        return new JsonModel($authors);
    }

    public function searchBranchAction() {
        return $this->searchAction(new Branch());
    }

    public function searchSampleAction() {
        return $this->searchAction(new Sample());
    }

    public function searchAction(ExtractInterface $extract = null)
    {
        if ($extract === null) {
            $extract = new Specimen();
        }
        $field = $this->getRequest()->getQuery('field', null);
        $search = $this->getRequest()->getQuery('search', '');
        /** @var \Elastic\Service\AutocompleteService $autocompleteService */
        $autocompleteService = $this->getServiceLocator()->get('Elastic\Service\AutocompleteService');
        $autocompleteService->setExtract($extract);
        $results = $autocompleteService->autocomplete($field, $search, 20);

        return new JsonModel($results);
    }

    public function accessionOriginalSpecimenIDAction() {
        /** @var SequenceGeneratorInterface $seqGen */
        $seqGen = $this->getServiceLocator()->get('Triplestore\Service');
        $config = $this->getServiceLocator()->get('Config');
        $next = $seqGen->getNextSeqVal('ACCESSION');
        if (isset($config['accessionYearlyNumberReset'])) {
            $next = $next - $config['accessionYearlyNumberReset'];
        }
        return new JsonModel(['id' => date("Y") . '-' . str_pad($next, 4, '0', STR_PAD_LEFT)]);
    }

    public function checkOriginalSpecimenIDAction() {
        /** @var Search $adapter */
        $adapter = $this->serviceLocator->get('Elastic\Client\Search');
        /** @var TypeMapping $mappings */
        $query = new Query(Specimen::getIndexName(), Specimen::getTypeName());
        $value = $this->getRequest()->getQuery('value', false);
        if ($value === false) {
            return new JsonModel(['status' => 'ok']);
        }
        $queryStr = 'originalSpecimenID:"' . $value . '"';
        $query->setQueryString($queryStr);
        $query->setSize(1);
        $results = $adapter->query($query)->execute();
        if ($results->count() > 0) {
            $excaper = new Escaper('utf-8');
            return new JsonModel([
                'status' => 'fail',
                'searchLink' => 'Original catalogue number <a target="_blank" href="/specimens/search?q=' . $excaper->escapeHtmlAttr($queryStr) . '">already in use</a>'
            ]);
        }
        return new JsonModel(['status' => 'ok']);
    }

    public function checkSampleAdditionalIDAction() {
        /** @var Search $adapter */
        $adapter = $this->serviceLocator->get('Elastic\Client\Search');
        /** @var TypeMapping $mappings */
        $query = new Query(Specimen::getIndexName(), Specimen::getTypeName());
        $value = $this->getRequest()->getQuery('value', false);
        if ($value === false) {
            return new JsonModel(['status' => 'ok']);
        }
        $queryStr = 'sampleAdditionalIDs:"' . $value . '"';
        $query->setQueryString($queryStr);
        $query->setSize(1);
        $results = $adapter->query($query)->execute();
        if ($results->count() > 0) {
            $excaper = new Escaper('utf-8');
            return new JsonModel([
                'status' => 'fail',
                'searchLink' => 'Additional ID <a target="_blank" href="/specimens/search?q=' . $excaper->escapeHtmlAttr($queryStr) . '">already in use</a>'
            ]);
        }
        return new JsonModel(['status' => 'ok']);
    }

    public function generateIdsAction() {
        /** @var \Kotka\Service\SequenceService $seqService */
        $seqService = $this->serviceLocator->get('Kotka\Service\SequenceService');
        /** @var Request $request */
        $request = $this->getRequest();
        $json = $request->getQuery('g', false);
        if ($json === false) {
            return new JsonModel([]);
        }
        $data = json_decode($json, true);
        foreach ($data as &$row) {
            $parts = explode(':', $row);
            if (!isset($parts[1]) || !empty($parts[1]) || empty($parts[0])) {
                continue;
            }
            $row .= $seqService->next($parts[0]);
        }

        return new JsonModel($data);
    }
}
