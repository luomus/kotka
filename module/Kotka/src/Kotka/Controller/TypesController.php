<?php

namespace Kotka\Controller;

use Elastic\Client\Search;
use Kotka\DataGrid\TypeGrid;
use Common\Service\IdService;
use Zend\Http\Request;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Paginator\Paginator;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class TypesController extends AbstractActionController
{

    protected $defaultParams = [
        'accepted' => 1,
        'types' => 1,
    ];

    /**
     * Default action if none provided
     *
     * @return array
     */
    public function indexAction()
    {
        $this->getResponse()->setStatusCode(404);
    }


    public function byCollectionAction()
    {
        /** @var Request $request */
        $request = $this->getRequest();
        $query = $request->getQuery(null, [])->toArray();
        $params = array_replace($query, $this->defaultParams);
        $collection = $request->getQuery('uri', 'HR.128');
        $collection = IdService::getQName($collection);

        /** @var \Elastic\Client\Search $adapter */
        $adapter = $this->serviceLocator->get('Elastic\Client\Search');

        $grid = new TypeGrid();
        $grid->setServiceLocator($this->getServiceLocator());

        $grid->prepareGrid($params, $adapter);
        $grid->addSpotParameter();
        $result = $grid->getGrid()->getResponse();
        /** @var Paginator $paginator */
        $paginator = $result['paginator'];

        return new ViewModel(['specimens' => $paginator, 'collection' => $collection, 'criteria' => $query]);
    }

    public function countInCollectionAction()
    {
        /** @var Request $request */
        $request = $this->getRequest();
        $uri = $request->getQuery('uri', 'HR.128');
        $collection = IdService::getUri($uri);

        /** @var Search $adapter */
        $adapter = $this->serviceLocator->get('Elastic\Client\Search');
        $grid = new TypeGrid();
        $query = $grid->CriteriaToQuery([
            'accepted' => 1,
            'types' => 1,
            'uri' => $uri
        ]);
        $query->setSize(1);
        $results = $adapter->query($query)->execute();

        return new JsonModel(['collection' => $collection, 'count' => $results->count()]);
    }

}
