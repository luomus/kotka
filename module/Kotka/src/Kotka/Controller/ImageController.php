<?php

namespace Kotka\Controller;

use ImageApi\Model\Media;
use ImageApi\Model\PictureMeta;
use ImageApi\Service\ImageService;
use ImageApi\Util\ArrayUtil;
use Kotka\Form\ImageUpload;
use Kotka\Http\HttpClientTrait;
use Kotka\Job\Images;
use Common\Service\IdService;
use Kotka\Triple\MAPerson;
use Triplestore\Classes\MAPersonInterface;
use Zend\Form\FormInterface;
use Zend\Http\Request;
use Zend\Http\Response;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

/**
 * This class handles sending multiple images to Kotka
 *
 * @package Kotka\Controller
 */
class ImageController extends AbstractActionController
{
    use HttpClientTrait;

    private $imageService;
    private $specimenService;

    protected $searchFields = [
        'documentIds' => 'Document Id',
        'capturers' => 'Capturer',
        'rightsOwner' => 'Holder of intellectual copyright',
        'identifications_taxonIds' => 'Taxon Id'
    ];

    public function editAction()
    {
        if ($this->getRequest()->getQuery('noLayout')) {
            $this->layout('layout/minimal');
        }
        $vm = [];
        /** @var Request $request */
        $request = $this->getRequest();
        /** @var \ImageApi\Service\ImageService $imageService */
        $imageService = $this->getServiceLocator()->get('ImageService');
        $vm['params'] = $request->getQuery()->toArray();
        if (isset($vm['params']['documentIds']) && !IdService::isUri($vm['params']['documentIds'])) {
            $vm['params']['documentIds'] = IdService::getUri($vm['params']['documentIds']);
            $vm['images'] = $imageService->search($vm['params']);
            if (empty($vm['images'])) {
                $vm['images'] = $imageService->search($vm['params']);
            }
        } else {
            $vm['images'] = $imageService->search($vm['params']);
        }
        $vm['images'] = $vm['images'] ?: [];
        $vm['fields'] = $this->searchFields;
        $vm['metaForm'] = $this->getMetaForm();
        $viewHelperManager = $this->getServiceLocator()->get('ViewHelperManager');
        $user = $viewHelperManager->get('user');
        if ($user->isGuest() &&
            (!isset($vm['params']['personToken']) ||
            !$this->getPerson($vm['params']['personToken']))) {
            $vm['params']['personToken'] = '';
            $vm['images'] = [];
        }
        return new ViewModel($vm);
    }

    public function taxonAction()
    {
        $this->layout('layout/minimal');
        $view = $this->indexAction();
        $view->setVariable('taxonID', $this->getRequest()->getQuery('taxonID'));

        return $view;
    }

    public function indexAction()
    {
        $view = new ViewModel();
        $imagesForm = $this->getImagesForm();
        $allowLoggedOut = false;
        $person = [];
        $metaForm = $this->getMetaForm();
        $request = $this->getRequest();
        $personToken = $request->getQuery('personToken', false);
        $view->setVariable('personToken', $personToken);
        $view->setVariable('imagesForm', $imagesForm);
        $view->setVariable('metaForm', $metaForm);
        if ($personToken) {
            $person = $this->getPerson($personToken);
            if ($person) {
                $allowLoggedOut = true;
            } else {
                $errors['images']['upload'][] = "Unable to authenticate the user!";
                $imagesForm->setMessages($errors);
                return $view;
            }
        }
        if ($request->isPost()) {
            $postData = array_merge_recursive($request->getPost()->toArray(), $request->getFiles()->toArray());
            if (!isset($postData['check']) || $postData['check'] !== 'ok') {
                $max = ini_get("upload_max_filesize");
                $errors['images']['upload'][] = "Combined size of of all the images has to be less than $max. (You can use directory path option to get around this limitation)";
                $imagesForm->setMessages($errors);
                return $view;
            }
            if (isset($postData['images']) && count($postData['images']) > ImageUpload::MAX_FILE_COUNT) {
                $errors['images']['upload'][] = "Too many images, maximum '" . ImageUpload::MAX_FILE_COUNT . "' are allowed. Please use path option to upload your images or upload them in smaller sets.";
                $imagesForm->setMessages($errors);
                return $view;
            }
            $imagesForm->setData($postData);
            $metaForm->setData($postData);
            if ($metaForm->isValid() && $imagesForm->isValid()) {
                /** @var \ImageApi\Service\ImageService $imageService */
                /** @var PictureMeta $meta */
                $imageService = $this->getServiceLocator()->get('ImageService');
                $success = false;
                if (isset($postData['path']) && !empty($postData['path'])) {
                    $meta = $metaForm->getData(FormInterface::VALUES_NORMALIZED);
                    $imageService->uploadEvent($meta);
                    /** @var MAPerson $user */
                    $user = $allowLoggedOut ? $person : $this->getServiceLocator()->get('user');
                    $email = $user->getMAEmailAddress();
                    $queue = $this->getServiceLocator()->get('SlmQueue\Queue\QueuePluginManager')->get('default');
                    $job = new Images();
                    $job->setContent([
                        'meta' => $meta->getArrayCopy(),
                        'path' => $postData['path'],
                        'format' => $postData['format'],
                        'userID' => $user->getSubject(),
                        'email' => $email,
                        'name' => $user->getMAFullName()
                    ]);
                    $queue->push($job);
                    $success = "Successfully added instructions to the background process. You'll be notified by email to $email when the import is complete.";
                } else {
                    $errors = [];
                    $images = $postData['images'];
                    $data = $metaForm->getData(FormInterface::VALUES_AS_ARRAY);
                    $documents = [];
                    foreach ($images as $image) {
                        $meta = new PictureMeta();
                        $meta->exchangeArray($data);
                        if ($allowLoggedOut) {
                            $meta->setAllowLoggedOut($allowLoggedOut);
                            $meta->setUploadedBy($person->getSubject());
                        }
                        $meta->setOriginalFilename($image['name']);
                        if ($imageService->uploadPicture($image['tmp_name'], $meta, $postData['format'], false) === false) {
                            $reason = $imageService->getResponseMessage();
                            $errors['images']['upload'][] = 'Failed to add \'' . $image['name'] . '\' Reason: ' . $reason;
                        } else {
                            $documentIds = $meta->getDocumentIds();

                            foreach ($documentIds as $documentId) {
                                $pos = strpos($documentId, 'MX.');
                                if ($pos === false || $pos !== 0) {
                                    isset($documents[$documentId]) ? $documents[$documentId]++ : $documents[$documentId] = 1;
                                }
                            }
                        }
                    }
                    if ($documents) {
                      /** @var \Kotka\Service\SpecimenService $specimenService */
                      $specimenService = $this->getSpecimenService();

                      $document_keys = array_keys($documents);
                      $specimenService->imageUploadResend($document_keys);
                    }
                    if (count($errors) == 0) {
                        $success = 'Successfully added the new images!';
                    } else {
                        $imagesForm->setMessages($errors);
                    }
                }
                $view->setVariable('success', $success);
            }
        }
        return $view;
    }

    public function getAction()
    {
        $type = $this->getRequest()->getQuery('type');
        $imageService = $this->getImageService();
        switch ($type) {
            case 'dropzone':
                $documentId = $this->getRequest()->getQuery('documentId');
                $data = $imageService->getImages($documentId, true);
                if ($data === false) {
                    break;
                }
                foreach ($data as $key => $image) {
                    $secret = '';
                    if (isset($image['secretKey']) && !empty($image['secretKey'])) {
                        $secret = '?secret=' . $image['secretKey'];
                    }
                    $data[$key] = [];
                    $data[$key]['name'] = $image['meta']['originalFilename'];
                    $data[$key]['type'] = 'image/jpeg';
                    $data[$key]['size'] = 0;
                    $data[$key]['prev'] = $image['urls']['thumbnail'] . $secret;
                    $data[$key]['data'] = $image;
                }
                break;
            case 'single':
                $id = $this->getRequest()->getQuery('id');
                $data = $imageService->getImage($id, true);
                break;
            default:
                $data = [];
                break;
        }
        if ($data === false) {
            return $this->errorResponse($imageService);
        }
        return new JsonModel($data);
    }

    public function postAction()
    {
        $data = $this->getRequest()->getFiles();
        $form = $this->getImageForm();
        $documentId = $this->getRequest()->getQuery('documentId');
        $documentIds = $this->getRequest()->getQuery('documentIds', null);
        if ($documentIds === null) {
            $documentIds = [$documentId];
        }
        $jsonData = [];
        $form->setData($data);
        if ($form->isValid()) {
            $options = $this->getOptions();
            $data = $form->getData();
            $meta = new PictureMeta();
            $imageService = $this->getImageService();
            if (!isset($data[$options->getImageParam()])) {
                throw new \RuntimeException("Post data didn't include picture data in '" . $options->getImageParam() . "' parameter.");
            }
            $imageData = $data[$options->getImageParam()];
            $meta->setOriginalFilename($imageData['name']);
            $meta->setSourceSystem($options->getSystem());
            $meta->setDocumentIds($documentIds);
            $result = $imageService->uploadPicture($imageData['tmp_name'], $meta);
            if ($result === false) {
                return $this->errorResponse($imageService);
            }
            $jsonData['id'] = $result;
        } else {
            return $this->errorResponseString(ArrayUtil::multi_implode(', ', $form->getMessages()));
        }
        return new JsonModel($jsonData);
    }

    public function deleteAction()
    {
        $id = $this->getRequest()->getQuery('id');
        $imageService = $this->getImageService();

        $documentIds = $imageService->getImage($id)->getMeta()->getDocumentIds();

        if (!$imageService->removeImage($id)) {
            return $this->errorResponse($imageService);
        }

        /** @var \Kotka\Service\SpecimenService $specimenService */
        $specimenService = $this->getSpecimenService();

        if ($documentIds) {
          $specimenService->imageUploadResend($documentIds);
        }

        return new JsonModel();
    }

    public function putAction()
    {
        $id = $this->getRequest()->getQuery('id');
        $imageService = $this->getImageService();
        $image = $imageService->getImage($id);
        if (!$image instanceof Media) {
            return $this->errorResponse($imageService);
        }
        $data = [];
        parse_str($this->getRequest()->getContent(), $data);
        $meta = $image->getMeta();
        if (isset($data['personToken'])) {
            $person = $this->getPerson($data['personToken']);
            if ($person) {
                $meta->setAllowLoggedOut(true);
            }
        } else {
            $viewHelperManager = $this->getServiceLocator()->get('ViewHelperManager');
            $user = $viewHelperManager->get('user');
            if ($user->isGuest()) {
                return new JsonModel(['error' => 'Not allowed']);
            }
        }
        $form = $this->getMetaForm(isset($data['wgs84Coordinates']));
        $form->bind($meta);
        $form->setData($data);
        if ($form->isValid()) {
            $imageService->setDefaultData($form->getData(FormInterface::VALUES_AS_ARRAY));
            $imageService->setDefaultMeta($meta, null, false);
            $result = $imageService->updateMeta($id, $meta);
            if ($result === false) {
                return $this->errorResponse($imageService);
            }
        } else {
            return $this->errorResponseString(ArrayUtil::multi_implode(', ', $form->getMessages()));
        }
        return new JsonModel();
    }

    private function errorResponseString($msg, $code = 400)
    {
        $this->getResponse()->setStatusCode($code);
        return new JsonModel(['error' => $msg]);
    }

    private function errorResponse(ImageService $imageService)
    {
        $result = $imageService->getServerResponse();
        $code = 500;
        $error = 'Server error!';
        if ($result instanceof Response) {
            $code = $result->getStatusCode();
            $error = $result->getBody();
        }
        $response = $this->getResponse();
        if ($response instanceof Response) {
            $response->setStatusCode($code);
        }
        return new JsonModel(['error' => $error]);
    }

    /**
     * @return \ImageApi\Options\ImageServerConfiguration
     */
    private function getOptions()
    {
        return $this->serviceLocator->get('ImageApi\Service\Configuration');
    }

    /**
     * @return \ImageApi\Form\Image
     */
    private function getImageForm()
    {
        return $this->serviceLocator->get('FormElementManager')->get('ImageApi\Form\Image');
    }

    /**
     * @return \ImageApi\Service\ImageService
     */
    private function getImageService()
    {
        if ($this->imageService === null) {
            $this->imageService = $this->getServiceLocator()->get('ImageService');
        }
        return $this->imageService;
    }

    /**
     * @return \Kotka\Service\SpecimenService
     */ 
    private function getSpecimenService()
    {
        if ($this->specimenService === null) {
            $this->specimenService = $this->getServiceLocator()->get('Kotka\Service\SpecimenService');
        }
        return $this->specimenService;
    }

    /**
     * @return \Kotka\Form\ImageUpload
     */
    private function getImagesForm()
    {
        if ($this->getRequest()->getQuery('taxonID')) {
            return $this->serviceLocator->get('FormElementManager')->get('Kotka\Form\TaxonImageUpload');
        } else {
            return $this->serviceLocator->get('FormElementManager')->get('Kotka\Form\ImageUpload');
        }
    }

    /**
     * @return \ImageApi\Form\PictureMeta
     */
    private function getMetaForm($advancedFields = false)
    {
        /** @var \ImageApi\Form\PictureMeta $form */
        $form = $this->serviceLocator->get('FormElementManager')->get('ImageApi\Form\PictureMeta');
        /** @var \User\View\Helper\User $user */
        $viewHelperManager = $this->getServiceLocator()->get('ViewHelperManager');
        $user = $viewHelperManager->get('user');
        if ($user->isAdmin() || $advancedFields) {
            $form->addAdvancedFields();
        }
        $taxonID = $this->getRequest()->getQuery('taxonID');
        if ($taxonID) {
            $form->setData(['identifications' => ['taxonIds' => [$taxonID]]]);
        } 
        return $form;
    }

    /**
     * @param $token
     * @return MAPersonInterface|boolean
     */
    private function getPerson($token) {
        /** @var \Kotka\Options\ExternalsOptions $externals */
        $externals = $this->getServiceLocator()->get('Kotka\Options\ExternalsOptions');
        $apiLaji = $externals->getApiLaji();
        $response = $this->getHttpClient()
            ->setUri($apiLaji->getUrl() . 'person/' . $token)
            ->setHeaders(['Authorization' => $apiLaji->getPass()])
            ->send();
        /** @var \TripleStore\Service\ObjectManager $om */
        $om = $this->getServiceLocator()->get('TripleStore\ObjectManager');
        $om->enableHydrator();
        if ($response->isOk()) {
            $person = json_decode($response->getBody(), true);
            if (isset($person['id'])) {
                /** @var MAPersonInterface $person */
                $person = $om->fetch($person['id']);
                $role = $person->getMARole();
                if (!is_array($role)) {
                    $role = [$role];
                }
                if (in_array('MA.taxonEditorUser', $role) || in_array('MA.taxonEditorUserDescriptionWriterOnly', $role) || in_array('MA.admin', $role)) {
                    return $person;
                }
            }
        }
        return false;
    }
}
