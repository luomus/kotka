<?php

namespace Kotka\Controller;

use Kotka\Triple\GXDataset;
use Zend\View\Model\ViewModel;

/**
 * Class DatasetsController
 *
 * This controller handles all the http datasets requests.
 *
 * @package Kotka\Controller
 */
class DatasetsController extends AbstractKotkaController
{
    /** @var \Kotka\Service\DatasetService */
    protected $documentService;

    protected $addTitle = 'Add dataset';
    protected $editTitle = 'Edit dataset';

    /**
     * Displays the list of datasets
     * Template: view/kotka/collecions/index.phtml
     *
     * @return \Zend\View\Model\ViewModel
     */
    public function indexAction()
    {
        return new ViewModel(array('datasets' => $this->getDocumentService()->getAll()));
    }

    /**
     * handels adding new dataset
     * Uses the same template as editAction
     *
     * @return \Zend\View\Model\ViewModel
     */
    public function addAction()
    {
        return parent::addAction();
    }

    /**
     * Handles the editing of the dataset
     * Template is in view/kotka/datasets/edit.phtml
     *
     * @return \Zend\View\Model\ViewModel
     */
    public function editAction()
    {
        return parent::editAction();
    }

    /**
     * Returns form for editing the datasets
     *
     * @return \Kotka\Form\WarningForm
     */
    public function getEditForm()
    {
        /** @var \Kotka\Service\FormGeneratorService $formGenerator */
        $formGenerator = $this->serviceLocator->get('Kotka\Service\FormGeneratorService');
        return $formGenerator->getForm(new GXDataset());
    }

    /**
     * Returns form for adding new dataset
     *
     * @return \Kotka\Form\WarningForm
     */
    public function getAddForm()
    {
        $form = $this->getEditForm();
        $form->setObject(new GXDataset());

        return $form;
    }

    /**
     * Return dataset service
     *
     * @return \Kotka\Service\DatasetService
     */
    protected function getDocumentService()
    {
        if ($this->documentService === null) {
            $this->documentService = $this->serviceLocator->get('Kotka\Service\DatasetService');
        }

        return $this->documentService;
    }

    protected function getEditTitle()
    {
        return $this->editTitle . ' ' . $this->document->getUri();
    }
}
