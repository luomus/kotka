<?php

namespace Kotka\Controller;

use Kotka\DataGrid\ElasticGrid;
use Kotka\ElasticExtract\Specimen;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

/**
 * Class CultureController
 *
 * This controller handles all the http sample requests.
 *
 * @package Kotka\Controller
 */
class CultureController extends AbstractActionController
{
    private static $typeMap = [
        'bac' => 'RAH',
        'uhcc' => 'RAV',
        'fbcc' => 'RAX'
    ];

    /**
     * Displays culture search page
     * Template: view/kotka/culture/index.phtml
     *
     * please note that: Adding culture is handled by specimen
     *
     * @return \Zend\View\Model\ViewModel
     */
    public function indexAction()
    {
        return $this->searchPage('');
    }

    public function bacAction()
    {
        return $this->searchPage('bac');
    }

    public function uhccAction()
    {
        return $this->searchPage('uhcc');
    }

    public function fbccAction()
    {
        return $this->searchPage('fbcc');
    }

    public function searchPage($type)
    {
        $view = new ViewModel();
        $view->setTemplate('kotka/culture/index');
        /** @var \Kotka\Form\Search $form */
        $form = $this->serviceLocator->get('FormElementManager')->get('Kotka\Form\Search');
        $view->setVariable('form', $form);

        $params = array_merge($this->params()->fromQuery(), $this->params()->fromPost());
        $params['_type'] = $type;
        /** @var \Elastic\Client\Search $adapter */
        $adapter = $this->serviceLocator->get('Elastic\Client\Search');
        $mappings = $adapter->getMappings(Specimen::getIndexName(), Specimen::getTypeName());
        $view->setVariable('mappings', $mappings);
        $form->setData($params);
        $view->setVariable('params', $params);

        return $view;
    }

    public function tableAction() {
        $view = new ViewModel();
        $view->setTerminal(true);
        $grid = new ElasticGrid();
        /** @var \Kotka\Form\Search $form */
        $form = $this->serviceLocator->get('FormElementManager')->get('Kotka\Form\Search');
        $params = $this->getParams();
        $view->setVariable('form', $form);
        $view->setVariable('params', $grid->getMeaningfulParams($params));
        $form->setData($params);

        // Check to see if any data was send as parameters or if the data is incorrect
        if (!$form->isValid()) {
            $view->setVariable('error', $form->getMessages());
            return $view;
        }

        /** @var \Elastic\Client\Search $adapter */
        $adapter = $this->serviceLocator->get('Elastic\Client\Search');
        /** @var \Kotka\DataGrid\ElasticGrid $grid */

        $grid->setServiceLocator($this->getServiceLocator());
        $fields = ['taxon', 'originalSpecimenID'];
        try {
            $grid->prepareGrid($params, $adapter, $fields);
            $grid->addSpotParameter();
            $gridView = $grid->getGrid()->getViewModel();
            $view->addChild($gridView, 'dataGrid');
            $this->Store(SearchController::SESSION_NAVIGATE_KEY, $params);
        } catch (\Exception $e) {
            $view->setVariable('error', 'Invalid query! <br>' . $this->prepareErrorMessage($e->getMessage()));
        }
        return $view;
    }

    protected function getParams() {
        $params = array_replace($this->params()->fromQuery(), $this->params()->fromPost());
        $type = isset($params['_type']) ? $params['_type']  : '';
        $type = empty($type) ? '' : ' AND namespace:"' . self::$typeMap[$type] . '"';
        if (isset($params['q']) && !empty($params['q'])) {
            $params['q'] = 'datatype: "culture"' . $type . ' AND (' . $params['q'] . ')';
        } else {
            $params['q'] = 'datatype: "culture"' . $type . '';
        }
        return $params;
    }
}
