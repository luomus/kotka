<?php

namespace Kotka\Controller;

use Kotka\Enum\Document as DocumentEnum;
use Kotka\Service\DocumentAccess;
use Kotka\Service\DocumentServiceInterface;
use Common\Service\IdService;
use Kotka\Service\SpecimenService;
use Kotka\Triple\KotkaEntityInterface;
use Kotka\Triple\MYDocument;
use Triplestore\Classes\MAPersonInterface;
use Triplestore\Classes\MYDocumentInterface;
use User\Enum\User;
use Zend\Http\Request;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

/**
 * Class SpecimensController handles all the specimens
 * @package Kotka\Controller
 */
class SpecimensController extends AbstractKotkaController
{
    private $unreliableFields = array();
    private $denyEditReason;

    /**
     * This contains an array which is used to make sure that
     * subject is empty on all these levels
     * true includes level but with empty subject value and
     * false ignores the whole value
     *
     * @var array
     */
    protected $subjectCheck = array(
        'MYGathering' => array(
            'MYUnit' => array(
                'MYIdentification' => true,
                'MYTypeSpecimen' => true,
                'MYMeasurement' => true,
                'MFSample' => array(
                    'MFSample' => true,
                    'MYMeasurement' => true
                ),
            )
        )
    );

    /** @var  \Kotka\Triple\MYDocument */
    protected $document;

    /**
     * Contains zoo or botany based on the request parameter
     * @var string
     */
    protected $group;

    /**
     * Displays the list of specimens
     * Template: view/kotka/specimens/index.phtml
     *
     * @return \Zend\View\Model\ViewModel
     */
    public function indexAction()
    {
        return new ViewModel(array('specimens' => $this->getDocumentService(false)->getAll()));
    }

    /**
     * handles adding new specimen
     * Uses the same template as editAction
     *
     * @return \Zend\View\Model\ViewModel
     */
    public function addAction()
    {
        $this->group = $this->getEvent()->getRouteMatch()->getParam('group');
        $this->getDocumentService()->setDefaultQNamePrefix();
        $view = parent::addAction();
        if ($view instanceof ViewModel) {
            $view->setVariable('group', $this->group);
        }

        return $view;
    }

    /**
     * Handles the editing of the specimen
     * Template is in view/kotka/specimens/edit.phtml
     *
     * @return \Zend\View\Model\ViewModel
     */
    public function editAction()
    {
        $this->group = $this->getEvent()->getRouteMatch()->getParam('group');
        $view = parent::editAction();
        if ($view instanceof ViewModel && $this->document instanceof MYDocumentInterface) {
            $type = $this->document->getMYDatatype();
            if (!empty($type)) {
                $this->group = str_replace('specimen', '', $type);
            }

            $user = $this->getServiceLocator()->get('user');
            $view->setVariable('group', $this->group);
            $view->setVariable('canDelete', DocumentAccess::canDelete($this->document, $user));
            /** @var Request $request */
            $request = $this->getRequest();
            if ($request->getQuery('cancelDelete')) {
                if (!$this->getDocumentService(false)->cancelDelete($this->document)) {
                    $this->flashMessenger()->addErrorMessage('<strong>Canceling deletion failed place contact <a href="mailto:kotka@luomus.fi">Kotka support!</a></strong>');
                }
                $query = $request->getQuery()->toArray();
                unset($query['cancelDelete']);
                return $this->redirect()->toRoute('https/specimens', ['action' => 'edit'], ['query' => $query]);
            }
        }

        return $view;
    }

    public function deleteAction()
    {
        $view = new JsonModel();
        /** @var Request $request */
        $request = $this->getRequest();
        $uri = $request->getQuery('uri');
        $ds = $this->getDocumentService(false);
        $document = $ds->getById($uri);
        if (!$document instanceof MYDocumentInterface) {
            $this->getResponse()->setStatusCode(404);
            $view->setVariable('error', "couldn't find item!");
            return $view;
        }
        if (!$ds->delete($document)) {
            $this->getResponse()->setStatusCode(400);
            $view->setVariable('error', "You do not have rights to delete the item!");
            return $view;
        }

        return $view;
    }

    protected function customizeView(ViewModel $view)
    {
        $pictureForm = $this->serviceLocator->get('FormElementManager')->get('ImageApi\Form\PictureMeta');
        $view->setVariable('pictureForm', $pictureForm);
        $view->setVariable('showCopyAsNew', true);
        $view->setVariable('denyEditReason', $this->denyEditReason);
    }

    /**
     * Returns form for editing specimen
     *
     * @return \Zend\Form\Form
     */
    public function getEditForm()
    {
        /** @var \Kotka\Service\FormGeneratorService $formGenerator */
        $formGenerator = $this->serviceLocator->get('Kotka\Service\FormGeneratorService');
        return $formGenerator->getForm(new MYDocument());
    }

    /**
     * Returns for for adding specimen
     *
     * @return \Zend\Form\Form
     */
    public function getAddForm()
    {
        /** @var \Kotka\Form\MYDocument $form */
        $form = $this->getEditForm();
        $document = new MYDocument();
        /** @var SpecimenService $ds */
        $ds = $this->getDocumentService($this->group);
        $document->setMYDatatype($ds->resolveDatatype($this->group));
        $form->setObject($document);
        $form->setType($this->group);

        return $form;
    }

    protected function userAllowedToEdit(KotkaEntityInterface $obj)
    {
        return  parent::userAllowedToEdit($obj);
    }

    /**
     * Sets document service
     * @param DocumentServiceInterface $documentService
     */
    public function setDocumentService(DocumentServiceInterface $documentService)
    {
        $this->documentService = $documentService;
    }

    /**
     * @param MYDocument $document
     * @param $postData
     * @param $type
     */
    protected function postValidate($document, $postData, $type)
    {
        if (count($this->unreliableFields) > 0) {
            $document->setMYUnreliableFields(trim(implode("\n", $this->unreliableFields)));
        }
        if ($type === self::ACTION_ADD) {
            $document->setSubject($this->getSubject($postData));
        }
    }

    /**
     * Gets document service
     * @param bool $setGroup
     * @return \Kotka\Service\SpecimenService
     */
    protected function getDocumentService($setGroup = true)
    {
        if ($this->documentService === null) {
            $this->documentService = $this->serviceLocator->get('Kotka\Service\SpecimenService');
        }
        if ($setGroup) {
            $group = $this->getEvent()->getRouteMatch()->getParam('group');
            $this->documentService->setCurrentType($group);
        }
        return $this->documentService;
    }

    protected function getEditTitle()
    {
        $this->group = $this->getEvent()->getRouteMatch()->getParam('group');
        return sprintf('Edit %s specimen %s', $this->group, $this->document->getUri());
    }

    protected function getAddTitle()
    {
        $this->group = $this->getEvent()->getRouteMatch()->getParam('group');
        return sprintf('Create new %s specimen', $this->group);
    }

    protected function copyEvent($postData)
    {
        $dataType = $this->document->getMYDatatype();
        if (null !== $dataType) {
            $this->routeParams['group'] = str_replace('specimen', '', $dataType);
        }
        return parent::copyEvent($postData);
    }

    protected function forwardEvent($postData)
    {
        $dataType = $this->document->getMYDatatype();
        if (null !== $dataType) {
            $this->routeParams['group'] = str_replace('specimen', '', $dataType);
        }
        return parent::forwardEvent($postData);
    }

    private function getSubject($postData)
    {
        $idGenerator = new IdService();
        if (isset($postData[DocumentEnum::FORM_NAMESPACE_ID]) &&
            !empty($postData[DocumentEnum::FORM_NAMESPACE_ID]) &&
            isset($postData[DocumentEnum::FORM_OBJECT_ID]) &&
            !empty($postData[DocumentEnum::FORM_OBJECT_ID])
        ) {
            $idGenerator->generateFromPieces($postData['MYObjectID'], $postData['MYNamespaceID'], DocumentEnum::TYPE);

            return IdService::getQName($idGenerator->getFQDN());
        }
        return null;
    }

}
