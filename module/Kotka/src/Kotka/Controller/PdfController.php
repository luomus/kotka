<?php

namespace Kotka\Controller;

use Kotka\Controller\Exception\LogicException;
use Zend\Http\Headers;
use Zend\Http\PhpEnvironment\Response;
use Zend\Mvc\Controller\AbstractActionController;

/**
 * This class handles pdfs needed in Kotka
 *
 * @package Kotka\Controller
 */
class PdfController extends AbstractActionController
{
    /** @var \Kotka\Service\PdfService */
    protected $pdfService;

    /** @var \Kotka\Service\TransactionService */
    protected $transactionService;

    /** @var \Kotka\Service\OrganizationService */
    protected $organizationService;

    /**
     * Action for insect labels
     *
     * @return Response
     */
    public function insectLabelsAction()
    {
        $transaction = $this->getTransaction();
        $pdf = $this->getPdfService()->getInsectLabelsPdf($transaction);

        return $this->PdfRespose($pdf);
    }

    /**
     * Action for transaction dispatch sheet.
     *
     * @return Response
     */
    public function dispatchSheetAction()
    {
        $transaction = $this->getTransaction();
        $organization = $this->getOrganizationService()->getById($transaction->getHRACorrespondentOrganization());
        $pdf = $this->getPdfService()->getDispatchPdf($transaction, $organization);

        return $this->PdfRespose($pdf);
    }

    /**
     * Action for transaction inquiry sheet
     *
     * @return Response
     */
    public function inquirySheetAction()
    {
        $transaction = $this->getTransaction();
        $organization = $this->getOrganizationService()->getById($transaction->getHRACorrespondentOrganization());
        $pdf = $this->getPdfService()->getInquiryPdf($transaction, $organization);

        return $this->PdfRespose($pdf);
    }

    /**
     * Action for return sheet
     *
     * @return Response
     */
    public function returnSheetAction()
    {
        $transaction = $this->getTransaction();
        $organization = $this->getOrganizationService()->getById($transaction->getHRACorrespondentOrganization());
        $owner = $this->getOrganizationService()->getById($transaction->getMZOwner());
        $pdf = $this->getPdfService()->getReturnPdf($transaction, $organization, $owner);

        return $this->PdfRespose($pdf);
    }

    /**
     * Action for receipt
     *
     * @return Response
     */
    public function receiptAction()
    {
        $transaction = $this->getTransaction();
        $organization = $this->getOrganizationService()->getById($transaction->getHRACorrespondentOrganization());
        $owner = $this->getOrganizationService()->getById($transaction->getMZOwner());
        $pdf = $this->getPdfService()->getReceiptPdf($transaction, $organization, $owner);

        return $this->PdfRespose($pdf);
    }

    /**
     * Returns the transaction
     * This uses route parameter id to signify qname
     *
     * @return \Kotka\Triple\HRATransaction
     * @throws Exception\LogicException
     */
    protected function getTransaction()
    {
        $QName = $this->getEvent()->getRouteMatch()->getParam('id');
        $transaction = $this->getTransactionService()->getById($QName);
        if ($transaction === null) {
            throw new LogicException('Could not find loan with ' . $QName);
        }

        return $transaction;
    }

    /**
     * Makes the pdf response
     *
     * @param $pdfContent
     * @return Response
     */
    protected function PdfRespose($pdfContent)
    {
        $headers = new Headers();
        $response = new Response();
        $headers->clearHeaders()
            ->addHeaderLine("Content-type: application/pdf")
            ->addHeaderLine("Content-Disposition: inline;")
            ->addHeaderLine("Content-Length", strlen($pdfContent));
        $response->setHeaders($headers);
        $response->setContent($pdfContent);

        return $response;

    }

    /**
     * Return the PdfService.
     * This service makes all the hard work compiling the pdfs etc
     *
     * @return \Kotka\Service\PdfService
     */
    protected function getPdfService()
    {
        if ($this->pdfService === null) {
            $this->pdfService = $this->serviceLocator->get('Kotka\Service\PdfService');
        }

        return $this->pdfService;
    }

    /**
     * Return the transaction service
     *
     * @return \Kotka\Service\TransactionService
     */
    protected function getTransactionService()
    {
        if ($this->transactionService === null) {
            $this->transactionService = $this->serviceLocator->get('Kotka\Service\TransactionService');
        }

        return $this->transactionService;
    }

    /**
     * Return the organization service
     *
     * @return \Kotka\Service\OrganizationService
     */
    protected function getOrganizationService()
    {
        if ($this->organizationService === null) {
            $this->organizationService = $this->serviceLocator->get('Kotka\Service\OrganizationService');
        }

        return $this->organizationService;
    }

}
