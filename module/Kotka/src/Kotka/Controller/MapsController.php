<?php

namespace Kotka\Controller;

use Zend\Http\Request;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class MapsController extends AbstractActionController
{
    public function indexAction()
    {

        /** @var Request $request */
        $request = $this->getRequest();
        $lat = $request->getQuery('latitude', null);
        $lon = $request->getQuery('longitude', null);
        $color = $request->getQuery('color', 'eee');
        $scale = (float)$request->getQuery('scale', 0.22690848204816927);
        $dot = (float)$request->getQuery('dot', 1);

        list($x, $y) = $this->latLon2Point($lat, $lon, $scale);
        $response = $this->getResponse();
        $response
            ->getHeaders()
            ->addHeaderLine('Content-Type', 'image/svg+xml');

        $view = new ViewModel(['pointX' => $x, 'pointY' => $y, 'scale' => $scale, 'color' => $color, 'dot' => $dot]);
        $view->setTerminal(true);

        return $view;
    }

    /**
     * Convert coordinates to points on the map using Millers projection
     *
     * @param $lat
     * @param $lng
     * @return array
     */
    private function latLon2Point($lat, $lng, $scale)
    {
        if ($lat === null || $lng === null) {
            return [null, null];
        }
        $bbox = [
            ["y" => -12671671.123330014, "x" => -20004297.151525836],
            ["y" => 6930392.025135122, "x" => 20026572.39474939]
        ];
        $centralMeridian = 11.5;
        $radius = 6381372;
        $pi = M_PI;
        $radDeg = $pi / 180;
        $width = 900;
        $height = 440;

        $lat = (float)$lat;
        $lng = (float)$lng;

        if ($lng < (-180 + $centralMeridian)) {
            $lng += 360;
        }
        $x = $radius * ($lng - $centralMeridian) * $radDeg;
        $y = -$radius * log(tan((45 + 0.4 * $lat) * $radDeg)) / 0.8;

        $x = ($x - $bbox[0]['x']) / ($bbox[1]['x'] - $bbox[0]['x']) * $width * $scale;
        $y = ($y - $bbox[0]['y']) / ($bbox[1]['y'] - $bbox[0]['y']) * $height * $scale;

        return [$x, $y];
    }
}
