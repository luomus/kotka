<?php

namespace Kotka\Controller\Exception;

/**
 * Exception that represents error in the program logic.
 * This kind of exceptions should directly lead to a fix in the code.
 * @package Kotka\Controller\Exception
 */
class LogicException extends \LogicException
{
}
