<?php

namespace Kotka\Controller\Exception;

/**
 * Runtime exceptions represent problems that are the result of a programming problem
 * @package Kotka\Controller\Exception
 */
class RuntimeException extends \RuntimeException
{
}
