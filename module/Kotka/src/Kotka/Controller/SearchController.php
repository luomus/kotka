<?php

namespace Kotka\Controller;

use Elastic\Extract\ExtractInterface;
use Kotka\DataGrid\ElasticGrid;
use Kotka\DataGrid\Type\BranchURI;
use Kotka\DataGrid\Type\URI;
use Kotka\ElasticExtract\Branch;
use Kotka\ElasticExtract\Sample;
use Kotka\ElasticExtract\Specimen;
use Common\Service\IdService;
use Kotka\Job\ExportExcel;
use Kotka\Stdlib\Hydrator\OntologyHydrator;
use Kotka\Stdlib\Hydrator\Strategy\DateTimeStrategy;
use Kotka\Triple\MYDocument;
use Triplestore\Stdlib\Hydrator\DateTime;
use Zend\Escaper\Escaper;
use Zend\Http\Headers;
use Zend\Http\PhpEnvironment\Response;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use ZfcDatagrid\Renderer\PHPExcel\Renderer;

/**
 * This class handles the specimen search part
 * and the labels search can produce.
 * @package Kotka\Controller
 */
class SearchController extends AbstractActionController
{
    const SESSION_NAVIGATE_KEY = 'search-navigation';
    const SESSION_BRANCH_NAVIGATE_KEY = 'search-branch-navigation';
    const SESSION_SAMPLE_NAVIGATE_KEY = 'search-sample-navigation';

    const MAX_EXPORT = 10000;
    const MAX_EXPORT_EXCEL = 10000;
    const MAX_EXPORT_PDF = 500;

    /** @var \Kotka\Service\SpecimenService */
    protected $specimenService;

    /**
     * Handles the search form and it's results
     * This can produce normal html view or pdf view if one is wanted
     *
     * @param ExtractInterface|null $extract
     * @param string $queryKey
     * @param null|ViewModel\ $view
     * @return JsonModel|ViewModel|\ZfcDatagrid\Ambigous|null
     * @throws \Exception
     */
    public function indexAction(
        ExtractInterface $extract = null,
        $queryKey = self::SESSION_NAVIGATE_KEY,
        $view = null
    )
    {
        if (!isset($view)) {
            $view = new ViewModel();
            $view->setVariable('title', 'Search specimens');
        }
        ;
        /** @var \Kotka\Form\Search $form */
        $form = $this->serviceLocator->get('FormElementManager')->get('Kotka\Form\Search');
        $view->setVariable('form', $form);

        $params = array_merge($this->params()->fromQuery(), $this->params()->fromPost());
        if (isset($params['search'])) {
            $params['q'] = '"' . $params['search'] . '"';
        }
        if (count(array_diff_key($params, ['export' => '', 'link' => ''])) == 0) {
            $session = $this->Store()->fetch($queryKey);
            if (is_array($session)) {
                $params = array_merge($session, $params);
            }
        }
        if ($extract === null) {
            $extract = new Specimen();
        }
        /** @var \Elastic\Client\Search $adapter */
        $adapter = $this->serviceLocator->get('Elastic\Client\Search');
        $mappings = $adapter->getMappings($extract::getIndexName(), $extract::getTypeName());
        $view->setVariable('mappings', $mappings);
        $form->setData($params);
        $view->setVariable('params', $params);

        // Check to see if any data was send as parameters or if the data is incorrect
        if (!isset($params['export']) || empty($params['export']) || !$form->isValid()) {
            return $view;
        }
        if (in_array($params['export'], ['excel', 'hertta-excel'])) {
            $view = new JsonModel();
        }
        /** @var \Kotka\DataGrid\ElasticGrid $grid */
        $grid = new ElasticGrid($extract);
        $grid->setServiceLocator($this->getServiceLocator());
        $fields = isset($params['fields']) ? $params['fields'] : [];
        $grid->prepareGrid($params, $adapter, $fields);
        $documents = $grid->getDocument(in_array($params['export'], ['sample-pdf']) ? ['documentQName', 'sampleDocumentQName'] : ['documentQName']);
        $count = $documents->count();
        $results = [];
        if ($count > self::MAX_EXPORT) {
            $view->setVariable('error', sprintf("Too many results found: %d (max: %s).", $count, self::MAX_EXPORT));

            return $view;
        }

        $sampleMap = [];
        foreach ($documents as $result) {
            $uri = IdService::getQName($result['documentQName'], true);
            if (isset($result['sampleDocumentQName'])) {
                if (!isset($sampleMap[$uri])) {
                    $sampleMap[$uri] = [];
                }
                $sampleMap[$uri][] = IdService::getQName($result['sampleDocumentQName'], true);
            }
            $results[$uri] = $uri;
        }

        switch ($params['export']) {
            case 'pdf':
                return $this->pdfExport($view, $results, $params);
            case 'sample-pdf':
                return $this->samplePdfExport($view, $results, $params, $sampleMap);
            case 'excel':
                return $this->excelExport($view, $params);
            case 'simple-excel':
                return $this->simpleExcelExport($view, $grid);
            case 'hertta-excel':
                return $this->excelExport($view, $params, 'hertta');
            case 'page':
                return $this->exportPage($view, $results, $params);
            default:
                $view->setVariable('error', sprintf("Unable to export to type: '{$params['export']}'"));
                return $view;

        }
    }

    public function countAction() {
        $params = array_merge($this->params()->fromQuery(), $this->params()->fromPost());
        /** @var \Elastic\Client\Search $adapter */
        $adapter = $this->serviceLocator->get('Elastic\Client\Search');
        $extract = new Specimen();
        $grid = new ElasticGrid($extract);
        $grid->setServiceLocator($this->getServiceLocator());
        $grid->prepareGrid(['q' => $params['search']], $adapter, ['documentQName']);
        $documents = $grid->getDocument(['documentQName']);
        return new JsonModel(['count' => $documents->count()]);
    }

    public function branchAction() {
        return $this->indexAction(new Branch(), self::SESSION_BRANCH_NAVIGATE_KEY);
    }

    public function branchTableAction() {
        $uri = new BranchURI();
        $view = $this->tableAction(
            new Branch(),
            true,
            self::SESSION_BRANCH_NAVIGATE_KEY,
            'https/search_branch',
            $uri
        );
        $view->setTemplate('kotka/search/table');
        return $view;
    }

    public function sampleAction() {
        return $this->indexAction(new Sample(), self::SESSION_SAMPLE_NAVIGATE_KEY);
    }

    public function sampleTableAction() {
        $view = $this->tableAction(
            new Sample(),
            true,
            self::SESSION_SAMPLE_NAVIGATE_KEY,
            'https/search_sample'
        );
        $view->setTemplate('kotka/search/table');
        return $view;
    }

    public function tableAction(
        ExtractInterface $extract = null,
        $export = true,
        $queryKey = self::SESSION_NAVIGATE_KEY,
        $url = 'https/search_specimens',
        $urlCol = null
    )
    {
        if ($extract === null) {
            $extract = new Specimen();
        }
        $view = new ViewModel();
        $view->setTerminal(true);
        $grid = new ElasticGrid($extract);
        /** @var \Kotka\Form\Search $form */
        $form = $this->serviceLocator->get('FormElementManager')->get('Kotka\Form\Search');
        $params = array_replace($this->params()->fromQuery(), $this->params()->fromPost());
        $view->setVariable('form', $form);
        $view->setVariable('base', $url);
        $view->setVariable('params', $grid->getMeaningfulParams($params));
        $form->setData($params);

        // Check to see if any data was send as parameters or if the data is incorrect
        if (!$form->isValid()) {
            $view->setVariable('error', $form->getMessages());
            return $view;
        }

        /** @var \Elastic\Client\Search $adapter */
        $adapter = $this->serviceLocator->get('Elastic\Client\Search');
        /** @var \Kotka\DataGrid\ElasticGrid $grid */

        $grid->setServiceLocator($this->getServiceLocator());
        $fields = isset($params['fields']) ? $params['fields'] : [];
        try {
            if ($urlCol) {
                $grid->setUrlColumn($urlCol);
            }
            $grid->prepareGrid($params, $adapter, $fields);
            if (method_exists($urlCol, 'addSpot') || !$urlCol) {
                $grid->addSpotParameter($this->getSpotTypeParam($extract));
            }
            $gridView = $grid->getGrid()->getViewModel();
            $gridView->setVariable('export', $export ? self::MAX_EXPORT : false);
            $view->addChild($gridView, 'dataGrid');
            $this->Store($queryKey, $params);
        } catch (\Exception $e) {
            $view->setVariable('error', 'Invalid query! <br>' . $this->prepareErrorMessage($e->getMessage()));
        }
        return $view;
    }

    private function getSpotTypeParam(ExtractInterface $extract) {
        if ($extract instanceof Sample) {
            return 'sample';
        } else if ($extract instanceof Branch) {
            return 'branch';
        }
        return 'document';
    }

    private function prepareErrorMessage($error) {
        return str_replace(['query_parsing_exception: '], '', $error);
    }

    public function builderAction()
    {
        $view = new ViewModel();
        $view->setTerminal(true);
        /** @var \Kotka\Form\SearchSimple $form */
        $form = $this->getServiceLocator()->get('FormElementManager')->get('Kotka\Form\SearchSimple');
        $view->setVariable('form', $form);

        return $view;
    }

    public function mapAction()
    {
        $view = new ViewModel();
        $view->setTerminal(true);

        return $view;
    }

    private function exportPage($view, $documents, $params)
    {
        $count = count($documents);
        if ($count == 1) {
            $uri = array_values($documents)[0];
            return $this->redirect()->toRoute('view', [], ['query' => ['uri' => $uri]]);
        } elseif ($count == 0) {
            $escaper = new Escaper('utf-8');
            $identifier = isset($params['identifier']) ? $params['identifier'] : '';
            $identifier = $escaper->escapeHtml($identifier);
            $this->flashMessenger()->addErrorMessage('Specimen ' . $identifier . ' not found.');
            return $this->redirect()->toRoute('https/home');
        } else {
            return $view;
        }
    }

    private function getSpecimensAsArray($documents)
    {
        /** @var \Kotka\Service\SpecimenService $specimenService */
        $specimenService = $this->serviceLocator->get('Kotka\Service\SpecimenService');
        $specimens = $specimenService->getById($documents);
        if (!is_array($specimens)) {
            $specimens = [];
        }
        $hydrator = new OntologyHydrator();
        $dateStrategy = new DateTimeStrategy();
        $dateStrategy->setFormat(DateTime::FORMAT);
        $hydrator->addStrategy('MZDateEdited', $dateStrategy);
        $specimenList = array();
        foreach ($specimens as $specimen) {
            if ($specimen instanceof MYDocument) {
                if ($specimen->getMZScheduledForDeletion() === true) {
                    continue;
                }
                $specimenList[] = $hydrator->extract($specimen);
            }
        }
        return $specimenList;
    }

    private function excelExport(ViewModel $view, $params, $type = '')
    {
        /** @var \Kotka\Service\ExcelService $excelService */
        /** @var \Kotka\Triple\MAPerson $user */
        /** @var \SlmQueue\Queue\AbstractQueue $queue */
        $user = $this->serviceLocator->get('User');
        $queue = $this->serviceLocator->get('SlmQueue\Queue\QueuePluginManager')->get('slow');

        $file = ExportExcel::getUserExportLog($user->getSubject());
        if (file_exists($file)) {
            unlink($file);
        }

        $job = new ExportExcel();
        $job->setContent(['user' => $user->getSubject(), 'export-params' => $params, 'type' => $type]);
        $queue->push($job);

        $view->setVariable('excelExported', true);

        return $view;
    }

    private function simpleExcelExport(ViewModel $view, ElasticGrid $elasticGrid)
    {
        $grid = $elasticGrid->getGrid('excel', false);
        /** @var Renderer $renderer */
        $renderer = $grid->getRenderer();
        $types = $renderer->getAllowedColumnTypes();
        URI::$onlyText = true;
        $types[] = 'Kotka\DataGrid\Type\GenericType';
        $types[] = 'Kotka\DataGrid\Type\GeoPointType';
        $types[] = 'Kotka\DataGrid\Type\URI';
        $renderer->setAllowedColumnTypes($types);

        return $grid->getResponse();
    }

    private function pdfExport(ViewModel $view, array $results, array $params)
    {
        /** @var \Kotka\Service\SpecimenService $specimenService */
        /** @var \Kotka\Service\LabelService $labelService */
        /** @var \Kotka\Service\PdfService $pdfService */
        $specimenService = $this->getServiceLocator()->get('Kotka\Service\SpecimenService');
        $labelService = $this->getServiceLocator()->get('Kotka\Service\LabelService');
        $pdfService = $this->getServiceLocator()->get('Kotka\Service\PdfService');
        $specimens = $specimenService->getById($results);
        if (!is_array($specimens)) {
            $specimens = [];
        }
        $specimens = array_merge($results, $specimens);
        $specimens = array_filter($specimens, array($this, 'removeMissing'));
        $labels = $labelService->extractLabelData($specimens);
        if (isset($params['format']) && $params['format'] === 'labelDesigner') {
            $labelDesignerView = new ViewModel();
            $labelDesignerView->setTemplate('kotka/tools/generic-label');
            $labelDesignerView->setVariable('labels', $labels);
            return $labelDesignerView;
        }
        $html = $labelService->getLabelHtml($labels, $params);
        // echo $html; exit();
        if ($html === false || $html === '' || count($labels) == 0) {
            $view->setVariable('error', "No identification data found from the given set.");

            return $view;
        }

        $headers = new Headers();
        $headers->addHeaderLine('Content-type: application/pdf');
        $headers->addHeaderLine('Content-Disposition: attachment; filename="search.pdf"');
        $response = $this->getResponse();
        $response->setHeaders($headers);
        $response->setContent($pdfService->htmlToPdfApi($html, $params['format']));

        return $response;
    }
    
    private function samplePdfExport(ViewModel $view, array $results, array $params, $sampleMap) {
        /** @var \Kotka\Service\SpecimenService $specimenService */
        /** @var \Kotka\Service\LabelService $labelService */
        /** @var \Kotka\Service\PdfService $pdfService */
        $specimenService = $this->getServiceLocator()->get('Kotka\Service\SpecimenService');
        $labelService = $this->getServiceLocator()->get('Kotka\Service\LabelService');
        $pdfService = $this->getServiceLocator()->get('Kotka\Service\PdfService');
        $specimens = $specimenService->getById($results);
        if (!is_array($specimens)) {
            $specimens = [];
        }
        $specimens = array_merge($results, $specimens);
        $specimens = array_filter($specimens, array($this, 'removeMissing'));
        $labels = $labelService->extractLabelData($specimens, $sampleMap);
        if (isset($params['format']) && $params['format'] === 'labelDesigner') {
          $labelDesignerView = new ViewModel();
          $labelDesignerView->setTemplate('kotka/tools/generic-label');
          $labelDesignerView->setVariable('labels', $labels);
          return $labelDesignerView;
        }
        $html = $labelService->getLabelHtml($labels, $params);
        //echo $html; exit();
        if ($html === false || $html === '' || count($labels) == 0) {
            $view->setVariable('error', "No identification data found from the given set.");

            return $view;
        }

        $headers = new Headers();
        $headers->addHeaderLine('Content-type: application/pdf');
        $response = $this->getResponse();
        $response->setHeaders($headers);
        $response->setContent($pdfService->htmlToPdfApi($html, $params['format']));

        return $response;
    }

    public function removeMissing($var)
    {
        return $var instanceof MYDocument;
    }

    /**
     * Return Specimen service
     * @return \Kotka\Service\SpecimenService
     */
    protected function getSpecimenService()
    {
        if ($this->specimenService === null) {
            $this->specimenService = $this->serviceLocator->get('Kotka\Service\SpecimenService');
        }

        return $this->specimenService;
    }
}
