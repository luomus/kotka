<?php

namespace Kotka\Controller\Plugin;

use Exception;
use Zend\Form\Form;
use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * This return wanted form for the controller.
 * In the future this can be extended to include creating of the form object
 * based ob the instructions given by database or some api.
 * @package Kotka\Controller\Plugin
 */
class getForm extends AbstractPlugin implements ServiceLocatorAwareInterface
{
    const DOCUMENT_BASE = 'Kotka\Document';
    const TEMPLATE_BASE = 'Kotka\Form\Template';

    protected $serviceLocator;

    /**
     * Fetches the from.
     * At the moment this is only used in the specimen controller.
     *
     * @param $name
     * @return Form
     * @throws \Exception
     */
    public function __invoke($name)
    {
        $formClass = "Kotka\\Form\\Specimen";
        $form = $this->getServiceLocator()->getServiceLocator()->get('FormElementManager')->get($formClass);
        if (!$form instanceof Form) {
            throw new Exception('Could not create form in getForm controller plugin');
        }

        return $form;
    }

    /**
     * Sets the service locator
     * @param ServiceLocatorInterface $sl
     */
    public function setServiceLocator(ServiceLocatorInterface $sl)
    {
        $this->serviceLocator = $sl;
    }

    /**
     * Return thr service locator
     * @return ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

}
