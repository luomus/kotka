<?php
namespace Kotka\Controller\Plugin;

/**
 * This stores data to the session for only until it's fetch from it
 * @package Kotka\Controller\Plugin
 */
class StoreOverRequest extends Store
{
    /**
     * Initializes the storage
     */
    public function __construct()
    {
        parent::__construct('Kotka_SoRP');
    }

    /**
     * Fetches the value of the given key and empties it
     * @param $key
     * @return mixed
     */
    public function fetch($key)
    {
        $data = $this->session->$key;
        $this->session->$key = null;

        return $data;
    }

}
