<?php
namespace Kotka\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\Session\Container;

/**
 * Stores data to the session
 * @package Kotka\Controller\Plugin
 */
class Store extends AbstractPlugin
{
    /** @var \Zend\Session\Container */
    protected $session;

    /**
     * Constructs the store with the namespace given.
     *
     * @param string $ns optional
     */
    public function __construct($ns = 'Kotka_Session_Store')
    {
        $this->session = new Container($ns);
    }

    public function __invoke($key = null, $value = null)
    {
        if ($value === null || $key === null) {
            return $this;
        }
        $this->session->$key = $value;

        return $this;
    }

    /**
     * Stores the value with the key into the session
     *
     * @param $key
     * @param $value
     * @return $this
     */
    public function store($key, $value)
    {
        $this->session->$key = $value;

        return $this;
    }

    /**
     * Return the value from session
     * @param $key
     * @return mixed
     */
    public function fetch($key)
    {
        return $this->session->$key;
    }

}
