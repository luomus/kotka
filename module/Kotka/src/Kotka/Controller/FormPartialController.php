<?php

namespace Kotka\Controller;

use Kotka\Triple\HRATransaction;
use Kotka\Triple\MYDocument;
use Kotka\View\Helper\Id;
use Zend\Form\Element\Collection;
use Zend\Http\Request;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class FormPartialController extends AbstractActionController
{

    protected $elements = array(
        'MYGathering' => 'kotka/specimens/partial/gathering',
        'MYUnit' => 'kotka/specimens/partial/unit',
        'MYIdentification' => 'kotka/specimens/partial/identification',
        'MYTypeSpecimen' => 'kotka/specimens/partial/types',
        'MFSample' => 'kotka/specimens/partial/sample',
        'MFPreparation' => 'kotka/specimens/partial/preparation',
        'images' => 'kotka/specimens/partial/image',
    );

    public function indexAction()
    {
        $view = new ViewModel();
        $view->setTerminal(true);
        $target = $this->getEvent()->getRouteMatch()->getParam('element');
        /** @var \Zend\Http\PhpEnvironment\Response $response */
        $response = $this->getResponse();
        if (!isset($this->elements[$target])) {
            $response->setStatusCode(404);
            return $view;
        }
        /** @var \Kotka\Form\MYDocument $form */
        /** @var \Kotka\Service\FormGeneratorService $formGenerator */
        $formGenerator = $this->serviceLocator->get('Kotka\Service\FormGeneratorService');
        /** @var Request $request */
        $request = $this->getRequest();
        $type = $request->getQuery('type');
        $name = $request->getQuery('name');
        $form = $formGenerator->getForm($this->getTargetForm($name));
        $data = array();
        parse_str($name . '[subject]=', $data);
        $form->setData($data, false);
        /** @var Collection $element */
        $element = $this->getTargetElement($form, $target, $data);
        $element->setShouldCreateTemplate(true);
        if ($element === null) {
            $response->setStatusCode(404);
            return $view;
        }
        $id = $this->getIdFromName($name);
        /** @var Collection $element */
        $template = $element->getTemplateElement();
        /** @var \Zend\View\Resolver\TemplateMapResolver $resolver */
        $resolver = $this->getEvent()
            ->getApplication()
            ->getServiceManager()
            ->get('Zend\View\Resolver\TemplateMapResolver');

        $partial = $this->elements[$target];
        $candidate = $this->elements[$target] . '_' . $type;
        if ($type !== null && $resolver->resolve($candidate) !== false) {
            $partial = $candidate;
        }
        $form->prepare();
        $view->setVariable('partial', $partial);
        $view->setVariable('template', $template);
        $view->setVariable('id', $id);
        $view->setVariable('group', $type);
        $header = $response->getHeaders();
        $header->addHeaderLine('charset', 'utf-8');

        return $view;
    }

    private function getTargetForm($name) {
        return $name === 'HRATransaction' ? new HRATransaction() : new MYDocument();
    }

    private function getTargetElement($form, $target, $path)
    {
        if ($form->has($target)) {
            return $form->get($target);
        }
        $key = key($path);
        $last = key(end($path));
        if (isset($path[$key][$last]['subject'])) {
            $form = $form->get($key);
            $form = $form->get($last);
            if ($form->has($target)) {
                return $form->get($target);
            }
            return null;
        }
        if ($form->has($key)) {
            $form = $form->get($key);
        }
        if ($form->has($last)) {
            $form = $form->get($last);
        }
        if (!is_array($path[$key][$last])) {
            return null;
        }

        return $this->getTargetElement($form, $target, $path[$key][$last]);
    }

    private function getIdFromName($name)
    {
        $idHelper = new Id();
        return $idHelper($name);
    }

} 