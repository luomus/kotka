<?php

namespace Kotka\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

/**
 * Class IndexController
 * This is responsible for the index page in the Kotka
 *
 * @package Kotka\Controller
 */
class IndexController extends AbstractActionController
{
    /**
     * This is the home page in Kotka
     * Template is in view/kotka/index/index.phtml
     *
     * @return ViewModel
     */
    public function indexAction()
    {
        /** @var \Kotka\Service\UserStatsService $stats */
        $stats = $this->getServiceLocator()->get('Kotka\Service\UserStatsService');
        $data = $stats->getFrontPageStats();
        /** @var \Kotka\Service\NewsService $newsService */
        $newsService = $this->getServiceLocator()->get('Kotka\Service\NewsService');
        $data['news'] = $newsService->getNewsAsHtml();

        return new ViewModel($data);
    }

    public function httpAction()
    {
        return $this->redirect()->toRoute('https/home');
    }

}
