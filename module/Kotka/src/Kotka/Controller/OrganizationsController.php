<?php

namespace Kotka\Controller;

use Kotka\Triple\MOSOrganization;
use Zend\View\Model\ViewModel;

/**
 * This class handles all the http requests associated with organizations
 *
 *
 * @package Kotka\Controller
 */
class OrganizationsController extends AbstractKotkaController
{
    protected $editTitle = 'Edit organization ';
    protected $addTitle = 'Add organization ';

    /**
     * This is showing the list with all the organizations in it.
     * Template is in view/kotka/organizations/index.phtml
     *
     * @return ViewModel
     */
    public function indexAction()
    {
        $fields = array(
            'MOS.organizationLevel1',
            'MOS.organizationLevel2',
            'MOS.organizationLevel3',
            'MOS.organizationLevel4',
            'MOS.Abbreviation',
            'MOS.postalCode',
            'MOS.abbreviation',
            'MOS.hidden',
            'MOS.dateOrdersDue',
            'MOS.datasetID'
        );
        return new ViewModel(array('organizations' => $this->getDocumentService()->getAll($fields)));
    }

    /**
     * Handles adding new organization
     *
     * @return ViewModel
     */
    public function addAction()
    {
        return parent::addAction();
    }

    /**
     * Handles editing existing organization
     *
     * @return ViewModel
     */
    public function editAction()
    {
        return parent::editAction();
    }

    /**
     * Returns form for editing organization
     *
     * @return \Kotka\Form\MOSOrganization
     */
    public function getEditForm()
    {
        /** @var \Kotka\Service\FormGeneratorService $formGenerator */
        $formGenerator = $this->serviceLocator->get('Kotka\Service\FormGeneratorService');
        return $formGenerator->getForm(new MOSOrganization());
    }

    /**
     * Returns form for adding the organization
     *
     * @return \Kotka\Form\MOSOrganization
     */
    public function getAddForm()
    {
        $form = $this->getEditForm();
        $form->setObject(new MOSOrganization());

        return $form;
    }

    /**
     * Return service for handling organization documents
     *
     * @return \Kotka\Service\OrganizationService
     */
    protected function getDocumentService()
    {
        if ($this->documentService === null) {
            $this->documentService = $this->serviceLocator->get('Kotka\Service\OrganizationService');
        }

        return $this->documentService;
    }

    protected function getEditTitle()
    {
        return $this->editTitle . ' ' . $this->document->getUri();
    }

    protected function preValidate($document, &$data, $type)
    {
        // Need to remove DateOrdersDue since it's data type is DateTime (and empty cannot be converter do datetime).
        if (isset($data['MOSDateOrdersDue']) && empty($data['MOSDateOrdersDue'])) {
            unset($data['MOSDateOrdersDue']);
        }
    }
}
