<?php

namespace Kotka\Controller;

use Kotka\Form\WarningForm;
use Kotka\Service\DocumentServiceInterface;
use Common\Service\IdService;
use Kotka\Triple\KotkaEntityInterface;
use Zend\Form\Form;
use Zend\Http\Request;
use Zend\Log\LoggerInterface;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

/**
 * Class AbstractKotkaController is the common controller
 * for all the controllers that are handling documents.
 *
 * Service used needs to implement @see \Kotka\Service\DocumentServiceInterface for this to work
 * as expected and the actual document needs to implement @see \Kotka\Document\DocumentInterface
 *
 * @package Kotka\Controller
 */
abstract class AbstractKotkaController extends AbstractActionController
{
    /** Add action name for route */
    const ACTION_ADD = 'add';
    /** Edit action name for route */
    const ACTION_EDIT = 'edit';

    protected $subjectCheck = array();
    protected $editTitle = 'Edit';
    protected $addTitle = 'Add';
    /** @var  \Kotka\Service\DocumentServiceInterface */
    protected $documentService;
    /** @var \Kotka\Triple\KotkaEntityInterface */
    protected $document;
    /** @var \Kotka\Service\ValidationService */
    protected $validationService;
    protected $routeParams = array();
    protected $routeName;
    protected $prePopulate = array();
    protected $successCarryMsg = "<strong>Record successfully saved into database.</strong> Below is a form for new record, prefilled with carried data.";
    protected $successClearMsg = "<strong>Record successfully saved into database.</strong> Below is an empty form.";
    protected $successKeepMsg = "<strong>Record successfully saved into database.</strong> Below is the saved record: you can view or edit it here.";

    /**
     * Initializes the controller
     */
    public function init()
    {
        $this->routeName = $this->getEvent()->getRouteMatch()->getMatchedRouteName();
    }

    /**
     * Acton called when adding document
     *
     * @return ViewModel
     */
    public function addAction()
    {
        $view = new ViewModel();
        $form = $this->getAddForm();
        $this->getWarnings($form);
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = array_replace_recursive($request->getPost()->toArray(), $request->getFiles()->toArray());
            $this->preValidate($this->document, $postData, self::ACTION_ADD);
            $form->setData($postData);
            if (!isset($postData['status']) || $postData['status'] != 'ok') {
                $form->setMessages(array('subject' => array("Form wasn't complete. Please try again little later")));
            } else {
                if ($form->isValid()) {
                    $this->document = $form->getData();
                    $this->postValidate($this->document, $postData, self::ACTION_ADD);
                    if ($this->getDocumentService()->create($this->document)) {
                        $this->storeWarnings($form);
                        return $this->forwardEvent($postData);
                    }
                    /** @var LoggerInterface $logger */
                    $logger = $this->getServiceLocator()->get('logger');
                    $logger->info('Failed post data: ' . json_encode($postData));
                    $form->setMessages(array('subject' => array("Saving failed. Please try again little later")));
                } else {
                    $this->postValidateError($this->document, $postData, self::ACTION_ADD);
                    $form->setData($postData);
                }
            }
        } else {
            $populate = $this->StoreOverRequest()->fetch('postdata');
            if ($this->prePopulate !== null) {
                $form->setData($this->prePopulate);
            }
            if ($populate !== null) {
                $form->setData($populate);
            }
        }
        $view->setVariable('title', $this->getAddTitle());
        $view->setVariable('form', $form);
        $view->setVariable('allowEdit', true);
        $this->customizeView($view);

        return $view;
    }

    /**
     * Action called when editing document
     *
     * @return ViewModel
     * @throws Exception\LogicException
     */
    public function editAction()
    {
        $view = new ViewModel();
        /** @var Request $request */
        $request = $this->getRequest();
        $uri = $request->getQuery('uri');
        $history = $request->getQuery('history', false);

        $this->document = $this->getDocumentService()->getById($uri);
        if ($this->document === null) {
            $view->setVariable('title', $this->editTitle);
            $view->setVariable('Uri', $uri);
            $view->setTemplate('kotka/common/notfound');

            return $view;
        }
        $form = $this->getEditForm($this->document);
        $allowEdit = $this->userAllowedToEdit($this->document);
        $this->getWarnings($form);

        if ($history &&
            $form->has('history') &&
            $allowEdit
        ) {
            $form->get('history')->setValueOptions(['subject' => $this->document->getSubject()]);
        } else {
            $history = false;
        }

        if ($request->isPost()) {
            $postData = array_replace_recursive($request->getPost()->toArray(), $request->getFiles()->toArray());
            if (isset($postData['subject']) && IdService::getQName($uri) !== IdService::getQName($postData['subject'])) {
                throw new \Exception(sprintf(
                    "Posted subject didn't match the one in the url! Post had %s when uri had %s",
                    $postData['subject'],
                    $uri
                ));
            }
            $this->preValidate($this->document, $postData, self::ACTION_EDIT);
            $form->bind($this->document);
            $form->setData($postData);
            if (!isset($postData['status']) || $postData['status'] != 'ok') {
                $form->setMessages(array('subject' => array("Form wasn't complete. Please try again little later")));
            } else {
                if (isset($postData['kotka_copy'])) {
                    return $this->copyEvent($postData);
                }
                if ($form->isValid()) {
                    if (!$this->userAllowedToEdit($this->document)) {
                        throw new Exception\LogicException('You do not have rights to edit this');
                    }
                    $this->postValidate($this->document, $postData, self::ACTION_EDIT);
                    if ($this->getDocumentService()->update($this->document)) {
                        $this->storeWarnings($form);
                        return $this->forwardEvent($postData);
                    }
                    /** @var LoggerInterface $logger */
                    $logger = $this->getServiceLocator()->get('logger');
                    $logger->info('Failed post data: ' . json_encode($postData));
                    $form->setMessages(array('subject' => array("Saving failed. Please try again little later")));
                } else {
                    $this->postValidateError($this->document, $postData, self::ACTION_EDIT);
                    $form->setData($postData);
                }
            }
        } else {
            $populate = $this->StoreOverRequest()->fetch('postdata');
            if ($history) {
                $dateValidator = new \Zend\Validator\Date(array('format' => 'YmdHis'));
                if ($dateValidator->isValid($history)) {
                    $om = $this->getServiceLocator()->get('Triplestore\ObjectManager');
                    $class = get_class($this->document);
                    $om->className($class);
                    $class = explode('\\', $class);
                    $document = $om->fetchFromHistory($this->document->getSubject(), $history, array_pop($class));
                    if ($document == null) {
                        $class = $this->documentService->getClassName();
                        $document = new $class;
                    }
                    $this->document = $document;
                    $populate['history'] = $history;
                }
            }
            $form->bind($this->document);

            if ($populate !== null) {
                $form->setData($populate, false);
            }
        }
        $view->setVariable('title', $this->getEditTitle());
        $view->setVariable('form', $form);
        $view->setVariable('history', $history);
        $view->setVariable('allowEdit', $allowEdit);
        $view->setVariable('document', $this->document);
        $view->setVariable('params', $request->getQuery()->toArray());
        $this->customizeView($view);

        return $view;
    }

    private function storeWarnings(Form $form)
    {
        if (!$form instanceof WarningForm) {
            return;
        }
        $messages = $form->getWarningMessages();
        $this->StoreOverRequest()->store('warnings', $messages);
    }

    private function getWarnings(Form $form)
    {
        if (!$form instanceof WarningForm) {
            return;
        }
        $messages = $this->StoreOverRequest()->fetch('warnings');
        if ($messages !== null) {
            $form->setWarningMessages($messages);
        }
    }

    protected function preValidate($document, &$data, $type)
    {

    }

    protected function postValidate($document, $data, $type)
    {

    }

    protected function postValidateError($document, &$data, $type) {

    }

    /**
     * Stores the post data and gives the http response object
     *
     * @param $toAction
     * @param $postData
     * @return \Zend\Http\Response
     */
    protected function redirectWithForm($toAction, $postData)
    {
        $this->StoreOverRequest()->store('postdata', $postData);
        $options = array();
        if (isset($postData['datatype']) && strpos($postData['datatype'], 'specimen') !== false) {
            $this->routeParams['group'] = str_replace('specimen', '', $postData['datatype']);
        }
        if ($toAction == self::ACTION_EDIT) {
            /** @var Request $request */
            $query = array('uri' => IdService::getUri($postData['subject']));
            $request = $this->getRequest();
            $page = $request->getQuery('page', null);
            $spot = $request->getQuery('spot', null);
            $type = $request->getQuery('type', 'document');
            if ($page !== null && $spot !== null) {
                $query['page'] = $page;
                $query['spot'] = $spot;
                $query['type'] = $type;
            }
            $options = array('query' => $query);
        }
        $this->routeParams['action'] = $toAction;

        return $this->redirect()->toRoute($this->routeName, $this->routeParams, $options);
    }

    /**
     * Checks if the user has right to edit the data
     * This uses Ltkm user authentication controller plugin to get the current user groups
     *
     * @param  KotkaEntityInterface $obj
     * @return bool
     */
    protected function userAllowedToEdit(KotkaEntityInterface $obj)
    {
        $user = $this->serviceLocator->get('user');
        $organizations = $user->getMAOrganisation();
        if (!is_array($organizations)) {
            $organizations = array();
        }
        $organization = $obj->getMZOwner();
        return array_key_exists($organization, $organizations) || empty($organization);
    }

    /**
     * This gives a change to controller that is implementing this to add fields that should be pre
     * populated in the form.
     * For example the isPartOf field in collections
     *
     * @param $key
     * @param $value
     */
    protected function addPrePopulatedField($key, $value)
    {
        $this->prePopulate[$key] = $value;
    }

    protected function copyEvent($postData)
    {
        if (method_exists($postData, 'toArray')) {
            $postData = $postData->toArray();
        }
        $postData = $this->clearSubjects($postData, $this->subjectCheck);
        return $this->redirectWithForm(self::ACTION_ADD, $postData);
    }

    protected function forwardEvent($postData)
    {
        if (isset($postData['kotka_carryforward'])) {
            return $this->carryForward($postData);
        } elseif (isset($postData['kotka_clear'])) {
            return $this->clearForward();
        }
        return $this->keep($postData);
    }

    /**
     * Event that takes place after save and when copying as new
     * @param $postData
     * @return \Zend\Http\Response
     */
    private function carryForward($postData)
    {
        $this->flashMessenger()->addSuccessMessage($this->successCarryMsg);
        if (method_exists($postData, 'toArray')) {
            $postData = $postData->toArray();
        }
        $postData = $this->clearSubjects($postData, $this->subjectCheck);
        if (isset($postData['MYOriginalSpecimenID'])) {
            unset($postData['MYOriginalSpecimenID']);
        }

        return $this->redirectWithForm(self::ACTION_ADD, $postData);
    }

    /**
     * Event that happens when saved and clear
     * @return \Zend\Http\Response
     */
    private function clearForward()
    {
        $this->flashMessenger()->addSuccessMessage($this->successClearMsg);
        $this->routeParams['action'] = self::ACTION_ADD;

        return $this->redirect()->toRoute($this->routeName, $this->routeParams);
    }

    /**
     * Event that happens when you want to stay on the same edit page
     *
     * @param $postData
     * @return \Zend\Http\Response
     */
    private function keep($postData)
    {
        $this->flashMessenger()->addSuccessMessage($this->successKeepMsg);

        return $this->redirectWithForm(self::ACTION_EDIT, array('subject' => $this->document->getSubject()));
    }

    /**
     * Return the form used for editing item
     *
     * @return \Kotka\Form\WarningForm
     */
    abstract protected function getEditForm();

    /**
     * Returns the form that is used for adding a new item
     *
     * @return \Zend\Form\Form
     */
    abstract protected function getAddForm();


    /**
     * @return DocumentServiceInterface
     */
    abstract protected function getDocumentService();

    /**
     * Returns the service that is used for document validation
     *
     * @return \Kotka\Service\ValidationService
     */
    protected function getValidationService()
    {
        if ($this->validationService === null) {
            $this->validationService = $this->getServiceLocator()->get('Kotka\Service\ValidationService');
        }

        return $this->validationService;
    }

    protected function customizeView(ViewModel $view)
    {

    }

    protected function getEditTitle()
    {
        return $this->editTitle;
    }

    protected function getAddTitle()
    {
        return $this->addTitle;
    }

    protected function clearSubjects($item, $next)
    {

        if (isset($item['subject'])) {
            $item['subject'] = '';
        }
        if (!is_array($next)) {
            return $item;
        }
        foreach ($next as $field => $has) {
            if (isset($item[$field])) {
                if ($has === false) {
                    unset($item[$field]);
                } else {
                    if (isset($item[$field][0])) {
                        foreach ($item[$field] as $key => $value) {
                            $item[$field][$key] = $this->clearSubjects($value, $has);
                        }
                    } else {
                        $item[$field] = $this->clearSubjects($item[$field], $has);
                    }
                }
            }
        }

        return $item;
    }

}
