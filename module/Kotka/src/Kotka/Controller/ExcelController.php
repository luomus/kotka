<?php

namespace Kotka\Controller;

use Kotka\Form\Element\ArrayCollection;
use Kotka\Form\WarningForm;
use Kotka\Service\ExcelService;
use Kotka\Triple\MYDocument;
use Triplestore\Model\Properties;
use Triplestore\Service\ObjectManager;
use Zend\Form\Element;
use Zend\Form\Fieldset;
use Zend\Form\Form;
use Zend\Http\Headers;
use Zend\Http\Response;
use Zend\InputFilter\CollectionInputFilter;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\InputInterface;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class ExcelController extends AbstractActionController
{
    private $required = [
        'MYGathering[0][MYUnit][0][MFSample][0][MFRecordParts]' => false,
        'MYGathering[0][MYUnit][0][MFSample][0][MFPreparationType]' => false
    ];

    private $keep = [];

    private $skipped = array(
        'subject' => true,
        'history' => true,
        'MZOwner' => true,
        'MYNamespaceID' => true,
        'MZDateEdited' => true,
        'MZSecureLevel' => true,
        'MYObjectID' => true,
        'MYProjectId' => true,
        'MYLatitude' => true,
        'MYDatatype' => true,
        'Longitude' => true,
        'MYInMustikka' => true,
        'MXSecureLevel' => true,
        'MYGathering[0][MYTimeStart]' => true,
        'MYGathering[0][MYTimeEnd]' => true,
        'MYGathering[0][MYUnit][0][MYInfrasubspecificSubdivision]' => true,
        'MYGathering[0][MYUnit][0][MYAlive]' => true,
        'MYUnreliableFields' => true,
        'MYGathering[0][MYIsPartOf]' => true,
        'MYGathering[0][MYCoordinatesGridYKJ]' => true,
        'MYGathering[0][MYWgs84Latitude]' => true,
        'MYGathering[0][MYWgs84Longitude]' => true,
        'MYGathering[0][MZPublicityRestrictions]' => true,
        'MYGathering[0][MYUnit][0][MYAdditionalIDs]' => true,
        'MYGathering[0][MYUnit][0][MYIsPartOf]' => true,
        'MYGathering[0][MYUnit][0][MYPreservation]' => true,
        'MYGathering[0][MYUnit][0][MZPublicityRestrictions]' => true,
        'MYGathering[0][MYUnit][0][MYDNASampleLocation]' => true,
        'MYGathering[0][MYUnit][0][MYSamplingMethod]' => true,
        'MYGathering[0][MYUnit][0][MYTypeSpecimen][0][MYIsPartOf]' => true,
        'MYGathering[0][MYUnit][0][MYTypeSpecimen][0][MZPublicityRestrictions]' => true,
        'MYGathering[0][MYUnit][0][MYIdentification][0][MYIsPartOf]' => true,
        'MYGathering[0][MYUnit][0][MYIdentification][0][MZPublicityRestrictions]' => true,
        'MYGathering[0][MYUnit][0][MYIdentification][0][MYAssociatedObservationTaxa]' => true,
        'MYGathering[0][MYUnit][0][MYTaxonID]' => true,
        'MYGathering[0][MYUnit][0][MYWingLengthMax]' => true,
        'MYGathering[0][MYUnit][0][MYSubstrateSpeciesID]' => true,
        'MYGathering[0][MYUnit][0][MYWingLengthMin]' => true,
        'MYGathering[0][MYUnit][0][MYWingLengthAccuracy]' => true,
        'MYGathering[0][MYUnit][0][MYMeasurement][MYIsPartOf]' => true,
        'MYGathering[0][MYUnit][0][MFSample][0][MZOwner]' => true,
        'MYGathering[0][MYUnit][0][MFSample][0][MYIsPartOf]' => true,
        'MYGathering[0][MYUnit][0][MFSample][0][MFPreparation][0][MYIsPartOf]' => true,
        'MYGathering[0][MYUnit][0][MFSample][0][MYMeasurement][MYIsPartOf]' => true,
    );

    private $spot = 0;

    public function createAction()
    {
        /** @var \Kotka\Form\MYDocument $form */
        /** @var \Kotka\Form\MYDocument $specimenForm */
        $specimenForm = $this->getSpecimenForm();
        $specimenForm->prepare();
        $checkBoxForm = new Form('excel');
        $inputFilter = $specimenForm->getInputFilter();
        $lang = $this->getRequest()->getQuery('lang', 'en');
        $this->spot = 0;
        $this->prepareCheckForm($this->getAllProperties(), $checkBoxForm, $specimenForm, $inputFilter, $lang);
        return new ViewModel(array(
            'form' => $checkBoxForm
        ));
    }

    protected function prepareCheckForm(Properties $properties, Form $checkboxForm, Element $specimen, $filter, $lang = 'en', &$cnt = 0, Form $original = null)
    {
        if ($original === null && $specimen instanceof WarningForm) {
            $original = $specimen;
        }
        foreach ($specimen as $element) {
            $name = $element->getName();
            $filterName = $original->getLastName($name);
            $origField = $properties->getOriginal($filterName);
            $sort = $properties->getOriginal($filterName);
            $label = $properties->getLabel($origField, $lang, $origField === null && !empty($element->getLabel())
                ? $element->getLabel() : $filterName);
            $currentFilter = $filter;
            $required = false;
            if (isset($this->customFields[$name])) {
                $this->{$this->customFields[$name]}($checkboxForm, $name, $label, $required, $element->getOption('help'));
                continue;
            }

            if ($filter instanceof InputFilterInterface && $filter->has($filterName)) {
                $currentFilter = $filter->get($filterName);
                if ($currentFilter instanceof CollectionInputFilter) {
                    $currentFilter = $currentFilter->getInputFilter();
                } else if ($currentFilter instanceof InputInterface && $currentFilter->isRequired()) {
                    $required = true;
                }
            }

            if ($element instanceof Fieldset && !$element instanceof ArrayCollection) {
                $this->prepareCheckForm($properties, $checkboxForm, $element, $currentFilter, $lang, $cnt, $original);
                continue;
            }

            if (empty($label) || isset($this->skipped[$name]) || $properties->getOrder($origField) <= 0) {
                if (in_array($name, $this->keep)) {
                    $this->addCheckBoxFormField($checkboxForm, $name, $label, $required, $element->getOption('help'));
                }
                continue;
            }
            $this->addCheckBoxFormField($checkboxForm, $name, $label, $required, $element->getOption('help'));
        }
    }

    protected function addCheckBoxFormField(Form $checkboxForm, $name, $label, $required, $help = null)
    {
        $value = $this->getTemplate($name);
        if (isset($this->required[$name])) {
            $required = $this->required[$name];
        }
        if ($required) {
            $field = new Element\Hidden();
            $field->setValue($value);
        } else {
            $field = new Element\Checkbox();
            $field->setLabel($label);
            $field->setCheckedValue($value);
            $field->setAttribute('class', ExcelService::getTopLevel($name, true));
            $field->setOption('help', $help);
            $field->setUseHiddenElement(false);
        }
        $field->setName('spot[' . $this->spot . ']');
        $checkboxForm->add($field);
        $this->spot++;
    }

    protected function addMeasurements(Form $checkboxForm, $name, $label, $required, $help = null)
    {
        /** @var \Kotka\Service\FormElementService $formElementService */
        $formElementService = $this->getServiceLocator()->get('Kotka\Service\FormElementService');
        $measurements = $formElementService->getSelect('MY.measurementEnum', false, false);
        foreach ($measurements as $field => $label) {
            $measurementName = $name . '[' . lcfirst(str_replace('MY.measurement', '', $field)) . ']';
            $this->addCheckBoxFormField($checkboxForm, $measurementName, $label, $required, $help);
        }
    }

    protected function getTemplate($name)
    {
        $name = str_replace('MYGathering[0]', 'MYGathering[__gathering__]', $name);
        $name = str_replace('[MYUnit][0]', '[MYUnit][__unit__]', $name);
        $name = str_replace('[MYIdentification][0]', '[MYIdentification][__identification__]', $name);
        $name = str_replace('[MYTypeSpecimen][0]', '[MYTypeSpecimen][__type__]', $name);
        return $name;
    }

    public function generateAction()
    {
        $request = $this->getRequest();
        $params = $request->isPost() ? $request->getPost() : $this->params()->fromQuery();
        $isSample = false;
        $hasPreparationType = false;

        foreach ($params['spot'] as $param) {
          if (strpos($param, 'MFSample') !== false) {
            $isSample = true;
          }

          if (strpos($param, 'MFPreparationType') !== false) {
            $hasPreparationType = true;
          }
        };

        if ($isSample && !$hasPreparationType) {
          $params['spot'] = array_merge($params['spot'], ['MYGathering[__gathering__][MYUnit][__unit__][MFSample][0][MFPreparationType]']);
        }
        /** @var \Kotka\Service\ExcelService $excelService */
        $excelService = $this->serviceLocator->get('Kotka\Service\ExcelService');
        $excelService->setForm($this->getSpecimenForm());
        $filename = $excelService->generateFromParams($params);

        $file = file_get_contents($filename);
        $headers = new Headers();
        $response = new Response();
        $headers->clearHeaders()
            ->addHeaderLine('Content-type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
            ->addHeaderLine('Content-Disposition', 'attachment; filename="Kotka_import_sheet.xlsx"')
            ->addHeaderLine("Content-Length", strlen($file));
        $response->setHeaders($headers);
        $response->setContent($file);

        return $response;
    }

    private function getSpecimenForm()
    {
        $formGenerator = $this->serviceLocator->get('Kotka\Service\FormGeneratorService');
        $form = $formGenerator->getForm(new MYDocument());
        $form->setData(array('MYGathering' => array(array(
            'MYUnit' => array(array(
                'MYTypeSpecimen' => [['MYTypeSpecies' => '']],
                'MFSample' => [['MZ.owner' => '']]
            ))
        ))), false);

        return $form;
    }

    /**
     * @return Properties
     */
    private function getAllProperties()
    {
        /** @var ObjectManager $om */
        $om = $this->getServiceLocator()->get('Triplestore\ObjectManager');
        return $om->getMetadataService()->getAllProperties();
    }
}