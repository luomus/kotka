<?php

namespace Kotka\Controller;


use DateTime;
use Kotka\ResultSet\AccessionResultSet;
use Kotka\Service\AccessionServiceTrait;
use Kotka\Service\DocumentAccess;
use Kotka\Service\DocumentService;
use Kotka\Triple\MYDocument;
use Kotka\Triple\PUUBranch;
use Kotka\Triple\PUUEvent;
use Triplestore\Classes\MAPersonInterface;
use Triplestore\Classes\PUUBranchInterface;
use User\View\Helper\User;
use Zend\Http\Request;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class AccessionsController extends AbstractActionController
{
    use AccessionServiceTrait;

    public function indexAction()
    {
        $view = new ViewModel();
        $uri  = $this->getRequest()->getQuery('uri', null);
        /** @var \Kotka\Service\SpecimenService $specimenService */
        $specimenService  = $this->getServiceLocator()->get('Kotka\Service\SpecimenService');
        $accessionService = $this->getAccessionService();
        $branch = $accessionService->getBranchById($uri);
        if ($branch instanceof PUUBranchInterface) {
            $uri = $branch->getPUUAccessionID();
        }
        $view->setVariable('branchesResultSet', $accessionService->getAllBranchesByRoot($uri, false));
        $view->setVariable('root', $specimenService->getById($uri));
        $view->setVariable('uri', $uri);

        return $view;
    }

    public function eventFormAction()
    {
        $view = new ViewModel();
        $uri  = $this->getRequest()->getQuery('uri', null);
        $accessionService = $this->getAccessionService();
        $event = $accessionService->getEventById($uri);
        if ($event === null) {
            /** @var MAPersonInterface $user */
            $user = $this->getServiceLocator()->get('user');
            $event = new PUUEvent();
            $event->setPUUDate(new DateTime());
            $event->setPUUAgent($user->getSubject());
            $view->setVariable('addition', true);
        }
        /** @var Request $request */
        $request = $this->request;
        $form = $this->getEventForm();
        $valid = false;
        $success = false;
        $form->bind($event);
        if ($request->isDelete()) {
            if (!$this->hasEditRightsToAccession($accessionService->getBranchById($event->getPUUBranchID())->getPUUAccessionID())) {
                return $this->getResponse()->setStatusCode(403);
            }
            $valid = true;
            $success = $accessionService->deleteEvent($event);
        } else if ($request->isPost()) {
            $postData = $this->request->getPost()->toArray();
            $form->setData($postData);
            if ($form->isValid()) {
                $valid = true;
                /** @var PUUEvent $event */
                $event = $form->getData();
                if (!$this->hasEditRightsToAccession($accessionService->getBranchById($event->getPUUBranchID())->getPUUAccessionID())) {
                    return $this->getResponse()->setStatusCode(403);
                }
                try {
                    $success = $accessionService->storeEvent($event);
                    $event->getPUUBranchID();
                } catch (\Exception $e) {
                    $this->getResponse()->setStatusCode(500);
                }
            }
        }
        $view->setVariable('form', $form);
        $view->setVariable('valid', $valid);
        $view->setVariable('success', $success);
        $view->setTerminal(true);

        return $view;
    }

    public function branchFormAction()
    {
        /** @var Request $request */
        $request = $this->request;
        $view = new ViewModel();
        $uri = $this->getRequest()->getQuery('uri', null);
        $accessionService = $this->getAccessionService();
        $branch = $accessionService->getBranchById($uri);
        if ($branch === null && !$request->isDelete()) {
            $branch = new PUUBranch();
            $branch->setPUUExists(1);
            $user = $this->getServiceLocator()->get('user');
            /*
            if ($user instanceof MAPersonInterface) {
                /** @var User $helper *
                $helper = $this->getServiceLocator()->get('viewhelpermanager')->get('User');
                $branch->setPUULeg($helper(User::FULLNAME));
            }
            */
            $view->setVariable('addition', true);
        }
        $form = $this->getBranchForm();
        $valid = false;
        $success = true;
        if ($branch !== null) {
            $form->bind($branch);
        }
        if ($request->isDelete()) {
            if (!$this->hasEditRightsToAccession($branch->getPUUAccessionID())) {
                return $this->getResponse()->setStatusCode(403);
            }
            $valid = true;
            $success = $branch !== null ? $accessionService->deleteBranch($branch) : true;
        } else if ($request->isPost()) {
            $success = false;
            $postData = $this->request->getPost()->toArray();
            $form->setData($postData);
            if ($form->isValid()) {
                $valid = true;
                /** @var PUUBranch $branch */
                $branch = $form->getData();
                if (!$this->hasEditRightsToAccession($branch->getPUUAccessionID())) {
                    return $this->getResponse()->setStatusCode(403);
                }
                try {
                    $success = $accessionService->storeBranch($branch, $uri);
                } catch (\Exception $e) {
                    $this->getResponse()->setStatusCode(500);
                }

            }
        }
        $view->setVariable('form', $form);
        $view->setVariable('valid', $valid);
        $view->setVariable('success', $success);
        $view->setTerminal(true);

        return $view;
    }

    public function branchAction()
    {
        $uri = $this->getRequest()->getQuery('uri', null);
        $full = $this->getRequest()->getQuery('full', false);
        $accessionService = $this->getAccessionService();
        $branch = $accessionService->getBranchById($uri);
        if ($branch === null) {
            $this->getResponse()->setStatusCode(500);
            $branch = new PUUBranch();
        }
        $view = new ViewModel(['branch' => $branch, 'full' => $full, 'branchResultSet' => new AccessionResultSet()]);
        $view->setTerminal(true);

        return $view;
    }

    public function eventAction()
    {
        $uri = $this->getRequest()->getQuery('uri', null);
        $accessionService = $this->getAccessionService();
        $event = $accessionService->getEventById($uri);
        if ($event === null) {
            $this->getResponse()->setStatusCode(500);
            $event = new PUUEvent();
        }
        $view = new ViewModel(['event' => $event]);
        $view->setTerminal(true);

        return $view;
    }

    public function moveRootEventAction()
    {
        $uri = $this->getRequest()->getQuery('uri', null);
        /** @var \Kotka\Service\SpecimenService $specimenService */
        $specimenService = $this->getServiceLocator()->get('Kotka\Service\SpecimenService');
        $root = $specimenService->getById($uri);
        if (!$root instanceof MYDocument) {
            return $this->statusMessage("Couldn't find '$uri'");
        }
        if (!$this->hasEditRightsToAccession($root)) {
            return $this->statusMessage("You cannot edit the '$uri'");
        }
        $remove = $this->getRequest()->getQuery('event', '');
        $events = $root->getMYEvent();
        $idx = array_search($remove, $events);
        if ($idx === false) {
            return $this->statusMessage("Couldn't find event '$remove' in '$root'");
        }
        unset($events[$idx]);
        $root->setMYEvent($events);
        if (!$specimenService->update($root)) {
            return $this->statusMessage("Saving specimen failed", 500);
        }
        return $this->statusMessage('ok', 200);
    }

    private function hasEditRightsToAccession($accession) {
        return DocumentAccess::canEdit(
            $this->getAccessionService()->getAccession($accession),
            $this->getServiceLocator()->get('user')
        );
    }

    private function statusMessage($msg, $code = 400)
    {
        $this->getResponse()->setStatusCode($code);
        return new JsonModel([
            'status' => $msg
        ]);
    }

    /**
     * @return \Kotka\Form\PUUBranch
     */
    protected function getBranchForm()
    {
        return $this->getForm(new PUUBranch());
    }

    /**
     * @return \Kotka\Form\PUUEvent
     */
    protected function getEventForm()
    {
        return $this->getForm(new PUUEvent());
    }

    protected function getForm($puu)
    {
        /** @var \Kotka\Service\FormGeneratorService $formGenerator */
        $formGenerator = $this->serviceLocator->get('Kotka\Service\FormGeneratorService');
        $form = $formGenerator->getForm($puu);
        $form->setObject($puu);
        return $form;
    }

    /**
     * @return Request
     */
    public function getRequest()
    {
        return parent::getRequest();
    }

}