<?php

namespace Kotka\Controller;

use Kotka\DataGrid\SeedGrid;
use Zend\Http\Request;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Paginator\Paginator;
use Zend\View\Model\ViewModel;

class SeedController extends AbstractActionController
{

    protected $defaultParams = [
        'accepted' => 1,
        'q' => 'datasetID:"GX.5804" AND branchExistsInLocationID: "HR.2649"',
        'sortBy' => 'family,acceptedTaxon,TaxonAndInfra',
        'sortDirection' => 'ASC,ASC,ASC'
    ];

    /**
     * Default action if none provided
     *
     * @return array
     */
    public function indexAction()
    {
        $this->getResponse()->setStatusCode(404);
    }

    public function listAction()
    {
        /** @var Request $request */
        $request = $this->getRequest();
        $query = $request->getQuery(null, [])->toArray();
        $params = array_replace($query, $this->defaultParams);
        $id = $this->getEvent()->getRouteMatch()->getParam('id');


        if (isset($id) && $id === 'tur') {
          $params = array_replace($params, [
            'q' => 'datasetID:"GX.18775" AND branchExistsInLocationID: "HR.6198"',
            'uri' => 'http://tun.fi/HR.5276'
          ]);
        }

        /** @var \Elastic\Client\Search $adapter */
        $adapter = $this->serviceLocator->get('Elastic\Client\Search');

        $grid = new SeedGrid();
        $grid->setServiceLocator($this->getServiceLocator());

        $grid->prepareGrid($params, $adapter);
        $result = $grid->getGrid()->getResponse();
        /** @var Paginator $paginator */
        $paginator = $result['paginator'];

        return new ViewModel(['specimens' => $paginator]);
    }
}
