<?php
namespace Kotka\Controller;

use Zend\Form\Element;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class DocumentController extends AbstractActionController
{

    public function indexAction()
    {

    }

    public function fieldAction()
    {
        /** @var \Kotka\Service\FormService $formService */
        $formService = $this->serviceLocator->get('Kotka\Service\FormService');
        $data = $formService->explainAllForms();
        return new ViewModel(['explains' => $data]);
    }

    public function distinctValuesAction()
    {
        $fields = ['MY.leg', 'MY.det'];
        /** @var \Kotka\Service\StatsService $statService */
        $statService = $this->serviceLocator->get('Kotka\Service\StatsService');
        $data = $statService->getDistinctValues($fields);
        return new ViewModel(['stats' => $data]);
    }

} 