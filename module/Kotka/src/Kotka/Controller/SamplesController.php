<?php

namespace Kotka\Controller;

use Common\Service\IdService;
use Kotka\Triple\MFSample;
use Zend\View\Model\ViewModel;

/**
 * Class SamplesController
 *
 * This controller handles all the http sample requests.
 *
 * @package Kotka\Controller
 */
class SamplesController extends AbstractKotkaController
{
    /** @var \Kotka\Service\SampleService */
    protected $documentService;

    protected $addTitle = 'Add sample';
    protected $editTitle = 'Edit sample';

    /**
     * Displays the list of samples
     * Template: view/kotka/sample/index.phtml
     *
     * @return \Zend\View\Model\ViewModel
     */
    public function indexAction()
    {
        return new ViewModel(array('samples' => $this->getDocumentService()->getAll()));
    }

    /**
     * handels adding new sample
     * Uses the same template as editAction
     *
     * @return \Zend\View\Model\ViewModel
     */
    public function addAction()
    {
        return parent::addAction();
    }

    /**
     * Handles the editing of the sample
     * Template is in view/kotka/sample/edit.phtml
     *
     * @return \Zend\View\Model\ViewModel
     */
    public function editAction()
    {
        return parent::editAction();
    }

    /**
     * Returns form for editing the sample
     * @param bool $removeID
     * @return \Kotka\Form\WarningForm
     */
    public function getEditForm($removeID = true)
    {
        /** @var \Kotka\Service\FormGeneratorService $formGenerator */
        $formGenerator = $this->serviceLocator->get('Kotka\Service\FormGeneratorService');
        $form = $formGenerator->getForm(new MFSample());
        if ($removeID) {
            $form->remove('SampleID');
            $inputFilter = $form->getInputFilter();
            $inputFilter->remove('SampleID');
        }
        return $form;
    }

    /**
     * Returns form for adding new sample
     *
     * @return \Kotka\Form\WarningForm
     */
    public function getAddForm()
    {
        $form = $this->getEditForm(false);
        $form->setObject(new MFSample());

        return $form;
    }

    /**
     * @param MFSample $document
     * @param $postData
     * @param $type
     */
    public function postValidate($document, $postData, $type)
    {
        if ($type === self::ACTION_ADD) {
            if (isset($postData['SampleID'])) {
                $document->setSubject(IdService::getQName($postData['SampleID']));
            }
        }
    }

    /**
     * Return sample service
     *
     * @return \Kotka\Service\SampleService
     */
    protected function getDocumentService()
    {
        if ($this->documentService === null) {
            $this->documentService = $this->serviceLocator->get('Kotka\Service\SampleService');
        }

        return $this->documentService;
    }

    protected function getEditTitle()
    {
        return $this->editTitle . ' ' . $this->document->getUri();
    }
}
