<?php

namespace Kotka\Controller;

use Common\Service\IdService;
use Kotka\Job\ExportExcel;
use Kotka\Job\ImportExcel;
use Kotka\Model\IdentifierInterpreter;
use Kotka\Service\ImportService;
use Kotka\Triple\MYDocument;
use Kotka\Validator\IdentifierRange;
use Kotka\Validator\ImagesPath;
use Kotka\Validator\UniqueRange;
use Triplestore\Classes\MAPersonInterface;
use User\Enum\User;
use Zend\Http\Header\Referer;
use Zend\Http\Headers;
use Zend\Http\Request;
use Zend\Log\LoggerInterface;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Permissions\Acl\Acl;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use ZfcDatagrid\Column;
use ZfcDatagrid\Column\Style;
use ZfcDatagrid\Column\Type;

class ToolsController extends AbstractActionController
{
    const CACHE_IMPORT_KEY = 'cache_import_key';
    const IMPORTED_KEY = 'imported';

    const TYPE_KEY = 'Type';
    const TYPE_UPDATE = 'update';
    const TYPE_INSERT = 'insert';

    const CELL_HAS_ERRORS = '<br>Error:&nbsp;&nbsp;<br>';
    const CELL_HAS_WARNING = '<br>Warning:&nbsp;&nbsp;<br>';

    /** @var \Kotka\Service\StatsService */
    protected $statsService;
    protected $logger;

    /**
     * Displays the simple stats page
     * Templates is in view/tools/simplestats.phtml
     *
     * @return ViewModel
     */
    public function simpleStatsAction()
    {
        return new ViewModel(array('stats' => $this->getStatsService()->getSimpleStats(),));
    }

    public function qrReaderAction()
    {
        return new ViewModel();
    }

    /**
     * Return the stat service
     *
     * @return \Kotka\Service\StatsService
     */
    protected function getStatsService()
    {
        if ($this->statsService === null) {
            $this->statsService = $this->serviceLocator->get('Kotka\Service\StatsService');
        }

        return $this->statsService;
    }

    public function statusAction()
    {
        $status = 'OK!';

        // Check that the web drive is mounted
        if (!file_exists(ImagesPath::IMAGE_REAL_PATH . 'Test/zabbix_test.txt')) {
            $status = 'Could not find mounted image folder. Adding images from directory will not work.';
        }

        if ($status !== 'OK!') {
            $status .= '<br><a target="_blank" href="https://wiki.helsinki.fi/xwiki/bin/view/luomusict/Luomus%20Biodiversity%20Informatics%20Unit/IT%20Issues/Kotka%20issues/">Help</a>';
        }

        echo $status;
        exit();
    }

    /**
     * Generates the generic labels
     */
    public function genericLabelAction()
    {
        $params = $this->getRequest()->getQuery()->toArray();
        $params['format'] = 'generic';
        $vm = [];
        /** @var \Kotka\Form\GenericLabel $form */
        $form = $this->serviceLocator->get('FormElementManager')->get('Kotka\Form\GenericLabel');
        $vm['form'] = $form;
        $vm['params'] = $params;
        $form->setData($params);
        if (isset($params['identifier']) && !isset($params['no']) && (!$form->isValid() || $form->hasWarnings())) {
            if ($form->isValid()) {
                $vm['pdf'] = $this->getRequest()->getUri() . '&no=1';;
            }
            $view = new ViewModel($vm);
            $this->layout('layout/minimal');
            return $view;
        }
        $vm['export'] = true;
        $vm['generic'] = true;
        if (!isset($params['export'])) {
            return new ViewModel($vm);
        }
        $this->layout('layout/minimal');
        /** @var MAPersonInterface $user */
        $user = $this->getServiceLocator()->get('user');
        $defaultDomain = $user->getMADefaultQNamePrefix() === null ?
            IdService::QNAME_PREFIX_LUOMUS :
            $user->getMADefaultQNamePrefix();
        $ids = new IdentifierInterpreter($params['identifier'], $defaultDomain);
        /** @var \Kotka\Service\LabelService $labelService */
        /** @var \Kotka\Service\PdfService $pdfService */
        $labelService = $this->getServiceLocator()->get('Kotka\Service\LabelService');
        $pdfService = $this->getServiceLocator()->get('Kotka\Service\PdfService');
        $labels = $labelService->extractGenericLabelData($ids, $params);
        $html = $labelService->getLabelHtml($labels, $params);

        $content = $pdfService->htmlToPdfApi($html, $params['format']);
        $headers = new Headers();
        $headers->addHeaderLine('Content-type: application/pdf');
        $response = $this->getResponse();
        $response->setHeaders($headers);
        $response->setContent($content);

        return $response;
    }

    public function clearCacheAction()
    {
        $cache = $this->serviceLocator->get('cache');
        $permaCache = $this->serviceLocator->get('perma-cache');
        /** @var \Triplestore\Service\ObjectManager $om */
        $om = $this->serviceLocator->get('Triplestore\ObjectManager');
        $om->getCache()->flush();
        $cache->flush();
        $permaCache->flush();
        $this->flashMessenger()->addSuccessMessage('Cache cleared!');

        return $this->redirect()->toUrl($this->getRefererUri());
    }

    public function excelSheetsAction()
    {
        $postData = $this->getRequest()->getFiles()->toArray();
        if (isset($postData['import']) && isset($postData['import']['tmp_name'])) {
            return new JsonModel($this->getImportService()->getExcelSheets($postData['import']['tmp_name']));
        }
        return new JsonModel([]);
    }

    /**
     * Displays the import form to the user.
     *
     * @return ViewModel
     */
    public function importAction()
    {
        $view = new ViewModel();
        $form = $this->getImportForm();
        $populate = $this->StoreOverRequest()->fetch('postdata');
        if ($populate !== null) {
            $form->setData($populate);
        }
        $view->setVariable('form', $form);
        $view->setVariable('header', "Import specimen data from Excel-file");

        return $view;
    }

    public function errorTestAction()
    {
        throw new \Exception('Just a error test!');
    }

    /**
     * Saves the imported excel spreadsheet
     * and stores the objects extracted to the db
     *
     * @return \Zend\Http\Response|ViewModel
     */
    public function previewAction()
    {
        if (!$this->getRequest()->isPost()) {
            return $this->redirect()->toRoute('https/tools', array('action' => 'import'));

        }
        $form = $this->getImportForm();
        $postData = array_merge_recursive($this->getRequest()->getPost()->toArray(), $this->getRequest()->getFiles()->toArray());
        $form->setData($postData);
        if (!$form->isValid()) {
            $this->flashMessenger()->addErrorMessage($form->getMessages());
            return $this->redirectWithForm('import', $postData);
        }
        $formData = $form->getData();
        $file = $formData['import']['tmp_name'];
        $sheet = isset($postData['sheet']) && $postData['sheet'] !== '' ? $postData['sheet'] : 0;
        if (isset($formData['huge']) && $formData['huge'] === '1') {
            $user = $this->serviceLocator->get('user');
            if ($user instanceof MAPersonInterface) {
                $formData['user'] = $user->getSubject();
            }
            $formData['sheet'] = $sheet;
            $this->Store()->store(self::CACHE_IMPORT_KEY, $formData);
            return $this->saveJobAction();
        }
        $importService = $this->getImportService();
        $data = $importService->Excel2Array($file, $sheet);
        if (isset($formData['override']) && $formData['override'] === '1' && $this->isAllowedAdvancedImports()) {
            $importService->setOptions([ImportService::OPTION_USER_FROM_DATA]);
        }
        $cnt = count($data);
        if (!is_array($data) || $cnt == 0) {
            $errorMsg = $this->getErrorMessage($this->getImportService());
            $this->flashMessenger()->addErrorMessage($errorMsg);
            return $this->redirectWithForm('import', $postData);
        }
        $result = $importService->validateArray($data, $formData['datatype'], $formData['organization']);
        $formData['data'] = $data;
        $dataGrid = $this->getDataGrid();
        if ($result) {
            $user = $this->serviceLocator->get('user');
            if ($user instanceof MAPersonInterface) {
                $formData['user'] = $user->getSubject();
            }
            $formData['sheet'] = $sheet;
            $this->Store()->store(self::CACHE_IMPORT_KEY, $formData);
        } else {
            if ($importService->getErrorCode() === ImportService::ERROR_FIELD_NOT_ON_FORM) {
                $this->flashMessenger()->addErrorMessage(sprintf("<strong>Invalid data in the file</strong>. '%s' is not an allowed field name, please check the name.", $this->getImportService()->getErrorCause()));
                return $this->redirectWithForm('import', $postData);
            }
            foreach ($data as $key => $value) {
                if (!isset($value['errors'])) {
                    continue;
                }
                $errors = $value['errors'];
                $data[$key]['errors'] = $dataGrid->mergeMessages($data[$key], $errors, self::CELL_HAS_ERRORS, '#a94442');
                if ($errors !== $data[$key]['errors']) {
                    $data[$key]['errors']['hasErrors'] = 'Data has error(s)!';
                }
            }
        }
        $dataGrid->setLabels($importService->getLabels());
        $dataGrid->prepareGrid($data, true, true);

        $view = new ViewModel();
        $view->setVariable('hasErrors', !$result);
        $view->setVariable('print', $this->getPrintForm());
        $view->addChild($dataGrid->getGrid()->getViewModel(), 'dataGrid');

        return $view;
    }

    public function saveJobAction()
    {
        $store = $this->Store()->fetch(self::CACHE_IMPORT_KEY);
        if (!empty($store) && is_array($store)) {
            $this->Store()->store(self::CACHE_IMPORT_KEY, '');
            $file = ImportExcel::getUserImportLogFile($store['user']);
            if (file_exists($file)) {
                unlink($file);
            }
            if (!isset($store['override'])) {
                $store['override'] = false;
            }
            if ($store['override'] === '1' && !$this->isAllowedAdvancedImports()) {
                $store['override'] = '0';
            }
            if (isset($store['data'])) {
                unset($store['data']);
            }
            $user = $this->serviceLocator->get('user');
            if ($user instanceof MAPersonInterface) {
                $store['defaultNS'] = $user->getMADefaultQNamePrefix() === null ?
                    trim(IdService::DEFAULT_DB_QNAME_PREFIX, ':') : $user->getMADefaultQNamePrefix()
                ;
            }
            $queue = $this->serviceLocator->get('SlmQueue\Queue\QueuePluginManager')->get('slow');
            $job = new ImportExcel();
            $job->setContent($store);
            $queue->push($job);
        }

        return $this->redirect()->toRoute('https/tools', array('action' => 'save'));
    }

    public function exportDownloadAction() {
        /** @var \Kotka\Service\ExcelService $excelService */
        $user = $this->getServiceLocator()->get('user');
        if ($user instanceof MAPersonInterface) {
            $file = ExportExcel::getUserExportExcel($user->getSubject());
            $excelService = $this->serviceLocator->get('Kotka\Service\ExcelService');
            return $excelService->getResponse($file, 'search-export-' . $user->getMALTKMLoginName() . '.xlsx');
        }
        return null;
    }

    public function exportStatusAction() {
        $user = $this->getServiceLocator()->get('user');
        $content = '';
        $status = 'waiting';
        $errors = false;
        $queue  = 0;
        if ($user instanceof MAPersonInterface) {
            $file = ExportExcel::getUserExportLog($user->getSubject());
            if (file_exists($file)) {
                $content = $this->readFile($file);
                $status = 'exporting';
            }
        }
        if (strpos($content, ImportExcel::DONE_STRING) !== false) {
            $status = 'done';
        }
        if (strpos($content, ImportExcel::ERROR_STRING) !== false) {
            $errors = true;
        }
        if ($status === 'waiting') {
            $queue = $this->getQueueLength('slow');
        }
        $view = new ViewModel([
            'status' => $status,
            'content' => $content,
            'errors' => $errors,
            'queue' => $queue
        ]);
        $view->setTerminal(true);

        return $view;
    }

    public function exportAction() {
        return new ViewModel([
            'queue' => $this->getQueueLength('slow')
        ]);
    }

    public function saveAction()
    {
        return new ViewModel([
            'queue' => $this->getQueueLength('slow')
        ]);
    }

    public function importStatusAction()
    {
        $user = $this->getServiceLocator()->get('user');
        $content = '';
        $status = 'waiting';
        $errors = false;
        $queue  = 0;
        if ($user instanceof MAPersonInterface) {
            $file = ImportExcel::getUserImportLogFile($user->getSubject());
            if (file_exists($file)) {
                $content = $this->readFile($file);
                $status = empty($content) ?
                    'validating' :
                    'importing';
            }
        }
        if (strpos($content, ImportExcel::DONE_STRING) !== false) {
            $status = 'done';
        }
        if (strpos($content, ImportExcel::VALIDATION_ERROR_STRING) !== false) {
            $status = 'validation-error';
        }
        if (strpos($content, ImportExcel::ERROR_STRING) !== false) {
            $errors = true;
        }
        if ($status === 'waiting') {
            $queue = $this->getQueueLength('slow');
        }
        $view = new ViewModel([
            'status' => $status,
            'content' => $content,
            'errors' => $errors,
            'queue' => $queue
        ]);
        $view->setTerminal(true);

        return $view;
    }

    private function getQueueLength($queue) {
        try {
            $pheanstalk = $this->getServiceLocator()->get('ZfBeanstalkdUI\Service\PheanstalkService');
            $tubes = $pheanstalk->listTubes();
            foreach($tubes as $tube) {
                if ($tube !== $queue) {
                    continue;
                }
                /** @var \Pheanstalk\Response\ArrayResponse $stats */
                $stats = $pheanstalk->statsTube($queue);
                if ($stats instanceof \Pheanstalk\Response\ArrayResponse) {
                    return (int)($stats['current-jobs-ready'] + $stats['current-jobs-reserved']);
                }
            }
        } catch (\Exception $e) {
            $this->getLogger()->err("Couldn't count items in queue!", [$e->getMessage()]);
        }
        return 0;
    }

    private function readFile($file) {
        $handle = fopen($file, "r");
        $size = filesize($file);
        $size++;
        $fileContents = fread($handle, $size);
        fclose($handle);
        return $fileContents;
    }

    public function labelsAction()
    {
        $store = $this->Store()->fetch(self::CACHE_IMPORT_KEY);
        $data = $store['data'];
        $params = $this->getRequest()->getQuery()->toArray();
        /** @var \Kotka\Service\LabelService $labelService */
        /** @var \Kotka\Service\PdfService $pdfService */
        $labelService = $this->getServiceLocator()->get('Kotka\Service\LabelService');
        $pdfService = $this->getServiceLocator()->get('Kotka\Service\PdfService');
        $specimens = array();
        if (!is_array($data)) {
            return new ViewModel(array('msg' => '<h1>System error:</h1> <p>Could not fetch specimen data.<br>Please try again little later.</p>'));
        }
        foreach ($data as $item) {
            if (isset($item['_model_']) && $item['_model_'] instanceof MYDocument) {
                $specimens[] = clone $item['_model_'];
            }
        }
        $labels = $labelService->extractLabelData($specimens);
        if (isset($params['format']) && $params['format'] === 'labelDesigner') {
            $labelDesignerView = new ViewModel();
            $labelDesignerView->setTemplate('kotka/tools/generic-label');
            $labelDesignerView->setVariable('labels', $labels);
            return $labelDesignerView;
        }
        $html = $labelService->getLabelHtml($labels, $params);
        if ($html === '') {
            return new ViewModel(array('msg' => 'No identification data found from the given set.'));
        }

        $content = $pdfService->htmlToPdfApi($html, $params['format']);
        $headers = new Headers();
        $headers->addHeaderLine('Content-type: application/pdf');
        $headers->addHeaderLine('Content-Disposition: attachment; filename="search.pdf"');
        $response = $this->getResponse();
        $response->setHeaders($headers);
        $response->setContent($content);

        return $response;
    }

    public function getErrorMessage(ImportService $importer)
    {
        $errorCode = $importer->getErrorCode();
        $errorCause = $importer->getErrorCause();
        $errorMsg = '<strong>Error importing file</strong>';
        switch ($errorCode) {
            case 0:
                $errorMsg = '<strong>Empty file</strong>. There must be at least one data row.';
                break;
            case ImportService::ERROR_TOO_MANY_ROWS:
                $errorMsg = '<strong>Too many rows to import</strong>.' .
                    '<p>Make sure that you don\'t have more than ' . ImportService::MAX_EXCEL_ROW_SIZE . ' data rows in the excel.</p>';
                break;
            case ImportService::ERROR_IN_VALID_FORMAT:
                $errorMsg = '<strong>File has cells formatted in a format other than text</strong>.' .
                    '<p>Change all the cells to text format and try again.<br><br>' .
                    '<strong>Please note</strong> that changing the format of a cell can in some cases change the actual value</p>';
                break;
            case ImportService::ERROR_READING_FILE:
                $errorMsg = '<strong>Unable to read the file</strong>.' .
                    '<p>Make sure that it\'s in a format that Kotka can understand.</p>';
                break;
            case ImportService::ERROR_MISSING_HEADER:
                $errorMsg = '<strong>Excel is missing column header info</strong>.' .
                    '<p>Check that the data is on the <strong>first sheet</strong> of the file.</p>';
                break;
            case ImportService::ERROR_AMBIGUOUS_HEADER:
                $errorMsg = '<strong>Excel has ambiguous column headers</strong>.' .
                    '<p>The column "' . $errorCause . '" exists more than once.</p>';
                break;
            case ImportService::ERROR_SAVING_DATA:
                $errorMsg = "<h1><strong>There was error saving data to database!</strong></h1><br>Not all items where successfully saved!<br>";
                $errorMsg .= "Failed to save " . $importer->getErrorCause();
        }
        return $errorMsg;
    }

    /**
     * Return the import form
     *
     * @return \Kotka\Form\ImportExcel
     */
    protected function getImportForm()
    {
        /** @var \Kotka\Form\ImportExcel $form */
        $form = $this->serviceLocator->get('FormElementManager')->get('Kotka\Form\ImportExcel');
        if ($this->isAllowedAdvancedImports()) {
            $form->addAdvancedFields();
        }
        return $form;
    }

    private function isAllowedAdvancedImports() {
        /** @var Acl $acl */
        $acl = $this->serviceLocator->get('acl');
        $user = $this->serviceLocator->get('user');
        return false;
        return ($user instanceof MAPersonInterface && $acl->isAllowed($user->getMARoleKotka(), 'advanced', 'import'));
        }

    /**
     * Returns the label print form
     *
     * @return \Kotka\Form\ImportPrint
     */
    protected function getPrintForm()
    {
        return $this->serviceLocator->get('FormElementManager')->get('Kotka\Form\ImportPrint');
    }

    /**
     * @return \Kotka\Service\ImportService
     */
    protected function getImportService()
    {
        return $this->getServiceLocator()->get('Kotka\Service\ImportService');
    }

    /**
     * @return \Kotka\Service\SpecimenService
     */
    protected function getSpecimenService()
    {
        return $this->getServiceLocator()->get('Kotka\Service\SpecimenService');
    }

    /**
     * @return \Kotka\DataGrid\ImportGrid
     */
    protected function getDataGrid()
    {
        return $this->getServiceLocator()->get('Kotka\DataGrid\ImportGrid');
    }

    /**
     * @return LoggerInterface
     */
    protected function getLogger()
    {
        if ($this->logger === null) {
            $this->logger = $this->getServiceLocator()->get('logger');
        }
        return $this->logger;
    }

    /**
     * This returns response with form data stored in session
     *
     * @param string $toAction The action name for the redirect
     * @param array $postData Data to be stored in session
     * @return \Zend\Http\Response
     */
    protected function redirectWithForm($toAction, $postData)
    {
        $this->StoreOverRequest()->store('postdata', $postData);

        return $this->redirect()->toRoute('https/tools', array('action' => $toAction));
    }

    private function getRefererUri()
    {
        $referer = $this->getRequest()->getHeader('Referer');
        if ($referer instanceof Referer) {
            return $referer->getUri();
        }
        return '/';
    }

    public function generatedFormAction()
    {
        /** @var \Kotka\Service\LajiForm $lajiFormService */
        $lajiFormService = $this->getServiceLocator()->get('Kotka\Service\LajiForm');
        $form = $lajiFormService->getForm(6798);
        /** @var Request $request */
        $request = $this->getRequest();
        $form->setData($request->getPost());
        return ['form' => $form, 'json' => $lajiFormService->getJson()];
    }

    public function autofillExcelAction() {
        $view = new ViewModel();
        $form = $this->getImportForm();
        $form->remove('datatype');
        $form->remove('organization');
        $form->remove('huge');

        $populate = $this->StoreOverRequest()->fetch('postdata');
        if ($populate !== null) {
            $form->setData($populate);
        }
        $view->setVariable('form', $form);
        $view->setVariable('header', "Autofill Excel content");

        /** @var Request $request */
        $request = $this->getRequest();

        if ($request->isPost()) {
            $files = $request->getFiles()->toArray();
            $sheet = $request->getPost('sheet', 0);
            if (isset($files['import']) && isset($files['import']['tmp_name'])) {
                try {
                    $excel = $this->getImportService()->autofillExcel($files['import']['tmp_name'], $sheet);
                    /** @var \Kotka\Triple\MAPerson $user */
                    $user = $this->serviceLocator->get('User');
                    /** @var \Kotka\Service\ExcelService $excelService */
                    $excelService = $this->serviceLocator->get('Kotka\Service\ExcelService');
                    $filename = 'filled-excel-' . $user->getSubject() . '.xlsx';
                    $fullFilename = $this->getPath() . $filename;
                    $excelService->setForm($this->getSpecimenForm());
                    $excelService->writeExcel2007($excel, $fullFilename);
                    return $excelService->getResponse($fullFilename, $filename);
                } catch (\Exception $e) {
                    $view->setVariable('error', "Excel creation failed!.");
                }
            }
        }

        return $view;
    }


    private function getPath()
    {
        $config = $this->serviceLocator->get('Config');
        return $config['search']['export_dir'] . DIRECTORY_SEPARATOR;
    }


    private function getSpecimenForm()
    {
        $formGenerator = $this->serviceLocator->get('Kotka\Service\FormGeneratorService');
        $form = $formGenerator->getForm(new MYDocument());

        return $form;
    }

}
