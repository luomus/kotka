<?php

namespace Kotka\Controller;

use Kotka\Service\StatsService;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class ReportController extends AbstractActionController
{
    public $transactionTypes = [
        'HRA.transactionTypeLoanIncoming' => 'li',
        'HRA.transactionTypeGiftIncoming' => 'gi',
        'HRA.transactionTypeExchangeIncoming' => 'ei',
        'HRA.transactionTypeLoanOutgoing' => 'lo',
        'HRA.transactionTypeGiftOutgoing' => 'go',
        'HRA.transactionTypeExchangeOutgoing' => 'eo',
        'total' => 'to'
    ];

    public function annualAction()
    {
        $lastYear = date("Y", strtotime("-1 year"));
        $year = $this->getRequest()->getQuery('year', $lastYear);
        if (strlen($year) != 4 || !is_numeric($year)) {
            $year = $lastYear;
        }
        /** @var StatsService $statService */
        $statService = $this->getServiceLocator()->get('Kotka\Service\StatsService');
        $results = $statService->getSpecimensInCollections($year);

        return new ViewModel(['year' => $year, 'stats' => $results]);
    }

    public function transactionAction()
    {
        /** @var \Kotka\Service\TransactionService $transactionService */
        $transactionService = $this->getServiceLocator()->get('Kotka\Service\TransactionService');
        $allTransactions = $transactionService->getAll([
            'MZ.owner',
            'HRA.transactionRequestReceived',
            'HRA.transactionType',
            'HRB.away',
            'HRB.awayOther',
            'HRB.damaged',
            'HRB.damagedOther',
            'HRB.missing',
            'HRB.missingOther',
            'HRB.returned',
            'HRB.returnedOther',
        ]);
        $results = [];
        $allYears = [];
        foreach ($allTransactions as $id => $data) {
            /** @var \Kotka\Triple\HRATransaction $data */
            $owner = $data->getMZOwner();
            if (!isset($results[$owner])) {
                $results[$owner] = [];
            }
            $year = $data->getHRATransactionRequestReceived();
            $type = $data->getHRATransactionType();
            if ($year instanceof \DateTime) {
                $year = $year->format('Y');
            }
            if (!isset($allYears[$year])) {
                $allYears[$year] = true;
            }
            if (!isset($results[$owner][$year])) {
                $results[$owner][$year]['total'] = 0;
                $results[$owner][$year . '-loan']['total'] = 0;
            }
            if (!isset($results[$owner][$year][$type])) {
                $results[$owner][$year][$type] = 0;
                $results[$owner][$year . '-loan'][$type] = 0;
            }
            $total = 0;
            $total += $data->getHRBAwayOther();
            $total += $data->getHRBDamagedOther();
            $total += $data->getHRBMissingOther();
            $total += $data->getHRBReturnedOther();
            $total += count($data->getHRBAway());
            $total += count($data->getHRBDamaged());
            $total += count($data->getHRBMissing());
            $total += count($data->getHRBReturned());
            $results[$owner][$year][$type] += $total;
            $results[$owner][$year]['total'] += $total;
            $results[$owner][$year . '-loan'][$type] += 1;
            $results[$owner][$year . '-loan']['total'] += 1;
        }
        $years = array_keys($allYears);
        asort($years);
        return new ViewModel(['years' => $years, 'results' => $results, 'types' => $this->transactionTypes]);
    }
}
