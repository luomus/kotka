<?php

namespace Kotka\Controller;

use Kotka\Enum\Document as DocumentEnum;
use Kotka\Enum\Document;
use Kotka\Http\HttpClientTrait;
use Kotka\Service\DocumentAccess;
use Common\Service\IdService;
use Triplestore\Service\ObjectManager;
use Triplestore\Service\PhpVariableConverter;
use Triplestore\Stdlib\ModelTools;
use Zend\Filter\FilterPluginManager;
use Zend\Filter\StaticFilter;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Zend\Http\Client;

/**
 * Class ViewController
 * This is responsible for the view
 *
 * @package Kotka\Controller
 */
class ViewController extends AbstractActionController
{
    use HttpClientTrait;

    const FORMAT_JSON = 'json';
    const FORMAT_NO_LAYOUT = 'noLayout';

    protected $typesWithView = [
        'MY.collection',
        DocumentEnum::TYPE,
        'MY.gathering',
        'MY.unit',
        'MY.identification',
        'MY.typeSpecimen',
        'MA.person',
        'GX.dataset',
        'MOS.organization'
    ];

    /**
     * NOTE!: Do not return 404 status
     *
     * @return JsonModel|ViewModel
     */
    public function indexAction()
    {
        $getData = $this->getRequest()->getQuery()->toArray();
        $format = isset($getData['format']) ? $getData['format'] : null;
        $viewModel = $this->getViewModel($format);
        $variables = ['status' => '404'];
        if (isset($getData['uri'])) {
            $variables = $this->getItemVariables($getData, $format);
        }
        $viewModel->setVariables($variables);

        return $viewModel;
    }

    private function getItemVariables($getData, $format)
    {
        $variables = [];
        /** @var \Triplestore\Service\ObjectManager $om */
        $om = $this->serviceLocator->get('Triplestore\ObjectManager');
        $om->disableHydrator();
        $model = $this->getModel($getData['uri'], $om);
        $om->enableHydrator();
        $type = $model === null ? '' : ModelTools::getType($model);
        if (!in_array($type, $this->typesWithView)) {
            $variables['status'] = '404';
            $this->getResponse()->setStatusCode(404);
            return $variables;
        }

        $variables = ModelTools::ModelToArray($model, $om->getMetadataService(), $format);
        $uri = IdService::getUri($variables['qname']);
        $restrictionMode = $this->getPublicityRestriction($uri, $model, $type);
        $variables = $this->filter($this->setPublicity($variables, $restrictionMode), $type, $restrictionMode);
        $variables = ['uri' => $uri] + $variables;

        return $variables;
    }

    private function getPublicityRestriction($url, $model, $type) {
        $mode = DocumentAccess::getPublicityRestriction($model);
        $user = $this->getServiceLocator()->get('user');
        if ($mode === DocumentAccess::MODE_PUBLIC && $type === 'MY.document' && !DocumentAccess::canView($model, $user, true)) {
            $externals = $this->getServiceLocator()->get('Kotka\Options\ExternalsOptions');
            $apiLaji = $externals->getApiLaji();
            $response = $this->getHttpClient()
                ->setUri($apiLaji->getUrl() . 'warehouse/query/document/aggregate?aggregateBy=document.secured&documentId=' . $url)
                ->setHeaders(['content-type' => 'application/json', 'Authorization' => $apiLaji->getPass()])
                ->send();
            if ($response->isOk()) {
                $content = json_decode($response->getBody(), true);
                if ($content && isset($content['results']) && is_array($content['results']) && count($content['results']) === 1) {
                    $by = $content['results'][0];
                    if (
                        isset($by['aggregateBy']) &&
                        isset($by['aggregateBy']['document.secured']) &&
                        $by['aggregateBy']['document.secured'] === 'false'
                    ) {
                        return $mode;
                    }
                }
            }
            return DocumentAccess::MODE_PRIVATE;
        }
        return $mode;
    }

    private function setPublicity($data, $mode) {
        if ($mode === DocumentAccess::MODE_PUBLIC) {
            return $data;
        }
        $data[Document::PREDICATE_PUBLICITY_RESTRICTION] = Document::PUBLICITY_PRIVATE;
        if (isset($data['has'])) {
            foreach ($data['has'] as $key => $gathering) {
                $data['has'][$key] = $this->setPublicity($gathering, $mode);
            }
        }
        return $data;
    }

    private function getModel($id, ObjectManager $om) {
        if (strpos($id, 'http') === 0 || strpos($id,':') !== false) {
            return $om->fetch($id);
        }
        $subjects = [
            IdService::QNAME_PREFIX_HERBO . $id,
            IdService::QNAME_PREFIX_LUOMUS . $id,
            IdService::QNAME_PREFIX_UTU . $id,
            IdService::QNAME_PREFIX_ZMUO . $id,
            IdService::QNAME_PREFIX_TUN . $id
        ];
        $models = $om->fetch($subjects);
        if (empty($models) || !is_array($models)) {
            return null;
        }
        return array_pop($models);
    }

    private function filter($data, $type, $mode)
    {
        $filterName = PhpVariableConverter::toPhpClassName($type);
        $class = '\Kotka\Filter\View\\' . $filterName;
        /** @var FilterPluginManager $filterManager */
        $filterManager = $this->getServiceLocator()->get('filterManager');
        if (class_exists($class)) {
            $filterManager->setInvokableClass($class, $class);
            StaticFilter::setPluginManager($filterManager);
            return StaticFilter::execute($data, $class, ['mode' => $mode]);
        }
        return $data;
    }

    private function getViewModel($format)
    {
        if ($format == self::FORMAT_JSON) {
            return new JsonModel();
        }
        $view = new ViewModel();
        if ($format == self::FORMAT_NO_LAYOUT) {
            $view->setTerminal(true);
            $view->setVariable('compact', true);
        }
        return $view;
    }

    public function getHttpClient() {
        if ($this->httpClient === null) {
            $this->httpClient = new Client(null, $this->httpClientConfig);
        }
        return $this->httpClient;
    }
}
