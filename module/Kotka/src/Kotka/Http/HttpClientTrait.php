<?php
namespace Kotka\Http;


use Zend\Http\Client;

trait HttpClientTrait
{
    /** @var Client */
    protected $httpClient;
    protected $httpClientConfig = [
        'adapter' => 'Zend\Http\Client\Adapter\Curl',
        'sslverifypeer' => false,
        'timeout' => 30
    ];

    public function getHttpClient() {
        if ($this->httpClient === null) {
            $this->httpClient = new Client(null, $this->httpClientConfig);
        }
        return $this->httpClient;
    }

    public function setHttpClient(Client $client = null) {
        $this->httpClient = $client;
    }

    public function getHttpClientConfig()
    {
        return $this->httpClientConfig;
    }

    public function setHttpClientConfig(array $config)
    {
        if ($this->httpClient !== null) {
            throw new \Exception("Configs can't be set after initializing the http client ");
        }
        $this->httpClientConfig = $config;
    }

}