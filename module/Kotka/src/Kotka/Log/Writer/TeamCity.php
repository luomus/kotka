<?php

namespace Kotka\Log\Writer;

use Zend\Escaper\Escaper;
use Zend\Http\Client;
use Zend\Http\Request;
use Zend\Log\Formatter\Simple as SimpleFormatter;
use Zend\Log\Writer\AbstractWriter;

class TeamCity extends AbstractWriter
{

    private static $send = false;
    private $escaper;
    private $url;
    private $user;
    private $pass;
    private $buildId;

    public function __construct(array $config, $options = null)
    {
        $this->url = isset($config['url']) ? $config['url'] : null;
        $this->user = isset($config['user']) ? $config['user'] : null;
        $this->pass = isset($config['pass']) ? $config['pass'] : null;
        $this->buildId = isset($config['buildId']) ? $config['buildId'] : null;
        parent::__construct($options);
    }


    /**
     * Set Escaper instance
     *
     * @param  Escaper $escaper
     * @return TeamCity
     */
    public function setEscaper(Escaper $escaper)
    {
        $this->escaper = $escaper;
        return $this;
    }

    /**
     * Get Escaper instance
     *
     * Lazy-loads an instance with the current encoding if none registered.
     *
     * @return Escaper
     */
    public function getEscaper()
    {
        if (null === $this->escaper) {
            $this->setEscaper(new Escaper('utf-8'));
        }
        return $this->escaper;
    }

    public function getFormatter()
    {
        if ($this->formatter === null) {
            $this->setFormatter(new SimpleFormatter('%message%'));
        }
        return $this->formatter;
    }

    /**
     * Write a message to the log
     *
     * @param array $event log data event
     * @return void
     */
    protected function doWrite(array $event)
    {
        if (self::$send) {
            return;
        }
        // Not using teamcity anymore
        // if (KOTKA_ENV !== 'production' || php_sapi_name() == 'cli') {
            return;
        // }
        try {
            $client = new Client();
            $client->setAdapter('Zend\Http\Client\Adapter\Curl');
            $uri = $this->url . '/httpAuth/app/rest/buildQueue';
            $client->setUri($uri);
            $client->setOptions(array(
                'maxredirects' => 0,
                'timeout' => 3
            ));
            $client->setAuth($this->user, $this->pass, Client::AUTH_BASIC);
            $client->setHeaders(array('Content-Type' => 'application/xml'));
            $escaper = $this->getEscaper();
            $description = $this->getFormatter()->format($event);
            $description = $escaper->escapeHtmlAttr($description);
            $xml = '<build><buildType id="' . $this->buildId . '"/><properties><property name="env.errorTrace" value="' . $description . '"/></properties></build>';
            $client->setRawBody($xml);
            $client->setMethod(Request::METHOD_POST);
            $client->send();
        } catch (\Exception $e) {
        }
        self::$send = true;
    }
}