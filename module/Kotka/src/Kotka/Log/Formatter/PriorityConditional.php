<?php

namespace Kotka\Log\Formatter;

use Zend\Log\Exception;
use Traversable;
use Zend\Log\Formatter\Base;
use Zend\Log\Logger;


class PriorityConditional extends Base
{
    const DEFAULT_FORMAT = '%message%';
    const DEFAULT_KEY = 'default';

    /**
     * Format specifier for log messages
     *
     * @var array
     */
    protected $format = [
        Logger::CRIT => '<div class="alert alert-danger">%message%</div>',
        Logger::ERR  => '<div class="error">%message%<spcn class="fail">failed</spcn> <small>(%timestamp%)</small></div>',
        Logger::INFO => '<div class="success">%message%<spcn class="success">succeeded</spcn> <small>(%timestamp%)</small></div>',
        self::DEFAULT_KEY => self::DEFAULT_FORMAT,
    ];

    /**
     * Class constructor
     *
     * @see http://php.net/manual/en/function.date.php
     * @param null|string $format Format specifier for log messages
     * @param null|string $dateTimeFormat Format specifier for DateTime objects in event data
     * @throws Exception\InvalidArgumentException
     */
    public function __construct($format = null, $dateTimeFormat = null)
    {
        if ($format instanceof Traversable) {
            $format = iterator_to_array($format);
        }

        if (is_array($format)) {
            $dateTimeFormat = isset($format['dateTimeFormat'])? $format['dateTimeFormat'] : null;
            $format         = isset($format['format'])? $format['format'] : $format;
        }

        if (isset($format) && !is_array($format)) {
            throw new Exception\InvalidArgumentException('Format must be an array');
        }

        if (isset($format)) {
            $this->format = $format;
        }


        parent::__construct($dateTimeFormat);
    }

    /**
     * Formats data into a single line to be written by the writer.
     *
     * @param array $event event data
     * @return string formatted line to write to the log
     */
    public function format($event)
    {
        $priority = isset($event['priority']) ? $event['priority'] : '';
        if (isset($this->format[$priority])) {
            $output = $this->format[$priority];
        } else {
            $output = isset($this->format[self::DEFAULT_KEY]) ? $this->format[self::DEFAULT_KEY] : self::DEFAULT_FORMAT;
        }

        $event = parent::format($event);
        foreach ($event as $name => $value) {
            if ('extra' == $name && count($value)) {
                $value = $this->normalize($value);
            } elseif ('extra' == $name) {
                // Don't print an empty array
                $value = '';
            }
            $output = str_replace("%$name%", $value, $output);
        }

        return $output;
    }
}