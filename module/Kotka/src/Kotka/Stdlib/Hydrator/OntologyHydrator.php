<?php

namespace Kotka\Stdlib\Hydrator;

use Triplestore\Stdlib\AbstractOntology;
use Zend\Stdlib\Exception;
use Zend\Stdlib\Hydrator\AbstractHydrator;
use Zend\Stdlib\Hydrator\Strategy\StrategyInterface;

class OntologyHydrator extends AbstractHydrator
{

    private $levelHydrator;

    public function __construct()
    {
        $this->levelHydrator = new Reflection();
    }

    public function addStrategy($name, StrategyInterface $strategy)
    {
        $this->levelHydrator->addStrategy($name, $strategy);
    }

    /**
     * Extract values from an object
     *
     * @param  object $object
     * @return array
     */
    public function extract($object)
    {
        if (!$object instanceof AbstractOntology) {
            return array();
        }
        $children = $object->getChildren();
        $overWrite = array();
        foreach ($children as $child) {
            $method = 'get' . $child;
            if (method_exists($object, $method)) {
                $childVal = $object->$method();
                if (!is_array($childVal)) {
                    $overWrite[$child] = $this->extract($childVal);
                } else {
                    foreach ($childVal as $key => $value) {
                        $result = $this->extract($value);
                        if (count($result) > 0) {
                            $overWrite[$child][$key] = $result;
                        }
                    }
                }
            }
        }
        $result = $this->levelHydrator->extract($object);
        foreach ($overWrite as $key => $value) {
            $result[$key] = $value;
        }

        return $result;
    }

    /**
     * Hydrate $object with the provided $data.
     *
     * @param array $data
     * @param object $object
     * @return object
     * @throws \Exception
     */
    public function hydrate(array $data, $object)
    {
        throw new \Exception("Use form to hydrate model and not this!");
    }

}
