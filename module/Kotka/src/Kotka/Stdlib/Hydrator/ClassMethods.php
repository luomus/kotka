<?php
namespace Kotka\Stdlib\Hydrator;

use Triplestore\Stdlib\OntologyInterface;
use Zend\Stdlib\Exception;
use Zend\Stdlib\Hydrator\AbstractHydrator;

class ClassMethods extends AbstractHydrator
{
    /**
     * These are the languages that this hydrator can extract
     * @var array
     */
    protected $supportedLang = array(
        'En',
        'Fi',
        'Sv'
    );

    /** TODO add object model here so that the metadata class can be used to ask if field is resource or not */
    private $skipLang = array(
        'MYGathering' => true,
        'MYUnit' => true,
        'MYIdentification' => true,
        'MYTypeSpecimen' => true,
        'MFSample' => true,
        'HRBSpecimen' => true,
    );


    /**
     * Extract values from an object with class methods
     *
     * Extracts the getter/setter of the given $object.
     *
     * @param  object $object
     * @return array
     * @throws Exception\BadMethodCallException for a non-object $object
     */
    public function extract($object)
    {

        if (!$object instanceof OntologyInterface) {
            throw new Exception\BadMethodCallException(sprintf(
                '%s expects the provided $object to be a PHP object)', __METHOD__
            ));
        }

        $attributes = array();
        $methods = get_class_methods($object);

        foreach ($methods as $method) {
            if (strpos($method, 'get') === false) {
                continue;
            }
            $attribute = substr($method, 3);
            $attributes[$attribute] = $this->extractValue($attribute, $object->$method(), $object);
            if (isset($this->skipLang[$attribute])) {
                continue;
            }
            foreach ($this->supportedLang as $lang) {
                $langAttr = $attribute . $lang;
                $attributes[$langAttr] = $this->extractValue($langAttr, $object->$method(lcfirst($lang)), $object);
            }
        }

        return $attributes;
    }

    /**
     * Hydrate an object by populating getter/setter methods
     *
     * Hydrates an object by getter/setter methods of the object.
     *
     * @param  array $data
     * @param  object $object
     * @return object
     * @throws Exception\BadMethodCallException for a non-object $object
     */
    public function hydrate(array $data, $object)
    {
        if (!$object instanceof OntologyInterface) {
            throw new Exception\BadMethodCallException(sprintf(
                '%s expects the provided $object to be a PHP object)', __METHOD__
            ));
        }

        foreach ($data as $property => $value) {
            $method = 'set' . ucfirst($property);
            if (is_callable(array($object, $method))) {
                $value = $this->hydrateValue($property, $value, $data);
                $object->$method($value);
            }
        }

        return $object;
    }
}
