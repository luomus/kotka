<?php
namespace Kotka\Stdlib\Hydrator;

use Triplestore\Stdlib\OntologyInterface;
use Zend\Stdlib\Exception;
use Zend\Stdlib\Hydrator\AbstractHydrator;

class ClassNamespaceMethods extends AbstractHydrator
{

    protected $namespace;

    protected $skip = [];

    /**
     * @return string
     */
    public function getNamespace()
    {
        return $this->namespace;
    }

    /**
     * @param string $namespace
     */
    public function setNamespace($namespace)
    {
        $this->namespace = $namespace;
    }

    /**
     * @return array
     */
    public function getSkip()
    {
        return array_keys($this->skip);
    }

    /**
     * @param array $skip
     */
    public function setSkip($skip)
    {
        if (!is_array($skip)) {
            $skip = [$skip];
        }
        $this->skip = array_flip($skip);
    }

    /**
     * Extract values from an object with class methods
     *
     * Extracts the getter/setter of the given $object.
     *
     * @param  object $object
     * @return array
     * @throws Exception\BadMethodCallException
     */
    public function extract($object)
    {
        $ns = $this->namespace;
        if (!$object instanceof OntologyInterface) {
            throw new Exception\BadMethodCallException(sprintf(
                '%s expects the provided $object to be a PHP object)', __METHOD__
            ));
        }

        $attributes = array();
        $methods = get_class_methods($object);

        foreach ($methods as $method) {
            if (strpos($method, 'get') === false) {
                continue;
            }
            $attribute = substr($method, 3);
            if (strpos($attribute, $ns) === 0) {
                $attribute = substr($attribute, strlen($ns));
            }
            $attributes[$attribute] = $this->extractValue($attribute, $object->$method(), $object);
        }

        return $attributes;
    }

    /**
     * Hydrate an object by populating getter/setter methods
     *
     * Hydrates an object by getter/setter methods of the object.
     *
     * @param  array $data
     * @param  object $object
     * @return object
     * @throws Exception\BadMethodCallException for a non-object $object
     */
    public function hydrate(array $data, $object)
    {
        $ns = $this->namespace;
        if (!$object instanceof OntologyInterface) {
            throw new Exception\BadMethodCallException(sprintf(
                '%s expects the provided $object to be a PHP object)', __METHOD__
            ));
        }

        foreach ($data as $property => $value) {
            if (isset($this->skip[$property])) {
                continue;
            }
            $method = 'set' . ucfirst($property);
            if (!method_exists($object, $method)) {
                $method = 'set' . $ns . ucfirst($property);
            }
            if (is_callable(array($object, $method))) {
                $value = $this->hydrateValue($property, $value, $data);
                $object->$method($value);
            }
        }

        return $object;
    }
}
