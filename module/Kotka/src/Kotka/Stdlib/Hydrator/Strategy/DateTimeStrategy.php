<?php

namespace Kotka\Stdlib\Hydrator\Strategy;


use DateTime;
use Zend\Stdlib\Hydrator\HydratorOptionsInterface;
use Zend\Stdlib\Hydrator\Strategy\StrategyInterface;

class DateTimeStrategy implements StrategyInterface, HydratorOptionsInterface
{

    private $format;

    /**
     * Returns the datetime format string
     *
     * @return string
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * Sets the datetime format
     *
     * @param string $format
     */
    public function setFormat($format)
    {
        $this->format = $format;
    }


    /**
     * Converts the given value so that it can be extracted by the hydrator.
     *
     * @param mixed $value The original value.
     * @return mixed Returns the value that should be extracted.
     */
    public function extract($value)
    {
        if ($value instanceof DateTime) {
            $value = $value->format($this->format);
        }
        return $value;
    }

    /**
     * Converts the given value so that it can be hydrated by the hydrator.
     *
     * @param mixed $value The original value.
     * @return mixed Returns the value that should be hydrated.
     */
    public function hydrate($value)
    {
        if ($value == null || $value == '') {
            return null;
        }
        $format = $this->format;
        if (is_int($value)
            || (is_string($value) && null !== $format)
        ) {
            return (is_int($value))
                ? date_create("@$value") // from timestamp
                : DateTime::createFromFormat($format, $value);

        } else if ($value instanceof DateTime) {
            return $value;
        }
        return null;
    }

    /**
     * @param  array|\Traversable $options
     * @return HydratorOptionsInterface
     */
    public function setOptions($options)
    {
        if (isset($options['format'])) {
            $this->format = $options['format'];
        }
    }
}