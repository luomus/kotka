<?php

namespace Kotka\Stdlib;


class StrUtils
{
    public static function isEmpty($value)
    {
        return ($value === '' || $value === null);
    }

}