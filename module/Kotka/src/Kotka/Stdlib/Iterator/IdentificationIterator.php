<?php
namespace Kotka\Stdlib\Iterator;


class IdentificationIterator implements \Iterator
{

    private $data = [];
    private $key = 0;
    private $skippedRecordTypes = [];

    /**
     * @return array
     */
    public function getSkippedRecordTypes()
    {
        return $this->skippedRecordTypes;
    }

    /**
     * @param array $skippedRecordTypes
     */
    public function setSkippedRecordTypes($skippedRecordTypes)
    {
        $this->skippedRecordTypes = $skippedRecordTypes;
    }

    public function init($data, $onlyAccepted = true)
    {
        $this->data = [];
        $this->key = 0;
        if (isset($data['MYGathering'])) {
            $gatherings = $data['MYGathering'];
            $specimen = $data;
            foreach ($gatherings as $gathering) {
                if (isset($gathering['MYUnit'])) {
                    $units = $gathering['MYUnit'];
                    foreach ($units as $unit) {
                        if (isset($unit['MYRecordBasis'])
                            && in_array($unit['MYRecordBasis'], $this->skippedRecordTypes)
                        ) {
                            continue;
                        }
                        if (isset($unit['MYTypeSpecimen'])) {
                            $types = $unit['MYTypeSpecimen'];
                        }
                        $spot = -1;
                        if (isset($unit['MYIdentification'])) {
                            $identifications = $unit['MYIdentification'];
                            foreach ($identifications as $identification) {
                                $spot++;
                                $type = isset($types[$spot]) ? $types[$spot] : null;
                                $this->addRow($specimen, $gathering, $unit, $identification, $type);
                                if ($onlyAccepted) {
                                    break;
                                }
                            }
                        }
                        while (!$onlyAccepted && isset($types[++$spot])) {
                            $this->addRow($specimen, $gathering, $unit, null, $types[$spot]);
                        }
                    }
                }
            }
        }
    }

    private function addRow($specimen, $gathering, $unit, $identification, $type)
    {
        $this->data[] = [
            $specimen,
            $gathering,
            $unit,
            $identification,
            $type
        ];
    }


    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Return the current element
     * @link http://php.net/manual/en/iterator.current.php
     * @return mixed Can return any type.
     */
    public function current()
    {
        return $this->data[$this->key];
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Move forward to next element
     * @link http://php.net/manual/en/iterator.next.php
     * @return void Any returned value is ignored.
     */
    public function next()
    {
        $this->key++;
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Return the key of the current element
     * @link http://php.net/manual/en/iterator.key.php
     * @return mixed scalar on success, or null on failure.
     */
    public function key()
    {
        return $this->key;
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Checks if current position is valid
     * @link http://php.net/manual/en/iterator.valid.php
     * @return boolean The return value will be casted to boolean and then evaluated.
     * Returns true on success or false on failure.
     */
    public function valid()
    {
        return isset($this->data[$this->key]);
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Rewind the Iterator to the first element
     * @link http://php.net/manual/en/iterator.rewind.php
     * @return void Any returned value is ignored.
     */
    public function rewind()
    {
        $this->key = 0;
    }
}