<?php

namespace Kotka\Stdlib;

class ArrayUtils
{

    public static function array_change_key_case_unicode($arr, $c = CASE_LOWER)
    {
        $c = ($c == CASE_LOWER) ? MB_CASE_LOWER : MB_CASE_UPPER;
        $ret = [];
        foreach ($arr as $k => $v) {
            $ret[mb_convert_case($k, $c, "UTF-8")] = $v;
        }
        return $ret;
    }

    public static function flatten_with_keys($array, $prefix = '', $addBracts = false)
    {
        $result = array();
        foreach ($array as $key => $value) {
            if ($addBracts) {
                $key = '[' . $key . ']';
            }
            if (is_array($value)) {
                $result = $result + self::flatten_with_keys($value, $prefix . $key . '', true);
            } else {
                $result[$prefix . $key] = $value;
            }
        }
        return $result;
    }

    public static function array_diff_key_recursive($a1, $a2)
    {
        $r = array();
        foreach ($a1 as $k => $v) {
            if (is_array($v)) {
                if (isset($a2[$k])) {
                    $r[$k] = self::array_diff_key_recursive($a1[$k], $a2[$k]);
                } else {
                    $r[$k] = $a1[$k];
                }

            } else {
                $r = array_diff_key($a1, $a2);
            }
            if (isset($r[$k]) && is_array($r[$k]) && count($r[$k]) == 0) {
                unset($r[$k]);
            }
        }
        return $r;
    }

    public static function array_remove_null($haystack)
    {
        foreach ($haystack as $key => $value) {
            if (is_array($value)) {
                $haystack[$key] = self::array_remove_null($haystack[$key]);
            } else if (is_null($haystack[$key])) {
                unset($haystack[$key]);
            }
        }

        return $haystack;
    }

    public static function is_array_empty($inputVariable)
    {
        $result = true;
        if (is_array($inputVariable) && count($inputVariable) > 0) {
            foreach ($inputVariable as $value) {
                $result = $result && self::is_array_empty($value);
            }
        } else {
            $result = empty($inputVariable);
        }

        return $result;
    }

    public static function mergeMessages(&$item, $messages, $text, $color = null)
    {
        $ret = array();
        $textWrap = '%s';
        if ($color !== null) {
            $textWrap = '<span class="import-error" style="color:' . $color . '">%s</span>';
        }
        foreach ($messages as $spot => $value) {
            if (!isset($item[$spot]) || $spot === 'subject' || !is_array($item)) {
                $ret[$spot] = $value;
                continue;
            }
            if (!is_array($value)) {
                $item .= sprintf($textWrap, $text . $value);
                continue;
            }
            $nextKey = key($value);
            if ($nextKey === 'subject') {
                $ret[$spot] = $value;
                continue;
            }
            if (!isset($item[$spot][$nextKey])) {
                $msgs = array();
                $break = false;
                foreach ($value as $msg) {
                    if (is_array($msg)) {
                        $ret[$spot] = array();
                        $ret[$spot][$nextKey] = $msg;
                        $break = true;
                    }
                    $msgs[] = $msg;
                }
                if ($break) {
                    continue;
                }
                if (isset($item[$spot]) && is_string($item[$spot])) {
                    $item[$spot] .= sprintf($textWrap, $text . implode('<br>', $msgs));
                    continue;
                } else if (isset($item[$spot][0]) && is_string($item[$spot][0])) {
                    $item[$spot][0] .= sprintf($textWrap, $text . implode('<br>', $msgs));
                    continue;
                }
            }
            $ret[$spot] = self::mergeMessages($item[$spot], $messages[$spot], $text, $color);
        }

        return $ret;
    }

    public static function xml_to_array($root)
    {
        $result = array();

        if ($root->hasAttributes()) {
            $attrs = $root->attributes;
            foreach ($attrs as $attr) {
                $result['@attributes'][$attr->name] = $attr->value;
            }
        }

        if ($root->hasChildNodes()) {
            $children = $root->childNodes;
            if ($children->length == 1) {
                $child = $children->item(0);
                if ($child->nodeType == XML_TEXT_NODE) {
                    $result['_value'] = $child->nodeValue;
                    return count($result) == 1
                        ? $result['_value']
                        : $result;
                }
            }
            $groups = array();
            foreach ($children as $child) {
                if (!isset($result[$child->nodeName])) {
                    $result[$child->nodeName] = self::xml_to_array($child);
                } else {
                    if (!isset($groups[$child->nodeName])) {
                        $result[$child->nodeName] = array($result[$child->nodeName]);
                        $groups[$child->nodeName] = 1;
                    }
                    $result[$child->nodeName][] = self::xml_to_array($child);
                }
            }
        }

        return $result;
    }

} 