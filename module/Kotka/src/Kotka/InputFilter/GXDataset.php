<?php

namespace Kotka\InputFilter;


class GXDataset extends TripleStoreInputFilter
{
    protected $skip = [
        'GXDatasetNameFi',
        'GXDatasetNameSv',
        'GXDatasetName',
    ];

    public function init()
    {
        $this->add(array(
            'name' => 'MZOwner',
            'required' => true,
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'options' => array(
                        'messages' => array(
                            \Zend\Validator\NotEmpty::IS_EMPTY => 'You have to specify the owner of the record',
                        ),
                    ),
                    'break_chain_on_failure' => true
                ),
                array(
                    'name' => 'UserInOrganization',
                )
            )
        ));

        $this->add(array(
            'name' => 'GXDatasetNameEn',
            'filters' => array(
                array(
                    'name' => 'StringTrim',
                )
            ),
            'validators' => array(
                array(
                    'name' => 'UniqueDatasetName',
                ),
                array(
                    'name' => 'NotEmpty',
                    'break_chain_on_failure' => true,
                    'options' => array(
                        'messages' => array(
                            \Zend\Validator\NotEmpty::IS_EMPTY => 'Name must be given.'
                        )
                    )
                ),
            )
        ));
        $this->add(array(
            'name' => 'GXDatasetNameEn',
            'filters' => array(
                array(
                    'name' => 'StringTrim',
                )
            ),
            'validators' => array(
                array(
                    'name' => 'UniqueDatasetName',
                ),
                array(
                    'name' => 'NotEmpty',
                    'break_chain_on_failure' => true,
                    'options' => array(
                        'messages' => array(
                            \Zend\Validator\NotEmpty::IS_EMPTY => 'Name must be given.'
                        )
                    )
                ),
            )
        ));

        $this->add(array(
            'name' => 'GXPersonsResponsible',
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'break_chain_on_failure' => true,
                    'options' => array(
                        'messages' => array(
                            \Zend\Validator\NotEmpty::IS_EMPTY => 'Persons responsible must be given.'
                        )
                    )
                )
            )
        ));

        parent::init();
    }


}