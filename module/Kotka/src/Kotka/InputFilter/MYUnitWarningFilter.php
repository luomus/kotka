<?php
namespace Kotka\InputFilter;

/**
 * Class MYUnitWarningFilter warning filter for the unit
 *
 * @package Kotka\InputFilter
 */
class MYUnitWarningFilter extends WarningFilter
{

    public function __construct()
    {

        $this->add(array(
            'type' => 'Zend\InputFilter\CollectionInputFilter',
            'input_filter' => new MYIdentificationWarningFilter()
        ), 'MYIdentification');
    }

} 