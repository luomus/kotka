<?php

namespace Kotka\InputFilter;


use Zend\Validator\InArray;

class MYIdentification extends TripleStoreInputFilter
{
    public function init()
    {
        $this->add(array(
            'name' => 'MYInfraRank',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'PrefixFilter',
                    'options' => array(
                        'prefix' => 'MY.infraRank'
                    )
                ),
                array(
                    'name' => 'NormalizeDatabaseSelect',
                    'options' => array(
                        'field' => 'MY.infraRank'
                    )
                )
            ),
            'validators' => array(
                array(
                    'name' => 'InDatabaseSelect',
                    'options' => array(
                        'field' => 'MY.infraRank'
                    )
                )
            )
        ));

        $this->add(array(
            'name' => 'MYPreferredIdentification',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'StringToLower'
                )
            ),
            'validators' => array(
                array(
                    'name' => 'InArray',
                    'options' => array(
                        'messages' => [
                            InArray::NOT_IN_ARRAY => "Value should be wither 'yes' or 'no'. Now '%value%' given!"
                        ],
                        'haystack' => ['yes', 'no']
                    )
                )
            )
        ));

        $this->add(array(
            'name' => 'MYTaxonRank',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'PrefixFilter',
                    'options' => array(
                        'prefix' => 'MX.'
                    )
                ),
                array(
                    'name' => 'NormalizeDatabaseSelect',
                    'options' => array(
                        'field' => 'MY.taxonRank'
                    )
                )
            ),
            'validators' => array(
                array(
                    'name' => 'Kotka\Validator\TaxonRank',
                    'options' => array(
                        'field' => 'MY.taxonRank'
                    )
                )
            )
        ));

        $this->add(array(
            'name' => 'MYTaxon',
            'required' => false,
            'validators' => array(
                array(
                    'name' => 'Kotka\Validator\TaxonName'
                )
            )
        ));
        parent::init();
    }
}