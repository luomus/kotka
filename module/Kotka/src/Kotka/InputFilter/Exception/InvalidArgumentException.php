<?php

namespace Kotka\InputFilter\Exception;

/**
 * Class InvalidArgumentException
 *
 * @package Kotka\InputFilter\Exception
 */
class InvalidArgumentException extends \InvalidArgumentException
{
}