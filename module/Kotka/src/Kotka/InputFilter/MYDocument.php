<?php

namespace Kotka\InputFilter;


use Kotka\Enum\Document;
use Kotka\Filter\AssociatedTaxaToUnit;
use Kotka\Filter\ClearEmptyCollections;
use Kotka\Filter\CopyUnreliable;
use Kotka\Filter\DateBegin;
use Kotka\Filter\DefaultQName;
use Kotka\Filter\KeyToPrefix;
use Kotka\Filter\MeasurementToArray;
use Kotka\Filter\MoveTo;
use Kotka\Filter\SortIdentifications;
use Kotka\Filter\SpecimenFilter;
use Kotka\Filter\StringToArray;
use Kotka\Filter\UnreliableFields;
use Kotka\Form\Element\Language;
use Kotka\Model\Util;
use Kotka\Validator\InDatabaseSelect;
use Kotka\Validator\RequireWithThis;
use Zend\Filter\FilterInterface;
use Zend\InputFilter\CollectionInputFilter;
use Zend\Stdlib\ArrayUtils;
use Zend\Validator\Date;
use Zend\Validator\InArray;
use Zend\Validator\Regex;

class MYDocument extends BaseInputFilter
{
    protected static $type = '';
    protected $specimenFilter;

    public function init()
    {
        $this->add(array(
            'name' => 'subject',
            'continue_if_empty' => true,
            'validators' => array(
                array(
                    'name' => 'Kotka\Validator\UniqueUri'
                ),
                array(
                    'name' => 'Kotka\Validator\PreferredIdentification'
                ),
                array(
                    'name' => 'Kotka\Validator\Secondary'
                )
            )
        ));
        $this->add(array(
            'name' => 'MYNamespaceID',
            'required' => false,
            'validators' => array(
                array(
                    'name' => 'Kotka\Validator\AllowedNamespace',
                ),
                array(
                    'name' => 'RequireWithThis',
                    'options' => array(
                        'field' => 'MYObjectID',
                        'messages' => array(
                            RequireWithThis::MISSING => 'If you set Namespace ID, you have to set object ID also.'
                        ),
                    ),
                ),
            )
        ));

        $this->add(array(
            'name' => 'MYObjectID',
            'required' => false,
            'filters' => array(
                array('name' => 'StringTrim')
            ),
            'validators' => array(
                array(
                    'name' => 'StringLength',
                    'options' => array(
                        'max' => 15,
                    )
                ),
                array(
                    'name' => 'Regex',
                    'options' => array(
                        'pattern' => '{^[a-zA-Z0-9-]*$}',
                        'messages' => array(
                            Regex::NOT_MATCH => 'The input can contain only letters, numbers and dashes.'
                        )
                    )
                ),
                array(
                    'name' => 'RequireWithThis',
                    'options' => array(
                        'field' => 'MYNamespaceID',
                        'messages' => array(
                            RequireWithThis::MISSING => 'If you set Object ID, you have to set namespace ID also.'
                        ),
                    ),
                ),
                array(
                    'name' => 'Kotka\Validator\NotSample'
                ),
            )
        ));

        $this->add(array(
            'name' => 'MYCollectionID',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'ToQName'
                )
            ),
            'validators' => array(
                array(
                    'name' => 'InDatabaseSelect',
                    'options' => array(
                        'field' => 'MY.collectionID'
                    )
                )
            )
        ));

        $this->add(array(
            'name' => 'MYDatasetID',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'ForceArray',
                ),
                array(
                    'name' => 'EmptyArray',
                ),
                array(
                    'name' => 'ToQName'
                ),
            ),
            'validators' => array(
                array(
                    'name' => 'InDatabaseSelect',
                    'options' => array(
                        'field' => 'MY.datasetID'
                    )
                )
            )
        ));

        $this->add(array(
            'name' => 'MYAcquiredFromOrganization',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'ToQName'
                ),
            ),
            'validators' => array(
                array(
                    'name' => 'InDatabaseSelect',
                    'options' => array(
                        'field' => 'MY.acquiredFromOrganization'
                    )
                )
            )
        ));

        $this->add(array(
            'name' => 'MYAdditionalIDs',
            'required' => false,
            'filters' => array(
                array('name' => 'ForceArray')
            ),
        ));

        $this->add(array(
            'name' => 'MYPublication',
            'required' => false,
            'filters' => array(
                array('name' => 'ForceArray')
            ),
        ));

        $this->add(array(
            'name' => 'MYBold',
            'required' => false,
            'filters' => array(
                array('name' => 'ForceArray')
            ),
        ));

        $this->add(array(
            'name' => 'MYGenbank',
            'required' => false,
            'filters' => array(
                array('name' => 'ForceArray')
            ),
        ));

        $this->add(array(
            'name' => 'MYProjectId',
            'required' => false,
            'filters' => array(
                array('name' => 'ForceArray')
            ),
        ));

        $this->add(array(
            'name' => 'MYSeparatedTo',
            'required' => false,
            'filters' => array(
                array('name' => 'ForceArray')
            ),
        ));

        $this->add(array(
            'name' => 'MYEvent',
            'required' => false,
            'filters' => array(
                array('name' => 'ForceArray')
            ),
        ));

        $this->add(array(
            'name' => 'MYAcquisitionDate',
            'required' => false,
            'validators' => array(
                array(
                    'name' => 'Kotka\Validator\DateMulti',
                    'options' => array(
                        'format' => ['Y', 'd.m.Y'],
                    )
                )
            )
        ));

        $this->add(array(
            'name' => 'MYStatus',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'PrefixFilter',
                    'options' => array(
                        'prefix' => 'MY.status'
                    )
                ),
                array(
                    'name' => 'NormalizeDatabaseSelect',
                    'options' => array(
                        'field' => 'MY.status'
                    )
                )
            ),
            'validators' => array(
                array(
                    'name' => 'InDatabaseSelect',
                    'options' => array(
                        'field' => 'MY.status'
                    )
                )
            )
        ));

        $this->add(array(
            'name' => 'MYVerificationStatus',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'ForceArray',
                ),
                array(
                    'name' => 'EmptyArray',
                ),
                array(
                    'name' => 'PrefixFilter',
                    'options' => array(
                        'prefix' => 'MY.verificationStatus'
                    )
                ),
                array(
                    'name' => 'NormalizeDatabaseSelect',
                    'options' => array(
                        'field' => 'MY.verificationStatus'
                    )
                )
            ),
            'validators' => array(
                array(
                    'name' => 'InDatabaseSelect',
                    'options' => array(
                        'field' => 'MY.verificationStatus'
                    )
                )
            )
        ));

        $this->add(array(
            'name' => 'MYPreservation',
            'required' => false,
            'filters' => array(
                array('name' => 'ForceArray'),
                array('name' => 'EmptyArray'),
                array(
                    'name' => 'PrefixFilter',
                    'options' => array(
                        'prefix' => 'MY.preservation'
                    )
                ),
                array(
                    'name' => 'NormalizeDatabaseSelect',
                    'options' => array(
                        'field' => 'MY.preservation'
                    )
                )
            ),
            'validators' => array(
                array(
                    'name' => 'InDatabaseSelect',
                    'options' => array(
                        'field' => 'MY.preservation'
                    )
                )
            )
        ));

        $this->add(array(
            'name' => 'MZPublicityRestrictions',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'PrefixFilter',
                    'options' => array(
                        'prefix' => 'MZ.publicityRestrictions'
                    )
                ),
                array(
                    'name' => 'NormalizeDatabaseSelect',
                    'options' => array(
                        'field' => 'MZ.publicityRestrictions'
                    )
                )
            ),
            'validators' => array(
                array(
                    'name' => 'InDatabaseSelect',
                    'options' => array(
                        'field' => 'MZ.publicityRestrictions'
                    )
                )
            )
        ));

        $this->add(array(
            'name' => 'MYEntered',
            'required' => false,
            /*'filters' => array(
                array(
                    'name' => 'ExcelTimeToDateTime',
                    'options' => array(
                        'format' => Util::DATETIME_FORMAT
                    )
                )
            ),*/
            'validators' => array(
                array(
                    'name' => 'Kotka\Validator\DateMulti',
                    'options' => array(
                        'format' => ['Y', Util::DATETIME_FORMAT],
                        'messages' => array(
                            Date::INVALID => 'Entered date is not in correct format'
                        )
                    )
                )
            )
        ));

        $this->add(array(
            'name' => 'MYLanguage',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'StringToLower',
                )
            ),
            'validators' => array(
                array(
                    'name' => 'InArray',
                    'options' => array(
                        'messages' => [
                            InArray::NOT_IN_ARRAY => "Unsupported language '%value%' given!"
                        ],
                        'haystack' => array_keys(Language::getAllLanguages())
                    )
                )
            )
        ));

        $this->add(array(
            'name' => 'MYRelationship',
            'required' => false,
            'validators' => array(
                array(
                    'name' => 'StartsWith',
                    'options' => array(
                        'field' => 'MY.relationshipEnum',
                        'fieldPrefix' => 'MY.relationship'
                    )
                )
            ),
            'filters' => array(
                array('name' => 'ForceArray')
            )
        ));

        $gatheringInputFilter = $this
            ->getServiceLocator()
            ->getServiceLocator()
            ->get('InputFilterManager')
            ->get('Kotka\InputFilter\MYGathering');

        $collectionContainerInputFilter = new CollectionInputFilter();
        $collectionContainerInputFilter->setInputFilter($gatheringInputFilter);
        $this->add($collectionContainerInputFilter, 'MYGathering');

        parent::init();

    }

    public static function clearCache()
    {
        InDatabaseSelect::clearCache();
    }

    /**
     * @return string
     */
    public static function getType()
    {
        return self::$type;
    }

    /**
     * @param string $type
     */
    public static function setType($type)
    {
        self::$type = $type;
    }

    /**
     * Runs the filter specified for the whole data
     *
     * @param $data
     * @return array|mixed
     */
    public function preFilter($data)
    {
        $dataFilter = $this->getSpecimenInputFilter();
        if (!$dataFilter instanceof FilterInterface) {
            return $data;
        }
        if ($data instanceof \Traversable) {
            $data = ArrayUtils::iteratorToArray($data);
        }
        if (!is_array($data)) {
            throw new Exception\InvalidArgumentException(sprintf(
                '%s expects an array or Traversable argument; received "%s"',
                __METHOD__,
                (is_object($data) ? get_class($data) : gettype($data))
            ));
        }
        return $dataFilter->filter($data);
    }

    protected function getSpecimenInputFilter()
    {
        if ($this->specimenFilter === null) {
            $this->specimenFilter = new SpecimenFilter();
            $this->specimenFilter
                ->attach(new MoveTo(['source' => 'MYObjectID', 'target' => 'MYOriginalSpecimenID', 'clear-source' => false, 'condition' => ['source' => 'MYNamespaceID', 'operation' => 'equal', 'value' => 'HA']]), SpecimenFilter::ROOT_LEVEL, SpecimenFilter::ALL_FIELDS, 0)
                ->attach(new MoveTo(['source' => 'MYProjectId', 'target' => 'MYDatasetID']), SpecimenFilter::ROOT_LEVEL, SpecimenFilter::ALL_FIELDS, 0)
                ->attach(new StringToArray(['delimiter' => ';']), 'MYUnit', 'MYRecordParts', 0)
                ->attach(new KeyToPrefix(), SpecimenFilter::ROOT_LEVEL, 'MYRelationship', 0)
                ->attach(new CopyUnreliable(['source' => 'MYDateBegin', 'target' => 'MYDateEnd', 'fewerThanNumber' => 7]), 'MYGathering', SpecimenFilter::ALL_FIELDS, 0)
                ->attach(new StringToArray(['delimiter' => ';']), 'MYMeasurement', SpecimenFilter::ALL_FIELDS, 0)
                ->attach(new StringToArray(['delimiter' => ';']), 'MYGathering', 'MYLeg', 0)
                ->attach(new StringToArray(['delimiter' => ';']), 'MFSample', 'MFDatasetID', 0)
                ->attach(new StringToArray(['delimiter' => ';']), 'MFSample', 'MFPreservation', 0)
                ->attach(new StringToArray(['delimiter' => ';']), 'MFSample', 'MFQualityCheckMethod', 0)
                ->attach(new StringToArray(['delimiter' => ';']), 'MFPreparation', 'MFPreparationMaterials', 0)
                ->attach(new StringToArray(['delimiter' => ';']), 'MFPreparation', 'MFPreparationProcess', 0)
                ->attach(new StringToArray(['delimiter' => ';']), SpecimenFilter::ROOT_LEVEL, 'MYAdditionalIDs', 0)
                ->attach(new StringToArray(['delimiter' => ';']), SpecimenFilter::ROOT_LEVEL, 'MYDatasetID', 0)
                ->attach(new StringToArray(['delimiter' => ';']), SpecimenFilter::ROOT_LEVEL, 'MYPublication', 0)
                ->attach(new SortIdentifications(), SpecimenFilter::ROOT_LEVEL, SpecimenFilter::ALL_FIELDS, 0)
                ->attach(new UnreliableFields(), SpecimenFilter::ROOT_LEVEL, SpecimenFilter::ALL_FIELDS, 1)
                ->attach(new ClearEmptyCollections(), SpecimenFilter::ROOT_LEVEL, SpecimenFilter::ALL_FIELDS, 1)
                ->attach(new AssociatedTaxaToUnit(['delimiter' => ';', 'recordBasis' => 'MY.recordBasisPreservedSpecimen', 'source' => 'MYAssociatedSpecimenTaxa']), 'MYGathering', SpecimenFilter::ALL_FIELDS, 1)
                ->attach(new AssociatedTaxaToUnit(['delimiter' => ';']), 'MYGathering', SpecimenFilter::ALL_FIELDS, 1)
                ->attach(new DateBegin(), 'MYGathering', SpecimenFilter::ALL_FIELDS, 1)
                ->attach(new DefaultQName(), SpecimenFilter::ROOT_LEVEL, Document::FORM_NAMESPACE_ID, 1);
        }
        return $this->specimenFilter;
    }

}