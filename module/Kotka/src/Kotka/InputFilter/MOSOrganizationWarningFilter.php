<?php

namespace Kotka\InputFilter;

use Zend\Validator\Digits;

/**
 * Class MOSOrganizationWarningFilter validates organizations
 *
 * @package Kotka\InputFilter
 */
class MOSOrganizationWarningFilter extends WarningFilter
{
    public function __construct()
    {
        $this->add(array(
            'name' => 'MOSPostOfficeBox',
            'required' => false,
            'validators' => array(
                array(
                    'name' => 'Digits',
                    'options' => array(
                        'messages' => array(
                            Digits::NOT_DIGITS => 'Post office box should be a number (integer).'
                        )
                    )
                )
            )
        ));
    }

}
