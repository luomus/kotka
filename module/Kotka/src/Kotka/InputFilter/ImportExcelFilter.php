<?php

namespace Kotka\InputFilter;

use Kotka\Service\ImportService;
use Zend\InputFilter\InputFilter;
use Zend\Validator;

/**
 * Class ImportExcelFilter validates excel import
 * @package Kotka\InputFilter
 */
class ImportExcelFilter extends InputFilter
{
    public function __construct()
    {
        $this->add(array(
            'name' => 'datatype',
            'required' => true,
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'break_chain_on_failure' => true,
                    'options' => array(
                        'messages' => array(
                            Validator\NotEmpty::IS_EMPTY => 'Please select datatype first.'
                        )
                    )
                )
            )
        ));

        $this->add(array(
            'name' => 'organization',
            'required' => true,
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'break_chain_on_failure' => true,
                    'options' => array(
                        'messages' => array(
                            Validator\NotEmpty::IS_EMPTY => 'Please select owner of the records.'
                        )
                    )
                )
            )
        ));

        $this->add(array(
            'name' => 'import',
            'require' => true,
            'validators' => array(
                array(
                    'name' => 'Zend\Validator\File\UploadFile',
                    'break_chain_on_failure' => true,
                    'options' => array(
                        'messages' => array(
                            Validator\File\UploadFile::FILE_NOT_FOUND => "File does not exist or is bigger than " . ini_get('upload_max_filesize')
                        )
                    )
                ),
                array(
                    'name' => 'Zend\Validator\File\Size',
                    'options' => array(
                        'max' => '10MB',
                        'messages' => array(
                            Validator\File\Size::TOO_BIG => "Maximum allowed size for file is '%max%' but '%size%' detected.<br>Please remove some rows from the file to make it smaller and try again."
                        )
                    )
                )
            ),
            'filters' => array(
                array(
                    'name' => 'filerenameupload',
                    'options' => array(
                        'randomize' => true,
                        'target' => ImportService::IMPORT_DIR,
                        'use_upload_name' => true,
                    )
                ),
                array(
                    'name' => 'Kotka\Filter\File\ConvertToUtf8'
                )
            )
        ));
    }

    /**
     * Returns the formatted size
     *
     * @param  int $size
     * @return string
     */
    protected function toByteString($size)
    {
        $sizes = array('B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
        for ($i = 0; $size >= 1024 && $i < 9; $i++) {
            $size /= 1024;
        }

        return round($size, 2) . $sizes[$i];
    }
}
