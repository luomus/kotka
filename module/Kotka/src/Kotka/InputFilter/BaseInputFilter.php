<?php

namespace Kotka\InputFilter;


class BaseInputFilter extends TripleStoreInputFilter
{

    public function init()
    {
        $this->add(array(
            'name' => 'MZOwner',
            'required' => true,
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'options' => array(
                        'messages' => array(
                            \Zend\Validator\NotEmpty::IS_EMPTY => 'You have to specify the owner of the record',
                        ),
                    ),
                    'break_chain_on_failure' => true
                ),
                array(
                    'name' => 'UserInOrganization',
                )
            )
        ));
    }
}