<?php

namespace Kotka\InputFilter;

use Kotka\Service\ImportService;
use Zend\InputFilter\InputFilter;
use Zend\Validator;

/**
 * Class ImportExcelFilter validates excel import
 * @package Kotka\InputFilter
 */
class GenericLabelWarning extends InputFilter
{
    public function init()
    {
        $this->add(array(
            'name' => 'identifier',
            'required' => false,
            'validators' => array(
                array(
                    'name' => 'Kotka\Validator\UniqueRange'
                )
            )
        ));
    }
}
