<?php

namespace Kotka\InputFilter;


use Zend\InputFilter\CollectionInputFilter;
use Zend\Validator\NotEmpty;

class MFSample extends TripleStoreInputFilter
{
    static $validMaterials = [
        'MF.preparationTypeSkin' => ['MF.materialBodyParts', 'MF.materialLeg', 'MF.materialEntireOrganism', 'MF.materialBirdStudySkin', 'MF.materialWing', 'MF.materialTail', 'MF.materialWingAndTail', 'MF.materialFeather', 'MF.materialOther'],
        'MF.preparationTypeSkeletal'=> ['MF.materialSkull', 'MF.materialShell', 'MF.materialEntireSkeleton', 'MF.materialBones', 'MF.materialSkullAndBones', 'MF.materialAntlers', 'MF.materialTeeth', 'MF.materialOther'],
        'MF.preparationTypeMount' => ['MF.materialHead', 'MF.materialOther'],
        'MF.preparationTypeTissue' => ['MF.materialMuscle', 'MF.materialLiver', 'MF.materialLeaf', 'MF.materialBlood', 'MF.materialLeg', 'MF.materialSkin', 'MF.materialFeather', 'MF.materialHair', 'MF.materialOther'],
        'MF.preparationTypeTissueEcotoxicology' => ['MF.materialFeather', 'MF.materialEggContent', 'MF.materialOther'],
        'MF.preparationTypeLiquid' => ['MF.materialEntireOrganism', 'MF.materialTail', 'MF.materialHead', 'MF.materialBodyParts', 'MF.materialEgg', 'MF.materialOther', 'MF.materialGenitalPreparation'],
        'MF.preparationTypeMicroscopeSlide' => ['MF.materialEntireOrganism','MF.materialAppendages', 'MF.materialGenitalPreparation', 'MF.materialSection', 'MF.materialChromosomes', 'MF.materialHair', 'MF.materialFeather', 'MF.materialOther'],
        'MF.preparationTypeDNAExtract' => ['MF.materialGenomicDNA', 'MF.materialMitochondrialDNA', 'MF.materialChloroplastDNA', 'MF.materialEnvironmentalDNA', 'MF.materialOther'],
        'MF.preparationTypeEgg' => ['MF.materialClutch', 'MF.materialEggshell', 'MF.materialEggshellFragments', 'MF.materialOther'],
        'MF.preparationTypeNest' => ['MF.materialNest', 'MF.materialNestMaterial', 'MF.materialOther'],
  ];

    public function init()
    {

        $inDbEnum = [
            'MFElutionMedium' => 'MF.elutionMedium',
            'MFIndividualsInPreparation' => 'MF.individualsInPreparation',
            'MFQualityCheckMethod' => 'MF.qualityCheckMethod',
            'MFStatus' => 'MY.status',
            'MFQuality' => 'MF.quality',
            'MFPreservation' => 'MY.preservation',
        ];

        foreach ($inDbEnum as $field => $db) {
            $this->add(array(
                'name' => $field,
                'required' => false,
                'filters' => array(
                    array(
                        'name' => 'PrefixFilter',
                        'options' => array(
                            'prefix' => $db
                        )
                    ),
                    array(
                        'name' => 'NormalizeDatabaseSelect',
                        'options' => array(
                            'field' => $db
                        )
                    )
                ),
                'validators' => array(
                    array(
                        'name' => 'InDatabaseSelect',
                        'options' => array(
                            'field' => $db
                        )
                    )
                )
            ));
        }

        $this->add(array(
            'name' => 'MFPreparationType',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'PrefixFilter',
                    'options' => array(
                        'prefix' => 'MF.preparationType'
                    )
                ),
                array(
                    'name' => 'NormalizeDatabaseSelect',
                    'options' => array(
                        'field' => 'MF.preparationType'
                    )
                )
            ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'break_chain_on_failure' => true,
                    'options' => array(
                        'messages' => array(
                            NotEmpty::IS_EMPTY => 'Field preparationType can\'t be empty.'
                        )
                    )
                ),
                array(
                    'name' => 'InDatabaseSelect',
                    'options' => array(
                        'field' => 'MF.preparationType'
                    )
                )
            )
        ));

        $this->add(array(
            'name' => 'MFMaterial',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'PrefixFilter',
                    'options' => array(
                        'prefix' => 'MF.material'
                    )
                ),
                array(
                    'name' => 'NormalizeDatabaseSelect',
                    'options' => array(
                        'field' => 'MF.material'
                    )
                )
            ),
            'validators' => array(
                array(
                    'name' => 'ConditionalList',
                    'options' => array(
                        'field' => 'MFPreparationType',
                        'fieldValuePrefix' => 'MF.preparationType',
                        'allowedValues' => self::$validMaterials
                    )
                )
            )
        ));

        $this->add(array(
            'name' => 'MFCollectionID',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'ToQName'
                )
            ),
            'validators' => array(
                array(
                    'name' => 'InDatabaseSelect',
                    'options' => array(
                        'field' => 'MY.collectionID'
                    )
                )
            )
        ));

        $this->add(array(
            'name' => 'MFDatasetID',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'ForceArray',
                ),
                array(
                    'name' => 'EmptyArray',
                ),
                array(
                    'name' => 'ToQName'
                ),
            ),
            'validators' => array(
                array(
                    'name' => 'InDatabaseSelect',
                    'options' => array(
                        'field' => 'MY.datasetID'
                    )
                )
            )
        ));

        $subFilter = $this
            ->getServiceLocator()
            ->getServiceLocator()
            ->get('InputFilterManager')
            ->get('Kotka\InputFilter\MYMeasurement');
       // $this->add($subFilter, 'MYMeasurement');

        $subFilter = $this
            ->getServiceLocator()
            ->getServiceLocator()
            ->get('InputFilterManager')
            ->get('Kotka\InputFilter\MFPreparation');
        $subCollectionInputFilter = new CollectionInputFilter();
        $subCollectionInputFilter->setInputFilter($subFilter);
        $this->add($subCollectionInputFilter, 'MFPreparation');

        parent::init();
    }
}