<?php

namespace Kotka\InputFilter;


use Zend\InputFilter\CollectionInputFilter;
use Zend\Validator\NotEmpty;

class MYUnit extends TripleStoreInputFilter
{
    public function init()
    {
        $this->add(array(
            'name' => 'MYRecordBasis',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'PrefixFilter',
                    'options' => array(
                        'prefix' => 'MY.recordBasis'
                    )
                ),
                array(
                    'name' => 'NormalizeDatabaseSelect',
                    'options' => array(
                        'field' => 'MY.recordBasis'
                    )
                )
            ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'break_chain_on_failure' => true,
                    'options' => array(
                        'messages' => array(
                            NotEmpty::IS_EMPTY => 'Field recordType can\'t be empty.'
                        )
                    )
                ),
                array(
                    'name' => 'InDatabaseSelect',
                    'options' => array(
                        'field' => 'MY.recordBasis'
                    )
                ),
            )
        ));

        $this->add(array(
            'name' => 'MYRecordParts',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'PrefixFilter',
                    'options' => array(
                        'prefix' => 'MY.recordParts'
                    )
                ),
                array(
                    'name' => 'NormalizeDatabaseSelect',
                    'options' => array(
                        'field' => 'MY.recordParts'
                    )
                )
            ),
            'validators' => array(
                array(
                    'name' => 'InDatabaseSelect',
                    'options' => array(
                        'field' => 'MY.recordParts'
                    )
                ),
            )
        ));

        $this->add(array(
            'name' => 'MYProvenance',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'PrefixFilter',
                    'options' => array(
                        'prefix' => 'MY.provenance'
                    )
                ),
                array(
                    'name' => 'NormalizeDatabaseSelect',
                    'options' => array(
                        'field' => 'MY.provenance'
                    )
                )
            ),
            'validators' => array(
                array(
                    'name' => 'InDatabaseSelect',
                    'options' => array(
                        'field' => 'MY.provenance'
                    )
                ),
            )
        ));

        $this->add(array(
            'name' => 'MYFruitType',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'PrefixFilter',
                    'options' => array(
                        'prefix' => 'MY.fruitType'
                    )
                ),
                array(
                    'name' => 'NormalizeDatabaseSelect',
                    'options' => array(
                        'field' => 'MY.fruitType'
                    )
                )
            ),
            'validators' => array(
                array(
                    'name' => 'InDatabaseSelect',
                    'options' => array(
                        'field' => 'MY.fruitType'
                    )
                ),
            )
        ));

        $this->add(array(
            'name' => 'MYSeedMaturity',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'PrefixFilter',
                    'options' => array(
                        'prefix' => 'MY.seedMaturity'
                    )
                ),
                array(
                    'name' => 'NormalizeDatabaseSelect',
                    'options' => array(
                        'field' => 'MY.seedMaturity'
                    )
                )
            ),
            'validators' => array(
                array(
                    'name' => 'InDatabaseSelect',
                    'options' => array(
                        'field' => 'MY.seedMaturity'
                    )
                ),
            )
        ));

        $this->add(array(
            'name' => 'MYSeedMorphology',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'PrefixFilter',
                    'options' => array(
                        'prefix' => 'MY.seedMorphology'
                    )
                ),
                array(
                    'name' => 'NormalizeDatabaseSelect',
                    'options' => array(
                        'field' => 'MY.seedMorphology'
                    )
                )
            ),
            'validators' => array(
                array(
                    'name' => 'InDatabaseSelect',
                    'options' => array(
                        'field' => 'MY.seedMorphology'
                    )
                ),
            )
        ));

        $this->add(array(
            'name' => 'MYEarliestEpochOrLowestSeries',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'PrefixFilter',
                    'options' => array(
                        'prefix' => 'MY.epochOrSeries'
                    )
                ),
                array(
                    'name' => 'NormalizeDatabaseSelect',
                    'options' => array(
                        'field' => 'MY.earliestEpochOrLowestSeries'
                    )
                )
            ),
            'validators' => array(
                array(
                    'name' => 'InDatabaseSelect',
                    'options' => array(
                        'field' => 'MY.earliestEpochOrLowestSeries'
                    )
                ),
            )
        ));

        $this->add(array(
            'name' => 'MYLatestEpochOrHighestSeries',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'PrefixFilter',
                    'options' => array(
                        'prefix' => 'MY.epochOrSeries'
                    )
                ),
                array(
                    'name' => 'NormalizeDatabaseSelect',
                    'options' => array(
                        'field' => 'MY.latestEpochOrHighestSeries'
                    )
                )
            ),
            'validators' => array(
                array(
                    'name' => 'InDatabaseSelect',
                    'options' => array(
                        'field' => 'MY.latestEpochOrHighestSeries'
                    )
                ),
            )
        ));

        $this->add(array(
            'name' => 'MYMicrobiologicalRiskGroup',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'PrefixFilter',
                    'options' => array(
                        'prefix' => 'MY.microbiologicalRiskGroup'
                    )
                )
            ),
            'validators' => array(
                array(
                    'name' => 'InDatabaseSelect',
                    'options' => array(
                        'field' => 'MY.microbiologicalRiskGroup'
                    )
                ),
            )
        ));

        $this->add(array(
            'name' => 'MYLifeStage',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'PrefixFilter',
                    'options' => array(
                        'prefix' => 'MY.lifeStage'
                    )
                ),
                array(
                    'name' => 'NormalizeDatabaseSelect',
                    'options' => array(
                        'field' => 'MY.lifeStage'
                    )
                )
            ),
            'validators' => array(
                array(
                    'name' => 'InDatabaseSelect',
                    'options' => array(
                        'field' => 'MY.lifeStage'
                    )
                )
            )
        ));

        $this->add(array(
            'name' => 'MYPlantStatusCode',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'PrefixFilter',
                    'options' => array(
                        'prefix' => 'MY.plantStatusCode'
                    )
                ),
                array(
                    'name' => 'NormalizeDatabaseSelect',
                    'options' => array(
                        'field' => 'MY.plantStatusCode'
                    )
                )
            ),
            'validators' => array(
                array(
                    'name' => 'InDatabaseSelect',
                    'options' => array(
                        'field' => 'MY.plantStatusCode'
                    )
                )
            )
        ));

        $this->add(array(
            'name' => 'MYSex',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'PrefixFilter',
                    'options' => array(
                        'prefix' => 'MY.sex'
                    )
                ),
                array(
                    'name' => 'NormalizeDatabaseSelect',
                    'options' => array(
                        'field' => 'MY.sex'
                    )
                )
            ),
            'validators' => array(
                array(
                    'name' => 'InDatabaseSelect',
                    'options' => array(
                        'field' => 'MY.sex'
                    )
                )
            )
        ));

        $this->add(array(
            'name' => 'MYWeightInGrams',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'ForceArray'
                )
            )
        ));

        $this->add(array(
            'name' => 'MYWingInMillimeters',
            'required' => false,
            'filters' => array(
                array('name' => 'ForceArray')
            ),
        ));

        $this->add(array(
            'name' => 'MYGonadInMillimeters',
            'required' => false,
            'filters' => array(
                array('name' => 'ForceArray')
            ),
        ));

        $this->add(array(
            'name' => 'MYLengthInMillimeters',
            'required' => false,
            'filters' => array(
                array('name' => 'ForceArray')
            ),
        ));

        $this->add(array(
            'name' => 'MYTailInMillimeters',
            'required' => false,
            'filters' => array(
                array('name' => 'ForceArray')
            ),
        ));

        $this->add(array(
            'name' => 'MYAnkleInMillimeters',
            'required' => false,
            'filters' => array(
                array('name' => 'ForceArray')
            ),
        ));

        $identificationInputFilter = $this
            ->getServiceLocator()
            ->getServiceLocator()
            ->get('InputFilterManager')
            ->get('Kotka\InputFilter\MYIdentification');
        $identificationCollectionInputFilter = new CollectionInputFilter();
        $identificationCollectionInputFilter->setInputFilter($identificationInputFilter);

        $this->add($identificationCollectionInputFilter, 'MYIdentification');

        $typeInputFilter = $this
            ->getServiceLocator()
            ->getServiceLocator()
            ->get('InputFilterManager')
            ->get('Kotka\InputFilter\MYTypeSpecimen');
        $typeCollectionInputFilter = new CollectionInputFilter();
        $typeCollectionInputFilter->setInputFilter($typeInputFilter);
        $this->add($typeCollectionInputFilter, 'MYTypeSpecimen');

        $subFilter = $this
            ->getServiceLocator()
            ->getServiceLocator()
            ->get('InputFilterManager')
            ->get('Kotka\InputFilter\MFSample');
        $subCollectionInputFilter = new CollectionInputFilter();
        $subCollectionInputFilter->setInputFilter($subFilter);
        $this->add($subCollectionInputFilter, 'MFSample');

        $subFilter = $this
            ->getServiceLocator()
            ->getServiceLocator()
            ->get('InputFilterManager')
            ->get('Kotka\InputFilter\MYMeasurement');
        $this->add($subFilter, 'MYMeasurement');

        parent::init();
    }
}