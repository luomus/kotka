<?php

namespace Kotka\InputFilter;


class MYTypeSpecimen extends TripleStoreInputFilter
{
    public function init()
    {
        $this->add(array(
            'name' => 'MYTypeStatus',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'PrefixFilter',
                    'options' => array(
                        'prefix' => 'MY.typeStatus'
                    )
                ),
                array(
                    'name' => 'NormalizeDatabaseSelect',
                    'options' => array(
                        'field' => 'MY.typeStatus'
                    )
                )
            ),
            'validators' => array(
                array(
                    'name' => 'InDatabaseSelect',
                    'options' => array(
                        'field' => 'MY.typeStatus'
                    )
                )
            )
        ));

        $this->add(array(
            'name' => 'MYTypeVerification',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'PrefixFilter',
                    'options' => array(
                        'prefix' => 'MY.typeVerification'
                    )
                ),
                array(
                    'name' => 'NormalizeDatabaseSelect',
                    'options' => array(
                        'field' => 'MY.typeVerification'
                    )
                )
            ),
            'validators' => array(
                array(
                    'name' => 'InDatabaseSelect',
                    'options' => array(
                        'field' => 'MY.typeVerification'
                    )
                )
            )
        ));
        parent::init();
    }
}