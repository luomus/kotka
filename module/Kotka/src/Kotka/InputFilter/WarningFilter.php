<?php

namespace Kotka\InputFilter;

use Zend\InputFilter\InputFilter;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

abstract class WarningFilter extends InputFilter implements ServiceLocatorAwareInterface
{

    use ServiceLocatorAwareTrait;

    /**
     * Ensure warning filters work with partial validation groups
     *
     * @param  array $inputs
     * @return void
     */
    protected function validateValidationGroup(array $inputs)
    {

    }


} 