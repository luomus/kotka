<?php

namespace Kotka\InputFilter;


use Kotka\Model\Util;
use Kotka\Validator\BothNotSet;
use Kotka\Validator\Coordinate;
use Kotka\Validator\DateCompare;
use Kotka\Validator\OneOrTheOther;
use Kotka\Validator\RequireWithThis;
use Kotka\Validator\OnlyOnePrimarySpecimen;
use Zend\InputFilter\CollectionInputFilter;

class MYGathering extends TripleStoreInputFilter
{
    protected $primarySpecimenValidator;

    public function init()
    {
        $this->add(array(
            'name' => 'MYDateBegin',
            'required' => false,
            'validators' => array(
                array(
                    'name' => 'DateCompare',
                    'options' => array(
                        'format' => Util::DATETIME_FORMAT,
                        'direction' => DateCompare::BEFORE
                    )
                )
            ),
            'filters' => array(
                array(
                    'name' => 'RomanDateToDate'
                )
            ),
        ));

        $this->add(array(
            'name' => 'MYDateEnd',
            'required' => false,
            'validators' => array(
                array(
                    'name' => 'RequireWithThis',
                    'break_chain_on_failure' => true,
                    'options' => array(
                        'field' => 'MYDateBegin',
                        'messages' => array(
                            RequireWithThis::MISSING => 'If the gatherings date end is given so should the start'
                        ),
                    ),
                ),
                array(
                    'name' => 'DateCompare',
                    'options' => array(
                        'format' => Util::DATETIME_FORMAT,
                        'direction' => DateCompare::BEFORE
                    )
                ),
                array(
                    'name' => 'DateCompare',
                    'options' => array(
                        'format' => Util::DATETIME_FORMAT,
                        'direction' => DateCompare::AFTER,
                        'compareTo' => 'MYDateBegin'
                    )
                )
            ),
            'filters' => array(
                array(
                    'name' => 'RomanDateToDate'
                )
            ),
        ));

        $this->add(array(
            'name' => 'MYCountry',
            'required' => true,
            'allow_empty' => true,
            'continue_if_empty' => true,
            'validators' => array(
                array(
                    'name' => 'OneOrTheOther',
                    'options' => array(
                        'other' => 'MYHigherGeography',
                        'messages' => array(
                            OneOrTheOther::MISSING => 'Either country of higherGeography should be set.'
                        )

                    ),
                ),
            )
        ));

        $this->add(array(
            'name' => 'MYPercentageGivingSeeds',
            'required' => false,
            'validators' => array(
                array(
                    'name' => 'Between',
                    'break_chain_on_failure' => true,
                    'options' => array(
                        'min' => 0,
                        'max' => 100,
                        'messages' => array(
                            \Zend\Validator\Between::NOT_BETWEEN => '% of plant population giving seeds must be between 0...100.'
                        )
                    )
                ),
                array(
                    'name' => 'Digits',
                    'break_chain_on_failure' => true,
                    'options' => array(
                        'messages' => array(
                            \Zend\Validator\Digits::NOT_DIGITS => '% of plant population giving seeds must be whole number (integer) . If exact value is not known, insert the best estimate.'
                        )
                    )
                )
            )
        ));

        $this->add(array(
            'name' => 'MYHigherGeography',
            'required' => false
        ));

        $this->add(array(
            'name' => 'MYGeoreferenceSource',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'PrefixFilter',
                    'options' => array(
                        'prefix' => 'MY.georeferenceSource'
                    )
                ),
                array(
                    'name' => 'NormalizeDatabaseSelect',
                    'options' => array(
                        'field' => 'MY.georeferenceSource'
                    )
                )
            ),
            'validators' => array(
                array(
                    'name' => 'InDatabaseSelect',
                    'options' => array(
                        'field' => 'MY.georeferenceSource'
                    )
                )
            )
        ));

        $this->add(array(
            'name' => 'MYCoordinateSource',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'PrefixFilter',
                    'options' => array(
                        'prefix' => 'MY.coordinateSource'
                    )
                ),
                array(
                    'name' => 'NormalizeDatabaseSelect',
                    'options' => array(
                        'field' => 'MY.coordinateSource'
                    )
                )
            ),
            'validators' => array(
                array(
                    'name' => 'BothNotSet',
                    'options' => array(
                        'hasNot' => 'MYGeoreferenceSource',
                        'messages' => array(
                            BothNotSet::BOTH_SET => 'Both coordSource and georeferenceSource cannot be set at the same time. Choose only one of them, based on whether the coordinates are recorded-on-site or georeferenced later.'
                        ),
                    ),
                ),
                array(
                    'name' => 'InDatabaseSelect',
                    'options' => array(
                        'field' => 'MY.coordinateSource'
                    )
                )
            )
        ));

        $this->add(array(
            'name' => 'MYCoordinateSystem',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'PrefixFilter',
                    'options' => array(
                        'prefix' => 'MY.coordinateSystem'
                    )
                ),
                array(
                    'name' => 'NormalizeDatabaseSelect',
                    'options' => array(
                        'field' => 'MY.coordinateSystem'
                    )
                ),
                array(
                    'name' => 'PregReplace',
                    'options' => array(
                        'pattern' => '/MY.coordinateSystemEtrs89-tm35fin/',
                        'replacement' => 'MY.coordinateSystemEtrs-tm35fin'
                    )
                )
            ),
            'validators' => array(
                array(
                    'name' => 'InDatabaseSelect',
                    'options' => array(
                        'field' => 'MY.coordinateSystem'
                    )
                )
            )
        ));

        $this->add(array(
            'name' => 'MYCoordinateRadius',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'PregReplace',
                    'options' => array(
                        'pattern' => '/(m|\sm)\.?$/',
                        'replacement' => ''
                    )
                ),
            ),
            'validators' => array(
                array(
                    'name' => 'Digits',
                    'break_chain_on_failure' => true,
                    'options' => array(
                        'messages' => array(
                            \Zend\Validator\Digits::NOT_DIGITS => "Coordinate radius has to be whole number. Now '%value%' given."
                        )
                    )
                ),
                array(
                    'name' => 'GreaterThan',
                    'break_chain_on_failure' => true,
                    'options' => array(
                        'min' => 0,
                        'messages' => array(
                            \Zend\Validator\GreaterThan::NOT_GREATER => 'Coordinate radius has to be greater than 0.'
                        )
                    )
                )
            )
        ));

        $this->add(array(
            'name' => 'MYAlt',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'PregReplace',
                    'options' => array(
                        'pattern' => '/(m|\sm)\.?$/',
                        'replacement' => ''
                    )
                ),
                array(
                    'name' => 'FloatNormalizer',
                )
            ),
            'validators' => array(
                array(
                    'name' => 'FloatRange',
                    'break_chain_on_failure' => true,
                    'options' => array(
                        'allow-range' => true,
                        'allow-negative' => true,
                        'messages' => array(
                          \Kotka\Validator\FloatRange::NOT_FLOAT => "Altitude should be whole number, decimal or range. Now '%value%' given.",
                          \Kotka\Validator\FloatRange::INVALID_RANGE => "Altitude should be an integer or decimal number (5.5) or a range (500-800). Now '%value%' given."
                        )
                    )
                ),
            )
        ));
        $this->add(array(
            'name' => 'MYSamplingAreaSizeInSquareMeters',
            'required' => false,
            'validators' => array(
                array(
                    'name' => 'FloatRange',
                    'break_chain_on_failure' => true,
                    'options' => array(
                        'allow-range' => true,
                        'messages' => array(
                            \Kotka\Validator\FloatRange::NOT_FLOAT => "Sampling area should be whole number or decimal. Now '%value%' given.",
                            \Kotka\Validator\FloatRange::INVALID_RANGE => "Sampling area should be valid range of number or decimal. Now '%value%' given."
                        )
                    )
                ),
            )
        ));

        $this->add(array(
            'name' => 'MYDepth',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'PregReplace',
                    'options' => array(
                        'pattern' => '/(m|\sm)\.?$/',
                        'replacement' => ''
                    )
                ),
                array(
                    'name' => 'FloatNormalizer',
                )
            ),
            'validators' => array(
                array(
                    'name' => 'FloatRange',
                    'break_chain_on_failure' => true,
                    'options' => array(
                        'allow-range' => true,
                        'messages' => array(
                            \Kotka\Validator\FloatRange::NOT_FLOAT => "Depth should be whole number or decimal. Now '%value%' given.",
                            \Kotka\Validator\FloatRange::INVALID_RANGE => "Depth should be valid range of number or decimal. Now '%value%' given."
                        )
                    )
                )
            )
        ));

        $this->add(array(
            'name' => 'MYLatitude',
            'required' => false,
            'validators' => array(
                array(
                    'name' => 'Coordinate',
                    'options' => array(
                        'coordinateSystemIn' => 'MYCoordinateSystem',
                        'otherOfPair' => 'MYLongitude',
                        'direction' => Coordinate::LATITUDE,
                        'messages' => array(
                            Coordinate::OUT_OF_BOUNDS => 'Latitude (N) out of bounds.',
                            Coordinate::NOT_NUMERIC => 'Latitude (N) must be integer.'
                        ),
                    )
                ),
            ),
            'filters' => array(
                array(
                    'name' => 'Coordinate'
                ),
            ),
        ));

        $this->add(array(
            'name' => 'MYLongitude',
            'required' => false,
            'validators' => array(
                array(
                    'name' => 'Coordinate',
                    'options' => array(
                        'coordinateSystemIn' => 'MYCoordinateSystem',
                        'direction' => Coordinate::LONGITUDE,
                        'messages' => array(
                            Coordinate::OUT_OF_BOUNDS => 'Longitude (E) out of bounds.',
                            Coordinate::NOT_NUMERIC => 'Longitude (E) must be integer.'
                        ),
                    )
                ),
            ),
            'filters' => array(
                array(
                    'name' => 'Coordinate'
                ),
            ),
        ));

        $this->add(array(
            'name' => 'MYSamplingMethod',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'PrefixFilter',
                    'options' => array(
                        'prefix' => 'MY.samplingMethod'
                    )
                ),
                array(
                    'name' => 'NormalizeDatabaseSelect',
                    'options' => array(
                        'field' => 'MY.samplingMethod'
                    )
                )
            ),
            'validators' => array(
                array(
                    'name' => 'InDatabaseSelect',
                    'options' => array(
                        'field' => 'MY.samplingMethod'
                    )
                )
            )
        ));

        $this->add(array(
            'name' => 'MYLeg',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'ForceArray'
                )
            )
        ));

        $this->add(array(
            'name' => 'MYForestVegetationZone',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'PrefixFilter',
                    'options' => array(
                        'prefix' => 'MY.forestVegetationZone'
                    )
                ),
                array(
                    'name' => 'NormalizeDatabaseSelect',
                    'options' => array(
                        'field' => 'MY.forestVegetationZone'
                    )
                )
            ),
            'validators' => array(
                array(
                    'name' => 'InDatabaseSelect',
                    'options' => array(
                        'field' => 'MY.forestVegetationZone'
                    )
                )
            )
        ));

        $unitInputFilter = $this
            ->getServiceLocator()
            ->getServiceLocator()
            ->get('InputFilterManager')
            ->get('Kotka\InputFilter\MYUnit');

        $collectionContainerInputFilter = new CollectionInputFilter();
        $collectionContainerInputFilter->setInputFilter($unitInputFilter);
        $this->add($collectionContainerInputFilter, 'MYUnit'); 

        $this->primarySpecimenValidator = new OnlyOnePrimarySpecimen();
        parent::init();
    }

    public function isValid($context = null) {
        $valid = true;
        $valid = $this->primarySpecimenValidator->isValid(null, array ('MYUnit' => $this->get('MYUnit')->data));
        $valid = parent::isValid($context) === true ? $valid : false;

        return $valid;
    }

    public function getMessages() {
        $messages = parent::getMessages();
        $primaryMessages = $this->primarySpecimenValidator->getMessages();
        if (count($primaryMessages) === 0) {
            return $messages;
        }

        $messages['MYUnit'][] = $this->primarySpecimenValidator->getMessages();

        return $messages;
    }
}