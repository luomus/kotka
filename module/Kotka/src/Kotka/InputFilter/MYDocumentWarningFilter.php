<?php

namespace Kotka\InputFilter;

use Kotka\Form\Element;
use Kotka\Validator\ArrayContains;
use Zend\InputFilter\CollectionInputFilter;

/**
 * Class MYDocumentWarningFilter validates specimens and sets the warning messages
 *
 * @package Kotka\InputFilter
 */
class MYDocumentWarningFilter extends WarningFilter
{

    public function init()
    {
        $gatheringInputFilter = $this
            ->getServiceLocator()
            ->getServiceLocator()
            ->get('InputFilterManager')
            ->get('Kotka\InputFilter\MYGatheringWarningFilter');

        $collectionContainerInputFilter = new CollectionInputFilter();
        $collectionContainerInputFilter->setInputFilter($gatheringInputFilter);
        $this->add($collectionContainerInputFilter, 'MYGathering');

        $this->add(array(
            'name' => 'MYAdditionalIDs',
            'required' => false,
            'validators' => array(
                array(
                    'name' => '\Kotka\Validator\ArrayContains',
                    'options' => array(
                        'contain' => ':',
                        'messages' => array(
                            ArrayContains::NOT_FOUND => "Additional ID should have prefix separated with ':'. Now contains '%value%'."
                        ),
                    ),
                )
            )
        ));
    }
}
