<?php

namespace Kotka\InputFilter;


use Zend\InputFilter\InputFilter;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

abstract class TripleStoreInputFilter extends InputFilter implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    private $metadataService;
    protected $skip = [];

    public function init()
    {
        parent::init();
        $this->addRest(get_class($this));
    }

    protected function addRest($class)
    {
        $metadata = $this->getMetadataService()->getMetadataFor($class);
        $properties = $metadata->getProperties();
        $allProperties = $metadata->getAllProperties();
        $allProperties->initAliases();
        foreach ($properties as $property) {
            $field = $allProperties->getAlias($property);
            $required = $metadata->isRequired($property) && $metadata->getOrder($property) > 0;
            if ($metadata->isMultiLanguage($property)) {
                foreach (['En','Sv','Fi'] as $lang) {
                    $langField = $field . $lang;
                    if ($this->has($langField) || in_array($langField, $this->skip)) {
                        continue;
                    }
                    $this->add([
                        'name' => $langField,
                        'required' => $required,
                    ]);
                }
                continue;
            }
            if ($this->has($field) || in_array($field, $this->skip)) {
                continue;
            }
            $spec = [
                'name' => $field,
                'required' => $required,
                'filters' => []
            ];
            if ($metadata->hasMany($property)) {
                $spec['filters'][] = ['name' => 'ForceArray'];
            }
            $this->add($spec);
        }
    }

    protected function getMetadataService()
    {
        if ($this->metadataService === null) {
            /** @var \Triplestore\Service\ObjectManager $om */
            $om = $this->getServiceLocator()->getServiceLocator()->get('Triplestore\ObjectManager');
            $this->metadataService = $om->getMetadataService();
        }
        return $this->metadataService;
    }

}