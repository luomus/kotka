<?php
namespace Kotka\InputFilter;


use Kotka\Model\Util;
use Kotka\Validator\ArrayContains;
use Kotka\Validator\RequireWithThis;
use Zend\InputFilter\CollectionInputFilter;

/**
 * Class MYGatheringWarningFilter
 *
 * @package Kotka\InputFilter
 */
class MYGatheringWarningFilter extends WarningFilter
{
    public function init()
    {
        $unitInputFilter = $this
            ->getServiceLocator()
            ->getServiceLocator()
            ->get('InputFilterManager')
            ->get('Kotka\InputFilter\MYUnitWarningFilter');

        $collectionContainerInputFilter = new CollectionInputFilter();
        $collectionContainerInputFilter->setInputFilter($unitInputFilter);
        $this->add($collectionContainerInputFilter, 'MYUnit');

        $this->add(array(
            'name' => 'MYLongitude',
            'required' => false,
            'validators' => array(
                array(
                    'name' => '\Kotka\Validator\RequireWithThis',
                    'options' => array(
                        'field' => 'MYCoordinateSystem',
                        'messages' => array(
                            RequireWithThis::MISSING => 'Coordinate system should be set, when coordinates are given.'
                        ),
                    ),
                ),
                array(
                    'name' => '\Kotka\Validator\RequireWithThis',
                    'options' => array(
                        'field' => 'MYLatitude',
                        'messages' => array(
                            RequireWithThis::MISSING => 'Latitude should be set, when longitude is given.'
                        ),
                    ),
                )
            )
        ));

        // Fields that are used for validation need to be in the filter for the context to populate
        $this->add(array(
            'name' => 'MYCoordinateSystem',
            'required' => false,
        ));

        $this->add(array(
            'name' => 'MYLatitude',
            'required' => false,
            'validators' => array(
                array(
                    'name' => '\Kotka\Validator\RequireWithThis',
                    'options' => array(
                        'field' => 'MYLongitude',
                        'messages' => array(
                            RequireWithThis::MISSING => 'Longitude should be set, when latitude is given.'
                        ),
                    ),
                )
            )
        ));

        $this->add(array(
            'name' => 'MYLeg',
            'required' => false,
            'validators' => array(
                array(
                    'name' => '\Kotka\Validator\ArrayContains',
                    'options' => array(
                        'pattern' => Util::NAME_REGEX_PATTERN,
                        'messages' => array(
                            ArrayContains::NOT_MATCH => "Leg should be 'Lastname, Firstname' (is now '%value%')."
                        ),
                    ),
                )
            )
        ));

        $this->add([
            'name' => 'MYMunicipality',
            'required' => false,
            'validators' => [
                [
                    'name' => '\Kotka\Validator\LocationNameMatchesCoordinates'
                ]
            ]
        ]);
    }

} 