<?php

namespace Kotka\InputFilter;

use Kotka\Model\Util;
use Zend\Validator\Date;
use Zend\Validator\Regex;

/**
 * Class IdentificationsWarningFilter handles warnings for identifications
 *
 * @package Kotka\InputFilter
 */
class MYIdentificationWarningFilter extends WarningFilter
{
    public function __construct()
    {
        $this->add(array(
            'name' => 'MYDetDate',
            'required' => false,
            'validators' => array(
                array(
                    'name' => 'Kotka\Validator\DateMulti',
                    'options' => array(
                        'format' => array(
                            'Y',
                            Util::DATETIME_FORMAT
                        ),
                        'messages' => array(
                            Date::INVALID_DATE => 'Det date is not in correct dd.mm.yyyy or yyyy format.',
                            Date::INVALID => 'Det date is not in correct dd.mm.yyyy or yyyy format.',
                            Date::FALSEFORMAT => 'Det date is not in correct dd.mm.yyyy or yyyy format.'
                        )
                    )
                )
            )
        ));

        $this->add(array(
            'name' => 'MYDet',
            'required' => false,
            'validators' => array(
                array(
                    'name' => 'regex',
                    'options' => array(
                        'pattern' => Util::NAME_REGEX_PATTERN,
                        'messages' => array(
                            Regex::NOT_MATCH => "Det should be 'Lastname, Firstname' (is now '%value%')."
                        )
                    ),
                ),
            )
        ));
    }

}
