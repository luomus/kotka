<?php

namespace Kotka\InputFilter;

use Kotka\Validator\RequireWithThis;

class HRAPermit extends TripleStoreInputFilter
{
    protected $specimenFilter;

    public function init()
    {

        $this->add(array(
            'name' => 'HRAPermitStartDate',
            'required' => false
        ));
        $this->add(array(
            'name' => 'HRAPermitStatus',
            'required' => false,
            'validators' => [
                [
                    'name' => 'InDatabaseSelect',
                    'options' => array(
                        'field' => 'HRA.permitStatus'
                    )
                ],
                [
                    'name' => 'RequireWithThis',
                    'options' => array(
                        'field' => 'HRAPermitFile',
                        'is_file' => true,
                        'when_contains' => 'HRA.permitStatusAvailable',
                        'messages' => array(
                            RequireWithThis::MISSING => 'You have to upload the permit when available!'
                        ),
                    ),
                ]
            ]
        ));

        $this->add(array(
            'name' => 'HRAPermitFile',
            'require' => false,
            'validators' => [
                [
                    'name' => 'Kotka\Validator\File\Size',
                    'options' => array(
                        'max' => '2MB',
                        'messages' => array(
                            \Zend\Validator\File\Size::TOO_BIG => "Maximum allowed size for file is '%max%' but '%size%' detected."
                        )
                    )
                ]
            ]
        ));

        parent::init();
    }

}