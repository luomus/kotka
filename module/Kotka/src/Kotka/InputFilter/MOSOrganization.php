<?php

namespace Kotka\InputFilter;


class MOSOrganization extends TripleStoreInputFilter
{
    protected $skip = [
        'MOSOrganizationLevel1Fi',
        'MOSOrganizationLevel1Sv',
    ];

}