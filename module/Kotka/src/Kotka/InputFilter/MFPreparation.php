<?php

namespace Kotka\InputFilter;


class MFPreparation extends TripleStoreInputFilter
{
    public function init()
    {

        $inDb = [
            'MFPreparationMaterials' => 'MF.preparationMaterials',
            'MFPreparationProcess' => 'MF.preparationProcess',
        ];

        foreach ($inDb as $field => $db) {
            $this->add(array(
                'name' => $field,
                'required' => false,
                'filters' => array(
                    array(
                        'name' => 'PrefixFilter',
                        'options' => array(
                            'prefix' => $db
                        )
                    ),
                    array(
                        'name' => 'ForceArray',
                    ),
                    array(
                        'name' => 'EmptyArray',
                    ),
                    array(
                        'name' => 'ToQName'
                    ),
                ),
                'validators' => array(
                    array(
                        'name' => 'InDatabaseSelect',
                        'options' => array(
                            'field' => $db
                        )
                    )
                )
            ));
        }

        parent::init();
    }
}