<?php

namespace Kotka\Transform;


use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

class Lutikka implements TransformInterface, ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;
    private $currentSpecimen;

    protected $databaseMap = [
        'GX.470' => '07coleoptera',
        'GX.471' => '08diptera',
        'GX.472' => '07ephemer',
        'GX.473' => '08lepidoptera',
        'GX.474' => '08lichenes',
        'GX.475' => '08trichoptera',
        'tun:GX.490' => '07nests',
        'tun:GX.491' => '07birds',
        'tun:GX.492' => '08bryophytes',
        'tun:GX.493' => '08crustacea',
        'tun:GX.494' => '08hymenoptera',
        'tun:GX.495' => '07fungi',
        'tun:GX.496' => '08hemiptera',
        'tun:GX.497' => '07mammals',
        'tun:GX.498' => '08misc_insects',
        'tun:GX.499' => '08mollusca',
        'tun:GX.500' => '11neuroptera',
        'tun:GX.501' => '08plecoptera',
        'tun:GX.502' => '07plants',
        'tun:GX.503' => '07fish',
        'tun:GX.504' => '11orthoptera',
        'tun:GX.505' => '07reptiles',
        'tun:GX.506' => '08thysanoptera',
    ];

    protected $infraRanks = [
        'ssp' => 'MY.infraRankSsp',
        'var' => 'MY.infraRankVar',
        'f' => 'MY.infraRankForma'
    ];

    protected $adapters = [];

    /**
     * This transforms the given data to another form. This is very close to the way filters work, but these are meant
     * to be more versatile. These look the data as whole and do the necessary alterations before returning the transformed
     * data.
     *
     * These are meant to be used with imports where data needs to be altered before sending to the validators.
     *
     * @param mixed $data
     * @return mixed
     */
    public function transform($data)
    {
        $this->currentSpecimen = isset($data['subject']) ? $data['subject'] : null;
        if (!isset($data['MYDatasetID']) || !isset($data['MYAdditionalIDs'])) {
            return $data;
        }
        $adapter = null;
        if (!is_array($data['MYDatasetID'])) {
            if (is_string($data['MYDatasetID'])) {
                $data['MYDatasetID'] = [$data['MYDatasetID']];
            } elseif ($data['MYDatasetID'] === null) {
                echo "invalid null dataset!";
                var_dump($data);
                exit();
            } else {
                echo "invalid dataset!";
                var_dump($data);
                exit();
            }
        }
        foreach ($data['MYDatasetID'] as $dataset) {
            if (isset($this->databaseMap[$dataset])) {
                $adapter = $this->getAdapter($this->databaseMap[$dataset]);
                break;
            }
        }
        if (!$adapter instanceof Adapter) {
            return $data;
        }
        $specID = null;
        if (!is_array($data['MYAdditionalIDs'])) {
            if (is_string($data['MYAdditionalIDs'])) {
                $data['MYAdditionalIDs'] = [$data['MYAdditionalIDs']];
            } elseif ($data['MYAdditionalIDs'] === null) {
                echo "invalid null additional IDs!";
                var_dump($data);
                exit();
            } else {
                echo "invalid additional ID!";
                var_dump($data);
                exit();
            }
        }
        foreach ($data['MYAdditionalIDs'] as $additional) {
            if (strpos($additional, 'lutikkadatabase:') === 0) {
                $specID = str_replace('lutikkadatabase:', '', $additional);
                break;
            }
        }
        $identification = null;
        if (isset($data['MYGathering']) &&
            isset($data['MYGathering'][0]) &&
            isset($data['MYGathering'][0]['MYUnit']) &&
            isset($data['MYGathering'][0]['MYUnit'][0]) &&
            isset($data['MYGathering'][0]['MYUnit'][0]['MYIdentification']) &&
            isset($data['MYGathering'][0]['MYUnit'][0]['MYIdentification'][0])
        ) {
            $identification = &$data['MYGathering'][0]['MYUnit'][0]['MYIdentification'][0];
        }

        if ($specID === null || $identification === null) {
            return $data;
        }

        $sql = new Sql($adapter);
        $select = $sql->select('specimen');
        $select->where(['spcmID' => $specID]);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        foreach ($results as $result) {
            if (!isset($result['spcm_specID'])) {
                continue;
            }
            $taxonSelect = $sql->select('t_species');
            $taxonSelect->join('t_genus', 't_species.spec_genuID = t_genus.genuID');
            $taxonSelect->where(['specID' => $result['spcm_specID']]);
            $statement = $sql->prepareStatementForSqlObject($taxonSelect);
            $results2 = $statement->execute();
            foreach ($results2 as $k2 => $value) {
                if ($k2 > 0) {
                    throw new \Exception("DUPLICATE IDS: " . $result['spcm_specID']);
                }
                if (empty($value['specType'])) {
                    $this->species($identification, $value);
                } else {
                    $this->subSpecies($identification, $value, $sql);
                }
            }
        }
        return $data;
    }

    private function species(&$identification, $db, $verbatimSuffix = '')
    {
        $authFromGenus = false;
        $db['specSCname'] = trim($db['specSCname']);
        if (empty($db['specSCname']) || preg_match('/sp[\.)]*$/', $db['specSCname'])) {
            $identification['MYTaxonRank'] = 'MX.genus';
            $authFromGenus = true;
        } elseif (strpos($db['specSCname'], ' x ') !== false) {
            $identification['MYTaxonRank'] = 'MX.hybrid';
        } else {
            $identification['MYTaxonRank'] = 'MX.species';
        }
        if ($authFromGenus) {
            $identification['MYAuthor'] = $db['genuAuctor'];
        } else {
            $identification['MYAuthor'] = $db['specAuctor'];
        }
        $identification['MYTaxon'] = $db['genuSCname'] . ' ' . $db['specSCname'];
        $identification['MYTaxonVerbatim'] = $db['genuSCname'] . ' ' . $db['specSCname'] . ' ' . $db['specAuctor'] . ' ' . $verbatimSuffix;
    }

    private function subSpecies(&$identification, $db, Sql $sql)
    {
        $taxonVerbatim = '';
        if (isset($this->infraRanks[$db['specType']])) {
            $identification['MYInfraRank'] = $this->infraRanks[$db['specType']];
        } else {
            $identification['MYInfraRank'] = $db['specType'];
        }
        $identification['MYInfraEpithet'] = $db['specSCname'];
        $identification['MYInfraAuthor'] = $db['specAuctor'];
        $taxonVerbatim .= $db['specType'] . ' ' . $db['specSCname'] . ' ' . $db['specAuctor'];

        $taxonSelect = $sql->select('t_species');
        $taxonSelect->join('t_genus', 't_species.spec_genuID = t_genus.genuID');
        $taxonSelect->where(['specID' => $db['spec_specID']]);
        $statement = $sql->prepareStatementForSqlObject($taxonSelect);
        $results = $statement->execute();
        foreach ($results as $key => $result) {
            if ($key > 0) {
                throw new \Exception("DUPLICATE IDS: " . $db['spcm_specID']);
            }
            $this->species($identification, $result, $taxonVerbatim);
        }
    }


    /**
     * @param $dbname
     * @return Adapter
     */
    protected function getAdapter($dbname)
    {
        if (isset($this->adapters[$dbname])) {
            return $this->adapters[$dbname];
        }
        $dsn = sprintf('mysql:host=localhost;dbname=%s;charset=utf8', $dbname);
        $config = [
            'driver' => 'Pdo',
            'dsn' => $dsn,
            'username' => 'kotka',
            'password' => 'kotka',
            'driver_options' => array(
                \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''
            ),
        ];
        $this->adapters[$dbname] = new Adapter($config);
        return $this->adapters[$dbname];
    }

}