<?php

namespace Kotka\Transform;


interface TransformInterface
{
    /**
     * This transforms the given data to another form. This is very close to the way filters work, but these are meant
     * to be more versatile. These look the data as whole and do the necessary alterations before returning the transformed
     * data.
     *
     * These are meant to be used with imports where data needs to be altered before sending to the validators.
     *
     * @param mixed $data
     * @return mixed
     */
    public function transform($data);
}