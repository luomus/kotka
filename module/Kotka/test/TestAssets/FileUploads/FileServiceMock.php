<?php

namespace TestAssets\FileUploads;

use Kotka\Service\FileService;

class FileServiceMock extends FileService
{

    public function _move($source, $fullPath, $fromUpload = true)
    {
        return copy($source, $fullPath);
    }
} 