<?php

namespace KotkaTest\Validator;

use Kotka\Model\Label;
use Kotka\Triple\MYIdentification;
use Kotka\View\Helper\FullTaxon;

class FullTaxonTest extends \PHPUnit_Framework_TestCase
{

    /** @var FullTaxon */
    public $helper;

    public function setUp()
    {
        $this->helper = new FullTaxon();
    }

    public function tearDown()
    {
        unset($this->helper);
    }

    public function testGettingCorrectWithoutAnything()
    {
        $this->assertEquals('', $this->helper->__invoke(''));
        $this->assertEquals('', $this->helper->__invoke());
        $this->assertEquals('Taxon', $this->helper->__invoke('Taxon'));
    }

    public function testGenusQualifierIsInRightSpot()
    {
        $this->assertEquals('Taxon foo', $this->helper->__invoke('Taxon', 'foo'));
        $this->assertEquals('Taxon foo Name', $this->helper->__invoke('Taxon Name', 'foo'));
        $this->assertEquals('Taxon foo Name Long', $this->helper->__invoke('Taxon Name Long', 'foo'));
        $this->assertEquals('Taxon Name Long', $this->helper->__invoke('Taxon Name Long', 'Name'));
    }

    public function testSpeciesQualifierIsInRightSpot()
    {
        $this->assertEquals('Taxon foo', $this->helper->__invoke('Taxon',  null, 'foo'));
        $this->assertEquals('Taxon Name foo', $this->helper->__invoke('Taxon Name',  null, 'foo'));
        $this->assertEquals('Taxon cf Name', $this->helper->__invoke('Taxon Name',  null, 'cf'));
        $this->assertEquals('Taxon CF Name', $this->helper->__invoke('Taxon Name',  null, 'CF'));
        $this->assertEquals('Taxon Cf. Name', $this->helper->__invoke('Taxon Name',  null, 'Cf.'));
    }

    public function testDifficultCases()
    {
        $this->assertEquals('Taxon foo Name', $this->helper->__invoke(' Taxon Name', 'foo'));
        $this->assertEquals('Taxon Name coll', $this->helper->__invoke('Taxon   Name  ',  null, 'coll'));
        $this->assertEquals('Taxon Name', $this->helper->__invoke('Taxon Name  ',  null, 'Name'));
        $this->assertEquals('Taxon Name', $this->helper->__invoke('Taxon Name  ',  null, 'name'));
        $this->assertEquals('TaxonName Name', $this->helper->__invoke('TaxonName',  null, 'Name'));
    }

    public function testGettingTaxonWithLabelClass()
    {
        $label = new Label();
        $label->setTaxon('Foo Bar');
        $label->setSpeciesQualifier('cf');
        $this->assertEquals('Foo cf Bar', $this->helper->__invoke($label));
        $label->setSpeciesQualifier('coll');
        $this->assertEquals('Foo Bar coll', $this->helper->__invoke($label));
    }

    public function testGettingTaxonWithIdentification()
    {
        $identification = new MYIdentification();
        $identification->setMYTaxon('Foo');
        $identification->setMYSpeciesQualifier('bar');
        $identification->setMYGenusQualifier('genus');
        $this->assertEquals('Foo genus bar', $this->helper->__invoke($identification));
    }

}
 