<?php

namespace KotkaTest\Validator;

use Kotka\View\Helper\SearchQuery;

class SearchQueryTest extends \PHPUnit_Framework_TestCase
{

    /** @var SearchQuery */
    public $helper;

    public function setUp()
    {
        $this->helper = new SearchQuery();
    }

    public function tearDown()
    {
        unset($this->helper);
    }

    public function testGettingSearchValueWithSimpleString()
    {
        $searchWord = 'search_work';
        $this->getViewMock(['q' => $searchWord]);
        $this->assertTrue($this->helper->__invoke($searchWord));
    }

    public function testGettingSearchValueWithElasticQuery()
    {
        $searchWord = 'collectionID: "HR.123"';
        $this->getViewMock(['q' => $searchWord]);
        $this->assertTrue($this->helper->__invoke($searchWord));
    }

    public function testSearchWithSimpleArray()
    {
        $search = ['collectionID' => 'HR.123'];
        $this->getViewMock(['q' => 'collectionID: "HR.123"']);
        $this->assertTrue($this->helper->__invoke($search));
    }

    public function testSearchWithMultipleCriteria()
    {
        $search = ['collectionID' => 'HR.123', 'editor' => 'MA.97'];
        $this->getViewMock(['s' => ['collectionID: "HR.123"', 'editor: "MA.97"']]);
        $this->assertTrue($this->helper->__invoke($search));
    }

    private function getViewMock($with)
    {
        /** @var \Zend\View\Renderer\RendererInterface $rendererMock */
        $rendererMock = $this->getMock('Zend\View\Renderer\PhpRenderer');
        $rendererMock->expects($this->once())
            ->method('__call')
            ->with(
                $this->equalTo('url'),
                $this->equalTo([
                    'https/search_specimens',
                    [],
                    ['query' => $with]
                ])
            )
            ->will($this->returnValue(true));
        $this->helper->setView($rendererMock);

        return $rendererMock;
    }

}
 