<?php

namespace KotkaTest\Validator;

use Kotka\View\Helper\Label\Coordinate;

class CoordinateTest extends \PHPUnit_Framework_TestCase
{

    /** @var Coordinate */
    public $helper;

    public function setUp()
    {
        $this->helper = new Coordinate();
    }

    public function tearDown()
    {
        unset($this->helper);
    }

    public function testGettingDefaultType()
    {
        $this->assertEquals('long', $this->helper->getType());
    }

    public function testSettingType()
    {
        $this->helper->setType('short');
        $this->assertEquals('short', $this->helper->getType());
    }

    /**
     * @expectedException \Exception
     */
    public function testSettingNonExistingType()
    {
        $this->helper->setType('non-existing');
    }

    public function testSettingCoordinateSet()
    {
        $set = [
            'ykj' => '%lat%,%lon%',
            'default' => '%lat:%lon'
        ];
        $this->helper->setCoordinateSet('test', $set);
        $this->assertEquals($set, $this->helper->getCoordinateSet());
        $this->assertEquals('test', $this->helper->getType());
    }

    public function testCoordinateString()
    {
        $set = [
            'ykj' => '%lat%,%lon%,%wgsN%,%wgsE%,%radius%,%accuracy%',
            'default' => '%lat%-%lon%-%wgsN%-%wgsE%-%radius%-%accuracy%'
        ];
        $this->helper->setCoordinateSet('test', $set);
        $this->assertEquals('1234,5678,3°0\'0",4°0\'0",0,720', $this->helper->__invoke('ykj', '1234', '5678', 3, 4, 0));
        $this->assertEquals('1234,5678,3°0\'0",4°0\'0",0,720', $this->helper->__invoke('MY.coordinateSystemYkj', '1234', '5678', 3, 4, 0));
        $this->assertEquals('1234-5678-3-4-0-', $this->helper->__invoke('other', '1234', '5678', 3, 4, 0));
        $this->assertEquals('1234-5678-20-60-300-', $this->helper->__invoke('other', '1234', '5678', '20', '60', 300));
        $this->assertEquals('1234,5678,3°0\'0",4°0\'0",100,820', $this->helper->__invoke('MY.coordinateSystemYkj', '1234', '5678', 3, 4, 100));
        $this->assertEquals('6650000,3300000,3°0\'0",4°0\'0",100,100', $this->helper->__invoke('ykj', '6650000', '3300000', 3, 4, 100));
        $this->assertEquals('6650000,3300000,3°0\'0",4°0\'0",0,1', $this->helper->__invoke('ykj', '6650000', '3300000', 3, 4, '0'));
        $this->assertEquals('6650000,3300000,3°0\'0",4°0\'0",10,10', $this->helper->__invoke('ykj', '6650000', '3300000', 3, 4, '10'));
    }

    public function testAugmentingValue()
    {
        $set = [
            'ykj' => '%lat%,%lon%,%wgsN%,%wgsE%,%radius%,%accuracy%',
            'default' => '%lat%-%lon%-%wgsN%-%wgsE%-%radius%-%accuracy%',
            'augment' => ['lat' => 'lat: %lat%', 'radius' => '%rad%: %radius%', 'accuracy' => 'accuracy: %accuracy%']
        ];
        $this->helper->setCoordinateSet('test', $set);
        $this->assertEquals('lat: 1234,5678,3°0\'0",4°0\'0",%rad%: 100,accuracy: 820', $this->helper->__invoke('ykj', '1234', '5678', 3, 4, 100));
        $this->assertEquals('lat: 1234,5678,3°0\'0",4°0\'0",,accuracy: 720', $this->helper->__invoke('ykj', '1234', '5678', 3, 4, null));
        $this->assertEquals(',5678,3°0\'0",4°0\'0",,', $this->helper->__invoke('ykj', null, '5678', 3, 4, null));
        $this->assertEquals('lat: 1234 N-5678 E----', $this->helper->__invoke('wgs84', '1234', '5678', null, null, null));
    }

    public function testNegativeRadius()
    {
        $set = [
            'ykj' => '%lat%,%lon%,%wgsN%,%wgsE%,%radius%,%accuracy%',
            'default' => '%lat%-%lon%-%wgsN%-%wgsE%-%radius%-%accuracy%'
        ];
        $this->helper->setCoordinateSet('test', $set);
        $this->assertEquals('6650000,3300000,3°0\'0",4°0\'0",-10,10', $this->helper->__invoke('ykj', '6650000', '3300000', 3, 4, '-10'));
    }

}
 