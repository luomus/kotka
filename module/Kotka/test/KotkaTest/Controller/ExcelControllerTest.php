<?php
namespace KotkaTest\Controller;

/**
 * @group ExcelControllerTest
 */
class ExcelControllerTest extends CommonKotkaHttp
{

    public function setUp()
    {
        parent::setUp();
        $this->loginToKotka();
        $cache = $this->getServiceManager()->get('cache');
        $cache->flush();
    }

    public function testFieldDocumentationHasData()
    {
        $this->dispatch('https://' . $this->hostname . '/excel');
        $result = $this->getResponse()->getContent();
        $this->assertContains('MYTaxon', $result, "Taxon field is missing from excel generator page");
        $this->assertContains('MYTypeStatus', $result, "Type status field is missing from excel generator page");
    }

}
 