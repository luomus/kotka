<?php
namespace KotkaTest\Controller;

use Common\Service\IdService;

/**
 * @group ViewController
 */
class ViewControllerTest extends CommonKotkaHttp
{

    const QNAME_PUBLIC = 'luomus:GV.1';
    const QNAME_PROTECTED = 'luomus:GAC.165';
    const QNAME_PRIVATE = 'luomus:GAC.166';
    const QNAME_NOT_FOUND = 'HT.9090901';

    public function setUp()
    {
        parent::setUp();
    }

    public function testWorksWithHttps()
    {
        $this->dispatch('https://' . $this->hostname . '/view', 'GET', array('uri' => IdService::getUri(self::QNAME_PUBLIC)));
        $this->assertResponseStatusCode(200);
    }

    public function testWorksWithQname()
    {
        $this->dispatch('http://' . $this->hostname . '/view', 'GET', array('uri' => IdService::getQName(self::QNAME_PUBLIC)));
        $this->assertResponseStatusCode(200);
    }

    public function testReturns404OnNotFound()
    {
        $this->dispatch('http://' . $this->hostname . '/view', 'GET', array('uri' => IdService::getUri(self::QNAME_NOT_FOUND)));
        $this->assertResponseStatusCode(404);
    }

    public function testWorksWithGuest()
    {
        $this->dispatch('http://' . $this->hostname . '/view', 'GET', array('uri' => IdService::getUri(self::QNAME_PUBLIC)));
        $this->assertResponseStatusCode(200);
        $this->assertQuery('.MYCollectionID');
    }

    public function testProtectedWithGuest()
    {
        $this->dispatch('http://' . $this->hostname . '/view', 'GET', array('uri' => IdService::getUri(self::QNAME_PROTECTED)));
        $this->assertResponseStatusCode(200);
        $this->assertNotQuery('.MYDet');
        $this->assertQuery('.MYDateBegin');
        $this->assertQueryContentContains('.MYDateBegin .view-value', '
                                            1970-80                                    ');
        $this->assertNotQuery('.MYMunicipality');
        $this->assertNotQuery('.MYDocumentLocation');
        $this->assertNotQuery('.MYEditNotes');
    }

    public function testPrivateWithGuest()
    {
        $this->dispatch('http://' . $this->hostname . '/view', 'GET', array('uri' => IdService::getUri(self::QNAME_PRIVATE)));
        $this->assertResponseStatusCode(200);
        $this->assertQuery('.MYTaxonRank.MYTaxon');
        $this->assertNotQuery('#img');
        $this->assertNotQuery('.MYLatitude');
    }

    public function testProtectedWithMember()
    {
        $this->loginToKotka();
        $this->dispatch('http://' . $this->hostname . '/view', 'GET', array('uri' => IdService::getUri(self::QNAME_PROTECTED)));
        $this->assertResponseStatusCode(200);
        $this->assertQuery('.MYDet');
    }

    public function testPrivateWithMember()
    {
        $this->loginToKotka();
        $this->dispatch('http://' . $this->hostname . '/view', 'GET', array('uri' => IdService::getUri(self::QNAME_PRIVATE)));
        $this->assertResponseStatusCode(200);
        $this->assertQueryCount('.MYLeg', 1);
        $this->assertQuery('.MYCountry .view-value');
        $this->assertQuery('.MYRecordBasis');
    }

    /**
     * @dataProvider viewQuestViewProvider
     */
    public function testLoanWithGuest($id, $exists, $missing)
    {
        $this->dispatch('http://' . $this->hostname . '/view', 'GET', array('uri' => $id));
        $this->assertResponseStatusCode(200);
        foreach ($exists as $exist) {
            $this->assertQuery($exist);
        }
        foreach ($missing as $miss) {
            $this->assertNotQuery($miss);
        }
    }

    public function testFetchingInvalidObject()
    {
        $this->dispatch('http://' . $this->hostname . '/view', 'GET', array('uri' => 'MA.person'));
        $this->assertResponseStatusCode(404);
    }

    /**
     * @dataProvider jsonProvider
     * @param $id
     * @param $expectedJson
     */
    public function testJsonResponse($id, $expectedJson)
    {
        $this->dispatch('http://' . $this->hostname . '/view', 'GET', array('uri' => $id, 'format' => 'json'));
        $this->assertResponseStatusCode(200);
        $this->assertResponseStatusCode(200);
        $this->assertJsonStringEqualsJsonString($expectedJson, $this->getResponse()->getContent());
    }

    /**
     * @group NullUser
     */
    public function testAccessDeniedWithNullRole()
    {
        $this->loginToKotka(null);
        $this->dispatch('http://' . $this->hostname . '/view', 'GET', array('uri' => IdService::getUri(self::QNAME_PRIVATE)));
        $this->assertResponseStatusCode(200);
        $this->assertQuery('.MYCountry .view-value');
        $this->assertNotQuery('.MYRecordBasis');
    }

    public function viewQuestViewProvider()
    {
        return [
            ['MA.97', ['.MAInheritedName'], ['.MALTKMLoginName']],
            ['HRA.240', ['.page-header h1'], ['.HRATransactionRequestReceived']],
            ['HR.128', ['.MYCollectionName', '.tree'], []],
        ];
    }

    public function jsonProvider()
    {
        return [
            ['MA.97', '{"uri":"http:\/\/tun.fi\/MA.97","qname":"MA.97","rdf:type":"MA.person","MA.inheritedName":"Riihikoski","MA.preferredName":"Ville-Matti","MA.defaultQNamePrefix":"luomus"}'],
            ['HRA.240', '{"uri":"http:\/\/tun.fi\/HRA.240","qname":"HRA.240","rdf:type":"HRA.transaction"}'],
            ['luomus:GAC.165', '{"uri":"http:\/\/id.luomus.fi\/GAC.165","qname":"luomus:GAC.165","rdf:type":"MY.document","has":[{"qname":"MY.164208","rdf:type":"MY.gathering","MY.country":"Nigeria","MY.dateBegin":"1970-80","MY.higherGeography":"Africa","has":[{"qname":"MY.164209","rdf:type":"MY.unit","has":[{"qname":"MY.164210","rdf:type":"MY.identification","MY.taxonRank":"MX.species","MY.taxon":"Berosus nigerianus","MY.author":"Sch\u00f6dl, 1994"},{"qname":"MY.164211","rdf:type":"MY.typeSpecimen"}]}]}],"noImage":true}'],
            ['luomus:GAC.166', '{"uri":"http:\/\/id.luomus.fi\/GAC.166","qname":"luomus:GAC.166","rdf:type":"MY.document","has":[{"qname":"MY.164214","rdf:type":"MY.gathering","MY.country":"Sudan","MY.dateBegin":"1960-70","MY.higherGeography":"Africa","has":[{"qname":"MY.164215","rdf:type":"MY.unit","has":[{"qname":"MY.164216","rdf:type":"MY.identification","MY.taxonRank":"MX.species","MY.taxon":"Berosus vitticollis","MY.author":"Boheman, 1851"}]}]}],"noImage":true}'],
        ];
    }

}
 