<?php

namespace KotkaTest\Controller;

use Common\Service\IdService;
use Kotka\Triple\MAPerson;
use Triplestore\Model\Model;
use Triplestore\Service\PhpVariableConverter;
use Zend\Mvc\MvcEvent;

abstract class CommonKotkaHttp extends CommonKotka
{
    protected $protocol = KOTKA_ENV === 'dev' ? 'http' : 'https';
    protected $hostname = 'localhost';
    protected $findQnameFromRedirect = true;
    protected $escaper;
    /** @var  \Triplestore\Service\ObjectManager */
    protected $om;
    protected $qname;
    private $langs = array(
        '',
        'En',
        'Fi',
        'Sv'
    );

    public function setUp()
    {
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $_SERVER['HTTP_HOST'] = 'kotkadev.luomus.fi';
        parent::setUp();
    }

    /**
     * This makes sure that no lose ends are left in to the database
     * by unit tests.
     *
     * @param \Exception $e
     * @throws \Exception
     */
    protected function onNotSuccessfulTest(\Exception $e)
    {
        $qname = null;
        if ($this->findQnameFromRedirect) {
            $url = $this->getRedirectLocation();
            $qname = $this->getQnameFromUrl($url);
        }
        if ($qname === null) {
            $qname = $this->qname;
        }
        if ($qname !== null) {
            $this->getObjectManager()->remove($qname);
        }

        throw $e;
    }

    protected function loginToKotka($role = 'MA.member', $organisations = null)
    {
        $em = $this->getApplication()->getEventManager();
        $events = $em->getListeners(MvcEvent::EVENT_ROUTE);
        foreach ($events as $event) {
            $metadata = $event->getMetadata();
            if ($metadata['priority'] == -100) {
                $em->detach($event);
            }
        }
        $this->mockAuthenctication($role, $organisations);
    }

    protected function mockAuthenctication($role = 'MA.member', $organisations = null)
    {
        $user = $this->getUser($role, $organisations);

        $authMock = $this->getMockBuilder('Zend\Authentication\AuthenticationService')
            ->disableAutoload()
            ->getMock();

        $authMock->expects($this->any())
            ->method('getIdentity')
            ->will($this->returnValue($user));
        $authMock->expects($this->any())
            ->method('hasIdentity')
            ->will($this->returnValue(true));

        $sl = $this->getApplicationServiceLocator();
        $sl->setAllowOverride(true);
        $sl->setService('auth', $authMock);
        $sl->setService('user', $user);
    }


    public function getUser($role = 'MA.member', $organisations = null)
    {
        if ($organisations === null) {
            $organisations = [
                'MOS.1024' => 'Testing organization, Luomus, Testing department',
                'MOS.1016' => 'University of Helsinki, Finnish Museum of Natural History, General Services Unit, IT Team'
            ];
        }
        $user = new MAPerson();
        $user->setMALTKMLoginName('TestUser');
        $user->setMAFullName('User... Test User');
        $user->setMAEmailAddress('testuser@kotkadev.luomus.fi');
        $user->setSubject('MA.0');
        $user->setMARoleKotka($role);
        $user->setMAOrganisation($organisations);
        IdService::setDefaultQNamePrefix('tun');
        $user->setMADefaultQNamePrefix('luomus');

        return $user;
    }

    /**
     * Assert model has the data
     *
     * @param Model $model
     * @param array $data
     * @throws \PHPUnit_Framework_ExpectationFailedException
     */
    protected function assertModelHasData(Model $model, array $data)
    {
        unset($data['subject']);
        foreach ($model as $predicate) {
            /** @var \Triplestore\Model\Predicate $predicate $predicate */
            $name = $predicate->getName();
            $phpSafeName = PhpVariableConverter::toPhpMethod($name);
            $results = array();

            foreach ($this->langs as $lang) {
                if (isset($data[$phpSafeName . $lang])) {
                    if (is_array($data[$phpSafeName . $lang])) {
                        $results = array_merge($results, $data[$phpSafeName . $lang]);
                    } else {
                        $results[] = $data[$phpSafeName . $lang];
                    }
                    unset($data[$phpSafeName . $lang]);
                }
            }
            if (empty($results)) {
                continue;
            }
            $regularValue = false;
            foreach ($predicate as $object) {
                /** @var \Triplestore\Model\Object $object */
                $value = $object->getValue();
                if ($value instanceof Model) {
                    $found = false;
                    foreach ($results as $key => $result) {
                        try {
                            $this->assertModelHasData($value, $result);
                            $found = true;
                            unset($results[$key]);
                            break;
                        } catch (\Exception $e) {
                            continue;
                        }
                    }
                    if (!$found) {
                        throw new \PHPUnit_Framework_ExpectationFailedException(sprintf(
                            'Failed asserting model "%s" data not found', serialize($results)
                        ));
                    }
                } else {
                    if (!$regularValue) {
                        $results = array_flip($results);
                        $regularValue = true;
                    }
                    $this->assertArrayHasKey($value, $results, "Could not find value '$value' from model");
                    unset($results[$value]);
                }
            }
            $this->assertEmpty($results, "Some value data was not found from database (" . json_encode($results) . ")");
        }
        $this->assertEmpty($data, "Some data was not found from database (" . json_encode($data) . ")");
    }

    /**
     *
     * @return \Triplestore\Service\ObjectManager
     */
    protected function getObjectManager()
    {
        if ($this->om == null) {
            $this->om = $this->getApplicationServiceLocator()->get('Triplestore\ObjectManager');
            $this->om->disableHydrator();
        }

        return $this->om;
    }

    public function getQnameFromUrl($url)
    {
        if ($url === null) {
            return null;
        }
        $parts = explode('=', $url);
        if (count($parts) > 1) {
            $url = array_pop($parts);
        }
        $qname = IdService::getQName($url);
        return $qname;
    }

    public function dispatchFollow($url, $method = null, $params = array(), $maxFollows = 1)
    {
        $this->dispatch($url, $method, $params);
        $maxFollows--;
        $location = $this->getRedirectLocation();
        if ($maxFollows < 0 || $location == null) {
            return;
        }
        $this->reset();
        $this->loginToKotka();
        $this->dispatchFollow($location, null, array(), $maxFollows);
    }

    public function getRedirectLocation()
    {
        /** @var \Zend\Http\PhpEnvironment\Response $response */
        $response = $this->getResponse();
        $location = null;
        $statusCode = $response->getStatusCode();
        if ($statusCode == '302' || $statusCode == '301') {
            $locationHeader = $response->getHeaders()->get('location');
            if ($locationHeader == false) {
                return null;
            }
            $location = $locationHeader->getUri();
        }
        return $location;
    }
}