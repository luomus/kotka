<?php

namespace KotkaTest\Controller;

use Kotka\Triple\MYDocument;
use Kotka\Triple\MYGathering;
use Kotka\Triple\MYIdentification;
use Kotka\Triple\MYUnit;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;

abstract class CommonKotka extends AbstractHttpControllerTestCase
{
    private static $numericId = 1;
    private static $MYNumericId = 1;

    protected $traceError = true;
    protected $pluginManager;
    protected $serviceManager;
    protected $formElementManager;
    protected $validatorManager;

    public function setUp()
    {
        $this->setApplicationConfig(
            include 'config/application.config.php');
        parent::setUp();
    }

    public function generateDummyMYDocument($namespace = null, $id = null, array $data = array())
    {
        $document = new MYDocument();
        $namespace = $namespace === null ? 'JAA' : $namespace;
        $id = $id === null ? self::$numericId++ : $id;
        $document->setSubject($namespace . '.' . $id);
        $this->setData($document, $data);

        return $document;
    }

    public function generateDummyMYGathering(MYDocument $document = null, array $data = array())
    {
        $gathering = new MYGathering();
        $gathering->setSubject('MY.' . self::$MYNumericId++);
        $this->setData($gathering, $data);
        if ($document !== null) {
            $gatherings = $document->getMYGathering();
            $gatherings[] = $gathering;
            $document->setMYGathering($gatherings);
        }

        return $gathering;
    }

    public function generateDummyMYUnit(MYGathering $gathering = null, array $data = array())
    {
        $unit = new MYUnit();
        $unit->setSubject('MY.' . self::$MYNumericId++);
        $this->setData($unit, $data);
        if ($gathering !== null) {
            $units = $gathering->getMYUnit();
            $units[] = $unit;
            $gathering->setMYUnit($units);
        }

        return $unit;
    }

    public function generateDummyMYIdentification(MYUnit $unit = null, array $data = array())
    {
        $identification = new MYIdentification();
        $identification->setSubject('MY.' . self::$MYNumericId++);
        $this->setData($identification, $data);
        if ($unit !== null) {
            $identifications = $unit->getMYIdentification();
            $identifications[] = $identification;
            $unit->setMYIdentification($identifications);
        }

        return $identification;
    }

    private function setData($object, array $data = array())
    {
        foreach ($data as $method => $value) {
            $method = 'set' . $method;
            $object->$method($value);
        }
    }


    public function overridePlugin($name, $class)
    {
        $this->getPluginManager()->setService($name, $class);
    }

    public function overrideService($name, $class)
    {
        $this->getServiceManager()->setService($name, $class);
    }

    public function getServiceManager()
    {
        if ($this->serviceManager === null) {
            $this->serviceManager = $this->getApplicationServiceLocator();
            $this->serviceManager->setAllowOverride(true);
        }
        return $this->serviceManager;
    }

    public function getPluginManager()
    {
        if ($this->pluginManager === null) {
            $this->pluginManager = $this->getServiceManager()->get('Zend\Mvc\Controller\Plugin\Manager');
            $this->pluginManager->setAllowOverride(true);
        }
        return $this->pluginManager;
    }

    public function overrideFormElement($name, $class)
    {
        $this->getFormElementManager()->setService($name, $class);
    }

    public function getValidatorManager()
    {
        if ($this->validatorManager === null) {
            $this->validatorManager = $this->getServiceManager()->get('ValidatorManager');
            $this->validatorManager->setAllowOverride(true);
        }
        return $this->validatorManager;
    }

    public function getFormElementManager()
    {
        if ($this->formElementManager === null) {
            $this->formElementManager = $this->getServiceManager()->get('FormElementManager');
            $this->formElementManager->setAllowOverride(true);
        }
        return $this->formElementManager;
    }

    protected function convertDateToDBDate($date)
    {
        $date = $this->convertDateTimeToDBDateTime($date);
        if ($date === null) {
            return null;
        }
        return substr($date, 0, 10);

    }

    protected function convertDateTimeToDBDateTime($datetime)
    {
        if ($datetime == null || $datetime == '') {
            return null;
        }
        $datetime = new \DateTime($datetime);
        return $datetime->format(DATE_ISO8601);
    }

    protected function array_remove_empty($haystack)
    {
        foreach ($haystack as $key => $value) {
            if (is_array($value)) {
                $haystack[$key] = $this->array_remove_empty($haystack[$key]);
            }

            if (empty($haystack[$key])) {
                unset($haystack[$key]);
            }
        }

        return $haystack;
    }

}
