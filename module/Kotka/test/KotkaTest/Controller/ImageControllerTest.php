<?php
namespace KotkaTest\Controller;

/**
 * @group ImageController
 */
class ImageControllerTest extends CommonKotkaHttp
{

    public function setUp()
    {
        parent::setUp();
        $this->loginToKotka();
    }

    public function testHttpsRequired()
    {
        if ($this->protocol === 'http') {
            $this->markTestSkipped();
        }
        $this->dispatch('http://' . $this->hostname . '/specimens/images');
        $this->assertNotResponseStatusCode(200);
    }

    public function testIndexActionCanBeAccessed()
    {
        $this->dispatch($this->protocol . '://' . $this->hostname . '/specimens/images');
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('Kotka');
        $this->assertControllerName('Kotka\Controller\Image');
        $this->assertControllerClass('ImageController');
        $this->assertMatchedRouteName('https/image_upload');
    }

}
 