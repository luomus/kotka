<?php
namespace KotkaTest\Controller;

require(__DIR__ . '/../../TestAssets/FileUploads/FileServiceMock.php');

use Kotka\Service\Factory\FileServiceFactory;
use TestAssets\FileUploads\FileServiceMock;
use User\Controller\UserController;
use Zend\Session\Container;

/**
 * @group FileController
 */
class FileControllerTest extends CommonKotkaHttp
{

    const QNAME_LOAN = 'HRA.1142';

    public function setUp()
    {
        parent::setUp();
    }

    public function testNoAccessWithHttp()
    {
        if ($this->protocol === 'http') {
            $this->markTestSkipped();
        }
        $this->loginToKotka();
        $uri = $this->getUri('transaction', self::QNAME_LOAN, null, false);
        $this->dispatch($uri);
        $this->assertResponseStatusCode(404);
    }

    public function testNoAccessToListWithoutLogin()
    {
        $uri = $this->getUri('transaction', self::QNAME_LOAN);
        $this->dispatch($uri);
        $this->assertResponseStatusCode(403);
    }

    public function testAddingToLoanWithoutBelongingToCorrectOrganization()
    {
        $this->loginToKotka('MA.member', array());
        $this->simulateFileUpload('dispatchsheet_HRA.1142.pdf');
        $uri = $this->getUri('transaction', self::QNAME_LOAN);
        $this->dispatch($uri, 'POST', $_FILES, true);
        $this->assertResponseStatusCode(403);
    }

    public function testAddingFileWithCorrectPermissions()
    {
        $file = 'dispatchsheet_HRA.1142.pdf';
        $this->clearFile(self::QNAME_LOAN, $file); // Clear the existing file if for some reason it exists
        $this->loginToKotka();
        $this->simulateFileUpload($file);
        $this->overrideFormElement('Kotka\Form\PdfUpload',
            $this->mockPdfUploadForm($_FILES));
        $this->overrideService('Kotka\Service\FileService',
            new FileServiceMock(FileServiceFactory::PATH));
        $uri = $this->getUri('transaction', self::QNAME_LOAN);
        $this->dispatch($uri, 'POST', $_FILES, true);
        $this->assertResponseStatusCode(201);

        return $file;
    }

    private function clearFile($subject, $file)
    {
        /** @var \Kotka\Service\FileService $fileService */
        $fileService = $this->getServiceManager()->get('Kotka\Service\FileService');
        $fileService->delete($subject, $file);
    }

    /**
     * @depends testAddingFileWithCorrectPermissions
     */
    public function testGettingTheFile($file)
    {
        // TODO: This test leaves the stream open so that the file cannot be deleted a the end
        $this->markTestSkipped('Must be revisited.');
        $this->loginToKotka();
        $uri = $this->getUri('transaction', self::QNAME_LOAN, $file);
        $this->dispatch($uri);
        $this->assertResponseStatusCode(200);
        $this->assertResponseHeaderContains('Content-type', 'application/octet-stream');

        return $file;
    }

    /**
     * @depends testAddingFileWithCorrectPermissions
     */
    public function testGettingTheFileWithoutLogin($file)
    {
        $uri = $this->getUri('transaction', self::QNAME_LOAN, $file);
        $this->dispatch($uri);
        $this->assertResponseStatusCode(403);

        return $file;
    }

    /**
     * @depends testAddingFileWithCorrectPermissions
     */
    public function testDeletingTheFileWithoutLogin($file)
    {
        if (empty($file)) {
            $this->fail('File parameter should not be empty');
        }
        $uri = $this->getUri('transaction', self::QNAME_LOAN, $file);
        $this->dispatch($uri, 'DELETE');
        $this->assertResponseStatusCode(403);

        return $file;
    }

    /**
     * @depends testAddingFileWithCorrectPermissions
     */
    public function testGettingTheFileWithDifferentOrganization($file)
    {
        $this->loginToKotka('MA.member', ['MOS.111' => 'testi organisaatio']);
        $uri = $this->getUri('transaction', self::QNAME_LOAN, $file);
        $this->dispatch($uri);
        $this->assertResponseStatusCode(403);

        return $file;
    }

    /**
     * @depends testAddingFileWithCorrectPermissions
     */
    public function testDeletingTheFileWithDifferentOrganization($file)
    {
        $this->loginToKotka('MA.member', ['MOS.111' => 'testi organisaatio']);
        $uri = $this->getUri('transaction', self::QNAME_LOAN, $file);
        $this->dispatch($uri, 'DELETE');
        $this->assertResponseStatusCode(403);

        return $file;
    }

    /**
     * @depends testAddingFileWithCorrectPermissions
     */
    public function testListHasTheFile($file)
    {
        $this->loginToKotka();
        $uri = $this->getUri('transaction', self::QNAME_LOAN);
        $this->dispatch($uri, 'GET');
        $this->assertResponseStatusCode(200);
        $response = $this->getResponse()->getBody();
        $data = json_decode($response);
        $cnt = count($data);
        $this->assertTrue(in_array($file, $data), "$file was not found in the list of files");

        return ['file' => $file, 'cnt' => $cnt];
    }

    /**
     * @depends testListHasTheFile
     */
    public function testRemovingItem($data)
    {
        $this->loginToKotka();
        $uri = $this->getUri('transaction', self::QNAME_LOAN, $data['file']);
        $this->overrideService('Kotka\Service\FileService',
            new FileServiceMock(FileServiceFactory::PATH));
        $this->dispatch($uri, 'DELETE');
        $this->assertResponseStatusCode(204);

        return $data;
    }

    /**
     * @depends testRemovingItem
     */
    public function testListDoesntHaveItem($data)
    {
        $this->loginToKotka();
        $uri = $this->getUri('transaction', self::QNAME_LOAN);
        $this->dispatch($uri, 'GET');
        $this->assertResponseStatusCode(200);
        $response = $this->getResponse()->getBody();
        $responseData = json_decode($response);
        $cnt = count($responseData);
        $this->assertFalse(in_array($data['file'], $responseData), "{$data['file']} was not removed from the list of files");
        $this->assertEquals(($data['cnt'] - 1), $cnt);

        return $data;
    }

    /**
     * @depends testRemovingItem
     */
    public function testGettingNonExistingFile($data)
    {
        $this->loginToKotka();
        $uri = $this->getUri('transaction', self::QNAME_LOAN, $data['file']);
        $this->dispatch($uri);
        $this->assertResponseStatusCode(404);
    }

    private function simulateFileUpload($file)
    {
        $directory = dirname(dirname(__DIR__)) . '/TestAssets/FileUploads/';
        $path = $directory . $file;
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        if (!file_exists($path)) {
            $this->fail('Missing testAsset ' . $file);
        }
        $_FILES = [
            'file' => [
                'name' => $file,
                'type' => finfo_file($finfo, $path),
                'size' => filesize($path),
                'tmp_name' => $path,
                'error' => 0
            ]
        ];
    }

    private function mockPdfUploadForm($postData)
    {
        $mockImportForm = $this->getMockBuilder('Kotka\Form\PdfUpload')
            ->disableOriginalConstructor()
            ->getMock();
        $mockImportForm->expects($this->once())
            ->method('setData')
            ->with($postData);
        $mockImportForm->expects($this->once())
            ->method('isValid')
            ->will($this->returnValue(true));
        $mockImportForm->expects($this->once())
            ->method('getData')
            ->will($this->returnValue($postData));
        return $mockImportForm;
    }

    private function getUri($type = null, $subject = null, $file = null, $secure = true)
    {
        $uri = $secure ? ($this->protocol . '://') : 'http://';
        $uri .= $this->hostname . '/file/';
        $uri .= is_null($type) ? '' : $type . '/';
        $uri .= is_null($subject) ? '' : $subject . '/';
        $uri .= is_null($file) ? '' : $file;
        return rtrim($uri, '/');
    }

}