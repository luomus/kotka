<?php
namespace KotkaTest\Controller;

/**
 * @group DocumentController
 */
class DocumentControllerTest extends CommonKotkaHttp
{


    const RESOURCE = '__resource__';
    const LITERAL = '__literal__';

    protected $testSubjects = array();
    protected $om;

    public function setUp()
    {
        parent::setUp();
        $this->loginToKotka();
        $cache = $this->getServiceManager()->get('cache');
        $cache->flush();
    }

    public function testFieldDocumentationCanBeAccessed()
    {
        $this->dispatch($this->protocol . '://' . $this->hostname . '/documentation/field');
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('Kotka');
        $this->assertControllerName('Kotka\Controller\Document');
        $this->assertControllerClass('DocumentController');
        $this->assertMatchedRouteName('document');
    }

    public function testFieldDocumentationHasData()
    {
        $this->dispatch($this->protocol . '://' . $this->hostname . '/documentation/field');
        $this->assertQuery('.documentation-database');
        $this->assertQueryCountMin('.documentation-database', 5);
    }

    public function testAllFieldsHaveExplanation()
    {
        $this->dispatch($this->protocol . '://' . $this->hostname . '/documentation/field');
        $body = $this->getResponse()->getContent();
        $this->assertNotContains('Unknown validator', $body, "There is unknown validator on document page! Fix it!");
        $this->assertNotContains('Unknown filter', $body, "There is unknown filter on document page! Fix it!");
    }
}
 