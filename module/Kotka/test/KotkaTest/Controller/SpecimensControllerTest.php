<?php
namespace KotkaTest\Controller;

use Common\Service\IdService;
use Kotka\Triple\MYDocument;
use Kotka\Triple\MYGathering;
use Kotka\Triple\MYIdentification;
use Kotka\Triple\MYUnit;
use Triplestore\Model\Model;


/**
 * @group SpecimensController
 */
class SpecimensControllerTest extends CommonKotkaHttp
{

    private $minSuccess = array(
        'subject' => '',
        'MZOwner' => 'MOS.1024',
        'MYCollectionID' => 'HR.74',
        'status' => 'ok',
        'MYGathering' => array(
            array(
                'MYCountry' => 'Testi maa',
                'MYUnit' => array(
                    array(
                        'MYRecordBasis' => 'MY.recordBasisPreservedSpecimen',
                    )
                )
            )
        ),
    );

    /** @var string This is from luomus becouse dedault domain fot the test user is luomus */
    private $namespace = 'luomus:HT';


    public function setUp()
    {
        parent::setUp();
        $this->loginToKotka();
    }

    public function testClearingDb()
    {
        $om = $this->getObjectManager();
        $om->remove('tun:JAAA.1124232');
        $om->remove('JA.1124231');
        $om->remove('JA.1124233');
    }

    public function testBotanyHttpsRequired()
    {
        if ($this->protocol === 'http') {
            $this->markTestSkipped();
        }
        $this->dispatch('http://' . $this->hostname . '/botany/specimens/add');
        $this->assertNotResponseStatusCode(200);
    }

    public function testZooHttpsRequired()
    {
        if ($this->protocol === 'http') {
            $this->markTestSkipped();
        }
        $this->dispatch('http://' . $this->hostname . '/zoo/specimens/add');
        $this->assertNotResponseStatusCode(200);
    }

    public function testRequiredFields()
    {
        foreach ($this->minSuccess as $key => $value) {
            if ($key == 'subject') {
                continue;
            }
            if (is_array($value)) {
                foreach ($value[0] as $k2 => $v2) {
                    if (is_array($v2)) {
                        foreach ($v2[0] as $k3 => $v3) {
                            $missing = $this->minSuccess;
                            unset($missing[$key][0][$k2][0][$k3]);
                            $this->dispatch($this->protocol . '://' . $this->hostname . '/zoo/specimens/add', 'POST', $missing);
                            $this->assertResponseStatusCode(200);
                            $this->assertQuery('.alert-danger');
                        }
                    } else {
                        $missing = $this->minSuccess;
                        unset($missing[$key][0][$k2]);
                        $this->dispatch($this->protocol . '://' . $this->hostname . '/zoo/specimens/add', 'POST', $missing);
                        $this->assertResponseStatusCode(200);
                        $this->assertQuery('.alert-danger');
                    }
                }
            } else {
                $missing = $this->minSuccess;
                unset($missing[$key]);
                $this->dispatch($this->protocol . '://' . $this->hostname . '/zoo/specimens/add', 'POST', $missing);
                $this->assertResponseStatusCode(200);
                $this->assertQuery('.alert-danger');
            }
        }
    }

    /**
     * @group SaveSpecimenMin
     */
    public function testSuccessWithEmptyNamespaceAndObject()
    {
        $this->expectSuccessfulSave();
        $postData = $this->minSuccess;
        $postData['MYNamespaceID'] = '';
        $postData['MYObjectID'] = '';

        $this->dispatch($this->protocol . '://' . $this->hostname . '/zoo/specimens/add', 'POST', $postData);
        $this->assertResponseStatusCode(302);

        $location = $this->getRedirectLocation();
        $this->qname = $this->getQnameFromUrl($location);
        $this->assertTrue(strpos($this->qname, $this->namespace . '.') === 0, 'Wrong namespace with created item');
        $om = $this->getObjectManager();
        $om->remove($this->qname);
    }

    /**
     * @group SaveSpecimenMin
     */
    public function testSuccessWithMinim()
    {
        $this->expectSuccessfulSave();

        $this->dispatch($this->protocol . '://' . $this->hostname . '/zoo/specimens/add', 'POST', $this->minSuccess);
        $this->assertResponseStatusCode(302);

        $location = $this->getRedirectLocation();
        $this->qname = $this->getQnameFromUrl($location);
        $this->assertTrue(strpos($this->qname, $this->namespace . '.') === 0, 'Wrong namespace with created item');
        return $this->qname;
    }

    /**
     * @depends testSuccessWithMinim
     * @group SaveSpecimenMin
     */
    public function testAddingIdentification($qname)
    {
        $this->qname = $qname;
        $postData = $this->minSuccess;
        $postData['subject'] = $qname;
        $postData['MYGathering'][0]['MYUnit'][0]['MYIdentification'][0]['MYTaxon'] = 'ääkkönen';

        $this->dispatch($this->protocol . '://' . $this->hostname . '/zoo/specimens/edit?uri=' . $this->qname, 'POST', $postData);
        $this->assertResponseStatusCode(302);

        return array('qname' => $qname, 'data' => $postData);
    }

    /**
     * @depends testAddingIdentification
     * @group SaveSpecimenMin
     */
    public function testEditingDataInDocument($data)
    {
        $this->qname = $data['qname'];
        $postData = $data['data'];
        $postData['subject'] = $this->qname;
        $postData['MYGathering'][0]['MYUnit'][0]['MYIdentification'][1]['MYTaxon'] = 'jääkkönen';
        $postData['MYGathering'][0]['MYUnit'][0]['MYTypeSpecimen'][0]['MYTypeSpecies'] = 'öökkeset';

        $this->dispatch($this->protocol . '://' . $this->hostname . '/zoo/specimens/edit?uri=' . $this->qname, 'POST', $postData);
        $this->assertResponseStatusCode(302);

        return array('qname' => $this->qname, 'data' => $postData);
    }

    /**
     * @depends testEditingDataInDocument
     * @group SaveSpecimenMin
     */
    public function testDocumentHasElements($data)
    {
        $this->qname = $data['qname'];
        $postData = $data['data'];
        $getData = array(
            'uri' => $this->qname
        );
        $this->dispatch($this->protocol . '://' . $this->hostname . '/zoo/specimens/edit', 'GET', $getData);
        $this->assertResponseStatusCode(200);

        return array('qname' => $this->qname, 'data' => $postData);
    }

    /**
     * This test if the database has the actual data and then cleans it up
     * @depends testDocumentHasElements
     * @group SaveSpecimenMin
     */
    public function testDatabaseHasCorrectData($data)
    {
        $this->qname = $data['qname'];
        $itemData = $data['data'];
        $itemData = $this->convertDataToDatabaseData($itemData);
        $om = $this->getObjectManager();
        $om->disableHydrator();
        $item = $om->fetch($this->qname);

        $this->assertNotNull($item);
        $this->assertModelHasData($item, $itemData);

        $om->remove($item);

        return array($data);
    }

    public function testAddingCustomTunDomainNamespaceAndId()
    {
        $this->expectSuccessfulSave();

        $postData = $this->minSuccess;
        $postData['MYNamespaceID'] = 'tun:JAAA';
        $postData['MYObjectID'] = '1124232';
        $this->dispatch($this->protocol . '://' . $this->hostname . '/zoo/specimens/add', 'POST', $postData);
        $this->assertResponseStatusCode(302);

        $location = $this->getRedirectLocation();
        $this->qname = $this->getQnameFromUrl($location);
        $this->assertEquals($postData['MYNamespaceID'] . '.' . $postData['MYObjectID'], $this->qname, 'Namespace and object id didn\'t match');
        $om = $this->getObjectManager();
        $om->remove($this->qname);
    }

    public function testAddingCustomLuomusDomainNamespaceAndId()
    {
        $this->expectSuccessfulSave();

        $postData = $this->minSuccess;
        $postData['MYNamespaceID'] = 'luomus:JA';
        $postData['MYObjectID'] = '1124233';
        $this->dispatch($this->protocol . '://' . $this->hostname . '/zoo/specimens/add', 'POST', $postData);
        $this->assertResponseStatusCode(302);

        $location = $this->getRedirectLocation();
        $this->qname = $this->getQnameFromUrl($location);
        $this->assertEquals($postData['MYNamespaceID'] . '.' . $postData['MYObjectID'], $this->qname, 'Namespace and object id didn\'t match');
        $om = $this->getObjectManager();
        $om->remove($this->qname);
    }

    public function testAddingCustomNamespaceAndId()
    {
        $this->expectSuccessfulSave();

        $postData = $this->minSuccess;
        $postData['MYNamespaceID'] = 'JA';
        $postData['MYObjectID'] = '1124231';
        $this->dispatch($this->protocol . '://' . $this->hostname . '/zoo/specimens/add', 'POST', $postData);
        $this->assertResponseStatusCode(302);

        $location = $this->getRedirectLocation();
        $this->qname = $this->getQnameFromUrl($location);
        $this->assertEquals(IdService::getDefaultQNamePrefix() . ':' . $postData['MYNamespaceID'] . '.' . $postData['MYObjectID'], $this->qname, 'Namespace and object id didn\'t match');
        $om = $this->getObjectManager();
        $om->remove($this->qname);
    }

    public function testAddingUnreliableField()
    {
        $this->expectSuccessfulSave();

        $postData = $this->minSuccess;
        $postData['MYCollectionID'] = '? HR.138';

        $this->dispatch($this->protocol . '://' . $this->hostname . '/zoo/specimens/add', 'POST', $postData);
        $this->assertResponseStatusCode(302);
        $location = $this->getRedirectLocation();
        $this->qname = $this->getQnameFromUrl($location);

        $om = $this->getObjectManager();
        $om->enableHydrator();
        /** @var MYDocument $model */
        $model = $om->fetch($this->qname);
        $this->assertEquals('MYCollectionID', $model->getMYUnreliableFields());
        $this->assertEquals('HR.138', $model->getMYCollectionID());
        $om->remove($this->qname);
    }

    private function expectSuccessfulSave()
    {
        $mockFlashMessage = $this->getMockBuilder('Zend\Mvc\Controller\Plugin\FlashMessenger')
            ->disableOriginalConstructor()
            ->getMock();
        $mockFlashMessage->expects($this->once())
            ->method('addSuccessMessage')
            ->with('<strong>Record successfully saved into database.</strong> Below is the saved record: you can view or edit it here.');
        $this->overridePlugin('flashMessenger', $mockFlashMessage);
    }

    public function testAddingTwoIdentifications()
    {
        $postData = $this->minSuccess;
        $postData['MYGathering'][0]['MYUnit'][0]['MYIdentification'][1] = array(
            'subject' => '',
            'MYTaxon' => 'testRemoveIdentification'
        );
        $this->dispatch($this->protocol . '://' . $this->hostname . '/zoo/specimens/add', 'POST', $postData);
        $this->assertResponseStatusCode(302);
        $location = $this->getRedirectLocation();
        $this->qname = $this->getQnameFromUrl($location);

        return array('qname' => $this->qname, 'data' => $postData);
    }

    /**
     * @depends testAddingTwoIdentifications
     */
    public function testAddedIdentificationsOnTheForm($data)
    {
        $this->qname = $data['qname'];
        $postData = $data['data'];
        $getData = array(
            'uri' => $this->qname
        );
        $this->dispatch($this->protocol . '://' . $this->hostname . '/zoo/specimens/edit', 'GET', $getData);
        $this->assertResponseStatusCode(200);
        $this->assertQuery('input[value="testRemoveIdentification"]');

        return array('qname' => $this->qname, 'data' => $postData);
    }

    /**
     * @depends testAddedIdentificationsOnTheForm
     */
    public function testRemovingIdentification($data)
    {
        $this->qname = $data['qname'];
        $postData = $data['data'];
        $postData['subject'] = $this->qname;
        unset($postData['MYGathering'][0]['MYUnit'][0]['MYIdentification'][1]);
        $this->dispatch($this->protocol . '://' . $this->hostname . '/zoo/specimens/edit?uri=' . $this->qname, 'POST', $postData);
        $this->assertResponseStatusCode(302);

        return $this->qname;
    }

    /**
     * @depends testRemovingIdentification
     */
    public function testRemovedIdentificationIsConeFromView($qname)
    {
        $this->qname = $qname;
        $getData = array(
            'uri' => $this->qname
        );
        $this->dispatch($this->protocol . '://' . $this->hostname . '/zoo/specimens/edit', 'GET', $getData);
        $this->assertResponseStatusCode(200);

        $page = str_replace("\n", '', $this->getRequest()->getContent());
        $this->assertNotContains('testRemoveIdentification', $page);

        $om = $this->getObjectManager();
        $om->remove($this->qname);

    }

    /* Field not in form not possible as of 3.0 since context dosn't contain the whole resultset
    /**
     * @dataProvider notInFormProvider

    public function testFieldNotForm($data, $field) {
        $postData = array_replace_recursive($this->minSuccess, $data);
        $this->dispatch($this->protocol . '://' . $this->hostname . '/zoo/specimens/add', 'POST', $postData);
        $this->assertResponseStatusCode(200);
        $this->assertQueryContentRegex('div.alert-danger ul li', "{.*$field.*}");
    }
    */

    public function testAddingWithSetSubjects()
    {
        $this->expectSuccessfulSave();
        $postData = $this->minSuccess;
        $postData['MYGathering'][0]['subject'] = 'MY.331';
        $postData['MYGathering'][0]['MYUnit'][0]['subject'] = 'MY.332';
        $postData['MYGathering'][0]['MYUnit'][0]['MYIdentification'][0]['subject'] = 'MY.333';
        $postData['MYGathering'][0]['MYUnit'][0]['MYIdentification'][0]['MYTaxon'] = 'taxon';
        $this->dispatch($this->protocol . '://' . $this->hostname . '/zoo/specimens/add', 'POST', $postData);
        $this->assertResponseStatusCode(302);
        $location = $this->getRedirectLocation();
        $this->qname = $this->getQnameFromUrl($location);

        $om = $this->getObjectManager();
        $om->enableHydrator();

        $subject = [];
        /** @var MYDocument $document */
        $document = $om->fetch($this->qname);
        $this->assertInstanceOf('Kotka\Triple\MYDocument', $document);
        /** @var MYGathering $gathering */
        $gathering = $document->getMYGathering()[0];
        $subject['MYGathering'] = $gathering->getSubject();
        $this->assertInstanceOf('Kotka\Triple\MYGathering', $gathering);
        $this->assertNotEquals($postData['MYGathering'][0]['subject'], $subject['MYGathering']);
        /** @var MYUnit $unit */
        $unit = $gathering->getMYUnit()[0];
        $subject['MYUnit'] = $unit->getSubject();
        $this->assertInstanceOf('Kotka\Triple\MYUnit', $unit);
        $this->assertNotEquals($postData['MYGathering'][0]['MYUnit'][0]['subject'], $subject['MYUnit']);
        /** @var MYIdentification $identification */
        $identification = $unit->getMYIdentification()[0];
        $subject['MYIdentification'] = $identification->getSubject();
        $this->assertInstanceOf('Kotka\Triple\MYIdentification', $identification);
        $this->assertNotEquals($postData['MYGathering'][0]['MYUnit'][0]['MYIdentification'][0]['subject'], $subject['MYIdentification']);
        $this->assertEquals($postData['MYGathering'][0]['MYUnit'][0]['MYIdentification'][0]['MYTaxon'], $identification->getMYTaxon());

        return ['qname' => $this->qname, 'subject' => $subject, 'postdata' => $postData];
    }

    /**
     * @depends testAddingWithSetSubjects
     */
    public function testEditKeepsTheSubjects($data)
    {
        $this->expectSuccessfulSave();
        $this->qname = $data['qname'];
        $postData = $data['postdata'];
        $postData['subject'] = $this->qname;
        $subject = $data['subject'];
        $postData['MYGathering'][0]['MYUnit'][0]['MYIdentification'][0]['MYTaxon'] = 'edited';
        $this->dispatch($this->protocol . '://' . $this->hostname . '/zoo/specimens/edit?uri=' . $this->qname, 'POST', $postData);
        $this->assertResponseStatusCode(302);

        $om = $this->getObjectManager();
        $om->enableHydrator();

        /** @var MYDocument $document */
        $document = $om->fetch($this->qname);
        $this->assertInstanceOf('Kotka\Triple\MYDocument', $document);
        /** @var MYGathering $gathering */
        $gathering = $document->getMYGathering()[0];
        $this->assertInstanceOf('Kotka\Triple\MYGathering', $gathering);
        $this->assertEquals($subject['MYGathering'], $gathering->getSubject());
        /** @var MYUnit $unit */
        $unit = $gathering->getMYUnit()[0];
        $this->assertInstanceOf('Kotka\Triple\MYUnit', $unit);
        $this->assertEquals($subject['MYUnit'], $unit->getSubject());
        /** @var MYIdentification $identification */
        $identification = $unit->getMYIdentification()[0];
        $this->assertInstanceOf('Kotka\Triple\MYIdentification', $identification);
        $this->assertEquals($subject['MYIdentification'], $identification->getSubject());
        $this->assertEquals($postData['MYGathering'][0]['MYUnit'][0]['MYIdentification'][0]['MYTaxon'], $identification->getMYTaxon());

        $om->remove($this->qname);
    }

    public function testAddingTwoUnits()
    {
        $postData = $this->minSuccess;
        $postData['MYGathering'][0]['MYUnit'][1] = array(
            'subject' => '',
            'MYRecordBasis' => 'MY.recordBasisPreservedSpecimen',
            'MYLifeStageDescription' => 'AnotherUnit'
        );
        $this->dispatch($this->protocol . '://' . $this->hostname . '/zoo/specimens/add', 'POST', $postData);
        $this->assertResponseStatusCode(302);
        $location = $this->getRedirectLocation();
        $this->qname = $this->getQnameFromUrl($location);
        return array('qname' => $this->qname, 'data' => $postData);
    }

    /**
     * @depends testAddingTwoUnits
     */
    public function testFormHasTwoUnits($data)
    {
        $this->qname = $data['qname'];
        $getData = array(
            'uri' => $this->qname
        );
        $this->dispatch($this->protocol . '://' . $this->hostname . '/zoo/specimens/edit', 'GET', $getData);
        $this->assertResponseStatusCode(200);
        $this->assertQuery('input[value="AnotherUnit"]');

        return $data;
    }

    /**
     * @depends testFormHasTwoUnits
     */
    public function testRemoveSecondUnit($data)
    {
        $this->qname = $data['qname'];
        $postData = $data['data'];
        unset($postData['MYGathering'][0]['MYUnit'][1]);
        $om = $this->getObjectManager();
        $om->disableHydrator();
        /** @var Model $item */
        $item = $om->fetch($this->qname);
        $om->enableHydrator();
        $gathering = $item->getPredicate('MY.gathering')->current()->getValue();
        $unit = $gathering->getPredicate('MY.unit')->current()->getValue();
        $postData['subject'] = $item->getSubject();
        $postData['MYGathering'][0]['subject'] = $gathering->getSubject();
        $postData['MYGathering'][0]['MYUnit'][0]['subject'] = $unit->getSubject();
        $this->dispatch($this->protocol . '://' . $this->hostname . '/zoo/specimens/edit?uri=' . $this->qname, 'POST', $postData);
        $this->assertResponseStatusCode(302);

        return $data;
    }

    /**
     * @depends testRemoveSecondUnit
     */
    public function testSecondUnitRemovedFromView($data)
    {
        $this->qname = $data['qname'];
        $getData = array(
            'uri' => $this->qname
        );
        $this->dispatch($this->protocol . '://' . $this->hostname . '/zoo/specimens/edit', 'GET', $getData);
        $this->assertResponseStatusCode(200);
        $this->assertNotQuery('input[value="AnotherUnit"]');

        $om = $this->getObjectManager();
        $om->remove($this->qname);
    }

    public function testFieldWithWrongValue()
    {
        $postData = $this->minSuccess;
        $invalid = 'KJ.59';
        $postData['MYCollectionID'] = $invalid;
        $this->dispatch($this->protocol . '://' . $this->hostname . '/zoo/specimens/add', 'POST', $postData);
        $this->assertResponseStatusCode(200);
        $this->assertQueryContentRegex('div.alert-danger ul li', "{^Value '$invalid' is not.*}");
    }

    public function testAddingAccosiatedTaxa()
    {
        $postData = $this->minSuccess;
        $taxas = ['AnotherTaxa', 'TestValue'];
        $postData['MYGathering'][0]['MYUnit'][0]['MYIdentification'][0] = array(
            'subject' => '',
            'MYAssociatedObservationTaxa' => implode(';', $taxas)
        );
        $this->dispatch($this->protocol . '://' . $this->hostname . '/zoo/specimens/add', 'POST', $postData);
        $this->assertResponseStatusCode(302);
        $location = $this->getRedirectLocation();
        $this->qname = $this->getQnameFromUrl($location);
        return array('qname' => $this->qname, 'data' => $postData, 'taxas' => $taxas);
    }

    /**
     * @depends testAddingAccosiatedTaxa
     */
    public function testFormHasNewUnitsFromAccosiatedTaxa($data)
    {
        $this->qname = $data['qname'];
        $plugins = $this->getServiceManager()->get('ControllerPluginManager');
        $store = $plugins->get('StoreOverRequest');
        $store->store('postdata', ['subject' => $this->qname]);
        $getData = array(
            'uri' => $this->qname
        );
        $taxas = $data['taxas'];
        $this->dispatch($this->protocol . '://' . $this->hostname . '/zoo/specimens/edit', 'GET', $getData);
        $this->assertResponseStatusCode(200);
        $this->assertQueryCount('.partial-unit', count($taxas) + 1);
        foreach ($taxas as $taxa) {
            $this->assertQuery('input[value="' . $taxa . '"]');
        }

        $om = $this->getObjectManager();
        $om->remove($this->qname);
    }

    /**
     * @dataProvider failingProvider
     */
    public function testValidation($data, $type)
    {
        $postData = array_merge($this->minSuccess, $data);
        $this->dispatch($this->protocol . '://' . $this->hostname . '/' . $type . '/specimens/add', 'POST', $postData);
        $this->assertResponseStatusCode(200);
        $this->assertQuery('.alert-danger');
    }

    public function failingProvider()
    {
        return array(
            array(array(
                'MYGathering' => array(
                    array(
                        'MYUnit' => array(
                            array(
                                'MYRecordBasis' => 'NOT THERE'
                            )
                        )
                    )
                ),
            ), 'zoo'),
            array(array(
                'MYNamespaceID' => 'HA',
                'MYObjectID' => '556644',
            ), 'zoo'),
            array(array(
                'MYNamespaceID' => 'HV',
                'MYObjectID' => '556644',
            ), 'botany'),
            array(array(
                'MYObjectID' => '556644',
            ), 'botany'),
            array(array(
                'MYNamespaceID' => 'JA',
            ), 'zoo'),
        );
    }

    public function notInFormProvider()
    {
        return array(
            array(
                array(
                    'MYNotThere' => '1234',
                ),
                'MYNotThere'
            ),
            array(
                array(
                    'MYGathering' => array(
                        array(
                            'MYHmmm' => '1234'
                        )
                    ),
                ),
                'MYHmmm'
            ),
            array(
                array(
                    'MYGathering' => array(
                        array(
                            'MYUnit' => array(
                                array(
                                    'MYMissing' => 'value'
                                )
                            )
                        )
                    ),
                ),
                'MYMissing'
            ),
            array(
                array(
                    'MYGathering' => array(
                        array(
                            'MYUnit' => array(
                                array(
                                    'MYIdentification' => array(
                                        array(
                                            'MYFail' => 'value'
                                        )
                                    )
                                )
                            )
                        )
                    ),
                ),
                'MYFail'
            ),
            array(
                array(
                    'MYGathering' => array(
                        array(
                            'MYUnit' => array(
                                array(
                                    'MYTypeSpecimen' => array(
                                        array(
                                            'MYTypos' => 'value'
                                        )
                                    )
                                )
                            )
                        )
                    ),
                ),
                'MYTypos'
            ),
        );
    }

    private function convertDataToDatabaseData(array $data)
    {
        if (isset($data['status'])) {
            unset($data['status']);
        }
        return $data;
    }
}
 