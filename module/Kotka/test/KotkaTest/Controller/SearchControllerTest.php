<?php
namespace KotkaTest\Controller;

/**
 * @group SearchController
 */
class SearchControllerTest extends CommonKotkaHttp
{

    public function setUp()
    {
        parent::setUp();
        $this->loginToKotka();
    }

    public function testHttpsRequired()
    {
        if ($this->protocol === 'http') {
            $this->markTestSkipped();
        }
        $this->dispatch('http://' . $this->hostname . '/specimens/search');
        $this->assertNotResponseStatusCode(200);
    }

    public function testSearchActionCanBeAccessed()
    {
        $this->dispatch($this->protocol . '://' . $this->hostname . '/specimens/search');
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('Kotka');
        $this->assertControllerName('Kotka\Controller\Search');
        $this->assertControllerClass('SearchController');
        $this->assertMatchedRouteName('https/search_specimens');
    }

    public function testSearchShowsResults()
    {
        $get = array('export' => '');
        $this->dispatch($this->protocol . '://' . $this->hostname . '/specimens/search/table', 'GET', $get);
        $this->assertResponseStatusCode(200);
        $this->assertQuery('#documentQName');
    }

    public function testCanFindItem()
    {
        $this->markTestSkipped('must be revisited when elastic is updated.');
        $get = array('identifier' => 'GV.4');
        $this->dispatch($this->protocol . '://' . $this->hostname . '/specimens/search/table', 'GET', $get);
        $this->assertQueryCount('.document-uri', 1);
    }

    public function testCanFindItems()
    {
        $this->markTestSkipped('must be revisited when elastic is updated.');
        $get = array('identifier' => 'GV.99-102');
        $this->dispatch($this->protocol . '://' . $this->hostname . '/specimens/search/table', 'GET', $get);
        $this->assertQueryCount('.document-uri', 4);
    }

    public function testExportToExcel()
    {
        $this->markTestSkipped('must be revisited when elastic is updated.');
        $get = array('identifier' => 'HT.57-60', 'export' => 'excel');
        $this->dispatch($this->protocol . '://' . $this->hostname . '/specimens/search', 'GET', $get);
        $this->assertResponseStatusCode(200);
        $this->assertResponseHeaderContains('Content-type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    }

    /**
     * @dataProvider typeProvider
     */
    public function testPdfLoading($type)
    {
        $this->markTestSkipped('must be revisited when elastic is updated.');
        $get = array('identifier' => 'HT.57-60', 'export' => 'pdf', 'labelFields' => array('country', 'province', 'municipality'), 'format' => $type);
        $this->dispatch($this->protocol . '://' . $this->hostname . '/specimens/search', 'GET', $get);
        $this->assertResponseStatusCode(200);
        $this->assertResponseHeaderContains('Content-type', 'application/pdf');
    }

    public function excluededTestDificultPDF()
    {
        $this->markTestSkipped('must be revisited when elastic is updated.');
        $type = 'bryophyte';
        $get = array('identifier' => 'JA.543', 'export' => 'pdf', 'labelFields' => array('country', 'province', 'municipality'), 'displayFormat' => $type);
        $this->dispatch($this->protocol . '://' . $this->hostname . '/specimens/search', 'GET', $get);
        $this->assertResponseStatusCode(200);
        $this->assertResponseHeaderContains('Content-type', 'application/pdf');
    }

    public function typeProvider()
    {
        return array(
            array('bryophyte'),
            array('vascular'),
            array('insecta_v2'),
            array('insecta_det_v2'),
            array('temporary'),
            array('invertebrate_tube'),
            array('invertebrate_box'),
            array('slide'),
            array('vertebrate_1'),
            array('vertebrate_2'),
        );
    }

    /**
     * @dataProvider identifierProvider
     */
    public function testIdentifier($identifier, $success, $resultCnt = false)
    {
        $this->markTestSkipped('must be revisited when elastic is updated.');
        $get = array('identifier' => $identifier, 'export' => '');
        $this->dispatch($this->protocol . '://' . $this->hostname . '/specimens/search/table', 'POST', $get);
        if ($success) {
            $this->assertNotQuery('.error-message');
        } else {
            $this->assertQueryContentContains('.error-message', "Invalid identifier '$identifier' given.");
        }
        if ($resultCnt) {
            $this->assertQueryCount('.document-uri', $resultCnt);
        }
    }

    public function identifierProvider()
    {
        return array(
            // Identification string, successful, [Optional] number of results
            array('GV.10-100', true),
            array('GV', true),
            array('http://id.luomus.fi/GV.3', true, 1),
            array('http://id.luomus.fi/GV.3,http://id.luomus.fi/GV.4', true, 2),
            array('http://id.luomus.fi/GV.3, http://id.luomus.fi/GV.4', true, 2),
            array('GV.3,GV.4', true),
            array('GV.3,GV.10-100', true),
            array('GV-', true),
            array('GV.2-', true),
            array('GV.100-10', true),
            array('.-', false),
        );
    }

}
 