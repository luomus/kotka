<?php
namespace KotkaTest\Controller;

/**
 * @group FormPartialController
 */
class FormPartialControllerTest extends CommonKotkaHttp
{

    public function setUp()
    {
        parent::setUp();
        $this->loginToKotka();
    }

    /* Commented out because gathering is no longer repeating
     * public function testGettingGathering() {
        $this->dispatch('https://' . $this->hostname . '/form/MYGathering', 'GET', array('name' => 'MYDocument'));
        $this->assertResponseStatusCode(200);
        $this->assertQuery('#MYGathering___gathering___');
    }*/

    public function testGettingUnit()
    {
        $this->dispatch('https://' . $this->hostname . '/form/MYUnit', 'GET', array('name' => 'MYGathering[2]'));
        $this->assertResponseStatusCode(200);
        $this->assertQuery('#MYGathering_2__MYUnit____unit____MYIdentification__0_');
    }

    public function testGettingIdentification()
    {
        $this->dispatch('https://' . $this->hostname . '/form/MYIdentification', 'GET', array('name' => 'MYGathering[3][MYUnit][4]'));
        $this->assertResponseStatusCode(200);
        $this->assertQuery('#MYGathering_3__MYUnit__4__MYIdentification____identification___');
    }

    public function testGettingType()
    {
        $this->dispatch('https://' . $this->hostname . '/form/MYTypeSpecimen', 'GET', array('name' => 'MYGathering[3][MYUnit][4]'));
        $this->assertResponseStatusCode(200);
        $this->assertQuery('#MYGathering_3__MYUnit__4__MYTypeSpecimen____type___');
    }


}
 