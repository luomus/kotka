<?php
namespace KotkaTest\Controller;

/**
 * @group DatasetsController
 */
class DatasetsControllerTest extends CommonKotkaHttp
{

    private $minSuccess = array(
        'subject' => '',
        'MZOwner' => 'MOS.1024',
        'GXDatasetNameEn' => 'testDataset',
        'GXPersonsResponsible' => 'Mad Max',
        'status' => 'ok',
    );

    private $namespace = 'tun:GX';

    public function setUp()
    {
        parent::setUp();
        $this->loginToKotka();
    }

    public function testHttpsRequired()
    {
        if ($this->protocol === 'http') {
            $this->markTestSkipped();
        }
        $this->dispatch('http://' . $this->hostname . '/datasets');
        $this->assertNotResponseStatusCode(200);
    }

    public function testIndexActionCanBeAccessed()
    {
        $this->dispatch($this->protocol . '://' . $this->hostname . '/datasets');
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('Kotka');
        $this->assertControllerName('Kotka\Controller\Datasets');
        $this->assertControllerClass('DatasetsController');
        $this->assertMatchedRouteName('https/datasets');
    }

    public function testIndexHasItems()
    {
        $this->dispatch($this->protocol . '://' . $this->hostname . '/datasets');
        $this->assertQueryCountMin('.table tr', 10);
    }

    public function testRequiredFields()
    {
        foreach ($this->minSuccess as $key => $value) {
            if ($key == 'subject') {
                continue;
            }
            $missing = $this->minSuccess;
            unset($missing[$key]);
            $this->dispatch($this->protocol . '://' . $this->hostname . '/datasets/add', 'POST', $missing);
            $this->assertResponseStatusCode(200);
            $this->assertQuery('.alert-danger');
        }
    }

    public function testSuccessWithMinim()
    {
        $mockFlashMessage = $this->getMockBuilder('Zend\Mvc\Controller\Plugin\FlashMessenger')
            ->disableOriginalConstructor()
            ->getMock();
        $mockFlashMessage->expects($this->once())
            ->method('addSuccessMessage')
            ->with('<strong>Record successfully saved into database.</strong> Below is the saved record: you can view or edit it here.');
        $this->overridePlugin('flashMessenger', $mockFlashMessage);

        $this->dispatch($this->protocol . '://' . $this->hostname . '/datasets/add', 'POST', $this->minSuccess);
        $this->assertResponseStatusCode(302);

        $location = $this->getRedirectLocation();
        $qname = $this->getQnameFromUrl($location);
        $this->assertTrue(strpos($qname, $this->namespace . '.') === 0, 'Wrong namespace with created item');
        $om = $this->getObjectManager();
        $item = $om->fetch($qname);
        $this->assertNotNull($item);
        $om->remove($item);
    }

    public function testEditAccessible()
    {
        $qname = 'GX.7';
        $this->dispatch($this->protocol . '://' . $this->hostname . '/datasets/edit?uri=' . $qname);
        $this->assertResponseStatusCode(200);
        $this->assertQuery('input[value="ok"]');
    }

    /* Field not in form not possible as of 3.0 since context dosn't contain the whole resultset
    public function testFieldNotInForm() {
        $postData   = $this->minSuccess;
        $wrongField = 'NOTThere';
        $postData[$wrongField] = '1234';
        $this->dispatch($this->protocol . '://' . $this->hostname . '/datasets/add', 'POST', $postData);
        $this->assertResponseStatusCode(200);
        $this->assertQueryContentRegex('div.alert-danger ul li', "{.*$wrongField.*}");
    }
    */

    public function testValidation()
    {
        $postData = $this->minSuccess;
        $postData['GXDatasetNameEn'] = 'Saikion nisäkkäät';
        $this->dispatch($this->protocol . '://' . $this->hostname . '/datasets/add', 'POST', $postData);
        $this->assertResponseStatusCode(200);
        $this->assertQuery('.alert-danger');
    }
}
 