<?php
namespace KotkaTest\Controller;

use Common\Service\IdService;
use Kotka\Controller\ToolsController;
use Kotka\Job\ImportExcel;
use Kotka\Service\ImportService;
use Triplestore\Classes\MAPersonInterface;
use Triplestore\Model\Model;
use Triplestore\Model\Object;

/**
 * @group ToolsController
 */
class ToolsControllerTest extends CommonKotkaHttp
{


    const RESOURCE = '__resource__';
    const LITERAL = '__literal__';
    const NO_VALUE = '__empty__';

    public static $testSubjects = [];
    /** @var \Triplestore\Service\ObjectManager */
    protected $om;

    protected $checkThatValueExists = array(
        'luomus:JA.11123' => array(
            'MY.latitude' => '65',
            'MY.longitude' => '25',
            'MY.recordBasis' => 'MY.recordBasisFossilSpecimen',
            'MY.editor' => 'Tunematon, Sotilas',
            'MY.entered' => '21.05.2014',
            'MZ.editor' => 'MA.0',
            'MY.notes' => "Kenttä\njossa rivi vaihto",
        ),
        'luomus:JA.11124' => array(),
        'luomus:JA.11125' => array(
            'MY.taxon' => array(
                'Eka identification',
                'Toka identification'
            ),
        ),
        'luomus:JAA.11123' => array(
            'MY.typeStatus' => array(
                'MY.typeStatusSyntype',
                'MY.typeStatusSyntype'
            ),
            'MZ.owner' => 'MOS.1024',
            'MY.taxon' => 'Jokin nimi',
            'MY.publication' => array(
                'publication',
                'unsure'
            ),
            'MY.datasetID' => array(
                'GX.10',
                'GX.22',

            ),
            'MY.recordBasis' => 'MY.recordBasisFossilSpecimen',
            'MY.taxonRank' => 'MX.family',
            'MY.unreliableFields' => "MYPublication[0]\nMYPublication[1]\nMYGathering[0][MYUnit][0][MYIdentification][0][MYTaxon]\nMYGathering[0][MYUnit][0][MYRecordBasis]"
        ),
        'luomus:JAA.11124' => array(
            'MY.taxonRank' => 'MX.species',
            'MY.typeBasionymePubl' => "Raatikainen, M., 1961: Dicondylus helleni n. sp. Hym., Dryinidae, a parasite of Calligypona sordidula Stal and C. excisa Mel.. Annales Entomologici Fennici, 27: 126-137",
            'MY.datasetID' => self::NO_VALUE,
        ),
        'luomus:GAC.11125' => array(
            'MY.typeBasionymePubl' => "Raatikainen, M., 1961: Dicondylus helleni n. sp. Hym., Dryinidae, a parasite of Calligypona sordidula Stal and C. excisa Mel.. Annales Entomologici Fennici, 27: 126-137",
            'MY.taxonRank' => self::NO_VALUE,
        ),
        'luomus:JAA.11126' => array(
            'MY.collectionID' => 'HR.110',
            'MY.entered' => '13.6.2014',
            'MY.dateBegin' => '4.7.1999',
            'MY.dateEnd' => '8.7.1999',
            'MY.leg' => 'Joku, Kolma',
            'MY.count' => '1',
            'MY.taxonRank' => 'MX.species',
            'MY.author' => 'Stönnius, 1831',
            'MY.det' => 'Riihikoski, Ville-Matti',
            'MY.detDate' => '2009',
        ),
        'luomus:JAA.11127' => array(
            'MZ.publicityRestrictions' => array(
                'MZ.publicityRestrictionsPrivate'
            ),
            'MY.taxon' => array(
                'Gonatopus hellenii',
                'Gonatopus helleni'
            )
        ),
        'luomus:JAA.11130' => array(
            'MY.recordBasis' => 'MY.recordBasisPreservedSpecimen'
        ),
        'luomus:JAA.11131' => array(
            'MY.collectionID' => 'HR.828',
            'MY.higherGeography' => array(
                self::LITERAL => 'Europe'
            ),
            'MY.datasetID' => self::NO_VALUE,
            'MY.taxonRank' => 'MX.kingdom',
        ),
        'luomus:JA.123' => [],
        'luomus:JA.124' => [],
        'luomus:JA.125' => [],
        'luomus:JAA.337' => [],
        'luomus:JAA.336' => [],
        'luomus:JAA.335' => [],
        'luomus:JAA.334' => [],
        'luomus:JAA.333' => [],
    );

    protected $updatedValues = array(
        'luomus:JAA.11123' => array(
            'MY.typeStatus' => array(
                'MY.typeStatusSyntype',
                'MY.typeStatusSyntype'
            ),
            'MZ.owner' => 'MOS.1024',
            'MY.taxon' => 'Uusi nimi',
            'MY.publication' => array(
                'publication',
                'unsure'
            ),
            'MY.datasetID' => array(
                'GX.10',
            ),
            'MY.unreliableFields' => "MYPublication[0]\nMYPublication[1]"
        ),
        'luomus:JAA.11124' => array(
            'MY.typeBasionymePubl' => "Pitkä",
            'MY.datasetID' => self::NO_VALUE,
        ),
        'luomus:GAC.11125' => array(
            'MY.typeBasionymePubl' => "Raatikainen, M., 1961: Dicondylus helleni n. sp. Hym., Dryinidae, a parasite of Calligypona sordidula Stal and C. excisa Mel.. Annales Entomologici Fennici, 27: 126-137"
        ),
        'luomus:JAA.11126' => array(
            'MY.collectionID' => 'HR.92',
            'MY.entered' => '13.6.2000',
            'MY.dateBegin' => '4.7.1900',
            'MY.dateEnd' => '8.7.1999',
            'MY.leg' => 'Neljäs, Nimi',
            'MY.count' => '3',
            'MY.taxonRank' => 'MX.species',
            'MY.author' => 'Stönnius, 1831',
            'MY.det' => 'Eka, Det',
            'MY.detDate' => '2009'
        ),
        'luomus:JAA.11127' => array(
            'MZ.publicityRestrictions' => array(
                'MZ.publicityRestrictionsPrivate'
            ),
            'MY.taxon' => array(
                'Gonatopus hellen',
                'Gonatopus'
            )
        ),
        'luomus:JAA.11130' => array(
            'MY.recordBasis' => 'MY.recordBasisPreservedSpecimen'
        ),
        'luomus:JAA.11131' => array(
            'MY.higherGeography' => array(
                self::LITERAL => 'Europe'
            ),
            'MY.datasetID' => 'GX.2'
        ),
    );

    public function setUp()
    {
        parent::setUp();
        $this->loginToKotka();
    }

    public function testClearDb()
    {
        $om = $this->getServiceManager()->get('Triplestore\ObjectManager');
        foreach ($this->checkThatValueExists as $subject => $value) {
            $om->remove($subject);
        }
    }

    public function testHttpsRequired()
    {
        if ($this->protocol === 'http') {
            self::markTestSkipped();
        }
        $this->dispatch($this->protocol . '://' . $this->hostname . '/tools/import');
        $this->assertNotResponseStatusCode(200);
    }

    public function testImportCanBeAccessed()
    {
        $this->dispatch($this->protocol . '://' . $this->hostname . '/tools/import');
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('Kotka');
        $this->assertControllerName('Kotka\Controller\Tools');
        $this->assertControllerClass('ToolsController');
        $this->assertMatchedRouteName('https/tools');
    }

    public function testSaveCanBeAccessedWithoutAnythingIn()
    {
        $this->dispatch($this->protocol . '://' . $this->hostname . '/tools/save');
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('Kotka');
        $this->assertControllerName('Kotka\Controller\Tools');
        $this->assertControllerClass('ToolsController');
        $this->assertMatchedRouteName('https/tools');
    }

    /**
     * @group success
     */
    public function testImportingSuccessfulBotanyFile()
    {
        $file = dirname(dirname(__DIR__)) . '/TestAssets/ImportFiles/BOTANY_SUCCESS.csv';
        $postData = array(
            'datatype' => 'botanyspecimen',
            'organization' => 'MOS.1024',
            'import' => array(
                'name' => 'testName.csv',
                'tmp_name' => $file,
                'error' => 0,
            )
        );
        $this->overrideFormElement('Kotka\Form\ImportExcel',
            $this->mockImportForm($postData));
        $this->dispatch($this->protocol . '://' . $this->hostname . '/tools/preview', 'POST', $postData);
        $this->assertResponseStatusCode(200);
        $this->assertActionName('preview');
        $this->assertQueryContentContains('p strong', 'Validation succeeded!', 'There where errors in the imported file!');
        $this->assertNotQuery('#warnings', 'There where warnings when there should not be');
        $plugins = $this->getServiceManager()->get('ControllerPluginManager');
        $store = $plugins->get('Store');

        return $store->fetch(ToolsController::CACHE_IMPORT_KEY);
    }

    /**
     * @depends testImportingSuccessfulBotanyFile
     * @group success
     */
    public function testSavingBotanyImport($data)
    {
        $plugins = $this->getServiceManager()->get('ControllerPluginManager');
        $this->om = $this->getServiceManager()->get('Triplestore\ObjectManager');
        $store = $plugins->get('Store');
        $store->store(ToolsController::CACHE_IMPORT_KEY, $data);
        $this->dispatch($this->protocol . '://' . $this->hostname . '/tools/save-job');
        $this->doImportJob($data);
        $testData = array_intersect_key($this->checkThatValueExists, array_flip(self::$testSubjects));
        $this->om->disableHydrator();
        foreach ($testData as $subject => $data) {
            $model = $this->om->fetch($subject);
            $this->assertNotNull($model);
            $this->checkThatDataExists($model, $data);
            $this->assertCount(0, $data, 'Some of the data was not found from the model! ' . implode(', ', array_keys($data)));
        }
        foreach (self::$testSubjects as $subject) {
            $this->om->remove($subject);
        }
    }

    private function doImportJob($payload, $importOptions = []) {
        /** @var \Kotka\Job\ImportExcel $file */
        $file = ImportExcel::getUserImportLogFile($payload['user']);
        $this->assertFalse(file_exists($file), "User import file wasn't removed as expected");
        /** @var ImportService $importService */
        $importService = $this->getServiceManager()->get('Kotka\Service\ImportService');
        $importService->setOptions($importOptions);
        $user = $this->getServiceManager()->get('user');
        $payload['defaultNS'] = trim(IdService::DEFAULT_DB_QNAME_PREFIX, ':');
        if ($user instanceof MAPersonInterface) {
            $payload['defaultNS'] = $user->getMADefaultQNamePrefix() === null ?
                $payload['defaultNS'] : $user->getMADefaultQNamePrefix()
            ;
        }

        $import = new ImportExcel(
            $importService,
            $this->getServiceManager()->get('Logger'),
            $this->getServiceManager()->get('Triplestore\ObjectManager'),
            $this->getServiceManager()->get('user')
        );
        $import->setContent($payload);
        $import->execute();
        self::$testSubjects = $this->extractSubjects($file);
    }

    /**
     * @group success
     */
    public function testImportingSuccessfulZooFile()
    {
        $file = dirname(dirname(__DIR__)) . '/TestAssets/ImportFiles/ZOOLOGY_SUCCESS.xlsx';
        $postData = array(
            'datatype' => 'zoospecimen',
            'organization' => 'MOS.1024',
            'import' => array(
                'name' => 'testName.xlsx',
                'tmp_name' => $file,
                'error' => 0,
            )
        );
        $this->overrideFormElement('Kotka\Form\ImportExcel',
            $this->mockImportForm($postData));
        $this->dispatch($this->protocol . '://' . $this->hostname . '/tools/preview', 'POST', $postData);
        $this->assertResponseStatusCode(200);
        $this->assertActionName('preview');
        $this->assertQueryContentContains('p strong', 'Validation succeeded!');
        $plugins = $this->getServiceManager()->get('ControllerPluginManager');
        $store = $plugins->get('Store');

        return $store->fetch(ToolsController::CACHE_IMPORT_KEY);
    }

    /**
     * @depends testImportingSuccessfulZooFile
     * @group success
     */
    public function testPrintingLabelsFromPreview($data)
    {
        $this->storeImportData($data);
        $get = array('labelFields' => array('country', 'province', 'municipality'), 'format' => 'bryophyte');
        $this->dispatch($this->protocol . '://' . $this->hostname . '/tools/labels', 'GET', $get);
        $this->assertResponseStatusCode(200);
        $this->assertResponseHeaderContains('Content-type', 'application/pdf');

        return $data;
    }

    /**
     * @depends testImportingSuccessfulZooFile
     * @group success
     */
    public function testSavingZooImport($data)
    {
        $this->om = $this->getServiceManager()->get('Triplestore\ObjectManager');
        $this->storeImportData($data);
        $this->dispatch($this->protocol . '://' . $this->hostname . '/tools/save-job');
        $this->doImportJob($data);
        $testData = array_intersect_key($this->checkThatValueExists, array_flip(self::$testSubjects));
        $this->om->disableHydrator();
        foreach ($testData as $subject => $item) {
            $model = $this->om->fetch($subject);
            $this->assertNotNull($model);
            $this->checkThatDataExists($model, $item);
            $this->assertCount(0, $item, 'Some of the data was not found from the ' . $subject . ' model! ' . implode(', ', array_keys($item)));
        }
    }

    /**
     * @depends testSavingZooImport
     */
    public function testSavingUpdatedZooImportWithNotMatchingDateEditedTime()
    {
        $file = dirname(dirname(__DIR__)) . '/TestAssets/ImportFiles/ZOOLOGY_UPDATE_FAIL.xlsx';
        $postData = array(
            'datatype' => 'zoospecimen',
            'organization' => 'MOS.1024',
            'import' => array(
                'name' => 'testName.xlsx',
                'tmp_name' => $file,
                'error' => 0,
            )
        );
        $this->overrideFormElement('Kotka\Form\ImportExcel',
            $this->mockImportForm($postData));
        $this->dispatch($this->protocol . '://' . $this->hostname . '/tools/preview', 'POST', $postData);
        $this->assertResponseStatusCode(200);
        $this->assertActionName('preview');
        $this->assertQueryContentContains('.alert strong', 'Data can not be saved', "Input was accepted even if it should have not!");
        $this->assertQueryCount('td[data-columnuniqueid="errors"] p', 3);
        $this->assertQueryCount('span.import-error', 0);
    }

    /**
     * @depends testSavingZooImport
     * @group success
     *
     */
    public function testImportingUpdatedSuccessfulZooFile()
    {
        $file = dirname(dirname(__DIR__)) . '/TestAssets/ImportFiles/ZOOLOGY_UPDATE_SUCCESS.xlsx';
        $postData = array(
            'datatype' => 'zoospecimen',
            'organization' => 'MOS.1024',
            'import' => array(
                'name' => 'testName.xlsx',
                'tmp_name' => $file,
                'error' => 0,
            )
        );
        $validatorManager = $this->getValidatorManager();
        $validatorManager->setService('Kotka\Validator\UniqueUri', $this->mockUniqueUri());
        $this->overrideFormElement('Kotka\Form\ImportExcel',
            $this->mockImportForm($postData));
        $this->dispatch($this->protocol . '://' . $this->hostname . '/tools/preview', 'POST', $postData);
        $this->assertResponseStatusCode(200);
        $this->assertActionName('preview');
        $this->assertQueryContentContains('p strong', 'Validation succeeded!', 'There where errors in the imported file!');
        $plugins = $this->getServiceManager()->get('ControllerPluginManager');
        $store = $plugins->get('Store');

        return $store->fetch(ToolsController::CACHE_IMPORT_KEY);
    }

    /**
     * @depends testImportingUpdatedSuccessfulZooFile
     * @group success
     */
    public function testSavingUpdatedZooImport($data)
    {
        $this->om = $this->getServiceManager()->get('Triplestore\ObjectManager');
        $this->om->disableHydrator();
        $testData = array_intersect_key($this->updatedValues, array_flip(self::$testSubjects));
        $origSubjects = [];
        foreach ($testData as $subject => $value) {
            $model = $this->om->fetch($subject);
            $origSubjects[$subject] = $this->getSubjects($model);
        }

        $this->assertGreaterThan(0, count($origSubjects), 'Test is missing test subjects!');

        $validatorManager = $this->getValidatorManager();
        $validatorManager->setService('Kotka\Validator\UniqueUri', $this->mockUniqueUri());

        $this->om->enableHydrator();
        $this->storeImportData($data);
        $this->dispatch($this->protocol . '://' . $this->hostname . '/tools/save-job');
        $this->doImportJob($data, [ImportService::OPTION_UPDATE_EXISTING]);
        $this->om->disableHydrator();

        foreach ($testData as $subject => $item) {
            $model = $this->om->fetch($subject);
            $this->assertNotNull($model);
            $this->checkThatDataExists($model, $item);
            $this->assertCount(0, $item, 'Some of the data was not found from the ' . $subject . ' model! ' . implode(', ', array_keys($item)));
            $subjects = $this->getSubjects($model);
            $subjects = array_flip($subjects);
            foreach ($origSubjects[$subject] as $key => $sub) {
                $this->assertArrayHasKey($sub, $subjects, 'Update has made a new subject when it should use the ones that already exist');
                unset($origSubjects[$subject][$key]);
            }
            $this->assertCount(0, $origSubjects[$subject], 'Some subjects that where originally present are missing after update');
        }

        foreach (self::$testSubjects as $subject) {
            $this->om->remove($subject);
        }
    }

    private function getSubjects(Model $model)
    {
        $subjects = [$model->getSubject()];
        foreach ($model as $predicate) {
            foreach ($predicate as $object) {
                /** @var \Triplestore\Model\Object $object */
                $value = $object->getValue();
                if ($value instanceof Model) {
                    $subjects = array_merge($subjects, $this->getSubjects($value));
                }
            }
        }

        return $subjects;
    }

    private function storeImportData($data)
    {
        $plugins = $this->getServiceManager()->get('ControllerPluginManager');
        $store = $plugins->get('Store');
        $store->store(ToolsController::CACHE_IMPORT_KEY, $data);
    }

    /**
     * @dataProvider failingImportProvider
     */
    public function testImportWithErrors($datatype, $organization, $file, $errorInCell = false, $errorColumnContains = null)
    {
        $postData = array(
            'datatype' => $datatype,
            'organization' => $organization,
            'import' => array(
                'name' => 'testName.csv',
                'tmp_name' => $file,
                'error' => 0,
            )
        );
        $this->overrideFormElement('Kotka\Form\ImportExcel',
            $this->mockImportForm($postData));
        $this->dispatch($this->protocol . '://' . $this->hostname . '/tools/preview', 'POST', $postData);
        $this->assertResponseStatusCode(200);
        $this->assertActionName('preview');
        $this->assertQueryContentContains('.alert strong', 'Data can not be saved', "Input was accepted even if it should have not!");
        if ($errorInCell) {
            $cnt = is_numeric($errorInCell) ? $errorInCell : 1;
            $this->assertQueryCount('td[data-columnuniqueid="errors"] p', 1);
            $errorColumnContains = $errorColumnContains === null ? 'Data has error(s)!' : $errorColumnContains;
            $this->assertQueryContentContains('td[data-columnuniqueid="errors"] p', $errorColumnContains);
            $this->assertQueryCount('span.import-error', $cnt);
        } else {
            $this->assertQueryCount('td[data-columnuniqueid="errors"] p', 1);
            if ($errorColumnContains !== null) {
                $this->assertQueryContentContains('td[data-columnuniqueid="errors"] p', $errorColumnContains);
            }
            $this->assertQueryCount('span.import-error', 0);
        }

    }

    /**
     * @dataProvider failAndReturnToImportProvider
     */
    public function testReturnToImportOnCriticalFailure($datatype, $organization, $file)
    {
        $postData = array(
            'datatype' => $datatype,
            'organization' => $organization,
            'import' => array(
                'name' => 'testName.csv',
                'tmp_name' => $file,
                'error' => 0,
            )
        );
        $this->overrideFormElement('Kotka\Form\ImportExcel',
            $this->mockImportForm($postData));
        $mockFlashMessage = $this->getMockBuilder('Zend\Mvc\Controller\Plugin\FlashMessenger')
            ->disableOriginalConstructor()
            ->getMock();
        $mockFlashMessage->expects($this->once())
            ->method('addErrorMessage')
            ->withAnyParameters();
        $this->overridePlugin('flashMessenger', $mockFlashMessage);
        $this->dispatch($this->protocol . '://' . $this->hostname . '/tools/preview', 'POST', $postData);
        $this->assertResponseStatusCode(302);
    }

    public function testImportingWarningZooFile()
    {
        $file = dirname(dirname(__DIR__)) . '/TestAssets/ImportFiles/ZOOLOGY_ONLY_WARNINGS.csv';
        $postData = array(
            'datatype' => 'zoospecimen',
            'organization' => 'MOS.1024',
            'import' => array(
                'name' => 'testName.csv',
                'tmp_name' => $file,
                'error' => 0,
            )
        );
        $this->overrideFormElement('Kotka\Form\ImportExcel',
            $this->mockImportForm($postData));
        $this->dispatch($this->protocol . '://' . $this->hostname . '/tools/preview', 'POST', $postData);
        $this->assertResponseStatusCode(200);
        $this->assertActionName('preview');
        $this->assertQueryContentContains('p strong', 'Validation succeeded!', 'There where errors in the imported file!');
        $this->assertQuery('#warnings');
        $plugins = $this->getServiceManager()->get('ControllerPluginManager');
        $store = $plugins->get('Store');

        return $store->fetch(ToolsController::CACHE_IMPORT_KEY);
    }

    /**
     * @depends testImportingWarningZooFile
     */
    public function testSavingZooImportWithWarnings($data)
    {
        $plugins = $this->getServiceManager()->get('ControllerPluginManager');
        $this->om = $this->getServiceManager()->get('Triplestore\ObjectManager');
        $store = $plugins->get('Store');
        $store->store(ToolsController::CACHE_IMPORT_KEY, $data);

        $this->dispatch($this->protocol . '://' . $this->hostname . '/tools/save-job');
        $this->doImportJob($data);

        $testData = array_intersect_key($this->checkThatValueExists, array_flip(self::$testSubjects));
        $this->om->disableHydrator();
        foreach ($testData as $subject => $data) {
            $model = $this->om->fetch($subject);
            $this->assertNotNull($model);
            $this->assertCount(0, $data, 'Some of the data was not found from the model! ' . implode(', ', array_keys($data)));
        }

        foreach (self::$testSubjects as $subject) {
            $this->om->remove($subject);
        }
    }

    public function failingImportProvider()
    {
        $directory = dirname(dirname(__DIR__)) . '/TestAssets/ImportFiles/';
        return array(
            // Datatype, owner, file, error in cell (can also indicate how many times the error occurs)
            array('botanyspecimen', 'MOS.1024', $directory . 'BOTANY_FAIL_LIFESTAGE.csv', true),
            array('zoospecimen', 'MOS.1024', $directory . 'ZOOLOGY_FAIL_TWO_PREFERRED.xlsx', false),
            array('zoospecimen', 'MOS.1024', $directory . 'ZOOLOGY_FAIL_DATEEND_BRFORE_DATEBEGIN.xlsx', true),
            array('zoospecimen', 'MOS.1024', $directory . 'ZOOLOGY_ERROR_MULTIPLIED_WHEN_FILTER_NOT_CLEARED.xlsx', true),
            array('zoospecimen', 'MOS.1024', $directory . 'ZOOLOGY_FAIL_MISSING_DATA.xlsx', 3),
            array('zoospecimen', 'MOS.1024', $directory . 'ZOOLOGY_FAIL_MISSING_DATA_FROM_GATHERING.xlsx', 2),
            array('zoospecimen', 'MOS.1024', $directory . 'ZOOLOGY_FAIL_MISSING_DATA_FROM_ALL.xlsx', 3),
            array('zoospecimen', 'MOS.1024', $directory . 'ZOOLOGY_FAIL_INVALID_LANGUAGE.xlsx', true),
            //array('botanyspecimen', 'MOS.1024', $directory . 'BOTANY_FAIL_ENTERED.csv', true),
            array('zoospecimen', 'MOS.1024', $directory . 'ZOOLOGY_FAIL_MISSING_TAXON_RANK.xlsx', true),
            array('zoospecimen', 'MOS.1024', $directory . 'ZOOLOGY_FAIL_ID_TWICE.xlsx', false, 'Import error: luomus:GE.1203 already used in this import!'),
        );
    }

    public function failAndReturnToImportProvider()
    {
        $directory = dirname(dirname(__DIR__)) . '/TestAssets/ImportFiles/';
        return array(
            array('botanyspecimen', '', $directory . 'BOTANY_FAIL_DET.csv'),
            //array('zoospecimen', '', $directory . 'ZOOLOGY_FAIL_TOO_MANY_ROWS.xlsx'),
            array('zoospecimen', '', $directory . 'ZOOLOGY_FAIL_TOO_BIG.xlsx'),
            array('zoospecimen', '', $directory . 'ZOOLOGY_FAIL_CELL_FORMATTING.xlsx'),
            array('zoospecimen', '', $directory . 'ZOOLOGY_FAIL_NO_ROWS.xlsx'),
            array('zoospecimen', '', $directory . 'ZOOLOGY_FAIL_TWO_SAME_COLUMN_NAMES.xlsx'),
        );
    }

    /**
     * This makes sure that no lose ends are left in to the database
     * by unit tests.
     *
     * @param \Exception $e
     * @throws \Exception
     */
    protected function onNotSuccessfulTest(\Exception $e)
    {
        if ($this->om !== null) {
            $remove = self::$testSubjects;
            if (!is_array($remove)) {
                $remove = array($remove);
            }
            foreach ($remove as $subject) {
                $this->om->remove($subject);
            }
        }
        throw $e;
    }

    private function mockUniqueUri()
    {
        $mockUniqueuUri = $this->getMockBuilder('Kotka\Validator\UniqueUri')
            ->disableOriginalConstructor()
            ->getMock();
        $mockUniqueuUri->expects($this->any())
            ->method('isValid')
            ->will($this->returnValue(true));
        return $mockUniqueuUri;
    }

    private function mockImportForm($postData)
    {
        $mockImportForm = $this->getMockBuilder('Kotka\Form\ImportExcel')
            ->disableOriginalConstructor()
            ->getMock();
        $mockImportForm->expects($this->once())
            ->method('setData')
            ->with($postData);
        $mockImportForm->expects($this->once())
            ->method('isValid')
            ->will($this->returnValue(true));
        $mockImportForm->expects($this->once())
            ->method('getData')
            ->will($this->returnValue($postData));
        return $mockImportForm;
    }

    private function checkThatDataExists(Model $model, &$data)
    {
        if (count($data) == 0) {
            return;
        }
        $value = null;
        $type = null;
        foreach ($model as $predicate) {
            $name = $predicate->getName();
            if (!isset($data[$name])) {
                foreach ($predicate as $object) {
                    $objectValue = $object->getValue();
                    if ($objectValue instanceof Model) {
                        $this->checkThatDataExists($objectValue, $data);
                    }
                }
                continue;
            }
            $lang = null;
            $type = null;
            $value = $data[$name];
            if (is_array($data[$name])) {
                $key = key($data[$name]);
                if ($key === self::RESOURCE) {
                    $type = Object::RESOURCE;
                } else if ($key === self::LITERAL) {
                    $type = Object::LITERAL;
                }
                $value = $data[$name];
            }
            foreach ($predicate as $object) {
                /** @var \Triplestore\Model\Object $object */
                $objectValue = $object->getValue();
                if (is_array($value)) {
                    if (!in_array($objectValue, $value)) {
                        $this->fail("Subjects '" . $model->getSubject() . "' predicate '$name' had some extra data in the object ($objectValue)! If you're using checks you should include check for every object value");
                    }
                    foreach ($value as $key2 => $value2) {
                        if ($value2 !== $objectValue) {
                            continue;
                        }
                        if ($type !== null && $object->getType() !== $type) {
                            continue;
                        }
                        unset($data[$name][$key2]);
                        if (count($data[$name]) == 0) {
                            unset($data[$name]);
                        }
                        break;
                    }
                } else {
                    if ($value !== $objectValue) {
                        $this->fail("Predicate $name value '$objectValue' didn't match '$value'!");
                    }
                    if ($type !== null && $object->getType() !== $type) {
                        continue;
                    }
                    unset($data[$name]);
                }

            }
        }
        foreach ($data as $key => $value) {
            if ($value == self::NO_VALUE) {
                unset($data[$key]);
            }
        }
    }

    private function  extractSubjects($file)
    {
        $subjects = array();
        $handle = fopen($file, "r");
        if ($handle) {
            while (($line = fgets($handle)) !== false) {
                $line = trim(strip_tags($line));
                $pos = strpos($line, ' ');
                $subject = substr($line, 0, $pos);
                if (is_string($subject)) {
                    $subjects[] = IdService::getQName($subject, true);
                }
            }
            fclose($handle);
        } else {
            fclose($handle);
        }
        return $subjects;
    }
}
 