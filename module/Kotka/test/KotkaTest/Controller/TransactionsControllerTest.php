<?php
namespace KotkaTest\Controller;

/**
 * @group TransactionsController
 */
class TransactionsControllerTest extends CommonKotkaHttp
{

    public static $subject;

    private $minSuccess = array(
        'subject' => '',
        'MZOwner' => 'MOS.1024',
        'HRATransactionType' => 'HRA.transactionTypeLoanIncoming',
        //'HRACorrespondenceHeaderOrganizationCode' => 'H',
        'HRATransactionRequestReceived' => '01.02.2014',
        'HRACorrespondentOrganization' => 'MOS.1010',
        'status' => 'ok'
    );

    private $namespace = 'tun:HRA';

    public function setUp()
    {
        parent::setUp();
        $this->loginToKotka();
    }

    public function testHttpsRequired()
    {
        if ($this->protocol === 'http') {
            self::markTestSkipped();
        }
        $this->dispatch('http://' . $this->hostname . '/transactions');
        $this->assertNotResponseStatusCode(200);
    }

    public function testIndexActionCanBeAccessed()
    {
        $this->dispatch($this->protocol . '://' . $this->hostname . '/transactions');
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('Kotka');
        $this->assertControllerName('Kotka\Controller\Transactions');
        $this->assertControllerClass('TransactionsController');
        $this->assertMatchedRouteName('https/transactions');
    }

    public function testIndexHasItems()
    {
        $this->dispatch($this->protocol . '://' . $this->hostname . '/transactions');
        $this->assertQueryCountMin('.table tr', 10);
    }

    public function testRequiredFields()
    {
        foreach ($this->minSuccess as $key => $value) {
            if ($key == 'subject') {
                continue;
            }
            $missing = $this->minSuccess;
            unset($missing[$key]);
            $this->dispatch($this->protocol . '://' . $this->hostname . '/transactions/add', 'POST', $missing);
            $this->assertResponseStatusCode(200);
            $this->assertQuery('.alert-danger');
        }
    }

    public function testSuccessWithMinim()
    {
        $mockFlashMessage = $this->getMockBuilder('Zend\Mvc\Controller\Plugin\FlashMessenger')
            ->disableOriginalConstructor()
            ->getMock();
        $mockFlashMessage->expects($this->once())
            ->method('addSuccessMessage')
            ->with('<strong>Record successfully saved into database.</strong> Below is the saved record: you can view or edit it here.');
        $this->overridePlugin('flashMessenger', $mockFlashMessage);

        $this->dispatch($this->protocol . '://' . $this->hostname . '/transactions/add', 'POST', $this->minSuccess);
        $this->assertResponseStatusCode(302);

        $location = $this->getRedirectLocation();
        $qname = $this->getQnameFromUrl($location);
        $this->assertTrue(strpos($qname, $this->namespace . '.') === 0, 'Wrong namespace with created item');
        return $qname;
    }

    /**
     * @depends testSuccessWithMinim
     */
    public function testAddingSpecimens($qname)
    {
        $this->qname = $qname;
        self::$subject = $qname;
        $postData = $this->minSuccess;
        $postData['subject'] = $qname;
        $postData['HRBAway'] = array(
            'luomus:GV.1',
            'luomus:GV.3',
            'luomus:GV.4',
            'luomus:GV.5',
            'luomus:GV.6',
        );
        $postData['HRBReturned'] = array(
            'Another specimen 4',
            'Another specimen 5',
            'Another specimen 6',
            'Another specimen 7',
            'Another specimen 8',
        );

        $formData = $postData;
        $formData['HRBAway'] = implode(', ', $formData['HRBAway']);
        $formData['HRBReturned'] = implode(', ', $formData['HRBReturned']);

        $this->dispatch($this->protocol . '://' . $this->hostname . '/transactions/edit?uri=' . $qname, 'POST', $formData);
        $this->assertResponseStatusCode(302);

        return array('qname' => $qname, 'data' => $postData);
    }

    /**
     * @depends testAddingSpecimens
     */
    public function testTransactionHasSpecimens($data)
    {
        $this->qname = $data['qname'];
        $postData = $data['data'];
        $getData = array(
            'uri' => $this->qname
        );
        $this->dispatch($this->protocol . '://' . $this->hostname . '/transactions/edit', 'GET', $getData);
        $this->assertResponseStatusCode(200);

        $this->assertQuery('.HRBAway');

        return array('qname' => $this->qname, 'specimen' => $postData);
    }

    /**
     * @depends testAddingSpecimens
     */
    public function testExportingSpecimensToExcel($data)
    {
        $this->dispatch($this->protocol . '://' . $this->hostname . '/transactions/exportSpecimenExcel', 'GET', array('uri' => $data['qname']));
        $this->assertResponseStatusCode(200);
        $this->assertResponseHeaderContains('Content-type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

        return $data;
    }

    /**
     * @depends      testAddingSpecimens
     * @dataProvider pdfExportProvider
     */
    public function testExportingToPdf($type)
    {
        $url = $this->protocol . '://' . $this->hostname . '/pdf/' . $type . self::$subject . '.pdf';
        $this->dispatch($url);
        $this->assertResponseStatusCode(200);
        $this->assertResponseHeaderContains('Content-type', 'application/pdf');
    }

    public function pdfExportProvider()
    {
        return array(
            array('dispatchsheet_'),
            array('inquirysheet_'),
            array('returnsheet_'),
            array('insectlabels_')
        );
    }

    /**
     * This test if the database has the actual data and then cleans it up
     * @depends testAddingSpecimens
     */
    public function testDatabaseHasCorrectData($data)
    {
        $this->qname = $data['qname'];
        $itemData = $this->convertDataToDatabaseData($data['data']);
        $om = $this->getObjectManager();
        $item = $om->fetch($this->qname);

        $this->assertNotNull($item);
        $this->assertModelHasData($item, $itemData);

        $om->remove($item);
    }

    /* This would need another input filter to be added with name HRATransaction and to add all database select filters
    public function testFieldWithWrongValue() {
        $postData = $this->minSuccess;
        $invalid  = 'Invalid';
        $postData['HRATransactionType'] = $invalid;
        $this->dispatch('https://' . $this->hostname . '/transactions/add', 'POST', $postData);
        $this->assertResponseStatusCode(200);
        $this->assertQueryContentRegex('div.alert-danger ul li', "{^Value '$invalid' is not.*}");
    }
    */

    private function convertDataToDatabaseData(array $data)
    {
        $data['HRATransactionRequestReceived'] = $this->convertDateToDBDate($data['HRATransactionRequestReceived']);
        $data = $this->array_remove_empty($data);
        if (isset($data['status'])) {
            unset($data['status']);
        }

        return $data;
    }
}
 