<?php
namespace KotkaTest\Controller;

/**
 * @group OrganizationsController
 */
class OrganizationsControllerTest extends CommonKotkaHttp
{

    private $minSuccess = array(
        'subject' => '',
        'MZOwner' => 'MOS.1024',
        'MOSOrganizationLevel1En' => 'testOrganization',
        'MOSAbbreviation' => '',
        'MOSAbbreviationExplanation' => '',
        'status' => 'ok'
    );

    private $notNeededInMin = [
        'subject',
        'MOSAbbreviation',
        'MOSAbbreviationExplanation'

    ];

    private $namespace = 'tun:MOS';

    public function setUp()
    {
        parent::setUp();
        $this->loginToKotka();
    }

    public function testHttpsRequired()
    {
        if ($this->protocol === 'http') {
            $this->markTestSkipped();
        }
        $this->dispatch('http://' . $this->hostname . '/organizations');
        $this->assertNotResponseStatusCode(200);
    }

    public function testIndexActionCanBeAccessed()
    {
        $this->dispatch($this->protocol . '://' . $this->hostname . '/organizations');
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('Kotka');
        $this->assertControllerName('Kotka\Controller\Organizations');
        $this->assertControllerClass('OrganizationsController');
        $this->assertMatchedRouteName('https/organizations');
    }

    public function testIndexHasItems()
    {
        $this->dispatch($this->protocol . '://' . $this->hostname . '/organizations');
        $this->assertQueryCountMin('.table tr', 10);
    }

    public function testRequiredFields()
    {
        foreach ($this->minSuccess as $key => $value) {
            if (in_array($key, $this->notNeededInMin)) {
                continue;
            }
            $missing = $this->minSuccess;
            unset($missing[$key]);
            $this->dispatch($this->protocol . '://' . $this->hostname . '/organizations/add', 'POST', $missing);
            $this->assertResponseStatusCode(200);
            $this->assertQuery('.alert-danger');
        }
    }

    public function testSuccessWithMinim()
    {
        $mockFlashMessage = $this->getMockBuilder('Zend\Mvc\Controller\Plugin\FlashMessenger')
            ->disableOriginalConstructor()
            ->getMock();
        $mockFlashMessage->expects($this->once())
            ->method('addSuccessMessage')
            ->with('<strong>Record successfully saved into database.</strong> Below is the saved record: you can view or edit it here.');
        $this->overridePlugin('flashMessenger', $mockFlashMessage);

        $this->dispatch($this->protocol . '://' . $this->hostname . '/organizations/add', 'POST', $this->minSuccess);
        $this->assertResponseStatusCode(302);

        $location = $this->getRedirectLocation();
        $qname = $this->getQnameFromUrl($location);
        $this->assertTrue(strpos($qname, $this->namespace . '.') === 0, 'Wrong namespace with created item');
        $om = $this->getObjectManager();
        $item = $om->fetch($qname);
        $this->assertNotNull($item);
        $om->remove($item);
    }

    /* Field not in form not possible as of 3.0 since context dosn't contain the whole resultset
    public function testFieldNotInForm() {
        $postData   = $this->minSuccess;
        $wrongField = 'NOTThere';
        $postData[$wrongField] = '1234';
        $this->dispatch($this->protocol . '://' . $this->hostname . '/organizations/add', 'POST', $postData);
        $this->assertResponseStatusCode(200);
        $this->assertQueryContentRegex('div.alert-danger ul li', "{.*$wrongField.*}");
    }
    */

    /* This would need another input filter to be added with name MOSOrganization and to add all database select filters
    public function testFieldWithWrongValue() {
        $postData = $this->minSuccess;
        $postData['MOSAbbreviation'] = 'test';
        $invalid  = 'Invalid';
        $postData['MOSAbbreviationExplanation'] = $invalid;
        $this->dispatch($this->protocol . '://' . $this->hostname . '/organizations/add', 'POST', $postData);
        $this->assertResponseStatusCode(200);
        $this->assertQueryContentRegex('div.alert-danger ul li', "{^Value '$invalid' is not.*}");
    }
    */

    /**
     * @dataProvider failingProvider
     */
    public function testValidation($data)
    {
        $postData = array_replace_recursive($this->minSuccess, $data);
        $this->dispatch($this->protocol . '://' . $this->hostname . '/organizations/add', 'POST', $postData);
        $this->assertResponseStatusCode(200);
        $this->assertQuery('.alert-danger');
    }

    public function failingProvider()
    {
        return array(
            array(array(
                'MOSAbbreviation' => 'fail'
            )),
            array(array(
                'MOSAbbreviationExplanation' => 'MOS.abbreviationExplanationIndexHerbariorum'
            )),
            array(array(
                'MOSAd' => 'seurantatiimi',
            )),
            array(array(
                'MZOwner' => 'MOS.1010',
            )),
        );
    }

}
 