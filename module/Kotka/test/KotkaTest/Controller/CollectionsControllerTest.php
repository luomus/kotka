<?php
namespace KotkaTest\Controller;

/**
 * @group CollectionController
 */
class CollectionControllerTest extends CommonKotkaHttp
{

    private $minSuccess = array(
        'MZOwner' => 'MOS.1024',
        'MYCollectionNameEn' => 'testEn',
        'MYCollectionNameFi' => 'testFi',
        'MYCollectionType' => 'MY.collectionTypeSpecimens',
        'MYDescriptionEn' => 'DescEn',
        'MYDescriptionFi' => 'DescFi',
        'MYPersonResponsible' => 'Mee',
        'MYIntellectualRights' => 'MY.intellectualRightsCC0',
        'MYContactEmail' => 'testaaja@luomus.fi',
        'MYDataQuality' => 'MY.dataQuality1',
        'status' => 'ok',
    );

    private $namespace = 'tun:HR';

    public function setUp()
    {
        parent::setUp();
        $this->loginToKotka();
    }

    public function testHttpsRequired()
    {
        if ($this->protocol === 'http') {
            $this->markTestSkipped();
        }
        $this->dispatch('http://' . $this->hostname . '/collections');
        $this->assertNotResponseStatusCode(200);
    }

    public function testIndexActionCanBeAccessed()
    {
        $this->dispatch($this->protocol . '://' . $this->hostname . '/collections');
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('Kotka');
        $this->assertControllerName('Kotka\Controller\Collections');
        $this->assertControllerClass('CollectionsController');
        $this->assertMatchedRouteName('https/collections');
    }

    public function testIndexHasItems()
    {
        $this->dispatch($this->protocol . '://' . $this->hostname . '/collections');
        $this->assertQueryCountMin('.table tr', 10);
    }

    public function testRequiredFields()
    {
        foreach ($this->minSuccess as $key => $value) {
            $missing = $this->minSuccess;
            unset($missing[$key]);
            $this->dispatch($this->protocol . '://' . $this->hostname . '/collections/add', 'POST', $missing);
            echo "Missing $key\n";
            $this->assertResponseStatusCode(200);
            $this->assertQuery('.alert-danger');
        }
    }

    public function testPartOfSelected()
    {
        $collection = array(
            'uri' => 'HR.221',
            'name' => 'Vertebrates Collection'
        );
        $this->dispatch($this->protocol . '://' . $this->hostname . '/collections/add?isPartOf=' . $collection['uri']);
        $this->assertResponseStatusCode(200);
        $this->assertQueryContentContains('option[selected="selected"]', $collection['name']);
    }

    public function testSuccessWithMinim()
    {
        $mockFlashMessage = $this->getMockBuilder('Zend\Mvc\Controller\Plugin\FlashMessenger')
            ->disableOriginalConstructor()
            ->getMock();
        $mockFlashMessage->expects($this->once())
            ->method('addSuccessMessage')
            ->with('<strong>Record successfully saved into database.</strong> Below is the saved record: you can view or edit it here.');
        $this->overridePlugin('flashMessenger', $mockFlashMessage);

        $this->dispatch($this->protocol . '://' . $this->hostname . '/collections/add', 'POST', $this->minSuccess);
        $this->assertResponseStatusCode(302);

        $location = $this->getRedirectLocation();
        $qname = $this->getQnameFromUrl($location);
        $this->assertTrue(strpos($qname, $this->namespace . '.') === 0, 'Wrong namespace with created item');
        $om = $this->getObjectManager();
        $item = $om->fetch($qname);
        $this->assertNotNull($item);
        $om->remove($item);
    }

    /* Field not in form not possible as of 3.0 since context dosn't contain the whole resultset
    public function testFieldNotInForm() {
        $postData   = $this->minSuccess;
        $wrongField = 'NOTThere';
        $postData[$wrongField] = '1234';
        $this->dispatch('https://' . $this->hostname . '/collections/add', 'POST', $postData);
        $this->assertResponseStatusCode(200);
        $this->assertQueryContentRegex('div.alert-danger ul li', "{.*$wrongField.*}");
    }
    */

    /* This would need another input filter to be added with name MYCollection and to add all database select filters
    public function testFieldWithWrongValue() {
        $postData = $this->minSuccess;
        $invalid  = 'VALUE';
        $postData['MYCollectionType'] = $invalid;
        $this->dispatch('https://' . $this->hostname . '/collections/add', 'POST', $postData);
        $this->assertResponseStatusCode(200);
        $this->assertQueryContentRegex('div.alert-danger ul li', "{^Value '$invalid' is not.*}");
    }
    */

    /**
     * @dataProvider failingProvider
     */
    public function testValidation($data)
    {
        $postData = $this->minSuccess + $data;
        $this->dispatch($this->protocol . '://' . $this->hostname . '/collections/add', 'POST', $postData);
        $this->assertResponseStatusCode(200);
        $this->assertQuery('.alert-danger');
    }

    public function failingProvider()
    {
        return array(
            array(array(
                'MYDigitizedSize' => 'fail'
            )),
            array(array(
                'MYDigitizedSize' => 101,
            )),
            array(array(
                'MYDigitizedSize' => -1,
            )),
            array(array(
                'MYTypesSize' => 1.2345
            )),
            array(array(
                'MYCollectionSize' => -1
            ))
        );
    }

}
 