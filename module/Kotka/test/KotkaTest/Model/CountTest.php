<?php
namespace KotkaTest\Model;


class CountTest extends \PHPUnit_Framework_TestCase
{
    /** @var \Kotka\Model\Count */
    private $count;

    public function setUp()
    {
        parent::setUp();
        $this->count = new \Kotka\Model\Count();
    }

    public function testBlankHasZero()
    {
        $this->assertEquals(0, $this->count->getTotal());
    }

    public function testAnalyzingCount()
    {
        $this->count->refresh('10f 2m 4');
        $this->assertEquals(16, $this->count->getTotal());
        $this->assertEquals(10, $this->count->getFemale());
        $this->assertEquals(2, $this->count->getMale());
        $this->assertEquals(4, $this->count->getOther());

        $this->count->refresh(null);
        $this->assertEquals(0, $this->count->getTotal());

        $this->count->refresh('10 F 2M 4 juv.');
        $this->assertEquals(16, $this->count->getTotal());
        $this->assertEquals(10, $this->count->getFemale());
        $this->assertEquals(2, $this->count->getMale());
        $this->assertEquals(4, $this->count->getOther());

        $this->count->refresh('10f');
        $this->assertEquals(10, $this->count->getTotal());
        $this->assertEquals(10, $this->count->getFemale());
        $this->assertEquals(0, $this->count->getMale());
        $this->assertEquals(0, $this->count->getOther());

        $this->count->refresh('>50');
        $this->assertEquals(50, $this->count->getTotal());
        $this->assertEquals(50, $this->count->getOther());

        $this->count->refresh('3-5');
        $this->assertEquals(5, $this->count->getTotal());
        $this->assertEquals(5, $this->count->getOther());

        $this->count->refresh('-3');
        $this->assertEquals(3, $this->count->getTotal());
        $this->assertEquals(3, $this->count->getOther());
    }

    public function testStringAnalysing()
    {
        $this->count->refresh(' useita');
        $this->assertEquals(2, $this->count->getTotal());
        $this->assertEquals(2, $this->count->getOther());

        $this->count->refresh('yksi');
        $this->assertEquals(1, $this->count->getTotal());
        $this->assertEquals(1, $this->count->getOther());
    }

}