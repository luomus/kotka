<?php

namespace KotkaTest\Model\Label;

class Helper extends \PHPUnit_Framework_TestCase
{
    /** @var \Kotka\Model\Label\Helper */
    private $helper;

    public function setUp()
    {
        parent::setUp();
        $this->helper = new \Kotka\Model\Label\Helper();
    }

    /**
     * @dataProvider coordinates
     */
    public function testCoordinateConversion($system, $lat, $lon, $expected)
    {
        $actual = $this->helper->getShortCoordinateText($system, $lat, $lon);
        $actual = trim($actual);
        $this->assertEquals($expected, $actual);
    }

    public function coordinates()
    {
        return [
            //system, lat, lon, string
            ['wgs84', 12.34, 56.7, '12.34 N:56.7 E (wgs84)'],
            ['wgs84dms', 12.34, -56.7, '12.34 N:56.7 W (wgs84dms)'],
            ['wgs84', -12.34, 56.7, '12.34 S:56.7 E (wgs84)'],
            ['wgs84', -12.34, -56.7, '12.34 S:56.7 W (wgs84)'],
        ];
    }

    public function testGetShortCoordinateTextDelimiter()
    {
        $this->assertEquals('10 N_10 E (wgs84) ', $this->helper->getShortCoordinateText('wgs84', 10, 10, '_'));
        $this->assertEquals('10 N_-_10 E (wgs84) ', $this->helper->getShortCoordinateText('wgs84', 10, 10, '_-_'));
        $this->assertEquals('10 N10 E (wgs84) ', $this->helper->getShortCoordinateText('wgs84', 10, 10, ''));
    }

}
 