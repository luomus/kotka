<?php

namespace KotkaTest\Validator;


use Kotka\Triple\MAPerson;
use Kotka\Validator\UniqueUri;
use Kotka\Validator\UserInOrganization;
use KotkaTest\Util\ServiceManagerFactory;

class UserInOrganizationTest extends \PHPUnit_Framework_TestCase
{

    /** @var  UniqueUri */
    private $validator;
    private $serviceManager;

    public function setUp()
    {
        parent::setUp();
        $this->serviceManager = ServiceManagerFactory::getServiceManager();
        $this->validator = new UserInOrganization();
        $this->validator->setServiceLocator($this->serviceManager->get('Zend\Mvc\Controller\Plugin\Manager'));
    }

    public function testInvalidWithNoValue()
    {
        $this->serviceManager->setService('user', $this->getUserInOrganization(['MOS.1' => '']));
        $this->assertFalse($this->validator->isValid(''));
        $messages = $this->validator->getMessages();
        $this->assertArrayHasKey(UserInOrganization::NOT_IN_ORGANIZATION, $messages);
    }

    public function testInvalidWithNullValue()
    {
        $this->serviceManager->setService('user', $this->getUserInOrganization(['MOS.1' => '']));
        $this->assertFalse($this->validator->isValid(null));
        $messages = $this->validator->getMessages();
        $this->assertArrayHasKey(UserInOrganization::NOT_IN_ORGANIZATION, $messages);
    }

    public function testInvalidWhenUserNotInOrganization()
    {
        $this->serviceManager->setService('user', $this->getUserInOrganization(['MOS.2' => '']));
        $this->assertFalse($this->validator->isValid('MOS.1'));
        $messages = $this->validator->getMessages();
        $this->assertArrayHasKey(UserInOrganization::NOT_IN_ORGANIZATION, $messages);
    }

    public function testValidWhenUserInOrganization()
    {
        $this->serviceManager->setService('user', $this->getUserInOrganization(['MOS.1' => '', 'MOS.2' => '']));
        $this->assertTrue($this->validator->isValid('MOS.1'));
    }

    public function testInvalidWhenOrganizationEmptyArray()
    {
        $this->serviceManager->setService('user', $this->getUserInOrganization(array()));
        $this->assertFalse($this->validator->isValid('MOS.1'));
        $messages = $this->validator->getMessages();
        $this->assertArrayHasKey(UserInOrganization::NOT_IN_ANY_ORGANIZATION, $messages);
    }

    private function getUserInOrganization($organization)
    {
        $user = new MAPerson();
        $user->setMAOrganisation($organization);

        return $user;
    }

}
 