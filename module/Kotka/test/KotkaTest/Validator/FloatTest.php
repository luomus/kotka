<?php

namespace KotkaTest\Validator;


use Kotka\Validator\FloatValidator;

class FloatTest extends \PHPUnit_Framework_TestCase
{

    /** @var Float */
    private $validator;

    public function setUp()
    {
        $this->validator = new FloatValidator();
    }

    public function testValidFloats()
    {
        $this->assertTrue($this->validator->isValid('10.12'));
        $this->assertTrue($this->validator->isValid('-10.12'));
        $this->assertTrue($this->validator->isValid('1000.12657'));
        $this->assertTrue($this->validator->isValid('-9000.12657'));
    }


    public function testInvalidFloats()
    {
        $this->assertFalse($this->validator->isValid('1,12'));
        $this->assertFalse($this->validator->isValid('+10.12'));
        $this->assertFalse($this->validator->isValid('a'));
        $this->assertFalse($this->validator->isValid('-1^10*3'));
        $this->assertFalse($this->validator->isValid('10 0001'));
    }
}
 