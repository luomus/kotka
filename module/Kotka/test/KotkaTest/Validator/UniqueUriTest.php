<?php

namespace KotkaTest\Validator;


use Kotka\Validator\UniqueUri;
use KotkaTest\Util\ServiceManagerFactory;
use Triplestore\Model\Object;
use Triplestore\Model\Predicate;

class UniqueUriTest extends \PHPUnit_Framework_TestCase
{

    /** @var  UniqueUri */
    private $validator;
    private $serviceManager;

    public function setUp()
    {
        parent::setUp();
        $this->serviceManager = ServiceManagerFactory::getServiceManager();
        $this->validator = new UniqueUri();
        $this->validator->setServiceLocator($this->serviceManager->get('Zend\Mvc\Controller\Plugin\Manager'));
    }

    public function testValidOnEmpty()
    {
        $this->assertTrue($this->validator->isValid('', array()));
    }

    public function testValidWithNamespaceAndId()
    {
        $data = ['MYNamespaceID' => 'luomus:test', 'MYObjectID' => '10'];

        $omMock = $this->getMockBuilder('Triplestore\Service\ObjectManager')
            ->disableOriginalConstructor()
            ->getMock();
        $omMock->expects($this->once())
            ->method('getPredicate')
            ->with($data['MYNamespaceID'] . '.' . $data['MYObjectID'], 'MZ.dateEdited')
            ->will($this->returnValue(null));
        $this->serviceManager->setService('Triplestore\ObjectManager', $omMock);
        $this->assertTrue($this->validator->isValid('', $data));
    }

    public function testInValidWhenNamespaceAndIdExists()
    {
        $data = ['MYNamespaceID' => 'luomus:test', 'MYObjectID' => '10'];
        $predicate = new Predicate('');
        $predicate->addObject(new Object('anything'));

        $omMock = $this->getMockBuilder('Triplestore\Service\ObjectManager')
            ->disableOriginalConstructor()
            ->getMock();
        $omMock->expects($this->once())
            ->method('getPredicate')
            ->with($data['MYNamespaceID'] . '.' . $data['MYObjectID'], 'MZ.dateEdited')
            ->will($this->returnValue($predicate));
        $this->serviceManager->setService('Triplestore\ObjectManager', $omMock);
        $this->assertFalse($this->validator->isValid('', $data));
    }

    public function testValidWhenDateEditedTimeMatches()
    {
        $data = ['MYNamespaceID' => 'luomus:test', 'MYObjectID' => '10', 'MZDateEdited' => '2014-01-01 01:01:01'];
        $predicate = new Predicate('');
        $predicate->addObject(new Object($data['MZDateEdited']));

        $omMock = $this->getMockBuilder('Triplestore\Service\ObjectManager')
            ->disableOriginalConstructor()
            ->getMock();
        $omMock->expects($this->once())
            ->method('getPredicate')
            ->with($data['MYNamespaceID'] . '.' . $data['MYObjectID'], 'MZ.dateEdited')
            ->will($this->returnValue($predicate));
        $this->serviceManager->setService('Triplestore\ObjectManager', $omMock);
        $this->assertTrue($this->validator->isValid('', $data));
    }

    public function testInvalidWhenDateEditedTimeDoesntMatches()
    {
        $data = ['MYNamespaceID' => 'luomus:test', 'MYObjectID' => '10', 'MZDateEdited' => '2014-01-01 01:01:01'];
        $predicate = new Predicate('');
        $predicate->addObject(new Object('2014-01-01 01:01:00'));

        $omMock = $this->getMockBuilder('Triplestore\Service\ObjectManager')
            ->disableOriginalConstructor()
            ->getMock();
        $omMock->expects($this->once())
            ->method('getPredicate')
            ->with($data['MYNamespaceID'] . '.' . $data['MYObjectID'], 'MZ.dateEdited')
            ->will($this->returnValue($predicate));
        $this->serviceManager->setService('Triplestore\ObjectManager', $omMock);
        $this->assertFalse($this->validator->isValid('', $data));
    }

}
 