<?php

namespace KotkaTest\Validator;


use Common\Service\IdService;
use Kotka\InputFilter\MYDocument;
use Kotka\Validator\AllowedNamespace;
use KotkaTest\Util\ServiceManagerFactory;

class AllowedNamespaceTest extends \PHPUnit_Framework_TestCase
{

    /** @var  AllowedNamespace */
    private $validator;
    private $serviceManager;

    public function setUp()
    {
        parent::setUp();
        $this->serviceManager = ServiceManagerFactory::getServiceManager();
        $this->validator = new AllowedNamespace();
        MYDocument::setType('');
        $this->validator->setServiceLocator($this->serviceManager->get('Zend\Mvc\Controller\Plugin\Manager'));
    }

    public function testValidatingWithMissingType()
    {
        $this->setExpectedException('Kotka\Validator\Exception\InvalidArgumentException');
        $this->validator->isValid('test');
    }

    public function testValidatingCorrectValue()
    {
        MYDocument::setType('zoo');
        $this->setAllowedNamespaces(['tun:T' => 'tun:T']);
        $this->assertTrue($this->validator->isValid('tun:T'));
    }

    public function testValidatingIncorrectValue()
    {
        MYDocument::setType('zoo');
        $this->setAllowedNamespaces(['tun:E' => 'tun:E']);
        $this->assertFalse($this->validator->isValid('tun:T'));
    }

    public function testValidatingIncorrectNull()
    {
        MYDocument::setType('zoo');
        $this->setAllowedNamespaces(['tun:E' => 'tun:E']);
        $this->assertFalse($this->validator->isValid(null));
    }

    public function testValidatingIncorrectEmpty()
    {
        MYDocument::setType('zoo');
        $this->setAllowedNamespaces(['tun:E' => 'tun:E']);
        $this->assertFalse($this->validator->isValid(''));
    }

    public function testDatatypeFromContext()
    {
        MYDocument::setType('Specimen');
        $this->assertFalse($this->validator->isValid('TEST'));
        $messages = $this->validator->getMessages();
        $this->assertArrayHasKey(AllowedNamespace::MISSING_DATATYPE, $messages);

        $this->setAllowedNamespaces(['tun:ANOTHER' => 'tun:ANOTHER']);
        $this->assertTrue($this->validator->isValid('tun:ANOTHER', ['MYDatatype' => 'zoo']));
    }

    private function setAllowedNamespaces(array $ns)
    {
        $esMock = $this->getMockBuilder('Kotka\Service\FormElementService')
            ->disableOriginalConstructor()
            ->getMock();
        $esMock->expects($this->once())
            ->method('getSelect')
            ->will($this->returnValue($ns));
        $this->serviceManager->setService('Kotka\Service\FormElementService', $esMock);
    }


}
 