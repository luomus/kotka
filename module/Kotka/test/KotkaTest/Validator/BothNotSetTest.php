<?php

namespace KotkaTest\Validator;


use Kotka\Validator\BothNotSet;

class BothNotSetTest extends \PHPUnit_Framework_TestCase
{

    /** @var  BothNotSet */
    private $validator;

    public function setUp()
    {
        $this->validator = new BothNotSet();
    }

    public function testSettingOptions()
    {
        $this->assertNull($this->validator->getHasNot());

        $this->validator->setHasNot('Field');
        $this->assertEquals('Field', $this->validator->getHasNot());

        $validator = new BothNotSet(['hasNot' => 'test']);
        $this->assertEquals('test', $validator->getHasNot());
    }

    public function testInvalidIfBothSet()
    {
        $this->validator->setHasNot('field');
        $this->assertFalse($this->validator->isValid('set', ['field' => 'alsoSet']));
        $messages = $this->validator->getMessages();
        $this->assertArrayHasKey(BothNotSet::BOTH_SET, $messages);
    }

    public function testInvalidIfNoContext()
    {
        $this->assertFalse($this->validator->isValid('set'));
        $messages = $this->validator->getMessages();
        $this->assertArrayHasKey(BothNotSet::NO_CONTEXT, $messages);
    }

    public function testValidIfValueEmpty()
    {
        $this->validator->setHasNot('field');
        $this->assertTrue($this->validator->isValid('', ['field' => 'alsoSet']));
    }

    public function testValidWhenArray()
    {
        $this->validator->setHasNot('field');
        $this->assertTrue($this->validator->isValid('has Value', ['field' => []]));
        $this->assertFalse($this->validator->isValid('has Value', ['field' => ['']]));
    }

    public function testValidIfEmptyImageArray()
    {
        $this->validator->setHasNot('field');
        $this->assertTrue($this->validator->isValid('has Value', ['field' => [['name' => '']]]));
        $this->assertFalse($this->validator->isValid('has Value', ['field' => [['name' => 'file.txt']]]));
    }

    public function testValidIfOtherFieldIsEmpty()
    {
        $this->validator->setHasNot('field');
        $this->assertTrue($this->validator->isValid('value', ['field' => '']));
    }

}
 