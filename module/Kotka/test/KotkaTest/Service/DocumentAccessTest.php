<?php

namespace KotkaTest\Service;


use Kotka\Enum\Document;
use Kotka\Service\DocumentAccess;
use Kotka\Triple\MAPerson;
use Kotka\Triple\MYDocument;
use Triplestore\Model\Model;
use Triplestore\Model\Object;
use Triplestore\Model\Predicate;
use Triplestore\Service\ObjectManager;
use Triplestore\Stdlib\Hydrator\Date;
use User\Enum\User;

class DocumentAccessTest extends \PHPUnit_Framework_TestCase
{

    public function testGettingNonExistingField()
    {
        $data = [
            'MYField' => 'correct'
        ];
        $this->assertEquals('correct', DocumentAccess::getValue($data, 'MYField'));
        $this->assertNull(DocumentAccess::getValue($data, 'MYNonField'));
        $this->assertEquals('default', DocumentAccess::getValue($data, 'MYNonField', 'default'));
    }

    public function testGettingValuesFromArray()
    {
        $data = [
            'MYField' => 'correct'
        ];
        $this->assertEquals('correct', DocumentAccess::getValue($data, 'MYField'));
        $this->assertEquals('correct', DocumentAccess::getValue($data, 'MY.field'));
    }

    public function testGettingValuesFromModel()
    {
        $model = new Model();
        $predicate = new Predicate('MY.field');
        $predicate->setObject(new Object('correct'));
        $predicate->addObject(new Object('langEN', 'en', null, Object::LITERAL));
        $model->setPredicate($predicate);
        $this->assertEquals('correct', DocumentAccess::getValue($model, 'MY.field'));
        $this->assertEquals('correct', DocumentAccess::getValue($model, 'MY.field', null, DocumentAccess::VALUE_TYPE_RESOURCE));
        $this->assertEquals(null, DocumentAccess::getValue($model, 'MY.field', null, DocumentAccess::VALUE_TYPE_LITERAL));
        $this->assertEquals('langEN', DocumentAccess::getValue($model, 'MY.field', null, DocumentAccess::VALUE_TYPE_LITERAL, 'en'));

        $predicate->addObject(new Object('another'));
        $this->assertEquals(['correct', 'another'], DocumentAccess::getValue($model, 'MY.field', null, DocumentAccess::VALUE_TYPE_RESOURCE));
    }

    public function testGettingValuesFromDocument()
    {
        $document = new MYDocument();
        $document->setMYNotes('correct');
        $this->assertEquals('correct', DocumentAccess::getValue($document, 'MYNotes'));
        $this->assertEquals('correct', DocumentAccess::getValue($document, 'MY.notes'));
        $this->assertEquals([], DocumentAccess::getValue($document, 'MY.bold'));
        $this->assertEquals(null, DocumentAccess::getValue($document, 'MYCollectionID'));
    }

    public function testMemberCanView()
    {
        $member = $this->getUser('MA.1', User::ROLE_MEMBER);
        $docData = [
            ObjectManager::PREDICATE_TYPE => Document::TYPE,
            Document::PREDICATE_PUBLICITY_RESTRICTION => Document::PUBLICITY_PUBLIC
        ];
        $this->assertTrue(DocumentAccess::canView($docData, $member));
        $docData[Document::PREDICATE_PUBLICITY_RESTRICTION] = Document::PUBLICITY_PROTECTED;
        $this->assertTrue(DocumentAccess::canView($docData, $member));
        $docData[Document::PREDICATE_PUBLICITY_RESTRICTION] = Document::PUBLICITY_PRIVATE;
        $this->assertTrue(DocumentAccess::canView($docData, $member));
    }

    public function testGuestCanView()
    {
        $guest = $this->getUser();
        $docData = [
            ObjectManager::PREDICATE_TYPE => Document::TYPE,
        ];
        $this->assertTrue(DocumentAccess::canView($docData, $guest));
        $docData[Document::PREDICATE_PUBLICITY_RESTRICTION] = Document::PUBLICITY_PUBLIC;
        $this->assertTrue(DocumentAccess::canView($docData, $guest));
        $docData[Document::PREDICATE_PUBLICITY_RESTRICTION] = Document::PUBLICITY_PROTECTED;
        $this->assertFalse(DocumentAccess::canView($docData, $guest));
        $docData[Document::PREDICATE_PUBLICITY_RESTRICTION] = Document::PUBLICITY_PRIVATE;
        $this->assertFalse(DocumentAccess::canView($docData, $guest));
    }

    public function testMemberCanDelete()
    {
        $member = $this->getUser('MA.1', User::ROLE_MEMBER);
        $dateTime = new \DateTime();
        $docData = [
            ObjectManager::PREDICATE_TYPE => Document::TYPE,
            Document::PREDICATE_DATE_CREATED => $dateTime->format(Date::FORMAT),
            Document::PREDICATE_CREATOR => 'MA.1'
        ];
        $this->assertTrue(DocumentAccess::canDelete($docData, $member));
    }

    public function testMemberCannotDelete()
    {
        $member = $this->getUser('MA.1', User::ROLE_MEMBER);
        $dateTime = new \DateTime();
        $docData = [
            ObjectManager::PREDICATE_TYPE => Document::TYPE,
            Document::PREDICATE_CREATOR => 'MA.2'
        ];
        $this->assertFalse(DocumentAccess::canDelete($docData, $member));
        $docData[Document::PREDICATE_DATE_CREATED] = $dateTime->format(Date::FORMAT);
        $this->assertFalse(DocumentAccess::canDelete($docData, $member));
        $docData[Document::PREDICATE_CREATOR] = 'MA.1';
        $this->assertTrue(DocumentAccess::canDelete($docData, $member));
        $dateTime->modify('-3week');
        $docData[Document::PREDICATE_DATE_CREATED] = $dateTime->format(Date::FORMAT);
        $this->assertFalse(DocumentAccess::canDelete($docData, $member));
    }

    public function testAdminCanDelete()
    {
        $member = $this->getUser('MA.3', User::ROLE_ADMIN);
        $dateTime = new \DateTime();
        $docData = [
            ObjectManager::PREDICATE_TYPE => Document::TYPE,
            Document::PREDICATE_DATE_CREATED => $dateTime->format(Date::FORMAT),
            Document::PREDICATE_CREATOR => 'MA.1'
        ];
        $this->assertTrue(DocumentAccess::canDelete($docData, $member));
    }

    public function testAdminCannotDelete()
    {
        $member = $this->getUser('MA.1', User::ROLE_MEMBER);
        $dateTime = new \DateTime();
        $dateTime->modify('-3week');
        $docData = [
            ObjectManager::PREDICATE_TYPE => Document::TYPE,
            Document::PREDICATE_DATE_CREATED => $dateTime->format(Date::FORMAT),
            Document::PREDICATE_CREATOR => 'MA.2'
        ];
        $docData[Document::PREDICATE_DATE_CREATED] = $dateTime->format(Date::FORMAT);
        $this->assertFalse(DocumentAccess::canDelete($docData, $member));
    }


    private function getUser($id = 'MA.007', $role = User::ROLE_GUEST, $organization = User::GUEST_ORGANIZATION)
    {
        if (!is_array($organization)) {
            $organization = [$organization => $organization];
        }
        $user = new MAPerson();
        $user->setSubject($id);
        $user->setMARoleKotka($role);
        $user->setMAOrganisation($organization);

        return $user;
    }

}