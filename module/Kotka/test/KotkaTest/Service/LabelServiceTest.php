<?php
namespace KotkaTest\Service;

use Kotka\Service\LabelService;
use KotkaTest\Controller\CommonKotka;

/**
 * @group LabelService
 */
class LabelServiceTest extends CommonKotka
{

    /** @var  LabelService */
    private $labelService;

    public function setUp()
    {
        parent::setUp();
        $sm = $this->getApplication()->getServiceManager();
        $this->labelService = $sm->get('Kotka\Service\LabelService');
    }

    public function testOnlyPreferredIdentificationIsInLabel()
    {
        $document = $this->getDocumentWith([
            array('MYTaxon' => 'NOPREFERRED'),
            array(
                'MYTaxon' => 'NOPREFERRED',
                'MYPreferredIdentification' => 'no'
            ),
            array(
                'MYTaxon' => 'CORRECT',
                'MYPreferredIdentification' => 'yes'
            ),
            array(
                'MYTaxon' => 'NOPREFERRED',
                'MYPreferredIdentification' => 'no'
            ),
            array('MYTaxon' => 'NOPREFERRED')
        ]);
        $result = $this->labelService->getLabelHtml(
            $this->labelService->extractLabelData(array($document)),
            array('format' => 'insecta_det_v2', 'labelFields' => array()));

        $this->assertFalse(strpos($result, 'NOPREFERRED'), 'No Preferred item was found on the labels!');
        $this->assertGreaterThan(0, strpos($result, 'CORRECT'), "Correct identification wasn't found on the labels!");
    }

    public function testCorrectOrderWithDetDate()
    {
        $document = $this->getDocumentWith([
            array('MYTaxon' => 'NOPREFERRED'),
            array(
                'MYTaxon' => 'NOPREFERRED',
                'MYDetDate' => '2010'
            ),
            array(
                'MYTaxon' => 'CORRECT',
                'MYDetDate' => '01.01.2011'
            ),

            array(
                'MYTaxon' => 'NOPREFERRED',
                'MYDetDate' => '01.01.2015',
                'MYPreferredIdentification' => 'no'
            ),
        ]);
        $result = $this->labelService->getLabelHtml(
            $this->labelService->extractLabelData(array($document)),
            array('format' => 'insecta_det_v2', 'labelFields' => array()));

        $this->assertFalse(strpos($result, 'NOPREFERRED'), 'No Preferred item was found on the labels!');
        $this->assertGreaterThan(0, strpos($result, 'CORRECT'), "Correct identification wasn't found on the labels!");

        $document = $this->getDocumentWith([
            array('MYTaxon' => 'NOPREFERRED'),
            array(
                'MYTaxon' => 'NOPREFERRED',
                'MYDetDate' => '2010'
            ),
            array(
                'MYTaxon' => 'NOPREFERRED',
                'MYDetDate' => '01.01.2011'
            ),

            array(
                'MYTaxon' => 'CORRECT',
                'MYDetDate' => '01.01.2015',
            ),
        ]);
        $result = $this->labelService->getLabelHtml(
            $this->labelService->extractLabelData(array($document)),
            array('format' => 'insecta_det_v2', 'labelFields' => array()));

        $this->assertFalse(strpos($result, 'NOPREFERRED'), 'No Preferred item was found on the labels!');
        $this->assertGreaterThan(0, strpos($result, 'CORRECT'), "Correct identification wasn't found on the labels!");
    }

    public function testSameDetDates()
    {
        $document = $this->getDocumentWith([
            array('MYTaxon' => 'NOPREFERRED'),
            array(
                'MYTaxon' => 'NOPREFERRED',
                'MYDetDate' => '2010'
            ),
            array(
                'MYTaxon' => 'CORRECT',
                'MYDetDate' => '2010'
            )
        ]);
        $result = $this->labelService->getLabelHtml(
            $this->labelService->extractLabelData(array($document)),
            array('format' => 'insecta_det_v2', 'labelFields' => array()));

        $this->assertFalse(strpos($result, 'NOPREFERRED'), 'No Preferred item was found on the labels!');
        $this->assertGreaterThan(0, strpos($result, 'CORRECT'), "Correct identification wasn't found on the labels!");
    }

    private function getDocumentWith(array $identification)
    {
        $document = $this->generateDummyMYDocument();
        $gathering = $this->generateDummyMYGathering($document, array('MYCountry' => 'FI'));
        $unit = $this->generateDummyMYUnit($gathering, array('MYRecordBasis' => 'MY.recordBasisPreservedSpecimen'));
        foreach ($identification as $data) {
            $this->generateDummyMYIdentification($unit, $data);
        }

        return $document;
    }

    public function testLabelDosntHaveIdentificationsWithRecordBasisHumanObservation()
    {
        $document = $this->generateDummyMYDocument();
        $gathering = $this->generateDummyMYGathering($document, array('MYCountry' => 'CHECK'));
        $unit1 = $this->generateDummyMYUnit($gathering, array('MYRecordBasis' => 'MY.recordBasisHumanObservation'));
        $this->generateDummyMYIdentification($unit1,
            array('MYTaxon' => 'OBSERVATION')
        );
        $unit1 = $this->generateDummyMYUnit($gathering, array('MYRecordBasis' => 'MY.recordBasisPreservedSpecimen'));
        $this->generateDummyMYIdentification($unit1,
            array('MYTaxon' => 'CORRECT')
        );
        $result = $this->labelService->getLabelHtml(
            $this->labelService->extractLabelData(array($document)),
            array('format' => 'insecta_v2', 'labelFields' => array('taxon', 'country')));

        $this->assertFalse(strpos($result, 'OBSERVATION'), 'Photo record type was on the label sheet even when it should not');
        $this->assertGreaterThan(0, strpos($result, 'CORRECT'), "Identification wasn't on the label sheet!");
        $this->assertEquals(1, substr_count($result, 'CHECK'), "Whole label should be taken away. Now information with missing taxon is present");
    }

    public function testUnreliableFieldsMarked()
    {
        $document = $this->generateDummyMYDocument(null, null, array('MYUnreliableFields' => "MYGathering[0][MYCountry]\nMYGathering[0][MYUnit][0][MYIdentification][0][MYTaxon]"));
        $gathering = $this->generateDummyMYGathering($document, array('MYCountry' => 'FI'));
        $unit1 = $this->generateDummyMYUnit($gathering, array('MYRecordBasis' => 'MY.recordBasisPreservedSpecimen'));
        $this->generateDummyMYIdentification($unit1,
            array('MYTaxon' => 'OBSERVATION')
        );
        $result = $this->labelService->getLabelHtml(
            $this->labelService->extractLabelData(array($document)),
            array('format' => 'bryophyte', 'labelFields' => array()));

        $this->assertGreaterThan(0, strpos($result, '?OBSERVATION'), "Taxon wasn't mark as unreliable even thou it should have");
        $this->assertGreaterThan(0, strpos($result, '?FI'), "Country wasn't mark as unreliable even thou it should have");
    }

    public function testTwoUnitsCreatingTwoLabels()
    {
        $document = $this->generateDummyMYDocument();
        $gathering = $this->generateDummyMYGathering($document, array('MYCountry' => 'CHECK'));
        $unit1 = $this->generateDummyMYUnit($gathering, array('MYRecordBasis' => 'MY.recordBasisPreservedSpecimen'));
        $this->generateDummyMYIdentification($unit1,
            array('MYTaxon' => 'CORRECT1')
        );
        $unit1 = $this->generateDummyMYUnit($gathering, array('MYRecordBasis' => 'MY.recordBasisPreservedSpecimen'));
        $this->generateDummyMYIdentification($unit1,
            array('MYTaxon' => 'CORRECT2')
        );
        $result = $this->labelService->getLabelHtml(
            $this->labelService->extractLabelData(array($document)),
            array('format' => 'invertebrate_tube', 'labelFields' => array('taxon')));

        $this->assertGreaterThan(0, strpos($result, 'CORRECT1'), "First units Identification wasn't on the label sheet!");
        $this->assertGreaterThan(0, strpos($result, 'CORRECT2'), "Second units identification wasn't on the label sheet!");
    }

}
 