<?php

namespace KotkaTest\Filter\View;

use Kotka\Filter\View\MYDocument;
use KotkaTest\Controller\CommonKotka;

class MYDocumentTest extends CommonKotka
{

    /** @var MYDocument */
    private $filter;

    public function setUp()
    {
        parent::setUp();
        $sm = $this->getApplication()->getServiceManager();
        $this->filter = $sm->get('filterManager')->get('Kotka\Filter\View\MYDocument'); // MYDocument extends Editable filter
    }

    public function testFilterDoesNotAddKeyWhenNotNeeded()
    {
        $data = ['qname' => 'some'];
        $filtered = $this->filter->filter($data);
        $this->assertArrayNotHasKey('loanedIn', $filtered);


        $result = null;
        $data = ['qname' => 'test'];
        $this->mockOnLoan($result, 'test');
        $filtered = $this->filter->filter($data);
        $this->assertArrayNotHasKey('loanedIn', $filtered);

    }

    public function testAddedLoanedInKey()
    {
        $testSubject = 'JAA.jokin';
        $data = ['qname' => 'test'];
        $this->mockOnLoan($testSubject, 'test');
        $filtered = $this->filter->filter($data);
        $this->assertArrayHasKey('loanedIn', $filtered);
        $this->assertEquals($testSubject, $filtered['loanedIn']);
    }

    private function mockOnLoan($return, $with = null)
    {
        $mockService = $this->getMockBuilder('Kotka\Service\TransactionService')
            ->disableOriginalConstructor()
            ->getMock();
        $mockService->expects($this->once())
            ->method('isLoaned')
            ->with($with)
            ->will($this->returnValue($return));
        $this->overrideService('Kotka\Service\TransactionService',
            $mockService);
    }

}
