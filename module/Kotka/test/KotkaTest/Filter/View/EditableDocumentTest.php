<?php

namespace KotkaTest\Filter\View;


use Kotka\Filter\View\EditableDocument;
use KotkaTest\Controller\CommonKotkaHttp;

class EditableDocumentTest extends CommonKotkaHttp
{

    /** @var EditableDocument */
    private $filter;

    public function setUp()
    {
        parent::setUp();
        $sm = $this->getApplication()->getServiceManager();
        $this->filter = $sm->get('filterManager')->get('Kotka\Filter\View\MYDocument'); // MYDocument extends Editable filter
    }

    public function testCannotEditWhenNotLoggedIn()
    {
        $data = [];
        $filtered = $this->filter->filter($data);
        $this->assertArrayHasKey('canEdit', $filtered);
        $this->assertFalse($filtered['canEdit']);

        $data = [
            'MZ.owner' => 'MOS.007'
        ];
        $filtered = $this->filter->filter($data);
        $this->assertArrayHasKey('canEdit', $filtered);
        $this->assertFalse($filtered['canEdit']);
    }

    public function testCanEditWhenLoggedWithSameOrganization()
    {
        $data = [
            'MZ.owner' => 'MOS.007'
        ];
        $this->loginToKotka('MA.member', ['MOS.1' => '', 'MOS.2' => '', 'MOS.007' => '']);
        $filtered = $this->filter->filter($data);
        $this->assertArrayHasKey('canEdit', $filtered);
        $this->assertTrue($filtered['canEdit']);
    }

    public function testCannotEditWhenPrimaryDataLocationSomewhereElse()
    {
        self::markTestSkipped('Primary data location does not effect editability anymore');
        $data = [
            'MZ.owner' => 'MOS.007',
            'MY.primaryDataLocation' => 'somewhere else'
        ];
        $this->loginToKotka('MA.member', ['MOS.1' => '', 'MOS.2' => '', 'MOS.007' => '']);
        $filtered = $this->filter->filter($data);
        $this->assertArrayHasKey('canEdit', $filtered);
        $this->assertFalse($filtered['canEdit']);
    }

    public function testCannotEditWhenLoggedWithDifferentOrganization()
    {
        $data = [
            'MZ.owner' => 'MOS.007'
        ];
        $this->loginToKotka('MA.member', ['MOS.1' => '', 'MOS.2' => '']);
        $filtered = $this->filter->filter($data);
        $this->assertArrayHasKey('canEdit', $filtered);
        $this->assertFalse($filtered['canEdit']);
    }
}
