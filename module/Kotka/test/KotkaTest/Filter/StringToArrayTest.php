<?php

namespace KotkaTest\Filter;


use Kotka\Filter\StringToArray;

class StringToArrayTest extends \PHPUnit_Framework_TestCase
{
    /** @var  StringToArray */
    private $filter;

    public function setUp()
    {
        parent::setUp();
        $this->filter = new StringToArray();
    }

    public function testFilteringSimpleStringToArray()
    {
        $this->assertEquals(array('Jari'), $this->filter->filter('Jari'));
        $this->assertEquals(array('Jari Kahanpää'), $this->filter->filter('Jari Kahanpää'));
    }

    public function testFilteringStringWithTwoNames()
    {
        $this->assertEquals(array('Jari', 'Marko'), $this->filter->filter('Jari, Marko'));
        $this->assertEquals(array('Jari Kahanpää', 'Jussi Kulju'), $this->filter->filter('Jari Kahanpää,Jussi Kulju'));
    }

    public function testFilteringArrayWithElementThatHasMany()
    {
        $this->assertEquals(array('A', 'M', 'C', 'N'), $this->filter->filter(array('A', 'M,C', 'N')));
        $this->assertEquals(array('A', 'M', 'C', 'C', 'N'), $this->filter->filter(array('A', 'M,C', 'C', 'N')));
    }

    public function testThrowingExceptionWhenOtherThanStringOrArray()
    {
        $this->setExpectedException('Zend\Filter\Exception\RuntimeException');
        $this->filter->filter(1);
    }

    public function testSpecifyingDelimiter()
    {
        $this->filter->setDelimiter('--');
        $this->assertEquals(array('A', 'B', 'C'), $this->filter->filter('A--B--C'));
        $this->assertEquals(array('A', 'B', 'C'), $this->filter->filter(array('A', 'B--C')));
    }
}
 