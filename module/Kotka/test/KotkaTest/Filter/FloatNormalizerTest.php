<?php

namespace KotkaTest\Filter;


use Kotka\Filter\FloatNormalizer;

class FloatNormalizerTest extends \PHPUnit_Framework_TestCase
{
    /** @var  FloatNormalizer */
    private $filter;

    public function setUp()
    {
        parent::setUp();
        $this->filter = new FloatNormalizer();
    }

    public function testRemovingSpaces()
    {
        $this->assertEquals('-10', $this->filter->filter('- 10'));
        $this->assertEquals('-100000', $this->filter->filter('- 100 000'));
        $this->assertEquals('100000', $this->filter->filter(' 1 0 0 0 0  0  '));
    }

    public function testConvertingCommaToDot()
    {
        $this->assertEquals('-1000.01', $this->filter->filter('-1000,01'));
        $this->assertEquals('1999.99', $this->filter->filter('1999,99'));
    }

    public function testCorrectInput()
    {
        $this->assertEquals('-1000.01', $this->filter->filter('-1000.01'));
        $this->assertEquals('1999.99', $this->filter->filter('1999.99'));
    }

    public function testLocalicedFloatConversion()
    {
        $this->assertEquals('-10000.01', $this->filter->filter('-10,000.01'));
        $this->assertEquals('1999.99', $this->filter->filter('1 999.99'));
        $this->assertEquals('1999.99', $this->filter->filter('1 999,99'));
        $this->assertEquals('-10000.01', $this->filter->filter('-10.000,01'));
    }
}
 