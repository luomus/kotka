<?php

namespace KotkaTest\Filter;

use Kotka\Filter\ExcelTimeToDateTime;

class ExcelTimeToDateTimeTest extends \PHPUnit_Framework_TestCase
{
    /** @var  ExcelTimeToDateTime */
    private $filter;

    public function setUp()
    {
        parent::setUp();
        $this->filter = new ExcelTimeToDateTime();
    }

    public function testFilterIgnoresInvalidValues()
    {
        $this->assertEquals('1234', $this->filter->filter('1234'));
        $this->assertEquals('123.4', $this->filter->filter('123.4'));
        $this->assertEquals(['test'], $this->filter->filter(['test']));
        $this->assertEquals(12345, $this->filter->filter(12345));
    }

    public function testFilteringReturnDateTimeObjectByDefault()
    {
        $this->assertInstanceOf('DateTime', $this->filter->filter('12345'));
    }

    public function testFilterReturnsCorrectFormat()
    {
        $this->assertInstanceOf('DateTime', $this->filter->filter('12345'));
    }

    /**
     * @param $excelTime
     * @param $correct
     * @param $format
     * @dataProvider dataProvider
     */
    public function testFilterReturnCorrectDataInCorrectFormat($excelTime, $correct, $format)
    {
        $filter = new ExcelTimeToDateTime(['format' => $format]);
        $this->filter->setFormat($format);
        $this->assertEquals($correct, $filter->filter($excelTime));
        $this->assertEquals($correct, $this->filter->filter($excelTime));
    }

    public function dataProvider()
    {
        return [
            ['10000', '1927-05', 'Y-m'],
            ['41170', '2012-09-18', 'Y-m-d'],
            ['41179', '2012.09.27', 'Y.m.d'],
            ['99999', '13/10/2173', 'd/m/Y'],
        ];
    }

}
 