<?php

namespace KotkaTest\Filter;


use Kotka\Filter\ArrayStringTrim;

class ArrayStringTrimTest extends \PHPUnit_Framework_TestCase
{
    /** @var  ArrayStringTrim */
    private $filter;

    public function setUp()
    {
        parent::setUp();
        $this->filter = new ArrayStringTrim();
    }

    public function testTrimmingSimpleString()
    {
        $this->assertEquals('Jari', $this->filter->filter(' Jari    '));
        $this->assertEquals('Jari Kahanpää', $this->filter->filter('  Jari Kahanpää '));
    }

    public function testTrimmingSimpleArray()
    {
        $this->assertEquals(array('Jari'), $this->filter->filter(array(' Jari    ')));
        $this->assertEquals(array('A', 'B', 'C D'), $this->filter->filter(array(' A', 'B', ' C D    ')));
    }

    public function testTrimmingWithCharlist()
    {
        $this->filter->setCharList('- ');
        $this->assertEquals(array('', 'B', 'C- D'), $this->filter->filter(array('', '-B ', '- - C- D -')));
    }

}
 