<?php

namespace KotkaTest\Filter;


use Kotka\Filter\MoveTo;

class MoveToTest extends \PHPUnit_Framework_TestCase
{
    /** @var MoveTo */
    private $filter;

    public function setUp()
    {
        parent::setUp();
        $this->filter = new MoveTo();
        $this->filter->setSource('source');
        $this->filter->setTarget('target');
    }

    public function testDefaultValues()
    {
        $filter = new MoveTo();
        $this->assertFalse($filter->isOverride());
        $this->assertNull($filter->getSource());
        $this->assertNull($filter->getTarget());
    }

    public function testConstructionInitialization()
    {
        $filter = new MoveTo([
            'target' => 'testTarget',
            'source' => 'testSource',
            'override' => true,
        ]);
        $this->assertEquals('testTarget', $filter->getTarget());
        $this->assertEquals('testSource', $filter->getSource());
        $this->assertTrue($filter->isOverride());
    }

    public function testMoving()
    {
        $data = [
            'source' => 'still here',
        ];
        $filtered = $this->filter->filter($data);
        $this->assertArrayHasKey('target', $filtered);
        $this->assertEquals($data['source'], $filtered['target']);
        $this->assertArrayNotHasKey('source', $filtered);
    }

    public function testMovingWithoutOverriding()
    {
        $data = [
            'source' => 'still here',
            'target' => 'not overridden'
        ];
        $filtered = $this->filter->filter($data);
        $this->assertEquals($data['source'], $filtered['source']);
        $this->assertEquals($data['target'], $filtered['target']);
    }

    public function testMovingWithOverriding()
    {
        $data = [
            'source' => 'still here',
            'target' => 'overridden'
        ];
        $this->filter->setOverride(true);
        $filtered = $this->filter->filter($data);
        $this->assertArrayNotHasKey('source', $filtered);
        $this->assertEquals($data['source'], $filtered['target']);
    }

    public function testMovingWithoutClearingSource()
    {
        $data = [
            'source' => 'still here',
            'target' => 'overridden'
        ];
        $this->filter->setOverride(true);
        $this->filter->setClearSource(false);
        $filtered = $this->filter->filter($data);
        $this->assertArrayHasKey('source', $filtered);
        $this->assertEquals($data['source'], $filtered['source']);
        $this->assertEquals($data['source'], $filtered['target']);
    }

    public function testMovingWithConditionNotMet()
    {
        $data = [
            'source' => 'still here',
            'target' => 'overridden'
        ];
        $this->filter->setCondition([
            'source' => 'source',
            'operation' => 'equal',
            'value' => 'test'
        ]);
        $this->filter->setOverride(true);
        $filtered = $this->filter->filter($data);
        $this->assertArrayHasKey('source', $filtered);
        $this->assertEquals($data['target'], $filtered['target']);
    }


    public function testMovingWithConditionMet()
    {
        $data = [
            'source' => 'still here',
            'target' => 'overridden'
        ];
        $this->filter->setCondition([
            'source' => 'source',
            'operation' => 'equal',
            'value' => 'still here'
        ]);
        $this->filter->setOverride(true);
        $filtered = $this->filter->filter($data);
        $this->assertArrayNotHasKey('source', $filtered);
        $this->assertEquals($data['source'], $filtered['target']);
    }

    public function testMovingArray()
    {
        $data = [
            'source' => ['some', 'value', 'in', 'array'],
            'target' => 'overridden'
        ];
        $this->filter->setOverride(true);
        $filtered = $this->filter->filter($data);
        $this->assertArrayNotHasKey('source', $filtered);
        $this->assertEquals($data['source'], $filtered['target']);
    }

}
 