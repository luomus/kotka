<?php

namespace KotkaTest\Filter;


use Kotka\Filter\AssociatedTaxaToUnit;

class AssociatedTaxaToUnitTest extends \PHPUnit_Framework_TestCase
{
    /** @var  AssociatedTaxaToUnit */
    private $filter;

    public function setUp()
    {
        parent::setUp();
        $this->filter = new AssociatedTaxaToUnit();
    }

    public function testAddsNewUnitWhenNoUnits()
    {
        $test = [
            'MYAssociatedObservationTaxa' => 'test'
        ];
        $data = $this->filter->filter($test);
        $this->hasNewUnit($data, 1, 'test');
    }

    protected function hasNewUnit($data, $unitCount, $identification, $unitKey = null)
    {
        $unitKey = $unitKey ?: ($unitCount - 1);
        $this->assertArrayHasKey('MYUnit', $data);
        $this->assertCount($unitCount, $data['MYUnit']);
        $this->assertArrayHasKey($unitKey, $data['MYUnit']);
        $this->assertArrayHasKey('MYRecordBasis', $data['MYUnit'][$unitKey]);
        $this->assertEquals('MY.recordBasisHumanObservation', $data['MYUnit'][$unitKey]['MYRecordBasis']);
        $this->assertArrayHasKey('MYIdentification', $data['MYUnit'][$unitKey]);
        $this->assertArrayHasKey('0', $data['MYUnit'][$unitKey]['MYIdentification']);
        $this->assertArrayHasKey('MYTaxon', $data['MYUnit'][$unitKey]['MYIdentification'][0]);
        $this->assertCount(1, $data['MYUnit'][$unitKey]['MYIdentification']);
        $this->assertEquals($identification, $data['MYUnit'][$unitKey]['MYIdentification'][0]['MYTaxon']);
    }

    public function testAddingNewUnitWhenUnitsExists()
    {
        $test = [
            'MYAssociatedObservationTaxa' => 'test',
            'MYUnit' => [
                [
                    'MYRecordBasis' => 'MY.recordBasisPreserverSpecimen'
                ]
            ]
        ];
        $data = $this->filter->filter($test);
        $this->hasNewUnit($data, 2, 'test');
        $this->assertEquals('MY.recordBasisPreserverSpecimen', $data['MYUnit'][0]['MYRecordBasis']);


        $test = [
            'MYAssociatedObservationTaxa' => 'test',
            'MYUnit' => [
                [
                    'MYRecordBasis' => 'MY.recordBasisPreserverSpecimen'
                ],
                [
                    'MYRecordBasis' => 'MY.recordBasisPreserverSpecimen'
                ]
            ]
        ];
        $data = $this->filter->filter($test);
        $this->hasNewUnit($data, 3, 'test');
        $this->assertEquals('MY.recordBasisPreserverSpecimen', $data['MYUnit'][0]['MYRecordBasis']);
        $this->assertEquals('MY.recordBasisPreserverSpecimen', $data['MYUnit'][1]['MYRecordBasis']);
    }

    public function testAddingNewUnitWhenDocumentGiven()
    {
        $test = [
            'MYAssociatedObservationTaxa' => 'test',
            'MYUnit' => [
                [
                    'MYRecordBasis' => 'MY.recordBasisPreserverSpecimen'
                ]
            ]
        ];
        $document = [
            'MYNotes' => 'Some',
            'MYGathering' => [
                $test
            ]
        ];
        $data = $this->filter->filter($document);
        $this->assertArrayHasKey('MYNotes', $data);
        $this->assertArrayHasKey('MYGathering', $data);
        $this->assertEquals('Some', $data['MYNotes']);
        $this->assertCount(1, $data['MYGathering']);
        $this->hasNewUnit($data['MYGathering'][0], 2, 'test');
    }

}
 