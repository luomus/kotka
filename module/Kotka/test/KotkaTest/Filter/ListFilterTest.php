<?php

namespace KotkaTest\Filter;


use Kotka\Filter\ListFilter;
use Kotka\Service\DocumentAccess;

class ListFilterTest extends \PHPUnit_Framework_TestCase
{
    /** @var ListFilter */
    private $filter;

    private $modes = [
        ListFilter::MODE_ALL,
        DocumentAccess::MODE_PRIVATE,
        DocumentAccess::MODE_PROTECTED,
        DocumentAccess::MODE_PUBLIC,
    ];

    public function setUp()
    {
        parent::setUp();
        $this->filter = new ListFilter();
    }

    public function testDefaultValues()
    {
        $filter = new ListFilter();
        $this->assertEquals(ListFilter::MODE_ALL, $filter->getMode());
        foreach ($this->modes as $mode) {
            $this->assertEquals([], $filter->getBlacklist($mode));
            $this->assertEquals([], $filter->getWhitelist($mode));
        }
    }

    public function testSettingOptionsInConstructor()
    {
        $options = [
            'mode' => DocumentAccess::MODE_PRIVATE,
            'whitelist' => [
                ListFilter::MODE_ALL => [
                    'all'
                ],
                DocumentAccess::MODE_PUBLIC => [
                    'whitelist',
                    'test'
                ]
            ],
            'blacklist' => [
                DocumentAccess::MODE_PROTECTED => [
                    'blacklist',
                    '1'
                ],
                DocumentAccess::MODE_PRIVATE => [
                    'blacklist',
                    '2'
                ]
            ]
        ];
        $filter = new ListFilter($options);
        $this->assertEquals(DocumentAccess::MODE_PRIVATE, $filter->getMode());
        $this->assertEquals([], $filter->getWhitelist());
        $this->assertEquals($options['whitelist'][ListFilter::MODE_ALL], $filter->getWhitelist(ListFilter::MODE_ALL));
        $this->assertEquals($options['whitelist'][DocumentAccess::MODE_PUBLIC], $filter->getWhitelist(DocumentAccess::MODE_PUBLIC));
        $this->assertEquals($options['blacklist'][DocumentAccess::MODE_PROTECTED], $filter->getBlacklist(DocumentAccess::MODE_PROTECTED));
        $this->assertEquals($options['blacklist'][DocumentAccess::MODE_PRIVATE], $filter->getBlacklist());
        $this->assertEquals([], $filter->getBlacklist(DocumentAccess::MODE_PUBLIC));
    }

    public function testBlacklist()
    {
        $data = [
            'fine' => 'yes',
            'missing' => 'Not present',
            '1' => 'test'
        ];
        $this->filter->setBlacklist(['missing']);
        $filtered = $this->filter->filter($data);
        $this->assertArrayHasKey('fine', $filtered);
        $this->assertArrayHasKey('1', $filtered);
        $this->assertEquals($data['fine'], $filtered['fine']);
        $this->assertEquals($data['1'], $filtered['1']);
        $this->assertArrayNotHasKey('missing', $filtered);
    }

    public function testWhiteList()
    {
        $data = [
            'fine' => 'yes',
            'missing' => 'Not present',
            '1' => 'still here'
        ];
        $this->filter->setWhitelist(['1']);
        $filtered = $this->filter->filter($data);
        $this->assertArrayHasKey('1', $filtered);
        $this->assertEquals($data['1'], $filtered['1']);
        $this->assertArrayNotHasKey('missing', $filtered);
        $this->assertArrayNotHasKey('fine', $filtered);
    }

    public function testWhiteAndBlackListTogether()
    {
        $data = [
            'fine' => 'yes',
            'missing' => 'Not present',
            '0' => 'still here'
        ];
        $this->filter->setWhitelist(['fine', 'missing']);
        $this->filter->setBlacklist(['missing']);
        $filtered = $this->filter->filter($data);
        $this->assertArrayHasKey('fine', $filtered);
        $this->assertEquals($data['fine'], $filtered['fine']);
        $this->assertArrayNotHasKey('missing', $filtered);
        $this->assertArrayNotHasKey('0', $filtered);
    }

    public function testWhitelistWithDifferentMode()
    {
        $data = [
            'fine' => 'yes',
            'missing' => 'Not present',
            '1' => 'still here'
        ];
        $this->filter->setWhitelist(['missing'], DocumentAccess::MODE_PROTECTED);
        $this->filter->setWhitelist(['missing'], DocumentAccess::MODE_PUBLIC);
        $this->filter->setWhitelist(['fine'], DocumentAccess::MODE_PRIVATE);
        $this->filter->setMode(DocumentAccess::MODE_PRIVATE);
        $filtered = $this->filter->filter($data);
        $this->assertArrayHasKey('fine', $filtered);
        $this->assertEquals($data['fine'], $filtered['fine']);
        $this->assertArrayNotHasKey('missing', $filtered);
        $this->assertArrayNotHasKey('1', $filtered);
    }

    public function testBlackListWithDifferentMode()
    {
        $data = [
            'fine' => 'yes',
            'missing' => 'Not present',
            '1' => 'still here'
        ];
        $this->filter->setBlacklist(['missing', 'fine'], DocumentAccess::MODE_PROTECTED);
        $this->filter->setBlacklist(['1'], DocumentAccess::MODE_PUBLIC);
        $this->filter->setBlacklist(['1'], DocumentAccess::MODE_PRIVATE);
        $this->filter->setMode(DocumentAccess::MODE_PROTECTED);
        $filtered = $this->filter->filter($data);
        $this->assertArrayHasKey('1', $filtered);
        $this->assertEquals($data['1'], $filtered['1']);
        $this->assertArrayNotHasKey('missing', $filtered);
        $this->assertArrayNotHasKey('fine', $filtered);
    }

    public function testWhitelistWithAllAndPublicMode()
    {
        $data = [
            'fine' => 'yes',
            'missing' => 'Not present',
            '1' => 'still here'
        ];
        $this->filter->setWhitelist(['fine', '1'], ListFilter::MODE_ALL);
        $this->filter->setWhitelist(['fine', 'missing'], DocumentAccess::MODE_PUBLIC);
        $this->filter->setMode(DocumentAccess::MODE_PUBLIC);
        $filtered = $this->filter->filter($data);
        $this->assertArrayHasKey('fine', $filtered);
        $this->assertEquals($data['fine'], $filtered['fine']);
        $this->assertArrayNotHasKey('missing', $filtered);
        $this->assertArrayNotHasKey('1', $filtered);
    }

    public function testBlackListWithAllAndPrivateMode()
    {
        $data = [
            'fine' => 'yes',
            'missing' => 'Not present',
            '1' => 'still here'
        ];
        $this->filter->setBlacklist(['1'], ListFilter::MODE_ALL);
        $this->filter->setBlacklist(['missing'], DocumentAccess::MODE_PRIVATE);
        $this->filter->setMode(DocumentAccess::MODE_PRIVATE);
        $filtered = $this->filter->filter($data);
        $this->assertArrayHasKey('fine', $filtered);
        $this->assertEquals($data['fine'], $filtered['fine']);
        $this->assertArrayNotHasKey('missing', $filtered);
        $this->assertArrayNotHasKey('1', $filtered);
    }

    /**
     * @expectedException \Kotka\Filter\Exception\InvalidArgumentException
     */
    public function testSettingInvalidMode()
    {
        $this->filter->setMode('invalid');
    }

    /**
     * @expectedException \Kotka\Filter\Exception\InvalidArgumentException
     */
    public function testSettingInvalidModeWithWhiteList()
    {
        $this->filter->setWhitelist(['test'], 'invalid');
    }

    /**
     * @expectedException \Kotka\Filter\Exception\InvalidArgumentException
     */
    public function testSettingInvalidModeWithBlackList()
    {
        $this->filter->setBlacklist(['test'], 'invalid');
    }

}
 