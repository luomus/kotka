<?php
return array(
    
    'ZfcDatagrid' => array(
        
        'settings' => array(
            
            'default' => array(
                // If no specific rendere given, use this renderes for HTTP / console
                'renderer' => array(
                    'http' => 'bootstrapTable',
                    'console' => 'zendTable',
                ),
            ),
            
            'export' => array(
                //Export is enabled?
                'enabled' => false,
                
                'formats' => array(
                    //type => Displayname (Toolbar - you can use here HTML too...)
                    //'ZendTable' => 'tableRow',
                ),
                
                // The output+save directory
                'path' => 'data/generated_excels',
                
                // mode can be:
                // direct = PHP handles header + file reading
                // @TODO iframe = PHP generates the file and a hidden <iframe> sends the document (ATTENTION: your webserver must enable "force-download" for excel/pdf/...)
                'mode' => 'direct'
            )
            
        ),
        
        //The cache is used to save the filter + sort and other things for exporting        
        'cache' => array(
            
            'adapter' => array(
                'name' => 'Filesystem',
                'options' => array(
                    'ttl' => 720000, // cache with 200 hours,
                    'cache_dir' => 'data/ZfcDatagrid'
                )
            ),
            'plugins' => array(
                'exception_handler' => array(
                    'throw_exceptions' => false
                ),

                'Serializer'
            )
        ),
        
        'renderer' => array(
            
            'bootstrapTable' => array(
                'parameterNames' => array(
                    'currentPage' => 'page',
                    'sortColumns' => 'sortBy',
                    'sortDirections' => 'sortDirection'
                )
            ),
            'json' => array(
                'parameterNames' => array(
                    'currentPage' => 'page',
                    'sortColumns' => 'sortBy',
                    'sortDirections' => 'sortDirection'
                )
            ),
            'PHPExcel' => array(
                'sheetName' => 'kotka_export',
                'displayTitle' => true,
                'rowTitle' => 0,
                'startRowData' => 1,
                'papersize' => 'A4',
                'orientation' => 'landscape',
            )
        ),
        
        // General parameters
        'generalParameterNames' => array(
            'rendererType' => 'rendererType'
        )
    ),
    
    'service_manager' => array(
        'invokables' => array(
            'DatagridManager' => 'ZfcDatagrid\ServiceManager\DatagridManagerFactory',
            'zfcDatagrid.renderer.bootstrapTable' => 'ZfcDatagrid\Renderer\BootstrapTable\Renderer',
            'zfcDatagrid.renderer.json' => 'ZfcDatagrid\Renderer\Json\Renderer',
            'zfcDatagrid.renderer.excel' => 'ZfcDatagrid\Renderer\PHPExcel\Renderer',
        ),
        'factories' => array(
            'zfcDatagrid' => 'ZfcDatagrid\Service\DatagridFactory'
        )
    ),
    
    'view_helpers' => array(
        'invokables' => array(
            'bootstrapTableRow' => 'ZfcDatagrid\Renderer\BootstrapTable\View\Helper\TableRow',
        )
    ),
    
    'view_manager' => array(
        
        'template_map' =>  include __DIR__  .'/../template_map.php',

    ),

);
