<?php
namespace ZfcDatagrid\DataSource;

use Doctrine\ODM\MongoDB\Query\Builder as QueryBuilder;
use ZfcDatagrid\Column;
use ZfcDatagrid\DataSource\Mongo\Paginator as PaginatorAdapter;

class Mongo extends AbstractDataSource
{

    /**
     *
     * @var QueryBuilder
     */
    private $qb;

    /**
     * Data source
     *
     * @param mixed $data            
     */
    public function __construct($data)
    {
        if ($data instanceof QueryBuilder) {
            $this->qb = $data;
        } else {
            $return = $data;
            if (is_object($data)) {
                $return = get_class($return);
            }
            throw new \InvalidArgumentException("Unknown data input..." . $return);
        }
    }

    /**
     *
     * @return QueryBuilder
     */
    public function getData()
    {
        return $this->qb;
    }

    public function execute()
    {
        $qb = $this->getData();
        $qb->hydrate(false);

        /**
         * Step 1) Apply needed columns
         */
        $selectColumns = array();
        foreach ($this->getColumns() as $column) {
            if ($column instanceof Column\Select) {
                $selectColumns[] =$column->getUniqueId();
            }
        }
        call_user_func_array(array($qb, "select"), $selectColumns);

        /**
         * Step 2) Apply filters
         */
        $filterColumn = new Mongo\Filter($qb);
        foreach ($this->getFilters() as $filter) {
            if ($filter->isColumnFilter() === true) {
                $filterColumn->applyFilter($filter);
            }
        }

        /**
         * Step 3) Apply sorting
         */
        if (count($this->getSortConditions()) > 0) {
            // Minimum one sort condition given -> so reset the default orderBy

            foreach ($this->getSortConditions() as $sortCondition) {
                /* @var $column \ZfcDatagrid\Column\AbstractColumn */
                $column = $sortCondition['column'];
                $qb->sort($column->getUniqueId(), $sortCondition['sortDirection']);
            }
        }

        /**
         * Step 4) Pagination
         */
        $this->setPaginatorAdapter(new PaginatorAdapter($qb->getQuery()->execute()));
    }
}
