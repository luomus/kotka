<?php
namespace ZfcDatagrid\DataSource\Mongo;

use Doctrine\ODM\MongoDB\Query\Builder as QueryBuilder;
use MongoRegex;
use ZfcDatagrid\Filter as DatagridFilter;

class Filter
{

    /**
     *
     * @var QueryBuilder
     */
    private $qb;

    public function __construct(QueryBuilder $qb)
    {
        $this->qb = $qb;
    }

    /**
     *
     * @return \Doctrine\ODM\MongoDB\Query\Builder
     */
    public function getQueryBuilder()
    {
        return $this->qb;
    }

    /**
     * @param DatagridFilter $filter
     * @throws \InvalidArgumentException
     */
    public function applyFilter(DatagridFilter $filter)
    {
        $qb = $this->getQueryBuilder();
        
        $column = $filter->getColumn();
        $colString = $column->getSelectPart1();
        if ($column->getSelectPart2() != '') {
            $colString .= '.' . $column->getSelectPart2();
        }
        $values = $filter->getValues();

        foreach ($values as $key => $value) {
            $valueParameterName = ':' . str_replace('.', '', $column->getUniqueId() . $key);
            
            switch ($filter->getOperator()) {
                
                case DatagridFilter::LIKE:
                    $qb->field($colString)->equals(new MongoRegex('/'.$value.'/'));
                    break;
                
                case DatagridFilter::LIKE_LEFT:
                    $qb->field($colString)->equals(new MongoRegex('/'.$value.'$/'));
                    break;
                
                case DatagridFilter::LIKE_RIGHT:
                    $qb->field($colString)->equals(new MongoRegex('/^'.$value.'/'));
                    break;
                
                case DatagridFilter::NOT_LIKE:
                    $qb->field($colString)->notEqual(new MongoRegex('/'.$value.'/'));
                    break;
                
                case DatagridFilter::NOT_LIKE_LEFT:
                    $qb->field($colString)->notEqual(new MongoRegex('/'.$value.'$/'));
                    break;
                
                case DatagridFilter::NOT_LIKE_RIGHT:
                    $qb->field($colString)->notEquals(new MongoRegex('/^'.$value.'/'));
                    break;
                
                case DatagridFilter::EQUAL:
                    $qb->field($colString)->equals($value);
                    break;
                
                case DatagridFilter::NOT_EQUAL:
                    $qb->field($colString)->notEqual($value);
                    break;
                
                case DatagridFilter::GREATER_EQUAL:
                    $qb->field($colString)->gte($valueParameterName);
                    break;
                
                case DatagridFilter::GREATER:
                    $qb->field($colString)->gt($valueParameterName);
                    break;
                
                case DatagridFilter::LESS_EQUAL:
                    $qb->field($colString)->lte($valueParameterName);
                    break;
                
                case DatagridFilter::LESS:
                    $qb->field($colString)->lt($valueParameterName);
                    break;
                
                case DatagridFilter::BETWEEN:
                    $minParameterName = ':' . str_replace('.', '', $colString . '0');
                    $maxParameterName = ':' . str_replace('.', '', $colString . '1');

                    $qb->field($colString)->range($minParameterName, $maxParameterName);

                    break 2;
                
                default:
                    throw new \InvalidArgumentException('This operator is currently not supported: ' . $filter->getOperator());
                    break;
            }
        }
    }
}
