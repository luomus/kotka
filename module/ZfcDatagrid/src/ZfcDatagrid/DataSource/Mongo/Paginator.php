<?php

namespace ZfcDatagrid\DataSource\Mongo;

use Doctrine\ODM\MongoDB\Cursor;
use Doctrine\ODM\MongoDB\Query;
use Zend\Paginator\Adapter\AdapterInterface;

class Paginator implements AdapterInterface
{

    /**
     *
     * @var Cursor
     */
    protected $cursor = null;

    /**
     * Total item count
     *
     * @var integer
     */
    protected $rowCount = null;

    /**
     *
     * @param Cursor $cursor
     */
    public function __construct(Cursor $cursor)
    {
        $this->cursor = $cursor;

    }

    /**
     *
     * @return Cursor
     */
    private function getCursor()
    {
        return $this->cursor;
    }

    /**
     * Returns an array of items for a page.
     *
     * @param integer $offset            
     * @param integer $itemCountPerPage            
     * @return array
     */
    public function getItems($offset, $itemCountPerPage)
    {

        $cursor = $this->getCursor();
        $cursor->skip($offset);
        $cursor->limit($itemCountPerPage);

        return $cursor->toArray();
    }

    /**
     * Returns the total number of rows in the result set.
     *
     * @return integer
     */
    public function count()
    {
        return $this->getCursor()->count();
    }
}