<?php
namespace ZfcDatagrid\Column\Type;

class Identifications extends AbstractType
{

    public function getTypeName ()
    {
        return 'Identifications';
    }

    /**
     * Convert a value into an array
     *
     * @param mixed $value            
     * @return array
     */
    public function getUserValue ($value)
    {
        if (!is_array($value)) {
            return '';
        }
        $ret = array();
        foreach ($value as $key => $identification) {
            $iden =  "<strong>$key:</strong> " ;
            foreach ($identification as $field => $value) {
                $iden .= $field . ':&nbsp;' . $value . '<br />';
            }
            array_push($ret, $iden);
        }
        return implode('<br />', $ret);
    }
}
