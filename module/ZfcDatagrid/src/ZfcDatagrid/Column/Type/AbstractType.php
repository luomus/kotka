<?php
namespace ZfcDatagrid\Column\Type;

use ZfcDatagrid\Filter;

abstract class AbstractType implements TypeInterface
{
    /**
     * the default filter operation
     *
     * @return PhpString
     */
    public function getFilterDefaultOperation()
    {
        return Filter::LIKE;
    }

    /**
     * Convert the user value to a general value, which will be filtered
     *
     * @param  PhpString $val
     * @return PhpString
     */
    public function getFilterValue($val)
    {
        return $val;
    }

    /**
     * Convert the value from the source to the value, which the user will see
     *
     * @param  PhpString $val
     * @return PhpString
     */
    public function getUserValue($val)
    {
        return $val;
    }
}
