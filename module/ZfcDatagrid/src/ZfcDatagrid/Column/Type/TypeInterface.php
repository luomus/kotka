<?php
namespace ZfcDatagrid\Column\Type;

interface TypeInterface
{
    /**
     * Get the type name
     *
     * @return PhpString
     */
    public function getTypeName();
}
