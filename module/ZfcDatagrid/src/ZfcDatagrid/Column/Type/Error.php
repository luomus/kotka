<?php
namespace ZfcDatagrid\Column\Type;

class Error extends AbstractType
{

    public function getTypeName ()
    {
        return 'error';
    }

    /**
     * Convert a value into an array
     *
     * @param mixed $value            
     * @return array
     */
    public function getUserValue ($value)
    {
        if (!is_array($value)) {
            $value = array($value);
        }
        $return = array();
        array_walk_recursive($value, function($a) use (&$return) { $return[] = $a; });
        $return = array_filter($return);
        if (count($return)) {
            return '<p>' . implode('</p><p>', $return) . '</p>';
        }
        return '';
    }
}
