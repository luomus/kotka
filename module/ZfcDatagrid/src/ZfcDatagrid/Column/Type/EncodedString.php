<?php
namespace ZfcDatagrid\Column\Type;

class EncodedString extends AbstractType
{

    public function getTypeName ()
    {
        return 'encoded';
    }

    /**
     * Convert a value into an array
     *
     * @param mixed $value            
     * @return array
     */
    public function getUserValue ($value)
    {
        return utf8_encode($value);
    }
}
