<?php
namespace ZfcDatagrid\Column\Style;

/**
 * Background color of a column/row
 */
class RowClass extends AbstractStyle
{
    private $class;

    public function __construct($class) {
        $this->class = $class;
    }

    /**
     * @return string
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * @param string $class
     */
    public function setClass($class)
    {
        $this->class = $class;
    }

}