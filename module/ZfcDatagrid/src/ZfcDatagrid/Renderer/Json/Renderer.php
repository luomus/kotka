<?php
/**
 * Render datagrid as CSV
 *
 */
namespace ZfcDatagrid\Renderer\Json;

use ZfcDatagrid\Column\Type;
use ZfcDatagrid\Renderer\AbstractExport;

class Renderer extends AbstractExport
{

    public static $params;

    public function getName()
    {
        return 'json';
    }

    public function isExport()
    {
        return false;
    }

    public function isHtml()
    {
        return true;
    }

    /**
     * @return \Zend\View\Model\ViewModel
     */
    public function execute()
    {
        return ['paginator' => $this->getPaginator()];
    }

    public function getCurrentPageNumber()
    {
        $optionsRenderer = $this->getOptionsRenderer();
        $parameterNames = $optionsRenderer['parameterNames'];
        $params = $this->getRequest();
        $this->currentPageNumber = (int) isset($params[$parameterNames['currentPage']]) ? $params[$parameterNames['currentPage']] : 1;

        return (int) $this->currentPageNumber;
    }

    /**
     *
     * @see \ZfcDatagrid\Renderer\AbstractRenderer::getSortConditions()
     *
     * @return array
     */
    public function getSortConditions()
    {
        if (is_array($this->sortConditions)) {
            // set from cache! (for export)
            return $this->sortConditions;
        }

        $params = $this->getRequest();

        $optionsRenderer = $this->getOptionsRenderer();
        $parameterNames = $optionsRenderer['parameterNames'];

        $sortConditions = array();
        $sortColumns = isset($params[$parameterNames['sortColumns']]) ? $params[$parameterNames['sortColumns']] : null;
        $sortDirections = isset($params[$parameterNames['sortDirections']]) ? $params[$parameterNames['sortDirections']] : null;
        if ($sortColumns != '') {
            $sortColumns = explode(',', $sortColumns);
            $sortDirections = explode(',', $sortDirections);

            if (count($sortColumns) != count($sortDirections)) {
                throw new \Exception('Count missmatch order columns/direction');
            }

            foreach ($sortColumns as $key => $sortColumn) {
                $sortDirection = strtoupper($sortDirections[$key]);

                if ($sortDirection != 'ASC' && $sortDirection != 'DESC') {
                    $sortDirection = 'ASC';
                }

                foreach ($this->getColumns() as $column) {
                    /* @var $column \ZfcDatagrid\Column\AbstractColumn */
                    if ($column->getUniqueId() == $sortColumn) {
                        $sortConditions[] = array(
                            'sortDirection' => $sortDirection,
                            'column' => $column
                        );

                        $column->setSortActive($sortDirection);
                    }
                }
            }
        }

        if (count($sortConditions) > 0) {
            $this->sortConditions = $sortConditions;
        } else {
            // No user sorting -> get default sorting
            $this->sortConditions = $this->getSortConditionsDefault();
        }

        return $this->sortConditions;
    }

    /**
     *
     * @return \Zend\Stdlib\RequestInterface
     */
    public function getRequest()
    {
        return self::$params;
    }

}
