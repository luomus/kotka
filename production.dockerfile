# base image
FROM php:7.1-fpm

ENV ORACLE_INSTANTCLIENT=https://cdn.laji.fi/_inst/
ENV LD_LIBRARY_PATH=/usr/local/instantclient
ENV ORACLE_HOME=/usr/local/instantclient

RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"

# install dependencies
RUN apt-get update \
    && apt-get install -y libpng-dev libturbojpeg-dev libwebp-dev libxpm-dev libldap2-dev \
       zlib1g-dev libzip-dev libicu-dev libaio-dev g++ git libxml2-dev \
       wget unzip \
    && mkdir -p /usr/lib/x86_64-linux-gnu \
    && docker-php-ext-configure ldap --with-libdir=lib/x86_64-linux-gnu/ \
    && docker-php-ext-install ldap \
    && docker-php-ext-configure intl \
    && docker-php-ext-install intl \
    && docker-php-ext-configure exif \
    && docker-php-ext-install exif \
    && docker-php-ext-configure gd \
    && docker-php-ext-install gd \
    && docker-php-ext-configure soap \
    && docker-php-ext-install soap \
    && docker-php-ext-configure zip --with-libzip \
    && docker-php-ext-install zip \
    && pecl install xdebug-2.6.0 \
    && docker-php-ext-enable xdebug \
    && wget ${ORACLE_INSTANTCLIENT}instantclient-basic-linux.x64-12.2.0.1.0.zip -P /tmp/ \
    && wget ${ORACLE_INSTANTCLIENT}instantclient-sdk-linux.x64-12.2.0.1.0.zip -P /tmp/ \
    && unzip /tmp/instantclient-basic-linux.x64-12.2.0.1.0.zip -d /usr/local/ \
    && unzip /tmp/instantclient-sdk-linux.x64-12.2.0.1.0.zip -d /usr/local/ \
    && ln -s /usr/local/instantclient_12_2 /usr/local/instantclient \
    && ln -s /usr/local/instantclient/libclntsh.so.12.1 /usr/local/instantclient/libclntsh.so \
    && echo 'instantclient,/usr/local/instantclient' | pecl install oci8-2.2.0 \
    && apt-get remove -y wget unzip \
    && rm -rf /tmp/instantclient-sdk-linux.x64-12.2.0.1.0.zip \
    && rm -rf /tmp/instantclient-sdk-linux.x64-12.2.0.1.0.zip \
    && rm -rf /var/lib/apt/lists/*

# configure dependencies
RUN echo "error_reporting = E_ALL" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    && echo "display_startup_errors = On" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    && echo "display_errors = On" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    && echo "opcache.enable=1" >> /usr/local/etc/php/conf.d/opcache.ini \
    && echo "opcache.validate_timestamps=1" >> /usr/local/etc/php/conf.d/opcache.ini \
    && echo "opcache.max_accelerated_files=10000" >> /usr/local/etc/php/conf.d/opcache.ini \
    && echo "opcache.memory_consumption=192" >> /usr/local/etc/php/conf.d/opcache.ini \
    && echo "opcache.max_wasted_percentage=10" >> /usr/local/etc/php/conf.d/opcache.ini \
    && echo "opcache.interned_strings_buffer=16" >> /usr/local/etc/php/conf.d/opcache.ini \
    && echo "opcache.fast_shutdown=1" >> /usr/local/etc/php/conf.d/opcache.ini \
    && echo "date.timezone = Europe/Helsinki" >> /usr/local/etc/php/conf.d/docker-kotka-php-limit.ini \
    && echo "max_file_uploads = 101" >> /usr/local/etc/php/conf.d/docker-kotka-php-limit.ini \
    && echo "max_execution_time = 600" >> /usr/local/etc/php/conf.d/docker-kotka-php.ini \
    && echo "max_input_vars = 10000" >> /usr/local/etc/php/conf.d/docker-kotka-php-limit.ini \
    && echo "memory_limit = 4096M" >> /usr/local/etc/php/conf.d/docker-kotka-php-limit.ini \
    && echo "post_max_size = 350M" >> /usr/local/etc/php/conf.d/docker-kotka-php-limit.ini \
    && echo "upload_max_filesize = 350M" >> /usr/local/etc/php/conf.d/docker-kotka-php-limit.ini \
    && echo "session.cookie_lifetime = 432000" >> /usr/local/etc/php/conf.d/docker-kotka-php.ini \
    && echo "session.gc_maxlifetime = 432000" >> /usr/local/etc/php/conf.d/docker-kotka-php.ini \
    && echo "extension=oci8.so" > /usr/local/etc/php/conf.d/php-oci8.ini

WORKDIR /var/www/html

# add deps
COPY composer* ./
RUN mkdir -p ./data/generated_classes \
    && php composer.phar install --no-dev --no-scripts --ansi --no-interaction

# add other code files
COPY docker-entrypoint.sh ./
COPY bin ./bin
COPY config ./config
COPY module ./module
COPY public ./public
COPY version.dist.php ./
COPY init_autoloader.php ./

# Need to give permissions here since openshift build doesn't recognice the --chown parameter in COPY
RUN chown www-data:www-data -R .

ENTRYPOINT ["./docker-entrypoint.sh"]
CMD ["php-fpm"]