#!/bin/bash
set -e

mkdir -p ./data/cache/file
mkdir -p ./data/cache/perma
mkdir -p ./data/cache/triplestore
mkdir -p ./data/context
mkdir -p ./data/generated_classes
mkdir -p ./data/generated_excels
mkdir -p ./data/imported_files
mkdir -p ./data/logs
mkdir -p ./data/mail
mkdir -p ./data/modulecache
mkdir -p ./data/sessions
mkdir -p ./data/upload/permits
mkdir -p ./data/upload/tmp
mkdir -p ./data/upload/transaction
mkdir -p ./data/user-logs/export
mkdir -p ./data/user-logs/import
mkdir -p ./data/ZfcDatagrid
mkdir -p ./vendor/luomus/triplestore/src/Triplestore/Classes/Hydrator

php composer.phar install --no-dev --no-scripts --ansi --no-interaction
php public/index.php generate class
php composer.phar dump-autoload -o

if [ -z "$WITH_WORKERS" ]
then
  echo "Set WITH_WORKERS env variable to workers on background"
  echo "For manual run use the following:"
  echo "php public/index.php queue beanstalkd slow"
  echo "php public/index.php queue beanstalkd default"
else
  php public/index.php queue beanstalkd slow &
  php public/index.php queue beanstalkd default &
fi

if [ -z "$WITH_SPECIMEN" ]
then
  echo "Set WITH_SPECIMEN=true env variable to index all samples on start up"
  if [ -z "$WITH_SAMPLES" ]
  then
    echo "Set WITH_SAMPLES=true env variable to index all samples on start up"
  else
    php public/index.php update reindex-all-sample
  fi

  if [ -z "$WITH_BRANCHES" ]
  then
    echo "Set WITH_BRANCHES=true env variable to index all branches on start up"
  else
    php public/index.php update reindex-all-branches
  fi
else
  # Specimen already includes samples and branhces so no need to reindex them if this is chosen.
  php public/index.php update reindex-all
fi

curl -k -XPUT "http://elastic:9200/kotka/_settings" -d'{"index":{"max_result_window":5000000}}' || true
curl -k -XPUT "http://elastic:9200/kotka_sample/_settings" -d'{"index":{"max_result_window":2000000}}' || true
curl -k -XPUT "http://elastic:9200/kotka_branch/_settings" -d'{"index":{"max_result_window":2000000}}' || true

exec "$@"