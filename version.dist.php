<?php
$version_time = new DateTime();
define('KOTKA_BUILD',$version_time->format('h'));
define('KOTKA_LAST_UPDATE', $version_time->format('d/m/Y H:m'));
unset($version_time);