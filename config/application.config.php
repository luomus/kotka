<?php
$env = getenv('KOTKA_ENV') ?: 'production';

if (!defined('KOTKA_ENV')) {
    define('KOTKA_ENV', $env);
}

if (!defined('KOTKA_BUILD')) {
    define('KOTKA_BUILD', 42);
}

if (!defined('NEW_KOTKA_URL')) {
  define('NEW_KOTKA_URL', $env !== 'production' ? 'https://kotka-dev.laji.fi' : 'https://kotka.laji.fi');
}

$useCache = true;
$modules = array(
    'User',
    'Triplestore',
    'Kotka',
    'PdfCreator',
    'Api',
    'KotkaConsole',
    'ZfcDatagrid',
    'Importer',
    'SlmQueue',
    'SlmQueueBeanstalkd',
    'ImageApi',
    'Elastic',
    'Trimble'
);

if ($env !== 'test' && $env !== 'dev') {
   array_push($modules, 'Admin', 'ZfBeanstalkdUI');
}

if ($env == 'dev') {
    $useCache = false;
}


return array(
    'modules' => $modules,
    'module_listener_options' => array(
        'module_paths' => array(
            './module',
            './vendor'
        ),
        'config_glob_paths' => array('config/autoload/{,*.}{global,local}.php'),
        'config_cache_enabled' => $useCache,
        'config_cache_key' => KOTKA_BUILD,
        'module_map_cache_enabled' => $useCache,
        'module_map_cache_key' => KOTKA_BUILD,
        'cache_dir' => 'data/modulecache',
        'check_dependencies' => ($env != 'production'),
    )
);