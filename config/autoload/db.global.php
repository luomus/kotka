<?php
return array(
    'db' => array(
        'adapters' => array(
            'triplestore' => array(
                'database' =>  getenv('TRIPLESTORE_USERNAME')
            )
        )
    ),
    'triplestore' => array(
        'classNS' => 'Kotka\Triple',
        'classDir' => __DIR__ . '/../../data/generated_classes',
        'repositoryNS'=>'Kotka\Repository',
        'database' => array(
            'host' => getenv('TRIPLESTORE_HOST'),
            'username' => getenv('TRIPLESTORE_USERNAME'),
            'password' => getenv('TRIPLESTORE_PASSWORD'),
            'database' => getenv('TRIPLESTORE_DATABASE'),
            'charset' => 'AL32UTF8'
        ),
        'generator' => [
            'context' => [
                'path' => './data/context'
            ]
        ]
    ),
);