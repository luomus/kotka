<?php
return array(
    'mailer' => array(
        'type' => 'file',
        'file_options' => array(
            'path' => 'data/mail'
        )
    )
);
