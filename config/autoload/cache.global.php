<?php
return array(
    'cache' => array(
        'adapter' => array(
            'name' => 'filesystem',
            'options' => array (
                'cache_dir' => 'data/cache/file',
                'ttl'       => 7200,
            )
        ),
        'plugins' => array(
            'exception_handler' => array(
                'throw_exceptions' => false
            ),
            'serializer' => array (
                'serializer' => 'Zend\Serializer\Adapter\PhpSerialize',
            )
        )
    ),
    'perma-cache' => array (
        'adapter' => array(
            'name' => 'filesystem',
            'options' => array (
                'cache_dir'   => 'data/cache/perma',
                'key_pattern' => '/^[a-z0-9_\+\-\.]*$/Di',
                'ttl'         => 43200,
            )
        ),
        'plugins' => array(
            'exception_handler' => array(
                'throw_exceptions' => false
            ),
            'serializer' => array (
                'serializer' => 'Zend\Serializer\Adapter\PhpSerialize',
            )
        )
    ),
    'cache-enabled-classes' => array(
        //'\\Zend\\Paginator\\Paginator',
    )
);