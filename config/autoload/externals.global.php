<?php
return array(
    'externals' => array(
        'apiLaji' => array(
            'url' => getenv('API_LAJI_BASE'),
            'pass' => getenv('API_LAJI_TOKEN')
        ),
        'triplestoreApi' => array(
            'url' => getenv('TRIPLESTORE_API_BASE'),
            'user' => getenv('TRIPLESTORE_API_USERNAME'),
            'pass' => getenv('TRIPLESTORE_API_PASSWORD'),
        ),
        'lajiETL' => array(
            'url' => getenv('LAJI_ETL_URL'),
            'user' => getenv('LAJI_ETL_USERNAME'),
            'pass' => getenv('LAJI_ETL_PASSWORD'),
        ),
        'mustikka' => array(
            'url' => getenv('MUSTIKKA_URL'),
            'user' => getenv('MUSTIKKA_USERNAME'),
            'pass' => getenv('MUSTIKKA_PASSWORD'),
        ),
        'coordinate' => array(
            'url' => getenv('COORDINATE_URL'),
            'user' => getenv('COORDINATE_USERNAME'),
            'pass' => getenv('COORDINATE_PASSWORD')
        ),
        'tipuApi' => array(
            'url' => getenv('TIPU_URL'),
            'user' => getenv('TIPU_USERNAME'),
            'pass' => getenv('TIPU_PASSWORD')
        ),
        'digitariumApi' => array(
            'url' => getenv('DIGITARIUM_API_URL')
        ),
        'namespaceWS' => [
            'url' => getenv('NAMESPACE_URL')
        ],
        'newKotka' => array(
            'url' => getenv('NEW_KOTKA_BASE'),
            'pass' => getenv('NEW_KOTKA_AUTH')
        ),
    )
);