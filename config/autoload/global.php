<?php
/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */
return array(
    'view_manager' => [
        'display_exceptions' => getenv('DISPLAY_EXCEPTIONS') === 'true',
    ],
    'environment' => getenv('KOTKA_ENV') ?: 'production',
    'pdf_converted' => getenv('PDF_TO_HTML'),
    'imageserver' => array(
        'server_location' => getenv('IMAGE_API_SERVER'),
        'system' => getenv('IMAGE_API_USERNAME'),
        'username' => getenv('IMAGE_API_USERNAME'),
        'password' => getenv('IMAGE_API_PASSWORD'),
    ),
    'elastic' => array(
        'client' => array(
            'sniffOnStart' => false,
            'SSLVerification' => getenv('ELASTICSEARCH_VERIFY_SSL') === 'false' ? false : true,
            'hosts' => array(
                getenv('ELASTICSEARCH') ?: '127.0.0.1:9200',
            )
        )
    ),
    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Db\Adapter\AdapterAbstractServiceFactory',
        ),
    ),
    'excel' => array(
        'directory' => 'data/generated_excels',
    ),
    'search' => array(
        'export_dir' => 'data/generated_excels',
    ),
    'auth' => array(
        'userClass' => '\Kotka\Triple\MAPerson',
        'systemId' => getenv('SYSTEM_ID'),
        'lajiAuthUri' => getenv('AUTH_URL'),
        'ldap' => array(
            'tammi' => array(
                'host' => getenv('AUTH_LDAP_HOST'),
                'baseDn' => getenv('AUTH_LDAP_BASE_DN'),
                'accountDomainName' => getenv('AUTH_LDAP_ADN'),
                'port' => getenv('AUTH_LDAP_PORT')
            )
        ),
        'httpClientOptions' => array(
            'adapter' => 'Zend\Http\Client\Adapter\Curl',
            'curloptions' => array(
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_TIMEOUT => 10,
            ),
        )
    ),
    'accessionYearlyNumberReset' => getenv('ACCESSIOM_YEARLY_NUMBER'),
    'session_config' => array(
        'name' => 'KOTKA_ID',
        'remember_me_seconds' => 432000,
        'cookie_lifetime' => 0,
        'use_cookies' => true,
        'cookie_httponly' => true,
        'cookie_secure' => false
    ),
    'test' => getenv('TEST_USERNAME') ? [
        'login' => [
            'username' => getenv('TEST_USERNAME'),
            'password' => getenv('TEST_PASSWORD'),
        ]
    ] : []
);
