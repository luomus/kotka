<?php
return array(
    'slm_queue' => array(
        'beanstalkd' => array(
            'connection' => array(
                'host' => getenv('BEANSTALKD_HOST') ?: '127.0.0.1',
                // 'port' => 11300,
                // 'timeout' => 2
            ),
        ),
        'worker_strategies' => array (
                'default' => array (
                    'SlmQueue\\Strategy\\MaxRunsStrategy' => array (
                        'max_runs' => 100000,
                    ),
                    'SlmQueue\\Strategy\\MaxMemoryStrategy' => array (
                        'max_memory' => 2 * 1024 * 1024 * 1024,
                    )
                ),
                'queues' => array (
                    'default' => array (
                        'tube' => 'default'
                    ),
                ),
            ),
         /**
          * Allow to configure a specific queue.
          *
          * Available options depends on the queue factory
          */
         'queues' => array(
             'default' => array(
                 'tube' => 'default'
             )
         ),

        /**
         * Allow to configure dependencies for jobs that are pulled from any queue. This works like any other
         * PluginManager in Zend Framework 2. For instance, if you want to inject something into every job using
         * a factory, just adds an element into the "factories" array, with the key being the FQCN of the job,
         * and the value the factory:
         *
         * 'job_manager' => array(
         *     'factories' => array(
         *         'Application\Job\UserJob' => 'Application\Factory\UserJobFactory'
         *     )
         * )
         *
         * Therefore, the job will be created through the factory (the identifier and content of the job will be
         * automatically set after creation). Note that this plugin manager is configured as such it automatically
         * add any unknown classes to the invokables list. This means you should only add factories and/or abstract
         * factories here.
         */
        'job_manager' => array(
            'factories' => array(
                'Kotka\Job\Elastic'     => 'Kotka\Job\ElasticFactory',
                'Kotka\Job\Specimen'    => 'Kotka\Job\SpecimenFactory',
                'Kotka\Job\Images'      => 'Kotka\Job\ImagesFactory',
                'Kotka\Job\Delete'      => 'Kotka\Job\DeleteFactory',
                'Kotka\Job\Mustikka'    => 'Kotka\Job\MustikkaFactory',
                'Kotka\Job\ImportExcel' => 'Kotka\Job\ImportExcelFactory',
                'Kotka\Job\ExportExcel' => 'Kotka\Job\ExportExcelFactory',
            )
        ),

        /**
         * Allow to add queues. You need to have at least one queue. This works like any other PluginManager in
         * Zend Framework 2. For instance, if you have a queue whose name is "email", you can add it as an
         * invokable this way:
         *
         * 'queue_manager' => array(
         *     'invokables' => array(
         *         'email' => 'Application\Queue\MyQueue'
         *     )
         * )
         *
         * Please note that you can find built-in factories for several queue systems (Beanstalk, Amazon Sqs...)
         * in SlmQueueSqs and SlmQueueBeanstalk
         */
        'queue_manager' => array(
            'factories' => array(
                'default' => 'SlmQueueBeanstalkd\Factory\BeanstalkdQueueFactory',
                'slow' => 'SlmQueueBeanstalkd\Factory\BeanstalkdQueueFactory'
            )
        )
    ),
);
