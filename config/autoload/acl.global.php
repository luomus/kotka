<?php
return array(
    'acl' => array(
        'role' => array (
            // role -> multiple parents
            'MA.guest'    => null,
            'MA.member'   => array('MA.guest'),
            'MA.advanced' => array('MA.member'),
            'MA.admin'    => null,
        ),
        'resource' => array (
            // resource -> single parent
            'user'      => null, // user module
            'admin'     => null, // admin module
            'js'        => null, // js controller in kotka
            'viewer'    => null, // viewer controller in kotka
            'type-list' => 'viewer',
            'maps'      => 'viewer',
            'document'  => null, // document controller
            'excel-generator' => null, // excel controller
            'index'     => null, // index controller in kotka
            'kotka'     => null, // all the rest of kotka modules pages
            'image'     => 'kotka', // image section
            'api'       => null, // api module
            'tools'     => null, // tools controller
            'culture'   => null, // culture controller
            'advanced'  => null, // advanced features
        ),
        'deny'  => array (
            // array('role', 'resource', array('permission-1', 'permission-2', ...)),
            array('MA.guest', null, null), // null as parameter means all
            array('MA.member', 'image', ['delete', 'extra-fields']),
            array('MA.member', 'admin', null),
            array('MA.member', 'advanced', null),
        ),
        'allow' => array (
            // array('role', 'resource', array('permission-1', 'permission-2', ...)),
            array('MA.guest', 'user', null),
            array('MA.guest', 'culture', null),
            array('MA.guest', 'index', array('http')),
            array('MA.guest', 'tools', array('import', 'labels', 'preview', 'generic-label', 'status', 'qr-reader')),
            array('MA.guest', 'viewer', null),
            array('MA.guest', 'js', array('init', 'aggregate')),
            array('MA.guest', 'api', null),
            array('MA.guest', 'document', array('field')),
            array('MA.guest', 'excel-generator', null),
            array('MA.guest', 'image', array('taxon', 'edit', 'get', 'put')),
            array('MA.member', 'index', null),
            array('MA.member', 'js', null),
            array('MA.member', 'kotka', null),
            array('MA.member', 'user', null),
            array('MA.member', 'tools', null),
            array('MA.advanced', 'image', ['delete']),
            array('MA.advanced', 'advanced', null),
            array('MA.admin', null, null), // the admin can do anything with the accounts
        ),
        'defaults' => array (
            'guest_role' => 'MA.guest', // role for the use not logged in
            'member_role' => 'MA.guest',  // role for the logged in user if it doesn't have role or the role is not recognised
        ),
        'resource_aliases' => array (
            'Admin' => 'admin',
            'Kotka' => 'kotka',
            'Api' => 'api',
            'Kotka\Controller\View' => 'viewer',
            'Kotka\Controller\Js' => 'js',
            'Kotka\Controller\Types' => 'type-list',
            'Kotka\Controller\Index' => 'index',
            'Kotka\Controller\Tools' => 'tools',
            'Kotka\Controller\Maps' => 'maps',
            'Kotka\Controller\Document' => 'document',
            'Kotka\Controller\Excel' => 'excel-generator',
            'Kotka\Controller\Image' => 'image',
            'Kotka\Controller\Culture' => 'culture',
        ),
        // List of modules to apply the ACL. This is how we can specify if we have to protect the pages in our current module.
        'modules' => array (
            'User',
            'Kotka',
            'Admin',
            'Api',
            'ZfBeanstalkdUI'
        ),
    )
);