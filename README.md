Kotka
=======================

Introduction
------------
This is the source code of Kotka collection management system, which is being built for Finnish natural history collections. [More information about Kotka](https://wiki.helsinki.fi/xwiki/bin/view/digit/Kotka%20Collection%20Management%20System/)

In addition to this repository, using Kotka depends on Luomus' ontology database, triplestore editor and triplestore API. *If you are interested in testing Kotka, please contact kotka(ät)luomus.fi.*

Kotka is released under [MIT license](https://bitbucket.org/luomus/kotka/src/HEAD/LICENSE.txt?fileviewer=file-view-default).

Installation (dev enviroment)
------------
* You need the following software installed:
    * [docker-desktop](https://www.docker.com/products/docker-desktop)
    * git
* Clone this repository
  
* Copy .env-template file to .dev.env and update it's content to match

* In the newly created folder run

    `docker-compose up`
  (if there has been changes in any of the docker files you can run `docker-compose up --build` to get those changes)

* Optional if you need cli access to the machine you can run
 
     `docker-compose exec kotka /bin/bash`
     
* Open browser to address [127.0.0.1](http://127.0.0.1)

* Note: User that is provided in Laji-auth login must exist in the development triplestore database (ltkm_kotka) and the user must have Kotka role and at least one organization. 

* To close the env you can issue a command

    `docker-compose down -v --remove-orphans`

Update 3rd party Luomus libraries
------------
To update dependencies you first need to make the changes to the dependant repository
and then on [prod2](https://wiki.helsinki.fi/xwiki/bin/view/luomusict/Luomus%20Biodiversity%20Informatics%20Unit/Servers/fmnh-ws-prod2.it.helsinki.fi/) machine run the following:

```
sudo su -
cd /data/packagist
./update.sh
```

This will update package repository to contain the newest items.
On your local machine do the following when Kotka is up and running

```
docker-compose exec kotka /bin/bash
php composer.phar update
cp composer.lock public/composer.lock
```

Now in your IDE you will see composer.lock file in the public folder.
You need to copy only the relative section in that file the composer.lock file in the root of the repo.
**DO NOT** copy all since there are breaking changes in other packages that would require resolving them first.
Ones you've done that you can delete the composer.lock in the public folder and commit the changes in the composer.lock.
This will trigger the redeployment process to the kotkatest. 
To test the changes locally, run `docker-compose up --build`.
