#!/bin/bash
USAGE="Usage: update.sh <version number>"
TODAY=$(date +"%F")
UPDATE_LOG="update.log"
VERSION_FILE="version.php"
VERSION=$1

if [ $(id -u) -ne 0 ]; then
  echo "Please run as root"
  exit 1;
fi

if [ $# == 0 ]; then
    echo ${USAGE}
    exit 1;
fi

function writeVersion {
    cat > ${VERSION_FILE} <<EOL
<?php
define('KOTKA_BUILD','${VERSION}');
define('KOTKA_LAST_UPDATE', '${TODAY}');
EOL
}

# Clear update log
echo "" > ${UPDATE_LOG}

echo "Fetching updates"
git fetch --tags > ${UPDATE_LOG} 2>&1
if [ $? -ne 0 ]; then
  echo "Unable to fecth updates"
  exit 1;
fi

git rev-parse b${VERSION} > ${UPDATE_LOG} 2>&1
if [ $? -ne 0 ]; then
  echo "Version ${VERSION} didn't exist in repository"
  exit 1;
fi

echo "Changing to version ${VERSION}"
git checkout tags/b${VERSION} > ${UPDATE_LOG} 2>&1
if [ $? -ne 0 ]; then
  echo "Failed to checkout wanted version"
  exit 1;
fi

writeVersion

echo "Updating dependencies and writing autoload files"
php composer.phar install --no-dev -o > ${UPDATE_LOG} 2>&1
if [ $? -ne 0 ]; then
  echo "Failed to update dependencies with composer"
  exit 1;
fi

echo "Generating classes"
php public/index.php generate class

echo "Changing permissions"
chown apache:apache -R .

echo "${VERSION} ${TODAY}" >> version_history.txt

echo "Restart background jobs"
killall -9 php

echo "DONE!"
