#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
KOTKADIR="$DIR/../resourses/less"

cd $KOTKADIR
lessc -x --clean-css ./kotka.less ../../public/css/kotka.css
cd -
echo "Done"
