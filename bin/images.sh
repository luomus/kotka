#!/bin/bash
#
# This file is made for parsing image data from the atlantis dump
# It will eventually safe the files in format oid_<originalID>_<internal img id>.jpg
# From there is should be simple to map to correct document ids
#


echo "Splitting dump into files"
csplit $1 '/^mmitem:$/' '{*}' -k

echo "Keep first part as src.xml"
mv xx00 src.xml

echo "Picking data from the file and removing extra lines"
for i in ./xx*; do
    # Process file
    SIZE=`head $i -n 5 | sed -n '5p'`
    if [ "$SIZE" == "m_hires" ]; then
        ID=`head $i -n 2 | sed -n '2p'`

        if tail -n +7 $i > img_$ID.jpg ; then
            printf "."
        else
            printf "x"
        fi
    fi
    rm -rf $i
done

echo "Splitting to accessions"
csplit src.xml '/<BTAccession/' '{*}' -k

# TODO: Something fishy in xx20 file (it has many identifications). Need to check it manually
mv xx20 weird.xml

echo "Moving images to files with original id in the name"
for file in $(grep MMImage ./xx* -l); do
    ORIGINAL_ID=`grep -oP 'm_identificatie="\K[^"]*' $file`

    for OID in $(grep -oP 'OID="\K[^"]*' $file); do
        if [ -e ./img_$OID.jpg ]; then
           mv ./img_$OID.jpg ./oid_${ORIGINAL_ID}_$OID.jpg
        fi
    done
done

echo "Clearing extra data"
rm -rf ./xx*