@ECHO OFF
SET DIR=%~dp0
SET KOTKA_ROOT="%DIR%../"

if "%1"=="" goto usage
if "%2"=="wait" goto wait
:start
pushd %KOTKA_ROOT%

@echo Fetching updates
call git fetch --tags
if %errorlevel% neq 0 goto :error
call git rev-parse b%1% >nul 2>&1
if %errorlevel% neq 0 goto :errorNoVersion
@echo Changing to version %1%:
call git checkout tags/b%1% > null
if %errorlevel% neq 0 goto :error

@echo Updating composer:
call php composer.phar install --no-dev -o >nul
if %errorlevel% neq 0 goto :error

@echo Generating classes:
call php public/index.php generate class >nul
if %errorlevel% neq 0 goto :error

@echo Setting up version.php
goto :write
:back1

@echo %1% >> version_history.txt
@echo Done!
goto :eof

:usage
    @echo Usage: %0 ^<version^>
    goto :close

:write
    for /f "tokens=1-4 delims=./ " %%i in ("%date%") do (
        set dow=%%i
        set month=%%k
        set day=%%j
        set year=%%l
    )
    set datestr=%day%/%month%/%year%
    @echo ^<?php > version.php
    @echo define('KOTKA_BUILD','%1%'); >> version.php
    @echo define('KOTKA_LAST_UPDATE', '%datestr%'); >> version.php
    goto :back1

:wait
    @echo Wainting for 10sec
    ping 127.0.0.1 -n 10 > nul
    goto :start

:error
    @echo Error occurred halting!
    goto :close

:errorNoVersion
    @echo Couldn't find version %1% from the repository
    goto :close

:close
if "%2"=="wait" exit %errorlevel%
exit /b %errorlevel%