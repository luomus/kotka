FROM nginx:1.19

WORKDIR /app

COPY ./config/site.conf /etc/nginx/conf.d/default.conf
COPY ./public ./