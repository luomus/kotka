var $form = $('form');
function setPage(page) {
    $('input[name="page"]').val(page);
}
function sortGrid(by, direction) {
    $('input[name="sortBy"]').val(by);
    $('input[name="sortDirection"]').val(direction);
    //$('input[name="perPage"]').val($('select[name="itemsPerPage"]').val());
    $form.submit();
}

function goToPage(page) {
    setPage(page);
    $form.submit();
}

function perPageChange(elem) {
    var $this = $(elem);
    $('input[name="perPage"]').val($this.val());
    setPage(1);
    $form.submit();
}
