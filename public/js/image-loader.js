function loadImage(qname) {
    $.getJSON('/apiwrapper/image/' + qname, function(response) {
        generateImageViewContentAndLinkToAddImages(response);
    });
}

function generateImageViewContentAndLinkToAddImages(jsonResponse) {
    var html = generateImageViewContent(jsonResponse);
    var $images = $("#img");
    $images.html(html);
    $images.magnificPopup({
        delegate: 'a',
        type: 'image',
        gallery: {
            enabled: true
        }
    })
}
function generateImageViewContent(jsonResponse) {
    var response = jQuery('<div class="well"/>');
    jsonResponse = jsonResponse[0];
    if (jsonResponse.num_images === undefined || jsonResponse.num_images == '0') {
        return response.append('No images for this specimen.');
    }
    var images = jsonResponse.images;
    for (var index in jsonResponse.images) {
        var image = images[index];
        var fullImageURL = image.web_url;
        var thumbnailURL = image.thumbnail;
        var imageName = image.filename;
        var link = '<a href="' + fullImageURL + '" target="_blank"><img style="max-width:100px; max-height: 100px" src="' + thumbnailURL + '" alt="' + imageName + '"/></a>';
        response.append('<div class="col-sm-4"><figure>' + link + '<figcaption class="small">' + imageName + '</figcaption></figure></div>');
    }
    response.append('<div class="row"/>');
    return response;
}