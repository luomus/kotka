
// IE10 viewport hack for Surface/desktop Windows 8 bug
//
// See Getting Started docs for more information
if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
    var msViewportStyle = document.createElement("style");
    msViewportStyle.appendChild(
        document.createTextNode(
            "@-ms-viewport{width:auto!important}"
        )
    );
    document.getElementsByTagName("head")[0].
        appendChild(msViewportStyle);
}

var $window   = $(window);
var $body     = $(document.body);
var $specimen = $('#specimen');
var navHeight = $('.navbar').outerHeight(true) + 10;
var $form     = $('#MYDocument');
var lajiAutocomplete = 'https://laji.fi/api/autocomplete';

$form.on("keyup keypress", 'input, select', function(e) {
    var code = e.keyCode || e.which;
    if (code  == 13) {
        e.preventDefault();
        return false;
    }
});

var navSpot;
function reBuildNav(depth, $parent) {
    if (typeof  depth === "number") {
        depth++;
    } else {
        depth = 1;
        navSpot = 1;
    }
    var nav      = '';
    var dataAttr = 'data-nav-' + depth;
    var ulClass  = 'nav';
    var $items;
    if(typeof $parent === "undefined") {
        $items  = $('[' + dataAttr + ']');
        ulClass = 'nav bs-sidenav';
    } else {
        $items = $parent.find('[' + dataAttr + ']');
    }
    $items.each(function() {
        var $this = $(this);
        var id = 'nav-' + navSpot++;
        var name = $this.data('nav-' + depth);
        $this.attr('id',id);
        nav += '<li><a href="#' + id + '" class="to-element">' + name + '</a>';
        nav += reBuildNav(depth, $this);
        nav += '</li>';
    });
    if (nav !== '') {
        nav = '<ul class="' + ulClass + '">' + nav + '</ul>';
    }
    return nav;
}

$('#sidenav').html(reBuildNav());
$('body').on('click', '.to-element', function(e) {
    var name = $(e.target).attr('href');
    scrollToElement(name);
    e.preventDefault();
});

$body.scrollspy({
    target: '#sidenav',
    offset: navHeight
});

$(function() {
    $('.panel-collapse').collapse({
        toggle: false
    });
    $('#toggle-panel-hide').click(function(e) {
        $('.panel-collapse').collapse('hide');
        e.preventDefault();
    });
    $('#toggle-panel-show').click(function(e) {
        $('.panel-collapse').collapse('show');
        e.preventDefault();
    });
});

$specimen.on('shown.bs.collapse', function (e) {
    $body.scrollspy('refresh');
});

$specimen.on('hidden.bs.collapse', function (e) {
    $body.scrollspy('refresh');
});

$specimen.on('click', '.add-tab', function (e) {
    e.preventDefault();
    var elem = this.dataset['target'];
    var placeholder = this.dataset['placeholder'];
    var placement = this.dataset['placement'];
    var type = this.dataset['type'];
    var $container = $("#" + elem );
    var count = parseInt($container.data('count'));
    var name = $container.data('template-name');
    var element = $container.data('template-elem');
    $.get('/form/' + element + '?name=' + name + '&type=' + type, function(template) {
        var regex = new RegExp(placeholder, "g");
        var $template = $(template.replace(regex, count));
        add_advanced_fields($template);
        if (advancedVisible) {
            $template.find('.advanced').show()
        }
        $container.find('.collapse').removeClass('in');
        if (placement === 'top') {
            $container.prepend($template);
        } else {
            $container.append($template);
        }
        $container.data('count', count + 1);
        $specimen.trigger('requestRefresh');
    });

});

$specimen.on('click', '.generate-additional-ids', function () {
    var inputs$ =$(this).parents('.form-group').find('input[type=text]');
    var values = [];
    var has = false;
    inputs$.each(function() {
        var value = $(this).val();
        values.push(value);
        if (value.indexOf(':') !== -1) {
            has = true;
        }
    });
    if (!has) {
        alert('You need to have value with ":"-character at the end');
        return;
    }
    if (!confirm('Are you sure that you want to generate ids for additional ids?')) {
        return;
    }
    $.get('/js/data/generate-ids.json?g=' + encodeURIComponent(JSON.stringify(values)))
        .success(function (data) {
            inputs$.each(function(idx) {
                $(this).val(data[idx]);
            });
        })
        .fail(function () {
            alert('Failed to generate');
        });
});

$specimen.on('click', 'li a .close', function() {
    var tabId = $(this).parents('li').children('a').attr('href');
    $(this).parents('li').remove('li');
    $(tabId).remove();
});


function clearCoordinateSystem(system) {
    return system.replace('MY.coordinateSystem', '').toLowerCase()
}

var undoCnt = 1;
function enableUndo(dataSource, callBack) {
    var id = 'undo_' + undoCnt++;
    $body.append('<div id="' + id + '" class="alert alert-warning alert-dismissable top"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Coordinates updated from the map <a href="#" class="undo" data-dismiss="alert" data-event="' + callBack + '"  data-source="' + dataSource + '">undo</a></div>');
    window.setTimeout(function() {
        $("#" + id).fadeTo(500, 0).slideUp(500, function(){
            $(this).remove();
        });
    }, 5000);
}

// Map part of specimen
function updateMap(elem) {
    var $elem = $('#' + elem);
    var WKMap = $elem.data('WKMap');
    var sysSource = $elem.data('system-source');
    var latSource = $elem.data('latitude-source');
    var lonSource = $elem.data('longitude-source');
    var latTarget = $elem.data('latitude-target');
    var lonTarget = $elem.data('longitude-target');
    var lat  = $(latSource + ' input').val();
    var lon  = $(lonSource + ' input').val();
    var $sys = $(sysSource + ' select');
    var verbatimTarget = $elem.data('verbatim-target');
    var undo = $elem.data('undo');

    if (lat.length > 0 && lon.length > 0) {
        var latFlaot = parseFloat(lat.replace(',','.'));
        var lonFlaot = parseFloat(lon.replace(',','.'));
        if ($sys.val() == '') {
            if (lat.search(/^[0-9\-\.,]*$/i) === -1 || lon.search(/^[0-9\-\.,]*$/i) === -1) {
                return
            } else if (lat.indexOf(',') === -1 && lat.indexOf('.') === -1 && lat.length >= 3 && lat.length == lon.length
                    && lon.indexOf(',') === -1 && lon.indexOf('.') === -1 && lon.length >= 3) {
                $sys.val('MY.coordinateSystemYkj');
            } else if (latFlaot <= 90 && latFlaot >= -90 && lonFlaot <= 180 && lonFlaot >= -180) {
                $sys.val('MY.coordinateSystemWgs84');
            } else {
                return;
            }
        }
        var sys = clearCoordinateSystem($sys.val());
        $.get('/apiwrapper/coordinates?lat=' + lat + '&lon=' + lon + '&system=' + sys,
            function(data) {
                lat = data[0];
                lon = data[1];
                if (lat === undefined || lon === undefined) {
                    if (undo && WKMap.hasMarkers()) {
                        if (!confirm("Are you sure that you want to change coordinates to something that cannot be displayed on map?")) {
                            undoMapChanges(elem);
                            return;
                        }
                    }
                    WKMap.clearMarkers();
                    return
                }
                $(latTarget + ' input').val(lat);
                $(lonTarget + ' input').val(lon);
                WKMap.importData([{
                    locationType: "POINT",
                    location: [lat, lon],
                    notes: ""
                }]);
                WKMap.reCenter(WKMap.getCenter(), 10);
                undo = {
                    'system':$(sysSource + ' select').val(),
                    'longitudeTarget': $(lonTarget + ' input').val(),
                    'latitudeTarget': $(latTarget + ' input').val(),
                    'latitudeSource': $(latSource + ' input').val(),
                    'longitudeSource': $(lonSource + ' input').val(),
                    'verbatimTarget': $(verbatimTarget + ' input').val()
                };
                $elem.data('undo', undo);
            });
    }
}

$specimen.on('change', '.map-latitude', function() {
    var $mapGroup = $(this).closest('.map-group');
    var $elem = $mapGroup.find('.has-map');
    updateMap($elem.attr('id'));
});

$specimen.on('change', '.map-longitude', function() {
    var $mapGroup = $(this).closest('.map-group');
    var $elem = $mapGroup.find('.has-map');
    updateMap($elem.attr('id'));
});

$specimen.on('change', '.MYCoordinateSystem', function() {
    var $mapGroup = $(this).closest('.map-group');
    var $elem = $mapGroup.find('.has-map');
    updateMap($elem.attr('id'));
});

$specimen.on('change', '.depending-select .source select', function() {
    var $srcSelect = $(this);
    var $dep = $srcSelect.closest('.depending-select');
    var $target = $dep.find('.target');
    var $targetSelect = $target.find('select');
    var value = $srcSelect.val();
    var map = JSON.parse($dep.data('depending').replace(/'/g, '"'));
    var showValue = map[value] || [];
    if (showValue.length > 0) {
        $target.show();
    } else {
        $target.hide();
    }
    if (showValue.indexOf($targetSelect.val()) === -1) {
        if ($dep.hasClass('init')) {
            showValue.push($targetSelect.val());
        } else {
            $targetSelect.val('');
        }
    }
    $dep.removeClass('init');
    $targetSelect.find('option').each(function() {
       var $option = $(this);
       var optionValue = $option.attr('value');
       if (optionValue === '') {
           return;
       }
       if (showValue.indexOf(optionValue) !== -1) {
           $option.show();
       } else {
           $option.hide();
       }
    });
});

var mapConfig = {
    single: true,
    edit: true,
    annotate: false,
    mapOptions: {
        scale: true,
        center: [61.783513, 25.642089],
        zoom: 6
    }
};

var taxonName = new Bloodhound({
    datumTokenizer: function(d) {return d.scientificName; },
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    limit: 20,
    remote: {
        url: lajiAutocomplete + '/taxon?limit=20&matchType=exact,partial&includePayload=true&q=%QUERY',
        wildcard: '%QUERY',
        filter: function(list) {
            var result = [];
            var ids = {};
            for (var i in list) {
                var suggest = list[i];
                if (!list.hasOwnProperty(i) || ids[suggest['key']] ||!suggest.payload) {
                    continue;
                }
                ids[suggest['key']] = true;
                for (var k in suggest.payload) {
                    if (!suggest.payload.hasOwnProperty(k)) {
                        continue;
                    }
                    suggest[k] = suggest.payload[k];
                }
                result.push(suggest);
            }
            return result;
        }
    }
});
taxonName.initialize();

$specimen.on('requestRefresh', function () {
    $form.find('.add-datetimepicker')
        .removeClass('add-datetimepicker')
        .datetimepicker({
            widgetPositioning: {
                vertical: 'bottom'
            },
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down",
                previous: "fa fa-angle-left",
                next: "fa fa-angle-right"
            }
        });

    $('textarea').not('.has-auto-size').addClass('has-auto-size').autosize();
    $('#sidenav').html(reBuildNav());
    $body.scrollspy('refresh');

    $('.add-map').removeClass('add-map').addClass('has-map').each(function() {
        var $this = $(this);
        var WKMap = L.wkartta($this.attr('id'), mapConfig);
        var latTarget = $this.data('latitude-target');
        var lonTarget = $this.data('longitude-target');
        var lat = $(latTarget + ' input').val();
        var lon = $(lonTarget + ' input').val();
        $this.data('WKMap', WKMap);
        if (lat && lon) {
            setTimeout(function() {
                updateMap($this.attr('id'));
            }, 1000);
        }

        WKMap.on('change',function(mapEntries) {
            mapEntries = mapEntries.data;
            var lat = '' + mapEntries[0]['location'][0];
            var lon = '' + mapEntries[0]['location'][1];
            if (lat !== undefined && lon !== undefined) {
                lat = lat.substring(0, 7);
                lon = lon.substring(0, 7);
                var lonTarget = this.data('longitude-target');
                var sysSource = this.data('system-source');
                var latSource = this.data('latitude-source');
                var lonSource = this.data('longitude-source');
                var latTarget = this.data('latitude-target');
                var verbatimTarget = this.data('verbatim-target');
                var undo = {
                    'system':$(sysSource + ' select').val(),
                    'longitudeTarget': $(lonTarget + ' input').val(),
                    'latitudeTarget': $(latTarget + ' input').val(),
                    'latitudeSource': $(latSource + ' input').val(),
                    'longitudeSource': $(lonSource + ' input').val(),
                    'verbatimTarget': $(verbatimTarget + ' input').val()
                };
                $this.data('undo', undo);
                if (undo['system'] !== '' ||
                    undo['latitudeSource'] !== '' ||
                    undo['latitudeTarget'] !== '' ||
                    undo['longitudeSource'] !== '' ||
                    undo['longitudeTarget'] !== '') {
                    if (!confirm("Are you sure you want to move the marker here?")) {
                        undoMapChanges($this.attr('id'));
                        return;
                    }
                }
                $(sysSource + ' select').val('MY.coordinateSystemWgs84');
                $(latTarget + ' input').val(lat);
                $(latSource + ' input').val(lat);
                $(lonTarget + ' input').val(lon);
                $(lonSource + ' input').val(lon);
            }
        }, $this);
    });

    $('.add-measurement')
        .removeClass('add-measurement')
        .each(function() {
            var $this = $(this);
            var $measurements = $this.parents('.measurements');
            var $fieldContainer = $measurements.find('.measurement-container');

            var fields = $measurements.data('no-value').map(function(field) {
                field.lowerLabel = field.label.toLowerCase();
                return field;
            });
            var indexes = $measurements.data('indexes');

            $this.typeahead({
                hint: false,
                highlight: true,
                minLength: 0
            }, {
                name: 'Measurement',
                display: 'label',
                source: function(query, cb) {
                    if (!query) {
                        return cb(fields);
                    }
                    var result = [];
                    var lowerQuery = query.toLowerCase();
                    for (var i = 0; i < fields.length; i++) {
                        if (fields[i].lowerLabel.indexOf(lowerQuery) !== -1) {
                            result.push(fields[i])
                        }
                    }
                    cb(result);
                },
                limit: 50
            }).bind('typeahead:selected', function(event, selected) {
                var $template = $(selected.value.replace('__index__', indexes[selected.label]));
                indexes[selected.label]++;
                $fieldContainer.append($template);
                $this.typeahead('val', '');
                $template.find('.form-control').focus();
            });

            $this.bind('blur', function() {
                $this.typeahead('close');
                $this.val('');
            });
        });

    $('.MYTaxon').not('.tt-input').each(function() {
        var $this = $(this);
        var $group  = $this.parent().parent().parent().parent();
        var $rank   = $group.find('.MYTaxonRank');
        var $author = $group.find('.MYAuthor');
        $this.typeahead({
            hint: false,
            highlight: true,
            minLength: 3
        }, {
            name: 'Taxon',
            displayKey: 'scientificName',
            limit: 20,
            source: taxonName.ttAdapter()
        }).bind('typeahead:selected', function(obj, selected, name) {
            if (typeof selected.payload.taxonRankId !== "undefined" && $rank.find("option[value='" + selected.payload.taxonRankId + "']").length > 0) {
                $rank.val(selected.payload.taxonRankId);
            } else {
                $rank.val('');
            }
            if (typeof selected.payload.scientificNameAuthorship !== "undefined") {
                $author.val(selected.payload.scientificNameAuthorship);
            } else {
                $author.val('');
            }
            $rank.change();
        }).off('blur');

        $this.bind('blur', function() {
            $this.typeahead('close');
        });
    });

    $('.depending-select.init .source select').trigger('change');
});

function undoMapChanges(source) {
    var $source = $('#' + source);
    var data = $source.data('undo');
    var sysSource = $source.data('system-source');
    var latSource = $source.data('latitude-source');
    var latTarget = $source.data('latitude-target');
    var lonSource = $source.data('longitude-source');
    var lonTarget = $source.data('longitude-target');
    var verbatimTarget = $source.data('verbatim-target');
    $(sysSource + ' select').val(data.system);
    $(latTarget + ' input').val(data.latitudeTarget);
    $(latSource + ' input').val(data.latitudeSource);
    $(lonTarget + ' input').val(data.longitudeTarget);
    $(lonSource + ' input').val(data.longitudeSource);
    $(verbatimTarget + ' input').val(data.verbatimTarget);
    updateMap(source);
}

$body.on('click', '.undo', function(e) {
    var $this = $(this);
    var functionName = $this.data('event');
    var source = $this.data('source');
    var fn = window[functionName];
    if (typeof fn === "function") fn(source);
    e.preventDefault();
});

$specimen.trigger('requestRefresh');

// original sample additional id validator
$body.on('blur', '.MFAdditionalIDsWrap input', function (e) {
    $(this).on('blur', function () {
        var $elem = $(this);
        var $group = $elem.closest('.input-group');
        var value = $elem.val();
        if (!value) {
            return;
        }
        $.get('/js/data/check-sample-additionalID.json?value=' + value)
            .success(function (data) {
                var $help = $group.find('.help-block').first();
                $group.removeClass('has-success has-warning has-error');
                if (data.status === 'ok') {
                    $group.addClass('has-success');
                    $help.html('');
                } else {
                    $group.addClass('has-warning');
                    if ($help.length === 0) {
                        $group.append('<span class="help-block" style="display: table-row"></span>');
                        $help = $group.find('.help-block').first();
                    }
                    $help.html(data.searchLink);
                }
            })
            .fail(function () {});
    });
});

// original catalog number validator
$('.MYOriginalSpecimenID').on('blur', function () {
   var $elem = $(this);
   var $group = $elem.closest('.form-group');
   var value = $elem.val();
   if (!value) {
       return;
   }
   $.get('/js/data/check-original-specimenID.json?value=' + value)
       .success(function (data) {
           var $help = $group.find('.help-block').first();
           $group.removeClass('has-success has-warning has-error');
           if (data.status === 'ok') {
               $group.addClass('has-success');
               $help.html('');
           } else {
               $group.addClass('has-warning');
               if ($help.length === 0) {
                   $group.append('<span class="help-block"></span>');
                   $help = $group.find('.help-block').first();
               }
               $help.html(data.searchLink);
           }
       })
       .fail(function () {});
});


var taxonChecked = '';

// Taxon validation
var validateTaxon = function() {
    var $causeElement = $(this);
    var $group = $causeElement.closest('.form-group');
    var taxon = $group.find('.MYTaxon').first().val();
    var taxonRank = $group.find('.MYTaxonRank').first().val();
    var taxonAuthor = $group.find('.MYAuthor').first().val();
    var $help = $group.find('.help-block').first();
    var newCheck = taxon + '_' + taxonRank + '_' + taxonAuthor;

    if (taxon.length === 0 || newCheck === taxonChecked) {
        return;
    }
    taxonChecked = newCheck;

    $.getJSON(lajiAutocomplete + '/taxon?matchType=exact&includePayload=true&q=' + taxon, function (data) {
        var resultText;
        var resultClass;
        var hasMatch = false;
        if (data.length > 0) {
            for (var i in data) {

                if (hasMatch || !data.hasOwnProperty(i)) {
                    continue;
                }
                var match = ['Name'];
                var missMatch = [];
                if (taxonRank === data[i].payload.taxonRankId) {
                    match.push('taxon rank');
                } else {
                    missMatch.push('taxon rank');
                }
                if (typeof data[i].payload.scientificNameAuthorship !== 'undefined') {
                    if (taxonAuthor.replace(/ /g,'') === data[i].payload.scientificNameAuthorship.replace(/ /g,'')) {
                        match.push('author');
                        if (match.length === 3) {
                            hasMatch = true;
                        }
                    } else {
                        missMatch.push('author');
                    }
                } else if (!taxonAuthor && match.length === 2) {
                    hasMatch = true;
                }

                resultText = match.join(', ') + " OK";
                resultClass = "has-success";

                if (missMatch.length > 0) {
                    resultClass = "has-warning";
                    resultText += ", " + missMatch.join(', ') + " mismatch";
                }
                resultText += " (" + data[i].key + ")";
            }
            if (!hasMatch) {
                resultText = "Found multiple names";
                resultClass = "has-warning";
            }
        } else {
            resultText = "Name not found";
            resultClass = "has-error";
        }
        $group.removeClass('has-success has-warning has-error');
        $group.addClass(resultClass);
        $help.text(resultText);
    });
};



$specimen.on('change', '.MYTaxonRank', validateTaxon);
$specimen.on('blur', '.MYTaxon', validateTaxon);
$specimen.on('blur', '.MYAuthor', validateTaxon);

var valueForId = function (id) {
    return document.getElementById(id).value;
};

// Unreliable fields
var $unreliable = $('#unreliableFields');
var fields = $unreliable.val().split('\n');
$.each(fields, function(index, item) {
    var $elem = $('[name="' + item + '"]:not(".reset")');
    $elem.val('? ' + $elem.val());
});
$unreliable.val('');

var advancedVisible = !$('.advanced:first-child').is(':visible');
var markingAdvanced = false;
var changeAdvanced  = false;

$('.toggle_advanced').on('click', function(e) {
    e.preventDefault();
    advancedVisible = !advancedVisible;
    var $stat = $(this).find('span');
    if (advancedVisible) {
        $('.advanced').show();
        $($stat[0]).hide();
        $($stat[1]).show();
    } else {
        $('.advanced').hide();
        $($stat[0]).show();
        $($stat[1]).hide();
    }
    $specimen.trigger('requestRefresh');
    window.KotkaApp.setRemember('advanced', advancedVisible);

});

$specimen.on('mousedown focus', '.form-control', function(e) {
    if (!markingAdvanced) {
        return;
    }
    e.preventDefault();
    var $this = $(this);
    if ($this.parent().siblings('em').hasClass('required')) {
        $this.closest('.advanced-group').find('.advanced-mark').removeClass('advanced-mark');
        alert("Can't mark required field as advanced field!");
        return;
    }
    if ($this.hasClass('advanced-mark')) {
        $this.closest('.advanced-group').find('.advanced-mark').removeClass('advanced-mark');
    } else {
        $this.addClass('advanced-mark');
    }
    $this.blur();
});

$('.mark_advanced').on('click', function(e) {
    e.preventDefault();
    var $this = $(this);
    var $advanceToggle = $('.toggle_advanced');
    var $select2 = $('.select2-offscreen');
    var $submits = $('[type="submit"]:not([disabled="disabled"])');
    if (markingAdvanced) {
        var $marked = $('.advanced-mark').removeClass('advanced-mark');
        $select2.select2("enable", true);
        $submits.removeClass('disabled');
        var fields = [];
        $marked.each(function() {
            var $this = $(this);
            var name = $this.attr('name');
            var prefix = '';
            if (typeof(name) == 'undefined') {
                if ($this.attr('id') == 's2id_datasetID') {
                    name = 'MYProjectId';
                } else {
                    return;
                }
            }
            if ($this.hasClass('MYIdentification')) {
                prefix = 'MYIdentification_';
            } else if ($this.hasClass('MYTypeSpecimen')) {
                prefix = 'MYTypeSpecimen_';
            } else if ($this.hasClass('MYUnit')) {
                prefix = 'MYUnit_';
            } else if ($this.hasClass('MYGathering')) {
                prefix = 'MYGathering_';
            } else if ($this.hasClass('MYDocument')) {
                prefix = 'MYDocument_';
            }
            var parts = name.split('[');
            var field = parts.pop();
            field = field.replace(']','');
            if (isNumber(field) || field == '') {
                field = parts.pop().replace(']','');
            }
            field = prefix + field;
            if (fields.indexOf(field) == -1) {
                fields.push(field);
            }
        });
        $advanceToggle.show();
        $this.text('Start marking advanced fields').parent().removeClass('active');
        window.KotkaApp.setRemember('advanced-fields', fields);
        $specimen.trigger('advanced.mark');

        if (!changeAdvanced) {
            advancedVisible = !advancedVisible;
        }
        changeAdvanced = false;

        $advanceToggle.trigger('click');
    } else {
        if (!advancedVisible) {
            changeAdvanced = true;
            $advanceToggle.trigger('click');
        }
        var $advanced = $('.advanced:not(.text)').removeClass('advanced');
        $advanced
            .find('input:not(.btn)').addClass('advanced-mark').end()
            .find('select').addClass('advanced-mark').end()
            .find('textarea').addClass('advanced-mark').end()
            .find('.select2-container').addClass('advanced-mark');
        $advanceToggle.hide();
        $select2.select2("enable", false).each(function() {
            var $cont = $($(this).select2('container'));
            $cont.removeClass('select2-container-disabled');
        });
        $submits.addClass('disabled');


        $this.text("Marking advanced fields. Stop and save").parent().addClass('active');
    }
    markingAdvanced = !markingAdvanced;
});

$specimen.on('advanced.mark', function () {
    add_advanced_fields($specimen);
});

function add_advanced_fields($content) {
    var advanced = window.KotkaApp.getRemember('advanced-fields');
    if (advanced === false) {
        advanced = [
            'MZPublicityRestrictions','MYLegID','MYAdditionalIDs','MYDataSource','MYPublication','MYSeparatedFrom','MYSeparatedTo','MYDuplicatesIn','MYAcquiredFrom','MYAcquisitionDate','MYExsiccatum','MYPreservation','MYURL','MYLanguage',
            'MYCoordinateRadius','MYAlt','MYDepth','MYAFEQuadrat','MYUTMQuadrat',
            'MYSubstrate','MYLocalityID','MYLocalityDescription','MYHabitatDescription','MYHabitatClassification','MYNotes',
            'MYSex','MYAge','MYCount','MYPopulationAbundance','MYHost','MYHostDBH','MYHostDecayStage','MYChemistry','MYMicroscopy','MYMacroscopy','MYRing','MYPreparations','MYNotes',
            'MYGenusQualifier','MYSpeciesQualifier','MYTaxonID','MYIdentificationNotes', 'MYCoordinateNotes'
        ];
    }
    for (var i in advanced) {
        var field = advanced[i];
        var find  = '';
        var parts = field.split('_');
        if (typeof(parts[1]) !== 'undefined') {
            find = "." + parts[0] + "." + parts[1];
        } else {
            find = "." + parts[0];
        }
        $content.find(find).closest('.advanced-group').addClass('advanced');
    }
    $content.find('[data-advanced]').each(function() {
        var $this = $(this);
        var cnt = $this.find('.advanced').length;
        if (cnt >= $this.data('advanced')) {
            $this.addClass('advanced');
        }
    });
}

init.push(function() {
    $specimen.trigger('advanced.mark');
    var advanced = this.getRemember('advanced');
    if (advanced) {
        advancedVisible = !advancedVisible;
    }
    $('.toggle_advanced').trigger('click');
});

function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

$('#history').on('change', function() {
    var history = $(this).val();
    var subject = getUrlParameter('uri');
    var url;
    url = 'https://' + window.location.host + '/specimens/edit?uri=' + subject + '&history=' + history;
    window.location.href = url;
});

function getUrlParameter(sParam)
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++)
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam)
        {
            return sParameterName[1];
        }
    }
}