(function($) {

    var spot = getUrlParameter('spot');
    var page = getUrlParameter('page');
    var uri  = getUrlParameter('uri');
    var type = getUrlParameter('type') || 'document';
    if (typeof spot == 'undefined' || typeof page == 'undefined' || typeof uri == 'undefined') {
        return;
    }

    $.get('/js/data/navigate.json?type=' + type + '&page=' + page + '&spot=' + spot + '&uri=' + uri, function (data) {
        var $link = $('.navigation-links');
        var url = $link.data('nav-base');
        if (data.prev) {
            $link.find('.navigation-link-prev')
                .attr('href', url + data.prev)
                .removeClass('disabled');
            $link.removeClass('hidden');
        }
        if (data.next) {
            $link.find('.navigation-link-next')
                .attr('href', url + data.next)
                .removeClass('disabled');
            $link.removeClass('hidden');
        }
    });

    function getUrlParameter(sParam)
    {
        var sPageURL = window.location.search.substring(1);
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++)
        {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] == sParam)
            {
                return sParameterName[1];
            }
        }
    }
})(jQuery);
