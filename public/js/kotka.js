var Kotka;
(function (Kotka) {
    var Util = /** @class */ (function () {
        function Util() {
        }
        Util.errorDialog = function (title, msg) {
            BootstrapDialog.show({
                title: title,
                message: msg,
                size: BootstrapDialog.SIZE_SMALL,
                type: BootstrapDialog.TYPE_DANGER,
                buttons: [{
                        label: 'ok',
                        cssClass: 'btn-danger',
                        action: function (dialog) {
                            dialog.close();
                        }
                    }]
            });
        };
        return Util;
    }());
    Kotka.Util = Util;
    var IdService = /** @class */ (function () {
        function IdService() {
            IdService.map = { 'tun:': 'http://tun.fi/', 'luomus:': 'http://id.luomus.fi/' };
        }
        IdService.getQName = function (val, generated) {
            if (generated === void 0) { generated = false; }
            if (val.indexOf('http:') === -1) {
                if (val.indexOf(':') === -1) {
                    if (generated) {
                        return IdService.defaultQNamePrefix + val;
                    }
                    return 'luomus:' + val;
                }
                return val;
            }
            for (var qname in IdService.map) {
                if (IdService.map.hasOwnProperty(qname)) {
                    var uri = IdService.map[qname];
                    val = val.replace(uri, qname);
                }
            }
            return val;
        };
        IdService.getUri = function (val, generated) {
            if (generated === void 0) { generated = false; }
            if (val.indexOf('http:') === -1) {
                if (val.indexOf(':') === -1) {
                    if (generated) {
                        val = IdService.defaultQNamePrefix + val;
                    }
                    else {
                        val = 'luomus:' + val;
                    }
                }
                for (var qname in IdService.map) {
                    if (IdService.map.hasOwnProperty(qname)) {
                        var uri = IdService.map[qname];
                        val = val.replace(qname, uri);
                    }
                }
            }
            return val;
        };
        IdService.setDefaultQNamePrefix = function (val) {
            this.defaultQNamePrefix = val;
        };
        IdService.defaultQNamePrefix = '';
        return IdService;
    }());
    Kotka.IdService = IdService;
})(Kotka || (Kotka = {}));
//# sourceMappingURL=kotka.js.map