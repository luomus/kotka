$(document.body).on('requestRefresh', function () {
    $('.add-prefix-autocomplete').removeClass('add-prefix-autocomplete').textcomplete([
        {
            words: null,
            match: /\w*$/,
            index: 0,
            search: function (term, callback) {
                term = term.toLowerCase();
                if (this.words === null) {
                    this.words = this.$el.data('prefixes').split('|');
                }
                callback($.map(this.words, function (word) {
                    return word.toLowerCase().indexOf(term) === 0 ? word : null;
                }));
            },
            replace: function (value) {
                return value + ':';
            },
            context: function(text) {
                var match = text.match(/:/g);
                return !match;
            }
        }
    ], {zIndex: 100000}).bind('focus', function() {
        var element = this;
        setTimeout(function () {
            $(element).textcomplete('trigger');
        }, 100);
    });
});