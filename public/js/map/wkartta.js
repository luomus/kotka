(function (L) {

    var DEFAULT_LOCALE = 'fi';
    var DEFAULT_GEOLOCATE_ZOOM = 10;
    var WORLD_BOUNDS = [
        [-90.0, -180.0],
        [90.0, 180.0]
    ];
    var DEFAULT_FIN_BOUNDS = [
        [59.445, 20.127],
        [70.021, 30.322]
    ];
    var FIN_POLYGON = [
        [59.545,20.720],
        [60.185,19.292],
        [60.609,19.160],
        [60.813,20.479],
        [61.711,21.138],
        [63.204,20.435],
        [63.568,20.984],
        [63.724,22.039],
        [64.746,23.994],
        [65.740,24.258],
        [66.461,23.708],
        [66.740,23.928],
        [66.814,24.038],
        [67.178,23.566],
        [67.302,23.763],
        [67.458,23.818],
        [67.509,23.445],
        [67.601,23.577],
        [67.867,23.511],
        [67.942,23.665],
        [68.155,23.291],
        [68.334,23.027],
        [68.488,22.302],
        [68.520,22.061],
        [68.903,21.006],
        [69.029,20.819],
        [69.069,20.599],
        [69.112,20.709],
        [69.037,21.006],
        [69.096,21.127],
        [69.202,20.995],
        [69.307,21.281],
        [69.272,21.621],
        [68.828,22.302],
        [68.704,22.346],
        [68.736,22.522],
        [68.689,22.775],
        [68.685,23.005],
        [68.625,23.148],
        [68.704,23.676],
        [68.824,23.818],
        [68.820,23.994],
        [68.708,24.291],
        [68.657,24.719],
        [68.544,24.851],
        [68.625,25.115],
        [68.816,25.181],
        [68.903,25.510],
        [68.879,25.642],
        [69.014,25.796],
        [69.334,25.774],
        [69.411,25.884],
        [69.565,25.884],
        [69.623,26.005],
        [69.710,25.983],
        [69.862,26.422],
        [69.942,26.488],
        [69.934,26.862],
        [69.911,27.026],
        [69.968,27.301],
        [70.069,27.664],
        [70.062,27.740],
        [70.088,27.938],
        [70.017,27.960],
        [69.919,28.136],
        [69.881,28.323],
        [69.824,28.323],
        [69.691,29.125],
        [69.481,29.312],
        [69.229,28.806],
        [69.112,28.784],
        [69.049,28.916],
        [68.923,28.389],
        [68.887,28.444],
        [68.867,28.806],
        [68.548,28.411],
        [68.192,28.630],
        [68.073,29.323],
        [67.793,29.641],
        [67.672,30.015],
        [67.378,29.630],
        [66.999,29.004],
        [66.870,29.015],
        [66.262,29.685],
        [66.125,29.916],
        [65.694,30.125],
        [65.699,30.004],
        [65.645,29.696],
        [65.568,29.850],
        [65.499,29.718],
        [65.316,29.685],
        [65.284,29.575],
        [65.224,29.619],
        [65.211,29.861],
        [65.109,29.806],
        [65.077,29.619],
        [65.003,29.575],
        [64.928,29.575],
        [64.793,29.718],
        [64.793,30.026],
        [64.624,30.070],
        [64.591,29.971],
        [64.378,30.037],
        [64.307,30.344],
        [64.264,30.377],
        [64.273,30.465],
        [64.206,30.454],
        [64.130,30.531],
        [63.937,30.322],
        [63.831,30.245],
        [63.773,29.938],
        [63.460,30.476],
        [63.357,30.916],
        [63.214,31.212],
        [63.120,31.223],
        [63.000,31.432],
        [62.905,31.542],
        [62.785,31.399],
        [62.654,31.333],
        [62.502,31.157],
        [62.217,30.630],
        [61.825,30.037],
        [61.674,29.817],
        [61.533,29.619],
        [61.502,29.487],
        [61.249,29.158],
        [61.143,28.817],
        [61.053,28.685],
        [60.957,28.652],
        [60.968,28.499],
        [60.668,27.927],
        [60.549,27.762],
        [60.436,27.664],
        [60.338,27.631],
        [60.196,26.708],
        [60.169,26.488],
        [60.059,26.279],
        [60.015,26.027],
        [59.961,25.422],
        [59.950,25.038],
        [59.640,22.687],
        [59.512,21.753]
    ];

    var TILE_LAYERS = {
        mm: {
            name: 'Maanmittauslaitos',
            url: 'http://tiles.kartat.kapsi.fi/peruskartta/{z}/{x}/{y}.jpg'
        },
        osm: {
            name: 'OpenStreetMap',
            url: 'http://a.tile.openstreetmap.org/{z}/{x}/{y}.png'
        }
    };

    var TYPE_MAPPINGS = {
        marker: 'POINT',
        polyline: 'LINESTRING',
        polygon: 'POLYGON',
        rectangle: 'POLYGON'
    };

    var getWKTCoordinates = function (leafletCoordinates) {
        var convert = function (value) {
            return [
                [value.lat, value.lng]
            ];
        };
        // Single coordinates case
        if (leafletCoordinates.lat !== undefined && leafletCoordinates.lng !== undefined) {
            return convert(leafletCoordinates)[0];
        } else {
            // List of coordinates
            var result = [];
            for (var i in leafletCoordinates) {
                result.push(convert(leafletCoordinates[i]));
            }
            return result;
        }
    };

    var getLayerCoordinates = function (layer) {
        return layer.getLatLngs !== undefined ?
            layer.getLatLngs() : layer.getLatLng();
    };

    L.TileLayerSwitch = L.TileLayer.extend({
        includes: L.Mixin.Events,
        options: {
            switchZoomUnder : -1,
            switchZoomAbove: -1,
            switchInside: -1,
            switchOutside: -1,
            switchLayer: null
        },
        setSwitchLayer: function (layer) {
            this.options.switchLayer = layer;
        },
        getSwitchInside: function() {
            return this.options.switchInside;
        },
        getSwitchOutside: function() {
            return this.options.switchOutside;
        },
        getSwitchZoomUnder: function () {
            return this.options.switchZoomUnder;
        },
        getSwitchZoomAbove: function () {
            return this.options.switchZoomAbove;
        },
        getSwitchLayer: function () {
            return this.options.switchLayer;
        }
    });

    L.TileLayerSwitch = function (url, options) {
        return new L.TileLayerSwitch(url, options);
    };

    L.SwitchLayerManager = L.Class.extend({
        _map: null,
        options: {
            baseLayers: null
        },
        initialize: function (map, options) {
            this._map = map;
            L.Util.setOptions(this, options);

            this._map.on({
                'moveend': this._update
            }, this)

        },
        _update: function (e) {
            var zoom = this._map.getZoom();
            var center = this._map.getCenter();

            for (var i in this.options.baseLayers) {
                var switchingTiles = this.options.baseLayers[i];
                var inside = switchingTiles.getSwitchInside();
                var outside = switchingTiles.getSwitchOutside();
                var zoomUnder = switchingTiles.getSwitchZoomUnder();
                var zoomAbove = switchingTiles.getSwitchZoomAbove();
                var switchLayer = switchingTiles.getSwitchLayer();
                var shouldSwitchZoom = true;
                var shouldSwitchPos  = true;
                var shouldSwitchBoth = false;
                // If layer got a switchlayer, and if layer actually displayed
                if (switchLayer && switchingTiles._map != null) {
                    // If layer has zoom instructions
                    if (zoomUnder != -1 || zoomAbove != -1) {
                        if(zoomUnder != -1 && zoom > zoomUnder) {
                            shouldSwitchZoom = false;
                        }
                        if(zoomAbove != -1 && zoom <= zoomAbove) {
                            shouldSwitchZoom = false;
                        }
                        if (inside == -1 && outside == -1 && shouldSwitchZoom) {
                            this._map.removeLayer(switchingTiles);
                            this._map.addLayer(switchLayer, false);
                            return;
                        }
                    }

                    // If layer has position instructions
                    if (inside != -1 || outside != -1) {
                        if (inside != -1 && !this._inside(center, inside)) {
                            shouldSwitchPos = false;
                        }
                        if (outside != -1 && this._inside(center, outside)) {
                            shouldSwitchPos = false;
                        }
                        if (zoomUnder == -1 && zoomAbove == -1 && shouldSwitchPos) {
                            this._map.removeLayer(switchingTiles);
                            this._map.addLayer(switchLayer, false);
                            return;
                        }
                    }

                    // If layer has both instructions
                    if (shouldSwitchPos && shouldSwitchZoom) {
                        shouldSwitchBoth = true;
                    } else if (outside != -1 && zoomUnder != -1 &&
                        (shouldSwitchPos || shouldSwitchZoom)) {
                        shouldSwitchBoth = true;
                    }

                    if (shouldSwitchBoth) {
                        this._map.removeLayer(switchingTiles);
                        this._map.addLayer(switchLayer, false);
                        return;
                    }
                }
            }
        },
        _inside: function(center, vs) {
            // ray-casting algorithm based on
            // http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html

            var x = center.lat, y = center.lng;
            var inside = false;

            for (var i = 0, j = vs.length - 1; i < vs.length; j = i++) {
                var xi = vs[i][0], yi = vs[i][1];
                var xj = vs[j][0], yj = vs[j][1];

                var intersect = ((yi > y) != (yj > y))
                    && (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
                if (intersect) inside = !inside;
            }

            return inside;
        }
    });

    L.WKartta = L.Class.extend({
        _map: null,
        _geoCoder: null,
        _switchmanager: null,
        _drawnItems: null,
        _entries: {},
        includes: L.Mixin.Events,
        options: {
            simple: true,
            single: true,
            annotate: true,
            edit: true,
            mapOptions: {
                scale: true,
                center: [60, 20],
                maxBounds: null,
                maxZoom: 17,
                minZoom: null,
                zoom: 8
            },
            locationTypes: {
                lineString: true,
                point: true,
                polygon: true,
                rectangle: true
            },
            mapTypes: ['mm','osm']
        },
        initialize: function(id, options, geoCoder) {
            L.Util.setOptions(this, options);
            var mapOptions = this._mapOptions(this.options);
            this._map = new L.Map(id, mapOptions);
            this._geoCoder = geoCoder;
            var isTouch = L.Browser.touch;

            //var controller = new this.Controller(View, options);
            this._drawnItems = L.featureGroup();

            this._initMap();
        },
        getLMap: function() {
            return this._map;
        },
        _mapOptions: function(options) {
            var mapOptions = L.extend({}, options.mapOptions);
            mapOptions.center = new L.LatLng(mapOptions.center[0], mapOptions.center[1]);
            mapOptions.maxBounds = mapOptions.maxBounds ? mapOptions.maxBounds : WORLD_BOUNDS;
            return mapOptions;
        },
        getCenter: function() {
            return this._map.getCenter();
        },
        reCenter: function (latLng, zoomLevel) {
            var map = this._map;
            var drawnItems = this._drawnItems;
            if (latLng) {
                map.setView(latLng, zoomLevel);
            } else {
                // If markers present fit them into the view
                if (drawnItems.getLayers().length > 0) {
                    map.fitBounds(drawnItems.getBounds());
                    // Zoom out to accomodate single markers, extremely small
                    // polygons etc.
                    if (map.getZoom() > this.options.mapOptions.zoom) {
                        // map.setZoom doesn't work
                        map.setView(map.getCenter(), this.options.mapOptions.zoom);
                    }
                } else {
                    map.fitBounds(DEFAULT_FIN_BOUNDS);
                }
            }
        },
        notify: function(msg) {
            var $msgElem = $('#message');
            $msgElem.text(msg);
            $msgElem.fadeIn(500);
            setTimeout(function () {
                $msgElem.fadeOut(500);
            }, 1000 + 100 * msg.length);
        },
        geocode: function(query, zoomLevel) {
            this._geoCoder.geocode({address: query},
                function (results, status) {
                    if (status === 'OK') {
                        var center = [results[0].geometry.location.lat(),
                            results[0].geometry.location.lng()];
                        this._map.setView(center, zoomLevel || DEFAULT_GEOLOCATE_ZOOM);
                        this.notify(results[0].formatted_address);
                    } else if (status === 'ZERO_RESULTS') {
                        this.notify('Ei osumia');
                    } else {
                        throw 'Error while geocoding: ' + status;
                    }
                });
        },
        importData: function(data) {
            if (!L.Util.isArray(data)) {
                throw "Data wasn't an array";
            }
            this.clearMarkers();
            var entries = {};
            this._entries = entries;
            for(var i in data) {
                var item = data[i];
                var layer;
                switch (item.locationType) {
                    case 'LINESTRING':
                        layer = new L.Polyline(item.location);
                        layer.layerType = 'polyline';
                        break;
                    case 'POINT':
                        layer = new L.Marker(item.location);
                        layer.layerType = 'marker';
                        break;
                    case 'POLYGON':
                        layer = new L.Polygon(item.location);
                        layer.layerType = 'polygon';
                        break;
                    default:
                        throw ('Unknown location type or not implemented yet: ' + data.locationType);
                }
                this._addLayer(layer);
                entries[layer._leaflet_id] = item;
            }
            this.reCenter();
        },
        clearMarkers: function() {
            var drawnItems = this._drawnItems;
            this._entries = {};
            drawnItems.clearLayers();
        },
        hasMarkers: function() {
            return (Object.keys(this._entries).length > 0);
        },
        _initMap: function() {
            var map = this._map;
            var options = this.options;
            map.setMaxBounds(WORLD_BOUNDS);
            map.addLayer(this._drawnItems);
            if (options.mapTypes === 'auto') {
                var osmLayer = new L.TileLayerSwitch(TILE_LAYERS.osm.url,
                    {   attribution: TILE_LAYERS.osm.name,
                        switchZoomAbove: 6,
                        switchInside: FIN_POLYGON  });
                var mmLayer = new L.TileLayerSwitch(TILE_LAYERS.mm.url,
                    {   attribution: TILE_LAYERS.mm.name,
                        switchZoomUnder: 6,
                        switchOutside: FIN_POLYGON });
                osmLayer.setSwitchLayer(mmLayer);
                mmLayer.setSwitchLayer(osmLayer);
                var baseLayers ={
                    'osm': osmLayer,
                    'mm': mmLayer
                };
                this._switchmanager = new L.SwitchLayerManager(map, {baseLayers: baseLayers});
                osmLayer.addTo(map);

            } else if (L.Util.isArray(options.mapTypes)) {
                var getLayer = function (key) {
                    if (TILE_LAYERS[key] === undefined) {
                        throw ('unkown layer type ' + key);
                    }
                    return TILE_LAYERS[key];
                };

                // Add the first map type as default tilelayer
                //new L.TileLayer(getLayer(this.options.mapTypes[0]).url).addTo(map);

                // If more than one map type, add a layer choosing control
                if (this.options.mapTypes.length > 1) {
                    var layers = {};
                    for (var index in options.mapTypes) {
                        var layerValues = getLayer(options.mapTypes[index]);
                        layers[layerValues.name] =
                            new L.TileLayer(layerValues.url,
                                {attribution: layerValues.name});
                    }
                    layers[layerValues.name].addTo(map);
                    L.control.layers(layers).addTo(map);
                }
            }
            if (this.options.mapOptions.scale) {
                (new L.Control.Scale({imperial: false})).addTo(map);
            }
            if (this.options.edit) {
                map.on('click', this._addHadler, this);
            }
        },
        _addHadler: function(e) {
            var drawnItems = this._drawnItems;
            if (this.options.single) {
                drawnItems.clearLayers();
                this._entries = {};
            }
            var layer = L.marker(e.latlng, {
                draggable: true
            });
            layer.layerType = 'marker';
            this._addLayer(layer);
            this._entries[layer._leaflet_id] = {
                locationType: TYPE_MAPPINGS[layer.layerType],
                location: getWKTCoordinates(getLayerCoordinates(layer)),
                notes: ''
            };

            this._fireMapDataChange();
        },
        _moveHandler: function(e) {
            var layer = e.target;
            this._entries[layer._leaflet_id].location = getWKTCoordinates(getLayerCoordinates(layer));
            this._fireMapDataChange();
        },
        _deleteHander: function(e) {
            var layer = e.target;
            var drawnItems = this._drawnItems;
            drawnItems.removeLayer(layer);
            delete this._entries[layer._leaflet_id];
            this._fireMapDataChange();
        },
        _fireMapDataChange: function() {
            this.fire('change', {'data': this._exportData()});
        },
        _addLayer: function (layer) {
            var drawnItems = this._drawnItems;
            if (layer.layerType === 'marker') {
                layer.on('dragend', this._moveHandler, this);
                if (this.options.annotate) {
                    layer.bindPopup('');
                }
            }
            layer.on('dblclick', this._deleteHander, this);
            drawnItems.addLayer(layer);
        },
        _exportData: function() {
            var entries = this._entries;
            var result  = [];
            for (var i in entries) {
                result.push(entries[i]);
            }
            return result;
        }
    });

    L.wkartta = function(id, options, geocodes) {
        return new L.WKartta(id, options, geocodes);
    };

})(L);


