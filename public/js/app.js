var hasHiddenCopy = false;
function disableForm(all) {
    if (typeof all == 'undefined') {
        all = false;
    }
    var selector = all ? ":input" : ":input:not('.unable-disable')";
    $(selector).each(function() {
            $(this).prop("disabled", true);
            if (!$(this).is(":submit") && !hasHiddenCopy) {
                createNonDisabledHiddenCopy(this);
            }
        }
    );
    hasHiddenCopy = true;
}
function createNonDisabledHiddenCopy(inputElement) {
    $("<input>").attr({
        type: "hidden",
        name: $(inputElement).attr("name"),
        value: $(inputElement).val()
    }).appendTo("#MYDocument");
}

(function(window, $) {
    window.fmpwizard = {
        views: {
            DropDownMenus: function(){
                var self = this;
                self.initDropDown = function(selector, url, allowNew){
                    if ($(selector).hasClass('select2-offscreen')) {
                        //if we already init'ed this element, exit.
                        return;
                    }
                    $(selector).select2({
                        allowClear: true,
                        minimumInputLength: 1,
                        multiple:true,
                        "ajax": {
                            data: function (term, page) {
                                return { term:term, page:page };
                            },
                            dataType:"json",
                            quietMillis:100,
                            results: function (data, page) {
                                return {results: data};
                            },
                            "url": url
                        },
                        id: function(object) {
                            return object.text;
                        },
                        initSelection: function (element, callback) {
                            var data = {id: element.val(), text: element.val()};
                            callback(data);
                        },
                        //Allow manually entered text in drop down.
                        createSearchChoice: function(term, data) {
                            if (!allowNew) {
                                return false;
                            }
                            if ( $(data).filter( function() {
                                return this.text.localeCompare(term)===0;
                            }).length===0) {
                                return {id: term, text: term};
                            }
                        }
                    });
                }
            }
        }
    };
})(this, jQuery);