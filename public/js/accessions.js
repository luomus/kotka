/// <reference path="./typings/jquery/jquery.d.ts" />
/// <reference path="./typings/bootstrap/bootstrap.d.ts" />
/// <reference path="./typings/ladda/ladda.d.ts" />
/// <reference path="./kotka.ts" />
var Accessions;
(function (Accessions) {
    var URLS = /** @class */ (function () {
        function URLS() {
        }
        URLS.branchForm = '/accessions/branchForm';
        URLS.eventForm = '/accessions/eventForm';
        URLS.branch = '/accessions/branch';
        URLS.event = '/accessions/event';
        URLS.view = '/view';
        return URLS;
    }());
    var HttpClient = /** @class */ (function () {
        function HttpClient() {
        }
        HttpClient.setCache = function (url, data) {
            HttpClient.cache[url] = data;
        };
        HttpClient.get = function (url, callback, useCache) {
            if (useCache === void 0) { useCache = false; }
            if (useCache && HttpClient.cache[url]) {
                callback(HttpClient.cache[url]);
                return;
            }
            $.get(url, function (data) {
                if (useCache) {
                    HttpClient.setCache(url, data);
                }
                callback(data);
            }).fail(function () {
                Kotka.Util.errorDialog('Error loading data', 'Could not load wanted data. Please try again little later');
            });
        };
        HttpClient.post = function (url, data, callback) {
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data) {
                    callback(data);
                },
                error: function (jqXHR) {
                    Ladda.stopAll();
                    Kotka.Util.errorDialog(jqXHR.status === 403 ? 'Insufficient rights' : 'Failed to delete data', jqXHR.status === 403 ? 'You do not have rights for the accession and that\'s why you cannot do this' : 'Wait for a while and try again. If still failing contact the kotka support.');
                }
            });
        };
        HttpClient.delete = function (url, callback) {
            $.ajax({
                type: "DELETE",
                url: url,
                success: function (data) {
                    callback(data);
                },
                error: function (jqXHR) {
                    Ladda.stopAll();
                    Kotka.Util.errorDialog(jqXHR.status === 403 ? 'Insufficient rights' : 'Failed to delete data', jqXHR.status === 403 ? 'You do not have rights for the accession and that\'s why you cannot do this' : 'Wait for a while and try again. If still failing contact the kotka support.');
                }
            });
        };
        HttpClient.cache = {};
        return HttpClient;
    }());
    var Form = /** @class */ (function () {
        function Form(formSource, element, parent, qname) {
            if (qname === void 0) { qname = null; }
            this.visible = false;
            this.loaded = false;
            this.formSource = formSource;
            this.parent = parent;
            this.element = element;
            this.qname = qname;
        }
        Form.prototype.getElement = function () {
            return this.element;
        };
        Form.prototype.getQName = function () {
            return this.qname;
        };
        Form.prototype.delete = function (callback) {
            var _this = this;
            var ladda = Ladda.create(this.getFormElement().find('.save-form')[0]);
            ladda.start();
            HttpClient.delete(this.formSource, function (data) {
                var formContainer = $(data).find('.form-data');
                ladda.stop();
                if (formContainer.data('success')) {
                    _this.qname = formContainer.data('form-id');
                    _this.originalValue = formContainer.find('form').serialize();
                    HttpClient.setCache(_this.formSource, data);
                    _this.getPopoverElement().data('bs.popover').options.content = data;
                    $.growl.notice({ title: 'Successfully', message: 'deleted' });
                    _this.show();
                    callback(_this);
                }
                else {
                    _this.getFormElement().html(data);
                    Service.container.trigger('refresh');
                }
            });
        };
        Form.prototype.save = function (callback) {
            var _this = this;
            var formData = this.getFormElement().serialize();
            var ladda = Ladda.create(this.getFormElement().find('.save-form')[0]);
            ladda.start();
            HttpClient.post(this.formSource, formData, function (data) {
                var formContainer = $(data).find('.form-data');
                ladda.stop();
                if (formContainer.data('success')) {
                    _this.qname = formContainer.data('form-id');
                    _this.originalValue = formContainer.find('form').serialize();
                    HttpClient.setCache(_this.formSource, data);
                    _this.getPopoverElement().data('bs.popover').options.content = data;
                    $.growl.notice({ title: 'Successfully', message: 'saved' });
                    _this.show();
                    callback(_this);
                }
                else {
                    _this.getFormElement().html(data);
                    Service.container.trigger('refresh');
                }
            });
        };
        Form.prototype.show = function () {
            var _this = this;
            if (!this.element.is(':visible')) {
                this.element.show();
            }
            this.getForm(function () {
                _this.visible = true;
                _this.getPopoverElement().popover('show');
            });
        };
        Form.prototype.getFormElement = function () {
            var popoverID = this.getPopoverElement().attr('aria-describedby');
            return $('#' + popoverID).find('form');
        };
        Form.prototype.getPopoverElement = function () {
            if (this.element.hasClass('popover-elem')) {
                return this.element;
            }
            return this.element.find('.popover-elem:first');
        };
        Form.prototype.getForm = function (callback) {
            var _this = this;
            if (!this.loaded) {
                HttpClient.get(this.formSource, function (data) {
                    _this.init($(data));
                    _this.loaded = true;
                    callback();
                }, true);
            }
            else {
                callback();
            }
        };
        Form.prototype.init = function (html) {
            var parentInput = html.find('.parent-id input');
            if (parentInput.length > 0) {
                parentInput.val(this.parent);
            }
            this.originalValue = html.find('form').serialize();
            this.getPopoverElement().popover({
                animation: false,
                content: html.html(),
                html: true,
                trigger: 'manual',
                placement: 'bottom'
            });
        };
        Form.prototype.hide = function (force) {
            if (force === void 0) { force = false; }
            if (!force && this.visible && this.originalValue !== this.getFormElement().serialize()
                && !confirm("Are you sure you want discard the changes you've made")) {
                return;
            }
            this.visible = false;
            if (this.loaded) {
                this.getPopoverElement().popover('hide');
            }
            Service.container.find('.dummy').hide();
        };
        Form.prototype.isVisible = function () {
            return this.visible;
        };
        Form.prototype.isBranch = function () {
            return (this.element.find('.branch').length > 0 || this.element.hasClass('branch'));
        };
        Form.prototype.isDummy = function () {
            return this.element.hasClass('dummy');
        };
        return Form;
    }());
    var FormContainer = /** @class */ (function () {
        function FormContainer() {
            this.forms = [];
            this.active = '-1';
            this.idx = -1;
        }
        FormContainer.prototype.addEvent = function (parent, element) {
            if (this.getActiveForm() instanceof Form) {
                this.getActiveForm().hide();
            }
            if (!this.forms[parent]) {
                this.forms[parent] = new Form(URLS.eventForm, element, parent);
            }
            this.active = parent;
            this.showActive();
        };
        FormContainer.prototype.addBranch = function (root, element) {
            if (this.getActiveForm() instanceof Form) {
                this.getActiveForm().hide();
            }
            if (!this.forms['-1']) {
                this.forms['-1'] = new Form(URLS.branchForm, element, root);
            }
            this.active = '-1';
            this.showActive();
        };
        FormContainer.prototype.editBranch = function (root, qname, element) {
            if (this.getActiveForm() instanceof Form) {
                this.getActiveForm().hide();
            }
            for (var i in this.forms) {
                if (this.forms[i].getQName() === qname) {
                    this.active = i;
                    this.showActive();
                    return;
                }
            }
            var idx = ++this.idx;
            var url = URLS.branchForm + '?uri=' + qname;
            this.forms[idx] = new Form(url, element, root);
            this.active = '' + idx;
            this.showActive();
        };
        FormContainer.prototype.editEvent = function (parent, qname, element) {
            if (this.getActiveForm() instanceof Form) {
                this.getActiveForm().hide();
            }
            for (var i in this.forms) {
                if (this.forms[i].getQName() === qname) {
                    this.active = i;
                    this.showActive();
                    return;
                }
            }
            var idx = ++this.idx;
            var url = URLS.eventForm + '?uri=' + qname;
            this.forms[idx] = new Form(url, element, parent);
            this.active = '' + idx;
            this.showActive();
        };
        FormContainer.prototype.getActiveForm = function () {
            if (!this.forms[this.active]) {
                return null;
            }
            return this.forms[this.active];
        };
        FormContainer.prototype.showActive = function () {
            this.getActiveForm().show();
        };
        return FormContainer;
    }());
    var Service = /** @class */ (function () {
        function Service(container, root) {
            Service.container = container;
            this.root = root;
            this.formContainer = new FormContainer();
        }
        Service.prototype.getDataElement = function (element) {
            var dataElem = element.closest('.elem-data');
            if (dataElem.length !== 1) {
                Kotka.Util.errorDialog('Error loading element data', 'Could not find element that holds the data. Please contact Kotka support');
                return null;
            }
            return dataElem;
        };
        Service.prototype.init = function () {
            var _this = this;
            // init depleted switch
            $('.depleted-switch')
                .bootstrapSwitch({
                    onText: 'show',
                    offText: 'hide'
                })
                .on('switchChange.bootstrapSwitch', function (event, state) {
                    if (state) {
                        Service.showDepleted();
                    }
                    else {
                        Service.hideDepleted();
                    }
                });
            // init listeners handling
            Service.container
                .on('click', '.show-accession', function () {
                    var $modal = $('#modal');
                    var $modalBody = $('#modal .modal-body');
                    var url = '/view?uri=' + Util.getRoot() + '&format=noLayout';
                    $modalBody.html('<img src="/img/ajax-loader.gif" />');
                    $.get(url, function (html) {
                        $modalBody.html(html);
                    }).fail(function () {
                        $modalBody.html('<h1>Not found</h1>');
                    });
                    $modal.modal('show');
                    $modal.show();
                })
                .on('click', '.add-branch', function (e) {
                    e.preventDefault();
                    var dataElem = _this.getDataElement($(e.target));
                    var target = $(dataElem.data('branch-elem'));
                    target.find('.remove-container').hide();
                    _this.formContainer.addBranch(_this.root, target);
                })
                .on('click', '.edit-branch', function (e) {
                    e.preventDefault();
                    var dataElem = _this.getDataElement($(e.target));
                    _this.formContainer.editBranch(_this.root, dataElem.data('elem-id'), dataElem);
                })
                .on('click', '.add-event', function (e) {
                    e.preventDefault();
                    var dataElem = _this.getDataElement($(e.target));
                    var target = dataElem.find('.event.dummy');
                    if (!dataElem.find('.events').is(':visible')) {
                        dataElem.find('.branch').click();
                    }
                    _this.formContainer.addEvent(dataElem.data('elem-id'), target);
                })
                .on('click', '.edit-event', function (e) {
                    e.preventDefault();
                    var dataElem = _this.getDataElement($(e.target));
                    var parentDataElem = _this.getDataElement(dataElem.parent());
                    _this.formContainer.editEvent(parentDataElem.data('elem-id'), dataElem.data('elem-id'), dataElem);
                })
                .on('click', '.close-form', function (e) {
                    e.preventDefault();
                    _this.formContainer.getActiveForm().hide();
                })
                .on('click', '.delete-form', function (e) {
                    e.preventDefault();
                    if (confirm('Are you sure you want to delete this?')) {
                        _this.formContainer.getActiveForm().delete(function (form) {
                            form.hide(true);
                            if (form.isBranch()) {
                                var branch = form.getElement().find('.branch').parent();
                                branch.remove();
                            }
                            else {
                                var event_1 = form.getElement();
                                var branch = event_1.parent().closest('.elem-data').find('.branch');
                                event_1.find('.event').parent().remove();
                                Branch.refresh(branch);
                            }
                        });
                    }
                })
                .on('click', '.save-form', function (e) {
                    e.preventDefault();
                    _this.formContainer.getActiveForm().save(function (form) {
                        if (form.isDummy()) {
                            // TODO: add without refresh
                            location.reload();
                            return;
                        }
                        if (form.isBranch()) {
                            Branch.getBranch(form.getQName(), function (elem) {
                                var branch = form.getElement().find('.branch');
                                var classes = elem.attr('class');
                                branch.html(elem.html());
                                branch.attr('class', classes);
                                Branch.refresh(branch);
                            });
                        }
                        else {
                            Event.getEvent(form.getQName(), function (elem) {
                                var event = form.getElement();
                                event.find('.event').html(elem.find('.event').html());
                                event.attr('data-sorting', elem.attr('data-sorting'));
                                event.attr('data-amount', elem.attr('data-amount'));
                                event.attr('data-element-id', elem.attr('data-element-id'));
                                var branch = event.parent().closest('.elem-data').find('.branch');
                                Branch.refresh(branch);
                                form.show();
                            });
                        }
                    });
                })
                .on('click', '.branch', function (e) {
                    var target = $(e.target);
                    if (target.hasClass('no-event-action')) {
                        return;
                    }
                    var events = _this.getDataElement(target).find('.events');
                    if (events.is(':visible')) {
                        events.slideUp();
                    }
                    else {
                        events.slideDown();
                    }
                })
                .on('refresh', function () {
                    Service.container.trigger('shown.bs.popover');
                })
                .on('shown.bs.popover', function (e) {
                    Service.container.find('.add-datetimepicker')
                        .removeClass('add-datetimepicker')
                        .datetimepicker({
                            widgetPositioning: {
                                horizontal: 'auto',
                                vertical: 'bottom'
                            },
                            keepInvalid: true,
                            icons: {
                                time: "fa fa-clock-o",
                                date: "fa fa-calendar",
                                up: "fa fa-arrow-up",
                                down: "fa fa-arrow-down",
                                previous: "fa fa-angle-left",
                                next: "fa fa-angle-right"
                            }
                        })
                        .end()
                        .find('.add-select2')
                        .removeClass('add-select2')
                        .each(function (index, elem) {
                            $(elem).select2({
                                placeholder: '',
                                allowClear: true
                            })
                        })
                        .end()
                        .find('.event-type-listener')
                        .removeClass('event-type-listener')
                        .each(function (index, eventTypeElem) {
                            var $eventType = $(eventTypeElem).find('.PUUEventType');
                            $eventType.on('change', function () {
                                var targetValue = $eventType.val();
                                $eventType
                                    .closest('.form-data')
                                    .find('[data-type]')
                                    .each(function (index, elem) {
                                        var $elem = $(elem);
                                        var shown = JSON.parse($elem.data('type').replace(/'/g, '"'));
                                        if (shown.indexOf(targetValue) === -1) {
                                            $elem.hide();
                                        }
                                        else {
                                            $elem.show();
                                        }
                                    });
                            });
                            $eventType.trigger('change');
                        })
                        .end()
                        .find('.add-yes-no')
                        .removeClass('add-yes-no')
                        .find("[type='checkbox']")
                        .bootstrapSwitch({
                            onText: 'Yes',
                            offText: 'No'
                        });
                });
            Service.container.find('.branch').not('.dummy').each(function (key, value) {
                Branch.refresh($(value));
            });
        };
        Service.showDepleted = function () {
            $('.depleted').closest('.elem-data').removeClass('hidden-branch');
        };
        Service.hideDepleted = function () {
            $('.depleted').closest('.elem-data').addClass('hidden-branch');
        };
        return Service;
    }());
    Accessions.Service = Service;
    var Event = /** @class */ (function () {
        function Event() {
        }
        Event.getEvent = function (qname, callback) {
            var url = URLS.event + '?uri=' + qname;
            HttpClient.get(url, function (data) {
                callback($(data));
            });
        };
        return Event;
    }());
    var Branch = /** @class */ (function () {
        function Branch() {
        }
        Branch.getBranch = function (qname, callback, fullBranch) {
            if (fullBranch === void 0) { fullBranch = false; }
            var url = URLS.branch + '?uri=' + qname;
            if (fullBranch) {
                url += '&full=true';
            }
            HttpClient.get(url, function (data) {
                callback($(data));
            });
        };
        Branch.refresh = function (branch) {
            var events = branch.closest('.elem-data').find('.events');
            branch.find('.event-cnt').text(events.find('.event').length);
            events.find('.elem-data').sort(function (a, b) {
                var dateA = parseInt(a.getAttribute('data-sorting'));
                var dateB = parseInt(b.getAttribute('data-sorting'));
                if (dateA === dateB) {
                    var idA = parseInt(a.getAttribute('data-elem-id').replace(/[^\d]/g, ''));
                    var idB = parseInt(b.getAttribute('data-elem-id').replace(/[^\d]/g, ''));
                    return idB - idA;
                }
                else if (dateA < dateB) {
                    return 1;
                }
                return -1;
            }).appendTo(events);
            var amountElem = events.find('.event-amount:first');
            var amount = 0;
            var amountReady = false;
            events.find('.elem-data').each(function (index, elem) {
                if (amountReady) {
                    return;
                }
                var elemCount = '' + elem.getAttribute('data-amount');
                var firstChar = elemCount.substr(0, 1);
                var elemAmount = parseInt(elemCount.replace(/[^\d]/g, ''));
                if (!isNaN(elemAmount) && typeof elemAmount === 'number') {
                    if (firstChar === '-') {
                        amount -= elemAmount;
                    }
                    else if (firstChar === '+') {
                        amount += elemAmount;
                    }
                    else {
                        amount += elemAmount;
                        amountReady = true;
                    }
                }
                else if (elemCount) {
                    amount = elemCount;
                    amountReady = true;
                }
            });
            if (amount <= 0 && !branch.hasClass('depleted')) {
                amount = amount + ' <strong style="color: #faebcc"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></strong>';
            }
            branch.find('.branch-amount').html('' + amount);
        };
        return Branch;
    }());
    var Util = /** @class */ (function () {
        function Util() {
        }
        Util.getRoot = function () {
            return $('#root').data('elem-id');
        };
        return Util;
    }());
})(Accessions || (Accessions = {}));
