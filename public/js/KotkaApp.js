(function (window, localStorage, $) {
    function KotkaApp() {
        this.init = [];
        this.user = {};
        this.rnd  = Math.random();
        this.localStorageSupported = typeof window.Storage !== "undefined";
        this.remember = {};
        this.prefix = '';
        this.pleaseWaitDiv = $('' +
            '<div class="modal" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">' +
                '<div class="modal-dialog" style="width:330px;">' +
                    '<div class="modal-content">' +
                        '<div class="modal-header">' +
                            '<h2 class="modal-title">Modal title</h2>' +
                        '</div>' +
                        '<div class="modal-body">' +
                            '<div class="progress">' +
                                '<div class="progress-bar progress-bar-striped active"  role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 100%">' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</div>');
        return this;
    }

    KotkaApp.prototype = (function() {

        function log(msg) {
            if (window.console) {
                window.console.log(msg);
            }
        }

        function addUserInfo() {
            var self = this;
            $('.user-username').removeClass('user-username').each(function() {
                    $(this).append(self.user.username).fadeIn();
            });
            if (this.localStorageSupported) {
                if (typeof self.user !== 'undefined' && this.prefix != self.user.username) {
                    localStorage.setItem('prefix', self.user.username);
                }
            }
        }

        function initRemember() {
            if (!this.localStorageSupported) {
                return false;
            }
            try {
                this.prefix = localStorage.getItem('prefix') + '-';
                var key     = this.prefix + 'remember';
                var result  = JSON.parse(getFromLocalStorage(key));
                if (result !== null) {
                    this.remember = result;
                }
            } catch (e) {
            }
        }

        function alertMessage(msg, show) {
            if (typeof show == 'undefined') {
                show = false;
            }
            if (show) {
                //TODO: add good looking alert dialog
                alert(msg)
            }
            log(msg);
        }

        function getFromLocalStorage(key) {
            try {
                return localStorage.getItem(key);
            } catch (e) {
                return false;
            }
        }

        function addToLocalStorage(key, value) {
            try {
                key = this.prefix + key;
                if (!this.localStorageSupported) {
                    return false;
                }
                localStorage.setItem(key, value);
            } catch (e) {
                return false;
            }
            return true;
        }

        return {
            constructor: KotkaApp,
            start: function (init) {
                var initilizer, _i, _len, _ref;
                var self = this;
                if (init == null) {
                    init = [];
                }
                if (init.length > 0) {
                    $.merge(this.init, init);
                }
                if (window.location.protocol == 'https:') {
                    $.get('/js/data/init.json?t=' + self.rnd, function(data) {
                        self.user = data.user;
                        self._(addUserInfo)();
                    }).fail(function() {
                        self._(alertMessage)('Failed to load initial data');
                    });
                }
                self._(initRemember)();
                _ref = this.init;
                _len = _ref.length;
                for (_i = 0; _i < _len; _i++) {
                    initilizer = _ref[_i];
                    $.proxy(initilizer, this)();
                }
                $('.help').tooltip();
                $('.form-help').tooltip();
            },
            getRemember: function (option) {
                if (typeof this.remember[option] == 'undefined') {
                    return false;
                } else {
                    return this.remember[option];
                }
            },
            setRemember: function(key, value) {
                this.remember[key] = value;
                this._(addToLocalStorage)('remember', JSON.stringify(this.remember));
            },
            showPleaseWait: function(title) {
                title = title || 'Processing...';
                this.pleaseWaitDiv.find('.modal-title').text(title);
                this.pleaseWaitDiv.modal();
            },
            hidePleaseWait: function() {
                this.pleaseWaitDiv.modal('hide');
            },
            _:function(callback){
                var self = this;
                return function(){
                    return callback.apply(self, arguments);
                };
            }
        }
    })();

    window.KotkaApp = new KotkaApp();

})(window, localStorage, $);