var $status         = $('#status');
var $form           = $('#HRATransaction');
var $markingToggle  = $('#item-toggle');

var specimens = ['HRBAway', 'HRBReturned', 'HRBMissing', 'HRBDamaged'];


$form.on('requestRefresh', function () {
    $form.find('.add-datetimepicker')
        .removeClass('add-datetimepicker')
        .datetimepicker({
            widgetPositioning: {
                vertical: 'bottom'
            },
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down",
                previous: "fa fa-angle-left",
                next: "fa fa-angle-right"
            }
        });
});

$('input[type="radio"]:checked').each(function() {
    var $this = $(this);
    $this.closest('label').addClass('active');
});

$.each(specimens, function (index, value) {
    var $source = $('.' + value);
    var $target = $('#' + value + ' .items');
    var list    = $source.val().split(', ');
    if (typeof list === 'undefined' || list.length == 0) {
        return;
    }
    list.sort();
    for (var i in list) {
        var item = list[i];
        if (item.length > 0) {
            add_line($target, item);
        }
    }
});


function mark_as_returned() {
    var $selects = $('#HRBAway .wrap');
    var $target = $('#HRBReturned .items');
    $selects.each(function () {
        $(this).appendTo($target);
    });
    toggle_items();
    return false;
}

function add_specimen() {
    var $range  = $('#id-range');
    var speRange = $range.val();
    if (speRange != '') {
        if (/^([A-Z0-9]+\.){0,1}[0-9]+-[0-9]+$/g.test(speRange)) {
            var $ajax = $('#ajax-loader');
            $ajax.show();
            $.get('/api/range/' + speRange, function (data) {
                $ajax.hide();
                if (data.status === 'ok') {
                    add_rows(data.items);
                    make_links();
                } else {
                    alert(data.status);
                }
            });
        } else {
            alert('Incorrect range format')
        }
        $range.val('');
    }
    var $ids = $('#ids');
    var speList = $ids.val().replace(/,/g, '\n').split('\n');
    add_rows(speList);
    make_links();
    $ids.val('');
}

function add_rows(speArray) {
    if (typeof speArray === 'undefined' || speArray.length == 0) {
        return;
    }
    var specimen;
    var $target = $('#HRBAway .items');
    var isObject = typeof speArray[0]['uri'] !== 'undefined';
    for (var i in speArray) {
        if (isObject) {
            specimen = speArray[i]['uri'];
        } else {
            specimen = $.trim(speArray[i]);
        }
        if (!specimen) {
            continue;
        }
        if (/^H[0-9]{7,8}$/.test(specimen)) {
            specimen = 'HA.' + specimen;
        }
        add_line($target, specimen);
    }
    toggle_items();
}

function add_line($target, item) {
    $target.append("<div class=\"wrap\"> " +
        "<div class=\"specimen add-uri\">" + item + "</div>" +
        ",</div");
}

function toggle_items() {
    var cnt = 0;
    var hide_on_away_empty = false;
    $.each(specimens, function (index, value) {
        var $field = $('#' + value);
        if ($field.has('.specimen').length) {
            $field.show();
        } else {
            cnt++;
            $field.hide();
            if (value == 'HRBAway') {
                hide_on_away_empty = true;
            }
        }
    });
    if (cnt == specimens.length) {
        $('.hide-on-empty').hide();
        $('#' + specimens[0]).show();
    } else {
        $('.hide-on-empty').show();
        if (hide_on_away_empty) {
            $('.hide-on-away-hidden').hide();
        }
    }
}
toggle_items();

function make_links() {
    var $labels = $('.add-uri');
    $labels.each(function(){
        var $this = $(this);
        $this.removeClass('add-uri');
        var uri = $.trim($this.text());
        if (idRegExp.test(uri) || uriRegExp.test(uri)) {
            $this.html('<a class="modal-link" href="/view?uri=' + uri + '">' +  uri + '</a>');
        }
    })
}
make_links();

var $modal = $('#modal');

var marking = false;
$markingToggle.on('click', 'a', function(e) {
    e.preventDefault();
    var $this = $(this);
    var isActive = $this.is('.active');
    $markingToggle.find('.active').removeClass('active');
    if (isActive) {
        $form.removeClass('marking');
        marking = false;
        return;
    }
    $this.addClass('active');
    $form.addClass('marking');
    marking = $($this.data('mark'));
});

$form.on('click', '.wrap', function (e) {
    e.preventDefault();
    var $this = $(this);
    if (marking === false) {
        var $link = $this.find('.modal-link');
        if ($link.length == 0) {
            return;
        }
        var $modalBody = $('#modal .modal-body');
        var url = $link.attr('href') + '&format=noLayout';
        $modalBody.html('<img src="/img/ajax-loader.gif" />');
        $.get(url, function (html) {
            $modalBody.html(html);
            var qnameReq = /([a-z0-9]{1,4}\.[a-z0-9]+)/i;
            //var qname = url.replace('id.luomus','').match(qnameReq)[1];
            //loadImage(qname);
        }).fail(function() {
            $modalBody.html('<h1>Not found</h1>');
        });
        $modal.modal('show');
    }
    $this.appendTo(marking);
    toggle_items();
});

$form.on('click', '.add-tab', function (e) {
    e.preventDefault();
    var elem = this.dataset['target'];
    var placeholder = this.dataset['placeholder'];
    var placement = this.dataset['placement'];
    var type = this.dataset['type'];
    var $container = $("#" + elem );
    var count = parseInt($container.data('count'));
    var name = $container.data('template-name');
    var element = $container.data('template-elem');
    $.get('/form/' + element + '?name=' + name + '&type=' + type, function(template) {
        var regex = new RegExp(placeholder, "g");
        var $template = $(template.replace(regex, count));
        $container.find('.collapse').removeClass('in');
        if (placement === 'top') {
            $container.prepend($template);
        } else {
            $container.append($template);
        }
        $container.data('count', count + 1);
        $form.trigger('requestRefresh');
    });

});

$form.on('submit', function(e) {
    $status.val('preparing');
    var has = false;
    var $text = $('.HRAIds');
    var $range = $('#id-range');
    if ($.trim($text.val())) {
        has = true;
        $text.parent().addClass('has-error');
    } else {
        $text.parent().removeClass('has-error');
    }
    if ($.trim($range.val())) {
        has = true;
        $range.parent().addClass('has-error');
    } else {
        $range.parent().removeClass('has-error');
    }
    if (has) {
        e.preventDefault();
        setTimeout(function() {
            alert('Please click Add first, or empty fields marked with red.');
            $('.ladda-button').removeAttr('disabled').removeAttr('data-loading');
        },100);
        return false;
    }
    $.each(specimens, function (index, value) {
        var $target = $('.' + value);
        var $source = $('#' + value + ' .items .specimen');
        var list    = [];
        $source.each(function() {
            list.push($(this).text());
        });
        list.sort();
        $target.val(list.join(', '));
    });
    $status.val('ok');
});

init.push(function() {
    //var $other      = $('#other-numbers');
    //var $main       = $('#main');
    var $specimens  = $('#specimens');
    var affixTop    = $('#transacsstion-specimens').position().top + 200;
    //var affixBottom = (($other.position().top) + $main.outerHeight(true) - 60);
    if ($specimens.outerHeight(true) > 500) {
        $('#transaction-specimen-status-buttons').affix({
            offset: {
                top: affixTop
                //bottom: affixBottom
            }
        });
    }
});


