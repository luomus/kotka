	
	function elementCopier(elementClass) {
		$("."+elementClass+":last").after("<button class=\"elementCopier\" onclick=\"elementCopy('"+elementClass+"'); return false;\">+ Open new identification section</button>");
		elementCopier_addDeleteButtons(elementClass);
	}
		
	function elementCopy(elementClass) {
		try {
			var original = $("."+elementClass+":first");
	
			var copy = original.clone();
				
			var newId = ++elementCopierMaxID[elementClass];
				
			copy.find("input,textarea,select").each(function(){
				var name = $(this).attr("name");
				var newName = elementCopierRename(name, newId);
				$(this).attr("name", newName);
				$(this).attr("id", newName);
			});
			copy.find("input,textarea,select").val('');
		
			copy.find("label").each(function(){
				var name = $(this).attr("for");
				var newName = elementCopierRename(name, newId);
				$(this).attr("for", newName);
			});
			copy.addClass("elementCopier_new");
		
			// Tästä alkaa Kotkaa varten ei-yleiskäyttöiset asiat 
			copy.find(".portlet-header").text('New identification');
			copy.find(".portlet-content").show();
			// --
		
			copy.hide();
			$("."+elementClass+":last").after(copy);
			elementCopier_addDeleteButtons(elementClass);
			copy.fadeIn(1000);
		} catch (err) {
			alert(err.stack);
		}
	}
	
	function elementCopierRename(name, newId) {
		return name.substring(0, name.indexOf("[")) +"[" + newId + "]" + name.substring(name.indexOf("]")+1, name.length);
	}
	
	function elementCopier_delete(element, elementClass) {
		var element = $(element).parent(); 
		element.fadeOut(100);
		setTimeout(function(){element.remove(); elementCopier_addDeleteButtons(elementClass);}, 100);
	}
	
	function elementCopier_addDeleteButtons(elementClass) {
		$("."+elementClass).not(".elementCopier_delete_added").each(function() {
			$(this).append("<button class=\"elementCopier elementCopier_delete elementCopier_delete_"+elementClass+"\" onclick=\"if (confirm('Are you sure?')) elementCopier_delete(this, '"+elementClass+"'); return false;\">Delete</button>");
			$(this).addClass("elementCopier_delete_added");
		});
		if ($("."+elementClass).size() <= 1) {
			$(".elementCopier_delete_"+elementClass).hide();
		} else {
			$(".elementCopier_delete_"+elementClass).show();
		}
	}
	