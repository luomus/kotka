
$.extend($.tablesorter.themes.bootstrap, {
    // these classes are added to the table. To see other table classes available,
    // look here: http://twitter.github.com/bootstrap/base-css.html#tables
    table      : 'table table-striped table-bordered table-condensed table-hover',
    caption    : 'caption',
    header     : 'bootstrap-header', // give the header a gradient background
    footerRow  : '',
    footerCells: '',
    icons      : '', // add "icon-white" to make them white; this icon class is added to the <i> in the header
    sortNone   : '',
    sortAsc    : 'icon-chevron-up fa fa-chevron-up',     // includes classes for Bootstrap v2 & v3
    sortDesc   : 'icon-chevron-down fa fa-chevron-down', // includes classes for Bootstrap v2 & v3
    active     : '', // applied when column is sorted
    hover      : '', // use custom css here - bootstrap class may not override it
    filterRow  : '', // filter row class
    even       : '', // odd row zebra striping
    odd        : ''  // even row zebra striping
});

var $tableSorter = $('.table-sortable');
var lastPage = 0;

$tableSorter.tablesorter({
    // this will apply the bootstrap theme if "uitheme" widget is included
    // the widgetOptions.uitheme is no longer required to be set
    theme : "bootstrap",

    widthFixed: true,

    headerTemplate : '{content} {icon}', // new in v2.7. Needed to add the bootstrap icon!

    // widget code contained in the jquery.tablesorter.widgets.js file
    // use the zebra stripe widget if you plan on hiding any rows (filter widget)
    widgets : [ "saveSort", "uitheme", "filter"],

    widgetOptions : {

        // reset filters button
        filter_reset : ".reset",
        filter_saveFilters: true
    },
    sortList: [[1,0]]
}).on('pagerInitialized pagerComplete', function (e, c) {
    var options = {
        aroundCurrent: 3
    };

    var indx,
        p = c.pager ? c.pager : c, // using widget
        content = '',
        pages = '',
        label = '',
        pageArray = [],
        cur = p.page + 1,
        start = cur > 1 ? (p.filteredPages - cur < options.aroundCurrent ? -(options.aroundCurrent + 1) + (p.filteredPages - cur) : -options.aroundCurrent) : 0,
        end = cur < options.aroundCurrent + 1 ? options.aroundCurrent + 3 - cur : options.aroundCurrent + 1;
        lastPage = p.filteredPages;

    for (indx = start; indx < end; indx++) {
        if (cur + indx >= 1 && cur + indx < p.filteredPages) { pageArray.push( cur + indx ); }
    }

    // make sure first and last page are included in the pagination
    if ($.inArray(1, pageArray) === -1) {
        pageArray.push(1);
    }
    if ($.inArray(c.filteredPages, pageArray) === -1) {
        pageArray.push(c.filteredPages);
    }
    // sort the list
    pageArray = pageArray.sort(function (a, b) {
        return a - b;
    });
    // make links and spacers
    $.each(pageArray, function (idx, value) {
        if (value === 0) {
            return;
        }
        label = (pageArray.length > 1 && idx == pageArray.length - 1 && ( pageArray[ idx - 1 ] + 1 !== value ) ? '...' : '');
        label += value;
        label += (pageArray.length > 1 && idx == 0 && ( pageArray[ idx + 1 ] - 1 !== value ) ? '...' : '');
        pages += '<li class="' + (value === cur ? 'active' : '') + '"><a href="#" data-goto="'+ value +'">' + label + '</a></li>';
    });
    content += '<li><a href="#" class="prev" data-goto="' + (cur - 1) + '">&laquo; Previous</a></li>' +
            pages +
            '<li><a href="#" class="next" data-goto="' + (cur + 1) + '">Next &raquo;</a></li>';
    $('.pagination').html(content);
}).tablesorterPager({

        // target the pager markup - see the HTML block below
        container: $(".ts-pager"),

        size: 50,

        // remove rows from the table to speed up the sort of large tables.
        // setting this to false, only hides the non-visible rows; needed if you plan to add/remove rows with the pager enabled.
        removeRows: false,

        // output string - default is '{page}/{totalPages}';
        // possible variables: {page}, {totalPages}, {filteredPages}, {startRow}, {endRow}, {filteredRows} and {totalRows}
        output: '{startRow} - {endRow} of {filteredRows}'

    });

// set up pager controls
$('.pagesize').on('change', function() {
    $tableSorter.trigger('pageSize', $(this).val());
});
$('.pagination').on('click', 'a', function (e) {
    e.preventDefault();
    var toPage = $(this).data('goto');
    if (toPage < 1 || toPage > lastPage) {
        return;
    }
    $(this)
        .closest('li')
        .addClass('active')
        .siblings()
        .removeClass('active');
    $tableSorter.trigger('pageSet', toPage);
});