var $body = $(document.body);

$body.on('click', '.remove-on-click', function (e) {
    if (confirm('Are you sure you want to remove that?')) {
        $(this).closest("div[data-removable]").remove();
        $body.trigger('requestRefresh');
    }
    e.preventDefault();
});

$body.on('collection.add', function(e, $elem) {
    var dataElem = $elem.closest('.repeating');
    if (!dataElem.attr('data-count')) {
        dataElem = dataElem.find('[data-count]');
    }
    var count = parseInt(dataElem.data('count'));
    var placeholder = dataElem.data('placeholder');
    var template = dataElem.find('[data-template]').data('template');
    dataElem.data('count', count + 1);
    var regex = new RegExp(placeholder, "g");
    dataElem.append(template.replace(regex, count));
    $body.trigger('requestRefresh');
});

$body.on('click', '.add', function (e) {
    e.preventDefault();
    $body.trigger('collection.add', [$(this)]);
});