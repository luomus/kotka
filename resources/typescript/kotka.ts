declare var BootstrapDialog;

module Kotka {
    export class Util {
        public static errorDialog(title:string, msg:string) {
            BootstrapDialog.show({
                title: title,
                message: msg,
                size: BootstrapDialog.SIZE_SMALL,
                type: BootstrapDialog.TYPE_DANGER,
                buttons: [{
                    label: 'ok',
                    cssClass: 'btn-danger',
                    action: function(dialog){
                        dialog.close();
                    }
                }]
            });
        }
    }

    export class IdService {
        static defaultQNamePrefix:string = '';
        static map: any;

        constructor() {
            IdService.map = {'tun:': 'http://tun.fi/', 'luomus:': 'http://id.luomus.fi/'}
        }

        static getQName(val:string, generated:boolean = false):string {
            if (val.indexOf('http:') === -1) {
                if (val.indexOf(':') === -1) {
                    if (generated) {
                        return IdService.defaultQNamePrefix + val;
                    }
                    return 'luomus:' + val;
                }
                return val;
            }
            for(var qname in IdService.map) {
                if (IdService.map.hasOwnProperty(qname)) {
                    var uri:string = IdService.map[qname];
                    val = val.replace(uri,qname);
                }
            }
            return val;
        }

        static getUri(val:string, generated:boolean = false):string {
            if (val.indexOf('http:') === -1) {
                if (val.indexOf(':') === -1) {
                    if (generated) {
                        val = IdService.defaultQNamePrefix + val;
                    } else {
                        val = 'luomus:' + val;
                    }
                }
                for(var qname in IdService.map) {
                    if (IdService.map.hasOwnProperty(qname)) {
                        var uri:string = IdService.map[qname];
                        val = val.replace(qname,uri);
                    }
                }
            }
            return val;
        }

        static setDefaultQNamePrefix(val:string):void {
            this.defaultQNamePrefix = val;
        }

    }
}
