/// <reference path="./typings/jquery/jquery.d.ts" />
/// <reference path="./typings/bootstrap/bootstrap.d.ts" />
/// <reference path="./typings/ladda/ladda.d.ts" />
/// <reference path="./kotka.ts" />

declare var root;

interface JQuery {
    datetimepicker(options: any): JQuery;
    bootstrapSwitch(options: any): JQuery;
    sort(options: any): JQuery;
}

interface grawl
{
    notice(options: any): void;
}

interface JQueryStatic
{
    growl: grawl;
}

module Accessions {

    class URLS {
        static branchForm:string   = '/accessions/branchForm';
        static eventForm:string   = '/accessions/eventForm';
        static branch:string = '/accessions/branch';
        static event:string = '/accessions/event';
        static view:string   = '/view';
    }

    class HttpClient {

        private static cache:any = {};

        public static setCache(url:string, data:any) {
            HttpClient.cache[url] = data;
        }

        public static get(url:string, callback:(data:any) => void, useCache:boolean = false):void {
            if (useCache && HttpClient.cache[url]) {
                callback(HttpClient.cache[url]);
                return;
            }
            $.get(url, (data) => {
                if (useCache) {
                    HttpClient.setCache(url, data);
                }
                callback(data);
            }).fail(() => {
                Kotka.Util.errorDialog(
                    'Error loading data',
                    'Could not load wanted data. Please try again little later'
                );
            });
        }

        public static post(url:string, data:string, callback:(data:any) => void):void {
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: (data) => {
                    callback(data);
                },
                error: function (jqXHR) {
                    Ladda.stopAll();
                    Kotka.Util.errorDialog(
                        jqXHR.status === 403 ? 'Insufficient rights' : 'Failed to delete data',
                        jqXHR.status === 403 ? 'You do not have rights for the accession and that\'s why you cannot do this' : 'Wait for a while and try again. If still failing contact the kotka support.'
                    );
                }
            });
        }

        public static delete(url:string, callback:(data:any) => void): void {
            $.ajax({
                type: "DELETE",
                url: url,
                success: (data) => {
                    callback(data);
                },
                error: function (jqXHR) {
                    Ladda.stopAll();
                    Kotka.Util.errorDialog(
                        jqXHR.status === 403 ? 'Insufficient rights' : 'Failed to delete data',
                        jqXHR.status === 403 ? 'You do not have rights for the accession and that\'s why you cannot do this' : 'Wait for a while and try again. If still failing contact the kotka support.'
                    );
                }
            });
        }
    }

    class Form {
        private parent:string;
        private formSource:string;
        private element:JQuery;
        private visible:boolean = false;
        private qname:string;
        private loaded:boolean = false;
        private originalValue:string;

        constructor(formSource:string, element:JQuery, parent:string, qname:string = null) {
            this.formSource = formSource;
            this.parent = parent;
            this.element = element;
            this.qname = qname;
        }

        public getElement():JQuery {
            return this.element;
        }

        public getQName():string {
            return this.qname;
        }

        public delete(callback:(form:Form) => void):void {
            var ladda = Ladda.create(this.getFormElement().find('.save-form')[0]);
            ladda.start();

            HttpClient.delete(this.formSource, (data) => {
                var formContainer = $(data).find('.form-data');
                ladda.stop();
                if (formContainer.data('success')) {
                    this.qname = formContainer.data('form-id');
                    this.originalValue = formContainer.find('form').serialize();
                    HttpClient.setCache(this.formSource, data);
                    this.getPopoverElement().data('bs.popover').options.content = data;
                    $.growl.notice({title: 'Successfully', message: 'deleted'});
                    this.show();
                    callback(this);
                } else {
                    this.getFormElement().html(data);
                    Service.container.trigger('refresh');
                }
            });

        }

        public save(callback:(form:Form) => void):void {
            var formData = this.getFormElement().serialize();
            var ladda = Ladda.create(this.getFormElement().find('.save-form')[0]);
            ladda.start();

            HttpClient.post(this.formSource, formData, (data) => {
                var formContainer = $(data).find('.form-data');
                ladda.stop();
                if (formContainer.data('success')) {
                    this.qname = formContainer.data('form-id');
                    this.originalValue = formContainer.find('form').serialize();
                    HttpClient.setCache(this.formSource, data);
                    this.getPopoverElement().data('bs.popover').options.content = data;
                    $.growl.notice({title: 'Successfully', message: 'saved'});
                    this.show();
                    callback(this);
                } else {
                    this.getFormElement().html(data);
                    Service.container.trigger('refresh');
                }
            });
        }

        public show():void {
            if (!this.element.is(':visible')) {
                this.element.show();
            }
            this.getForm(() => {
                this.visible = true;
                this.getPopoverElement().popover('show');
            });
        }

        public getFormElement():JQuery {
            var popoverID:string = this.getPopoverElement().attr('aria-describedby');
            return $('#' + popoverID).find('form');
        }

        private getPopoverElement():JQuery {
            if (this.element.hasClass('popover-elem')) {
                return this.element;
            }
            return this.element.find('.popover-elem:first');
        }

        private getForm(callback:() => void) {
            if (!this.loaded) {
                HttpClient.get(this.formSource, (data) => {
                    this.init($(data));
                    this.loaded = true;
                    callback();
                }, true);
            } else {
                callback();
            }
        }

        private init(html:JQuery) {
            var parentInput = html.find('.parent-id input');
            if (parentInput.length > 0) {
                parentInput.val(this.parent);
            }
            this.originalValue = html.find('form').serialize();
            this.getPopoverElement().popover({
                animation: false,
                content: html.html(),
                html: true,
                trigger: 'manual',
                placement: 'bottom'
            });
        }

        public hide(force = false):void {
            if (!force && this.visible && this.originalValue !== this.getFormElement().serialize()
                && !confirm("Are you sure you want discard the changes you've made")) {
                return;
            }
            this.visible = false;
            if (this.loaded) {
                this.getPopoverElement().popover('hide');
            }
            Service.container.find('.dummy').hide();
        }

        public isVisible():boolean {
            return this.visible;
        }

        public isBranch():boolean {
            return (this.element.find('.branch').length > 0 || this.element.hasClass('branch'))
        }

        public isDummy():boolean {
            return this.element.hasClass('dummy');
        }

    }

    class FormContainer {

        private forms:Array<Form> = [];
        private active:string = '-1';
        private idx = -1;

        public addEvent(parent:string, element:JQuery) {
            if (this.getActiveForm() instanceof Form) {
                this.getActiveForm().hide();
            }
            if (!this.forms[parent]) {
                this.forms[parent] = new Form(URLS.eventForm, element, parent);
            }
            this.active = parent;
            this.showActive();
        }

        public addBranch(root:string, element:JQuery) {
            if (this.getActiveForm() instanceof Form) {
                this.getActiveForm().hide();
            }
            if (!this.forms['-1']) {
                this.forms['-1'] = new Form(URLS.branchForm, element, root);
            }
            this.active = '-1';
            this.showActive();
        }

        public editBranch(root:string, qname:string, element:JQuery) {
            if (this.getActiveForm() instanceof Form) {
                this.getActiveForm().hide();
            }
            for(var i in this.forms) {
                if (this.forms[i].getQName() === qname) {
                    this.active = i;
                    this.showActive();
                    return;
                }
            }
            var idx = ++this.idx;
            var url = URLS.branchForm + '?uri=' + qname;
            this.forms[idx] = new Form(url, element, root);
            this.active = ''+idx;
            this.showActive();
        }

        public editEvent(parent:string, qname:string, element:JQuery) {
            if (this.getActiveForm() instanceof Form) {
                this.getActiveForm().hide();
            }
            for(var i in this.forms) {
                if (this.forms[i].getQName() === qname) {
                    this.active = i;
                    this.showActive();
                    return;
                }
            }
            var idx = ++this.idx;
            var url = URLS.eventForm + '?uri=' + qname;
            this.forms[idx] = new Form(url, element, parent);
            this.active = ''+idx;
            this.showActive();
        }

        public getActiveForm():Form {
            if (!this.forms[this.active]) {
                return null;
            }
            return this.forms[this.active];
        }

        public showActive() {
            this.getActiveForm().show();
        }

    }

    export class Service {

        public static container:JQuery;
        private root;
        private formContainer;

        constructor(container:JQuery, root:string) {
            Service.container = container;
            this.root = root;
            this.formContainer = new FormContainer();
        }

        private getDataElement(element:JQuery) {
            var dataElem = element.closest('.elem-data');
            if (dataElem.length !== 1) {
                Kotka.Util.errorDialog(
                    'Error loading element data',
                    'Could not find element that holds the data. Please contact Kotka support'
                );
                return null;
            }
            return dataElem;
        }

        public init() {
            // init depleted switch
            $('.depleted-switch')
                .bootstrapSwitch({
                    onText: 'show',
                    offText: 'hide'
                })
                .on('switchChange.bootstrapSwitch', (event:JQueryEventObject, state:boolean) => {
                    if (state) {
                        Service.showDepleted();
                    } else {
                        Service.hideDepleted();
                    }
                });

            // init listeners handling
            Service.container
                .on('click', '.show-accession', () => {
                    const $modal = $('#modal');
                    const $modalBody = $('#modal .modal-body');
                    const url = '/view?uri=' + Util.getRoot() + '&format=noLayout';
                    $modalBody.html('<img src="/img/ajax-loader.gif" />');
                    $.get(url, function (html) {
                        $modalBody.html(html);
                    }).fail(function() {
                        $modalBody.html('<h1>Not found</h1>');
                    });
                    $modal.modal('show');
                    $modal.show();
                })
                .on('click', '.add-branch', (e:JQueryEventObject) => {
                    e.preventDefault();
                    var dataElem = this.getDataElement($(e.target));
                    var target = $(dataElem.data('branch-elem'));
                    target.find('.remove-container').hide();
                    this.formContainer.addBranch(this.root, target);
                })
                .on('click', '.edit-branch', (e:JQueryEventObject) => {
                    e.preventDefault();
                    var dataElem = this.getDataElement($(e.target));
                    this.formContainer.editBranch(this.root, dataElem.data('elem-id'), dataElem);
                })
                .on('click', '.add-event', (e:JQueryEventObject) => {
                    e.preventDefault();
                    var dataElem = this.getDataElement($(e.target));
                    var target = dataElem.find('.event.dummy');
                    if (!dataElem.find('.events').is(':visible')) {
                        dataElem.find('.branch').click();
                    }
                    this.formContainer.addEvent(dataElem.data('elem-id'), target);
                })
                .on('click', '.edit-event', (e:JQueryEventObject) => {
                    e.preventDefault();
                    var dataElem = this.getDataElement($(e.target));
                    var parentDataElem = this.getDataElement(dataElem.parent());
                    this.formContainer.editEvent(parentDataElem.data('elem-id'), dataElem.data('elem-id'), dataElem);
                })
                .on('click', '.close-form', (e:JQueryEventObject) => {
                    e.preventDefault();
                    this.formContainer.getActiveForm().hide();
                })
                .on('click', '.delete-form', (e:JQueryEventObject) => {
                    e.preventDefault();
                    if (confirm('Are you sure you want to delete this?')) {
                        this.formContainer.getActiveForm().delete((form:Form) => {
                            form.hide(true);
                            if (form.isBranch()) {
                                const branch = form.getElement().find('.branch').parent();
                                branch.remove();
                            } else {
                                const event = form.getElement();
                                const branch = event.parent().closest('.elem-data').find('.branch');
                                event.find('.event').parent().remove();
                                Branch.refresh(branch);
                            }
                        });
                    }
                })
                .on('click', '.save-form', (e:JQueryEventObject) => {
                    e.preventDefault();
                    this.formContainer.getActiveForm().save((form:Form) => {
                        if (form.isDummy()) {
                            // TODO: add without refresh
                            location.reload();
                            return;
                        }
                        if (form.isBranch()) {
                            Branch.getBranch(form.getQName(), (elem:JQuery) => {
                                var branch = form.getElement().find('.branch');
                                var classes = elem.attr('class');
                                branch.html(elem.html());
                                branch.attr('class', classes);
                                Branch.refresh(branch);
                            });
                        } else {
                            Event.getEvent(form.getQName(), (elem:JQuery) => {
                                const event = form.getElement();
                                event.find('.event').html(elem.find('.event').html());
                                event.attr('data-sorting', elem.attr('data-sorting'));
                                event.attr('data-amount', elem.attr('data-amount'));
                                event.attr('data-element-id', elem.attr('data-element-id'));
                                const branch = event.parent().closest('.elem-data').find('.branch');
                                Branch.refresh(branch);
                                form.show();
                            });
                        }
                    });
                })
                .on('click', '.branch', (e:JQueryEventObject) => {
                    var target = $(e.target);
                    if (target.hasClass('no-event-action')) {
                        return;
                    }
                    var events = this.getDataElement(target).find('.events');
                    if (events.is(':visible')) {
                        events.slideUp();
                    } else {
                        events.slideDown();
                    }
                })
                .on('refresh', () => {
                    Service.container.trigger('shown.bs.popover');
                })
                .on('shown.bs.popover', (e) => {
                    Service.container.find('.add-datetimepicker')
                        .removeClass('add-datetimepicker')
                        .datetimepicker({
                            widgetPositioning: {
                                horizontal: 'auto',
                                vertical: 'bottom'
                            },
                            keepInvalid: true,
                            icons: {
                                time: "fa fa-clock-o",
                                date: "fa fa-calendar",
                                up: "fa fa-arrow-up",
                                down: "fa fa-arrow-down",
                                previous: "fa fa-angle-left",
                                next: "fa fa-angle-right"
                            }
                        })
                        .end()
                        .find('.add-select2')
                        .removeClass('add-select2')
                        .each(function (index, elem) {
                            $(elem).select2({
                                placeholder: '',
                                allowClear: true
                            })
                        })
                        .end()
                        .find('.event-type-listener')
                        .removeClass('event-type-listener')
                        .each(function (index, eventTypeElem) {
                            const $eventType = $(eventTypeElem).find('.PUUEventType');

                            $eventType.on('change', () => {
                                const targetValue = $eventType.val();
                                $eventType
                                    .closest('.form-data')
                                    .find('[data-type]')
                                    .each((index, elem) => {
                                        const $elem = $(elem);
                                        const shown = JSON.parse($elem.data('type').replace(/'/g, '"'));
                                        if (shown.indexOf(targetValue) === -1) {
                                            $elem.hide();
                                        } else {
                                            $elem.show();
                                        }
                                    });
                            });
                            $eventType.trigger('change');
                        })
                        .end()
                        .find('.add-yes-no')
                        .removeClass('add-yes-no')
                        .find("[type='checkbox']")
                        .bootstrapSwitch({
                            onText: 'Yes',
                            offText: 'No'
                        });
                });
            Service.container.find('.branch').not('.dummy').each((key, value) => {
                Branch.refresh($(value));
            });
        }

        public static showDepleted() {
            $('.depleted').closest('.elem-data').removeClass('hidden-branch');
        }

        public static hideDepleted() {
            $('.depleted').closest('.elem-data').addClass('hidden-branch');
        }
    }

    class Event {
        public static getEvent(qname:string, callback:(element:JQuery)=>void) {
            var url = URLS.event + '?uri=' + qname;
            HttpClient.get(url, (data) => {
                callback($(data));
            });
        }
    }

    class Branch {

        public static getBranch(qname:string, callback:(element:JQuery)=>void, fullBranch:boolean = false) {
            var url = URLS.branch + '?uri=' + qname;
            if (fullBranch) {
                url += '&full=true';
            }
            HttpClient.get(url, (data) => {
               callback($(data));
            });
        }

        public static refresh(branch:JQuery) {
            var events = branch.closest('.elem-data').find('.events');
            branch.find('.event-cnt').text(events.find('.event').length);
            events.find('.elem-data').sort((a,b) => {
                const dateA = parseInt(a.getAttribute('data-sorting'));
                const dateB = parseInt(b.getAttribute('data-sorting'));
                if (dateA === dateB) {
                    const idA = parseInt(a.getAttribute('data-elem-id').replace( /[^\d]/g, '' ));
                    const idB = parseInt(b.getAttribute('data-elem-id').replace( /[^\d]/g, '' ));
                    return idB - idA;
                } else if (dateA < dateB) {
                    return 1;
                }
                return -1;
            }).appendTo(events);
            const amountElem:JQuery = events.find('.event-amount:first');
            let amount: any = 0;
            let amountReady = false;
            events.find('.elem-data').each((index: number, elem: Element) => {
                if (amountReady) {
                    return;
                }
                const elemCount = '' + elem.getAttribute('data-amount');
                const firstChar = elemCount.substr(0, 1);
                const elemAmount = parseInt(elemCount.replace(/[^\d]/g, '' ));
                if (!isNaN(elemAmount) && typeof elemAmount === 'number') {
                    if (firstChar === '-') {
                        amount -= elemAmount;
                    } else if (firstChar === '+') {
                        amount += elemAmount;
                    } else {
                        amount += elemAmount;
                        amountReady = true;
                    }
                } else if (elemCount) {
                    amount = elemCount;
                    amountReady = true;
                }
            });
            if (amount <= 0 && !branch.hasClass('depleted')) {
                amount = amount + ' <strong style="color: #faebcc"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></strong>';
            }
            branch.find('.branch-amount').html('' + amount);
        }
    }

    class Util {
        public static getRoot():string {
            return $('#root').data('elem-id');
        }
    }

}

